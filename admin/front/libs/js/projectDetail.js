$(document).ready(function() {
    $('.nav-link').each(function() {
        $(this).on('click', function(){
            let id = $(this).attr('id')
            $('.nav-link').removeClass('active')
            $(this).addClass('active')

            $('.tab-pane').removeClass('show active')
            $('.tab-pane[aria-labelledby="' + id + '"]').addClass('show active')
        })
    })

    $('ul.debug-list li.tree-item div').on('click', function(event) {
        
        if (event.target.tagName != 'DIV' && event.target.tagName != 'SPAN') return;
        console.log(event.target.tagName);
        let childrenList
        if (event.target.tagName == 'DIV') childrenList = $(event.target).parent('li').children('ul');
        else childrenList = $(event.target).parent('div').parent('li').children('ul');
        
        if (!childrenList) return;
        console.log(childrenList.attr('class'));
        // childrenList.hidden = !childrenList.hidden;

        if ($(childrenList).hasClass('hidden')) {
            $(childrenList).removeClass('hidden');
            $(childrenList).css('display', 'block');
        }

        else {
            $(childrenList).addClass('hidden');
            $(childrenList).css('display', 'none');
        }
    })
})