// Прелоадер
// function loadData() {
//   return new Promise((resolve, reject) => {
//     // setTimeout не является частью решения
//     // Код ниже должен быть заменен на логику подходящую для решения вашей задачи
//     setTimeout(resolve, 1000);
//   })
// }

// loadData()
// 	.then(() => {
// 	let preloaderEl = document.getElementById('preloader');
// 	preloaderEl.classList.add('hidden');
// 	preloaderEl.classList.remove('visible');
// });

window.onload = function() {
	let preloaderEl = document.getElementById('preloader');
	preloaderEl.classList.add('hidden');
	preloaderEl.classList.remove('visible');
};

jQuery(document).ready(function(){

	//.. 10.12.2021 Всплывающее окошко пользователя
	if ($('.right-block').length) {
		let userHover = 0
		// наведение курсора на элемент
		$( '.user-icon' ).hover(function(){
			$('.user-icon .fa-user').removeClass('text-danger')
			$('.user-icon .fa-user').css({'color':'#ffd556'})
			$('.user-popup').css({'opacity' : '1', 'visibility':'visible'})
		// указатель выходит из элемента
		}, function(){
			$('.user-icon .fa-user').addClass('text-danger')
			$('.user-icon .fa-user').css('color', '')

			$( '.user-popup' ).hover(function(){
				userHover = 1
			}, function(){
				userHover = 0
			});

			setTimeout(function() {
				if (userHover == 0) {
					$('.user-popup').css({'opacity' : '0', 'visibility':'hidden'})
				}
			}, 500);
		});
		$( '.user-popup' ).hover(function(){
			// code
		}, function(){
			setTimeout(function() {
				if ($('.user-icon:hover').length == 0) {
					$('.user-popup').css({'opacity' : '0', 'visibility':'hidden'})
				}
			}, 500);
		});
	}

	// После 4.03.2022 закомментированный код ниже можно удалить (период тестирования другого способа)
	// let footerSize = 44;
	// Чтобы футер всегда был внизу
 	//    if (($(document).height() > ($(window).height()-footerSize))) $('.footerblock').css('position', 'relative');
 	//    $(window).resize(function() {
	//     if (($(document).height() > ($(window).height()-footerSize))) $('.footerblock').css('position', 'relative');
	//     else {
	//     	$('.content-container').css('margin-bottom', '70px');
	//     	$('.footerblock').css('position', 'absolute');
	//     }
	// });
	// $(document).mouseup(function() {
	// 	if (($(document).height() > ($(window).height()-footerSize))) $('.footerblock').css('position', 'relative');
	// 	else {
	// 		$('.content-container').css('margin-bottom', '70px');
	// 		$('.footerblock').css('position', 'absolute');
	// 	}
	// })
	// timer = setInterval(
	// 	function(){
	// 		console.log($(document).height())
	// 		console.log($(window).height()-footerSize)
	// 		if (($(document).height() > ($(window).height()-footerSize))) $('.footerblock').css('position', 'relative');
	// 		else {
	// 			$('.content-container').css('margin-bottom', '70px');
	// 			$('.footerblock').css('position', 'absolute');
	// 		}
	// 	},500
	// );

	// Если есть админ-панель.
	if ($('#header-panel').length > 0) {
		const copyButton = document.querySelector('#copyContent');
		let panel = $('#header-panel'), hp = $('#header-panel').css('height');
		panel.css('top', '-' + hp);
		// Раскрытие/скрытие админ-панели
		$('#header-panel i').on('click', function() {
			if (!panel.hasClass('slideUp') && !panel.hasClass('slideDown')) {
				panel.addClass('slideDown');
				panel.css('top', '0px');
				$(this).removeClass('fa-chevron-down');
				$(this).addClass('fa-chevron-up');
			} else if(panel.hasClass('slideUp')) {
				panel.removeClass('slideUp');
				panel.addClass('slideDown');
				panel.css('top', '0px');
				$(this).removeClass('fa-chevron-down');
				$(this).addClass('fa-chevron-up');
			} else if(panel.hasClass('slideDown')) {
				panel.removeClass('slideDown');
				panel.addClass('slideUp');
				panel.css('top', '-' + hp);
				$(this).removeClass('fa-chevron-up');
				$(this).addClass('fa-chevron-down');
			}
		})

		// По ctrl+a выделяем только текст во всплывающем окне
		// var isCtrl = false;
		// document.onkeyup=function(e){ if(e.which == 17) isCtrl=false; }
		// document.onkeydown=function(e) {
		//     if(e.which == 17) isCtrl=true;
		//     if(e.which == 65 && isCtrl === true) {
		//     	if ($('#devModal').css('display') != 'none') {
		    		
		//     	}
		//     	console.log($('#params-Content div.active pre').text());
		//         // return false;
		//     }
		// } 

		// $('#devModal #copyContent').on('click', function() {
		// 	let sb = $('#paramsContent div.active pre').attr('id');
		// 	$('#'+sb).select();
		// 	document.execCommand('copy');
		// })

		// copyButton.addEventListener('click', () => {
		// 	var $temp = $("<input>");
		// 	$("body").append($temp);
		// 	$temp.val($('#paramsContent div.active pre').text()).select();
		// 	$temp = $temp.val();
		// 	console.log($temp);
		// 	window.navigator.clipboard.writeText('$temp');
		// 	$temp.remove();
		// })

		// По нажатию на кнопки Сессия/POST/GET в меню админа
		// делается активной соответствующая вкладка в модальном окне.
		$('#showSession').bind('click', function() {
			$('#nav-tab a').removeClass('active');
			$('#paramsContent div').removeClass('show active');
			$('#session-tab').addClass('active');
			$('#nav-session').addClass('show active');
		})
		$('#showPost').bind('click', function() {
			$('#nav-tab a').removeClass('active');
			$('#paramsContent div').removeClass('show active');
			$('#post-tab').addClass('active');
			$('#nav-post').addClass('show active');
		})
		$('#showGet').bind('click', function() {
			$('#nav-tab a').removeClass('active');
			$('#paramsContent div').removeClass('show active');
			$('#get-tab').addClass('active');
			$('#nav-get').addClass('show active');
		})
		//==========================================================================
	    //========== Горизонтальный скроллинг массивов всплывающего окна ===========
	    //.. На мобильном устройстве включаем горизонтальный скроллинг
	    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
	        $('#paramsContent').css('overflow-x', 'scroll');
	    }
	    //.. Горизонтальный скроллинг таблицы заказов стрелками
	    var container = $('#paramsContent');
	    var duration = 2000;
	    $(document).on('keydown',function(e){
	    	let scrollBlockId = $('#paramsContent div.active pre').attr('id');
	    	let tblWidth = $('#'+scrollBlockId).width();
	        if (!$("#paramsContent div.active").is(":focus")) {
	            if(e.which == 39) {
	                container.animate({
	                    scrollLeft: tblWidth
	                }, duration, 'linear');
	            } else if(e.which == 37) {
	                container.animate({
	                    scrollLeft: 0
	                }, duration/6, 'linear');
	            }
	        }
	    });
	    $(document).keyup(function(){
	        container.stop(true);
	    });
	    //========== Горизонтальный скроллинг массивов всплывающего окна ===========
	    //==========================================================================
	}

	//================================== Скрипты для отладки =======================
	// Переключение чекбокса отладки в доп. меню хедера
	if ($('.debugSwitch').length) {
		let debugModeFlag = 0
		$('.debugSwitch input').on('change', function () {
			$('.debugSwitch').toggleClass('active')
			if ($('.debugSwitch').hasClass('active')) {
				$('.debugSwitch input').attr('checked', 'checked')
				debugModeFlag = 1
			} else {
				$('.debugSwitch input').removeAttr('checked')
				debugModeFlag = 0
			}
			$.post("/ajax_resp.php", {"debugMode": debugModeFlag})
		})
	}
	// const list = $('ul.debug-list li');
	// console.log(list.length);
	$('ul.debug-list li.tree-item div').on('click', function(event) {
		
	    if (event.target.tagName != 'DIV' && event.target.tagName != 'SPAN') return;
	    console.log(event.target.tagName);
	    let childrenList
	    if (event.target.tagName == 'DIV') childrenList = $(event.target).parent('li').children('ul');
	    else childrenList = $(event.target).parent('div').parent('li').children('ul');
	    
	    if (!childrenList) return;
	    console.log(childrenList.attr('class'));
	    // childrenList.hidden = !childrenList.hidden;

	    if ($(childrenList).hasClass('hidden')) {
			$(childrenList).removeClass('hidden');
			$(childrenList).css('display', 'block');
	    }

	    else {
	        $(childrenList).addClass('hidden');
	        $(childrenList).css('display', 'none');
	    }
	})
	//==============================================================================

    // --------------------------------------------------------------------------------------------------------
    //.. 8.12.2021 ----------------- Разблокировка проекта, отправленного в производство ----------------------
    //.. Нажатие на кнопку разблокировка проекта
    $('#unblockProject button').on('click', function(e) {
        $('#unblockProject').children('input').slideToggle().focus()

        if ($('#unblockProject button[name="unbProject"]').css('display') == 'none') {

            if ($('#unblockProject').children('input').val().length >= 3) {
                
                let projectName = $('#unblockProject').children('input').val()
                $.post("/ajax_resp.php", {"unblockProject": projectName}).done(function (data) {

                	let parse = JSON.parse(data)
					if (parse.status == 1) alert(parse.message)
					else alert('Ошибка: ' + parse.message)
                    
                    $('#unblockProject button[name="unbProject"]').css('display', 'block')
                    $('#unblockProject button[name="unbApply"]').css('display', 'none')

                    $('#unblockProject').children('input').slideToggle()
                    $('#unblockProject').children('input').val('')
                });
            }
            $('#unblockProject button[name="unbApply"]').css('display', 'none')
            $('#unblockProject button[name="unbProject"]').css('display', 'block')
        } else {
            $('#unblockProject button[name="unbProject"]').css('display', 'none')
            $('#unblockProject button[name="unbApply"]').css('display', 'block')
        }

    })

    //.. Ввод в инпут разблокировки проекта (обработка нажатия клавиши "Enter" когда фокус на инпуте)
    $(document).on('keypress', function(e) {
        if(e.which == 13 && $('#unblockProject').children('input').is(":focus")) {
            if ($('#unblockProject').children('input').val().length >= 3) {
                let projectName = $('#unblockProject').children('input').val()
                $.post("/ajax_resp.php", {"unblockProject": projectName}).done(function (data) {

                	let parse = JSON.parse(data)
					if (parse.status == 1) alert(parse.message)
					else alert('Ошибка: ' + parse.message)
                    
                    $('#unblockProject button').css('display', 'none')
                    $('#unblockProject button[name="unbProject"]').css('display', 'block')

                    $('#unblockProject').children('input').slideToggle()
                    $('#unblockProject').children('input').val('')
                });
            }
        }
    });
    //.. Ввод в инпут разблокировки проекта (обработка нажатия клавиши "Escape" когда фокус на инпуте)
    $(document).on('keyup', function(e) {
        if(e.which == 27 && ($('#unblockProject').children('input').is(":focus") || $('#unblockProject').children('input').css("display") != 'none')) {
            $('#unblockProject').children('input').val('')
            $('#unblockProject').children('input').slideToggle().focus()
            $('#unblockProject button').css('display', 'none')
            $('#unblockProject button[name="unbProject"]').css('display', 'block')
        }
    });
    // ------------------------ Разблокировка проекта, отправленного в производство ---------------------------
    // --------------------------------------------------------------------------------------------------------
});