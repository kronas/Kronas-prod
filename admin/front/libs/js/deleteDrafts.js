//.. 8.12.2021 ----------------- Удаление учётной записи контрагента ----------------------
//.. Нажатие на кнопку удаления
$('#deleteDraftButton').on('click', function(e) {

    const dataPath = $('input[name="requestPath"]').val();
    const mess = $('#deleteDrafts .messages');
    
    $.post(dataPath, {"deleteDrafts": "Drafts not empty"}).done(function (data) {

        let parse = JSON.parse(data)
        if (parse.status == 1) {
            mess.html('<p class="green">' + parse.message + '</p>');

        } else mess.html('<p class="red">' + parse.message + '</p>');

        setTimeout( () => {
            mess.fadeOut(350, function(){
                mess.html('');
                mess.fadeIn(350);
            });
        }, 5000);

    });

})