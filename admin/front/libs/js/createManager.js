$(document).ready(function() {
    let dataPath = $('input[name="requestPath"]').val();
	let managerData = new Object()
	const form   = $('.createManager'),
		  name   = $('.createManager input[name="name"]'),
		  pNum   = $('.createManager input[name="tel"]'),
		  email  = $('.createManager input[name="email"]'),
		  accId  = $('.createManager input[name="accentId"]'),
		  place  = $('.createManager input[name="place"]'),
		  pass   = $('.createManager input[name="password"]'),
		  submit = $('.createManager button[type="submit"]'),
		  mess   = $('.create_manager_container .messages')

	// Запретим "submit" по нажатию на "enter"
	form.on('keydown', function(event) {
		if(event.keyCode == 13) {
			event.preventDefault()
			
            managerData['name'] = name.val()
            managerData['phone'] = pNum.val()
            managerData['email'] = email.val()
            managerData['accentId'] = accId.val()
            managerData['place'] = place.val()
            managerData['passw'] = pass.val()
			// console.log('Enter')
			$.post(dataPath, {'managerData':managerData}).done(function (data) {
				let parse = JSON.parse(data)
				if (parse.status == 1) {
                    mess.html('<p class="green">'+parse.message+'</p>')

                    name.val('');
                    pNum.val('');
                    email.val('');
                    accId.val('');
                    place.val('');
                    pass.val('');

				} else mess.html('<p class="red">'+parse.message+'</p>')
				console.log(data)
				console.log(parse.status)
			});
		}
	});

	// Запретим "submit" по нажатию на кнопку "Создать"
	submit.on('click', function(event) {
		event.preventDefault()
		
		managerData['name'] = name.val()
        managerData['phone'] = pNum.val()
        managerData['email'] = email.val()
        managerData['accentId'] = accId.val()
        managerData['place'] = place.val()
        managerData['passw'] = pass.val()
		// console.log(name.val().length)
		$.post(dataPath, {'managerData':managerData}).done(function (data) {
			let parse = JSON.parse(data)
			if (parse.status == 1) {
                mess.html('<p class="green">'+parse.message+'</p>')

                name.val('');
                pNum.val('');
                email.val('');
                accId.val('');
                place.val('');
                pass.val('');
                
			} else mess.html('<p class="red">'+parse.message+'</p>')
			console.log(data)
			console.log(parse.status)
		});
	});

	//.. Маска для телефона
    $(function(){
      //2. Получить элемент, к которому необходимо добавить маску
      $('#managerPhone').mask("+38(099)999-99-99");
    });
	
	//.. Автодополнение филиала
    $( "#mngPlace" ).autocomplete({
        source: function(request, response){
            let managerPlace = {
                field:   'place',
                request: request.term
            };
            $.ajax({
                url: dataPath,
                method: 'POST',
                dataType: 'json',
                data: {
                    managerPlace
                },
                success: function (data) {
                    response(data);
                }
            });
        }
    });
})