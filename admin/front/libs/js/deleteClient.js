//.. 8.12.2021 ----------------- Удаление учётной записи контрагента ----------------------
//.. Нажатие на кнопку удаления
$('#deleteClient button').on('click', function(e) {

    const dataPath = $('input[name="requestPath"]').val();
    const clientId = $('input[name="clientId"]');
    const mess = $('#deleteClient .messages');
    
    $.post(dataPath, {"deleteClient": clientId.val()}).done(function (data) {
        console.log(data);
        let parse = JSON.parse(data)
        if (parse.status == 1) {
            mess.html('<p class="green">' + parse.message + '</p>');
            clientId.val('');

        } else mess.html('<p class="red">' + parse.message + '</p>');

        setTimeout( () => {
            mess.fadeOut(350, function(){
                mess.html('');
                mess.fadeIn(350);
            });
        }, 5000);

    });

})