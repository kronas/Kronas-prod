$(document).ready(function () {
    let dataPath = $('#dataPath').attr('data-path');
    let to = $('#redirectPath').attr('data-path');
    $('input[type="tel"]').mask('+38(000)000-00-00', {
        placeholder: "+38(0__)___-__-__"
    });
    $('#client_phone').on('input', function (e) {
        let phone = $(e.target).val();
        if(phone.length == 17) {
            $.get( dataPath + "?to=" + to + "&check_client_phone=" + phone, function( data ) {
                // Если нет такого клиента...
                // console.log(data)
                if(data == 0) {
                    $('#phone_check_result').html('');
                    $('#main_phone_container').hide('slow');
                    $('#main_register_container').show('slow');
                    $('#client_phone_check').val($('#client_phone').val());
                    $('#client_phone_input').val($('#client_phone').val());
                } else if (data == 2) {
                    $('#client_exists_in_accent').html('Клиент создан автоматичестки, так как уже был зарегистрирован в Акценте!')
                } else if (data == 3) {
                    $('#client_exists_in_accent').html('Данные клиента (имя, телефон, почта) успешно обновлены, так как они были изменены в Акценте!');
                } else {
                    $('#phone_check_result').html('Клиент с таким номером уже существует!')
                }
            });
        }
    });
});