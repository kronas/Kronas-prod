//.. 8.12.2021 ----------------- Разблокировка проекта, отправленного в производство ----------------------
//.. Нажатие на кнопку разблокировка проекта
$('#unblockProject button').on('click', function(e) {

    const dataPath = $('input[name="requestPath"]').val();
    const projectNum = $('input[name="projectNum"]');
    const mess = $('#unblockProject .messages');
    
    $.post(dataPath, {"unblockProject": projectNum.val()}).done(function (data) {
        let parse = JSON.parse(data)
        if (parse.status == 1) {
            mess.html('<p class="green">' + parse.message + '</p>');
            projectNum.val('');

        } else mess.html('<p class="red">' + parse.message + '</p>');

        setTimeout( () => {
            mess.fadeOut(350, function(){
                mess.html('');
                mess.fadeIn(350);
            });
        }, 5000);

    });

})