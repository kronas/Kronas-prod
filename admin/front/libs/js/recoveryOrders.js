//.. 22.03.2023 - Восстановление заказов и проектов из резервного хранилища
//.. Нажатие на кнопку восстановления
$('#recoveryOrders button').on('click', function(e) {

    const dataPath = $('input[name="requestPath"]').val();
    const recoveryDate = $('input[name="recoveryDate"]');
    const mess = $('#recoveryOrders .messages');
    
    $.post(dataPath, {"recoveryOrders": recoveryDate.val()}).done(function (data) {
        // console.log(data);
        let parse = JSON.parse(data)
        if (parse.status == 1) {
            mess.html('<p class="green">' + parse.message + '</p>');
        } else mess.html('<p class="red">' + parse.message + '</p>');

        // setTimeout( () => {
        //     mess.fadeOut(350, function(){
        //         mess.html('');
        //         mess.fadeIn(350);
        //     });
        // }, 10000);

    });

})