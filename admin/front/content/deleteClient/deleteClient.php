<div id="deleteClient" class="deleteClient">
    <h5 class="red">Операция безвозвратная, вы уверены, что хотите удалить учётную запись клиента?</h5>
    <h6>Введите ID клиента</h6>
    <input type="hidden" name="requestPath" value="<?= $page[$currentPath]['webPath'] ?>">
    <input type="text" name="clientId">
    <button name="unbApply" class="btn btn-primary" title="Удалить учётную запись клиента">Удалить</button>
    <div class="messages"></div>
</div>