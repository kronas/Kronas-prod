<div class="container admin-menu">
    <ul class="list-group">
        <li class="list-group-item"><a href="client-create">Создать клиента</a></li>
        <li class="list-group-item"><a href="manager-create">Создать менеджера</a></li>

        <?php if ($_SESSION['user']['access'] >= 100): ?>
        <li class="list-group-item"><a href="delete-drafts">Удаление черновиков</a></li>    
        <li class="list-group-item"><a href="api-help">Справка по API</a></li>
        <li class="list-group-item"><a href="unblock-project">Разблокировать проект для редактирования</a></li>
        <li class="list-group-item"><a href="project-detail">Деталировки проектов</a></li>
        <li class="list-group-item"><a href="recovery-orders">Восстановить заказы после сбоя</a></li>
        <?php endif ?>
    </ul>
</div>