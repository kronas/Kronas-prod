<?php

// Подключаем функции по работе с БД
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');

$minusMonthDate = date('Y-m-d H:i:s', strtotime('-1 month'));

$sql = 'SELECT * FROM `ORDER1C` WHERE `DB_AC_IN` <= "' . $minusMonthDate . '" AND `status` = "черновик" AND `del_order` = 1';
$deletedOrders = getTableData($sql);
$countDeletedOrders = count($deletedOrders);

?>

<div  id="deleteDrafts" class="container deleteDrafts">
    <h4 class="mb-5">На цій сторінці можна остаточно видалити чернетки замовлень, які були видалені менеджерами.</h4>
    <p class="mb-5">Коли менеджер видаляє чернетку замовлення, вона не видаляється, а просто помічається як видалена та залишається в базі, 
        проте не виводиться на сторінку разом з іншими замовленнями. На цій сторінці можна остаточно видалити ці чернетки. 
        Видаляються всі чернетки, старші за місяць після видалення, тож можна не боятися за те, що видаляться свіжовидалені.
    </p>
    <?php if ($countDeletedOrders > 0): ?>
        <p class="h5 mb-4">Наразі кількість чернеток, які можна остаточно видалити: <?= $countDeletedOrders ?></p>
        <button id="deleteDraftButton" class="btn btn-primary" title="Видалити усі чернетки"> Видалити усі </button>
    <?php else: ?>
        <h4>Наразі немає чернеток для видалення.</h4>
    <?php endif ?>
    <input type="hidden" name="requestPath" value="<?= $page[$currentPath]['webPath'] ?>">
    <div class="messages mt-5"></div>
</div>