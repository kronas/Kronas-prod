<?php
// Не админов редиректим
if(!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
	header('Location: ' . $main_dir . '/login.php');
}

include_once 'apiSourceArr.php';

$title = 'Справка по API';
?>

<div class="container-fluid container-documentation">
	<div class="accordion collapseAccordion" id="apiAccordion">
	
	<?php $i = 1; ?>
	<?php foreach ($api as $key => $value): ?>

	  <div class="accordion-item">
	    <h2 class="accordion-header" id="heading<?= $i ?>">
	      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?= $i ?>" aria-expanded="true" aria-controls="collapse<?= $i ?>">
	      	<span><?= $key + 1; ?>. </span>
	        <?= $value['title'] ?>
	      </button>
	    </h2>
	    <div id="collapse<?= $i ?>" class="accordion-collapse collapse" aria-labelledby="heading<?= $i ?>" data-bs-parent="#apiAccordion">
	      <div class="accordion-body">
	        <h5>Метод: <span><?= $value['method'] ?></span></h5>
			<h5>Пример ссылки: <small><a href="<?= $value['link'] ?>" target="_blank"><?= $value['link'] ?></a></small></h5>
			<p><small class="red">На локальном сервере должен быть включен VPN</small></p>
			<?php if ($value['method'] == 'GET'): ?>
				<h5>Параметры: <span><?= $value['description'] ?></span></h5>
			<?php elseif ($value['method'] == 'POST'): ?>
				<h5>Параметры: <span><pre><?= $value['description'] ?></pre></span></h5>
			<?php endif ?>

			<?php if (preg_match('#[\}|\]]#', $value['response'])): ?>
				<h5>Ответ: <span><pre><?= $value['response'] ?></pre></span></h5>
			<?php else: ?>
				<h5>Ответ: <span><?= $value['response'] ?></span></h5>
			<?php endif ?>
	      </div>
	    </div>
	  </div>

	<?php $i++; ?>
	<?php endforeach ?>

	</div>
</div>