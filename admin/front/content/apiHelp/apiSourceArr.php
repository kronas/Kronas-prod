<?php

$api = array(
	0 => array(
		'title'			=> 'Регистрация нового контрагента',
		'method'		=> 'POST',
		'link'			=> $kronas_api_link . '/gibLabService/agentCreate',
		'description'	=> 	'<p class="ml-4">Тело запроса:</p>
	{
	    "ExternalId": 1, - id из сервиса, если не будет передаваться ставить 0 по умолчанию
	    "Type": "company", - тип клиента (company - юр. лицо, fface - физ. лицо)
	    "Name": "TestGibLab", - наименование клиента (если клиента, юр. лицо)
	    "Surname": "Пупкин", - фамилия (если клиент, физ. лицо)
	    "FirstName": "Вася", - имя (если клиента, физ. лицо)
	    "ManagerId": 34043, - id отв-нного менеджера из Акцента, не обязательное поле
	    "Code1C": "ABEFADB6-49B3-11EA-80F6-000C29B9690F", - GUID контрагента из 1C, не обязательное поле
	    "Phones": [ - массив телефонов
	        {
	            "TypeId": 1, - id типа телефона из Акцента, не обязательное поле
	            "Number": "+38(099)-303-61-59" - номер телефона
	         },
	         {"TypeId": 2, "Number": "+(099)3038160"}
	    ],
	    "Emails": [ - массив email
	        {
	           "TypeId": 1, - id типа email из Акцента, не обязательно поле
	           "Mail": "test@gmail.com" - email
	         }
	    ],
	    "Addresses":[- массив адресов
	        {
	            "TypeId": 2, - id типа адреса из Акцента, не обязательное поле
	            "CityId": 908, - id города из Акцента
	            "RegionId": 10, - id области из Акцента, не обязательное поле
	            "Street": "Куреневский переулок", - улица, не обязательное поле
	            "House": "17" - дом, не обязательное поле
	        },
	        {
	            "TypeId": 3,
	            "CityId": 908,
	            "RegionId": 10,
	            "Street": "Академика Королева",
	            "House": "2"
	        }
	    ]
	}',
        'response'		=> 'Id контрагента в Акценте',
	),
	1 => array(
		'title'			=> 'API <span class="highlight-text">по ID менеджера</span> возвращает массив с заказами данного менеджера',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/GetManufacturOrdersFromManager/78976/false/25',
		'description'	=> 	'<p class="ml-4">первый параметр - Id менеджера в Акценте</p>
         					<p class="ml-4">второй параметр - true - произв-е заказы, false - не произв-е</p>
         					<p class="ml-4">третий параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
         					<p class="ml-4">Страницы разбиты на порции по 100 элементов.<br></p>',
        'response'		=> '[
		    {
		        "Id": 9481284,
		        "No": "к428/7",
		        "Date": "2020-07-02",
		        "AuthorId": 78976,
		        "Status": "выполнен",
		        "ProjectId": 0,
		        "Place": "1",
		        "ClientId": 25699,
		        "ClientName": "Коваль І.Й.",
		        "ClientPhone": "+38(067)506-96-00",
		        "ClientEmail": "mblyuz_igor@ukr.net"
		    },
		    {
		        "Id": 9482876,
		        "No": "к623/7",
		        "Date": "2020-07-02",
		        "AuthorId": 78976,
		        "Status": "выполнен",
		        "ProjectId": 0,
		        "Place": "2",
		        "ClientId": 25699,
		        "ClientName": "Коваль І.Й.",
		        "ClientPhone": "+38(067)506-96-00",
		        "ClientEmail": "mblyuz_igor@ukr.net"
		    }
		]',
	),
	2 => array(
		'title'			=> 'API <span class="highlight-text">по части имени клиента</span> возвращает массив клиентов подпадающих под эту часть имени',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/FindNameFromAgents/туту/20',
		'description'	=> 	'<p class="ml-4">Первый параметр - строка с частью имени клиента</p>
         					<p class="ml-4">Второй параметр - кол-во записей выдаваемых в ответ</p>',
        'response'		=> '[
		    {
		        "ClientId": 80645,
		        "ClientName": "Клиент 1",
		        "ClientPhone": "+38(097)123-45-67",
		        "ClientEmail": "tutumba@ukr.net"
		    },
		    {
		        "ClientId": 84186,
		        "ClientName": "Клиент 2",
		        "ClientPhone": "+38(097)765-43-21",
		        "ClientEmail": "0000"
		    }
		]',
	),
	3 => array(
		'title'			=> 'API <span class="highlight-text">по части номера телефона клиента</span> возвращает массив клиентов подпадающих под эту часть номера телефона',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/FindPhoneFromAgents/097/30',
		'description'	=> 	'<p class="ml-4">Первый параметр - строка с номером телефона клиента</p>
         					<p class="ml-4">Второй параметр - кол-во записей выдаваемых в ответ</p>',
        'response'		=> '[
		    {
		        "ClientId": 80645,
		        "ClientName": "Клиент 1",
		        "ClientPhone": "+38(097)123-45-67",
		        "ClientEmail": "tutumba@ukr.net"
		    },
		    {
		        "ClientId": 84186,
		        "ClientName": "Клиент 2",
		        "ClientPhone": "+38(097)765-43-21",
		        "ClientEmail": "0000"
		    }
		]',
	),
	4 => array(
		'title'			=> 'API <span class="highlight-text">по ID Акцента</span> (10205361) возвращает всю информацию о заказе',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/GetOrderFromId/10205361',
		'description'	=> 	'<p class="ml-4">Первый параметр - id заказа из Акцента</p>',
        'response'		=> '[
		    {
		        "Id": 10205361,
		        "No": "к998/2",
		        "Date": "2021-02-03",
		        "AuthorId": 168065,
		        "Status": "в работе",
		        "ProjectId": 323430,
		        "Place": "1",
		        "ClientId": 27200,
		        "ClientName": "Інфолайн Київ",
		        "ClientPhone": "+38(096)740-00-07",
		        "ClientEmail": "stastitomir@gmail.com"
		    }
		]',
	),
	5 => array(
		'title'			=> 'API <span class="highlight-text">по номеру заказа Акцента</span> (ук588/2) возвращает заказы, соответсвующие данному номеру',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/GetOrderFromNo?number=ук588/2',
		'description'	=> 	'<p class="ml-4">Первый параметр - номер заказа заказа из Акцента</p>',
        'response'		=> '[
	    {
	        "Id": 10226850,
	        "No": "ук588/2",
	        "Date": "2021-02-11",
	        "AuthorId": 36145,
	        "Status": "выполнен",
	        "ProjectId": 0,
	        "Place": null,
	        "ClientId": 420,
	        "ClientName": "ООО Континент К (ОПТ) ФОП",
	        "ClientPhone": "+38(050)663-26-35",
	        "ClientEmail": "tvn@continent.zp.ua"
	    },
	    {
	        "Id": 9081653,
	        "No": "ук588/2",
	        "Date": "2020-02-13",
	        "AuthorId": 61277,
	        "Status": "выполнен",
	        "ProjectId": 0,
	        "Place": null,
	        "ClientId": 57217,
	        "ClientName": "Л Мастер (опт) ООО",
	        "ClientPhone": "+38(068)777-28-07",
	        "ClientEmail": "pi@l-master.kiev.ua"
	    }
	]',
	),
	6 => array(
		'title'			=> 'API <span class="highlight-text">по дате/диапазону дат</span> возвращает заказы, за данную дату/диапазон дат.',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/GetManufacturOrdersFromPeriod/2020-01-01/2021-02-04/20',
		'description'	=> 	'<p class="ml-4">Первый параметр - начальная дата периода</p>
         					<p class="ml-4">Второй параметр - конечная дата периода</p>
         					<p class="ml-4">Третий параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
         					<p class="ml-4">Страницы разбиты на порции по 100 элементов.<br></p>',
        'response'		=> '[
	    {
	        "Id": 10081890,
	        "No": "к7487/12",
	        "Date": "2020-12-28",
	        "AuthorId": 168065,
	        "Status": "выполнен",
	        "ProjectId": 292228,
	        "Place": "1",
	        "ClientId": 29540,
	        "ClientName": "Долгий Станислав Викторович ",
	        "ClientPhone": "+38(044)360-36-23",
	        "ClientEmail": "sdolgiy72@ukr.net"
	    },
	    {
	        "Id": 10082152,
	        "No": "к7514/12",
	        "Date": "2020-12-30",
	        "AuthorId": 72902,
	        "Status": "выполнен",
	        "ProjectId": 292299,
	        "Place": "2",
	        "ClientId": 29644,
	        "ClientName": "Мальчик Світлана Петрівна",
	        "ClientPhone": "+38(067)393-61-75",
	        "ClientEmail": "svmalchik@icloud.com"
	    }
	]',
	),
	7 => array(
		'title'			=> 'API <span class="highlight-text">по участку</span> (от 1 до 9) возвращает заказы, соответсвующие данному участку.',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/FindPhoneFromAgents/097/30',
		'description'	=> 	'<p class="ml-4">Первый параметр - id участка в сервисе.</p>
         					<p class="ml-4">Второй параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
         					<p class="ml-4">Страницы разбиты на порции по 100 элементов.<br></p>',
        'response'		=> '[
	    {
	        "Id": 10081890,
	        "No": "к7487/12",
	        "Date": "2020-12-28",
	        "AuthorId": 168065,
	        "Status": "выполнен",
	        "ProjectId": 292228,
	        "Place": "1",
	        "ClientId": 29540,
	        "ClientName": "Долгий Станислав Викторович ",
	        "ClientPhone": "+38(044)360-36-23",
	        "ClientEmail": "sdolgiy72@ukr.net"
	    },
	    {
	        "Id": 10082152,
	        "No": "к7514/12",
	        "Date": "2020-12-30",
	        "AuthorId": 72902,
	        "Status": "выполнен",
	        "ProjectId": 292299,
	        "Place": "2",
	        "ClientId": 29644,
	        "ClientName": "Мальчик Світлана Петрівна",
	        "ClientPhone": "+38(067)393-61-75",
	        "ClientEmail": "svmalchik@icloud.com"
	    }
	]',
	),
	8 => array(
		'title'			=> 'API <span class="highlight-text">по статусу</span> возвращает заказы, соответсвующие данному статусу. Статусы: в работе, выполнен, не проведен',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/GetManufacturOrdersFromStatus/в работе/4',
		'description'	=> 	'<p class="ml-4">Первый параметр - статус (в работе, выполнен, не проведен, не проведен)</p>
         					<p class="ml-4">Второй параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
         					<p class="ml-4">Страницы разбиты на порции по 100 элементов.<br></p>',
        'response'		=> '[
	    {
	        "Id": 10081890,
	        "No": "к7487/12",
	        "Date": "2020-12-28",
	        "AuthorId": 168065,
	        "Status": "выполнен",
	        "ProjectId": 292228,
	        "Place": "1",
	        "ClientId": 29540,
	        "ClientName": "Долгий Станислав Викторович ",
	        "ClientPhone": "+38(044)360-36-23",
	        "ClientEmail": "sdolgiy72@ukr.net"
	    },
	    {
	        "Id": 10082152,
	        "No": "к7514/12",
	        "Date": "2020-12-30",
	        "AuthorId": 72902,
	        "Status": "выполнен",
	        "ProjectId": 292299,
	        "Place": "2",
	        "ClientId": 29644,
	        "ClientName": "Мальчик Світлана Петрівна",
	        "ClientPhone": "+38(067)393-61-75",
	        "ClientEmail": "svmalchik@icloud.com"
	    }
	]',
	),
	9 => array(
		'title'			=> 'API <span class="highlight-text">по ID Акцента</span> (1029623623121 (не существующий id для безопасности)) <span class="highlight-text">удаляет заказ</span> (в Акценте)',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/RemoveOrder/1029623623121',
		'description'	=> 	'<p class="ml-4">Первый параметр - id заказа из Акцента</p>',
        'response'		=> '<p class="ml-4">При удачном удалении заказа, возвращается статус 200,<br />если удалить нельзя возвращается ответ в формате json</p>
    	{
	    "Error": "Инфо об ошибке"
	}',
	),
	10 => array(
		'title'			=> 'API <span class="highlight-text">по набору фильтров</span> возвращает массив отфильтрованных заказов.',
		'method'		=> 'POST',
		'link'			=> $kronas_api_link . '/gibLabService/GetManufacturOrdersFromFilters/1',
		'description'	=> 	'<p class="ml-4">Первый параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
         					<p class="ml-4">Страницы разбиты на порции по 100 элементов.</p>
         					<p class="ml-4"><small>	Любой параметр в теле запроса не обязательный (не нужно указывать null или что-то подобное, просто не добавляем его в json).<br />
	Касательно дат: если нужны заказы за период указываем startDate и endDate, если нужны заказы на конкретное число, то указываем параметр date.<br />
	Параметры startDate и endDate указываются только в паре, иначе заказы будут взяты за год от текущей даты.<br />
	Если не указан ни один из параметров даты, то заказы берутся за год от текущей даты.<br />
	Для выполнения запроса без параметров в теле запроса, добавляем в тело запроса пустой json ({})</small></p>
         					<p class="ml-4">Тело запроса:</p>
         					{
		    "client": 145632, - Id клиента из Акцента
		    "manager": 78976, - Id менеджера из Акцента
		    "place": 2, - Id участка из сервиса
		    "date": "2021-03-22", - конкретная дата на которую вернется список заказов
		    "startDate": "2020-03-26", - начальная дата периода
		    "endDate": "2021-03-26", - конечная дата периода
		    "status": "в работе", - статус (в работе, выполнен, не проведен)
		    "type": 1 - тип (1 -обычные, 2 - пильные, 3 - РС)
		}',
        'response'		=> '[
	    {
	        "Id": 10081890,
	        "No": "к7487/12",
	        "Date": "2020-12-28",
	        "AuthorId": 168065,
	        "Status": "выполнен",
	        "ProjectId": 292228,
	        "Place": "1",
	        "ClientId": 29540,
	        "ClientName": "Долгий Станислав Викторович ",
	        "ClientPhone": "+38(044)360-36-23",
	        "ClientEmail": "sdolgiy72@ukr.net"
	    },
	    {
	        "Id": 10082152,
	        "No": "к7514/12",
	        "Date": "2020-12-30",
	        "AuthorId": 72902,
	        "Status": "выполнен",
	        "ProjectId": 292299,
	        "Place": "2",
	        "ClientId": 29644,
	        "ClientName": "Мальчик Світлана Петрівна",
	        "ClientPhone": "+38(067)393-61-75",
	        "ClientEmail": "svmalchik@icloud.com"
	    }
	]',
	),
	11 => array(
		'title'			=> 'API <span class="highlight-text">по ID заказа из Акцента</span> возвращает статус по данному заказу',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/getStatusOrder/9959974',
		'description'	=> 	'<p class="ml-4">Первый параметр - Id заказа в Акценте</p>',
        'response'		=> '{
	    "Id": 9959974,
	    "Status": "в работе"
	}',
	),
	12 => array(
		'title'			=> 'API возвращает <span class="highlight-text">статусы по заказам</span>',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/getStatusOrders/1',
		'description'	=> 	'<p class="ml-4">Первый параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
							<p class="ml-4">Страницы разбиты на порции по 100 элементов.</p>',
        'response'		=> '{
	        "Data": {
		        "Orders": [
		            {
		                "Id": 9996293,
		                "Status": "не проведен"
		            },
		            {
		                "Id": 9996203,
		                "Status": "в работе"
		            }
				],
				"Count": 8145
			}
		}
		«Count» – общее количество элементов.',
	),
	13 => array(
		'title'			=> 'API возвращает <span class="highlight-text">данные по клиенту</span> и список его пильных/не пильных заказов',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/getManufacturOrdersFromClient/56000/true/1',
		'description'	=> 	'<p class="ml-4">Первый параметр - Id клиента из Акцента.</p>
							<p class="ml-4">Второй параметр - «true» - пильные заказа, «false» - не пильные заказы.</p>
							<p class="ml-4">Третий параметр - Номер страницы, если не указывать, по умолчанию первая будет.</p>
							<p class="ml-4">Страницы разбиты на порции по 100 элементов.</p>',
        'response'		=> '{
	    "ClientId": 56000,
	    "ClientName": "Прометей МПТ",
	    "ClientPhone": "+38(050)332-26-43",
	    "ClientEmail": "5",
	    "Data": {
	        "Orders": [
	            {
	                "Id": 9916872,
	                "No": "к1854/11",
	                "Date": "2020-11-18",
	                "AuthorId": 64707,
	                "Status": "выполен",
	                "ProjectId": 248073,
	                "Place": "1"
	            },
	            {
	                "Id": 9880448,
	                "No": "к8606/10",
	                "Date": "2020-11-02",
	                "AuthorId": 168065,
	                "Status": "выполен",
	                "ProjectId": 238645,
	                "Place": "1"
	            }
	        ],
	        "Count": 4
		    }
		}',
	),
	14 => array(
		'title'			=> 'API возвращает <span class="highlight-text">статусы по заказу</span>',
		'method'		=> 'GET',
		'link'			=> $kronas_api_link . '/gibLabService/getInfoOrder/9977116',
		'description'	=> 	'<p class="ml-4">Первый параметр - Id заказа в Акценте.</p>',
        'response'		=> '[
	    {
	        "MateralId": 452,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 0,
	        "Count": 1.0000,
	        "Price": 735.7500
	    },
	    {
	        "MateralId": 563,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 0,
	        "Count": 1.0000,
	        "Price": 896.4000
	    },
	    {
	        "MateralId": 0,
	        "BandId": 29328,
	        "SimpleId": 0,
	        "ServiceId": 0,
	        "Count": 30.0000,
	        "Price": 2.9076
	    },
	    {
	        "MateralId": 0,
	        "BandId": 29340,
	        "SimpleId": 0,
	        "ServiceId": 0,
	        "Count": 22.0000,
	        "Price": 7.5364
	    },
	    {
	        "MateralId": 0,
	        "BandId": 29346,
	        "SimpleId": 0,
	        "ServiceId": 0,
	        "Count": 13.0000,
	        "Price": 21.1065
	    },
	    {
	        "MateralId": 0,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 16301,
	        "Count": 38.9600,
	        "Price": 12.6500
	    },
	    {
	        "MateralId": 0,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 16305,
	        "Count": 9.9300,
	        "Price": 15.9500
	    },
	    {
	        "MateralId": 0,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 16419,
	        "Count": 60.3500,
	        "Price": 6.6000
	    },
	    {
	        "MateralId": 0,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 77350,
	        "Count": 4.0000,
	        "Price": 60.0000
	    },
	    {
	        "MateralId": 0,
	        "BandId": 0,
	        "SimpleId": 0,
	        "ServiceId": 86121,
	        "Count": 10.9800,
	        "Price": 7.6000
	    }
	]',
	),
	15 => array(
		'title'			=> 'API <span class="highlight-text">по коду 1С</span> возвращает цены на продукты, соответсвующие данному участку.
							(откуда собираются коды и коды чего именно (кромка, детали, материалы, услуги) не разобрался)',
		'method'		=> 'POST',
		'link'			=> $kronas_api_link . '/gibLabService/getPrices/164180/1',
		'description'	=> 	'<p class="ml-4">Первый параметр - код продукта (1С).</p>
         					<p class="ml-4">Второй параметр - номер участка.</p>
         					<p class="ml-4">Тело запроса:</p> [{"code1c":"20927"},{"code1c":"23907"},{"code1c":"50101"},{"code1c":"103433"},{"code1c":"32187"},{"code1c":"32186"}]',
        'response'		=> '[
	    {
	        "Art": "20927",
	        "Price": 104.9721,
	        "Discount": 0.0000
	    },
	    {
	        "Art": "23907",
	        "Price": 10.5988,
	        "Discount": 0.0000
	    },
	    {
	        "Art": "32186",
	        "Price": 5.6900,
	        "Discount": 0.0000
	    },
	    {
	        "Art": "32187",
	        "Price": 14.1756,
	        "Discount": null
	    },
	    {
	        "Art": "50101",
	        "Price": 41.0000,
	        "Discount": 0.0000
	    },
	    {
	        "Art": "103433",
	        "Price": 8.5091,
	        "Discount": null
	    }
	]',
	),
	16 => array(
		'title'			=> 'API <span class="highlight-text">по коду 1С</span> возвращает остатки материалов, соответсвующие данному участку.
							(откуда собираются коды не разобрался)',
		'method'		=> 'POST',
		'link'			=> $kronas_api_link . '/gibLabService/getRests/1',
		'description'	=> 	'<p class="ml-4">Первый параметр - номер участка.</p>
         					<p class="ml-4">Тело запроса:</p> [{"code1c":"1354"},{"code1c":"28702"},{"code1c":"20927"},{"code1c":"20927"},{"code1c":"23907"},{"code1c":"23907"},{"code1c":"50101"},{"code1c":"50101"}]',
        'response'		=> '[
	    {
	        "Art": "1354",
	        "Count": 0.0000
	    },
	    {
	        "Art": "20927",
	        "Count": 0.0000
	    },
	    {
	        "Art": "23907",
	        "Count": 1476.0000
	    },
	    {
	        "Art": "28702",
	        "Count": 1.5000
	    },
	    {
	        "Art": "50101",
	        "Count": 296.0000
	    }
	]',
	),
);

?>