<div class="container create_manager_container">
    <div class="card">
        <article class="card-body">
            <h4 class="card-title text-center mt-1">Создать менеджера</h4>
            <h6 class="red text-center">Все поля обязательны!</h6>
            <hr>
            <form class="createManager">
                <input type="hidden" name="requestPath" value="<?= $page[$currentPath]['webPath'] ?>">
                <div id="login-user">
                    <div class="form-group">
                        <div class="input-group">
                            <input name="name" class="form-control" placeholder="Ф.И.О." type="text" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="tel" id="managerPhone" class="form-control" placeholder="Номер телефона" type="text" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="email" class="form-control" placeholder="E-mail" type="email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="accentId" class="form-control" placeholder="ID Акцента" type="text" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="place" class="form-control" placeholder="Участок(номер)" type="number" min="1" max="9" id="mngPlace" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input name="password" class="form-control" placeholder="Пароль" type="text" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="create-manager" type="submit" class="btn btn-primary btn-block">Создать</button>
                    </div>
                </div>
            </form>
        </article>
    </div>
    <div class="messages"></div>
</div>