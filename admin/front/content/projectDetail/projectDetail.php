<?php

require_once DIR_CORE . 'func.php';
require_once DIR_CORE . 'func_mini.php';
require_once ADMIN_FUNCTIONS . 'fs_debug.php';

$order = null;
$textarea = null;

if (isset($_GET['project']) && !empty($_GET['project'])) {
	if ($project_data = get_Project_file($_GET['project'])) {

		$order = $_GET['project'];
		$textarea = (str_replace('<', "\r\n\r\n<", $project_data['project_data']));

		$project_data = xml_parser($project_data['project_data']);
		file_put_contents('c:/Users/among/Downloads/debug.txt', $project_data, FILE_APPEND);
		$p = xml_parser_create();
		xml_parse_into_struct($p, $project_data, $vals, $index);
		xml_parser_free($p);
		$vals = vals_out($vals);
		$index = make_index($vals);

		$PROJECT = [];
		$GOOD = [];
		$PART = [];
		$OPERATION = [];
		$MATERIAL = [];
		$PROGRAMS = [];

		$PROJECT_count = count($index['PROJECT']);
		$GOOD_count = count($index['GOOD']);
		$PART_count = count($index['PART']);
		$OPERATION_count = count($index['OPERATION']);
		$MATERIAL_count = count($index['MATERIAL']);

		$ulProject = valsToUl($vals);

		$programs_count = 0;
		foreach ($index['OPERATION'] as $k => $v) {
			if (isset($vals[$v]['attributes']['PROGRAM'])) {
				$programs_count++;
				$pp = xml_parser_create();
				xml_parse_into_struct($pp, $vals[$v]['attributes']['PROGRAM'], $p_vals, $p_index);
				xml_parser_free($pp);
				$PROGRAMS['Program_' . $programs_count . ' id_' . $vals[$v]['attributes']['ID']] = $p_vals;
				$tmpUl = valsToUl($p_vals);
				$tmpUl['OPERATION_ID'] = $vals[$v]['attributes']['ID'];
				move_to_top($tmpUl, 'OPERATION_ID');
				$ulProject['PROGRAMS'][$vals[$v]['attributes']['ID']] = $tmpUl;
			}
		}

	} else echo '<p class="red">Проект не найден.</p>';
} else {
	$PROJECT = [];
	$GOOD = [];
	$PART = [];
	$OPERATION = [];
	$MATERIAL = [];
	$PROGRAMS = [];

	$PROJECT_count = 0;
	$GOOD_count = 0;
	$PART_count = 0;
	$OPERATION_count = 0;
	$programs_count = 0;
	$MATERIAL_count = 0;
}

function move_to_top(&$array, $key) {
    $temp = array($key => $array[$key]);
    unset($array[$key]);
    $array = $temp + $array;
}

?>

<div class="container-fluid detail-content">

	<form action="<?= $currentPath ?>">
		<div class="row g-3 align-items-center">
			<div class="col-auto">
				<input type="text"  class="form-control" name="project" value="<?= $order ?>" placeholder="ID проекта">
			</div>
			<div class="col-auto">
				<input type="submit"  class="btn btn-primary" value="Загрузить">
			</div>
		</div>
	</form>

	<?php if (null !== $textarea): ?>
		<textarea rows="10" cols="45" style="width:100%;min-height:30%;"><?= $textarea ?></textarea>
	<?php endif ?>

	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item" role="presentation">
			<button class="nav-link active" id="count-tab" data-bs-toggle="tab" data-bs-target="#count" type="button" role="tab" aria-controls="count" aria-selected="true">Количество</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="project-tab" data-bs-toggle="tab" data-bs-target="#project" type="button" role="tab" aria-controls="project" aria-selected="false">PROJECT</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="good-tab" data-bs-toggle="tab" data-bs-target="#good" type="button" role="tab" aria-controls="good" aria-selected="false">GOOD</button>
		</li>

		<li class="nav-item" role="presentation">
			<button class="nav-link" id="part-tab" data-bs-toggle="tab" data-bs-target="#part" type="button" role="tab" aria-controls="part" aria-selected="false">PART</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="operation-tab" data-bs-toggle="tab" data-bs-target="#operation" type="button" role="tab" aria-controls="operation" aria-selected="false">OPERATION</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="programms-tab" data-bs-toggle="tab" data-bs-target="#programms" type="button" role="tab" aria-controls="programms" aria-selected="false">Programs</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="material-tab" data-bs-toggle="tab" data-bs-target="#material" type="button" role="tab" aria-controls="material" aria-selected="false">MATERIAL</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="progsArr-tab" data-bs-toggle="tab" data-bs-target="#progsArr" type="button" role="tab" aria-controls="progsArr" aria-selected="false">Массив программ</button>
		</li>
		<li class="nav-item" role="presentation">
			<button class="nav-link" id="vals-tab" data-bs-toggle="tab" data-bs-target="#vals" type="button" role="tab" aria-controls="vals" aria-selected="false">Весь массив проекта</button>
		</li>
	</ul>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="count" role="tabpanel" aria-labelledby="count-tab">
			<p>PROJECTS - <?= $PROJECT_count ?></p>
			<p>GOODS - <?= $GOOD_count ?></p>
			<p>PARTS - <?= $PART_count ?></p>
			<p>OPERATIONS - <?= $OPERATION_count ?></p>
			<p>PROGRAMS - <?= $programs_count ?></p>
			<p>MATERIALS - <?= $MATERIAL_count ?></p>
		</div>
		<div class="tab-pane fade" id="project" role="tabpanel" aria-labelledby="project-tab"><?= $ulProject['PROJECT'] ?></div>
		<div class="tab-pane fade" id="good" role="tabpanel" aria-labelledby="good-tab"><?= $ulProject['GOOD'] ?></div>
		<div class="tab-pane fade" id="part" role="tabpanel" aria-labelledby="part-tab"><?= $ulProject['PART'] ?></div>
		<div class="tab-pane fade" id="operation" role="tabpanel" aria-labelledby="operation-tab"><?= $ulProject['OPERATION'] ?></div>
		<div class="tab-pane fade" id="programms" role="tabpanel" aria-labelledby="programms-tab"><?= p_($ulProject['PROGRAMS']) ?></div>
		<div class="tab-pane fade" id="material" role="tabpanel" aria-labelledby="material-tab"><?= $ulProject['MATERIAL'] ?></div>
		<div class="tab-pane fade" id="progsArr" role="tabpanel" aria-labelledby="progsArr-tab"><?= p_($PROGRAMS) ?></div>
		<div class="tab-pane fade" id="vals" role="tabpanel" aria-labelledby="vals-tab"><?= p_($vals) ?></div>
	</div>

</div>