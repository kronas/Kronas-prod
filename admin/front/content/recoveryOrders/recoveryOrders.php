<?php
$minDate = '2020-01-22';
?>
<div id="recoveryOrders" class="recoveryOrders">
    <h6>Выбериите дату, с которой хотите начать восстановление заказов из резервного хранилища</h6>
    <input type="hidden" name="requestPath" value="<?= $page[$currentPath]['webPath'] ?>">
    <input type="date" name="recoveryDate" value="<?= date('Y-m-d') ?>" min="<?= $minDate ?>" max="<?= date('Y-m-d') ?>">
    <button name="recoveryOrders" class="btn btn-primary" title="Восстановить заказы из резервного хранилища">Восстановить</button>
    <div class="messages"></div>
</div>