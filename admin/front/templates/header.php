<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?= ADMIN_BOOTSTRAP_CSS . '/bootstrap.min.css' ?>">
	<link rel="stylesheet" href="<?= ADMIN_FONTAWESOME_CSS . '/all.min.css' ?>">
	<link rel="stylesheet" href="<?= ADMIN_LIBS . '/css/jquery-ui.min.css' ?>">
	<link rel="stylesheet" href="<?= ADMIN_LIBS . '/css/navbar.css' ?>">
	<link rel="stylesheet" href="<?= ADMIN_LIBS . '/css/main.css' ?>">
	
	<?php if (isset($page[$currentPath]['styles'])): ?>
		<link rel="stylesheet" href="<?= $page[$currentPath]['styles'] ?>">
	<?php endif ?>

	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<div class="content">
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
				<div class="container-fluid">
					<div id="navbarSupportedContent">
						<ul class="navbar-nav me-auto mb-2 mb-lg-0">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
									<i class="fa-solid fa-bars"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item" href="<?= HOST_NAME ?>/manager_all_order.php"><i class="fa-solid fa-house-user"></i> К заказам</a></li>
									<li><a class="dropdown-item" href="client-create">Создать клиента</a></li>
									<li><a class="dropdown-item" href="manager-create">Создать менеджера</a></li>

									<?php if ($_SESSION['user']['access'] >= 100): ?>
										<li><a class="dropdown-item" href="api-help">Справка по API</a></li>
										<li><a class="dropdown-item" href="unblock-project">Разблокировать проект</a></li>
										<li><a class="dropdown-item" href="project-detail">Деталировки проектов</a></li>
										<li><a class="dropdown-item red client-delete" href="client-delete">Удалить контрагента</a></li>
										<li><a class="dropdown-item" href="recovery-orders">Восстановить заказы после сбоя</a></li>
									<?php endif ?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div class="main-content">