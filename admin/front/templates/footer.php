				<footer class="footer bg-light text-center text-lg-start">
				  <div class="footerline text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
				    <a class="footerdomain" href="<?= HOST_NAME ?>"><?= DOMAIN_NAME ?></a>
				  </div>
				</footer>
			</div>
			<!-- .main-content close -->
		</div>
		<!-- .content close -->
	</div>
	<!-- .wrapper close -->

<script src="<?= ADMIN_LIBS ?>/js/jquery.min.js"></script>
<script src="<?= ADMIN_LIBS ?>/js/jquery-ui.min.js"></script>
<script src="<?= ADMIN_BOOTSTRAP_JS ?>/bootstrap.bundle.min.js"></script>
<script src="<?= ADMIN_LIBS ?>/js/jquery.mask.min.js"></script>

<?php if (isset($page[$currentPath]['js'])): ?>
	
	<script src="<?= $page[$currentPath]['js'] ?>"></script>

<?php endif ?>

</body>
</html>