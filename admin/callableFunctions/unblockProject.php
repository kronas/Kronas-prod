<?php
// Подключаем функции по работе с БД
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');
$q = '';
$data['message'] = '';
$data['status'] = 0;


if (13 < iconv_strlen($incomingData)) {
	$data['message'] = 'Слишком много символов в строке.';
} elseif (5 > iconv_strlen($incomingData)) {
	$data['message'] = 'Слишком мало символов в строке.';
} else {
	if (stripos($incomingData, '/')) $q = 'SELECT * FROM `ORDER1C` WHERE `DB_AC_NUM` = "' . $incomingData . '"';
	elseif (preg_match('#^[\d]+$#', $incomingData)) $q = 'SELECT * FROM `ORDER1C` WHERE `DB_AC_ID` = "' . $incomingData . '"';
	else $data['message'] = 'Строка не похожа на код или ID проекта';

	if ($data['message'] == '' && $q != '') {
		if ($tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q)) {
			if ($tmp['res'] == 1) {
				$tmp = $tmp['data'];
				foreach ($tmp as $value) {
					if ($value['readonly'] == 1) {
						$q = 'UPDATE `ORDER1C` SET `readonly`= 0 WHERE `DB_AC_ID` = "' . $value['DB_AC_ID'] . '"';
						if ($tmp2 = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q)) {
							if ($tmp2['res'] == 1) {
								$data['status'] = 1;
								$data['message'] = 'Проект доступен для редактирования';
							} else $data['message'] = 'Не удалось изменить значение поля БД';
						} else {
							$data['status'] = 1;
							$data['message'] = 'Ошибка в запросе обновления поля, обратитесь к администратору';
						}
					} else {
						$data['status'] = 1;
						$data['message'] = 'Проект уже был доступен для редактирования';
					}
				}
				// da($tmp);
			} else {
				$data['message'] = 'Проект не найден';
			}
		} else {
			$data['message'] = 'Ошибка в запросе выборки, обратитесь к администратору';
		}
	}
	
}
$data = json_encode($data);

?>