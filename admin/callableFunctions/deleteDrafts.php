<?php

// Подключаем функции по работе с БД
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');

// Сообщение по умолчанию, если в $incomingData придут данные то сообщение изменится
$data['message'] = 'Помилка відправки даних.';
$data['status'] = 0;

if (isset($incomingData["deleteDrafts"]) && !empty($incomingData["deleteDrafts"])) {

	$data['message'] = 'Всі черенетки видалено.';
	$data['status'] = 1;

	$minusMonthDate = date('Y-m-d H:i:s', strtotime('-1 month'));
	$sql = 'SELECT * FROM `ORDER1C` WHERE `DB_AC_IN` <= "' . $minusMonthDate . '" AND `status` = "черновик" AND `del_order` = 1';
	$deletedOrders = getTableData($sql);

	if (count($deletedOrders) > 0) {
		
		$i = 0;
		foreach ($deletedOrders as $value) {
			$delStatus = deleteDraft($value['ID']);

			if (false === $delStatus) {
				$i++;
				$data['message'] = 'Одна чи декілька черенеток не видалено! Кількість невидалених: ' . $i;
				$data['status'] = 0;
			}
		}

	}
}

$data = json_encode($data);


function deleteDraft($orderId) {
	$del_sql = 'DELETE FROM `ORDER1C` WHERE `ID` = "' . $orderId . '"';
    $select_all_project = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = "' . $orderId . '"';
    $del_sql_project = 'DELETE FROM `PROJECT` WHERE `ORDER1C` = "' . $orderId . '"';
    $del_out = executeQuery($del_sql);
    if ($del_out !== true && false !== stripos($del_out, 'a foreign key constraint fails')) {
        $order_check = getTableData('SELECT * FROM `ticket` WHERE `order_id` = "' . $orderId . '" LIMIT 1');
        if (isset($order_check[0]['id'])) {
            $mpr_id = getTableData('SELECT * FROM `ticket_construct_mpr` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
            if (isset($mpr_id[0]['mpr_id'])) {
                foreach ($mpr_id as $value) {
                    $d1 = executeQuery('DELETE FROM `ticket_mpr_price` WHERE `mpr_id` = "' . $value['mpr_id'] . '"');
                }
            }
            $d2 = executeQuery('DELETE FROM `ticket_construct_mpr` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
            $d3 = executeQuery('DELETE FROM `ticket_message` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
            $d4 = executeQuery('DELETE FROM `ticket_assets` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
            $d5 = executeQuery('DELETE FROM `ticket` WHERE `id` = "' . $order_check[0]['id'] . '"');
            $del_out = executeQuery($del_sql);
        }
    }
    if ($del_out === true) {
        if ($all_project = getTableData($select_all_project)) {
            foreach ($all_project as $key => $value) {
                $id = $value['PROJECT_ID'];
                $project_storage = $config['storage_dir'].'/';
                $ps_inner_folder = $project_storage.substr($id, 0, 2).'/';
                if (file_exists($project_storage.$id.'.txt')) unlink($project_storage.$id.'.txt');
                elseif (file_exists($ps_inner_folder.$id.'.txt')) unlink($ps_inner_folder.$id.'.txt');
            }
        }
        executeQuery($del_sql_project);
    }

    $order_check = getTableData($sql);
    if(!empty($order_check[0])) return false;
    else return true;
}

?>