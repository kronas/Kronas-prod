<?php
// Подключаем функции по работе с БД
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');
$tmp = null;

// Обработка данных из формы
if (isset($incomingData["managerData"])) {
	$incomingData = $incomingData["managerData"];
	$name = $incomingData['name'];
	$pNum = preg_replace('#[\s\t\(\)-]#', '', $incomingData['phone']);
	$email = $incomingData['email'];
	$accId = $incomingData['accentId'];
	$place = $incomingData['place'];
	$pass = $incomingData['passw'];

	$data['status'] = 0;
	if (empty($name)) {
		$data['message'] = 'Заполните поле <b>Ф.И.О.</b>';
	} elseif (empty($pNum)) {
		$data['message'] = 'Заполните поле <b>Номер телефона</b>';
	} elseif (empty($email)) {
		$data['message'] = 'Заполните поле <b>E-mail</b>';
	} elseif (empty($accId)) {
		$data['message'] = 'Заполните поле <b>ID Акцента</b>';
	} elseif (empty($place)) {
		$data['message'] = 'Заполните поле <b>Участок(филиал)</b>';
	} elseif (empty($pass)) {
		$data['message'] = 'Заполните поле <b>Пароль</b>';
	} elseif (!empty($incomingData['name']) && !empty($incomingData['phone']) &&
			  !empty($incomingData['email']) && !empty($incomingData['accentId']) &&
			  !empty($incomingData['place']) && !empty($incomingData['passw'])) {
		if (13 > iconv_strlen($pNum)) {
			$data['message'] = 'В номере телефона не хватает как минимум одной цифры';
		} elseif (13 < iconv_strlen($pNum)) {
			$data['message'] = 'В номере телефона слишком много цифр';
		} elseif (!preg_match('#^([\w\d.]+)(@)(\w{1,})(\.)(\w.{2,})$#', $email)) {
			$data['message'] = 'Не верный формат E-mail адреса';
		} elseif (!preg_match('#^(\d+)$#', $accId)) {
			$data['message'] = 'В поле "ID Акцента" допускаются только цифры';
		} elseif (!preg_match('#^(\d{1,2})$#', $place)) {
			$data['message'] = 'В поле "Участок" допускаются только цифры';
		} elseif ((!preg_match('#([A-Za-z]{1,})#', $pass)) || (!preg_match('#(\d{1,})#', $pass))
				   || (preg_match('#[\W]#', $pass)) || (6 > iconv_strlen($pass))) {
			$data['message'] = 'Пароль должен состоять как минимум из шести символов, и содержать латинские буквы и цифры';
		} else {
			$q = 'SELECT * FROM `manager` WHERE `code` = "' . $accId . '" LIMIT 1';
			if ($tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q)) {
				if ($tmp['res'] == 1) {
					$tmp = $tmp['data'][0];
					if (empty($tmp['phone'])) {
						$q = 'UPDATE `manager` SET `phone`= "' . $pNum . '" WHERE `code` = "' . $accId . '"';
						$in_tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q);
					}
					if (empty($tmp['e-mail'])) {
						$q = 'UPDATE `manager` SET `e-mail`= "' . $email . '" WHERE `code` = "' . $accId . '"';
						$in_tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q);
					}
					if (empty($tmp['password'])) {
						$q = 'UPDATE `manager` SET `password`= "' . $pass . '" WHERE `code` = "' . $accId . '"';
						$in_tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q);
					}
					$data['status'] = 1;
					$data['message'] = 'Менеджер уже был создан ранее, данные обновлены';
				} else {
					$q = "INSERT INTO `manager` (`code`, `name`, `phone`, `e-mail`, `place`, `password`) VALUES ('" . $accId . "', '" . $name . "', '" . $pNum . "', '" . $email . "', '" . $place . "', '" . $pass . "')";
					$in_tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q);
					if (!isset($in_tmp['res']) || (isset($in_tmp['res']) && $in_tmp['res'] == 0)) $data['message'] = 'Ошибка: менеджер не был создан, обратитесь к администратору';
					else {
						$data['status'] = 1;
						$data['message'] = 'Менеджер успешно создан';
					}
				}
			} else {
				$data['message'] = 'Ошибка в запросе, обратитесь к администратору';
			}
			
		}
	}
}
// Обработка автодополнения
if (isset($incomingData["managerPlace"])) {
	$incomingData = $incomingData["managerPlace"];
	$q = 'SELECT * FROM `PLACES` WHERE `PLACES_ID` = "' . $incomingData['request'] . '" LIMIT 1';
	if ($tmp = getSqlData(__LINE__, __FILE__, __FUNCTION__, $q)) {
		if ($tmp['res'] == 1) {
			$tmp = $tmp['data'][0];
			$data[$tmp['PLACES_ID']]['label'] = $tmp['NAME'];
			$data[$tmp['PLACES_ID']]['value'] = $tmp['PLACES_ID'];
		}
	}
}

$data = json_encode($data);

?>