<?php

/** 
 * Плитки айтемов для вывода на экран
 * Функция принимает массив (детали, операции, материала), ищет id, code или name,
 * строит плитку, собирает все плитки в див и возвращает HTML
 */
function getHtmlTile($tagArray) {
	$txt = false;
	if (!empty($tagArray) && isset($tagArray['attributes'])) {
        $txt = '<div class="deb_sticker">';
        if (isset($tagArray["attributes"]['ID'])) {
            $txt .= '<p>ID: ' . $tagArray["attributes"]['ID'] . '</p>';
        }
        if (isset($tagArray["attributes"]['CODE'])) {
            $txt .= '<p>CODE: ' . $tagArray["attributes"]['CODE'] . '</p>';
        }
        if (isset($tagArray["attributes"]['NAME'])) {
            $txt .= '<p>NAME: ' . $tagArray["attributes"]['NAME'] . '</p>';
        }
        if (!isset($tagArray["attributes"]['ID']) && !isset($tagArray["attributes"]['CODE']) && !isset($tagArray["attributes"]['NAME'])) {
            $txt .= '<p>NAME: Noname</p>';
        }
        $txt .= '</div>';
    }
    return $txt;
}

/** 
 * Функция строит удобные для чтения блоки (детали, операции и пр.) из project_data или массива vals
 */
function valsToUl($vals) {
	$tmp = '';
	$out = [];
	
	if (!is_array($vals)) {
		$p = xml_parser_create();
		xml_parse_into_struct($p, $vals, $v, $i);
		xml_parser_free($p);
		$vals = $v;
		$index = $i;
	} else $index = getIndexes($vals);

	foreach ($index as $key => $value) {
		$tmp = '';
		foreach ($index[$key] as $k => $val) {
			$tmp .= '<li class="debug-item tree-item"><ul class="debug-list tree-list debug-one-block">' . valToUl($vals[$val]) . '</ul></li>';
		}
		$tmp = '<ul class="debug-list tree-list"><li class="debug-item tree-item">' . $tmp . '</li></ul>';
		$out[$key] = $tmp;
	}
	return $out;
}

/** 
 * Функция строит удобные для чтения блоки (детали, операции и пр.) из одного массива vals[(operation, part, good и пр.)]
 * Принимает один массив например:
 * Array (
 *    [tag] => GOOD
 *    [type] => open
 *    [level] => 2
 *    [attributes] => 
 *    Array (
 *        [TYPEID] => product
 *        [COST] => 6208.41524
 *        [COSTMATERIAL] => 6143.04639
 *        [COSTOPERATION] => 65.36885
 *        [COUNT] => 1
 *        [ID] => 1
 *        [NAME] => №6042 Смела общая
 *        [MY_DB_ID] => 58776
 *        [GOOD.MY_DB_ID] => 58776
 *    )
 * )
 * Работает в паре с valsToHtml()
 */
function valToUl($val) {
	$out = '';
    $blockId = '';
    $blockCode = '';
    $blockName = '';
    $blockTypeId = '';

	if (isset($val['attributes']['NAME'])) $blockName = 'Name: ' . $val['attributes']['NAME'] . '; ';
	if (isset($val['attributes']['ID'])) $blockId = 'Id: ' . $val['attributes']['ID'] . '; ';
	if (isset($val['attributes']['CODE'])) $blockCode = 'Code: ' . $val['attributes']['CODE'] . '; ';
    if (isset($val['attributes']['TYPEID'])) $blockTypeId = 'TypeId: ' . $val['attributes']['TYPEID'] . '; ';
	if (!isset($val['attributes']['CODE']) && !isset($val['attributes']['NAME']) && !isset($val['attributes']['ID']) && !isset($val['attributes']['TYPEID'])) $blockCode = 'Noname';

	foreach ($val as $k => $v) {
		if (!is_array($v)) {
			if ($k == 'tag' && ($val['type'] == 'open' || $val['type'] == 'complete')) {
				$out .= '<li class="debug-item tree-item"><div><span class="left-exp">' . $k . '</span><span class="right-exp">' . $v . '</span>' . ' ' . $blockId . $blockCode . $blockName . $blockTypeId . '</div></li>';
			} else {
				$out .= '<li class="debug-item tree-item"><div><span class="left-exp">' . $k . '</span><span class="right-exp">' . $v . '</span>' . '</div></li>';
			}
		} else {
			$out .= '<li class="debug-item tree-item tree-inner-items"><div><span class="left-exp">' . $k . ' -></span></div><ul class="debug-list tree-list hidden">' . valToUl($v) . '</ul></li>';
		}
	}
	return $out;
}

/** 
 * Функция собирает и отдаёт массив индексов по типам (operation, part, good и пр.)
 */
function getIndexes($vals) {
    foreach ($vals as $k => $v) {
        $temp[] = $v['tag'];
    }
    $in_key = array_unique($temp);
    foreach ($in_key as $v) {
        foreach ($vals as $k1 => $v1) {
            if($v == $v1['tag']) $index[$v][] = $k1;
        }
    }
    return $index;
}

?>