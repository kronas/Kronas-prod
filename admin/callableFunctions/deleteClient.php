<?php

// Подключаем функции по работе с БД
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');

// Сообщение по умолчанию, если в $incomingData придут данные то сообщение изменится
$data['message'] = 'Ошибка отправленных данных.';
$data['status'] = 0;

if (isset($incomingData["deleteClient"]) && !empty($incomingData["deleteClient"])) {
    $sql = 'SELECT * FROM `client` WHERE `client_id` = "' . $incomingData["deleteClient"] . '" LIMIT 1';
    $client = getTableData($sql);
    if (isset($client[0])) {
        
        $data['status'] = 1;
        $client = $client[0];
        $sql = 'SELECT * FROM `ORDER1C` WHERE `CLIENT` = "' . $client['code'] . '"';
        $clientOrders = getTableData($sql);

        if (!empty($clientOrders)) {
            foreach ($clientOrders as $key => $value) {
                if (false === deleteOrder($value['ID'])) {
                    $data['status'] = 0;
                    $data['message'] = 'Не удалось удалить заказ клиента c ID - ' . $value['ID'] . '.';
                    break;
                }
            }
        }

        if ($data['status'] === 1) {
            $del_sql = 'DELETE FROM `client` WHERE `client_id` = "' . $incomingData["deleteClient"] . '"';
            $del_out = del_data($del_sql);
            if ($del_out === true) {
                $data['message'] = 'Клиент ' . $client['name'] . ' удалён.';
            } else {
                $data['status'] = 0;
                $data['message'] = 'Не удалось удалить клиента ' . $client['name'] . '.';
            }
        }

    } else $data['message'] = 'Клиент с ID ' . $incomingData["deleteClient"] . ' не найден';
}

$data = json_encode($data);

function deleteOrder($orderId) {
    $sql = 'SELECT * FROM `ORDER1C` WHERE `ID` = "' . $orderId . '" LIMIT 1';
    $order_check = getTableData($sql);
    if(!empty($order_check[0])) {
        $del_sql = 'DELETE FROM `ORDER1C` WHERE `ID` = "' . $orderId . '"';
        $select_all_project = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = "' . $orderId . '"';
        $del_sql_project = 'DELETE FROM `PROJECT` WHERE `ORDER1C` = "' . $orderId . '"';
        $del_out = del_data($del_sql);
        if ($del_out !== true && strripos($del_out, 'a foreign key constraint fails')) {
            $order_check = getTableData('SELECT * FROM `ticket` WHERE `order_id` = "' . $orderId . '" LIMIT 1');
            if (isset($order_check[0]['id'])) {
                $mpr_id = getTableData('SELECT * FROM `ticket_construct_mpr` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
                if (isset($mpr_id[0]['mpr_id'])) {
                    foreach ($mpr_id as $value) {
                        $d1 = del_data('DELETE FROM `ticket_mpr_price` WHERE `mpr_id` = "' . $value['mpr_id'] . '"');
                    }
                }
                $d2 = del_data('DELETE FROM `ticket_construct_mpr` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
                $d3 = del_data('DELETE FROM `ticket_message` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
                $d4 = del_data('DELETE FROM `ticket_assets` WHERE `ticket_id` = "' . $order_check[0]['id'] . '"');
                $d5 = del_data('DELETE FROM `ticket` WHERE `id` = "' . $order_check[0]['id'] . '"');
                $del_out = del_data($del_sql);
            }
        }
        if ($del_out === true) {
            if ($all_project = getTableData($select_all_project)) {
                foreach ($all_project as $key => $value) {
                    $id = $value['PROJECT_ID'];
                    $project_storage = $config['storage_dir'].'/';
                    $ps_inner_folder = $project_storage.substr($id, 0, 2).'/';
                    if (file_exists($project_storage.$id.'.txt')) unlink($project_storage.$id.'.txt');
                    elseif (file_exists($ps_inner_folder.$id.'.txt')) unlink($ps_inner_folder.$id.'.txt');
                }
            }
            del_data($del_sql_project);
        }
    }
    $order_check = getTableData($sql);
    if(!empty($order_check[0])) return false;
    else return true;
}

?>