<?php

// Подключаем функции по работе с БД
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');
include_once(DIR_FUNCTIONS . 'fs_fsystem.php');

// Сообщение по умолчанию, если в $incomingData придут данные то сообщение изменится
$data['message'] = '<b>Ошибка:</b> Не верный формат данных.';
$data['status'] = 0;

$orderPath = DIR_FILES . 'ps_share/orders_backup/'; // Сохранённые записи БД заказов
$projectPath = DIR_FILES . 'ps_share/projects_backup/'; // Сохранённые записи БД проектов
$recoveryProjectFilesPath = DIR_FILES . 'ps_share/project_out/'; // Сохранённые файлы проектов
$projectFilesPath = DIR_FILES . 'project_out/'; // Папка для файлов проектов
$pathsEnd = [];
$countOrders = 0;
$countProjects = 0;

if (1 === preg_match('#^\d{4}-\d{2}-\d{2}$#', $incomingData)) {
    
    // Собираем даты от указаной, форматируем в пути и сразу проверяем, есть ли такая папка
    // и если есть, записываем в массив
    $tmpDate = $incomingData;
    while (date("Y-m-d", strtotime($thisDay . " +1 day")) >= date("Y-m-d", strtotime($tmpDate))) {
        $tmpPath = str_replace('-', '/', $tmpDate);
        if (file_exists($orderPath . $tmpPath)){
            if (!empty(GetFileNames($orderPath . $tmpPath))) {
                $pathsEnd[] = $tmpPath;
            }
        }
        $tmpDate = date("Y-m-d", strtotime($tmpDate . " +1 days"));
    }

    // Здесь проходимся по массиву и проверяем нет ли в БД такой записи, если нет, то записываем.
    // Новая запись пишется с новым идентификатором,
    // поэтому в проектах, принадлежащих этому заказу, которые будут восстанавливаться тоже меняем идентификатор заказа на новый
    foreach ($pathsEnd as $pathEnd) {
        $recoveryOrders = GetFileNames($orderPath . $pathEnd);
        foreach ($recoveryOrders as $oneOrder) {
            
            $recOrder = (array)json_decode(file_get_contents($orderPath . $pathEnd . '/' . $oneOrder));
            if (!empty($recOrder)) {
                $recProjects = $recOrder['projects'];
                $recOrderId = $recOrder['ID'];
                
                if (!empty($recOrder['DB_AC_ID'])) $q = "SELECT * FROM `ORDER1C` WHERE `DB_AC_ID` = '" . $recOrder['DB_AC_ID'] . "' LIMIT 1";
                else $q = "SELECT * FROM `ORDER1C` WHERE `ID` = '" . $recOrder['ID'] . "' AND `DB_AC_ID` IS NULL LIMIT 1";
                $q = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);

                if ($q['res'] === 0) {
                    unset($recOrder['ID'], $recOrder['projects']);
                    $recOrder = array_diff($recOrder, array(''));
                    $recOrderKeys = "`" . implode("`, `", array_keys($recOrder)) . "`";
                    $recOrderVals = "'" . implode("', '", array_values($recOrder)) . "'";
                    $q = "INSERT INTO `ORDER1C`($recOrderKeys) VALUES ($recOrderVals)";
                    $q = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
                    $recOrderNewId = $q['id'];

                    foreach ($recProjects as $recProjectId) {
                        $recProject = getRecoveryProject($recProjectId, $projectPath);
                        if ($recProject !== false) {
                            $recProject = array_diff((array)json_decode($recProject), array(''));
                            $q = "SELECT * FROM `PROJECT` WHERE `PROJECT_ID` = '" . $recProject['PROJECT_ID'] . "' AND `ORDER1C` = '" . $recProject['ORDER1C'] . "'";
                            $q = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);
                            
                            // Записываем данные проекта в БД
                            if ($q['res'] === 0) {
                                unset($recProject['PROJECT_ID']);
                                $recProject['ORDER1C'] = $recOrderNewId;
                                $recProjectKeys = "`" . implode("`, `", array_keys($recProject)) . "`";
                                $recProjectVals = "'" . implode("', '", array_values($recProject)) . "'";
                                $q = "INSERT INTO `PROJECT`($recProjectKeys) VALUES ($recProjectVals)";
                                $q = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
                                $recProjectNewId = $q['id'];
                            } else {
                                $q = "UPDATE `PROJECT` SET `ORDER1C` = '" . $recOrderNewId . "' WHERE `ORDER1C` = '" . $recProject['ORDER1C'] . "'";
                                $q = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
                                $recProject['ORDER1C'] = $recOrderNewId;
                                $recProjectNewId = $recProjectId; // эта строчка только для записи на диск, тут идентификатор остаётся старый, просто переменная используется ниже
                            }

                            // Папки хранения проектов внутри папки project_out/
                            $dirdigOldId = ceil($recProjectId / 1000) - 1;
                            $dirdigNewId = ceil($recProjectNewId / 1000) - 1;
                            if (!file_exists($projectFilesPath . $dirdigNewId)) mkdir($order_backup_path . $dirdigNewId, 0777, true);
                            if (!file_exists($recoveryProjectFilesPath . $dirdigNewId)) mkdir($recoveryProjectFilesPath . $dirdigNewId, 0777, true);

                            // Записываем файлы проекта на диск (files/project_out/)
                            if (file_exists($projectFilesPath . $dirdigOldId . '/' . $recProjectId . '.txt')) unlink($projectFilesPath . $dirdigOldId . '/' . $recProjectId . '.txt');
                            if (file_exists($recoveryProjectFilesPath . $dirdigOldId . '/' . $recProjectId . '.txt')) {
                                rename($recoveryProjectFilesPath . $dirdigOldId . '/' . $recProjectId . '.txt', $recoveryProjectFilesPath . $dirdigNewId . '/' . $recProjectNewId . '.txt');
                                copy($recoveryProjectFilesPath . $dirdigNewId . '/' . $recProjectNewId . '.txt', $projectFilesPath . $dirdigNewId . '/' . $recProjectNewId . '.txt');
                                if ($dirdigNewId > $dirdigOldId) unlink($recoveryProjectFilesPath . $dirdigOldId . '/' . $recProjectId . '.txt');
                            }

                            // И перезаписываем файл данных проекта (запись БД), потому что идентификаторы проекта и заказа изменились
                            rename($projectPath . $dirdigOldId . '/' . $recProjectId . '.json', $projectPath . $dirdigNewId . '/' . $recProjectNewId . '.json');
                            if ($dirdigNewId > $dirdigOldId) unlink($projectPath . $dirdigOldId . '/' . $recProjectId . '.json');
                            $recProject['PROJECT_ID'] = $recProjectNewId;
                            overwritingRecoveryProject($recProject['PROJECT_ID'], $recProject, $projectPath);
                        }
                        $countProjects++;
                    }

                    // И перезаписываем файл данных заказа (запись БД), потому что идентификаторы проекта и заказа изменились
                    rename($orderPath . $pathEnd . '/' . $oneOrder, $orderPath . $pathEnd . '/' . $recOrderNewId . '.json');
                    $recOrder['ID'] = $recOrderNewId;
                    $recOrder['projects'] = [];
                    $q = "SELECT `PROJECT_ID` FROM `PROJECT` WHERE `ORDER1C` = '" . $recProject['ORDER1C'] . "'";
                    $q = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);
                    if ($q['res'] == 1 && !empty($q['data'])) {
                        foreach ($q['data'] as $projectId) array_push($recOrder['projects'], (int)$projectId['PROJECT_ID']);
                    }
                    file_put_contents($orderPath . $pathEnd . '/' . $recOrder['ID'] . '.json', json_encode($recOrder));

                    $countOrders++;
                }
            }
        }
    }

    $data['status'] = 1;
    $data['message'] = "Восстановлено заказов: $countOrders, проектов: $countProjects";
    $data = json_encode($data);
}


/**
* Функция читает файл проекта с диска 'files/project_out/номер_подпапки'
**/
function getRecoveryProject($id, $projectPath) {
    $dirdig = ceil($id / 1000) - 1;
    $projectFile = $projectPath . $dirdig . '/' . abs($id) . ".json";

    if (file_exists($projectFile)) return file_get_contents($projectFile);
    else return false;
}

/**
* Функция записывает в файл данные (для БД) проекта
**/
function overwritingRecoveryProject($id, $databaseData, $projectPath) {
    $dirdig = ceil($id / 1000) - 1;
    file_put_contents($projectPath . $dirdig . '/' . abs($id) . '.json', json_encode($databaseData));
    return true;
}

?>