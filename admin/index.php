<?php

require_once 'config.php';

//.. Если не админ - на страницу авторизации (и если авторизирован, в кабинет менеджера).
if(!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
    header('Location: '.$main_dir.'/login.php');
}

// Обработка регистрации клиента
if(isset($_GET['check_client_phone']) && strlen($_GET['check_client_phone']) == 17) {
    require_once ADMIN_CONTENT . '/' . $_GET['to'] . '/' . $_GET['to'] . '.php';
    exit;
}

// Обработка ajax-запросов
if (isset($_POST["managerData"])
    || isset($_POST["managerPlace"])
    || isset($_POST["unblockProject"])
    || isset($_POST["recoveryOrders"])
    || isset($_POST["deleteDrafts"])
    || isset($_POST["deleteClient"])) {
    require_once 'ajax.php';
    exit;
}


require_once 'page.php';

?>