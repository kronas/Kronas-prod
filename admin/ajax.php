<?php

// ###########################################################################################
// Создание нового менеджера
if (isset($_POST["managerData"]) || isset($_POST["managerPlace"])) {
    // de($_POST);
    $data = [];
    
    if (isset($_POST["managerData"])) $incomingData["managerData"] = $_POST["managerData"];
    if (isset($_POST["managerPlace"])) $incomingData["managerPlace"] = $_POST["managerPlace"];

    require_once ADMIN_FUNCTIONS . 'createManager.php';
}
// ###########################################################################################

// ###########################################################################################
// Разблокировка проекта, отправленного в производство
if (isset($_POST["unblockProject"])) {
    $data = [];
    $incomingData = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["unblockProject"]))));
    require_once ADMIN_FUNCTIONS . 'unblockProject.php';
}
// ###########################################################################################

// ###########################################################################################
//.. 22.03.2023 - Восстановление заказов и проектов из резервного хранилища
if (isset($_POST["recoveryOrders"])) {
    $data = [];
    $incomingData = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["recoveryOrders"]))));
    require_once ADMIN_FUNCTIONS . 'recoveryOrders.php';
}
// ###########################################################################################

// ###########################################################################################
// Удаление клиента
if (isset($_POST["deleteClient"])) {
    $data = [];
    $incomingData["deleteClient"] = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["deleteClient"]))));
    require_once ADMIN_FUNCTIONS . 'deleteClient.php';
}
// ###########################################################################################

// ###########################################################################################
// Удаление черновиков заказов
if (isset($_POST["deleteDrafts"])) {
    $data = [];
    $incomingData["deleteDrafts"] = trim(strip_tags(stripcslashes(htmlspecialchars($_POST["deleteDrafts"]))));
    require_once ADMIN_FUNCTIONS . 'deleteDrafts.php';
}
// ###########################################################################################

print $data;

?>