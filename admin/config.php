<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/_1/config.php';
session_start();

define('ADMIN_TEMPLATES', DIR_CORE . 'admin/front/templates/');
define('ADMIN_CONTENT', DIR_CORE . 'admin/front/content/');
define('ADMIN_FUNCTIONS', DIR_CORE . 'admin/callableFunctions/');

define('DOMAIN_NAME', preg_replace('#^(http[s]?:\/\/|www.)#', '', HOST_NAME));
define('ADMIN_LIBS', HOST_NAME . '/admin/front/libs');
define('WEB_CONTENT', HOST_NAME . '/admin/front/content');
define('WEB_ADMIN', HOST_NAME . '/admin');
define('ADMIN_BOOTSTRAP_CSS', HOST_NAME . '/admin/front/libs/bootstrap-5.2.2-dist/css');
define('ADMIN_BOOTSTRAP_JS', HOST_NAME . '/admin/front/libs/bootstrap-5.2.2-dist/js');
define('ADMIN_FONTAWESOME_CSS', HOST_NAME . '/admin/front/libs/fontawesome-free-6.2.0-web/css');


$title = 'Админ-панель';
$pipe = ' | KRONAS SERVICE';

$page = [
    'api-help' => [
        'pathInfo' => 'apiHelp',
        'path' => ADMIN_CONTENT . 'apiHelp/apiHelp.php',
        'title' => 'Справка по API' . $pipe,
        'styles' => ADMIN_LIBS . '/css/apiHelp.css'
    ],
    'client-create' => [
        'pathInfo' => 'clientRegistration',
        'path' => ADMIN_CONTENT . 'clientRegistration/clientRegistration.php',
        'webPath' => '/admin/index.php',
        'title' => 'Регистрация контрагента' . $pipe,
        'styles' => ADMIN_LIBS . '/css/clientRegistration.css',
        'js' => ADMIN_LIBS . '/js/clientRegistration.js'
    ],
    'client-delete' => [
        'pathInfo' => 'deleteClient',
        'path' => ADMIN_CONTENT . 'deleteClient/deleteClient.php',
        'webPath' => '/admin/index.php',
        'title' => 'Удаление учётной записи контрагента' . $pipe,
        'styles' => ADMIN_LIBS . '/css/deleteClient.css',
        'js' => ADMIN_LIBS . '/js/deleteClient.js'
    ],
    'manager-create' => [
        'pathInfo' => 'createManager',
        'path' => ADMIN_CONTENT . 'createManager/createManager.php',
        'webPath' => '/admin/index.php',
        'title' => 'Создать нового менеджера' . $pipe,
        'styles' => ADMIN_LIBS . '/css/createManager.css',
        'js' => ADMIN_LIBS . '/js/createManager.js'
    ],
    'unblock-project' => [
        'pathInfo' => 'unblockProject',
        'path' => ADMIN_CONTENT . 'unblockProject/unblockProject.php',
        'webPath' => '/admin/index.php',
        'title' => 'Разблокировка для редактирования проекта, отправленного в производство' . $pipe,
        'styles' => ADMIN_LIBS . '/css/unblockProject.css',
        'js' => ADMIN_LIBS . '/js/unblockProject.js'
    ],
    'project-detail' => [
        'pathInfo' => 'projectDetail',
        'path' => ADMIN_CONTENT . 'projectDetail/projectDetail.php',
        'webPath' => '/admin/index.php',
        'title' => 'Деталировка проекта' . $pipe,
        'styles' => ADMIN_LIBS . '/css/projectDetail.css',
        'js' => ADMIN_LIBS . '/js/projectDetail.js'
    ],
    'recovery-orders' => [
        'pathInfo' => 'recoveryOrders',
        'path' => ADMIN_CONTENT . 'recoveryOrders/recoveryOrders.php',
        'webPath' => '/admin/index.php',
        'title' => 'Восстановление заказов и проектов после сбоя' . $pipe,
        'styles' => ADMIN_LIBS . '/css/recoveryOrders.css',
        'js' => ADMIN_LIBS . '/js/recoveryOrders.js'
    ],
    'delete-drafts' => [
        'pathInfo' => 'deleteDrafts',
        'path' => ADMIN_CONTENT . 'deleteDrafts/deleteDrafts.php',
        'webPath' => '/admin/index.php',
        'title' => 'Восстановление заказов и проектов после сбоя' . $pipe,
        'styles' => ADMIN_LIBS . '/css/deleteDrafts.css',
        'js' => ADMIN_LIBS . '/js/deleteDrafts.js'
    ],
];
?>