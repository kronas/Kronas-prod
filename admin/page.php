<?php

$currentPath = str_replace('/', '', $_SERVER['PATH_INFO']);

$content = $page[$currentPath]['pathInfo'];
$title = $page[$currentPath]['title'];

include_once ADMIN_TEMPLATES . 'header.php';

if (null !== $page[$currentPath]['path']) {
    include_once $page[$currentPath]['path'];
} else {
    include_once ADMIN_CONTENT . 'default.php';
}


include_once ADMIN_TEMPLATES . 'footer.php';

?>