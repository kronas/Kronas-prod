<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 06.04.2020
 * Time: 09:58
 */
?>

<html>
<head></head>
<body>
    <button id="mpr" onclick="send()">Send</button>


    <script>
        function send() {
            // 1. Создаём новый объект XMLHttpRequest
            var request = new XMLHttpRequest();
            var url = 'https://service.giblab.com';

            request.open("POST", url, false);
            request.setRequestHeader('Content-Type', 'application/octet-stream; charset=UTF-8');
            request.setRequestHeader('Content-Length', '1024');
            request.setRequestHeader('action', '0xDFFB69BB'); //action number / Номер действия
            request.setRequestHeader('fileName', 'test.mpr');//File name / Имя файла с расширением
            request.setRequestHeader('user-agent', 'Command');
            ///request.responseType = 'blob';

// 3. Отсылаем запрос
            request.send('sssss');



            if (request.status === 200) {
                data = stringToArrayBuffer(request.response);
                console.log(data)
            } else {
                alert('Something bad happen!\n(' + request.status + ') ' + request.statusText);
            }
// 4. Если код ответа сервера не 200, то это ошибка
            if (request.status != 200) {
                // обработать ошибку
                console.log( xhr.status + ': ' + xhr.statusText ); // пример вывода: 404: Not Found
            } else {
                // вывести результат
                console.log( request.responseText ); // responseText -- текст ответа.
                console.log( request.response ); // responseText -- текст ответа.
            }
        }


        function stringToArrayBuffer(str) {
            var buf = new ArrayBuffer(str.length);
            var bufView = new Uint8Array(buf);

            for (var i=0, strLen=str.length; i<strLen; i++) {
                bufView[i] = str.charCodeAt(i);
            }

            return buf;
        }
    </script>
</body>
</html>