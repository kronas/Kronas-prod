<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;


Route::match(['get', 'post'], '/', 'IndexController@index');
Route::match(['get', 'post'], '/projects/{project_id}', 'IndexController@index');
Route::match(['get', 'post'], '/send_xml', 'GiblabController@send_xml');
Route::get('/auth', function () {
    if(Request::input('logout') == 'true') {
        session(['isAdmin' => '']);
        session(['manager_name' => '']);
        session(['manager_id' => '']);
        return redirect('/');
    }
    $name = Request::input('login');
    $password = Request::input('password');
    $user = DB::table('manager')->where(['e-mail' => $name, 'password' => $password])->first();

    if(isset($user->id)) {
        session(['isAdmin' => $user->admin]);
        session(['manager_name' => $user->name]);
        session(['manager_id' => $user->code]);
        return 'Привет, '.$user->name;
    } else {
        return 'Данные авторизации не верны';
    }
});
Route::post('/send_project', function () {

    session_start();

    if( !session()->get('clear_project_data') || session()->get('clear_project_data') == '' ) {
        session(['clear_project_data' => Request::input('project_data')]);
    }
    session(['project_data' => Request::input('project_data')]);

    $session_id = session_id();

    $post_data = [
        '_token' =>  Request::input('_token'),
        'project_data' =>  Request::input('project_data'),
        'user_place' =>  Request::input('user_place'),
        'user_type_edge' =>  Request::input('user_type_edge'),
        'manager_id' => session('manager_id'),
        'manager_name' => session('manager_name'),
        'order_comment' => session('order_comment'),
        'session_id' => $session_id
    ];



    Storage::put('giblab_sessions/'.$session_id.'.txt', serialize($post_data));
    chmod('/home/varvar/public_html/kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt', 0777);
    session_destroy();

    return $session_id;
    ///return redirect()->away('http://test-gs.kronas.com.ua:11080/~varvar/sgd/index.php?data_id='.$session_id);
});

Route::post('/comment', function () {
    $comment = Request::input('comment');
    session(['order_comment' => $comment]);
    return redirect('/');
});

Route::post('/project_in_session', function(Request $request) {
    $project_data = Request::input('project_data');
    session(['project_data' => $project_data]);
    return 'Project save in session';
});
Route::get('/clear_project', function() {
    session(['project_data' => '']);
    session(['clear_project_data' => '']);
    session(['checked_project_data' => '']);
    return redirect()->back();
});

Route::get('/step_1', function(Request $request) {
    $data_id = Request::input('file');
    $data = unserialize(Storage::get('giblab_sessions/'.$data_id.'.txt'));
    ///dd($data['project_data']);
    session(['project_data' => $data['project_data']]);
    session(['checked_project_data' => $data['project_data']]);
    return redirect('/');
});
Route::get('/check_session', function() {
    $data = session()->all();
    return dd($data);
});
Route::get('/back_to_start', function() {
    session(['project_data' => session()->get('clear_project_data')]);
    return redirect('/');
});
Route::post('/project_in_session_checked', function() {
    $project_data = Request::input('project_data_checked');
    session(['checked_project_data' => $project_data]);
    return 'Checked project save in session';
});
Route::get('/save_data_id', function() {
    $data_id = Request::input('data_id');
    session(['data_id' => $data_id]);
});




Route::post('/user_save_project', function() {

    $data['checked_project_data'] = session()->get('checked_project_data');
    $data['data_id'] = session()->get('data_id');
    $data['order_comment'] = session()->get('order_comment');
    $data['project_data'] = Request::input('project_data');
    $data['user_place'] = Request::input('user_place');
    $data['user_type_edge'] = Request::input('user_type_edge');


    $encode_data = base64_encode(strrev(serialize($data)));
    $url = '/home/varvar/public_html/kronasapp/public';
    $file = '/users-projects/'.time().'.kronas';
    file_put_contents($url.$file, $encode_data);
    return 'http://test-gs.kronas.com.ua:8081'.$file;
});

Route::post('/user_upload_project', function(Request $request) {
    $file = Input::file('upload_project');
    session(unserialize(strrev(base64_decode(file_get_contents($file)))));
    return redirect('/');
});


Route::get('/back_clear_project', function() {
    session()->pull('checked_project_data');
    session(['project_data' => session()->get('clear_project_data')]);
    return redirect('/');
});


//Route::post('/admin_open_project', function () {
//    header('Content-type: text/xml');
//    $project = Request::input('open_project');
//    return $project;
//});






Route::post('/check_edits', function() {
    $project_data = Request::input('project');
    if($project_data != session()->get('checked_project_data')) {
        return 0;
    } else {
        return 1;
    }
});

