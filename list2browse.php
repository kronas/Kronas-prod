<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
       <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/main.css">

    <title>Title</title>
</head>
<body>


    <?php
    include_once('functions1.php');

    ?>
    <form method="post" enctype='multipart/form-data'>
        <?php

        $typeop=1; if(isset($_GET['typeop']) && ((int)$_GET['typeop'])>0) $typeop=(int)$_GET['typeop'];
        $vart=0; if(isset($_GET['vart']) && ((int)$_GET['vart'])>0) $vart=(int)$_GET['vart'];

        $where=array();$pics='';
        $data=get_list2browse($where, 0, 25,$typeop, $vart, $pics);
        ?>
<div class="row" >
    <div class="col-12" style="height: 120px; overflow: scroll;"> <?php
print $pics;
?>
    </div>
</div>
<div class="row">
    <div class="col-12" id="selected-prop"></div>
    <div class="col-6">


        <?php


        $tablerows =db_get_list_data_properties($typeop);

        $arr_prop_ids=array();
        $arr_prop_name=array();
        foreach ($tablerows as $row)
        {
            $arr_prop_ids[$row['PROPERTY_ID']][$row['VALUESTR'].$row['VALUEDIG']]=$row['VALUESTR'].$row['VALUEDIG'];
            $arr_prop_name[$row['PROPERTY_ID']] =array('NAME'=>$row['NAME'], 'DESCRIPTION'=>$row['DESCRIPTION']);
        }
        echo '<div class="accordion" id="accordionExample">';

        foreach ($arr_prop_ids as $key=>$vsl) {

//            echo $key.'<pre>';             print_r($vsl);             echo '</pre>';

            echo '<div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                        <button class="btn btn-secondary btn-sm btn-block" type="button" data-toggle="collapse" data-target="#clps-'.$key.'" aria-expanded="false" aria-controls="clps-'.$key.'">
                        '.$arr_prop_name[$key]['NAME'].'
                        </button>
                      </h2>
                    </div>
                
                    <div id="clps-'.$key.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body" style="max-height: 80vh; overflow: scroll;">';
            $counter=0;
        foreach ($vsl as $items) {
           echo '<div class="form-check">
                  <input class="form-check-input" type="checkbox" value="'.$items.'" id="check-'.$key.'-'.$counter.'">
                  <label class="form-check-label" for="check-'.$key.'-'.$counter.'">'.$items.'</label></div>';
            $counter++;
        }
             echo '</div>
            </div>
          </div>';
        }
        echo '</div>';
 ?><div class="col-md-12 text-center">
        <button type="button" id="submit" class="btn btn-success" value="filtr">Фильтровать</button>
        <button type="button" id="reset" class="btn btn-warning" value="reset">Сбросить фильтр</button>
    </div>
    </div>
    <div class="col-6 data-list">
        <?php
        echo $data;
        ?>
</div>
</div>
    </form>
    <script>
        <?php
            include_once('list2browse.js');
            ?>
    </script>
</body>
</html>