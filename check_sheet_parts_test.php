<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <title>Подтверждение заказа</title>
    <style >
        .erase-tr{
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }
        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }
        .loading-search{
            position: absolute;
            right: 22px;
            top: 3px;
            display: none;
        }
        #searchOnCode-box{padding: 10px;display: none;}
        #searchOnCode-box li{ cursor: pointer; }
        form{
            margin: 10px;
        }
        h3.success  {
            color: green;
        }
        body {
            padding: 45px;
        }
    </style>


</head>
<body>
<?php

include_once("func.php");
session_start();

if ((isset($_GET["data_id"]))&&(!isset($_POST["stock_to"]))) 
{
    $_SESSION = get_temp_file_data($_GET["data_id"]);
    $_SESSION["data_id"] = $_GET["data_id"];
    if (isset($_SESSION['error']))
    {
        put_temp_file_data($_SESSION["data_id"], $_SESSION);
        header("Location: ".$laravel_dir."/step_1?file=".$_SESSION["data_id"].'&nw='.$_GET['nw']);
    }
}
if ($_GET["data_d"]) {
    unset($_SESSION, $_POST);
    session_destroy();

}
// p_($_SESSION);exit;
// test(11,htmlspecialchars($_SESSION['project_data']));
if ($_SESSION["project_data"] && !isset($_POST["stock_to"]))
{
    // include ('step1.php');
    // if (!$_SESSION["user_place"]) $place=1;
    // else $place = $_SESSION["user_place"];
    $_SESSION["project_data"]=get_out_part_stock($_SESSION["project_data"]);
    $_POST["project_data"] = $_SESSION["project_data"];
    
    $response_optimization = send_post_data($_SESSION["project_data"], '55C00560');
    $array = json_decode($response_optimization, true);
    if (!isset($array['project'])) {
        echo "Данные не прошли оптимизацию";
        exit;
    } else {
        $data_xml = $array['project'];
        ///echo 'Данные прошли оптимизацию';
    }
    $_SESSION["project_data"]=$array['project'];

    get_vals_index($_SESSION["project_data"]);
    $vals = $_SESSION["vals"];
    unset ($_SESSION["vals"]);
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    // xml($vals);

    $_SESSION['vals']=$vals;
}
// p_($vals);exit;
if ((!isset($sheet_parts)) && !isset($_POST["stock_to"]))
{
 
    foreach ($index["PART"] as $i)
    {
        if (($vals[$i]['attributes']['CL']>0)&&($vals[$i]['attributes']['COUNT']>$vals[$i]['attributes']['USEDCOUNT']))
        {
            $part_problem[$vals[$i]['attributes']['ID']] = $vals[$i];
            $e4=part_get_material($vals, $vals[$i]["attributes"]["ID"]);
            $part_problem[$vals[$i]['attributes']['ID']]['sheet']=$e4;
        }
    }
    if (count ($part_problem)>0 && $_POST['go_check_sheets_part'] != 1)
    {
        echo ' Детали, которые не вошли в раскрой:';
        $html = '
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Название</th>
                  <th scope="col">Длина</th>
                  <th scope="col">Ширина</th>
                  <th scope="col">Не вошло в раскрой шт</th>
                  <th scope="col">Материал</th>
                </tr>
              </thead>
              <tbody>
        ';
        foreach ($part_problem as $id=>$part)
        {
            if (!isset($part['attributes']['NAME'])) $part['attributes']['NAME']='деталь без названия ';
            $under = $part['attributes']['COUNT']-$part['attributes']['USEDCOUNT'];
            $html .= <<<rrr
                    <tr>
                      <td>{$part['attributes']['NAME']}</td>
                      <td>{$part['attributes']['DL']}</td>
                      <td>{$part['attributes']['DW']}</td>
                      <td>{$under}</td>
                      <td>{$part_problem[$id]['sheet']['attributes']['NAME']}</td>
                    </tr>
            rrr;
        }
        $html .= '
             </tbody>
            </table>
            <div style="display: flex; flex-flow: row; align-items: center">
                <a href="'.$laravel_dir.'" class="btn btn-danger">Назад к редактированию</a>
                <form action="check_sheet_parts_test.php" method="POST">
                        <input type="hidden" name="go_check_sheets_part" value="1">
                        <button type="submit" class="btn btn-success">OK</button>
                </form>
                
            </div>
        ';

        echo $html;
        $_SESSION['part_problem_show'] = 1;
        exit;




    }
    foreach ($index["PART"] as $i)
    {
       
        if (($vals[$i]["attributes"]["SHEETID"]>0)&&($vals[$i]["attributes"]["L"]>299)&&($vals[$i]["attributes"]["W"]>299)
        &&($vals[$i]["attributes"]["WASTE"]=="true"))
        {
            $j=$i-1;
            $j1=$i-1;

            while (($vals[$j]["tag"]=="PART")&&($vals[$i]["attributes"]["SHEETID"]<>$vals[$j]["attributes"]["ID"]))
            {
                $j--;
            }
            while ($vals[$j1]["attributes"]["TYPEID"]<>"sheet")
            {
                $j1--;
            }
//j основной лист
//j1 sheet

                // p_($vals[$j1]);
                // p_($vals[$j]);
                // p_($vals[$i]);
                if ($vals[$j1]['attributes']['CODE']>0)
                {
                    $sql23="select * from MATERIAL where CODE=".$vals[$j1]['attributes']['CODE'];
                    $sql32=mysqli_query($link,$sql23);
                    $sql332=mysqli_fetch_array($sql32);
                    
                    if (isset($sql332))
                    {
                        
                        if (($vals[$j]["attributes"]["L"]-$vals[$i]["attributes"]["L"]<30)&&($vals[$i]["attributes"]["W"]>=$sql332["W"]/2-5)&&($sql332['MY_HALF']==1))
                        {
                       
                            if (($vals[$i]["attributes"]["W"]-$sql2["W"]/2-5>300)
                            &&
                            ($vals[$i]["attributes"]["L"]==$sql332["L"])&&
                            ($vals[$j]["attributes"]["W"]==$sql332["W"])
                            &&($vals[$i]["attributes"]["W"]-$vals[$j]["attributes"]["W"]/2-5>300)
                            && ($sql332['MY_1C_NOM']<>5931))
                            {
                                // p_($sql332);
                                // p_($vals[$i]);
                                // exit;
                                $vals[$i]["attributes"]["W"]=$vals[$i]["attributes"]["W"]-$vals[$j]["attributes"]["W"]/2-5;
                            }
                            
                        }
                        
                        // p_($sql332);
                        if(($vals[$i]["attributes"]["W"]<$sql332["W"]/2-5)OR($sql332['MY_HALF']<1)OR($sql332['ST']==1))
                        {
                            
                            $sheet_parts[$vals[$i]["attributes"]["ID"]]=
                            array("ID"=>$vals[$i]["attributes"]["ID"],
                            "L"=>round($vals[$i]["attributes"]["L"]-30),
                            "W"=>round($vals[$i]["attributes"]["W"]-30),
                            "T"=>round($sql332["T"],2),
                            "COUNT"=>$vals[$i]["attributes"]["COUNT"],
                            "CODE"=>$sql332["CODE"],
                            "USEDCOUNT"=>$vals[$i]["attributes"]["COUNT"],
                            "NAME"=>"Остаток клиента (".$sql332["CODE"].") (".round($vals[$i]["attributes"]["L"]-30)."|".round($vals[$i]["attributes"]["W"]-30)."|".round($sql332["T"],2).
                            ") ".$vals[$j]["attributes"]["NAME"],
                            "index"=>$i);
                            if (($sql332['ST']==1)&&($vals[$i]["attributes"]["W"]==$vals[$j]["attributes"]["W"]))
                            {
                                $sheet_parts[$vals[$i]["attributes"]["ID"]]['W']=$vals[$i]["attributes"]["W"];
                                $sheet_parts[$vals[$i]["attributes"]["ID"]]['NAME']="Остаток столешницы клиента (".$sql332["CODE"].") (".round($vals[$i]["attributes"]["L"]-30)."|".round($vals[$i]["attributes"]["W"])."|".round($sql332["T"],2).
                                ") ".$vals[$j]["attributes"]["NAME"];

                                // p_($sheet_parts);exit;
                            }
                            // 
                        
                        }
                    }
                }
                // else $_POST["stock_to"]='no';
        }
        
    }
    $_SESSION["vals"]=$vals;
    $_SESSION["sheet_parts"]=$sheet_parts;
    
    
}

// p_($sheet_parts);exit;
if (isset($sheet_parts)&&(!isset($_POST["stock_to"])))
{
    
    echo "
    <h3>В проекте есть остатки: </h3>
    <hr>
    <table cellspacing='0' ce>
    <thead>
        <tr>
            <td>Название</td>
            <td>Ширина(W)</td>
            <td>Длина(L)</td>
            <td>Количество</td>
            <td>Код</td>
        </tr>
    </thead>
    <tbody>
    

    ";
    // echo print_r_($sheet_parts);
    foreach ($sheet_parts as $item) {
        echo "
            <tr>
                <td>".$item['NAME']."</td>
                <td>".$item['W']."</td>
                <td>".$item['L']."</td>
                <td>".$item['COUNT']."</td>
                <td>".$item['CODE']."</td>
            </tr>

        ";
    }
    

    echo "</tbody></table><hr>";
    
    echo "


    <style>
        table td {
            border: 1px solid gray;
            padding: 9px;
            text-align: left;
        }
        table thead td {
            font-weight: bold;
        }
        table tbody td {
            opacity: 1;
        }
        table {
            margin: 15px 0;
            border-collapse: collapse;
            font-style: normal;
        }
    </style>
    
    
    ";


    echo "
    <form method=\"post\" enctype='multipart/form-data' action=\"check_sheet_parts.php?data_id=\"".$_GET[$data_id].'&nw='.$_GET['nw']."\">
    <input type=\"radio\" name=\"stock_to\" value=\"yes\"> Остатки в детали<Br>
    <input type=\"radio\" name=\"stock_to\" value=\"no\"> не нужны<Br>
       <input type='submit' value='Далее' id='check_sheet_parts_submit' > 
       </form>
       <div id=\"checks_lodaer_containers\">
    <div>
        <img src=\"".$laravel_dir."/images/download.gif\" alt=\"\">
        <h1>Идет пересчет проекта...</h1>
    </div>
    </div>
    <style>
        
        #checks_lodaer_containers {
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(255,255,255,0.4);
            width: 100%;
            height: 100%;
            display: none;
            z-index: 999999999;
        }
        #checks_lodaer_containers > div {
            width: 100%;
            display: flex;
            flex-flow: column;
            justify-content: center;
            align-items: center;
            z-index: 9999;
            height: 100%;
        }
        #checks_lodaer_containers > div > h1 {
            text-align: center;
        }
    </style>
    <script>
        let button = document.getElementById('check_sheet_parts_submit');
        let loader = document.getElementById('checks_lodaer_containers');
        
        button.onclick = function(e) {
            
            loader.style.display = 'flex';
      };
    
      
    </script>
       ";
       //echo $r;
    //    p_($_SESSION);
  
       exit;

     
}
// p_($_SESSION);exit;

if (isset($_POST["stock_to"])) $_SESSION['stock_to']=$_POST["stock_to"];
if ($_POST["stock_to"]=="yes") $vals_parts=put_sheet_parts_into_project($_SESSION['vals'],$_SESSION['sheet_parts']);
// xml($_SESSION['vals']);

$vals=$_SESSION['vals'];
$_SESSION['vals_shablon']=$vals;
$vals_shablon=$vals;

$data_xml = vals_index_to_project($vals);
$response_parametrization = send_post_data($data_xml, '4EC657AF');
$array = json_decode($response_parametrization, true);
if (!isset($array['project'])) {
    echo "Данные не прошли депараметризацию";
    exit;
} else {
    $data_xml = $array['project'];
    ///echo 'Данные прошли параметризацию';
}
$data=get_vals_index_without_session($data_xml);
$vals = $data["vals"];
// xml($_SESSION['vals']);




// если хотим записать после депараметризации, убрать ниже комент
// $_SESSION["project_data"]=$array['project'];


// header('Content-type: text/xml');
// echo vals_index_to_project($vals);
// p_($_SESSION);exit;

include ("calculate.php");


// перегнать Vals в строку, убрать параметризацию, пересчитать оптимизация. Отдать $vals, с которым я далее работаю.
?>


</body>
</html>