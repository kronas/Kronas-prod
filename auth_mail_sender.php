<?php
session_start();

include_once("func.php");
include_once("description.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once 'PHPMailer/Exception.php';
require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/SMTP.php';

$password = $_GET['password'];
$email = $_GET['email'];

function send_text_mail($text, $adress, $adress_name) {
    $mail = new PHPMailer;
    $mail_params=$GLOBALS['mail_params'];

    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->isSMTP();
    $mail->SMTPDebug = 2;
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = $mail_params['Host'];
    $mail->Port = $mail_params['Port']; // typically 587
    $mail->SMTPSecure = $mail_params['SMTPSecure']; // ssl is depracated
    $mail->SMTPAuth = $mail_params['SMTPAuth'];
    $mail->Username = $mail_params['Username'];
    $mail->Password = $mail_params['Password'];

    $mail->setFrom($mail_params['From'], 'Kronas');
    $mail->addAddress($adress, $adress_name);

    $mail->Subject = 'Ваш новый пароль!';
    $mail->msgHTML($text); // remove if you do not want to send HTML email
    $mail->AltBody = 'Ваш новый пароль!';
    ///$mail->addAttachment('docs/brochure.pdf'); //Attachment, can be skipped

    $mail->send();
}

$text_client = '<p>Уважаемый клиент!</p><p>Ваш новый пароль: <b><large>'.$password.'</large></b> , ';
send_text_mail($text_client, $email, '');

?>