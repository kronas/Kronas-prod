<?php

// include_once ("func.php");
// include_once ("./_1/config.php");

include_once(__DIR__.'/_1/config.php');
include_once(__DIR__.'/func.php');
function db_get_arr_one_in($in,$type)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_arr_one_in start', $_SESSION);
    $sql = "SELECT tbl.`code` FROM $type as tbl WHERE
tbl.`code` IN (".implode(',',$in).") AND (SELECT COUNT(*) FROM $type as t_in WHERE t_in.`code` = tbl.`code`)=1" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_arr_one_in end', $_SESSION);
    return $result['data'];
}
/*
function db_get_tree_OLD($type)
{
    $db=connect_db();
    $sql = "SELECT DISTINCT
root.FOLDER_ID,
root.`NAME` as fname,
root.PARENT,
root.1CCODE,
root.`CODE`
FROM
GOOD_TREE AS root
WHERE
root.PARENT = 0  AND root.FOLDER_ID IN ( SELECT  
trp.PARENT 
FROM
GOOD_TREE as trp
INNER JOIN GOOD_TREE_{$type}_CONN as pc ON pc.FOLDER_ID = trp.FOLDER_ID ) 
UNION 
SELECT DISTINCTROW 
tr.FOLDER_ID,
tr.`NAME`,
tr.PARENT,
tr.1CCODE,
tr.`CODE`
FROM
GOOD_TREE as tr
INNER JOIN GOOD_TREE_{$type}_CONN ON GOOD_TREE_{$type}_CONN.FOLDER_ID = tr.FOLDER_ID 
ORDER BY fname ASC" ;

    $result = mysqli_query($db, $sql) or die("Ошибка 3" . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
*/
function db_get_tree()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_tree start', $_SESSION);
    $sql = "SELECT DISTINCT root.FOLDER_ID, root.`NAME` as fname, root.PARENT, root.1CCODE, root.`CODE` FROM
GOOD_TREE AS root ORDER BY fname ASC" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_tree end', $_SESSION);
    return $result['data'];
}
function db_get_list_data($type)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_data start', $_SESSION);
    $_field=$type=='MATERIAL'?'m.L as L':'m.T as L';
    $sql = "SELECT m.`NAME`, m.TYPENAME, {$_field}, m.W, m.CODE, t.FOLDER_ID 
        FROM {$type} AS m LEFT JOIN GOOD_TREE_{$type}_CONN AS t ON t.{$type}_ID = m.{$type}_ID
        ORDER BY m.`NAME` ASC" ;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_data end', $_SESSION);
    return $result['data'];
}

function db_get_list_type_double()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_type_double start', $_SESSION);
    $sql = "SELECT dt.DOUBLE_TYPE_ID, dt.`NAME`, dt.DESCRIPTION, dt.SAME, dt.EDGE FROM DOUBLE_TYPE AS dt" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_type_double end', $_SESSION);
    return $result['data'];
}
function get_list_data($type)
{
    $arr=db_get_list_data($type);
    $out='<ul class="list-data-tree-'.strtolower($type).'" style="max-height: 80vh; overflow: scroll;min-height: 600px;">';
    foreach ($arr as $item)
    {
        $out.='<li class="folid-'.$item['FOLDER_ID'].'"  data-code="'.$item['CODE'].'"  data-w="'.$item['W'].'"> '.
            $item['NAME'] . ' {'.$item['TYPENAME'] .  ') '.$item['L'] . ' X '.$item['W'] . '</li>';
    }

    $out.='</ul>';
    return $out;
}
function get_list_type_double()
{
    $arr=db_get_list_type_double();
    $out='';
    foreach ($arr as $item)
    {
        $out.='<option value="'.$item['DOUBLE_TYPE_ID'] .  '"  data-description="'.$item['DESCRIPTION'].'" data-same="'.$item['SAME'].'" data-edge="'.$item['EDGE'].'"  >'.$item['NAME'] .  '</option>';
    }
    return $out;
}
function db_get_user_place_data()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_user_place_data start', $_SESSION);
    $sql = "SELECT PLACES.PLACES_ID, PLACES.`NAME` FROM PLACES ORDER BY PLACES.`NAME` ASC" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_user_place_data end', $_SESSION);
    return $result['data'];
}
function db_get_user_place_tool_data()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_user_place_tool_data start', $_SESSION);
    $sql = "SELECT 
TOOL_CUTTING_EQUIPMENT.PLACE,
TOOL_CUTTING.TYPE
FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
    inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID
WHERE
TOOL_CUTTING.PRODUCT = 'wt'" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_user_place_tool_data end', $_SESSION);
    return $result['data'];
}
function db_get_user_place($id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_user_place start', $_SESSION);
    $sql = "SELECT NAME, ADRESS, PHONE FROM PLACES WHERE PLACES_ID = ".$id ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_user_place end', $_SESSION);
    return implode(' ', $result['data'][0]);
}
function db_get_materials_data($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_materials_data start', $_SESSION);
    $out=array();
    $sql = "SELECT DISTINCTROW MATERIAL.*, MATERIAL_PIC.* 
            FROM MATERIAL
            INNER JOIN MATERIAL_PIC ON MATERIAL_PIC.MATERIAL_ID = MATERIAL.MATERIAL_ID
            WHERE MATERIAL.`CODE` IN (".implode(', ',$in).")
            GROUP BY MATERIAL.`CODE` ORDER BY MATERIAL_PIC.SORT ASC" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    foreach($result['data'] as $row)  {
        $out[$row['CODE']]=$row;
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_materials_data end', $_SESSION);
    return $out;
}
function db_get_list_add_data($search_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_materials_data start', $_SESSION);
    $pname=array(
        'sheet' => 'Плитные материалы',
        'band' => 'Кромочные материалы',
        'simple' => 'Фурнитура');
    $out='<ul class="list-group">';
    $search_in=trim($search_in);
    $sql=db_get_list_add_data_sub1($search_in, 1, 10);
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

    $row_cnt =$result['count'];
    if($row_cnt>0) db_get_list_add_data_crea($result['data'], $out);
    if($row_cnt<5 && substr_count($search_in,' ')>0)
    {
        $sql=db_get_list_add_data_sub1($search_in, 2, 10-$row_cnt);
        $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        $row_cnt2 = $result['count'];
        if($row_cnt2>0) db_get_list_add_data_crea($result['data'], $out);
        if(($row_cnt+$row_cnt2)<10)
        {
            $sql=db_get_list_add_data_sub1($search_in, 3, 10-($row_cnt+$row_cnt2));
            $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
            $row_cnt3 = $result['count'];
            if($row_cnt3>0) db_get_list_add_data_crea($result['data'], $out);
        }
    }
    $out.="</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_materials_data end', $_SESSION);
    return $out;
}

function db_get_list_add_data_crea($result, &$out)
{
    $pname=array(
        'sheet' => 'Плитные материалы',
        'band' => 'Кромочные материалы',
        'simple' => 'Фурнитура');
    // while ($row = $result->fetch_assoc()) {
    //     $out.="<li  onClick=\"list_data_select('".$row['mtype'].'_'.$row['mcode']."');\" class=\"list-group-item\">(".$pname[$row['mtype']].') '.$row['mname']."</li>";
    // }
    foreach ($result as $key => $value) {
        $out.="<li  onClick=\"list_data_select('".$value['mtype'].'_'.$value['mcode']."');\" class=\"list-group-item\">(".$pname[$value['mtype']].') '.$value['mname']."</li>";
    }


}
function db_get_list_add_data_sub1($search_in, $op, $limit)
{
    $_where=" WHERE CONVERT(CODE,char) LIKE '%$search_in%' OR  NAME LIKE '%$search_in%'  ";
    if($op==2)
    {
        $_arr_w1=explode(' ',$search_in);
        $_where=" WHERE NAME LIKE '%".implode("%' AND NAME LIKE '%",$_arr_w1)."%' ";
    }
    if($op==3)
    {
        $_arr_w1=explode(' ',$search_in);
        $_where=" WHERE NAME LIKE '%".implode("%' OR NAME LIKE '%",$_arr_w1)."%' ";
    }
    $sql = "SELECT DISTINCTROW 
MATERIAL.TYPEID as mtype, 
MATERIAL.CODE as mcode,
MATERIAL.NAME as mname 
FROM
MATERIAL
$_where   
UNION ALL 
SELECT DISTINCTROW 
BAND.TYPEID as mtype, 
BAND.CODE as mcode,
BAND.NAME as mname 
FROM
BAND 
$_where  
UNION ALL 
SELECT DISTINCTROW 
SIMPLE.TYPEID as mtype, 
SIMPLE.CODE as mcode,
SIMPLE.NAME as mname 
FROM
SIMPLE 
$_where 
ORDER BY mname ASC LIMIT 0,".$limit ;
    return $sql;
}
function db_get_list_add_line($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_add_line start', $_SESSION);
    $_in = explode('_', $in);
    // echo $in;
    if ($_in[0]=='undefined')$_in[0]='simple';
    $pname = array(
        'sheet' => 'MATERIAL',
        'band' => 'BAND',
        'simple' => 'SIMPLE');
    $sql = "SELECT * FROM " . $pname[$_in[0]] . " LEFT JOIN " . $pname[$_in[0]] . "_PIC ON " .
        $pname[$_in[0]] . "_PIC." . $pname[$_in[0]] . "_ID = " . $pname[$_in[0]] . "." . $pname[$_in[0]] . "_ID WHERE CODE = " . $_in[1];

    $row=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    switch ($_in[0])
    {
        case 'sheet':
            $_desc=$row[0]['L'].' X '.$row[0]['W'].' X '.$row[0]['T'].'<br>'.$row[0]['TYPENAME'].'<br>'.$row[0]['NAME'];
            break;
        case 'band':
            $_desc=$row[0]['W'].' X '.$row[0]['T'].'<br>'.$row[0]['TYPENAME'].'<br>'.$row[0]['NAME'];
            break;
        case 'simple':
            $_desc=$row[0]['TYPENAME'].'<br>'.$row[0]['NAME'];
            break;
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_add_line end', $_SESSION);
    return '<tr class="tablerow group-2 addtable-tr" data-calc="3" data-original-title= "' . $row[0]['NAME'] . '" 
data-img = "' . $row[0]['FILE_NAME_SMALL'] . '"  data-ins = "' . $_desc . '">
      <td>' .  $_in[1] . '</td>
      <td>' . $row[0]['NAME'] . '
      <input type="hidden" name="simpleprice[]" value="' . $_in[1] . '">
      </td>
      <td class="count text-center" data-count="1"><input type="text" name="simplepricecnt[]" value="1"></td>
      <td class="price text-center" data-price="' . $row[0]['COST'] . '">' . $row[0]['COST'] . '</td>
      <td class="total text-center">' . $row[0]['COST'] . '</td>
      <td class="erase text-center"><span class="erase-tr"><i class="close-sel">x</i> Удалить</span></td>
    </tr>';
}
function db_get_bands_data($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_bands_data start', $_SESSION);
    $out=array();
    $sql = "SELECT DISTINCTROW BAND.*, BAND_PIC.* 
            FROM BAND
            INNER JOIN BAND_PIC ON BAND_PIC.BAND_ID = BAND.BAND_ID
            WHERE BAND.`CODE` IN (".implode(', ',$in).")
            GROUP BY BAND.`CODE` ORDER BY BAND_PIC.SORT ASC" ;
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach($result as $row) {
        $out[$row['CODE']]=$row;
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_bands_data end', $_SESSION);
    return $out;
}
function get_user_place_data()
{
    $_pt=db_get_user_place_tool_data();
    $pt=array();
    $_js_auto=array();
    $_js_manual=array();
    foreach ($_pt as $key=>$value)
    {
        if(!isset($pt[$value['PLACE']])) $pt[$value['PLACE']]=array('auto'=>0,'manual'=>0);
        $pt[$value['PLACE']][$value['TYPE']]=1;
        switch ($value['TYPE'])
        {
            case 'auto':
                $_js_auto[]=$value['PLACE'];
                break;
            case 'manual':
                $_js_manual[]=$value['PLACE'];
                break;
        }
    }
    echo '<script type="text/javascript">
                                var arr_place_auto = ['.implode(',', $_js_auto).'];  
                                var arr_place_manual = ['.implode(',', $_js_manual).'];  
                                </script>';
    $arr=db_get_user_place_data();
    $out='';
    foreach ($arr as $item)
    {//  data-auto="'.$item['PLACES_ID'].'"
       $out.='<option value="'.$item['PLACES_ID'].'">'.$item['NAME'].'</option>';
    }
    return $out;
}
function get_folder_tree()
{
    $tablerows=db_get_tree();
$tree = form_tree($tablerows);


return build_tree($tree, 0, $type);
    }
function get_arr_invert($in, $out)
{
    $return = $in;
        foreach ($out as $k=>$v) {
            if(in_array($v['code'],$return))
            {
                foreach ($return as $k2=>$v2) {
                    if($v2==$v['code']) unset($return[$k2]);
                }
            }
        }
    return  $return;
}

function get_parts($vals, $index, $id)
{
    $objectID=0;
    $rez=array();
        foreach ($index['MATERIAL'] as $k=>$v) if($vals[$v]['attributes']['ID']==$id) $objectID=$v;
    $out='';
    if($objectID>0){
        $_range=array('min'=>0,'max'=>count($vals));
        $out='<table style="border: 1px solid gray;" border="2"><tr><td>W</td><td>L</td><td>Name</td></tr>';
        get_parts_range($index, $objectID, $_range);
/*
        echo  '<br>objectID='.$objectID.'<pre>';
        print_r($_range);
        echo  '</pre>';
        */
$_arr_part=array();
        for($i=$_range['min'];$i<=$_range['max'];$i++) {
            if ($vals[$i]['tag'] == 'PART') {
                $_arr_part[] = $vals[$i]['attributes']['ID'];
            }
        }

        foreach ($vals as $_key=>$_item)
        {
            if(isset($_item['tag']) && isset($_item['attributes']) && isset($_item['attributes']['ID']) && $_item['tag']=='PART' && in_array($_item['attributes']['ID'],$_arr_part ))
            {
                for($t=($_key-1);$t>=0;$t--)
                {
                    if($vals[$t]['tag']!='PART') {
                        if (isset($vals[$t]['attributes']['TYPEID']) && $vals[$t]['attributes']['TYPEID']=='product')
                        {
                            if(isset($vals[$_key]['attributes']['NAME'])) $name=$vals[$_key]['attributes']['NAME'];
                            $out.='<tr><td>'.$vals[$_key]['attributes']['W'].'</td><td>'.
                                $vals[$_key]['attributes']['L'].'</td><td>'.$name.'</td></tr>';

                        }
                        break;
                    }
                }
            }
        }

        $out.='</table>';
    }
    return $out;
}

function get_parts_range($index, $id, &$_range)
{
    foreach ($index['OPERATION'] as $k=>$v)
    {
        if($v>=$_range['min'] && $v<=$id) $_range['min']=$v;
        if($v<$_range['max'] && $v>$id) $_range['max']=$v;
    }
    $_range['max']=$_range['max']-1;
}

function form_tree($mess)
{
    if (!is_array($mess)) {
        return false;
    }
    $tree = array();
    foreach ($mess as $value) {
        $tree[$value['PARENT']][] = $value;
    }
    return $tree;
}
function build_tree($cats, $parent_id, $type)
{
    if (is_array($cats) && isset($cats[$parent_id])) {
        $tree = $parent_id==0?'<ul id="simple_list">':'<ul>';
        foreach ($cats[$parent_id] as $cat) {
            $tree .= '<li  data-folderid="'. $cat['FOLDER_ID'].'"><i class="fa fa-file-text-o" aria-hidden="true"></i><span>' . $cat['fname'].'</span>';
            $tree .= build_tree($cats, $cat['FOLDER_ID'],$type);
            $tree .= '</li>';
        }
        $tree .= '</ul>';
    } else {
        return false;
    }
    return $tree;
}

function get_arr_parts($vals, $index, &$arr_parts, &$materials)
{
    
    $vals=vals_out($vals);
    $index=make_index($vals);
    $arr_t_sheet=get_arr_t_sheet($vals);
    $arr_operation=get_arr_operation($vals);
    $arr_sheets=get_arr_sheets($vals);
//    echo '<pre>'; print_r($vals); echo '</pre>'; exit();

    $arr_t_parts=get_arr_t_parts($arr_operation, $arr_sheets);
    $arr=array();
    $tmp=array('start'=>0,'end'=>0,'id'=>0,'NAME'=>'');

    $_materials=array();
    foreach ($vals as $k=>$v)
    {

        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='open' && isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='product')
        {
            $tmp['start']=$k;
            $tmp['id']=$v['attributes']['ID'];
            $tmp['NAME']=$v['attributes']['NAME'];


        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='open' && isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='sheet')
        {
            $__name=trim($v['attributes']['NAME']);
            if(strlen($__name)>0 && isset($v['attributes']['CODE'])&&!isset($v['attributes']['MY_FIRST'])) {
                $_materials[] = array(
                    'arr_id' => $k,
                    'T' => $v['attributes']['T'],
                    'NAME' => $__name,
                    'ID' => $v['attributes']['ID'],
                    'CODE' => $v['attributes']['CODE'],
                );
            }
        }

        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='close')
        {
            $tmp['end']=$k;
            if($tmp['start']>0) $arr[$tmp['id']]=$tmp;
            $tmp=array('start'=>0,'end'=>0,'id'=>0,'NAME'=>'');


        }
    }
    // echo '<pre>'; print_r($arr); echo '</pre>';

    $arr2=array();
    foreach ($arr as $k=>$v)
    {
        for($i=$v['start'];$i<=$v['end'];$i++)
        {
            if(isset($vals[$i]['tag']) && $vals[$i]['tag']=='PART' )
            {
                
                $arr2[$i]=array(
                    'group_id'=>$v['id'],
                    'group_name'=>$v['NAME'],
                    'index'=>$i,
                    'edge'=>0,
                    'XNC'=>0,
                    'ID'=>$vals[$i]['attributes']['ID'],
                    'CODE'=>$arr_t_parts[$vals[$i]['attributes']['ID']]['CODE'],
                    'T'=>$arr_t_parts[$vals[$i]['attributes']['ID']]['T'],
                    'DL'=>$vals[$i]['attributes']['DL'],
                    
                    'DW'=>$vals[$i]['attributes']['DW'],
                    'L'=>$vals[$i]['attributes']['L'],
                    'W'=>$vals[$i]['attributes']['W'],
                    
                    'COUNT'=>$vals[$i]['attributes']['COUNT'],
                    'NAME'=>$vals[$i]['attributes']['NAME']
                );
                foreach ($vals[$i]['attributes'] as $rel=>$rel1)
                {
                    $arr2[$i][mb_strtolower($rel)]=$rel1;
                    $arr2[$i][$rel]=$rel1;
                }
               
                $arr2[$i]['CL']=$vals[$i]['attributes']['CL'];
                $arr2[$i]['CW']=$vals[$i]['attributes']['CW'];
            //    echo '<pre>'; print_r($vals[$i]['attributes']);                echo '</pre>';
                if(isset($vals[$i]['attributes']['MY_DOUBLE_RES'])) $arr2[$i]['MY_DOUBLE_RES']=$vals[$i]['attributes']['MY_DOUBLE_RES'];
                if(isset($vals[$i]['attributes']['MY_DOUBLE_ID'])) $arr2[$i]['MY_DOUBLE_ID']=$vals[$i]['attributes']['MY_DOUBLE_ID'];
                if(isset($vals[$i]['attributes']['PART.MY_CUT_ANGLE_T'])) $arr2[$i]['MY_CUT_ANGLE_T']=$vals[$i]['attributes']['PART.MY_CUT_ANGLE_T'];
                if(isset($vals[$i]['attributes']['PART.MY_CUT_ANGLE_B'])) $arr2[$i]['MY_CUT_ANGLE_B']=$vals[$i]['attributes']['PART.MY_CUT_ANGLE_B'];
                if(isset($vals[$i]['attributes']['PART.MY_CUT_ANGLE_R'])) $arr2[$i]['MY_CUT_ANGLE_R']=$vals[$i]['attributes']['PART.MY_CUT_ANGLE_R'];
                if(isset($vals[$i]['attributes']['PART.MY_CUT_ANGLE_L'])) $arr2[$i]['MY_CUT_ANGLE_L']=$vals[$i]['attributes']['PART.MY_CUT_ANGLE_L']; 
                if(isset($vals[$i]['attributes']['MY_XNC_SIDE_T'])) $arr2[$i]['MY_XNC_SIDE_T']=$vals[$i]['attributes']['MY_XNC_SIDE_T'];
                if(isset($vals[$i]['attributes']['MY_XNC_SIDE_B'])) $arr2[$i]['MY_XNC_SIDE_B']=$vals[$i]['attributes']['MY_XNC_SIDE_B'];
                if(isset($vals[$i]['attributes']['MY_XNC_SIDE_R'])) $arr2[$i]['MY_XNC_SIDE_R']=$vals[$i]['attributes']['MY_XNC_SIDE_R'];
                if(isset($vals[$i]['attributes']['MY_XNC_SIDE_L'])) $arr2[$i]['MY_XNC_SIDE_L']=$vals[$i]['attributes']['MY_XNC_SIDE_L'];
                if(isset($vals[$i]['attributes']['PART.SIDE_TO_CUT_B'])) $arr2[$i]['PART.SIDE_TO_CUT_T']=$vals[$i]['attributes']['PART.SIDE_TO_CUT_T'];
                if(isset($vals[$i]['attributes']['PART.SIDE_TO_CUT_B'])) $arr2[$i]['PART.SIDE_TO_CUT_B']=$vals[$i]['attributes']['PART.SIDE_TO_CUT_B'];
                if(isset($vals[$i]['attributes']['PART.SIDE_TO_CUT_R'])) $arr2[$i]['PART.SIDE_TO_CUT_R']=$vals[$i]['attributes']['PART.SIDE_TO_CUT_R'];
                if(isset($vals[$i]['attributes']['PART.SIDE_TO_CUT_L'])) $arr2[$i]['PART.SIDE_TO_CUT_L']=$vals[$i]['attributes']['PART.SIDE_TO_CUT_L'];
                // echo '<pre>'; print_r($vals[$i]['attributes']);                echo '</pre>'; ELTMAT
                if(isset($vals[$i]['attributes']['PART.MY_KL_ELT']) && $vals[$i]['attributes']['PART.MY_KL_ELT']==1 && isset($vals[$i]['attributes']['PART.MY_KL_ELT_CODE'])) { $arr2[$i]['MY_KL_ELT_CODE']=$vals[$i]['attributes']['PART.MY_KL_ELT_CODE']; $arr2[$i]['MY_KL_ELT_NAME']=$vals[$i]['attributes']['ELTMAT'];}
                if(isset($vals[$i]['attributes']['PART.MY_KL_ELL']) && $vals[$i]['attributes']['PART.MY_KL_ELL']==1 && isset($vals[$i]['attributes']['PART.MY_KL_ELL_CODE'])) { $arr2[$i]['MY_KL_ELL_CODE']=$vals[$i]['attributes']['PART.MY_KL_ELL_CODE']; $arr2[$i]['MY_KL_ELL_NAME']=$vals[$i]['attributes']['ELLMAT'];}
                if(isset($vals[$i]['attributes']['PART.MY_KL_ELR']) && $vals[$i]['attributes']['PART.MY_KL_ELR']==1 && isset($vals[$i]['attributes']['PART.MY_KL_ELR_CODE'])) { $arr2[$i]['MY_KL_ELR_CODE']=$vals[$i]['attributes']['PART.MY_KL_ELR_CODE']; $arr2[$i]['MY_KL_ELR_NAME']=$vals[$i]['attributes']['ELRMAT'];}
                if(isset($vals[$i]['attributes']['PART.MY_KL_ELB']) && $vals[$i]['attributes']['PART.MY_KL_ELB']==1 && isset($vals[$i]['attributes']['PART.MY_KL_ELB_CODE'])) { $arr2[$i]['MY_KL_ELB_CODE']=$vals[$i]['attributes']['PART.MY_KL_ELB_CODE']; $arr2[$i]['MY_KL_ELB_NAME']=$vals[$i]['attributes']['ELBMAT'];}

//                  if(isset($vals[$i]['attributes']['PART.MY_DOUBLE_TYPE'])) $arr2[$i]['PART.MY_DOUBLE_TYPE']=$vals[$i]['attributes']['PART.MY_DOUBLE_TYPE'];
                if(isset($vals[$i]['attributes']['ELT']) || isset($vals[$i]['attributes']['ELB']) || isset($vals[$i]['attributes']['ELL']) || isset($vals[$i]['attributes']['ELR']) )
                {
                    $arr2[$i]['edge']=1;
                }
                foreach ($vals as $k2=>$v2)
                {

                    if(isset($v2['tag']) && isset($v2['type']) && $v2['tag']=='OPERATION' && $v2['type']=='open' &&
                        isset($v2['attributes']['TYPEID']) && $v2['attributes']['TYPEID']=='XNC' &&
                       isset($vals[$k2+1]['attributes']['ID']) && $vals[$k2+1]['attributes']['ID']==$vals[$i]['attributes']['ID'])
                    {
                        $arr2[$i]['XNC']=$k2;
                        $arr2[$i]['XNC_COUNTMILL']=$v2['attributes']['COUNTMILL'];
                        $arr2[$i]['XNC_CODE']=$v2['attributes']['CODE'];


                    }


                }

            }
        }
    }
    // echo '<pre>'; print_r($arr2); echo '</pre>';
    foreach ($_materials as $k=>$v)
    {
        $materials.='<option value="'.$v['ID'].'" data-t="'.$v['T'].'" data-code="'.$v['CODE'].'">'.$v['NAME'].'</option>';
/*
             [arr_id] => 74
            [T] => 16
            [NAME] => ЛДСП-16 U112 PE Пепел (Пепельный)-КрУ
            [ID] => 13
            [CODE] => 1539
 */
    }

    $arr_parts= $arr2;
}
function get_arr_t_parts($arr_operation, $arr_sheets)
{
    $out=array();
    foreach ($arr_operation as $k => $v) {
        foreach ($v['PART'] as $k2 => $v2) {
            $out[$v2]['T']=$arr_sheets[$v['MATERIAL']]['T'];
            $out[$v2]['CODE']=$arr_sheets[$v['MATERIAL']]['CODE'];
        }
    }
    return $out;
}
function get_arr_operation($vals)
{
    $out = array();
    $tmp=array('start'=>0,'end'=>0,'id'=>0,'T'=>'', 'MATERIAL'=>0,'PART'=>array());
    foreach ($vals as $k => $v) {
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='OPERATION' && $v['type']=='open' &&
            isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='CS')
        {
            $tmp['start']=$k;
            $tmp['id']=$v['attributes']['ID'];
            $tmp['T']=$v['attributes']['T'];


        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='MATERIAL' && $v['type']=='complete' && $tmp['start']>0)
        {
            $tmp['MATERIAL']=$v['attributes']['ID'];
        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='PART' && $v['type']=='complete' && $tmp['start']>0)
        {
            $tmp['PART'][]=$v['attributes']['ID'];
        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='OPERATION' && $v['type']=='close' && $tmp['start']>0)
        {
            $tmp['end']=$k;
            $out[]=$tmp;
            $tmp=array('start'=>0,'end'=>0,'id'=>0,'T'=>'', 'MATERIAL'=>0,'PART'=>array());
        }

    }
    return $out;
}
function get_arr_sheets($vals)
{
    $out = array();
    $tmp=array('start'=>0,'end'=>0,'id'=>0,'T'=>'', 'MATERIAL'=>0,'PART'=>array());
    foreach ($vals as $k => $v) {
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='open' &&
            isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='sheet')
        {
            $out[$v['attributes']['ID']]=array('T'=>$v['attributes']['T'],'CODE'=>$v['attributes']['CODE']);


        }
    }
    return $out;
}
function get_arr_t_sheet($vals)
{
    $out=array();
    foreach ($vals as $k=>$v)
    {

        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='complete' && isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='tool.cutting')
        {
            if(isset($vals[$k+1]['tag']) && isset($vals[$k+1]['type']) && $vals[$k+1]['tag']=='GOOD' && $vals[$k+1]['type']=='open' &&
                 isset($vals[$k+1]['attributes']['TYPEID']) && $vals[$k+1]['attributes']['TYPEID']=='sheet' && isset($vals[$k+1]['attributes']['T']))
            {
                $out[$v['attributes']['ID']]=$vals[$k+1]['attributes']['T'];
        }


        }
    }
    return $out;
}
function create_data($main_text1)
{
    $main_text1=str_replace("\n","",$main_text1);

    $simple = $main_text1;
    $simple = xml_parser($simple);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $simple, $vals, $index);
    xml_parser_free($p);
    $_SESSION['vals']=$vals;
    $_SESSION['index']=$index;
}

function create_data_step2(&$sheet, &$band)
{
    $vals=$_SESSION['vals'];
    $index=$_SESSION['index'];


    $band_for_select='';
    $_band_for_select=array();
    foreach ($index['GOOD'] as $_good)
    {
        if(isset($vals[$_good]['attributes']) && isset($vals[$_good]['attributes']['TYPEID']) && ((int) $vals[$_good]['attributes']['CODE'])>0 &&
            ( $vals[$_good]['attributes']['TYPEID']=='sheet' || $vals[$_good]['attributes']['TYPEID']=='band' ) ) {
            switch ($vals[$_good]['attributes']['TYPEID'])
            {
                case 'sheet':
                    $sheet[$_good]=$vals[$_good]['attributes']['CODE'];
                    break;
                case 'band':
                    $_band_for_select[$vals[$_good]['attributes']['CODE']]=array('name' => $vals[$_good]['attributes']['NAME'], 'w' => $vals[$_good]['attributes']['W']);
                    $band[$_good]=$vals[$_good]['attributes']['CODE'];
                    break;
            }
        }
    }
    // p_($_band_for_select);
    foreach ($_band_for_select as $_k=>$_v)
    {
        $band_for_select.='<option value="'.$_k.'" data-w="'.$_v['w'].'">'.$_v['name'].'</option>';
    }
    $_SESSION['band_for_select']=$band_for_select;
}
function db_get_list_data_properties($typeop)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_data_properties start', $_SESSION);
    $nameDB='MATERIAL';
    if($typeop==2 || $typeop==4) $nameDB='BAND';

    $sql = "SELECT gp.*, gpb.* FROM
GOOD_PROPERTIES AS gp
INNER JOIN GOOD_PROPERTIES_{$nameDB} AS gpb ON gpb.PROPERTY_ID = gp.ID
WHERE
gp.ID IN ((SELECT DISTINCTROW 
GOOD_PROPERTIES_{$nameDB}.PROPERTY_ID FROM GOOD_PROPERTIES_{$nameDB}))
ORDER BY
gp.`NAME` ASC,
gpb.VALUEDIG ASC,
gpb.VALUESTR ASC" ;

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list_data_properties end', $_SESSION);
return $result;
}
function db_get_list2browse($filtr, $start, $limit, &$count_row, $typeop=1, $vart=0)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list2browse start', $_SESSION);
    $nameDB='MATERIAL';
    $optfield='m.L AS L';
    $fieldName='m.`NAME` ';
    if($typeop==2 || $typeop==4){
        $nameDB='BAND';
        $optfield='m.T AS T';
        $fieldName='m.KR_NAME AS NAME ';
    }
    $where='';
    $inners='';
    $_where=array();
    $counter=0;
    foreach ($filtr as $key=>$value)
    {
        $inners.=" LEFT JOIN GOOD_PROPERTIES_{$nameDB} AS prp{$counter} ON prp{$counter}.{$nameDB}_ID = m.{$nameDB}_ID ";
    $_where[]='(prp'.$counter.'.PROPERTY_ID = '.$key.' AND (prp'.$counter.'.VALUESTR in ("'.implode('", "',$value).'") OR prp'.$counter.'.VALUEDIG in ("'.implode('", "',$value).'")))';
        $counter++;
}
    if($vart>0)
    {
        $_vart=($vart*2)+3;
        $inners.=" LEFT JOIN GOOD_PROPERTIES_{$nameDB} AS vart ON vart.{$nameDB}_ID = m.{$nameDB}_ID ";
        $_where[]=" (vart.PROPERTY_ID = 2 AND  vart.VALUEDIG >{$_vart}) ";
   }


    if(count($_where)>0) $where=' WHERE '. implode(' AND ',$_where).' ';




    $sql = "SELECT COUNT(DISTINCT m.`CODE`) as cnt 
FROM
$nameDB AS m
LEFT JOIN GOOD_TREE_{$nameDB}_CONN AS t ON t.{$nameDB}_ID = m.{$nameDB}_ID
{$inners}
$where
" ;

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
if($result['data'][0]['cnt']==0)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list2browse end', $_SESSION);
    return $sql;
    return NULL;
}
    $sql = "SELECT DISTINCT 
m.{$nameDB}_ID AS OBJ_ID,
{$fieldName},
m.TYPENAME,
{$optfield},
m.W,
m.`CODE`,
m.COUNT,
m.COST,
m.UNIT  
FROM
{$nameDB} AS m
LEFT JOIN GOOD_TREE_{$nameDB}_CONN AS t ON t.{$nameDB}_ID = m.{$nameDB}_ID
{$inners} 
$where 
ORDER BY
m.`NAME` ASC LIMIT $start, $limit
" ;

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list2browse end', $_SESSION);
    return $result['data'];
}
function db_get_list2browse_pic($data, $typeop)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list2browse_pic start', $_SESSION);
    $nameDB='MATERIAL'; if($typeop==2 || $typeop==4)  $nameDB='BAND';
$_arr_whore=array();
    foreach ($data as $item)
    {
        $_arr_whore[]=$item['OBJ_ID'];
    }

    $sql = "SELECT DISTINCT {$nameDB}_ID AS OBJ_ID, FILE_NAME_BIG, FILE_NAME_SMALL,
SORT, DESCRIPTION FROM
{$nameDB}_PIC
WHERE
{$nameDB}_ID IN (".implode(',',$_arr_whore).")
ORDER BY SORT ASC" ;

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $out=array();
    foreach ($result['data'] as $item)
    {
        $out[$item['OBJ_ID']][]=array(
            'FILE_NAME_BIG'=>$item['FILE_NAME_BIG'],
            'FILE_NAME_SMALL'=>$item['FILE_NAME_SMALL'],
            'DESCRIPTION'=>$item['DESCRIPTION'],

        );
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list2browse_pic end', $_SESSION);
    return $out;
}

function get_list2browse($filtr, $start, $limit,$typeop=1, $vart=0, &$pics)
{
    $optfield='L'; if($typeop==2 || $typeop==4)  $optfield='T';
    $count_row=0;
    $data=db_get_list2browse($filtr, $start, $limit, $count_row,$typeop, $vart);

    $out='<param id="filtr-add-data" data-typeop="'.$typeop.'" data-vart="'.$vart.'">';
    $out.='<textarea id="filtr-browse" style="display: none;" >'.serialize($filtr).'</textarea>';
    $out.='<div style="max-height: 80vh; overflow: scroll;">';
    if($count_row==0)
    {
        $out.='<br>Данных нет</div>';
        $pics='';
        return $out;
    }
    $pictures=db_get_list2browse_pic($data, $typeop);

    foreach ($data as $item)
    {

        $_dotpos = (int)strpos($item['COST'], '.');
        $_cntD = $_dotpos > 0 ? strlen($item['COST']) - $_dotpos - 1 : 0;
        $cost=round($item['COST'],2);
        $unit=$item['UNIT'];
        if($_cntD>2)
        {
            $_diff=$_cntD-2;
            $cost=$item['COST']*pow(10,$_diff);
            $unit=pow(10,$_diff).' '.$item['UNIT'];
        }
        // echo $item['COST'].' = '.$cost.' = '.$newcost.' = '.pow(10,$_diff).' <br> ';


        $_data=$item['NAME'] . ' || ' .($item['COUNT']>20?'<span>в наличии</span>':'ограниченное количество') . ' || '. $cost . ' за ' . $unit;


        // $out.='<div class="data-list-line" data-toggle="popover" data-html=true data-placement="top" data-trigger="hover" title="'.$item['NAME'] .  '"
        $out.='<div class="data-list-line" data-html=true data-placement="top" title="'.$item['NAME'] .  '"
         data-content="'.$_data .  '" data-code="'.$item['CODE'].'"  data-w="'.$item['W'].'"> '.
             $item['NAME'] . ' {'.$item['TYPENAME'] .  ') '.$item[$optfield] . ' X '.$item['W'] . '</div>';

        $data_pics='';
        if(isset($pictures[$item['OBJ_ID']]) && count($pictures[$item['OBJ_ID']])>0)
        {


            foreach ($pictures[$item['OBJ_ID']] as $itempic)
            {
                $data_pics.='<a href="pic/'.$itempic['FILE_NAME_BIG'].'" title="'.$itempic['DESCRIPTION'].'"  class="rounded-sm zoom-images" target="_blank"><i></i><img src="pic/'.$itempic['FILE_NAME_SMALL'].'"></a>';
            }
        }

        $pics.='<div class="data-list-images images-'.$item['CODE'].'" style="display:none;">'.$data_pics.'</div>';

    }

    $out.='</div>';
    $out.=get_list2browse_pager( $start, $limit, $count_row);
    return $out;
}
function get_list2browse_pager( $start_in, $limit, $total_records)
{
    $start_in=$start_in+1;
    $links=2;
//    https://code.tutsplus.com/ru/tutorials/how-to-paginate-data-with-php--net-2928

    $last       = ceil($total_records / $limit );

    $start      = ( ( $start_in - $links ) > 0 ) ? $start_in - $links : 1;
    $end        = ( ( $start_in + $links ) < $last ) ? $start_in + $links : $last;

    $html       = '<nav aria-label="Page navigation example">  <ul class="pagination pagination-sm justify-content-center">';

    $class      = ( $start_in == 1 ) ? "disabled" : "";
    $html       .= '<li class="page-item ' . $class . '"><span class="page-link" data-pagego="' . ( $start_in - 1 ) . '">&laquo;</span></li>';

    if ( $start > 1 ) {
        $html   .= '<li class="page-item"><span class="page-link"  data-pagego="1">1</span></li>';
        $html   .= '<li class="page-item disabled"><span class="page-link" >...</span></li>';
    }

    for ( $i = $start ; $i <= $end; $i++ ) {
        $class  = ( $start_in == $i ) ? "active" : "";
        $html   .= '<li class="page-item ' . $class . '"><span class="page-link"  data-pagego="' . $i . '">' . $i . '</span></li>';
    }

    if ( $end < $last ) {
        $html   .= '<li class="page-item disabled"><span class="page-link" >...</span></li>';
        $html   .= '<li class="page-item"><span class="page-link" data-pagego="' . $last . '">' . $last . '</span></li>';
    }

    $class      = ( $start_in == $last ) ? "disabled" : "";
    $html       .= '<li class="page-item ' . $class . '"><span class="page-link"  data-pagego="' . ( $start_in + 1 ) . '">&raquo;</span></li>';

    $html       .= '</ul></nav>';
    return $html;
}




function al2b_get_folders($where, $mode, &$ret=array(),&$active=0)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_get_folders start', $_SESSION);
    $sql = "SELECT
GOOD_TREE.FOLDER_ID,
GOOD_TREE.`NAME`,
GOOD_TREE.PARENT,
GOOD_TREE.1CCODE,
GOOD_TREE.`CODE`
FROM
GOOD_TREE
WHERE
GOOD_TREE.PARENT IN (".implode(',',$where).")";
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $out='<ul class="list-group mode-'.$mode.'">'; $is_first=1;
    foreach ($result['data'] as $row) {
        $disp_children='';$is_active='';
        if($mode==0){ $ret[]=$row['FOLDER_ID']; if($is_first) { $is_active=' active '; $active=$row['FOLDER_ID'];}} else {
            if($active!=$row['PARENT']) $disp_children=' style="display:none;" ';

        }

        $out .= '<li class="list-group-item '.$is_active.'" '.
            ($mode==0?' data-children="'.$row['FOLDER_ID'].'" ':' data-target="'.$row['FOLDER_ID'].'"  data-parent="'.$row['PARENT'].'" '.$disp_children.' ').
            '>'.$row['NAME'].'</li>';
        $is_first=0;
    }
    $out .= "</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_get_folders end', $_SESSION);
    return $out;
}

function al2b_get_all_folders(&$where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_get_all_folders start', $_SESSION);
    $sql = "SELECT GOOD_TREE.FOLDER_ID FROM GOOD_TREE WHERE PARENT IN (".implode(',',$where).")";
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $_cnt=count($where);
    foreach ($result['data'] as $row) {
        $where[$row['FOLDER_ID']]=$row['FOLDER_ID'];
    }
    if($_cnt<count($where)) al2b_get_all_folders($where);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_get_all_folders end', $_SESSION);
}
function al2b_get_type_from_folders($where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_get_type_from_folders start', $_SESSION);
    $sql = "SELECT IF( COUNT(*)>0,'BAND','') as tname FROM GOOD_TREE_BAND_CONN
WHERE GOOD_TREE_BAND_CONN.FOLDER_ID IN (".implode(',',$where).")
UNION 
SELECT IF( COUNT(*)>0,'MATERIAL','') as tname FROM GOOD_TREE_MATERIAL_CONN 
WHERE GOOD_TREE_MATERIAL_CONN.FOLDER_ID IN (".implode(',',$where).") 
UNION 
SELECT IF( COUNT(*)>0,'SIMPLE','') as tname FROM GOOD_TREE_SIMPLE_CONN 
WHERE GOOD_TREE_SIMPLE_CONN.FOLDER_ID IN (".implode(',',$where).") ";

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $out='';
    foreach ($result['data'] as $row) {
        if(strlen($row['tname'])>3) $out=$row['tname'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_get_type_from_folders end', $_SESSION);
    return $out;
}

function al2b_db_get_list_data_properties($nameDB, $where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list_data_properties start', $_SESSION);
    $_where=''; if(is_array($where) && count($where)) $_where=" WHERE GOOD_TREE_{$nameDB}_CONN.FOLDER_ID IN (".implode(',',$where).") ";
    $sql = "SELECT gp.*, gpb.* FROM
GOOD_PROPERTIES AS gp
INNER JOIN GOOD_PROPERTIES_{$nameDB} AS gpb ON gpb.PROPERTY_ID = gp.ID
WHERE
gp.ID IN (SELECT DISTINCT
GOOD_PROPERTIES_{$nameDB}.PROPERTY_ID 
FROM
GOOD_PROPERTIES_{$nameDB}
INNER JOIN GOOD_TREE_{$nameDB}_CONN ON GOOD_TREE_{$nameDB}_CONN.{$nameDB}_ID = GOOD_PROPERTIES_{$nameDB}.{$nameDB}_ID 
$_where 
) AND LEFT(gp.NAME,1)<>'!' 
ORDER BY
gp.`NAME` ASC,
gpb.VALUEDIG ASC,
gpb.VALUESTR ASC" ;

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list_data_properties end', $_SESSION);
    return $result['data'] ;
}
function al2b_create_screen($where, $type)
{
$out='';
    $tablerows= al2b_db_get_list_data_properties($type, $where);

    $arr_prop_ids=array();
    $arr_prop_name=array();
    foreach ($tablerows as $row)
    {
        $arr_prop_ids[$row['PROPERTY_ID']][$row['VALUESTR'].$row['VALUEDIG']]=$row['VALUESTR'].$row['VALUEDIG'];
        $arr_prop_name[$row['PROPERTY_ID']] =array('NAME'=>$row['NAME'], 'DESCRIPTION'=>$row['DESCRIPTION']);
    }
    $out.='<div class="accordion" id="accordionExample">';

    foreach ($arr_prop_ids as $key=>$vsl) {

        $out.= '<div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                        <button class="btn btn-secondary btn-sm btn-block" type="button" data-toggle="collapse" data-target="#clps-'.$key.'" aria-expanded="false" aria-controls="clps-'.$key.'">
                        '.$arr_prop_name[$key]['NAME'].'
                        </button>
                      </h2>
                    </div>
                
                    <div id="clps-'.$key.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body" style="max-height: 80vh; overflow: scroll;">';
        $counter=0;
        foreach ($vsl as $items) {
            $out.= '<div class="form-check">
                  <input class="form-check-input" type="checkbox" value="'.$items.'" id="check-'.$key.'-'.$counter.'">
                  <label class="form-check-label" for="check-'.$key.'-'.$counter.'">'.$items.'</label></div>';
            $counter++;
        }
        $out.= '</div>
            </div>
          </div>';
    }
    $out.= '</div>';
    return $out;
}
function al2b_db_get_list2browse($filtr, $start, $limit, &$count_row, $nameDB='MATERIAL', $where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list2browse start', $_SESSION);
    $optfield2=' m.TYPENAME, m.W, m.COUNT, ';
    $optfield='m.L AS L, m.T AS T, m.W AS W, ';
    if($nameDB=='BAND'){
        $optfield='m.T AS T, m.W AS W, ';
    }

    if($nameDB=='SIMPLE'){
        $optfield=' ';
        $optfield2=' ';
    }
    $where=' WHERE t.FOLDER_ID IN ('. implode(', ',$where).') ';
    $inners='';
    $_where=array();
    $counter=0;
    foreach ($filtr as $key=>$value)
    {
        $inners.=" LEFT JOIN GOOD_PROPERTIES_{$nameDB} AS prp{$counter} ON prp{$counter}.{$nameDB}_ID = m.{$nameDB}_ID ";
        $_where[]='(prp'.$counter.'.PROPERTY_ID = '.$key.' AND (prp'.$counter.'.VALUESTR in ("'.implode('", "',$value).'") OR prp'.$counter.'.VALUEDIG in ("'.implode('", "',$value).'")))';
        $counter++;
    }

    if(count($_where)>0) $where.=' ANd '. implode(' AND ',$_where).' ';



    $sql = "SELECT COUNT(DISTINCT m.`CODE`) as cnt 
FROM
$nameDB AS m
LEFT JOIN GOOD_TREE_{$nameDB}_CONN AS t ON t.{$nameDB}_ID = m.{$nameDB}_ID
{$inners}
$where
" ;
   // echo $nameDB.$sql; exit();
   // echo $type.'<pre>'; print_r($where); echo '</pre>'; exit();
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $count_row=$result['data'][0]['cnt'];
    if($count_row==0)
    {
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list2browse end', $_SESSION);
        return NULL;
    }
    $start=$start*$limit;
    $sql = "SELECT DISTINCT 
m.{$nameDB}_ID AS OBJ_ID,
m.`NAME`,
{$optfield}
{$optfield2} 
m.`CODE`,
m.COST,
m.UNIT  
FROM
{$nameDB} AS m
LEFT JOIN GOOD_TREE_{$nameDB}_CONN AS t ON t.{$nameDB}_ID = m.{$nameDB}_ID
{$inners} 
$where 
ORDER BY
m.`NAME` ASC LIMIT $start, $limit
" ;

    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list2browse end', $_SESSION);
    return $result['data'];
}

function al2b_get_list2browse($filtr, $start, $limit, &$pics, &$where, &$type, &$count_row)
{

    if(strlen($type)==0) {
        $where = array($where);
        al2b_get_all_folders($where);
        $type = al2b_get_type_from_folders($where);
    }
//   echo $type.'<pre>'; print_r($where); echo '</pre>'; exit();


  if(strlen($type)>3)  $data=al2b_db_get_list2browse($filtr, $start, $limit, $count_row,$type,  $where);
   // echo $type.'<br>'.$count_row.'<br>'; print_r($data); echo '</pre>'; exit();

 //   echo $count_row.'<pre>'; print_r($data); echo '</pre>'; exit();
    $out='<param id="filtr-add-data" data-type="'.$type.'" >';
    $out.='<textarea id="filtr-browse" style="display: none;" >'.serialize($filtr).'</textarea>';
    $out.='<textarea id="filtr-browse-parent" style="display: none;" >'.serialize($where).'</textarea>';
    $out.='<div style="max-height: 80vh; overflow: scroll;">';
    if($count_row==0)
    {
        $out.='<br>Данных нет</div>';
        $pics='';
        return $out;
    }
    $pictures=al2b_db_get_list2browse_pic($data, $type);

    foreach ($data as $item)
    {

        $_dotpos = (int)strpos($item['COST'], '.');
        $_cntD = $_dotpos > 0 ? strlen($item['COST']) - $_dotpos - 1 : 0;
        $cost=round($item['COST'],2);
        $unit=$item['UNIT'];
        if($_cntD>2)
        {
            $_diff=$_cntD-2;
            $cost=$item['COST']*pow(10,$_diff);
            $unit=pow(10,$_diff).' '.$item['UNIT'];
        }
        // echo $item['COST'].' = '.$cost.' = '.$newcost.' = '.pow(10,$_diff).' <br> ';


        $_data=$item['NAME'] . ' || ' .($item['COUNT']>20?'<span>в наличии</span>':'ограниченное количество') . ' || '. $cost . ' грн. за ' . $unit;

        $optfield='';
        if($type=='MATERIAL') $optfield=$item['L'] . ' X '.$item['W'] ;
        if($type=='BAND') $optfield=$item['T'] . ' X '.$item['W'] ;
        if($type=='MATERIAL' || $type=='BAND') $optfieldtn=' {'.$item['TYPENAME'] .  ') ';

        switch ($type)
        {
            case 'MATERIAL':
                $_desc=$item['L'].' X '.$item['W'].' X '.$item['T'].'<br>Плитные материалы<br>'.$item['NAME'];
                break;
            case 'BAND':
                $_desc=$item['W'].' X '.$item['T'].'<br>Кромочные материалы<br>'.$item['NAME'];
                break;
            case 'SIMPLE':
                $_desc='Мебельная фурнитура<br>'.$item['NAME'];
                break;
        }
        $data_img='';
        $data_pics='';
        if(isset($pictures[$item['OBJ_ID']]) && count($pictures[$item['OBJ_ID']])>0)
        {


            foreach ($pictures[$item['OBJ_ID']] as $itempic)
            {
                if(strlen($data_img)==0) $data_img=$itempic['FILE_NAME_SMALL'];
                $data_pics.='<a href="pic/'.$itempic['FILE_NAME_BIG'].'" title="'.$itempic['DESCRIPTION'].'"  class="rounded-sm zoom-images" target="_blank"><i></i><img src="pic/'.$itempic['FILE_NAME_SMALL'].'"></a>';
            }
        }

        $pics.='<div class="data-list-images images-'.$item['CODE'].'" style="display:none;">'.$data_pics.'</div>';


        $out.='<div class="data-list-line" data-html=true data-placement="top" title="'.$item['NAME'] .  '"
         data-content="'.$_data .  '" data-code="'.$item['CODE'].'"  data-cost="'.$item['COST'].'" data-img="'.$data_img.'"  data-addesc="'.$_desc.'"  data-w="'.$item['W'].'"> '.
            $item['NAME'] .$optfieldtn.$optfield. '</div>';


    }

    $out.='</div>';
    $out.=get_list2browse_pager( $start, $limit, $count_row);
    return $out;
}
function al2b_db_get_list2browse_pic($data, $type)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list2browse_pic start', $_SESSION);
    $nameDB=$type;
    $_arr_whore=array();
    foreach ($data as $item)
    {
        $_arr_whore[]=$item['OBJ_ID'];
    }
    $sql = "SELECT DISTINCT {$nameDB}_ID AS OBJ_ID, FILE_NAME_BIG, FILE_NAME_SMALL,
SORT, DESCRIPTION FROM
{$nameDB}_PIC
WHERE
{$nameDB}_ID IN (".implode(',',$_arr_whore).")
ORDER BY SORT ASC" ;


    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $out=array();
    foreach ($result['data'] as $item)
    {
        $out[$item['OBJ_ID']][]=array(
            'FILE_NAME_BIG'=>$item['FILE_NAME_BIG'],
            'FILE_NAME_SMALL'=>$item['FILE_NAME_SMALL'],
            'DESCRIPTION'=>$item['DESCRIPTION'],

        );
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_list2browse_pic end', $_SESSION);
    return $out;
}
function al2b_db_get_simpleslist($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_simpleslist start', $_SESSION);
    $where=implode(',',$in);
    $sql = "SELECT
BAND.`CODE`,'BAND' as typee, CONCAT(BAND.T,' X ',BAND.W) as newdesc, 
(SELECT  
BAND_PIC.FILE_NAME_SMALL 
FROM
BAND_PIC
WHERE
BAND_PIC.BAND_ID = BAND.BAND_ID
ORDER BY
BAND_PIC.SORT ASC
LIMIT 0,1) as pic 
FROM
BAND
WHERE
BAND.`CODE` IN ($where) 
UNION 
SELECT
MATERIAL.`CODE`,'MATERIAL' as typee,  CONCAT(MATERIAL.L,' X ',MATERIAL.T,' X ',MATERIAL.W) as newdesc,
(SELECT  
MATERIAL_PIC.FILE_NAME_SMALL 
FROM
MATERIAL_PIC
WHERE
MATERIAL_PIC.MATERIAL_ID = MATERIAL.MATERIAL_ID
ORDER BY
MATERIAL_PIC.SORT ASC
LIMIT 0,1) as pic  
FROM
MATERIAL
WHERE
MATERIAL.`CODE` IN ($where)

UNION 
SELECT
SIMPLE.`CODE`,'SIMPLE' as typee, '' as newdesc,
(SELECT  
SIMPLE_PIC.FILE_NAME_SMALL 
FROM
SIMPLE_PIC
WHERE
SIMPLE_PIC.SIMPLE_ID = SIMPLE.SIMPLE_ID
ORDER BY
SIMPLE_PIC.SORT ASC
LIMIT 0,1) as pic  
FROM
SIMPLE
WHERE
SIMPLE.`CODE` IN ($where)" ;
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $out=array();
    foreach ($result['data'] as $row) {
        $out[$row['CODE']]=$row;
    }

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'al2b_db_get_simpleslist end', $_SESSION);
    return $out;
}
function db_get_simple_price_new($in, &$_simple_price_new, $_get_simple_cnt)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_simple_price_new start', $_SESSION);
    $_wgere=implode(', ',$in);
    $sql = "
SELECT BAND.TYPEID, BAND.`CODE`, BAND.`NAME`, BAND.T, BAND.W, '' AS L, BAND.COST 
FROM  BAND
WHERE BAND.`CODE` IN ($_wgere)
UNION 
SELECT MATERIAL.TYPEID, MATERIAL.`CODE`, MATERIAL.`NAME`, MATERIAL.T, MATERIAL.W, MATERIAL.L, MATERIAL.COST 
FROM MATERIAL
WHERE MATERIAL.`CODE` IN ($_wgere)
UNION 
SELECT SIMPLE.TYPEID, SIMPLE.`CODE`, SIMPLE.`NAME`, '' AS T, '' AS W, '' AS L, SIMPLE.COST
FROM SIMPLE
WHERE SIMPLE.`CODE` IN ($_wgere)
";
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    foreach ($result['data'] as $row) {
        $_desc = '';
        switch ($row['TYPEID']) {
            case 'sheet':
                $_desc = $row['NAME'] . '  ' . $row['L'] . ' X ' . $row['W'] . ' X ' . $row['T'];
                $_simple_price_new[]=array(
                    'Name' => $_desc,
                    'Code' => $row['CODE'],
                    'count' =>$_get_simple_cnt[(int) $row['CODE']],
                    'price' =>$row['NAME'],
                    'material_code' => $row['CODE'],
                    'sheet_count' =>$_get_simple_cnt[(int) $row['CODE']],
                    'sheet_price' =>$row['COST'],
                    'type' => 'material'
                );
                break;
            case 'band':
                $_desc = $row['NAME'] . '  ' . $row['W'] . ' X ' . $row['T'];
                $_simple_price_new[]=array(
                    'Name' => $_desc,
                    'Code' => $row['CODE'],
                    'count' =>$_get_simple_cnt[(int) $row['CODE']],
                    'price' =>$row['NAME'],
                    'band_code' => $row['CODE'],
                    'band_count' =>$_get_simple_cnt[(int) $row['CODE']],
                    'band_price' =>$row['COST'],
                    'type' => 'band'
                );
                break;
            case 'simple':
                $_desc = $row['NAME'];
                $_simple_price_new[]=array(
                    'Name' => $_desc,
                    'Code' => $row['CODE'],
                    'count' =>$_get_simple_cnt[(int) $row['CODE']],
                    'price' =>$row['NAME'],
                    'simple_code' => $row['CODE'],
                    'simple_count' =>$_get_simple_cnt[(int) $row['CODE']],
                    'simple_price' =>$row['COST'],
                    'type' => 'simple'
                );
                break;
        }

    }

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_simple_price_new end', $_SESSION);

}