<?php

require_once '_1/config.php';
require_once DIR_CORE . 'func.php';
require_once DIR_CORE . "settings.php";

if (!$vals)
{
    // $session_id = $_SESSION['session_id'];
    // $data = get_temp_file_data($session_id);
    $vals=$_SESSION['vals'];
    $vals=vals_out($vals);
    //print_r_($vals);
    $index=$_SESSION['index'];
    $index=make_index($vals);
    //print_r_($index); 
}

// $error = '';
// ##########################################################################################################
// Ищем пазы в торце и если находим, то выводим недопустимую ошибку.
// Пазы в торце определяются в func_math.php, в функции import_vp(), по признаку сторон 1/6 - лицо/изнанка, остальные - торцы.
// Если были найдены операции с пазами в торце, то их идентификаторы и имена записываются в массив сессии,
// по ключу $_SESSION['tec_info']['grooving_errors']
$bad_parts = [];
if (isset($_SESSION['tec_info']['grooving_errors'])) {
    $error_header = '';
    $tmp = '';
    $error_header .= '<br>[/0166] При импорте проектов из VP пазы в торце не импортируются, обратитесь к обработчикам с предоставлением исходных файлов и чертежей на детали с пазами торце, для передачи в производство, если Вам нужны детали с пазами в торце!<br>Перечень деталей с пазами в торце:';
    foreach ($_SESSION['tec_info']['grooving_errors'] as $value) {
        foreach ($index['OPERATION'] as $key => $val) {
            if (isset($vals[$val]['attributes']['ID']) && $vals[$val]['attributes']['ID'] == $value['ID']) {
                // Строка ниже нужна только для того, чтобы ниже проверить массив "$bad_parts" и если он пустой
                // и операций в нём нет, удалить соответствующий ключ сессии
                $bad_parts['grooving_errors'][] = '<br><b>' . $vals[$val]["attributes"]["NAME"] . '</b>';
                $tmp .= '<br><b>' . $vals[$val]["attributes"]["NAME"] . '</b><br>';
            }
        }
    }
    if (!empty($tmp)) {
        $error .= $error_header;
        $error .= $tmp;
    }
}
//.. 21.06.2021 Добавлен новый блок, врезные ручки
// Такой опции в Кронасе нет, так что сразу записываем в исключения
if (isset($_SESSION['tec_info']['borderAndRabbets_errors'])) {
    $error_header = '';
    $tmp = '';
    $error_header .= '<br>[/0167] При загрузке проекта из VP выявлены детали с не импортируемыми обработками, обратитесь к обработчикам для оценки возможности выполнения обработки (фрезеровки под врезные ручки или профиля не KRONAS не выполняем)!<br>Перечень деталей с не импортируемыми обработками:';
    foreach ($_SESSION['tec_info']['borderAndRabbets_errors'] as $value) {
        foreach ($index['OPERATION'] as $key => $val) {
            if (isset($vals[$val]['attributes']['ID']) && $vals[$val]['attributes']['ID'] == $value['ID']) {
                $bad_parts['borderAndRabbets_errors'][] = '<br><b>' . $vals[$val]["attributes"]["NAME"] . '</b>';
                $tmp .= '<br><b>' . $vals[$val]["attributes"]["NAME"] . '</b><br>';
            }
        }
    }
    if (!empty($tmp)) {
        $error .= $error_header;
        $error .= $tmp;
    }
}

if (!isset($bad_parts['grooving_errors'])) {
    unset($_SESSION['tec_info']['grooving_errors']);
}
if (!isset($bad_parts['borderAndRabbets_errors'])) {
    unset($_SESSION['tec_info']['borderAndRabbets_errors']);
}
if (isset($bad_parts['grooving_errors']) || isset($bad_parts['borderAndRabbets_errors'])) {
    $error .= '<br>';
}
unset($bad_parts);
// ##########################################################################################################

$pd = gl_recalc(__LINE__,__FILE__,__FUNCTION__, vals_index_to_project($vals) );
$new_vals = get_vals_index_without_session($pd);
$vals = vals_out($new_vals['vals']);
ksort($vals);
$vals=add_tag_print($vals);
// xml ($vals);
$index=make_index($vals);
$sql='SELECT * FROM DOUBLE_TYPE';
$sql2= mysqli_query($link,$sql);
while ($sql3=mysqli_fetch_array($sql2))
{
    $double_edge[$sql3['DOUBLE_TYPE_ID']]=$sql3;
//   p_($sql3);
}
if (!$_POST["user_type_edge"])
{
    if (!$_SESSION["user_type_edge"]) $user_type_edge="eva";
    else $user_type_edge=$_SESSION["user_type_edge"];
}
else $user_type_edge=$_POST["user_type_edge"];
if (!$user_type_edge) $user_type_edge=$_SESSION["user_type_edge"];

if (!$_POST["user_place"])
{
    if (!$_SESSION["user_place"]) $place=1;
    else $place=$_SESSION["user_place"];
}
else $place=$_POST["user_place"];
if (!$place) $place=$_SESSION["place"];
// echo $place;
$sql="select NAME from PLACES wher PLACES_ID=".$place;
$sql=mysqli_query($link,$sql);
$sql2=mysqli_fetch_array($sql);
foreach ($index["GOOD"] as $df)
{
    if (($vals[$df]["attributes"]["TYPEID"]=="band")&&($vals[$df]["attributes"]["CODE"]>0))
    {
        $sqlr="select MY_LASER from BAND where CODE=".$vals[$df]["attributes"]["CODE"].";";
        $sqlr=mysqli_query($link,$sqlr);
        $sqlr2=mysqli_fetch_array($sqlr);
        if ($sqlr2["MY_LASER"]==1)
        {
            $band_laz[]=$vals[$df]["attributes"]["NAME"];
        }
    }
}
$sql="select * from TOOL_EDGELINE_EQUIPMENT where PLACE=".$place.";";
// echo $sql;
$sql=mysqli_query($link,$sql);
while ($sql=mysqli_fetch_array($sql))
{
    $sql1[]=$sql;
}
foreach($sql1 as $sql)
{
    if ($sql["pur"]==1) $pur="1";
    if ($sql["laz"]==1) $laz="1";

}
if ($user_type_edge<>"eva")
{
    if($user_type_edge=="pur")
    {
        if ($pur<>1)
        {
            $error.="<br>[/00131] PUR кромкование не доступно для участка ".$sql2["NAME"].", выберите пожта иной участок, где доступен PUR.<br>";
        }
    }
    elseif($user_type_edge=="laz")
    {
        if (($band_laz)AND($laz<>1))
        {
            $error.="<br>[/00132] Лазерное кромкование не доступно для участка ".$sql2["NAME"].", выберите пожта иной участок, где доступно лазерное кромкование.<br>
            Кромки в проекте для обработки лазером:<br>";
            foreach($band_laz as $bl)
            {
                $error.=$bl."<br>";
            }
            // exit('qq');

        }
    }
}
//.. 05.07.2021 Добавлен диаметр 3.2
$v412_diam_mill=array(20,12,8,6,4,'3.2');
$v412_diam_mill_depth=array('3.2'=>10,4=>10,6=>22,8=>22,12=>25,20=>45);
if (!isset($check_index))
{
    if (isset($error_ok)) $error_ok.=check_stock($vals,$link,$place);
    else $error_ok=check_stock($vals,$link,$place);
}
// xml ($vals);
$error.=check_art($vals,$link,$place);
$vals=vals_out($vals);
ksort($vals);
$index=make_index($vals);
$vals=add_tag_print($vals);
// xml($vals);

foreach($index["PART"] as $i12)
{
    if(($vals[$i12]["attributes"]["CW"]>0)&&($vals[$i12]["attributes"]["CL"]>0))
    {
        $part++;
        $e4=get_cs_tool__sheet_part($vals,$vals[$i12]["attributes"]["ID"]);
        $part=get_all_part($vals,$vals[$i12]['attributes']['ID']);
        // p_($part);
        // xml($vals);
        if ($vals[$i12]["attributes"]["COUNT"]==0)
        {
            
            $error.="<br>Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$e4['sheet']['attributes']['NAME']."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - количество деталей равно 0. Либо удалите деталь, либо поставьте количество более 0.
            <br>";
        }
        if($e4['cs']['attributes']['CTRIMW']>0) $sheet_l=$e4['sheet']['attributes']['L']-$e4['cs']['attributes']['CTRIMW']*2-$e4['tool']['attributes'][mb_strtoupper('swSawthick')]*2;
        else  $sheet_l=$e4['sheet']['attributes']['L'];
        if($e4['cs']['attributes']['CTRIML']>0) $sheet_w=$e4['sheet']['attributes']['W']-$e4['cs']['attributes']['CTRIML']*2-$e4['tool']['attributes'][mb_strtoupper('swSawthick')]*2;
        else $sheet_w=$e4['sheet']['attributes']['W'];
        if (($e4['sheet']['attributes']['CODE']>0)&&(is_numeric($e4['sheet']['attributes']['CODE'])))
        {
            unset ($no_ch);
            $sql='SELECT * FROM MATERIAL WHERE CODE='.$e4['sheet']['attributes']['CODE'];
            // echo $sql;exit;
            $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['MY_1C_NOM'];
            if ($mat==5931) $no_ch=1;
        }
        if ((($vals[$i12]["attributes"]["CW"]>$sheet_w)OR($vals[$i12]["attributes"]["CL"]>$sheet_l))&&(!isset($no_ch)))
        {
            // p_($e4);
        // test ('w',$sheet_w);
        // test ('l',$sheet_l);
            $r12=$e4['sheet_index']+1;
            unset($ok_size_part);
            while($vals[$r12]['tag']=="PART")
            {
                if ((($vals[$i12]["attributes"]["CL"]<$vals[$r12]['attributes']['L'])AND($vals[$i12]["attributes"]["CW"]<$vals[$r12]['attributes']['W']))OR
                ((($vals[$i12]["attributes"]["CW"]<$vals[$r12]['attributes']['L'])AND($vals[$i12]["attributes"]["CL"]<$vals[$r12]['attributes']['W'])AND($vals[$i12]["attributes"]["TXT"]=="false"))))
                {
                    $ok_size_part=1;
                } 
                $r12++;
            }
            if ((!isset($ok_size_part))&&(count($part['cl'])<1))
            {
            //   p_($e4);
                $error.="<br>Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$e4['sheet']['attributes']['NAME']."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - размер детали больше чем размер листового материала после торцовки (".$e4['sheet']['attributes']['NAME']." - ".$sheet_l." мм * ".$sheet_w." мм), из которого она делается. Проверьте размеры введённых материалов или измените размер детали.
                <br>";
            }
        }
    }
}

if ($part<1)
{
    // xml ($vals);
    if (!$part) $part="0";
    if (!$product) $product="0";
    $error.="<br>[/00130] В проекте должна быть минимально хотя бы одна деталь. В данном проекте деталей ".$part.". Возможно, предидущие проверки удалили часть деталей (см. \"допустимые ошибки\" ниже.
    Вернитесь пожалуста к редактированию деталей на предидущей странице.<br>";
}
unset ($product,$part);
$index=make_index($vals);
foreach ($index['GOOD'] as $i)
{
    if (($vals[$i]["attributes"]["TYPEID"]=="sheet")&&($vals[$i]["attributes"]["good.my_client_material"]==1))
    {
        $mat_out[$vals[$i]["attributes"]["good.my_client_material_id"]]=1;
    }
}


// xml($vals);

foreach($index["PART"] as $i12)
{
    
    if(($vals[$i12]["attributes"]["CW"]>0)&&($vals[$i12]["attributes"]["CL"]>0))
    {
        // p_($vals[$i12]);
        $tr=$vals[$i12]["attributes"]["ID"];
        foreach ($index["OPERATION"]as $i)
        {
            if (($vals[$i]["type"]=="open")&&($vals[$i]["attributes"]["TYPEID"]=="CS"))
            {
                $j=$i+2;
                while ($vals[$j]["tag"]=="PART")
                {
                    if($vals[$j]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"])
                    {
                        // p_($vals[$i]);
                        
                        foreach ($index["GOOD"] as $r)
                        {
                            if (($vals[$r]["attributes"]["TYPEID"]=="tool.cutting")AND($vals[$i]["attributes"]["TOOL1"]==$vals[$r]["attributes"]["ID"]))
                            {
                                $swsawthick=$vals[$r]["attributes"]["SWSAWTHICK"];
                            }
                            elseif (($vals[$r]["attributes"]["TYPEID"]=="sheet")AND($vals[$i+1]["attributes"]["ID"]==$vals[$r]["attributes"]["ID"]))
                            {
                                $t12=$vals[$r]["attributes"]["T"];
                                $vals[$i12]["attributes"]["T"]=$t12;
                                $l12=$vals[$r]["attributes"]["L"];
                                $w12=$vals[$r]["attributes"]["W"];
                                $n12=$vals[$r]["attributes"]["NAME"];
                                $y=$r+1;
                                while ($vals[$y]["tag"]=="PART")
                                {
                                    if ($vals[$y]["attributes"]["L"]>$l12) $l12=$vals[$y]["attributes"]["L"];
                                    if ($vals[$y]["attributes"]["W"]>$w12) $w12=$vals[$y]["attributes"]["W"];
                                    $y++;
                                }
                            }
                        }
                        $ctriml=$vals[$i121]["attributes"]["CTRIML"];
                        $ctrimw=$vals[$i121]["attributes"]["CTRIMW"];
                        if ($ctriml>0) $triml=$ctriml*2+$swsawthick*2;
                        else $triml=0;
                        if ($ctrimw>0) $trimw=$ctrimw*2+$swsawthick*2;
                        else $trimw=0;
                        //test ("trim",$trim);
                    }
                    $j++;
                }
            }

        }
        // if (!isset($l12)) p_("!!");
        // else p_($l12);
        unset($i121,$i1211);
        foreach ($index["OPERATION"]as $i32)
        {
            
            if ($vals[$i32]["attributes"]["TYPEID"]=="GR")
            {
                $i1211=$i32+1;
                if ($vals[$i1211]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"]) 
               {
                    $gr12=$vals[$i32]["attributes"]["GRDEPTH"];
                    $gr112=$vals[$i32]["attributes"]["GRWIDTH"];
               }
            }
        }
        unset($i121,$i1211,$i32);
        $iddd=0;
        // unset()
        foreach ($index["OPERATION"]as $i33)
        {
            
            if (($vals[$i33]["attributes"]["TYPEID"]==="XNC")AND($vals[$i33]["type"]=="open"))
            {
                if ($vals[$i33+1]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"])
                {
                    $pr12=$vals[$i33]["attributes"]["PROGRAM"];
                    $r = xml_parser_create();
                    xml_parse_into_struct($r, $pr12, $v412[$tr][$iddd], $i412[$tr][$iddd]);
                    xml_parser_free($r);
                    $tr=$vals[$i12]["attributes"]["ID"];

                    if(($vals[$i33]["attributes"]["TURN"]==1)OR($vals[$i33]["attributes"]["TURN"]==3))
                    {
                        if(($v412[$tr][$iddd][0]["attributes"]["DX"]<>$vals[$i12]['attributes']['CW'])&&
                        ($v412[$tr][$iddd][0]["attributes"]["DX"]<>$vals[$i12]['attributes']['DW'])
                        &&(($v412[$tr][$iddd][0]["attributes"]["DX"]==$vals[$i12]['attributes']['CL'])OR
                        ($v412[$tr][$iddd][0]["attributes"]["DX"]==$vals[$i12]['attributes']['DL'])))
                        {
                            $t121_dl=$v412[$tr][$iddd][0]["attributes"]["DX"];
                            $v412[$tr][$iddd][0]["attributes"]["DX"]= $v412[$tr][$iddd][0]["attributes"]["DY"];
                            $v412[$tr][$iddd][0]["attributes"]["DY"]=$t121_dl;
                            $turn_in=1;
                        }
                        $turn=$vals[$i33]["attributes"]["TURN"];
                        $turn_text="(поворот детали = ".($turn*90)." градусов)";
                        // else
                        // {
                        //     if ((!$i412[$tr][$i]["MS"])AND(!$i412[$tr][$i]["ML"])AND(!$i412[$tr][$i]["MA"])AND(!$i412[$tr][$i]["MAC"]))
                        //     {
                        //         $v412[$tr][$iddd][0]["attributes"]["DX"]=$vals[$i12]['attributes']['DL'];
                        //         $v412[$tr][$iddd][0]["attributes"]["DY"]=$vals[$i12]['attributes']['DW'];
                        //     }
                        //     else
                        //     {
                        //         $v412[$tr][$iddd][0]["attributes"]["DX"]=$vals[$i12]['attributes']['CL'];
                        //         $v412[$tr][$iddd][0]["attributes"]["DY"]=$vals[$i12]['attributes']['CW'];
                        //     }
                        // }
                        
                        // echo "DX";
                        // p_($v412[$tr][$iddd][0]["attributes"]["DX"]);
                        // echo "DY";
                        // p_($v412[$tr][$iddd][0]["attributes"]["DY"]);
                        
                        // p_($vals[$i12]);
                        // p_($turn);
                    }
                   
                    $v412_pr=$v412;
                    $iddd++;
                }
            }
        }
        
                    // p_($v412);exit;
       
        unset($iddd,$i121,$i1211,$i33,$ban12);
        $side=array('l','r','b','t');
        foreach ($side as $s)
        {
            if (isset($vals[$i12]["attributes"]["EL".mb_strtoupper($s)]))
            {
                foreach ($index["OPERATION"] as $s1)
                {
                    if (($vals[$s1]["attributes"]["TYPEID"]=="EL")&&($vals[$s1]["attributes"]["ID"]==substr($vals[$i12]["attributes"]["EL".mb_strtoupper($s)],11)))
                    {
                        $ban12[mb_strtoupper($s)]=$vals[$s1+1]["attributes"]["ID"];
                        foreach ($index["GOOD"]as $i2212)
                        {
                            if ($vals[$i2212]["attributes"]["TYPEID"]=="band")
                            {
                                if($vals[$i2212]["attributes"]["ID"]==$vals[$s1+1]["attributes"]["ID"])
                                {
                                    
                                    $ban121[$s]["tb12"]=$vals[$i2212]["attributes"]["T"];
                                    $ban121[$s]["wb12"]=$vals[$i2212]["attributes"]["W"];
                                    $ban121[$s]["nb12"]=$vals[$i2212]["attributes"]["NAME"];
                                    $ban121[$s]["s"]=$s;
                                    if ($vals[$i2212]["attributes"]["MY_CHECK_KL"]>0) $ban121[$s]["kl12"]=1;
                                }
                            }
                        }
                    }
                }
                if (($vals[$i12]["attributes"]['MY_DOUBLE_RES']==1)&&($vals[$i12]["attributes"]['EL'.mb_strtoupper($s)]<>'')&&($double_edge[$vals[$i12]["attributes"]["MY_DOUBLE_TYPE"]]['el'.$s]==0)&&(isset($double_edge[$vals[$i12]["attributes"]["MY_DOUBLE_TYPE"]]['el'.$s])))
                {
                    // p_($double_edge);
                    // p_($s);
                    // p_($vals[$i12]["attributes"]['EL'.mb_strtoupper($s)]);
                    $error.="<br>[/0165] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    - недопутимое ".SIDE_T[$s]." кромкование для типа сшивки - ".$vals[$i12]["attributes"]["MY_DOUBLE_TYPE"].".
                    Уберите пожалуйста кромкование с этой стороны.
                    <br>";
                }
            }
        } 
        
        if (($t12<$gr12+5)AND($t12>5)) {
            $error.="<br>[/0001] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая глубина паза ('".$gr12."'). Максимальное значение ".($t12-5)." мм.<br>";
        }
        if ((($vals[$i12]["attributes"]["GRL"])OR($vals[$i12]["attributes"]["GRR"]))&&($gr112>$vals[$i12]["attributes"]["L"]))
        {
            $error.="<br>[/0002] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая ширина паза ('".$gr112."'). Максимальное значение ".$vals[$i12]["attributes"]["L"]." мм.<br>";
        }
        if ((($vals[$i12]["attributes"]["GRT"])OR($vals[$i12]["attributes"]["GRB"]))&&($gr112>$vals[$i12]["attributes"]["W"]))
        {
            $error.="<br>[/0003] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая ширина паза ('".$gr112."'). Максимальное значение ".$vals[$i12]["attributes"]["W"]." мм.<br>";
        }
        if ($vals[$i12]["attributes"]["MY_DOUBLE_RES"]==1)
        {
        
            if ($t12>54) {
                $error.="<br>[/0004] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая толщина 'T' ('".$t12."'). Максимальное значение 54 мм.<br>";
            }
            if ($t12<16) {
                $error.="<br>[/0005] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая толщина 'T' ('".$t12."'). Минимальное значение 16 мм.<br>";
            }
            if ($vals[$i12]["attributes"]["CW"]<70)
            { $error.="<br>[/0006] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 70 мм.<br>";
            }
            if ($vals[$i12]["attributes"]["CL"]<70)
            { $error.="<br>[/0007] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 70 мм.<br>";
            }
            if (($vals[$i12]["attributes"]["CW"]>=70)&&($vals[$i12]["attributes"]["CW"]<=100)&&($vals[$i12]["attributes"]["CL"]<=100))
            { $error.="<br>[/0008] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 70 до 100 мм, то 'L' должна быть больше 100 мм.<br>";
            }
            if (($vals[$i12]["attributes"]["CL"]>=70)&&($vals[$i12]["attributes"]["CL"]<=100)&&($vals[$i12]["attributes"]["CW"]<=100))
            { $error.="<br>[/0009] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 70 до 100 мм, то 'W' должна быть больше 100 мм.<br>";
            }
            if ($vals[$i12]["attributes"]["CW"]>($w12-$triml))
            { $error.="<br>[/0010] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
            }
            if ($vals[$i12]["attributes"]["CL"]>($l12-$trimw))
            { $error.="<br>[/0011] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
            - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
            }
        }
        
        
        if (!isset($vals[$i12]["attributes"]["MY_DOUBLE"]))
        {
            if ($t12<=21)
            {
                // p_($vals[$i12]);
                $part=get_all_part($vals,$vals[$i12]['attributes']['ID']);

                if ($vals[$i12]["attributes"]["CW"]<30)
                { 
                    
                    $error.="<br>[/0012] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 30 мм.<br>Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                          
                }
                if ($vals[$i12]["attributes"]["CL"]<30)
                { $error.="<br>[/0013] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 30 мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                }
                
                if ((($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]=="true"))&&(count($part['cl'])<1))
                { 
                    if (!isset($mat_out[$vals[$i12]["attributes"]["part.my_material_id"]]))
                    $error.="<br>[/0014] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - деталь нельзя вращать, недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                }
                elseif (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { 
                    if (($vals[$i12]["attributes"]["CL"]>($w12-$trimw))AND($vals[$i12]["attributes"]["CW"]>($l12-$trimw)))
                    {
                        $error.="<br>[/0015] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                    }
                }
                if ((($vals[$i12]["attributes"]["CW"]>($w12-$triml))AND($vals[$i12]["attributes"]["TXT"]=="true"))&&(count($part['cl'])<1))
                { 
                    // p_($mat_out);
                    if (!isset($mat_out[$vals[$i12]["attributes"]["part.my_material_id"]]))
                    $error.="<br>[/0122] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - деталь нельзя вращать, недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$w12." мм, L = ".$l12." мм<br>";
                }
                elseif ((($vals[$i12]["attributes"]["CW"]>($l12-$triml))AND($vals[$i12]["attributes"]["CW"]>($w12-$triml)))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { $error.="<br>[/0123] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$l12." мм, L = ".$l12." мм<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>=30)&&($vals[$i12]["attributes"]["CW"]<70)&&($vals[$i12]["attributes"]["CL"]<=100))
                { $error.="<br>[/0016] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 30 до 70 мм, то 'L' должна быть больше 100 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>=30)&&($vals[$i12]["attributes"]["CL"]<70)&&($vals[$i12]["attributes"]["CW"]<=100))
                { $error.="<br>[/0017] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 30 до 70 мм, то 'W' должна быть больше 100 мм.<br>";
                }
            }
            
            elseif (($t12>21)&&($t12<27))
            {
                if ($vals[$i12]["attributes"]["CW"]<30)
                { $error.="<br>[/0018] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 50 мм.<br>";
                }
                if ($vals[$i12]["attributes"]["CL"]<30)
                { $error.="<br>[/0019] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 50 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]=="true"))
                { $error.="<br>[/0020] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - деталь нельзя вращать, недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                }
                elseif (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { 
                    if (($vals[$i12]["attributes"]["CL"]>($w12-$trimw))AND($vals[$i12]["attributes"]["CW"]>($l12-$trimw)))
                    {
                        $error.="<br>[/0021] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                    }
                }
                if (($vals[$i12]["attributes"]["CW"]>($w12-$triml))AND($vals[$i12]["attributes"]["TXT"]=="true"))
                { $error.="<br>[/0124] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - деталь нельзя вращать, недопутимая длина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$w12." мм, L = ".$l12." мм<br>";
                }
                elseif ((($vals[$i12]["attributes"]["CW"]>($l12-$triml))AND($vals[$i12]["attributes"]["CW"]>($w12-$triml)))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { $error.="<br>[/0125] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$l12." мм, L = ".$l12." мм<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>=50)&&($vals[$i12]["attributes"]["CW"]<=70)&&($vals[$i12]["attributes"]["CL"]<=100))
                { $error.="<br>[/0022] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 50 до 70 мм, то 'L' должна быть больше 100 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>=50)&&($vals[$i12]["attributes"]["CL"]<=70)&&($vals[$i12]["attributes"]["CW"]<=100))
                { $error.="<br>[/0023] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 50 до 70 мм, то 'W' должна быть больше 100 мм.<br>";
                }
                
            }
            elseif ($t12>27)
            {
                if ($vals[$i12]["attributes"]["CW"]<30)
                { $error.="<br>[/0024] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 50 мм.<br>";
                }
                if ($vals[$i12]["attributes"]["CL"]<30)
                { $error.="<br>[/0025] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 50 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>($w12))&&($w12>0))
                { $error.="<br>[/0026] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12)." мм.<br>";
                //echo "!!!!!".$error;exit;
                }
                if (($vals[$i12]["attributes"]["CL"]>$l12)&&($l12>0))
                { $error.="<br>[/0027] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".$l12." мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>=50)&&($vals[$i12]["attributes"]["CW"]<=70)&&($vals[$i12]["attributes"]["CL"]<=100))
                { $error.="<br>[/0028] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 50 до 70 мм, то 'L' должна быть больше 100 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>=50)&&($vals[$i12]["attributes"]["CL"]<=70)&&($vals[$i12]["attributes"]["CW"]<=100))
                { $error.="<br>[/0029] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 50 до 70 мм, то 'W' должна быть больше 100 мм.<br>";
                }
            }
            
        }
            //print_r_($vals[$i12]);
        if (($vals[$i12]["attributes"]["ELL"]<>"")OR($vals[$i12]["attributes"]["ELR"]<>"")OR($vals[$i12]["attributes"]["ELT"]<>"")OR($vals[$i12]["attributes"]["ELB"]<>""))
        {
           # test ("tb12",$ban12["tb12"]);
            #test("id",$vals[$i12]["attributes"]["ID"]);
            if ($t12<8) {
                $error.="<br>[/0030] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая толщина 'T' ('".$t12."') для кромкования. Минимальное значение 8 мм.<br>";
            }
            if ($t12>50) {
                $error.="<br>[/0031] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                - недопутимая толщина 'T' ('".$t12."') для кромкования. Максимальное значение 50 мм.<br>";
            }
            // p_($ban121);
            foreach ($ban121 as $k_ban=>$ban12)
            {
                if (($vals[$i12]["attributes"]["MY_DOUBLE_RES"]==1)&&(isset($double_edge[$vals[$i12]["attributes"]["MY_DOUBLE_TYPE"]]['el'.$ban12["s"]])))
                {
                    $t12=$t12/2*$double_edge[$vals[$i12]["attributes"]["MY_DOUBLE_TYPE"]]['el'.$ban12["s"]];
                    // p_($t12);
                    // p_($ban12["s"]);

                }
                if($t12>($ban12["wb12"]-3))
                {
                    // p_($t12);
                    // p_($double_edge[$vals[$i12]["attributes"]["MY_DOUBLE_TYPE"]]);
                    // p_($ban121);
                   
                    // p_($double_edge);
                    $error.="<br>[/00032] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    - недопутимая ширина 'W' ('".$ban12["wb12"]."') для кромки ('".$ban12["nb12"]."'). Найдите кромку шириной от ".($t12+3)." мм.<br>";
                }
                // if(($t12<=24)&&($ban12["tb12"]<=0.8))
                if($t12<=24)
                {
                    
                    if ($vals[$i12]["attributes"]["L"]<35)
                    { $error.="<br>[/0033] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 35 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["W"]<35)
                    { $error.="<br>[/0034] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 35 мм.<br>";
                    }
                    if($ban12["kl12"]<>1)
                    {
                        if (($vals[$i12]["attributes"]["L"]>=35)&&($vals[$i12]["attributes"]["L"]<=59)&&($vals[$i12]["attributes"]["W"]<=100))
                        { $error.="<br>[/0035] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'L' от 35 до 59 мм, то 'W' должна быть больше 100 мм.<br>";
                        }
                        if (($vals[$i12]["attributes"]["W"]>=35)&&($vals[$i12]["attributes"]["W"]<=59)&&($vals[$i12]["attributes"]["L"]<=100))
                        { $error.="<br>[/0036] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'W' от 35 до 59 мм, то 'L' должна быть больше 100 мм.<br>";
                        }
                        if (($vals[$i12]["attributes"]["L"]>=60)&&($vals[$i12]["attributes"]["L"]<=70)&&($vals[$i12]["attributes"]["W"]<=60))
                        { $error.="<br>[/0037] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'L' от 60 до 70 мм, то 'W' должна быть больше 60 мм.<br>";
                        }
                        if (($vals[$i12]["attributes"]["W"]>=60)&&($vals[$i12]["attributes"]["W"]<=70)&&($vals[$i12]["attributes"]["L"]<=60))
                        { $error.="<br>[/0038] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'W' от 60 до 70 мм, то 'L' должна быть больше 60 мм.<br>";
                        }
                    }
                }  
                // else
                // {
                //     if($ban12["kl12"]<>1)
                //     {   
                //         if ($vals[$i12]["attributes"]["L"]<60)
                //         { $error.="<br>[/0039] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                //         - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 70 мм.<br>";
                //         }
                //         if ($vals[$i12]["attributes"]["W"]<60)
                //         { $error.="<br>[/0040] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                //         - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 70 мм.<br>";
                //         }
                //         if (($vals[$i12]["attributes"]["L"]>=70)&&($vals[$i12]["attributes"]["L"]<100)&&($vals[$i12]["attributes"]["W"]<100))
                //         { $error.="<br>[/0041] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                //         - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'L' от 70 до 100 мм, то 'W' должна быть больше 100 мм.<br>";
                //         }
                //         if (($vals[$i12]["attributes"]["W"]>=70)&&($vals[$i12]["attributes"]["W"]<100)&&($vals[$i12]["attributes"]["L"]<100))
                //         { $error.="<br>[/0042] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                //         - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'W' от 70 до 100 мм, то 'L' должна быть больше 100 мм.<br>";
                //         }
                //     }
                // }
            } 
        
        }
        
        // p_($v412);
        if ($_SESSION['check_index']==1)
          {
            if (!isset ($ch_text)) $error.='<br>'.$_SESSION['error'].'<br> !!!! В одной из XNC операций есть ошибка, исправьте пожалуйста и продолжите работу с проектом !!!!<hr>';
            $ch_text=1;  

          }
        elseif((isset($v412))&&(!$check_index))
        {
           
            
            //echo "<hr>".count($v412)."<hr>".count($v412[$tr]);
            //exit;
            for ($i=0;$i<count($v412[$tr]);$i++)
            { 
               // print_r_($v412[$i]);
               
                if($i412[$tr][$i]["GR"])
                {
                //     if (($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["DW"])AND($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["W"]))
                //     {
                        
                //         $error.="<br>[/0043] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (DW ".$vals[$i12]["attributes"]["DW"]." * DL ".$vals[$i12]["attributes"]["DL"].")
                //             - высота DY XNC ".$v412[$tr][$i][0]["attributes"]["DY"]." мм обработки не совпадает с длиной детали 'DW' ('".$vals[$i12]["attributes"]["DW"]."' мм)
                //             либо с длиной детали 'W' ('".$vals[$i12]["attributes"]["W"]."' мм)";
                //     }
                //     if (($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["DL"])AND($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["L"]))
                //     {
                //         $error.="<br>[/0044] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (DW ".$vals[$i12]["attributes"]["DW"]." * DL ".$vals[$i12]["attributes"]["DL"].")
                //             - ширина DX XNC ".$v412[$tr][$i][0]["attributes"]["DX"]." мм не совпадает с шириной детали  'DL' ('".$vals[$i12]["attributes"]["DL"]."' мм)
                //             либо с шириной детали  'L' ('".$vals[$i12]["attributes"]["L"]."' мм)";
                //     }
                }
                else
                {
                    // if ((!$i412[$tr][$i]["MS"])AND(!$i412[$tr][$i]["ML"])AND(!$i412[$tr][$i]["MA"])AND(!$i412[$tr][$i]["MAC"])AND(!$i412[$tr][$i]["GR"]))
                    // {
                        
                    //     if (($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["DW"])&&($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["DL"]))
                    //     {
                    //         // p_($v412[$tr][$i]);
                    //         // p_($vals[$i12]);

                           
                    //         $error.="<br>[/0045] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    //             - высота DY XNC ".$v412[$tr][$i][0]["attributes"]["DY"]." мм обработки не совпадает с длиной детали 'DW' ('".$vals[$i12]["attributes"]["DW"]."' мм).
                    //             Так как в программе ЧПУ нет фрезерной обработки, размер DY программы должен соответствовать DW - ширине готовой детали с кромкой.
                    //             ";
                    //     }
                    //     if (($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["DL"])&&($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["DW"]))
                    //     {
                    //         $error.="<br>[/0046] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    //             - ширина DX XNC ".$v412[$tr][$i][0]["attributes"]["DX"]." мм не совпадает с шириной детали  'DL' ('".$vals[$i12]["attributes"]["DL"]."' мм).
                    //             Так как в программе ЧПУ нет фрезерной обработки, размер DX программы должен соответствовать DL - длине готовой детали с кромкой.
                    //             ";
                    //     }
                    // }
                    // else
                    // {
                    //     if (($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["CW"])&&(!isset($turn_in)))
                    //     {
                            
                    //         $error.="<br>[/0126] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    //             - высота DY XNC ".$v412[$tr][$i][0]["attributes"]["DY"]." мм обработки не совпадает с длиной детали 'W' ('".$vals[$i12]["attributes"]["W"]."' мм).
                    //             Так как в программе ЧПУ есть фрезерная обработка, размер DY программы должен соответствовать W - ширине заготовки с учетом припуска на подфуговку.
                    //             ";
                    //     }
                    //     if (($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["CL"])&&(isset($turn_in)))
                    //     {
                    //         $error.="<br>[/0127] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                    //             - ширина DX XNC ".$v412[$tr][$i][0]["attributes"]["DX"]." мм не совпадает с шириной детали  'L' ('".$vals[$i12]["attributes"]["L"]."' мм).
                    //             Так как в программе ЧПУ есть фрезерная обработка, размер DX программы должен соответствовать X - длине заготовки с учетом припуска на подфуговку.
                    //             ";
                    //     }
                    // }
                }
                if ($v412[$tr][$i][0]["attributes"]["DZ"]<>$t12)
                {
                    $error.="<br>[/0047] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - толщина DZ XNC ".$v412[$tr][$i][0]["attributes"]["DZ"]." мм обработки не равна толщине детали 'Т' ('".$t12."').";
                }
                $query1112="SELECT * FROM `tools`";
                $result1112 = mysqli_query($link, $query1112) or die("Ошибка " . mysqli_error($link)); 
                if($result1112)
                {
                    $rows1112 = mysqli_num_rows($result1112); // количество полученных строк
                    for ($i1111 = 0 ; $i1111 < $rows1112 ; ++$i1111)
                    {
                        $tools_from_DB[] = mysqli_fetch_array($result1112);
                        
                    }
                }
                foreach ($tools_from_DB as $o21212)
                {
                    $name_tools_from_DB[$o21212["tools_id"]]=$o21212["name"];
                    $diameter_tools_from_DB[$o21212["tools_id"]]=$o21212["diameter"];
                    $diameter_tools_from_DB[$o21212["tools_id"]]=$o21212["diameter"];
                    if((int)($o21212["depth"]))
                    {
                        $depth_tools_from_DB[$o21212["tools_id"]]=$o21212["depth"];
                    }   
                    else
                    {
                        if (strpos($o21212["depth"],"+"))
                    {
                            $rrrr=explode("+",$o21212["depth"]);
                            foreach ($rrrr as $rrr)
                            {
                                    if ($rrr=="dz") $rrr=$t12;
                                    $rr+=$rrr;
                            } 
                            $depth_tools_from_DB[$o21212["tools_id"]]=$rr;
                            unset($rrrr,$rrr,$rr);
                    }
                    if (strpos($o21212["depth"],"-"))
                    {
                            $rrrr=explode("-",$o21212["depth"]);
                            foreach ($rrrr as $rrr)
                            {
                                    if ($rrr=="dz") 
                                    {
                                        $rrr=$t12;
                                        $rr+=$rrr;
                                    }
                                    else $rr-=$rrr;
                            } 
                            $depth_tools_from_DB[$o21212["tools_id"]]=$rr;
                            unset($rrrr,$rrr,$rr);
                    }
                    }
                }
                
                foreach ($v412[$tr] as $k1=>$o1)
                {
                  //print_r_($v412[$tr][0][0]["attributes"]);exit;
                    foreach ($o1 as $k2=>$o2)
                    {
                       // test ($k2,"k2");print_r_($o2["attributes"]);
                        foreach ($o2["attributes"] as $k3=>$o3)
                        {
                            unset($act);
                            if (substr_count($o3,"+")>0)
                            {
                                $ar=explode("+",$o3);
                                $act="plus";
                               // print_r_($ar);
                            }
                            elseif (substr_count($o3,"-")>0)
                            {
                                $ar=explode("-",$o3);
                                $act="minus";
                                //print_r_($ar);
                            }
                            if($act)
                            {
                               
                                if (mb_strtoupper($ar[0])=="DX")
                                {
                                    $ar[0]=$v412[$tr][0][0]["attributes"]["DX"];
                                   //print_r_($ar);
                                }
                                elseif (mb_strtoupper($ar[0])=="DY")
                                {
                                    $ar[0]=$v412[$tr][0][0]["attributes"]["DY"];
                                    //print_r_($ar);
                                }
                                elseif (mb_strtoupper($ar[0])=="DZ")
                                {
                                    $ar[0]=$v412[$tr][0][0]["attributes"]["DZ"];
                                    //print_r_($ar);
                                }
                                if($ar[0]>0)
                                {
                                    if ($act=="plus")
                                    {
                                        //test ("перед",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                        $v412[$tr][$k1][$k2]["attributes"][$k3]=$ar[0]+$ar[1];
                                        //test ("после",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                    }
                                    elseif ($act=="minus")
                                    {
                                    //test ("перед",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                        $v412[$tr][$k1][$k2]["attributes"][$k3]=$ar[0]-$ar[1];
                                       // test ("после",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                    }
                                }
                            }
                        }
                    }
                }
                unset($k1,$k2,$k3,$o1,$o2,$o3,$ar,$act);
                $diameter_tools_from_DB=array_unique($diameter_tools_from_DB);
                sort($diameter_tools_from_DB);
                foreach ($i412[$tr][$i]["TOOL"] as $ik121)
                {
                    //print_r_($v412[$tr][$i][$ik121]);
                    //echo "!!!";
                    if ((!in_array($v412[$tr][$i][$ik121]["attributes"]["D"],$diameter_tools_from_DB))AND(substr_count(mb_strtoupper($v412[$tr][$i][$ik121]["attributes"]["NAME"]),"BORE")>0))
                    {
                        // echo substr_count(mb_strtoupper($v412[$tr][$i][$ik121]["attributes"]["NAME"]),"BORE");exit;
                        // p_($v412[$tr][$i][$ik121]);
                        $error.="<br>[/0048] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - диаметр D инструмента ".$v412[$tr][$i][$ik121]["attributes"]["D"]." мм не обнаружен в нашей базе инструмента. <br>
                        Допустимые диаметры (".implode(",",$diameter_tools_from_DB).") мм.<br>";
                    }
                }
                
                //.. 16.08.2021 Максимальная ширина стола - 1540мм, если деталь для xnc-обработки больше по двум сторонам (по X, и по Y), то выдаём ошибку.
                foreach ($i412[$tr][$i]["PROGRAM"] as $value) {
                    if ($v412[$tr][$i][$value]['type'] == 'open' ) {
                        if ($v412[$tr][$i][$value]['attributes']['DX'] > 1540 && $v412[$tr][$i][$value]['attributes']['DY'] > 1540) {
                            $error.="<br>[/0168] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали фрезерования в программе для фрезерования DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1540 мм.<br>";
                        }
                    }
                }

                if (!$vals[$i33]["attributes"]["TURN"]) $vals[$i33]["attributes"]["TURN"]=0;
                foreach ($i412[$tr][$i]["BF"] as $ik121)
                {
                    // p_($vals[$i12]);
                    
                    foreach ($i412[$tr][$i]["TOOL"] as $g)
                    {
                        if ($v412[$tr][$i][$g]["attributes"]["NAME"]==$v412[$tr][$i][$ik121]["attributes"]["NAME"]) $v412_diam=$v412[$tr][$i][$g]["attributes"]["D"];
                    }
                    // p_($v412_diam);
                    // p_($i412);
                    // p_($v412[$tr][$i][$ik121]["attributes"]);

                    
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0049] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина фронтального сверления DP ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм".$turn_text."
                        больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>4200)
                    {
                        $error.="<br>[/0050] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                        
                    }
                    if(!$turn)
                    {
                        if (( $v412[$tr][$i][0]["attributes"]["DX"]<200)&&
                        (($v412[$tr][$i][0]["attributes"]["DX"]<70)&&($v412[$tr][$i][0]["attributes"]["DY"]>=200)))
                        {
                            $error.="<br>[/0051] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                            // p_($v412);
                        }
                    }
                    else
                    {
                        if (( $v412[$tr][$i][0]["attributes"]["DY"]<200)&&
                        (($v412[$tr][$i][0]["attributes"]["DY"]<70)&&($v412[$tr][$i][0]["attributes"]["DX"]>=200)))

                        {
                            $error.="<br>[/0051] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 200 мм.<br>";
                        }
                    }
                    if (( $v412[$tr][$i][0]["attributes"]["DY"]>1270)&&( $v412[$tr][$i][0]["attributes"]["DX"]>1270))
                    {
                        $error.="<br>[/0052] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1270 мм.<br>";
                    }
                    if(!$turn)
                    {
                        if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                        {
                            // p_($vals[$i12]);
                            // p_($v412[$tr][$i][0]["attributes"]["DY"]);
                            $error.="<br>[/0053] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                        }
                    }
                    else
                    {
                        if ( $v412[$tr][$i][0]["attributes"]["DX"]<70)
                        {
                            $error.="<br>[/0053] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали фронтального сверления в программе для сверления DY".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 70 мм.<br>";
                        }
                    }
                    if ($vals[$i12]["attributes"]["DL"]>4200)
                    {
                        $error.="<br>[/0054] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали для фронтального сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (!$turn)
                    {
                        // echo ('11');
                        if ($v412[$tr][$i][$ik121]["attributes"]["X"]>$v412[$tr][$i][0]["attributes"]["DX"])
                        {
                            // p_($vals[$i33]);
                            // p_($vals[$i12]);
                            // p_($v412[$tr][$i]);
                            // exit;
                            // test('ch_in',$check_index);
                            // xml($vals);
                            $error.="<br>[/0055] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата фронтального сверления X".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["X"]." больше чем длина программы детали DX ".$v412[$tr][$i][0]["attributes"]["DX"]."  мм. <br>";
                        }
                        if ($v412[$tr][$i][$ik121]["attributes"]["Y"]>$v412[$tr][$i][0]["attributes"]["DY"])
                        {
                            $error.="<br>[/0056] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата фронтального сверления Y".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." больше чем ширина программы детали DW ".$v412[$tr][$i][0]["attributes"]["DY"]."  мм. <br>";
                        }
                    }
                    elseif (isset($turn)&&(isset($turn_in)))
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["Y"]>$v412[$tr][$i][0]["attributes"]["DX"])
                        {
                            $error.="<br>[/0055] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата фронтального сверления X".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." больше чем длина программы детали DX ".$v412[$tr][$i][0]["attributes"]["DX"]."  мм. <br>";
                        }
                        if ($v412[$tr][$i][$ik121]["attributes"]["X"]>$v412[$tr][$i][0]["attributes"]["DY"])
                        {
                            $error.="<br>[/0056] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата фронтального сверления Y".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["X"]." больше чем ширина программы детали DY ".$v412[$tr][$i][0]["attributes"]["DY"]."  мм. <br>";
                        }
                    }
                    // p_($t12-3);
                    if (($v412[$tr][$i][$ik121]["attributes"]["DP"]<0)OR(((($v412[$tr][$i][$ik121]["attributes"]["DP"]>30)OR($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12-3)))AND($v412[$tr][$i][$ik121]["attributes"]["DP"]<$t12))))
                    {
                        // p_($v412[$tr][$i][$ik121]);
                        
                            $error.="<br>[/0057] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - глубина фронтального сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна ".($t12-3)." мм, либо сквозное более ".$t12." мм, максимальная глубина сверления 41 мм.<br>";
                        
                        // else 
                        // {
                        //     $error.="<br>[/0058] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        //     - глубина фронтального сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна ".($t12-3)." мм для глухого отверстия и более ".($t12)." мм для сквозного отверстия.
                        //     Максимальная глубина сверления 41 мм.
                        //     <br>";
                        // }
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>41)
                    {
                        $error.="<br>[/0057] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина фронтального сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна ".($t12-3)." мм, либо сквозное более ".$t12." мм, максимальная глубина сверления 41 мм.<br>";
                    
                    }
                    if (!$turn)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["AC"]>1)
                        {
                            if (!$v412[$tr][$i][$ik121]["attributes"]["AS"]) 
                            {
                                $t121_dw= $ik121-1;
                                while ($v412[$tr][$i][$t121_dw]["tag"]<>"TOOL")
                                {
                                    $t121_dw--;
                                } 
                                $v412[$tr][$i][$ik121]["attributes"]["AS"]=$v412[$tr][$i][$t121_dw]["attributes"]["D"];
                            }
                            if ($v412[$tr][$i][$ik121]["attributes"]["AV"]=="true")
                            {
                                $ac1=$v412[$tr][$i][$ik121]["attributes"]["Y"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["Y"]);
                                if($ac1>$v412[$tr][$i][0]["attributes"]["DY"])
                                {
                                    ///echo "<hr><hr>";
                                    $error.="<br>[/0059] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по вертикали (".$ac1." мм) более ширины программы детали DY ".$v412[$tr][$i][0]["attributes"]["DY"]." мм";
                                }
                            }
                            else
                            {
                                $ac=$v412[$tr][$i][$ik121]["attributes"]["X"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["X"]);
                                if($ac>$v412[$tr][$i][0]["attributes"]["DX"])
                                {
                                    $error.="<br>[/0060] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по горизонтали (".$ac." мм) более длины программы детали DX ".$v412[$tr][$i][0]["attributes"]["DX"]." мм";
                                }
                            }
                        }
                    }
                    elseif ($turn)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["AC"]>1)
                        {
                            if (!$v412[$tr][$i][$ik121]["attributes"]["AS"]) 
                            {
                                $t121_dw= $ik121-1;
                                while ($v412[$tr][$i][$t121_dw]["tag"]<>"TOOL")
                                {
                                    $t121_dw--;
                                } 
                                $v412[$tr][$i][$ik121]["attributes"]["AS"]=$v412[$tr][$i][$t121_dw]["attributes"]["D"];
                            }
                            if ($v412[$tr][$i][$ik121]["attributes"]["AV"]=="true")
                            {
                                $ac1=$v412[$tr][$i][$ik121]["attributes"]["Y"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["Y"]);
                                if($ac1>$v412[$tr][$i][0]["attributes"]["DX"])
                                {
                                    ///echo "<hr><hr>";
                                    $error.="<br>[/0059] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по вертикали (".$ac1." мм) более ширины программы детали DY ".$v412[$tr][$i][0]["attributes"]["DX"]." мм";
                                }
                            }
                            else
                            {
                                $ac=$v412[$tr][$i][$ik121]["attributes"]["X"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["X"]);
                                if($ac>$v412[$tr][$i][0]["attributes"]["DY"])
                                {
                                    $error.="<br>[/0060] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по горизонтали (".$ac." мм) более длины программы детали DX ".$v412[$tr][$i][0]["attributes"]["DY"]." мм";
                                }
                            }
                        }
                    }
                }
                foreach ($i412[$tr][$i]["BL"] as $ik121)
                {
                    
                    // if (!$v412[$tr][$i][$ik121]["attributes"]["NAME"])
                    // {
                    //     $v412_temp=$ik121-1;
                    //     while ($v412[$tr][$i][$v412_temp]["tag"]<>"TOOL")
                    //     {
                    //         $v412_temp--;
                    //     }
                    //     $v412_diam=$v412[$tr][$i][$v412_temp]["attributes"]["D"];
                    //     $v412_tool=$v412[$tr][$i][$v412_temp];
                    //     unset ($v412_temp);
                    //     //print_r_($ik121);exit;
                    // }
                    // else
                    // {
                    //     //echo "!!!";exit;
                    //     $v412_diam =trim( preg_replace('/[^0-9 , .]/', '', $v412[$tr][$i][$ik121]["attributes"]["NAME"]));
                    // }
                    foreach ($i412[$tr][$i]["TOOL"] as $g)
                    {
                        if ($v412[$tr][$i][$g]["attributes"]["NAME"]==$v412[$tr][$i][$ik121]["attributes"]["NAME"]) $v412_diam=$v412[$tr][$i][$g]["attributes"]["D"];
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0061] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина левого сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0062] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                   
                    if (( $v412[$tr][$i][0]["attributes"]["DX"]<200)&&
                    (($v412[$tr][$i][0]["attributes"]["DX"]<70)&&($v412[$tr][$i][0]["attributes"]["DY"]>=200)))
                    {
                        // echo '@';  
                        if($turn>0)
                        {
                            if ( $v412[$tr][$i][0]["attributes"]["DY"]<200)
                            {
                                $error.="<br>[/0063] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - размер детали в программе для левого сверления DX".$turn_text."
                                ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 200 мм.<br>";
                            }
                        }
                        else
                        {

                            $error.="<br>[/0063] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали в программе для левого сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                        }
                    }
                    // if (($vals[$i12]['attributes']['ID']==89379))
                    // {
                    //     p_($vals[$i12]['attributes']);
                    //     p_($error);
                    //     p_($v412[$tr][$i][0]["attributes"]);

                    // } 
                    if (( $v412[$tr][$i][0]["attributes"]["DY"]>1270)&&( $v412[$tr][$i][0]["attributes"]["DX"]>1270))
                    {
                        $error.="<br>[/0064] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1270 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0065] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0066] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали для левого сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Y"]>($v412[$tr][$i][0]["attributes"]["DY"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["Y"]<-20))
                    {
                        $error.="<br>[/0067] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - координата Y ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." левого сверления".$turn_text."  
                        не допустима, высота детали DY, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DY"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }

                    //.. 15.08.2022 Ограничение для торцевого сверления (диаметры 4.5мм и 8мм)
                    // В условие обавлено "!in_array($v412_diam, $max_face_drilling_diameter)"
                    // и внутри условия добавлен верхний "if" (if(!in_array($v412_diam, $max_face_drilling_diameter)))
                    // (торцевое сверление, диаметр 8мм, диаметр 10мм)
                    if (!in_array($v412_diam, $max_face_drilling_diameter)
                        OR($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))
                        OR(($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(!in_array($v412_diam, $max_face_drilling_diameter))
                        {
                            $error.="<br>[/0128_1] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - для торцевых (левого) сверлений доступны диаметры <b>4.5</b> и <b>8мм</b>. <br>";
                        } elseif(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                        {
                            $error.="<br>[/0128] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z  для левого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            отсутствует. <br>";
                        }
                        else
                        {
                            if(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                            {
                            // print_r_($v412[$tr][$i]);
                                $error.="<br>[/0068] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для левого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                                не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                            }
                            else
                            {
                                $error.="<br>[/0069] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для левого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                                не допустима для материала толщиною ".$t12." мм.";
                            }
                        }
                    }
                    if ( $v412_diam>($t12-6))
                    {
                        $error.="<br>[/0070] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - толщина материала ".$t12." мм левого сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0071] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина левого сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["BR"] as $ik121)
                {
                   
                    foreach ($i412[$tr][$i]["TOOL"] as $g)
                    {
                        if ($v412[$tr][$i][$g]["attributes"]["NAME"]==$v412[$tr][$i][$ik121]["attributes"]["NAME"]) $v412_diam=$v412[$tr][$i][$g]["attributes"]["D"];
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0072] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина правого сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0073] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (( $v412[$tr][$i][0]["attributes"]["DX"]<200)&&
                        (($v412[$tr][$i][0]["attributes"]["DX"]<70)&&($v412[$tr][$i][0]["attributes"]["DY"]>=200)))
                    {
                        if ($turn>0)
                        {
                            if ( $v412[$tr][$i][0]["attributes"]["DY"]<200)
                            {
                                $error.="<br>[/0074] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - размер детали в программе для правого сверления DX".$turn_text."
                                ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 200 мм.<br>";
                            }
                        }
                        else
                        {
                            $error.="<br>[/0074] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - размер детали в программе для правого сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                        }
                    }
                    if (($v412[$tr][$i][0]["attributes"]["DY"]>1270)&&( $v412[$tr][$i][0]["attributes"]["DX"]>1270))
                    {
                        $error.="<br>[/0075] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1270 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0076] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0077] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали для правого сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Y"]>($v412[$tr][$i][0]["attributes"]["DY"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["Y"]<-20))
                    {
                        $error.="<br>[/0078] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - координата Y ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." правого  сверления".$turn_text."  
                        не допустима, высота детали DY, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DY"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }

                    //.. 15.08.2022 Ограничение для торцевого сверления (диаметры 4.5мм и 8мм)
                    // В условие обавлено "!in_array($v412_diam, $max_face_drilling_diameter)"
                    // и внутри условия добавлен верхний "if" (if(!in_array($v412_diam, $max_face_drilling_diameter)))
                    // (торцевое сверление, диаметр 8мм, диаметр 10мм)
                    if (!in_array($v412_diam, $max_face_drilling_diameter)
                        OR($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))
                        OR(($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(!in_array($v412_diam, $max_face_drilling_diameter))
                        {
                            $error.="<br>[/0079_1] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - для торцевых (правого) сверлений доступны диаметры <b>4.5</b> и <b>8мм</b>. <br>";
                        } elseif(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                        {
                            $error.="<br>[/0079] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для правого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                            
                        }
                        else
                        {
                            $error.="<br>[/0080] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для правого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима для материала толщиною ".$t12." мм.";
                            /*print_r_($v412[$tr][$i]);
                            print_r_($v412_tool);
                            print_r_($v412[$tr][$i][$ik121]);
                            exit;*/
                        }
                       
                    }
                    if ($v412_diam>($t12-6))
                    {
                        $error.="<br>[/0081] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - толщина материала ".$t12." мм правого  сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0082] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина правого сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["BT"] as $ik121)
                {
                   
                    foreach ($i412[$tr][$i]["TOOL"] as $g)
                    {
                        if ($v412[$tr][$i][$g]["attributes"]["NAME"]==$v412[$tr][$i][$ik121]["attributes"]["NAME"]) $v412_diam=$v412[$tr][$i][$g]["attributes"]["D"];
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0083] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина верхнего сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0084] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][0]["attributes"]["DX"]<200)&&(($v412[$tr][$i][0]["attributes"]["DY"]>=200)&&($v412[$tr][$i][0]["attributes"]["DX"]<70)))
                    {
                        $error.="<br>[/0085] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                    }
                    if (( $v412[$tr][$i][0]["attributes"]["DY"]>1270)&&( $v412[$tr][$i][0]["attributes"]["DX"]>1270))
                    {
                        $error.="<br>[/0086] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1270 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0087] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0088] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали для верхнего сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["X"]>($v412[$tr][$i][0]["attributes"]["DX"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["X"]<20))
                    {
                        
                        // p_($v412[$tr][$i][$ik121]);
                        $error.="<br>[/0089] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - координата X ".$v412[$tr][$i][$ik121]["attributes"]["X"]." верхнего  сверления".$turn_text."  
                        не допустима, высота детали DX, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DX"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }

                    //.. 15.08.2022 Ограничение для торцевого сверления (диаметры 4.5мм и 8мм)
                    // В условие обавлено "!in_array($v412_diam, $max_face_drilling_diameter)"
                    // и внутри условия добавлен верхний "if" (if(!in_array($v412_diam, $max_face_drilling_diameter)))
                    // (торцевое сверление, диаметр 8мм, диаметр 10мм)
                    if (!in_array($v412_diam, $max_face_drilling_diameter)
                        OR ($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))
                        OR (($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(!in_array($v412_diam, $max_face_drilling_diameter))
                        {
                            $error.="<br>[/0090_1] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - для торцевых (верхнего) сверлений доступны диаметры <b>4.5</b> и <b>8мм</b>. <br>";
                        } elseif(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                        {
                            $error.="<br>[/0090] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для верхнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                        }
                        else
                        {
                            $error.="<br>[/0091] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для верхнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима для материала толщиною ".$t12." мм.";
                        }
                    }
                    if ($v412_diam>($t12-6))
                    {
                        $error.="<br>[/0092] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - толщина материала ".$t12." мм  верхнего  сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0093] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина верхнего сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["BB"] as $ik121)
                {
                    
                    foreach ($i412[$tr][$i]["TOOL"] as $g)
                    {
                        if ($v412[$tr][$i][$g]["attributes"]["NAME"]==$v412[$tr][$i][$ik121]["attributes"]["NAME"]) $v412_diam=$v412[$tr][$i][$g]["attributes"]["D"];
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0094] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина нижнего сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0095] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    
                    if  (($v412[$tr][$i][0]["attributes"]["DX"]<200)&&(($v412[$tr][$i][0]["attributes"]["DY"]>=200)&&($v412[$tr][$i][0]["attributes"]["DX"]<70)))
                    {
                        $error.="<br>[/0096] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                    }
                    if (( $v412[$tr][$i][0]["attributes"]["DY"]>1270)&&( $v412[$tr][$i][0]["attributes"]["DX"]>1270))
                    {
                        $error.="<br>[/0097] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1270 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0098] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0099] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - размер детали для нижнего сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["X"]>($v412[$tr][$i][0]["attributes"]["DX"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["X"]<-20))
                    {
                        $error.="<br>[/0100] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - координата X ".$v412[$tr][$i][$ik121]["attributes"]["X"]." нижнего сверления".$turn_text."  
                        не допустима, высота детали DX, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DX"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }

                    //.. 15.08.2022 Ограничение для торцевого сверления (диаметры 4.5мм и 8мм)
                    // В условие обавлено "!in_array($v412_diam, $max_face_drilling_diameter)"
                    // и внутри условия добавлен верхний "if" (if(!in_array($v412_diam, $max_face_drilling_diameter)))
                    // (торцевое сверление, диаметр 8мм, диаметр 10мм)
                    if (!in_array($v412_diam, $max_face_drilling_diameter)
                        OR ($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))
                        OR (($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(!in_array($v412_diam, $max_face_drilling_diameter))
                        {
                            $error.="<br>[/0101_1] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - для торцевых (нижнего) сверлений доступны диаметры <b>4.5</b> и <b>8мм</b>. <br>";
                        } elseif(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                        {
                            $error.="<br>[/0101] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для нижнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                        }
                        else
                        {
                            $error.="<br>[/0102] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для нижнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима для материала толщиною ".$t12." мм.";
                        }
                    }
                    if ($v412_diam>($t12-6))
                    {
                        $error.="<br>[/0103] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - толщина материала ".$t12." мм нижнего сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0104] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина нижнего сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["MS"] as $ik121)
                {
                   
                    unset ($v412_temp4);
                    foreach ($i412[$tr][$i]["TOOL"] as $v412temp2)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["NAME"]==$v412[$tr][$i][$v412temp2]["attributes"]["NAME"])
                        {
                            if (substr_count($v412[$tr][$i][$v412temp2]["attributes"]["NAME"],'Mill')==0) $v412_temp4=$v412[$tr][$i][$v412temp2]["attributes"]["D"];
                        //echo $v412_temp4; 
                        }
                    }
                    if ((!in_array($v412_temp4,$v412_diam_mill))&&(isset($v412_temp4)))
                    {
                        $error.="<br>[/0105] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - диаметр фрезы ".$v412_temp4." мм не используется у нас в производстве. 
                        Допустимые диаметры (".implode(",",array_unique($v412_diam_mill)).").";
                    }
                    else
                    {
                        if (!isset($v412_diam_mill_depth[$v412_temp4]) && isset($v412_diam_mill_depth[(int)$v412_temp4])) {
                            $v412_temp4 = (int)$v412_temp4;
                        }
                        if ((($v412[$tr][$i][$ik121]["attributes"]["DP"]>$v412_diam_mill_depth[$v412_temp4]))&&(isset($v412_temp4)))
                        {
                            $error.="<br>[/0106] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более возможной (".$v412_diam_mill_depth[$v412_temp4]." мм) для диаметра ".$v412_temp4." мм.";
                        }
                    }
                    if ($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12+3))
                    {
                        $error.="<br>[/0107] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более допустимой глубины ".($t12+3)." мм <br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["DP"]<$t12)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12-3))AND($v412[$tr][$i][$ik121]["attributes"]["DP"]<1))
                    {
                        $error.="<br>[/0108] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм недопустима. Диапазон глубины фрезерования от 1 мм до ".($t12-3)." мм, либо от ".$t12." мм до 39 мм. <br>";
                    }
                   // print_r_($i412[$tr][$i]); print_r_($v412[$tr][$i]); 
                    if ((count($i412[$tr][$i]["MA"])>0)OR(count($i412[$tr][$i]["MAC"])>0)OR(count($i412[$tr][$i]["ML"]>1)))
                    {
                        if ($vals[$i12]["attributes"]["DL"]<100)
                        {
                            $error.="<br>[/0109] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 100 мм <br>";
                        }
                        if ($vals[$i12]["attributes"]["DW"]<100)
                        {
                            $error.="<br>[/0110] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 100 мм <br>";
                        }
                        if ((($vals[$i12]["attributes"]["DW"]>100))AND(($vals[$i12]["attributes"]["DW"]<350)AND($vals[$i12]["attributes"]["DW"]<$vals[$i12]['attributes']['part.my_xnc_w']))AND(($vals[$i12]["attributes"]["DL"]<350)&&($vals[$i12]["attributes"]["DL"]<$vals[$i12]['attributes']['part.my_xnc_w'])))
                        {
                            $error.="<br>[/0111] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 350 мм, при условии что ширина DW более 100 мм и менее 350 мм <br>";
                        }
                        if (($vals[$i12]["attributes"]["DL"]>100)AND(($vals[$i12]["attributes"]["DL"]<350)AND($vals[$i12]["attributes"]["DL"]<$vals[$i12]['attributes']['part.my_xnc_l']))AND(($vals[$i12]["attributes"]["DW"]<350)&&($vals[$i12]["attributes"]["DW"]<$vals[$i12]['attributes']['part.my_xnc_w'])))
                        {
                            $error.="<br>[/0112] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 350 мм, при условии что длина DL более 100 мм и менее 350 мм <br>";
                        }
                    }
                    else
                    {
                        
                        if ($vals[$i12]["attributes"]["DL"]<100)
                        {
                            $error.="<br>[/0129] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].") 
                            - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для обработки на ручном раскроечном станке  100 мм.<br>";
                        }
                        if ($vals[$i12]["attributes"]["DW"]<100)
                        {
                            $error.="<br>[/0130] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для обработки на ручном раскроечном станке 100 мм.<br>";
                        }
                    }

                }
                foreach ($i412[$tr][$i]["GR"] as $ik121)
                {
                   
                   
                    foreach ($i412[$tr][$i]["TOOL"] as $v412temp2)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["NAME"]==$v412[$tr][$i][$v412temp2]["attributes"]["NAME"])
                        {
                        $v412_temp4=$v412[$tr][$i][$v412temp2]["attributes"]["D"];
                        //echo $v412_temp4; 
                        }
                    }
                    //.. 05.07.2021 Диаметр заменён на 3.2
                    // if ($v412_temp4<>4)
                    if ($v412_temp4<>"3.2" && $v412_temp4<>4)
                    {
                        if (!in_array($v412_temp4,$v412_diam_mill))
                        {
                            $error.="<br>[/0113] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - диаметр фрезы ".$v412_temp4." мм не используется у нас в производстве. 
                            Допустимые диаметры (".implode(",",array_unique($v412_diam_mill)).").";
                        }
                        else
                        {
                            if (($v412[$tr][$i][$ik121]["attributes"]["DP"]>$v412_diam_mill_depth[$v412_temp4]))
                            {
                                $error.="<br>[/0114] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более возможной (".$v412_diam_mill_depth[$v412_temp4]." мм) для диаметра ".$v412_temp4." мм.";
                            }
                        }
                    }
                    if ($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12-5))
                    {
                        $error.="<br>[/0115] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина паза ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более допустимой глубины ".($t12-5)." мм <br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["X1"]<>$v412[$tr][$i][$ik121]["attributes"]["X2"])AND($v412[$tr][$i][$ik121]["attributes"]["Y1"]<>$v412[$tr][$i][$ik121]["attributes"]["Y2"]))
                    {
                        $error.="<br>[/0116] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - паз должен быть строго вертикальным или строго горизонтальным, сейчас координаты паза (начало - ".$v412[$tr][$i][$ik121]["attributes"]["X1"].", ".$v412[$tr][$i][$ik121]["attributes"]["Y1"]."// конец  ".$v412[$tr][$i][$ik121]["attributes"]["X2"].", ".$v412[$tr][$i][$ik121]["attributes"]["Y2"].") <br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["DP"]<1)&&(is_numeric($v412[$tr][$i][$ik121]["attributes"]["DP"])))
                    {
                    //     p_($check_index);
                    //     p_($v412[$tr][$i][$ik121]);
                    //    exit;

                        $error.="<br>[/0117] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - глубина паза ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм должна быть более 1 мм <br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]<70)
                    {
                        $error.="<br>[/0118] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                        - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 100 мм <br>";
                    }
                    if ($vals[$i12]["attributes"]["DW"]<70)
                    {
                        if ((!isset($vals[$i12]["attributes"]["part.my_xnc_w"]))||($vals[$i12]["attributes"]["CW"]<$vals[$i12]["attributes"]["part.my_xnc_w"]))
                        {
                            // p_($vals[$i12]);
                            $error.="<br>[/0119] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 100 мм <br>";
                        }
                    }
                    if (!isset($turn))
                    {
                        if (isset($vals[$i12]["attributes"]["part.my_xnc_w"])) $dw=$vals[$i12]["attributes"]["part.my_xnc_w"];
                        else $dw=100;
                        if (isset($vals[$i12]["attributes"]["part.my_xnc_l"])) $dl=$vals[$i12]["attributes"]["part.my_xnc_l"];
                        else $dl=350;

                        if (($vals[$i12]["attributes"]["DL"]>=$dl)AND($vals[$i12]["attributes"]["DW"]<$dw))

                        {
                            if (($vals[$i12]["attributes"]["DW"]>=$dl)AND($vals[$i12]["attributes"]["DL"]>=$dw))
                            {

                            }
                            else
                            {
                                // echo ($turn);
                        // p_($vals[$i12]["attributes"]);
                                $error.="<br>[/0120] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм или ширина детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования. Один размер должен быть не менее 350 мм, второй не менее 100 мм при этом. <br>";
                            }
                        }
                        
                    }
                    else
                    {
                        if (($vals[$i12]["attributes"]["DW"]>100)AND($vals[$i12]["attributes"]["DL"]<350))

                        {
                            if (($vals[$i12]["attributes"]["DL"]>100)AND($vals[$i12]["attributes"]["DW"]>350))
                            {

                            }
                            else
                            {
                                $error.="<br>[/0120] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (L ".$vals[$i12]["attributes"]["L"]." * W ".$vals[$i12]["attributes"]["W"].")
                                - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм или ширина детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования. Один размер должен быть не менее 350 мм, второй не менее 100 мм при этом. <br>";
                            }
                        }
                    }

                }
            }
            //test("id",$tr);
           // print_r_($v412[$tr]);
        }
        if($error)
        {
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["ID"].=$vals[$i12]["attributes"]["ID"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["NAME"].=$vals[$i12]["attributes"]["NAME"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["DL"].=$vals[$i12]["attributes"]["DL"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["L"].=$vals[$i12]["attributes"]["L"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["DW"].=$vals[$i12]["attributes"]["DW"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["W"].=$vals[$i12]["attributes"]["W"];
            //$check_error_part[$vals[$i12]["attributes"]["ID"]]["PROGRAM"]=$v412_pr;
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["ERROR"].=$error;
        }

        unset($turn_text,$i,$ban121,$turn, $error,$v412_diam,$v412_pr,$t121_dw,$t121_dl,$v4122,$k2121211,$k12333,$o21219,$o212199,$ac,$gr12,$gr112,$mat12,$w12,$t12,$l12,$n12,$ban12["wb12"],$ban12["tb12"],$mat12,$ban12,$ban12["nb12"],$r, $pr12, $v412, $i412,$depth_tools_from_DB,$v412,$i412,$rrrr,$rrr,$rr,$o21212,
        $name_tools_from_DB, $diameter_tools_from_DB, $depth_tools_from_DB,$ik121);
    }

}
if($check_error_part)
{
    foreach ($check_error_part as $v12344)
    {
        $error.=$v12344["ERROR"];
    }
    unset ($v12344);
}

if(isset($error)) $_SESSION['error'].=$error;

if (!mysqli_commit($link)) {
    print("Не удалось зафиксировать транзакцию\n");
    exit();
}
?>
