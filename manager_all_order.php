<?php
session_start();

require_once '_1/config.php';
require_once DIR_CORE . 'func.php';
require_once DIR_CORE . 'func_mini.php';

if (!isset($_GET['filter_client']) && (count($_GET) <= 2)) {
    // Периодически случается, что в сессию попадают старые данные,
    // поэтому здесь удаляем всё из сессии, кроме авторизации
    foreach ($_SESSION as $key => $value) {
        if ($key != 'manager_name' && $key != 'manager_id' && $key != 'user' && $key != 'isAdmin' && $key != 'clrsdt' && $key != 'tec_info') {
            unset($_SESSION[$key]);
        }
    }

    // То-же самое и для сессии Ларавель
    // clrsdt - CLeaR Session DaTa
    if (isset($_SESSION['clrsdt'])) $_SESSION['clrsdt']++;
    else $_SESSION['clrsdt'] = 1;

    if ($_SESSION['clrsdt'] < 2) {
        header('Location: '.$laravel_dir.'/set_auth_manager_ids?manager_id='.$_SESSION['manager_id'].'&rssession=1&clrsdt=1&nw='.$_GET['nw']);
        exit();
    } else unset($_SESSION['clrsdt']);
}

function get_data($sql, $count = 0) {
    $result_array = null;
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        $result_array['count'] = $rows;
        if($count != 1) {
            $result_array['orders'] = [];
            for($i = 0; $i < $rows; ++$i) {
                $result_array['orders'][] = mysqli_fetch_assoc($result);
            }
        }
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $result_array;
}

function get_order_edit_variants($id, $flag = null) {
    global $main_dir;
    $sql = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$id.' ORDER BY `DATE` DESC';
    $projects = get_data($sql);
    $html = '';
    if(count($projects['orders']) < 1) {
        return false;
    } else {
        foreach ($projects['orders'] as $project) {
            $author = '';
            if($project['manager_id'] > 0) {
                $author = get_manager_name($project['manager_id']);
            } else if ($project['client_id'] > 0) {
                $author = 'Клиент';
            }
            //.. Изменения здесь!
            if (!$flag) {
                $html .= '<p><a href="'.$GLOBALS['laravel_dir'].'/projects_new/1_'.$id.'?order_project_id='.$project['PROJECT_ID'].'&ssnrst='.session_id().'">'.$author.' от '.$project['DATE'].'</a> </p>';
            } else {
                if ($flag == 'RS') {
                    $html .= '<a href="'.$main_dir.'/clients/RS/clients/create.php?order_project_id='.$project['PROJECT_ID'].'" id="'.$project['PROJECT_ID'].'" class="btn apply-project">'.$author.' от '.$project['DATE'].'</a>';
                }
            }

        }
        return $html;
    }

}

function get_manager_name($id_m) {
    if(!$id_m) {
        return 'Неизвесто';
    }
    $sql = 'SELECT * FROM `manager` WHERE `id` = '.$id_m;
    $manager = get_data($sql);
    return $manager['orders'][0]['name'];
}
function get_client_name($id_c) {
    if(!$id_c) {
        return 'Неизвестно';
    }
    $sql = 'SELECT * FROM `client_code` WHERE `code_ac` = '.$id_c;
    if ($client = get_data($sql)) {
        if (isset($client['orders']) && !empty($client['orders'])) {
            return $client['orders'][0]['name'];
        }
    }
    return 'Неизвестно';
}
function get_place_name($id_p) {
    if(!$id_p) {
        return 'Не указан';
    }
    $sql = 'SELECT * FROM `PLACES` WHERE `PLACES_ID` = '.$id_p;
    $place = get_data($sql);
    return $place['orders'][0]['NAME'];
}
// if (isset($_GET['manager_id'])) {
    $sql = '';
    if(isset($_GET['filter_client'])) {
        if (stripos($_POST['keywordOnText'], 'select') === false && stripos($_POST['keywordOnText'], 'insert') === false && stripos($_POST['keywordOnText'], 'delete') === false && stripos($_POST['keywordOnText'], 'update') === false && stripos($_POST['keywordOnText'], 'union all') === false) {
            $sql = 'SELECT `c`.`name`, `c`.`client_id`, `c`.`code`, `c`.`tel`, `code`.`name` as `agent_name` FROM `client` as `c` 
                LEFT JOIN `client_code` as `code` ON `c`.`code` = `code`.`code_ac`
                WHERE `c`.`name` LIKE "%'.$_GET['filter_client'].'%" 
                OR `c`.`tel` LIKE "%'.trim($_GET['filter_client']).'%" 
                OR `code`.`name` LIKE "%'.trim($_GET['filter_client']).'%" 
                GROUP BY `c`.`name` LIMIT 10';
        }
        $clients_data = get_data($sql);

        echo json_encode($clients_data['orders']);
        exit;
    }
    
    if(isset($_GET['download_programm'])) {
        $programs_url = base64_decode($_GET['download_programm']);
        chown($programs_url, 'i.kotok:apache');
        chmod($programs_url, 0777);


        function get_dir_files( $dir, $recursive = true, $include_folders = false ){
            if( ! is_dir($dir) )
                return array();

            $files = array();

            $dir = rtrim( $dir, "/\\" ); // удалим слэш на конце

            foreach( glob( "$dir/{,.}[!.,!..]*", GLOB_BRACE ) as $file ){

                if( is_dir( $file ) ){
                    if( $include_folders )
                        $files[] = $file;
                    if( $recursive )
                        $files = array_merge( $files, call_user_func( __FUNCTION__, $file, $recursive, $include_folders ) );
                }
                else
                    $files[] = $file;
            }

            return $files;
        }

        $programs = get_dir_files($programs_url);

        $zip = new ZipArchive();

        $zip_url = $serv_main_dir.'/files/zips/'.mt_rand(11111111,99999999).'programm.zip';

        $zip->open($zip_url,ZIPARCHIVE::CREATE);

    //    chown($zip_url, 'i.kotok:apache');
        chmod($zip_url, 0777);


        foreach($programs as $file) {
            if (!$zip->addFile($file, str_replace($programs_url.'/', '', $file))) {
                exit ('Error adding');
            }
        }

        $zip->close();

        $download_url = str_replace($serv_main_dir,$main_dir.'/', $zip_url);
        header('Location: '.$download_url.'&nw='.$_GET['nw']);

        exit;
    }

    if (isset($_GET['manager'])) {$filter_manager = $_GET['manager'];}
    if (isset($_GET['client'])) {$filter_client = $_GET['client'];}
    if (isset($_GET['date'])) {$filter_date = $_GET['date'];}
    if (isset($_GET['order_id'])) {$filter_order_id = $_GET['order_id'];}
    if (isset($_GET['order_akcent'])) {$filter_order_akcent = $_GET['order_akcent'];}
    if (isset($_GET['place'])) {$filter_place = $_GET['place'];}
    if (isset($_GET['status'])) {$filter_status = $_GET['status'];}
    if (isset($_GET['type'])) {$filter_type = $_GET['type'];}
    if (isset($_GET['page'])) {$page = $_GET['page'];}

    if (isset($filter_type) && !empty($filter_type)) {
        if($filter_type == 1) {
            $sql .= 'SELECT `o`.`ID`,`o`.`DB_AC_ID`,`o`.`DB_AC_NUM`,`o`.`DB_AC_IN`,`o`.`manager`,`o`.`CLIENT`,`o`.`PLACE`,`o`.`status`,`p`.`ORDER1C`,`o`.`CLIENT_ID`, `o`.`readonly`
                     FROM `ORDER1C` as `o` 
                     JOIN `PROJECT` as `p` 
                     ON `o`.`ID` = `p`.`ORDER1C` 
                     WHERE `o`.`ID` = `p`.`ORDER1C`';
        } else if($filter_type == 4) {
            $sql .= 'SELECT `o`.`ID`,`o`.`DB_AC_ID`,`o`.`DB_AC_NUM`,`o`.`DB_AC_IN`,`o`.`manager`,`o`.`CLIENT`,`o`.`PLACE`,`o`.`status`,`p`.`ORDER1C`,`o`.`CLIENT_ID` FROM `ORDER1C` as `o` LEFT OUTER JOIN `PROJECT` as `p` ON `o`.`ID` = `p`.`ORDER1C` WHERE `p`.`ORDER1C` IS NULL';
        } else {
            $sql = "SELECT * FROM `ORDER1C` as `o` WHERE `o`.`ID` > 0 ";
        }
    } else {
        $sql = "SELECT * FROM `ORDER1C` as `o` WHERE `o`.`ID` > 0 ";
    }

    // if(isset($_GET['manager_id'])) {
    //     $_SESSION['manager_id'] = $_GET['manager_id'];
    // }
    if(!isset($_SESSION['manager_id']) && !isset($_SESSION['user'])) {
        header('Location: '.$main_dir.'/login.php'.'?nw='.$_GET['nw']);
    }
    $manager_data_sql = 'SELECT * FROM `manager` WHERE `id` = '.$_SESSION['manager_id'];
    $manager_data_info = get_data($manager_data_sql);
    if (!empty($manager_data_info)) {
        $manager_data_info = $manager_data_info['orders'][0];
        $_SESSION['isAdmin'] = $manager_data_info['admin'];
        //.. Массив для поддержки авторизации create.php
        if (!isset($_SESSION['user'])) {
            $role = $manager_data_info['admin'] == 1 ? 'admin' : 'manager';
            $user_data_for_RS_auth = array(
                'ID' => $manager_data_info['id'],
                'name' => $manager_data_info['name'],
                'phone' => $manager_data_info['phone'],
                'mail' => $manager_data_info['e-mail'],
                'role' => $role,
                'code' => $manager_data_info['code'],
            );
            $_SESSION['user'] = $user_data_for_RS_auth;
        }
    }
    if(isset($_SESSION['manager_id']) && !isset($_GET['manager'])) {
        $filter_manager = $_SESSION['manager_id'];
    }
    if(isset($filter_manager) && !empty($filter_manager) && $filter_manager != 'all') {
        $sql .= ' AND `o`.`manager` = '.$filter_manager;
    }
    if(isset($filter_client) && !empty($filter_client) && $filter_client != 'all') {
        $sql .= ' AND `o`.`CLIENT` = ' . $filter_client;
    }
    if(isset($filter_date) && !empty($filter_date) && $filter_date != 'all') {
    //    $post_date = new DateTime();
    //    $new_date = date_sub($post_date, date_interval_create_from_date_string($filter_date.' days'));
    //    $post_date = date_format($new_date,"Y-m-d H:i:s");
    //    $sql .= ' AND `o`.`DB_AC_IN` between "'.$post_date.'" and "'.date("Y-m-d H:i:s").'"';
        $sql .= ' AND `o`.`DB_AC_IN` between "'.date('Y').'-'.$filter_date.'-01 00:00:00" and "'.date('Y').'-'.$filter_date.'-31 00:00:00"';
    }
    if(isset($filter_order_id) && !empty($filter_order_id) && $filter_order_id != 'all') {
        $sql .= ' AND `o`.`DB_AC_ID` LIKE "%'.$filter_order_id.'%"';
    }
    if(isset($filter_order_akcent) && !empty($filter_order_akcent) && $filter_order_akcent != 'all') {
        $sql .= ' AND `o`.`DB_AC_NUM` LIKE "%'.$filter_order_akcent.'%"';
    }
    if(isset($filter_place) && !empty($filter_place) && $filter_place != 'all') {
        $sql .= ' AND `o`.`PLACE` = '.$filter_place;
    }
    if(isset($filter_status) && !empty($filter_status) && $filter_status != 'all') {
        $sql .= ' AND `o`.`status` = "'.$filter_status.'"';
    }
    $orders_count = get_data(str_replace('SELECT *', 'SELECT COUNT(ID)', $sql), 0);

    if(isset($filter_type) && $filter_type == 1) {
        $sql .= ' GROUP BY `o`.`ID`';
    }
    if(!isset($page) || $page == 'all') {
        $limits = ' LIMIT 100 OFFSET 0';
    } else {
        $limits = ' LIMIT 100 OFFSET '.($page-1)*100;
    }

    $sql .= ' ORDER BY `o`.`DB_AC_IN` DESC';
    $sql .= $limits;
    $orders_db = get_data($sql);

    $filtered_orders = $orders_db['orders'];

    $pages = $orders_count['orders'][0]['COUNT(ID)']/100;
    if(round($pages) >= $pages) {
        $pages = round($pages);
    } else if(round($pages) < $pages) {
        $pages = round($pages) + 1;
    }
// } else {
//     header('Location: '.$main_dir.'/login.php');
// }

////Filters
$sql_manager = 'SELECT * FROM `manager` ORDER BY `name`';
$managers = get_data($sql_manager);

$sql_places = 'SELECT * FROM `PLACES`';
$palces = get_data($sql_places);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="<?= GENERAL_JS ?>/jquery.min.js"></script>
    <link rel="stylesheet" href="<?= GENERAL_CSS ?>/bootstrap.min.css">
    <script src="<?= GENERAL_JS ?>/popper.min.js"></script>
    <script src="<?= GENERAL_JS ?>/bootstrap.min.js"></script>
    <script src="<?= GENERAL_JS ?>/jquery.mask.min.js"></script>
    <link rel="stylesheet" href="<?= GENERAL_CSS ?>/fontawesome/all.min.css">
    <title>Каталог заказов KRONAS</title>
    <style >
        .erase-tr{
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }
        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }
        .loading-search{
            position: absolute;
            right: 22px;
            top: 3px;
            display: none;
        }
        #searchOnCode-box{padding: 10px;display: none;}
        #searchOnCode-box li{ cursor: pointer; }
        form{
            margin: 10px;
        }
        h3.success  {
            color: green;
        }
        body {
            padding: 45px;
        }
        .del_this_order {
            width: 50px;
            margin-left: 5px;
        }
        .pagination {
            overflow-x: scroll;
        }
        #client_variants {
            position: absolute;
            z-index: 99999;
            width: 400px;
        }
        #client_variants .list-group-item {
            position: relative;
            display: block;
            padding: 0.4rem 1.25rem;
            border: 1px solid rgba(0,0,0,.125);
            background: lightgray;
        }
        .action_button_container {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
        }
        .action_button_container button{
            height: 47px;
        }
        .filter_actions_container > button {
            margin-left: 15px;
        }
        .action_button_container > a {
            margin: 0 5px;
        }
        .filter_actions_container {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-end;
            align-items: center;
        }
        .action_button_container i.fa-sync-alt, .action_button_container i.fa-times, .action_button_container i.fa-cloud-download-alt{padding: 4px 0;}
        td {
            font-size: 13px;
            min-width: 134px;
            vertical-align: middle;
        }
        th {
            font-size: 13px;
        }
        .form-control {
            color: tomato;
        }
        #client_variants_auth {
            position: absolute;
            z-index: 99999;
            width: 400px;
        }
        #client_variants_auth .list-group-item {
            position: relative;
            display: block;
            padding: 0.4rem 1.25rem;
            border: 1px solid rgba(0,0,0,.125);
            background: lightgray;
        }
        #myTab {
            float: right;
            margin-top: -10px;
        }
        .link-btn{padding: 6px 2px 5px;font-weight: bold;margin-right: 5px;}
        .dd-left{width: 240px;padding-top: 0;}
        .dd-left form{margin: 0;}
        .dd-left h6{
            padding: .5rem .75rem;
            margin-bottom: 0;
            font-size: 1rem;
            background-color: #f7f7f7;
            border-bottom: 1px solid #ebebeb;
            border-top-left-radius: calc(.3rem - 1px);
            border-top-right-radius: calc(.3rem - 1px);
        }
        .apply-project{font-size: 0.9rem;}
        .apply-project:hover{color: #007bff;}
        #project-info a{color: #fff;}
        #project-info a:hover{color: #fff;}
        #project-info i{padding: 0 8px;}

        .button-block{padding: 10px 0;border-bottom: 1px solid #dee2e6;}
        .active-choice{background-color: #138496;border-color: #138496;}
        .order-chioce{width: 100%;}
    </style>


</head>
<body>

<div class="container-fluid">
    <form action="./manager_all_order.php<?='?nw='.$_GET['nw']?>" method="GET">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Менеджер:</label>
                    <select class="form-control" id="filter_manager" name="manager">
                        <option value="all">все</option>
                        <?php foreach($managers['orders'] as $manager): ?>
                            <option value="<?= $manager['id'] ?>" <?php if($filter_manager == $manager['id']) echo 'selected="selected"'; ?>><?= $manager['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Дата:</label>
                    <select class="form-control" id="filter_date" name="date">
                        <option value="all" <?php if(isset($filter_date) && $filter_date == 'all') echo 'selected'; ?> >все</option>
                        <!--                        <option value="1" --><?php //if($filter_date == 1) echo 'selected'; ?><!-- >За сегодняшний день</option>-->
                        <!--                        <option value="7" --><?php //if($filter_date == 7) echo 'selected'; ?><!-- >За прошлую неделю</option>-->
                        <!--                        <option value="30" --><?php //if($filter_date == 30) echo 'selected'; ?><!-- >За прошлых 30 дней</option>-->
                        <!--                        <option value="365" --><?php //if($filter_date == 365) echo 'selected'; ?><!-- >За весь год</option>-->


                        <option value="01" <?php if(isset($filter_date) &&$filter_date == '01') echo 'selected'; ?>>Январь <?= date('Y') ?></option>
                        <option value="02" <?php if(isset($filter_date) &&$filter_date == '02') echo 'selected'; ?>>Февраль <?= date('Y') ?></option>
                        <option value="03" <?php if(isset($filter_date) &&$filter_date == '03') echo 'selected'; ?>>Март <?= date('Y') ?></option>
                        <option value="04" <?php if(isset($filter_date) &&$filter_date == '04') echo 'selected'; ?>>Апрель <?= date('Y') ?></option>
                        <option value="05" <?php if(isset($filter_date) &&$filter_date == '05') echo 'selected'; ?>>Май <?= date('Y') ?></option>
                        <option value="06" <?php if(isset($filter_date) &&$filter_date == '06') echo 'selected'; ?>>Июнь <?= date('Y') ?></option>
                        <option value="07" <?php if(isset($filter_date) &&$filter_date == '07') echo 'selected'; ?>>Июль <?= date('Y') ?></option>
                        <option value="08" <?php if(isset($filter_date) &&$filter_date == '08') echo 'selected'; ?>>Август <?= date('Y') ?></option>
                        <option value="09" <?php if(isset($filter_date) &&$filter_date == '09') echo 'selected'; ?>>Сентябрь <?= date('Y') ?></option>
                        <option value="10" <?php if(isset($filter_date) &&$filter_date == '10') echo 'selected'; ?>>Октябрь <?= date('Y') ?></option>
                        <option value="11" <?php if(isset($filter_date) &&$filter_date == '11') echo 'selected'; ?>>Ноябрь <?= date('Y') ?></option>
                        <option value="12" <?php if(isset($filter_date) &&$filter_date == '12') echo 'selected'; ?>>Декабрь <?= date('Y') ?></option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Участок:</label>
                    <select class="form-control" id="filter_place" name="place">
                        <option value="all">все</option>
                        <?php foreach($palces['orders'] as $place): ?>
                            <option value="<?= $place['PLACES_ID'] ?>" <?php if(isset($filter_place) && $filter_place == $place['PLACES_ID']) echo 'selected="selected"'; ?>><?= $place['NAME'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Клиент:</label>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Имя</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Телефон</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <input type="text" class="form-control" id="filter_client_name" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <input type="text" class="form-control" id="filter_client_phone" placeholder="введите телефон контрагента" name="filter_client_name" <?php if($_GET['filter_client_name']) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                        </div>
                    </div>
                    <div id="client_variants">
                        <ul class="list-group">

                        </ul>
                    </div>


                    <?php if (!isset($filter_client) && empty($filter_client)) { $filter_client = ''; } ?>
                    <input type="hidden" name="client" id="filter_client" value="<?= $filter_client ?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Статус:</label>
                    <select class="form-control" id="filter_status" name="status">
                        <option value="all" <?php if(isset($filter_status) && $filter_status == 'all') echo 'selected'; ?> >все</option>
                        <option value="в работе" <?php if(isset($filter_status) && $filter_status == 'в работе') echo 'selected'; ?> >в работе</option>
                        <option value="выполнен" <?php if(isset($filter_status) && $filter_status == 'выполнен') echo 'selected'; ?> >выполнен</option>
                        <option value="черновик" <?php if(isset($filter_status) && $filter_status == 'черновик') echo 'selected'; ?> >черновик</option>
                        <option value="отменен" <?php if(isset($filter_status) && $filter_status == 'отменен') echo 'selected'; ?> >отменен</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Номер в Акценте:</label>
                    <input type="text" class="form-control" id="filter_order_akcent_name" placeholder="введите например к739/6" name="order_akcent"  <?php if(isset($filter_order_akcent)) echo 'value="'.$filter_order_akcent.'"' ?>>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlInput1">ID в Акценте:</label>
                    <input type="text" class="form-control" id="filter_order_id_name" placeholder="введите например 9466448" name="order_id" <?php if(isset($filter_order_id)) echo 'value="'.$filter_order_id.'"' ?>>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Тип:</label>
                    <select class="form-control" id="filter_type" name="type">
                        <option value="all" <?php if(isset($filter_type) && $filter_type == 'all') echo 'selected'; ?> >все</option>
                        <option value="1" <?php if(isset($filter_type) && $filter_type == 1) echo 'selected'; ?> >пильные</option>
                        <option value="4" <?php if(isset($filter_type) && $filter_type == 4) echo 'selected'; ?> >обычные</option>
                    </select>
                </div>
            </div>
            <div class="col-md-7">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                    Новый заказ
                </button>
                <a class="btn btn-success" href="<?= $main_dir ?>/client_registration.php" target="_blank">
                    Создать нового контрагента
                </a>
                <?php if ($_SESSION['user']['role'] == 'admin' && $_SESSION['user']['access'] >= 100): ?>
                    <a class="btn btn-success" href="<?= DIR_ADMIN ?>">Админка</a>
                <?php endif ?>
            </div>
            <div class="col-md-1">
                <a href="./login.php?logout_manager=1" class="btn btn-danger">Выйти</a>
            </div>
            <div class="col-md-4">
                <div class="filter_actions_container">
                    <a href="<?= $main_dir ?>/manager_all_order.php">Очистить фильтры</a>
                    <button type="submit" class="btn btn-primary">Фильтровать</button>
                </div>

            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <!--                    <th scope="col">ID</th>-->
                    <th scope="col">ID в Акценте</th>
                    <th scope="col">Номер в Акценте</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Файлы</th>
                    <th scope="col">Менеджер</th>
                    <th scope="col">Клиент</th>
                    <th scope="col">Участок</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($filtered_orders as $order): ?>
                    <?php if ($order['del_order'] != 1): ?>
                        
                        <?php 
                            $order23='<a href="' . $files_dir . 'fb/index.php?id='.base64_encode($order['DB_AC_ID']).'" target="_blank">Файлы</a>';
                            $sql = 'SELECT * FROM `order1c_files_generate` WHERE `order` = '.$order['ID'];
                            $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                            if (count($res)>0)
                            {
                                // p_($order);exit;
                                $order['DB_AC_ID']='<a href="show_log_file.php?order='.$order['ID'].'" target="_blank">'.$order['DB_AC_ID'].'</a>';
                                $order['DB_AC_NUM']='<a href="show_log_file.php?order='.$order['ID'].'" target="_blank">'.$order['DB_AC_NUM'].'</a>';
                                $order['DB_AC_IN']='<a href="show_log_file.php?order='.$order['ID'].'" target="_blank">'.$order['DB_AC_IN'].'</a>';

                            }
                        ?>

                        <tr class="order-line" data-id="<?= $order['ID'] ?>">
                            <td><?= $order['DB_AC_ID'] ?></td>
                            <td><?= $order['DB_AC_NUM'] ?></td>
                            <td><?= $order['DB_AC_IN'] ?></td>
                            <td><?= $order23 ?></td>

                            <td><?= get_manager_name($order['manager']) ?></td>
                            <td><?= get_client_name($order['CLIENT']) ?></td>
                            <td><?= get_place_name($order['PLACE']) ?></td>
                            <td><?= $order['status'] ?></td>
                            <td>
                                <div class="action_button_container">
                                    <?php if(get_order_edit_variants($order['ID'])): ?>
                                        <?php if ($order['rs_order'] == 1): ?>
                                            <?php
                                            $sql = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$order['ID'].' ORDER BY `DATE` DESC';
                                            $projects = get_data($sql);
                                            //.. Кнопка раздвижной системы
                                            ?>
                                            <button type="button" class="btn btn-success link-btn" id="project-info" title="Посмотреть деталировку раздвижной системы"><a href="<?=$main_dir.'/rs_detail.php?ip='.$projects['orders'][0]['PROJECT_ID']?>" id="<?=$projects['orders'][0]['PROJECT_ID']?>" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i></a></button>
                                            <?php //.. Кнопка раздвижной системы ?>
                                            <div class="dropleft" title="Открыть версии проектов заказа" >
                                                <button type="button" class="btn btn-success link-btn" data-toggle="dropdown" data-html="true">[РС]</button>
                                                <div class="dropdown-menu dd-left">
                                                    <h6>Версии проектов заказа:</h6>
                                                    <?php echo get_order_edit_variants($order['ID'], 'RS'); ?>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                        <button type="button" class="btn btn-success" data-toggle="popover" title="Версии проектов заказа:" data-html="true" data-content='<?php echo get_order_edit_variants($order['ID']); ?>' title="Редактировать"><i class="fas fa-edit"></i></button>
                                        <?php if($order['status'] != 'черновик' || (stripos($_SERVER['SERVER_NAME'], '.loc') && $_SESSION['user']['ID'] == 218)): ?>
                                            <a href="<?= $programm_link ?>/refresh_project_programm.php?project=<?= $order['ID'] ?>" target="_blank" class="btn btn-info" title="Обновить программы"><i class="fas fa-sync-alt"></i></a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <?php if($order['status'] != 'черновик' && $order['readonly'] == 1 || (stripos($_SERVER['SERVER_NAME'], '.loc') && $_SESSION['user']['ID'] == 218)): ?>
                                            <a href="http://kronas-service.dn-kronas.local/gibservice/doTestApp.asp?order=<?= $order['ID'] ?>" target="_blank" class="btn btn-info" title="Обновить программы"><i class="fas fa-sync-alt"></i></a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if(($order['status'] == 'черновик' && $order['manager'] == $_SESSION['manager_id']) || ($order['status'] !== 'черновик' && $order['manager'] == $_SESSION['manager_id'] && $_SESSION['user']['role'] == 'admin')): ?>
                                        <button name="del_this_order" data-id="<?= $order['ID'] ?>" class="btn btn-danger del_this_order"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    <?php endif; ?>

                                    <?php if(isset($_SESSION['isAdmin']) && ($_SESSION['isAdmin'] == 1) && $order['programm'] && $order['status'] != 'черновик'): ?>
                                        <a href="<?= $download_program ?>manager_all_order.php?download_programm=<?= base64_encode($order['programm']) ?>" class="btn btn-info" target="_blank" title="Скачать программы"><i class="fas fa-cloud-download-alt"></i></a>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php for($i = 1; $i <= $pages; $i++): ?>
                        <li class="page-item <?php echo $page == $i ? 'active' : ''; ?>"><a class="page-link" href="<?= $main_dir ?>/manager_all_order.php?page=<?=$i?>&manager=<?= $_GET['manager'] ?>&client=<?= $_GET['client'] ?>&date=<?= $_GET['date'] ?>&status=<?= $_GET['status'] ?>&order_akcent=<?= $_GET['order_akcent'] ?>&place=<?= $_GET['place'] ?>&order_id=<?= $_GET['order_id'] ?>&type=<?= $_GET['type'] ?>$filter_client_name=<?= $_GET['filter_client_name']?>"><?=$i?></a></li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>


<script>
    var client_filter_ckecker = 0;
    async function get_clients_filtered(client_id, phone = 0) {
        if(client_filter_ckecker == 1) {
            return false;
        }
        client_filter_ckecker = 1;
        console.log('clear');
        $('#client_variants ul').html(' ');
        console.log('clearz');
        $('#client_variants ul').html(`
            <div id="checks_lodaer_containers_auth">
              <div style="background: #f5f3f3;padding: 45px;display: flex;flex-flow: row nowrap;justify-content: center;align-items: center;position: absolute;width: 100%;height: 100%;z-index: 999999;">
                  <img src="<?= $main_dir ?>/kronasapp/public/images/download.gif" alt="" width="45px">
              </div>
            </div>
        `);
        if(!client_id) {
            $('#filter_client').val('');
            if(phone == 1) {
                $('#filter_client_phone').val('');
            } else {
                $('#filter_client_name').val('');
            }

            return;
        }
        let request = await fetch('<?= $main_dir ?>/manager_all_order.php?filter_client=' + client_id);
        let response = await request.json();
        let counter = 0;
        await response.forEach(function (element) {
            if(counter <= 10) {
                $('#client_variants ul').append(`
                    <li class="list-group-item" data-client="${element.code}" data-phone="${element.tel}">${element.name} (${element.agent_name})</li>
                `);
            }
            counter++;
        });
        $('#client_variants ul li').on('click', function (e) {
            let current_variant = $(e.target).attr('data-client');
            let current_phone = $(e.target).attr('data-phone');
            let current_variant_name = $(e.target).html();
            $('#filter_client').val(current_variant);
            if(phone == 1) {
                $('#filter_client_phone').val(current_phone);
            } else {
                $('#filter_client_name').val(current_variant_name);
            }
            $('#client_variants ul').html('   ');
        });
        setTimeout(function () {
            $('#checks_lodaer_containers_auth').hide();
            client_filter_ckecker = 0;
        },1000);
    }





    async function get_clients_filtered_auth(client_id, phone = 0) {
        if(client_filter_ckecker == 1) {
            return false;
        }
        client_filter_ckecker = 1;
        $('#client_variants_auth ul').html(' ');
        $('#client_variants_auth ul').html(`
            <div id="checks_lodaer_containers">
              <div style="background: #f5f3f3;padding: 45px;display: flex;flex-flow: row nowrap;justify-content: center;align-items: center;position: absolute;width: 100%;height: 100%;z-index: 999999;">
                  <img src="<?= $main_dir ?>/kronasapp/public/images/download.gif" alt="" width="45px">
              </div>
            </div>
        `);
        if(!client_id) {
            $('#filter_client_auth').val('');
            if(phone == 1) {
                $('#filter_client_phone_auth').val('');
            } else {
                $('#filter_client_name_auth').val('');
            }
            return;
        }
        let request = await fetch('<?= $main_dir ?>/manager_all_order.php?filter_client=' + client_id);
        // let request = await fetch('http://service.kronas.com.ua/manager_all_order.php?filter_client=' + client_id);
        let response = await request.json();
        let counter = 0;
        await response.forEach(function (element) {
            if(counter <= 10) {
                $('#client_variants_auth ul').append(`
                    <li class="list-group-item" data-client="${element.client_id}" data-code="${element.code}" data-phone="${element.tel}">${element.name} (${element.agent_name})</li>
                `);
            }
            counter++;
        });
        $('#client_variants_auth ul li').on('click', function (e) {
            let current_variant = $(e.target).attr('data-client');
            let current_code = $(e.target).attr('data-code');
            let current_phone = $(e.target).attr('data-phone');
            let current_variant_name = $(e.target).html();
            $('#filter_client_auth').val(current_variant);
            $('#filter_client_code_auth').val(current_code);
            if(phone == 1) {
                $('#filter_client_phone_auth').val(current_phone);
            } else {
                $('#filter_client_name_auth').val(current_variant_name);
            }
            $('#client_variants_auth ul').html('   ');
        });
        setTimeout(function () {
            $('#checks_lodaer_containers').hide();
            client_filter_ckecker = 0;
        },1000);
    }

    $(document).ready(function () {
        $('#filter_client_name').on('input', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered(client_data);
        });
        $('#filter_client_phone').on('input', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered(client_data, 1);
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
        $('#filter_client_phone').mask('+38(000)000-00-00', {
            placeholder: "+38(0__)___-__-__"
        });
        $('#myTab .nav-item .nav-link').on('click', function() {
            $('#filter_client_phone').val('');
            $('#filter_client_name').val('');
            $('#client_variants ul').html(' ');
        });

    });
    //.. 12.11.2020 Изменения: делегирование. $('#new-order-form').on
    $(document).ready(function () {
        $('#new-order-form').on('input', '#filter_client_name_auth', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered_auth(client_data);
        });
        $('#new-order-form').on('input', '#filter_client_phone_auth', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered_auth(client_data, 1);
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
        $('#filter_client_phone_auth').mask('+38(000)000-00-00', {
            placeholder: "+38(0__)___-__-__"
        });
        $('#new-order-form').on('click', '#myTab1 .nav-item .nav-link', function() {
            $('#filter_client_phone_auth').val('');
            $('#filter_client_name_auth').val('');
            $('#client_variants_auth ul').html(' ');
        });
        $('#new-order-form').on('click', '#client_auth_form_button', function (e) {
            e.preventDefault();
            $('#client_auth_message').html(' ');
            if($('#filter_client_auth').val() == null || $('#filter_client_auth').val() == '' || $('#filter_client_code_auth').val() == null || $('#filter_client_code_auth').val() == '') {
                $('#client_auth_message').html('<span style="color:red">Необходимо выбрать клиента из выпадающего списка!</span>');
            } else {
                $('#sac_form').submit();
            }
        })
    });
    //.. 12.11.2020 Функция выбора заказа
    $(document).ready(function () {
        $('.order-chioce').on('click', function() {
            let order = $(this).attr('id');
            if (order == 'PZ') {
                $('#RS').removeClass('active-choice');
                if (!$('#PZ').hasClass('active-choice')) {
                    $('#PZ').addClass('active-choice');
                }
            }
            if (order == 'RS') {
                $('#PZ').removeClass('active-choice');
                if (!$('#RS').hasClass('active-choice')) {
                    $('#RS').addClass('active-choice');
                }
            }
            $.post("ajax_resp.php", {"newOrderForm": order}).done(function (data) {
                $('#new-order-form').fadeOut(250, function(){
                    $(this).html(data);
                    $(this).fadeIn(500);
                });
            });
        });
    });

    //========================================================================================================================
    //===================================== Удаление заказа из хранилища по ID ===============================================
    $(document).ready(function () {
        $('.del_this_order').on('click', async function() {
            let order_id = $(this).attr('data-id')

            const formData = new FormData();
            formData.append('deleteOrderById', order_id);
            
            try {
                const response = await fetch('/ajax_resp.php', {
                    method: 'POST',
                    body: formData
                });
                const result = await response.text();
                if (result == 1) {
                    $('tr.order-line[data-id="'+order_id+'"]').hide('fast');
                }
            } catch (error) {
                console.error('Ошибка:', error);
            }
        })
    });
    //===================================== Удаление заказа из хранилища по ID ===============================================
    //========================================================================================================================
</script>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Выберите вид заказа и клиента:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="button-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <button id="PZ" class="btn btn-info order-chioce active-choice">Пильный заказ</button>
                        </div>
                        <div class="col-md-6">
                            <button id="RS" class="btn btn-info order-chioce">Раздвижная система</button>
                        </div>
                    </div>
                </div>
            </div> -->
            <div id="new-order-form">
                <form action="<?= $laravel_dir ?>/start_new_porject?saveAutorization=<?=$_SESSION['saveAutorization'].'?nw='.$_GET['nw']?>" method="GET" id="sac_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Клиент:</label>
                            <ul class="nav nav-tabs" id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-selected="true">Имя</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="false">Телефон</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent1">
                                <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
                                    <input type="text" class="form-control" id="filter_client_name_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                                </div>
                                <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                                    <input type="text" class="form-control" id="filter_client_phone_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                                </div>
                            </div>
                            <div id="client_variants_auth">
                                <ul class="list-group">

                                </ul>
                            </div>


                            <input type="hidden" name="client_id" id="filter_client_auth" value="">
                            <input type="hidden" name="manager_id" id="filter_manager_auth" value="<?= $_SESSION['manager_id'] ?>">
                            <input type="hidden" name="client_code" id="filter_client_code_auth" value="">
                            <input type="hidden" name="set_new_order" value="1">
                        </div>
                        <div class="form-group" id="client_auth_message">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="client_auth_form_button">Создать пильный заказ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>





