<?php
date_default_timezone_set('Europe/Kiev');
session_start();

include_once ("func.php");


/// Чистим папки заказов, где readonly = 0

// $sql_get_not_readonly_orders = 'SELECT `DB_AC_ID` FROM `ORDER1C` WHERE `readonly` = 1 AND `status` != "черновик" AND `status` != "выполнен" AND `status` != "отменен" AND `production` = 1 AND `programm` IS NULL';
// $not_readonly_orders = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_get_not_readonly_orders)['data'];

// foreach ($not_readonly_orders as $o) {
// //    delDir('');
// }

// /// Чистим папки заказов, которые уже выполнены
// $sql_get_complete_orders = 'SELECT `DB_AC_ID` FROM `ORDER1C` WHERE `status` = "выполнен" AND `production` = 1';
// $complete_orders = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_get_not_readonly_orders)['data'];

// foreach ($not_readonly_orders as $o) {
// //    delDir('');
// }

$checker = 0;
$cycle_works = true;

echo '
>>> Entering to while processing...
';

while($cycle_works) 
{

    /// Создаём программы для заказов, запущенных в работу
    /// AND `o`.`readonly` = 1

    $sql = 'SELECT * FROM `ORDER1C` as `o` JOIN `PROJECT` as `p` ON (`o`.`ID` = `p`.`ORDER1C`) WHERE `stanki` IS NULL AND `o`.`readonly` = 1 AND `o`.`ID` = `p`.`ORDER1C` AND `o`.`production` = 1  AND `o`.`status` = "в работе" AND `o`.`comment` != "checked" AND `o`.`comment` != "problem" AND `o`.`DB_AC_ID` > 0 GROUP BY `o`.`ID` ORDER BY `o`.`ID` DESC LIMIT 1';
    $order_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

    if($order_data['ID']>0) 
    {
        echo '
        >>> Generating new programs...
        ';

        echo '
        ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ';
            echo 'Генерация новых программ';
            echo '
        ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ';
            echo 'Aкцент ID: '.$order_data['DB_AC_ID'];
            echo '
        ID в сервисе: '.$order_data['ORDER1C'];
            echo '
        Номер в Акценте: '.$order_data['DB_AC_NUM'];
            echo '
        Клиент: '.$order_data['client_fio'];
            echo '
        Дата: '.$order_data['DATE'];
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'need_to_new_generate '.$order_data['DB_AC_NUM'], $_SESSION);

        $sql = 'SELECT * FROM `PROJECT` WHERE ORDER1C='.$order_data['ID'].' ORDER BY `PROJECT_ID` DESC LIMIT 1';
        
        $project_id = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['PROJECT_ID'];
        
        // foreach ($order_datas as $o) 
        // {
        //     if(check_project_order_time($o['ID'])) 
        //     {
        //         $order_data = $o;
        //         echo '
        //         +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //         ';
        //                     echo 'Перегенирация программ на более свежие';
        //                     echo '
        //         ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //         ';
        //             break;
        //     }

        // }

                // }

            // if($order_data['ID'] > 0) 
            // {
        echo '
        >>> Refreshing current programs...
        ';
        
        if(file_put_contents($serv_main_dir . '/check_programms_generator_work.txt', date("Y-m-d H:i:s"))) 
        {
            echo '
            ++++++++++ Время работы зафиксировано';
        }
        else 
        {
            echo '
            ++++++++++ ОШИБКА фиксации времени работы ';
        }
        
        //..27.11.2020 Меняем условие, читаем из файла, а не из базы:
        if($file_data = get_Project_file($project_id)) p_($file_data);
        else echo 'file <b>'.$project_id.'.txt</b> not found!';
        
        if ($file_data && !empty($file_data)) 
        {
            $order_id = $order_data['ID'];
            
            echo '
            ++++++++++ >>> refresh_project_programm [order_id = ' . $order_id . '] >>>
            ';
            unset($t);
            include('refresh_project_programm.php');
            
            if($t == true) 
            {
                $sql_update_order = 'UPDATE `ORDER1C` SET `comment`= "checked" WHERE `ID` = '. $order_id;    
                if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_order)['res']<>1) echo 'Проблема с обновлением записи checked';
                $sql_update = 'UPDATE `ORDER1C` SET `stanki`=1, `stanki_time` = CURRENT_TIMESTAMP,`del_programm` = NULL WHERE `ID` = '.$order_id;
                if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update)['res']<>1) echo 'Проблема с установкой stanki';
                echo 'Программы на сервисе успешно обновлены!
                Запускаем отправку на станки...
                ';
                echo '+++++++++++++++++++++++++ OK  ++++++++++++++++++++++++++++++';
                sleep(5);     
            }
            else
            {
                $sql_update_order = 'UPDATE `ORDER1C` SET `comment`= "problem" WHERE `ID` = '. $order_id;    
                if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_order)['res']<>1) echo 'Проблема с обновлением записи checked';
            }

        } 
        else 
        {
            sleep(5);
        }
    }
}

?>