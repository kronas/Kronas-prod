<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");
header("Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization");
header("Access-Control-Allow-Headers: *");

error_reporting(E_ERROR);
ini_set("display_errors", 1);

session_start();

date_default_timezone_set('Europe/Kiev');
include_once ("func.php");

if($_GET['project']) {
	$order_id = $_GET['project'];
}

if (($order_id>0)&&(is_numeric($order_id)))
{
    $request_order = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$order_id;
    $data_order=sql_data(__LINE__,__FILE__,__FUNCTION__,$request_order)['data'][0];

    $request_project = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$order_id.' ORDER BY `DATE` DESC';
    $data_project=sql_data(__LINE__,__FILE__,__FUNCTION__,$request_project)['data'][0];

	//..23.11.2020 Читаем из файла
	if(file_exists($serv_main_dir.'/files/project_out/'.$data_project['PROJECT_ID'].'.txt')){
	    //.. Читаем из файла
	    $_SESSION['project_data'] = base64_decode(file_get_contents($serv_main_dir.'/files/project_out/'.$data_project['PROJECT_ID'].'.txt'));
	} else {
    	$_SESSION['project_data'] =  base64_decode($data_project['MY_PROJECT_OUT']);
	}
}
else die ('Заказ order_id не распознан');

if($_POST['project_data']) {
    $_SESSION['project_data'] = $_POST['project_data'];
}

$_SESSION['user_place'] = $data_order['PLACE'];

$project = $data_project['PROJECT_ID'];
$new_order_id = $data_order['DB_AC_ID'];

$_SESSION['project_data']=gl_55 (__LINE__,__FILE__,__FUNCTION__,$_SESSION['project_data']);
$_SESSION['project_data'] = xml_parser($_SESSION['project_data']);

$p = xml_parser_create();
xml_parse_into_struct($p, $_SESSION['project_data'], $vals, $index);
xml_parser_free($p);

$vals_without_gr=change_gr($vals);

put_programm($vals_without_gr,$_SESSION['user_place'],$link, $project, $new_order_id, $serv_main_dir,$_SESSION,str2url($data_order['DB_AC_NUM']));

echo 'Программы на сервисе успешно обновлены!


';