<?php

if(isset($send_specification)) {
    $data=$send_specification['data'];
}
else
{
    $data=json_decode($_POST['data']);
}

$tbl1='';
if(count($data->table1)>0)
{
$tbl1='
                <table>
                    <thead>
                    <tr>
                        <th scope="col">Материал</th>
                        <th scope="col">Наименование</th>
                        <th scope="col" class=" text-center">Количество (ед. имз.)</th>
                        <th scope="col" class=" text-center">Цена</th>
                        <th scope="col" class=" text-right">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>';
    foreach ($data->table1 as $value)
    {
        $tbl1.='<tr><td>'.$value[0].'</td><td>'.$value[1].'</td><td class=" text-center">'.$value[2].'</td><td class=" text-center">'.$value[3].'</td><td class=" text-right">'.$value[4].'</td></tr>';
    }
    $tbl1.='<tr><td colspan="5" class="total">'.$data->total[0].'</td></tr></table><br><br>';
}
// print_r($_POST);exit;

$tbl2='';
if(count($data->table2)>0)
{
$tbl2='
                <table>
                    <thead>
                    <tr>
                        <th scope="col">Услуги</th>
                        <th scope="col">Наименование</th>
                        <th scope="col" class=" text-center">Количество (ед. имз.)</th>
                        <th scope="col" class=" text-center">Цена</th>
                        <th scope="col" class=" text-right">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>';
    foreach ($data->table2 as $value)
    {
        $tbl2.='<tr><td>'.$value[0].'</td><td>'.$value[1].'</td><td class=" text-center">'.$value[2].'</td><td class=" text-center">'.$value[3].'</td><td class=" text-right">'.$value[4].'</td></tr>';
    }
    $tbl2.='<tr><td colspan="5" class="total">'.$data->total[1].'</td></tr></table><br><br>';
}


$tbl3='';
if(count($data->table3)>0)
{
$tbl3='
                <table>
                    <thead>
                    <tr>
                        <th scope="col">Деталь</th>
                        <th scope="col">Материал</th>
                        <th scope="col">Описание</th>
                        <th scope="col" class=" text-center">Количество</th>
                        <th scope="col" class=" text-center">Цена</th>
                        <th scope="col" class=" text-right">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>';
    foreach ($data->table3 as $value)
    {
        $tbl3.='<tr><td>'.$value[0].'</td><td>'.$value[1].'</td><td>'.$value[2].'</td><td class=" text-center">'.$value[3].'</td><td class=" text-center">'.$value[4].'</td><td class=" text-right">'.$value[5].'</td></tr>';
    }
    $tbl3.='<tr><td colspan="6" class="total">'.$data->total[2].'</td></tr></table><br><br>';
}

$tbl4='';
if(count($data->table4)>0)
{
    $tbl4='<h3>Дополнительно:</h3>
                <table>
                    <thead>
                    <tr>
                        <th scope="col">Код</th>
                        <th scope="col">Наименование</th>
                        <th scope="col" class=" text-center">Количество</th>
                        <th scope="col" class=" text-center">Цена</th>
                        <th scope="col" class=" text-right">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>';
    foreach ($data->table4 as $value)
    {
        $tbl4.='<tr><td>'.$value[0].'</td><td>'.$value[1].'</td><td class=" text-center">'.$value[2].'</td><td class=" text-center">'.$value[3].'</td><td class=" text-right">'.$value[4].'</td></tr>';
    }
    $tbl4.='<tr><td colspan="5" class="total">'.$data->total[3].'</td></tr></table><br><br>';
}
$test=$tbl1.$tbl2.$tbl3.$tbl4.
'<div class="subtotal">'.$data->total[4].'</div>';

require_once __DIR__ . '/vendor/autoload.php';
// Create an instance of the class:
$mpdf = new \Mpdf\Mpdf();
$stylesheet = file_get_contents('css/pdf-style.css');


// Write some HTML code:
$mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($test,\Mpdf\HTMLParserMode::HTML_BODY);


// Output a PDF file directly to the browser
if(isset($send_specification)) {
    $mpdf->Output($send_specification['target'].'specification.pdf');
}
else
{
    $mpdf->Output();
}
