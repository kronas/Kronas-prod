<?php

include_once '_1/config.php';
include_once 'func.php';

// Форматированный вывод
function _print($val, $i = false){
    if(is_array($val) || is_object($val)){
        echo "<pre>";
        if($i == false) print_r($val);
        else var_dump($val);
        echo "</pre>";
    }else{ echo $val; }
}
// Вывод строки с переводом строки
function _eho($val = null){
    if (!$val) { echo '<br />'; }
    else { echo '<p>'.$val.'</p>'; }
    
}

/**
* Запись в файл
**/
function fwrite_stream($fp, $string) {
    for ($written = 0; $written < strlen($string); $written += $fwrite) {
        $fwrite = fwrite($fp, substr($string, $written));
        if ($fwrite === false) {
            return $written;
        }
    }
    return $written;
}

/**
* Запись в PDF-файл спецификации РС
* Функция принимает ID заказа или проекта.
* Функция ищет в дисковом хранилище данные проекта, а если не находит, то берёт из БД
**/
function RSDetailToPDF($id) {
    global $serv_main_dir;
    $pr_id = null;
    $result = null;
    $project_storage = $serv_main_dir.'/files/project_out/';
    $temp_storage = $serv_main_dir.'/files/temp/';

    if (!file_exists($project_storage.$id.'.txt')) {
        if($result = getTableData('SELECT `PROJECT_ID` FROM `PROJECT` WHERE `ORDER1C` = "'.$id.'" ORDER BY `PROJECT_ID` DESC')) {
            $project_id = $result[0]['PROJECT_ID'];
            $pr_id = $project_id;
            $result = $project_storage.$result[0]['PROJECT_ID'].'.txt';
            if (file_exists($result)) {
                $result = file_get_contents($result);
            } else {
                if($result = getTableData('SELECT `MY_PROJECT_OUT` FROM `PROJECT` WHERE `PROJECT_ID` = "'.$project_id.'"')) {
                    if ($result[0]['MY_PROJECT_OUT'] != '') {
                        $result = $result[0]['MY_PROJECT_OUT'];
                    }
                }
            }
        }elseif($result = getTableData('SELECT `MY_PROJECT_OUT` FROM `PROJECT` WHERE `PROJECT_ID` = "'.$id.'"')) {
            $pr_id = $id;
            if ($result[0]['MY_PROJECT_OUT'] != '') {
                $result = $result[0]['MY_PROJECT_OUT'];
            }
        }
    } else {
        if (file_exists($project_storage.$id.'.txt')) {
            $result = file_get_contents($project_storage.$id.'.txt');
        }
    }
    $result = RS_from_project(base64_decode($result));
    $result['project_id'] = $pr_id;
    return $result;
}

/**
* Функция для отчётов по РС
* Принимает id проекта
* Возвращает массив с клиентом и филиалом
**/
function getPlaceAndClient($id) {
    $data = array();
    $manager = array();
    $client = array();
    $place = array();
    if ($result = getTableData('SELECT * FROM `PROJECT` WHERE `PROJECT_ID` = "'.$id.'"')) {
        $client['date'] = $result[0]['DATE'];
        if ($result = getTableData('SELECT * FROM `ORDER1C` WHERE `ID` = "'.$result[0]['ORDER1C'].'"')) {
            if ($res = getTableData('SELECT * FROM `manager` WHERE `id` = "'.$result[0]['manager'].'"')) {
                $manager['name'] = $res[0]['name'];
                $manager['phone'] = $res[0]['phone'];
                $manager['mail'] = $res[0]['e-mail'];
            } else $manager = '';
            $client['name'] = $result[0]['client_fio'];
            $client['phone'] = $result[0]['client_phone'];
            $client['mail'] = $result[0]['client_email'];
            if ($result = getTableData('SELECT * FROM `places` WHERE `PLACES_ID` = "'.$result[0]['PLACE'].'"')) {
                $place[0] = $result[0]['NAME'];
                $place[1] = $result[0]['ADRESS'];
                $place[2] = $result[0]['PHONE'];
            } else $place = '';
        }
    } else{
        $manager = '';
        $client = '';
        $place = '';
    }
    $data[0] = $client;
    $data[1] = $place;
    $data['manager'] = $manager;

    return $data;
}

/**
* Удаляем MY_PROJECT_OUT (MPO) из базы и записываем в файл
**/
function deleteMPO($id) {
    global $serv_main_dir;
    $folder = $serv_main_dir.'/files/project_out/';
    $connect = db_connect();
    // Если MPO не пустая строка
    $MPO = getTableData('SELECT `MY_PROJECT_OUT` FROM `PROJECT` WHERE `PROJECT_ID` = "'.$id.'"')[0]['MY_PROJECT_OUT'];
    if (!empty($MPO)) {
        // Если нет файла с именем $id
        if (!file_exists($folder.$id.'.txt')) {
            $file = fopen($folder.$id.'.txt', 'w');
            fwrite_stream($file, $MPO);
            fclose($file);
        }
        // Если файл создан, то удаляем MY_PROJECT_OUT из базы
        if (file_exists($folder.$id.'.txt')) {
            mysqli_query($connect, 'UPDATE `PROJECT` SET `MY_PROJECT_OUT` = "" WHERE `PROJECT_ID` = "'.$id.'"');
        }
    }
    mysqli_close($connect);
}

/**
* Функция возвращает project_data по id проекта
**/
function getPD($id){
    global $serv_main_dir;
    $project_storage = $serv_main_dir.'/files/project_out/';
    if (file_exists($project_storage.$id.'.txt')) {
        return file_get_contents($project_storage.$id.'.txt');
    } else {
        return false;
    }
}

/**
* Парсинг из подготовленного url-а
* Если $post установлен в true, то должны передаваться и данные (в формате json)
**/
function getStdParse($url, $post = false, $data = false) {
    global $kronas_api_link;
    $res = null;
    $a = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    if ($post) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_USERAGENT, $a);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $res = curl_exec($ch);
    curl_close($ch);
    return $res;
}

function clearClientsPhone($phone) {
    $phone = str_replace('+38', '', $phone);
    $phone = str_replace('(', '', $phone);
    $phone = str_replace(')', '', $phone);
    $phone = str_replace('-', '', $phone);
    return $phone;
}

/**
* Записывает данные из .CSV файла в массив
**/
function csv_to_array($file){
    if(!file_exists($file) || !is_readable($file)){ //Если файл не найден, либо нечитаем, то функция возвращает FALSE
        return FALSE;
    }
    $header = NULL;
    $result = [];
    $data = [];
    if (($handle = fopen($file, 'r')) !== FALSE) { // Если файл корректно открыт, и не вернул FALSE
        while (($row = fgetcsv($handle, 3000, ';')) !== FALSE){ // Пока не закончится файл (массив)...
            if(!$header){ // Если $header равен нулю (или FALSE (то же самое))
                $header = $row; // Помещаем в $header первую строку (ключ массива)
                if(strripos($header[0], '﻿') == true){$header = str_replace('﻿', '', $header);}
                }else // А если (второй и последующие проходы) не равен
                $result[] = array_combine($header, $row) or die("<p><b>csv_to_array</b>. Несоответствие количества полей заголовку, строка: <b>".(count($result) + 1)."</b></p>"); // array_combine — Создает новый массив, используя один массив в качестве ключей, а другой для его значений
            }
        fclose($handle); // fclose закрывает дескриптор файла. Дескриптор должен указывать на файл, открытый ранее с помощью функции fopen() или fsockopen().
    }
    $data = $result;
    $result = null;
    return $data; // Возвращает в переменной массив, извлечённый из файла
}
?>