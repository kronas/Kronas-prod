<?php
/**
 * ----------------------------------------------------------------------------------
 * ИМПОРТ БАЗИС ПРОЕКТОВ
 * 
 * Импорт писался в первую очередь для корректной конвертации сшивок Базиса в Гиблаб.
 * Процесс заключается в: 
 *	 1) разборке Базис-проекта на запчасти, 
 * 	 2) внесении нужных изменений
 * 	 3) и пересборке проекта для Гиблаб.
 *     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Структура проекта:
 * Изделия (typeId = product)
 * Инструмент для раскроя (typeId = tool.cutting)
 * Материал для раскроя (typeId = sheet)
 * Операции листового раскроя (typeId = CS)
 * Инструмент для кромкования (typeId = tool.edgeline)
 * Материал для кромкования (typeId = band)
 * Операции кромкования (typeId = EL)
 * XNC-операции (typeId = XNC)
 * ----------------------------------------------------------------------------------
 **/

require_once $root . '/system/library/functions/fs_basis.php';
require_once $root . '/system/templates/basisTemplates.php';

$outProject = [];
// Части проекта (исходники): инструменты, материалы, операции и пр.
$productsPart = [];
$toolsPart = [];
$materialsPart = [];
$operationsPart = [];
// Здесь будут храниться отфильтрованные не нужные инструменты, материалы и операции
$unnecessaryTools = [];
$unnecessarySheets = [];
$unnecessaryCsOps = [];
// Номера операций сшивок непарных блоков (инструмент, материал, операция)
$stitchingOpCsArr = [];
// Здесь будут храниться названия и коды кромок и материалов (ключ - ID операции, значение - массив с кодом и именем)
$elNames = [];
$sheetNames = [];
// Здесь будут храниться коды материалов (нужно для выявления сшивок)
$detectMatStitch = [];
// Здесь будет храниться наибольшие идентификаторы (PART, GOOD, OPERATION) для создания дополнительных идентификаторов
$largestDetailId = 0;
$largestGoodId = 0;
$largestOpId = 0;

if (!isset($_SESSION['data_id']) || empty($_SESSION['data_id'])) {
	$_SESSION['data_id'] = $postData['data_id'];
}

// Извлекаем проект
// ####################################################################################################################################
// ####################################################################################################################################
// ####################################################################################################################################
$srcProject = base64_decode($postData['file']);

if (!strripos($srcProject, '<project')) return $data;

$p = xml_parser_create();
xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
xml_parse_into_struct($p, $srcProject, $srcVals, $srcIndex);
xml_parser_free($p);

$srcVals = delCdata($srcVals);
$srcIndex = getIndex($srcVals);

// Рассортировываем части проекта по массивам (изделия, инструменты, материалы, операции раскроя и кромкования, xnc-операции):
// массивы инструментов и материалов дополнительно разделяем на подмассивы ((листовой раскрой/кромкование) (sheet/band))
// массив операций - на 
// ключом каждого массива (изделия, инструменты, материалы, операции раскроя и кромкования, xnc-операции) является его ID
foreach ($srcIndex['good'] as $key => $goodKey) {
	if (isset($srcVals[$goodKey]['attributes'])) {

		// Находим наибольший идентификатор GOOD
		if ($srcVals[$goodKey]['attributes']['id'] > $largestGoodId) $largestGoodId = $srcVals[$goodKey]['attributes']['id'];

		// Изделия (вместе с деталями)
		if ($srcVals[$goodKey]['attributes']['typeId'] == 'product') {
			$tmpPartKey = ($goodKey + 1);
			$tmpID = $srcVals[$goodKey]['attributes']['id'];
			$productsPart[$tmpID]['open'] = $srcVals[$goodKey];
			while ($srcVals[$tmpPartKey]['tag'] == 'part') {
				$tmpPartId = $srcVals[$tmpPartKey]['attributes']['id'];
				$productsPart[$tmpID]['parts'][$tmpPartId] = $srcVals[$tmpPartKey];
				$tmpPartKey++;

				// Находим наибольший идентификатор детали (PART)
				if ($tmpPartId > $largestDetailId) $largestDetailId = $tmpPartId;
			}
			$productsPart[$tmpID]['close'] = ['tag' => 'good', 'type' => 'close', 'level' => 2];
		// Инструменты
		} elseif ($srcVals[$goodKey]['attributes']['typeId'] == 'tool.cutting') {
			$tmpID = $srcVals[$goodKey]['attributes']['id'];
			$toolsPart['cutting'][$tmpID] = $srcVals[$goodKey];
		} elseif ($srcVals[$goodKey]['attributes']['typeId'] == 'tool.edgeline') {
			$tmpID = $srcVals[$goodKey]['attributes']['id'];
			$toolsPart['edgeline'][$tmpID] = $srcVals[$goodKey];
		// Материалы
		} elseif ($srcVals[$goodKey]['attributes']['typeId'] == 'sheet') {
			$tmpID = $srcVals[$goodKey]['attributes']['id'];

			//.. 22.02.2022 Если материалы приходят без кода,
			// то подставляем подходящий по параметрам материал клиента из базы
			if (!isset($srcVals[$goodKey]['attributes']['code'])) {
				// $q = DB::table('MATERIAL')
				// 	->where('NAME', 'like', '%Материал клиента%')
				// 	->where('T', '=', $srcVals[$goodKey]['attributes']['t'])
				// 	->first();

				// if (!empty($q)) {
				// 	$q = (array)$q;
				// 	$srcVals[$goodKey]['attributes']['code'] = $q['CODE'];
				// 	$srcVals[$goodKey]['attributes']['name'] = '(!!! Нужно заменить !!!)' . $srcVals[$goodKey]['attributes']['name'];
				// }
				$srcVals[$goodKey]['attributes']['code'] = 'RAND_' . mt_rand(150000, 500000);
			}

			$tmpCode = $srcVals[$goodKey]['attributes']['code'];
			$materialsPart['sheet'][$tmpID] = $srcVals[$goodKey];
			if (isset($srcVals[($goodKey + 1)]['tag']) && $srcVals[($goodKey + 1)]['tag'] === 'part') {
				// Находим наибольший идентификатор детали (PART)
				$tmpPartId = $srcVals[($goodKey + 1)]['attributes']['id'];
				if ($tmpPartId > $largestDetailId) $largestDetailId = $tmpPartId;

				$materialsPart['sheet'][$tmpID . 'p'] = $srcVals[($goodKey + 1)];
			}
			if (isset($srcVals[($goodKey + 2)]['tag'])
				&& $srcVals[($goodKey + 2)]['tag'] === 'good'
				&& $srcVals[($goodKey + 2)]['type'] === 'close')
			{
				$materialsPart['sheet'][$tmpID . 'c'] = $srcVals[($goodKey + 2)];
			}
			
			// Собираем коды материалов для сравнения
			if (isset($detectMatStitch[$tmpCode])) {
				$detectMatStitch[$tmpCode]['piece']++;
				if ($detectMatStitch[$tmpCode]['thickness'] < $srcVals[$goodKey]['attributes']['t']) {
					$detectMatStitch[$tmpCode]['targetId'] = $tmpID;
				} else $detectMatStitch[$tmpCode]['noStId'] = $tmpID;
			} else {
				$detectMatStitch[$tmpCode]['noStId'] = $tmpID;
				$detectMatStitch[$tmpCode]['targetId'] = $tmpID;
				$detectMatStitch[$tmpCode]['operationCsId'] = null; // Заполнится ниже
				$detectMatStitch[$tmpCode]['toolId'] = null; // Заполнится ниже
				$detectMatStitch[$tmpCode]['code'] = $tmpCode;
				$detectMatStitch[$tmpCode]['thickness'] = $srcVals[$goodKey]['attributes']['t'];
				$detectMatStitch[$tmpCode]['piece'] = 1;
				$detectMatStitch[$tmpCode]['partIds'] = []; // Заполнится ниже
			}
		} elseif ($srcVals[$goodKey]['attributes']['typeId'] == 'band') {
			$tmpID = $srcVals[$goodKey]['attributes']['id'];
			$materialsPart['band'][$tmpID] = $srcVals[$goodKey];
		}
	}
}
$largestDetailId++;
$largestGoodId++;
foreach ($srcIndex['operation'] as $key => $opKey) {
	if (isset($srcVals[$opKey]['attributes'])) {

		// Находим наибольший идентификатор операции
		$tmpOpId = $srcVals[$opKey]['attributes']['id'];
		if ($tmpOpId > $largestOpId) $largestOpId = $tmpOpId;

		// Операции раскроя
		if ($srcVals[$opKey]['attributes']['typeId'] == 'CS') {
			$tmpOpKey = ($opKey + 1);
			$tmpID = $srcVals[$opKey]['attributes']['id'];
			$operationsPart['CS'][$tmpID]['open'] = $srcVals[$opKey];
			while ($srcVals[$tmpOpKey]['tag'] == 'part' || $srcVals[$tmpOpKey]['tag'] == 'material') {
				if ($srcVals[$tmpOpKey]['tag'] == 'material') {
					$tmpSheetId = $srcVals[$tmpOpKey]['attributes']['id'];
					break;
				}
			}
			while ($srcVals[$tmpOpKey]['tag'] == 'part' || $srcVals[$tmpOpKey]['tag'] == 'material') {
				$tmpOpId = $srcVals[$tmpOpKey]['attributes']['id'];
				if ($srcVals[$tmpOpKey]['tag'] == 'part') {
					$operationsPart['CS'][$tmpID]['parts'][$tmpOpId] = $srcVals[$tmpOpKey];

					// Собираем коды и названия материалов (нужно ниже для заполнения атрибутов детали)
					$tmpPartId = $srcVals[$tmpOpKey]['attributes']['id'];
					$sheetNames[$tmpPartId]['code'] = $materialsPart['sheet'][$tmpSheetId]['attributes']['code'];
					$sheetNames[$tmpPartId]['name'] = $materialsPart['sheet'][$tmpSheetId]['attributes']['name'];
				} else $operationsPart['CS'][$tmpID]['material'] = $srcVals[$tmpOpKey];
				$tmpOpKey++;
			}
			$operationsPart['CS'][$tmpID]['close'] = ['tag' => 'operation', 'type' => 'close', 'level' => 2];
		// Операции кромкования
		} elseif ($srcVals[$opKey]['attributes']['typeId'] == 'EL') {
			$tmpOpKey = ($opKey + 1);
			$tmpID = $srcVals[$opKey]['attributes']['id'];
			$operationsPart['EL'][$tmpID]['open'] = $srcVals[$opKey];
			while ($srcVals[$tmpOpKey]['tag'] == 'part' || $srcVals[$tmpOpKey]['tag'] == 'material') {
				$tmpOpId = $srcVals[$tmpOpKey]['attributes']['id'];
				if ($srcVals[$tmpOpKey]['tag'] == 'part') $operationsPart['EL'][$tmpID]['parts'][$tmpOpId] = $srcVals[$tmpOpKey];
				else {
					$operationsPart['EL'][$tmpID]['material'] = $srcVals[$tmpOpKey];

					// Собираем коды и названия кромок (нужно ниже для заполнения атрибутов детали)
					$tmpBandId = $srcVals[$tmpOpKey]['attributes']['id'];

					//.. 22.02.2022 Если кромки приходят без кода,
					// то подставляем подходящую по параметрам кромку клиента из базы
					if (!isset($materialsPart['band'][$tmpBandId]['attributes']['code'])) {

						// В параметре "W" кромки толщина не передаётся,
						// она содержится в имени, поэтому берём оттуда,
						// а если нет и там, то берём стандартную (22мм)
						// $tmpW = preg_replace('#^.*( [\d]{2} \*).*$#', '${1}', $materialsPart['band'][$tmpBandId]['attributes']['name']);
						// $tmpW = str_replace(' ', '', $tmpW);
						// $tmpW = str_replace('*', '', $tmpW);
						// if (empty((int)$tmpW)) {
						// 	$tmpW = '22';
						// }

						// $q = DB::table('BAND')
						// 	->where('NAME', 'like', '%Кромка клиента%')
						// 	->where('T', '=', $materialsPart['band'][$tmpBandId]['attributes']['t'])
						// 	->where('W', '=', $tmpW)
						// 	->first();

						// if (!empty($q)) {
						// 	$q = (array)$q;
						// 	$materialsPart['band'][$tmpBandId]['attributes']['code'] = $q['CODE'];
						// 	$materialsPart['band'][$tmpBandId]['attributes']['name'] = '(!!! Нужно заменить !!!)' . $materialsPart['band'][$tmpBandId]['attributes']['name'];
						// }

						$materialsPart['band'][$tmpBandId]['attributes']['code'] = 'RAND_' . mt_rand(150000, 500000);
					}

					$elNames[$tmpID]['code'] = $materialsPart['band'][$tmpBandId]['attributes']['code'];
					$elNames[$tmpID]['name'] = $materialsPart['band'][$tmpBandId]['attributes']['name'];
				}
				$tmpOpKey++;
			}
			$operationsPart['EL'][$tmpID]['close'] = ['tag' => 'operation', 'type' => 'close', 'level' => 2];
		// Операции пазования
		} elseif ($srcVals[$opKey]['attributes']['typeId'] == 'GR') {
			$tmpOpKey = ($opKey + 1);
			$tmpID = $srcVals[$opKey]['attributes']['id'];
			$operationsPart['GR'][$tmpID]['open'] = $srcVals[$opKey];
			while ($srcVals[$tmpOpKey]['tag'] == 'part' || $srcVals[$tmpOpKey]['tag'] == 'material') {
				$tmpOpId = $srcVals[$tmpOpKey]['attributes']['id'];
				if ($srcVals[$tmpOpKey]['tag'] == 'part') $operationsPart['GR'][$tmpID]['parts'][$tmpOpId] = $srcVals[$tmpOpKey];
				else $operationsPart['GR'][$tmpID]['material'] = $srcVals[$tmpOpKey];
				$tmpOpKey++;
			}
			$operationsPart['GR'][$tmpID]['close'] = ['tag' => 'operation', 'type' => 'close', 'level' => 2];
		// XNC-операции
		} elseif ($srcVals[$opKey]['attributes']['typeId'] == 'XNC') {
			$tmpOpKey = ($opKey + 1);
			$tmpID = $srcVals[$opKey]['attributes']['id'];
			$operationsPart['XNC'][$tmpID]['open'] = $srcVals[$opKey];
			while ($srcVals[$tmpOpKey]['tag'] == 'part') {
				$tmpOpId = $srcVals[$tmpOpKey]['attributes']['id'];
				$operationsPart['XNC'][$tmpID]['parts'][$tmpOpId] = $srcVals[$tmpOpKey];
				$tmpOpKey++;
			}
			$operationsPart['XNC'][$tmpID]['close'] = ['tag' => 'operation', 'type' => 'close', 'level' => 2];
		}
	}
}
$largestOpId++;

// Далле вносим правки
// ####################################################################################################################################
// ####################################################################################################################################
// ####################################################################################################################################

// Из базиса могут прийти детали только сшивки на материал.
// И могут также прийти детали со сшивками и без на один материал,
// в этом случае в проекте будут два материала с одинаковыми кодами.
// Соответственно, если есть два материала с одинаковыми кодами, то один из них - сшивка (с материалом из БД сравнивать не нужно)
// А если нет материалов с одинаковыми кодами, то сверяем толщину каждого материала проекта с толщиной материала в БД,
// и если толщина материала проекта больше, значит это сшивка.
// Сначала обрабатываем случай с одинаковыми кодами, а затем проверяем остальные материалы
$tmp = [];
foreach ($detectMatStitch as $matKey => $matValue) {
	// Собираем для дублирующихся материалов идентификаторы инструмента и листового раскроя
	// их (материал, инструмент, раскрой) впоследствии удалим, и преобразуем в сшивку
	// также эти данные нужны для доступа к деталям изделия
	if ($matValue['piece'] > 1) {
		foreach ($operationsPart['CS'] as $key => $opBlock) {
			if ($opBlock['material']['attributes']['id'] === $matValue['targetId']) {
				$matValue['operationCsId'] = $opBlock['open']['attributes']['id'];
				$matValue['toolId'] = $opBlock['open']['attributes']['tool1'];
				$matValue['partIds'] = array_keys($opBlock['parts']);
				break;
			}
		}
		$tmp[$matKey] = $matValue;
	// А для одиночных проверяем толщину, если 2X, значит сшивка
	} else {
		foreach ($operationsPart['CS'] as $key => $opBlock) {
			$tmpMatId = $opBlock['material']['attributes']['id'];
			if ($tmpMatId === $matValue['targetId']) {
				$tmpCode = $materialsPart['sheet'][$tmpMatId]['attributes']['code'];
				$tmpT = (int)$materialsPart['sheet'][$tmpMatId]['attributes']['t'];

				// $q = "SELECT `T` FROM `MATERIAL` WHERE `CODE` = $tmpCode";
				// $q = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);
				// if ($q['res'] === 1 && isset($q['data'][0])) {

				$q = DB::table('MATERIAL')->where(['CODE' => $tmpCode])->first();
				if (!empty($q)) {
					$q = (array)$q;
					if ((int)($q['T'] * 2) === $tmpT) {
						$matValue['operationCsId'] = $opBlock['open']['attributes']['id'];
						$matValue['toolId'] = $opBlock['open']['attributes']['tool1'];
						$matValue['partIds'] = array_keys($opBlock['parts']);
						$tmp[$matKey] = $matValue;
						break;
					}
				}
			}
		}
	}
}
// Теперь в "$detectMatStitch" только сшивки,
$detectMatStitch = $tmp;

$tmp = [];
// Дальше собираем все необходимые данные для сшивок (доп. детали, инструменты, материалы, раскрои) в массив
foreach ($detectMatStitch as $matKey => $dependenciesBlock) {
	foreach ($dependenciesBlock['partIds'] as $partId) {
		foreach ($productsPart as $product) {
			foreach ($product['parts'] as $partKey => $part) {
				if ($partId === $partKey) {
					$tmpPartSrc = $basisTemplates['stSrcDetPart'];
					$tmpPartSt = $basisTemplates['stDetPart'];
					// getPairedParts($part, $tmpPart, $largestDetailId);
					$tmp['details'][$partKey] = $matKey;
					$tmp[$matKey]['details'][$partKey] = getStDetPart($part, $tmpPartSrc, $tmpPartSt, $elNames, $sheetNames, $largestDetailId);
					$largestDetailId += 2;
					break 2;
				}
			}
		}
	}
}
$stitchingArray = $tmp;

foreach ($tmp as $sheetKey => $oneStBlock) {
	if (isset($oneStBlock['details'])) {
		foreach ($oneStBlock['details'] as $key => $detail) {

			// Собираем инструмент для сшивки (<good typeId="tool.cutting")
			$stMatToolCutting = $basisTemplates['stMatToolCutting'];
			$stMatToolCutting['attributes']['id'] = $largestGoodId;
			$largestGoodId++;
			$stMatToolCutting['attributes']['MY_DOUBLE_ID'] = $detail['face']['attributes']['id'];
			$stMatToolCutting['attributes']['description'] .= ' [/' . $detail['face']['attributes']['id'] . '/]';

			// Добавляем его в общий массив сшивок
			$stitchingArray[$sheetKey]['tool.cutting'][$key] = $stMatToolCutting;

			// Собираем материал (вместе с внутренним PART)
			$params = [
				'material' => $sheetKey,
				'stithingId' => $detail['face']['attributes']['id'],
				'goodId' => $largestGoodId,
				'partId' => $largestDetailId,
				'w' => $detail['face']['attributes']['w'],
				'l' => $detail['face']['attributes']['l'],
			];
			$largestDetailId++;
			$largestGoodId++;
			$sheetTemplate = $basisTemplates['stMatSheet'];
			$partTemplate = $basisTemplates['stMatPart'];
			$tmpSheet = getStSheet($params, $sheetTemplate, $partTemplate);
			// Добавляем его в общий массив сшивок
			$stitchingArray[$sheetKey]['sheet'][$key] = $tmpSheet['sheet'];
			$stitchingArray[$sheetKey]['innerSheetPart'][$key] = $tmpSheet['innerPart'];

			// Собираем операцию листового раскроя (typeId="CS")
			$params = [
				'opId' => $largestOpId,
				'materialId' => $stitchingArray[$sheetKey]['sheet'][$key]['attributes']['id'],
				'tool1' => $stitchingArray[$sheetKey]['tool.cutting'][$key]['attributes']['id'],
				'partDetailId' => $key,
				'partSheetId' => $stitchingArray[$sheetKey]['innerSheetPart'][$key]['attributes']['id'],
				'w' => $detail['face']['attributes']['w'],
				'l' => $detail['face']['attributes']['l'],
				't' => $stitchingArray[$sheetKey]['sheet'][$key]['attributes']['t'],
				'MY_DOUBLE_ID' => $detail['face']['attributes']['id']
			];
			$largestOpId++;
			$opCsTemplate = $basisTemplates['csOpOpen'];
			$opPartTemplate = $basisTemplates['simplePart'];
			$opMatTemplate = $basisTemplates['simpleMaterial'];
			$tmpOperation = getStCsOperation($params, $opCsTemplate, $opPartTemplate, $opMatTemplate);

			// Добавляем его в общий массив сшивок
			$stitchingArray[$sheetKey]['operation'][$key] = $tmpOperation['operation'];
			$stitchingArray[$sheetKey]['opPartDet'][$key] = $tmpOperation['partDet'];
			$stitchingArray[$sheetKey]['opPartSheet'][$key] = $tmpOperation['partSheet'];
			$stitchingArray[$sheetKey]['opMaterial'][$key] = $tmpOperation['material'];
		}
	}
}

// Пересобираем проект
// ####################################################################################################################################
// ####################################################################################################################################
// ####################################################################################################################################

if (isset($srcVals[0]['value'])) unset($srcVals[0]['value']);
$outProject['0'] = $srcVals[0];

// Изделия
foreach ($productsPart as $prodKey => $prodValue) {
	$outProject[] = $prodValue['open'];
	foreach ($prodValue['parts'] as $partKey => $partValue) {
		if (isset($stitchingArray['details'][$partKey])) {
			$tmpMatId = $stitchingArray['details'][$partKey];
			$outProject[] = $stitchingArray[$tmpMatId]['details'][$partKey]['stitching'];
			$outProject[] = $stitchingArray[$tmpMatId]['details'][$partKey]['face'];
			$outProject[] = $stitchingArray[$tmpMatId]['details'][$partKey]['back'];
		} else {
			$outProject[] = $partValue;
		}
		
	}
	$outProject[] = $prodValue['close'];
}

// $detectMatStitch[] содержит инструменты, материалы и раскрои,
// которые нужно удалить (при условии, что noStId и targetId разные)
// значит один блок (инструмент, материал, раскрой) удаляем, а другой остаётся.
// Если же noStId и targetId одинаковые, то блок нужно оставить
// [targetId] => материал
// [operationCsId] => операция раскроя
// [toolId] => инструмент
foreach ($detectMatStitch as $key => $value) {
	if ($value['noStId'] !== $value['targetId']) {
		$unnecessaryTools[] = $value['toolId'];
		$unnecessarySheets[] = $value['targetId'];
		$unnecessaryCsOps[] = $value['operationCsId'];
	} else $stitchingOpCsArr[] = $value['operationCsId'];
}

// Инструменты раскроя
foreach ($toolsPart['cutting'] as $toolKey => $toolValue) {
	if (!in_array($toolKey, $unnecessaryTools)) {
		$outProject[] = $toolValue;
	}
}
foreach ($stitchingArray as $value) {
	if (isset($value['tool.cutting'])) {
		foreach ($value['tool.cutting'] as $v) {
			$outProject[] = $v;
		}
	}
}

// Листовой материал
$sheetsIdCode = [];
foreach ($materialsPart['sheet'] as $matKey => $matValue) {
	if (is_int($matKey) && !in_array($matKey, $unnecessarySheets)) {
		$sheetsIdCode[$matValue['attributes']['id']] = $matValue['attributes']['code'];
		$outProject[] = $materialsPart['sheet'][$matKey];
		$outProject[] = $materialsPart['sheet'][$matKey . 'p'];
		$outProject[] = $materialsPart['sheet'][$matKey . 'c'];
	}
}

foreach ($stitchingArray as $key => $value) {
	if (is_int($key)) {
		foreach ($value['sheet'] as $k => $v) {
			$sheetsIdCode[$v['attributes']['id']] = $v['attributes']['MY_FIRST'];
			$outProject[] = $v;
			$outProject[] = $stitchingArray[$key]['innerSheetPart'][$k];
			$outProject[] = $basisTemplates['goodClose'];
		}
	}
}

// Операции раскроя
foreach ($operationsPart['CS'] as $opKey => $opValue) {
	if (!in_array($opKey, $unnecessaryCsOps)) {
		$outProject[] = $opValue['open'];
		$outProject[] = $opValue['material'];
		if (!in_array($opKey, $stitchingOpCsArr)) {
			foreach ($opValue['parts'] as $value) {
				$outProject[] = $value;
			}
		}
		$tmpSheetId = $opValue['material']['attributes']['id'];
		if (isset($sheetsIdCode[$tmpSheetId]) && isset($stitchingArray[$sheetsIdCode[$tmpSheetId]])) {
			foreach ($stitchingArray[$sheetsIdCode[$tmpSheetId]]['details'] as $key => $value) {
				$tmpPartFace = $basisTemplates['simplePart'];
				$tmpPartFace['attributes']['id'] = $value['face']['attributes']['id'];
				$outProject[] = $tmpPartFace;
				
				$tmpPartBack = $basisTemplates['simplePart'];
				$tmpPartBack['attributes']['id'] = $value['back']['attributes']['id'];
				$outProject[] = $tmpPartBack;
			}
		}
		$outProject[] = $opValue['close'];
	}
}
foreach ($stitchingArray as $key => $value) {
	if (is_int($key)) {
		foreach ($value['operation'] as $opKey => $opValue) {
			$outProject[] = $opValue;
			$outProject[] = $stitchingArray[$key]['opMaterial'][$opKey];
			$outProject[] = $stitchingArray[$key]['opPartSheet'][$opKey];
			$outProject[] = $stitchingArray[$key]['opPartDet'][$opKey];
			$outProject[] = $basisTemplates['opClose'];
		}
	}
}

// Инструменты кромкования
foreach ($toolsPart['edgeline'] as $toolKey => $toolValue) {
	$outProject[] = $toolValue;
}
// Кромочный материал
foreach ($materialsPart['band'] as $matKey => $matValue) {
	$outProject[] = $matValue;
}
// Операции кромкования
foreach ($operationsPart['EL'] as $opKey => $opValue) {
	$outProject[] = $opValue['open'];
	$outProject[] = $opValue['material'];
	foreach ($opValue['parts'] as $value) {
		$outProject[] = $value;
	}
	$outProject[] = $opValue['close'];
}
// XNC-операции
if (isset($operationsPart['XNC']) && !empty($operationsPart['XNC'])) {
	foreach ($operationsPart['XNC'] as $opValue) {
		$outProject[] = $opValue['open'];
		foreach ($opValue['parts'] as $value) {
			$outProject[] = $value;
		}
		$outProject[] = $opValue['close'];
	}
}
// Закрываем проект
$outProject[] = $basisTemplates['endProject'];
$outProject = preg_replace('/[\r\n\t]/', '', projectToXml($outProject));
session(['project_data' => $outProject]);
put_temp_file_data($_SESSION['data_id'], $_SESSION);
$data = 'true';

?>