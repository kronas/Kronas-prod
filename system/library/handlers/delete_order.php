<?php
session_start();
include_once(DIR_CORE . "func_mini.php");

$nowDate = date('Y-m-d H:i:s');

// Удаление заказа из хранилища по ID
if (isset($incomingData["deleteOrderById"])) {
    
    $delSafe = (bool)$incomingData['del_safe'];
    $sql = 'SELECT * FROM `ORDER1C` WHERE `ID` = "' . $incomingData["deleteOrderById"] . '" LIMIT 1';
    $order_check = getTableData($sql);
    
    if(!empty($order_check[0]) && ($order_check[0]['status'] == 'черновик' || ($_SESSION['user']['role'] === 'admin' && $_SESSION['user']['access'] >= 100))) {
    
        $q = 'UPDATE `ORDER1C` SET `del_order` = 1,`DB_AC_IN` = "' . $nowDate . '" WHERE `ID` = "' . $incomingData["deleteOrderById"] . '" LIMIT 1';
        $res = executeQuery($q);
        if ($res === true) {
            $data = true;
        } else {
            $data = false;
        }

    }

}

?>