<?php

/**
 * 
 **/
function calculate ($vals_z,$vals,$link,$place)
{
    //.. 06.04.2023 Тоценко. Добавлено условие, если в сессии нет участка, то использовать участок из параметра
    if (!isset($_SESSION['user_place'])) {
        $_SESSION['user_place'] = $place;
    }
    // exit ('11');
    $vals_mr_ch=mr_change_vals ($vals_z);
    
    // p_($vals_mr_ch);exit;
    // echo 'test: '.$vals_mr_ch;
    if ($vals_mr_ch != false)
    {
        $vals_back_sh=$vals_z;
        $data_xml = vals_index_to_project($vals_mr_ch);
//p_ ($vals);
        $data_xml=gl_recalc (__LINE__,__FILE__,__FUNCTION__,$data_xml);
        $data=get_vals_index_without_session($data_xml);
        $vals = $data["vals"];
    }
    // p_();exit;
    // p_($vals);exit;
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate start '.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
// p_('calculate start '.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'].' '.$vals[0]['attributes']['project.my_order_name']);exit;
    ini_set('max_execution_time', '500');
// phpinfo();
// exit;
    $vals=vals_out($vals);
    $vals=add_tag_print($vals);
    ksort ($vals);
// my_mail('1','2',vals_index_to_project($vals),'xml');
//     exit;


    $index=make_index($vals);

    $arr_st=st_from_akcent($vals,$place);
    // xml ($vals);

    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="product")
        {
            $ip=$i+1;
            while ($vals[$ip]["tag"]=="PART")
            {
                
                $epart=get_all_part($vals,$vals[$ip]["attributes"]["ID"]);
                // p_($epart);exit;
                if (count($epart['cl'])<1)
                {
                    $part_con[$vals[$ip]["attributes"]["ID"]]=$ip;
                    $e4=part_get_material($vals, $vals[$ip]["attributes"]["ID"]);
                    $part_edge[$vals[$ip]["attributes"]["ID"]]=get_el_tool__sheet_part($vals,$vals[$ip]["attributes"]["ID"]);
                    $part[$vals[$ip]["attributes"]["ID"]]['T']=$e4['attributes']['T'];
                    $part[$vals[$ip]["attributes"]["ID"]]['p_count']=$vals[$i]["attributes"]["COUNT"];
                    // p_($part);exit;
                    $vals[$ip]["attributes"]["T"]=$e4['attributes']['T'];
                    if ((isset($vals[$ip]["attributes"]["part.my_postforming_cut_top"]))||(isset($vals[$ip]["attributes"][mb_strtoupper("part.my_postforming_cut_top")])))
                    {
                        $part_cut_pf[$vals[$ip]["attributes"]["ID"]]=$vals[$ip]["attributes"]["CL"]/1000*$vals[$ip]["attributes"]["COUNT"];
                    }
                    if(isset($e4['attributes']['CODE'])) $part[$vals[$ip]["attributes"]["ID"]]['SHEET']="[/".$e4['attributes']['CODE']."/] ".$e4['attributes']['NAME'];
                    else $part[$vals[$ip]["attributes"]["ID"]]['SHEET']="[/".$e4['attributes']['MY_FIRST']." | ".$e4['attributes']['MY_SECOND']."/] ".$e4['attributes']['NAME'];
                    unset ($e4);
                    if(isset($vals[$ip]["attributes"]["W"])) $part[$vals[$ip]["attributes"]["ID"]]['W']=$vals[$ip]["attributes"]["W"];
                    if(isset($vals[$ip]["attributes"]["CW"])) $part[$vals[$ip]["attributes"]["ID"]]['CW']=$vals[$ip]["attributes"]["CW"];
                    if(isset($vals[$ip]["attributes"]["DW"])) $part[$vals[$ip]["attributes"]["ID"]]['DW']=$vals[$ip]["attributes"]["DW"];
                    if(isset($vals[$ip]["attributes"]["DL"])) $part[$vals[$ip]["attributes"]["ID"]]['DL']=$vals[$ip]["attributes"]["DL"];
                    if(isset($vals[$ip]["attributes"]["L"])) $part[$vals[$ip]["attributes"]["ID"]]['L']=$vals[$ip]["attributes"]["L"];
                    if(isset($vals[$ip]["attributes"]["part.my_rs_section"])) $part[$vals[$ip]["attributes"]["ID"]]['rs_section']=$vals[$ip]["attributes"]["part.my_rs_section"];
                    
                    if(isset($vals[$ip]["attributes"]["CL"])) $part[$vals[$ip]["attributes"]["ID"]]['CL']=$vals[$ip]["attributes"]["CL"];
                    if(isset($vals[$ip]["attributes"]["COUNT"])) $part[$vals[$ip]["attributes"]["ID"]]['count']=$vals[$ip]["attributes"]["COUNT"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_KL_ELL_CODE"])) $part[$vals[$ip]["attributes"]["ID"]]['KLL']=$vals[$ip]["attributes"]["PART.MY_KL_ELL_CODE"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_KL_ELR_CODE"])) $part[$vals[$ip]["attributes"]["ID"]]['KLR']=$vals[$ip]["attributes"]["PART.MY_KL_ELR_CODE"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_KL_ELT_CODE"])) $part[$vals[$ip]["attributes"]["ID"]]['KLT']=$vals[$ip]["attributes"]["PART.MY_KL_ELT_CODE"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_KL_ELB_CODE"])) $part[$vals[$ip]["attributes"]["ID"]]['KLB']=$vals[$ip]["attributes"]["PART.MY_KL_ELB_CODE"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_L"])) $part[$vals[$ip]["attributes"]["ID"]]['SIDE_TO_CUT_L']=$vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_L"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_R"])) $part[$vals[$ip]["attributes"]["ID"]]['SIDE_TO_CUT_R']=$vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_R"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_T"])) $part[$vals[$ip]["attributes"]["ID"]]['SIDE_TO_CUT_T']=$vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_T"];
                    if(isset($vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_B"])) $part[$vals[$ip]["attributes"]["ID"]]['SIDE_TO_CUT_B']=$vals[$ip]["attributes"]["PART.MY_SIDE_TO_CUT_B"];
                    if (!$vals[$ip]["attributes"]["MY_DOUBLE_RES"])
                    {
                        $parts[$vals[$ip]["attributes"]["ID"]]=$ip;
                        $parts_size[$vals[$ip]["attributes"]["ID"]]=array("L"=>$vals[$ip]["attributes"]["L"],"W"=>$vals[$ip]["attributes"]["W"],
                        "DL"=>$vals[$ip]["attributes"]["DL"],"DW"=>$vals[$ip]["attributes"]["DW"]
                    );
                        $parts_double[$vals[$ip]["attributes"]["ID"]]=$vals[$ip]["attributes"]["MY_DOUBLE_ID"];
                        $parts_count[$vals[$ip]["attributes"]["ID"]]=$vals[$ip]["attributes"]["COUNT"];
                    }
                    else
                    {
                        $parts_d[$vals[$ip]["attributes"]["ID"]]=$ip;
                        $parts_size_d[$vals[$ip]["attributes"]["ID"]]=array("L"=>$vals[$ip]["attributes"]["L"],"W"=>$vals[$ip]["attributes"]["W"],
                        "DL"=>$vals[$ip]["attributes"]["DL"],"DW"=>$vals[$ip]["attributes"]["DW"]
                    );
                        $parts_double[$vals[$ip]["attributes"]["ID"]]=$vals[$ip]["attributes"]["MY_DOUBLE_ID"];
                        $parts_count[$vals[$ip]["attributes"]["ID"]]=$vals[$ip]["attributes"]["COUNT"];


                    }
                }

                $ip++;

            }
        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate part_start'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

    $sql="select * from SERVICE";
    // $sql=mysqli_query($link,$sql);
    $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    // p_($sql2);exit;
    $ii=0;
    while (isset($sql2[$ii]))
    {
        $ii++;
        if ($sql2[$ii]['CODE']>0)
        {
            $serv_all[$sql2[$ii]["CODE"]]=$sql2[$ii];
            $serv_req[]=$sql2[$ii]["CODE"];
        }
    }
    // p_($serv_all);

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_service_get'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

    if (($_SESSION['client_code']>0)&&($_SESSION['user_place']>0))
    {
        $serv_get=get_actual_price($serv_req, $_SESSION['user_place'], $_SESSION['client_code']);
    } else {
        $serv_get=get_actual_price($serv_req, $_SESSION['user_place']);
    }
    
    if (count($serv_get)>0)
    {
        foreach ($serv_get as $c_srvvv=>$srvvv)
        {
            if($srvvv["PRICE"]>0) $service_pr[$c_srvvv]=$srvvv["PRICE"];
            else $service_pr[$c_srvvv]=0.01;
            $service_name[$c_srvvv]=$serv_all[$c_srvvv]["NAME"];
            $service_unit[$c_srvvv]=$serv_all[$c_srvvv]["UNIT"];
        }
    }
    else
    {
        //.. 30.07.2022 Правки, касательно цен. Если цены не получены (по API из Акцента)
        // выводим ошибку и страницу с кнопками пересчёта и сохранения (файл calculate.php)

        // echo 'Matched one<br>';
        $_SESSION['failed_prices'] = 1;

        // Это старый код, который брал цены из базы Сервиса, если API не отвечал
        // foreach ($serv_all as $c_srvvv=>$sql2)
        // {
        //     $sql="SELECT * FROM SERVICE_PRICE WHERE SERVICE_ID=".$sql2["SERVICE_ID"]." AND PLACE_ID=".$place.";";
        //     $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        //     $sql5=check_price($sql4,"SERVICE",$link,$sql2["SERVICE_ID"]);
        //     if ($sql5["PRICE"]>0)$service_pr[$sql2["CODE"]]=$sql5["PRICE"];
        //     else $service_pr[$sql2["CODE"]]=0.01;
        //     $service_name[$sql2["CODE"]]=$sql2["NAME"];
        //     $service_unit[$sql2["CODE"]]=$sql2["UNIT"];
        // }
    }
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate serv_get_price'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

    // p_($service_pr);exit;
    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="sheet")
        {
            if (($vals[$i]["attributes"]["MY_FIRST"]>0)&&($vals[$i]["attributes"]["MY_SECOND"]>0)) 
            {
                $sql="select * from MATERIAL where CODE=".$vals[$i]['attributes']['MY_FIRST'];
                $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                $sql11="select * from MATERIAL where CODE=".$vals[$i]['attributes']['MY_SECOND'];
                $sql22=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql11)['data'][0];

                foreach ($index["OPERATION"] as $io)
                {
                    if (($vals[$io]["attributes"]["TYPEID"]=='CS')&&($vals[$io+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                    {
                        $ip=$io+2;
                        while ($vals[$ip]["tag"]=="PART")
                        {
                            foreach ($index["PART"] as $ipp)
                            {
                                if (($vals[$ipp]["attributes"]["ID"]==$vals[$ip]["attributes"]["ID"])&&($vals[$ipp]["attributes"]["L"]>0))
                                {
                                    
                                        $vals[$ipp]["attributes"]["MY_DB_FIRST"]=$sql2['MATERIAL_ID'];
                                        $vals[$ipp]["attributes"]["MY_DB_SECOND"]=$sql22['MATERIAL_ID'];
                                    
                                }
                            }
                            $ip++;
                        }
                    }
                }
            }
        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_double_into_part'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
    $index=make_index($vals);
    // p_($vals);
    foreach ($index["GOOD"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="sheet")&&($vals[$i]["attributes"]["CODE"]>0)&&($vals[$i]["attributes"]["MATERIAL_ID"]>0))
        
        {
            $cs_mat[$vals[$i]["attributes"]["ID"]]=$vals[$i]["attributes"]["CODE"];
            $sql7="SELECT * FROM GOOD_PROPERTIES_MATERIAL WHERE MATERIAL_ID=".$vals[$i]["attributes"]["MATERIAL_ID"]." AND PROPERTY_ID=160 AND VALUESTR='глянцевая';";
            $sql21=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql7)['res'];
            $GL[$vals[$i]["attributes"]["CODE"]]['code']=$vals[$i]["attributes"]["CODE"];
            $GL[$vals[$i]["attributes"]["CODE"]]['res']=$sql21;

        }
    }

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_get_glyance'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="sheet")
        {
            if (($vals[$i]["attributes"]["CODE"]>0)&&(is_numeric($vals[$i]["attributes"]["CODE"])))
            {
                $sql="select * from MATERIAL where CODE=".$vals[$i]['attributes']['CODE'];
                $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                $i1=$i+1;

                if (($sql2['MATERIAL_ID']>0)&&(is_numeric($sql2['MATERIAL_ID'])))
                {
                    
                    $sql_e='SELECT * FROM MATERIAL_HALF where place_id='.$_SESSION['user_place'].' AND material_id='.$sql2['MATERIAL_ID'].';';
                    $sql_e_nr=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_e)['count'];
                    if ($sql_e_nr>0) $sql2['MY_HALF'] = 1;
                    else $sql2['MY_HALF'] = 0;
                   
                    foreach ($index['OPERATION'] as $rt)
                    {
                        if(($vals[$rt]['attributes']['TYPEID']=='CS')&&($vals[$rt]['type']=="open")&&($vals[$rt+1]['attributes']['ID']==$vals[$i]["attributes"]["ID"])&&($vals[$rt]['attributes']['operation.my_krat_sheet']=="sheet"))
                        {
                            $sql2['MY_HALF'] = 0;
                        }
                    }
                    // p_($sql2);exit;
                    foreach ($index['OPERATION'] as $t_op)
                    {
                        if (($vals[$t_op]['attributes']['TYPEID']=='CS')&&($vals[$t_op+1]['attributes']['ID']==$vals[$i]["attributes"]["ID"]))
                        {
                            // Торцовка (длина/ширина)
                            $ctriml_cs=$vals[$t_op]['attributes']['CTRIML']/1000;
                            $ctrimw_cs=$vals[$t_op]['attributes']['CTRIMW']/1000;
                            foreach ($index['GOOD'] as $t_tool_c)
                            {
                                if (($vals[$t_tool_c]['attributes']['TYPEID']=="tool.cutting")&&($vals[$t_op]['attributes']['TOOL1']==$vals[$t_tool_c]["attributes"]["ID"]))
                                {
                                    // Толщина пилы
                                    $saw_cs=$vals[$t_tool_c]['attributes'][mb_strtoupper('swSawthick')]/1000;
                                }
                            }
                        }
                    }
                    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_get_param_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

                    if ($vals[$i]['attributes']['good.my_client_material_id']>0)
                    {
                        $rt=$i+1;
                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_start_mat_client_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

                        while ($vals[$rt]['tag']=="PART")
                        {   
                            $sql="select * from MATERIAL where MATERIAL_ID=".$vals[$i]['attributes']['good.my_client_material_id'];
                            $sql_mk=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                            if (($vals[$rt]['attributes']['part.my_client_material_id']>0)&&($vals[$rt]['attributes']['USEDCOUNT']>0))
                            {
                                $mat_kl[$vals[$rt]['attributes']['part.my_client_material_id']]=$vals[$rt]['attributes']['USEDCOUNT'];
                            }
                            elseif (($vals[$rt]['attributes']['L'] == $sql_mk['L'])
                                 && (!isset($vals[$rt]['attributes']['part.my_client_material']))
                                 && (!isset($vals[$rt]['attributes']['SHEETID']))
                                 && ($vals[$rt]['attributes']['USEDCOUNT'] > 0))
                            {
                                $s_size_sheet[$sql_mk['CODE']][$vals[$rt]['attributes']['ID']] = array(
                                    'L' => $vals[$rt]["attributes"]["L"] / 1000,
                                    'W' => $vals[$rt]["attributes"]["W"] / 1000,
                                    'COUNT' => $vals[$rt]["attributes"]["USEDCOUNT"],
                                    'S' => round($vals[$rt]["attributes"]["USEDCOUNT"] * $vals[$rt]["attributes"]["L"] * $vals[$rt]["attributes"]["W"] / 1000 / 1000, 2),
                                    'ID' => $vals[$i]["attributes"]["ID"],
                                    'triml' => $ctriml_cs,
                                    'trimw' => $ctrimw_cs,
                                    'saw' => $saw_cs,
                                    'mat_kl' => 1
                                );   
                            }
                            $rt++;
                        }
                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_finish_mat_client_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                        
                    }
                    elseif ((isset($sql2['L'])&&($sql2['L']>0))&&($sql2['RS_GLASS']==0)&&($sql2['RS_MIRROR']==0))
                    {
                        
                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_start_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                        
                        while($vals[$i1]['tag']=='PART')
                        {
                            // ['SHEETID'] - остаток материала
                            if(($vals[$i1]['attributes']['SHEETID']>0)&&($sql2['MY_HALF']>0))
                            {
                                $j=$i1-1;
                                while ($vals[$j]['attributes']['ID']<>$vals[$i1]['attributes']['SHEETID'])
                                {
                                    $j--;
                                }
                                //j - из чего
                                //i1 - остаток
                                if (($vals[$j]['attributes']['L']==$sql2['L'])&&(($vals[$j]['attributes']['W']==$sql2['W']))&&($sql2['ST']<>1))
                                {
                                    if (($vals[$j]['attributes']['W']/2<=$vals[$i1]['attributes']['W'])&&
                                    ($vals[$j]['attributes']['L']-$vals[$i1]['attributes']['W']<30)&&($sql2['MY_HALF']==1))
                                    {
                                        $s_size[$vals[$i]["attributes"]["CODE"]][$vals[$j]['attributes']['ID']] = array(
                                            'L' => $vals[$i1]["attributes"]["L"] / 1000,
                                            'W' => ($vals[$i1]['attributes']['W'] - $vals[$j]['attributes']['W'] / 2) / 1000,
                                            'COUNT' => $vals[$i1]["attributes"]["COUNT"],
                                            'S' => round($vals[$i1]["attributes"]["COUNT"] * $vals[$i1]["attributes"]["L"] * ($vals[$i1]['attributes']['W'] - $vals[$j]['attributes']['W'] / 2) / 1000 / 1000, 2)
                                        );
                    
                                    }
                                    elseif (($vals[$i1]['attributes']['L']==$sql2['L'])&&($sql2['MY_HALF']==1))
                                    {
                                        $s_size[$vals[$i]["attributes"]["CODE"]][$vals[$j]['attributes']['ID']] = array(
                                            'L' => $vals[$i1]["attributes"]["L"] / 1000,
                                            'W' => $vals[$i1]["attributes"]["W"] / 1000,
                                            'COUNT' => $vals[$i1]["attributes"]["COUNT"],
                                            'S' => round($vals[$i1]["attributes"]["COUNT"] * $vals[$i1]["attributes"]["L"] * $vals[$i1]["attributes"]["W"] / 1000 / 1000, 2)
                                        );
                                    }
                                }
                                
                                // elseif ($sql2['ST']<>1)
                                // {
                                //     $s_size[$vals[$i]["attributes"]["CODE"]][$vals[$j]['attributes']['ID']]=array('L'=>$vals[$i1]["attributes"]["L"]/1000,
                                //     'W'=>$vals[$i1]["attributes"]["W"]/1000,'COUNT'=>$vals[$i1]["attributes"]["COUNT"],
                                //     'S'=>round($vals[$i1]["attributes"]["COUNT"]*$vals[$i1]["attributes"]["L"]*$vals[$i1]["attributes"]["W"]/1000/1000,2));
                                // }
                                $s_size_sheet[$vals[$i]["attributes"]["CODE"]][$vals[$j]['attributes']['ID']] = array(
                                    'L' => $vals[$j]["attributes"]["L"] / 1000,
                                    'W' => $vals[$j]["attributes"]["W"] / 1000,
                                    'COUNT' => $vals[$j]["attributes"]["USEDCOUNT"],
                                    'S' => round($vals[$j]["attributes"]["USEDCOUNT"] * $vals[$j]["attributes"]["L"] * $vals[$j]["attributes"]["W"] / 1000 / 1000, 2),
                                    'ID' => $vals[$i]["attributes"]["ID"],
                                    'triml' => $ctriml_cs,
                                    'trimw' => $ctrimw_cs,
                                    'saw' => $saw_cs
                                );
                            }
                            
                            elseif ($vals[$i1]["attributes"]["USEDCOUNT"]>0)
                            {
                                if (!isset($s_size_sheet[$vals[$i]["attributes"]["CODE"]][$vals[$i1]['attributes']['ID']]))
                                {
                                    $s_size_sheet[$vals[$i]["attributes"]["CODE"]][$vals[$i1]['attributes']['ID']] = array(
                                        'L' => $vals[$i1]["attributes"]["L"] / 1000,
                                        'W' => $vals[$i1]["attributes"]["W"] / 1000,
                                        'COUNT' => $vals[$i1]["attributes"]["USEDCOUNT"],
                                        'S' => round($vals[$i1]["attributes"]["USEDCOUNT"] * $vals[$i1]["attributes"]["L"] * $vals[$i1]["attributes"]["W"] / 1000 / 1000, 2),
                                        'ID' => $vals[$i]["attributes"]["ID"],
                                        'triml' => $ctriml_cs,
                                        'trimw' => $ctrimw_cs,
                                        'saw' => $saw_cs
                                    );
                                }
                            }
                            $i1++;
                        }
                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_finish_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

                    }
                    elseif (($sql2['RS_GLASS']==1)OR($sql2['RS_MIRROR']==1))
                    {
                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_start_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                        unset($s_gl);
                        if (!isset($s_size_sheet[$vals[$i]["attributes"]["CODE"]][$i]))
                        {
                            foreach ($index['OPERATION'] as $io_gl)
                            {
                                if (($vals[$io_gl]['attributes']['TYPEID']=='CS')&&($vals[$io_gl+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                                {
                                    $io_gl=$io_gl+2;
                                    // p_($vals[$i]);
                                    if ((rs_general('calc_right')==1)&&($vals[$io_gl-2]['attributes']['operation.glass']==1)) $k_gl=rs_general('glass_up');
                                    else $k_gl=1;
                                    // p_($k_gl);
                                    // p_($vals[$io_gl-2]);
                                    while ($vals[$io_gl]['tag']=='PART')
                                    {
                                        foreach ($index['PART'] as $gl_p)
                                        {
                                            if (($vals[$gl_p]['attributes']['CL']>0)&&($vals[$gl_p]['attributes']['ID']==$vals[$io_gl]['attributes']['ID']))
                                            {
                                                
                                                $s_gl+=$vals[$gl_p]['attributes']['CL']/1000*$vals[$gl_p]['attributes']['CW']/1000*$vals[$gl_p]['attributes']['COUNT']*$k_gl;
                                            // p_($vals[$gl_p]['attributes']);
                                            }
                                        }
                                        $io_gl++;
                                    }
                                }
                            }
                            
                            $s_size_sheet[$vals[$i]["attributes"]["CODE"]][$i] = array(
                                'L' => 1,
                                'W' => 1,
                                'COUNT' => $s_gl,
                                'S' => $s_gl,
                                'ID' => $vals[$i]["attributes"]["ID"],
                                'triml' => $ctriml_cs,
                                'trimw' => $ctrimw_cs,
                                'saw' => $saw_cs,
                                'glass' => 1
                            );
                            $_SESSION['koef_price'][$vals[$i]['attributes']['CODE']]=$k_gl;
                            unset($k_gl);
                        }
                        // xml ($vals);
                        // p_($s_size_sheet);exit;
                        // while($vals[$i1]['tag']=='PART')
                        // {
                        //     if(($vals[$i1]['attributes']['SHEETID']>0))
                        //     {
                        //         $s_size[$vals[$i]["attributes"]["CODE"]][$vals[$j]['attributes']['ID']]=
                        //         array('L'=>1,
                        //         'W'=>1,'COUNT'=>$vals[$i1]["attributes"]["COUNT"]*$vals[$i1]["attributes"]["L"]/1000*$vals[$i1]["attributes"]["W"]/1000,
                        //         'S'=>round($vals[$i1]["attributes"]["COUNT"]*$vals[$i1]["attributes"]["L"]*$vals[$i1]["attributes"]["W"]/1000/1000,2),
                        //         'ID'=>$vals[$i]["attributes"]["ID"],
                        //             'triml'=>$ctriml_cs,'trimw'=>$ctrimw_cs,'saw'=>$saw_cs,'glass'=>1
                        //         );
                        //     }
                        //     elseif(!isset($vals[$i1]['attributes']['SHEETID']))
                        //     {
                        //         if (!isset($s_size_sheet[$vals[$i]["attributes"]["CODE"]][$vals[$i1]['attributes']['ID']]))
                        //         {
                        //             $s_size_sheet[$vals[$i]["attributes"]["CODE"]][$vals[$i1]['attributes']['ID']]=
                        //             array('L'=>1,
                        //             'W'=>1,'COUNT'=>$vals[$i1]["attributes"]["USEDCOUNT"]*$vals[$i1]["attributes"]["L"]/1000*$vals[$i1]["attributes"]["W"]/1000,
                        //             'S'=>round($vals[$i1]["attributes"]["USEDCOUNT"]*$vals[$i1]["attributes"]["L"]*$vals[$i1]["attributes"]["W"]/1000/1000,2),
                        //             'ID'=>$vals[$i]["attributes"]["ID"],
                        //             'triml'=>$ctriml_cs,'trimw'=>$ctrimw_cs,'saw'=>$saw_cs,'glass'=>1
                        //             );
                        //         }
                        //     }
                        //     $i1++;
                        // }
                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_finish_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

                    }
                }
            }
            // xml ($vals);
            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_find_cs_start_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
            foreach ($index["OPERATION"] as $io)
            {
                if (($vals[$io]["attributes"]["TYPEID"]=='CS')&&($vals[$io+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                {
                    // p_($vals[$i]);
                    
                    $sql21=$GL[$cs_mat[$vals[$io+1]["attributes"]["ID"]]]['res'];
                    if ($sql21==1) $sheet_gl=1;
                    else unset ($sheet_gl,$serv_cs_art_code);
                    // p_($cs_mat[$vals[$io+1]["attributes"]["ID"]]);exit;

                    if ($sheet_gl>0)
                    {
                        $serv_cs_art_code=18587;
                    }
                    elseif ($vals[$io]["attributes"]["operation.glass"]==1)  $serv_cs_art_code=113638;
                    else
                    {
                        if ($vals[$io]['attributes']['operation.my_fix_card'] == 1) {
                            $serv_cs_art_code = 113380;
                        }
                        elseif (($vals[$i]["attributes"]["T"] >= 5)
                            && ($vals[$i]["attributes"]["T"] <= 22)
                            && ($sql2["MY_1C_NOM"] <> 5931)
                            && (!isset($vals[$io]["attributes"]['operation.my_client_material_id']))
                            &&(substr_count($vals[$i]["attributes"]["NAME"],'Кухонная декоративная панель ') == 0))
                        {
                            $serv_cs_art_code=16419;
                        }
                        elseif (($vals[$io]["attributes"]['operation.my_client_material_id']>0)||($sql2["MY_1C_NOM"]==5931))
                        {  
                            $serv_cs_art_code=67231;
                        }
                        elseif ($vals[$i]["attributes"]["T"]<5) $serv_cs_art_code=16371;
                        elseif (($sql2["ST"]==1)OR(substr_count($vals[$i]["attributes"]["NAME"],'Кухонная декоративная панель ')>0))
                        {  
                            // p_($sql2);
                            // p_(substr_count($vals[$i]["attributes"]["NAME"],'Кухонная декоративная панель '));
                            $serv_cs_art_code=16367;
                        }
                        elseif ((!isset($vals[$i]["attributes"]["CODE"]))&&($vals[$i]["attributes"]["MY_FIRST"]>0))
                        {  
                            $serv_cs_art_code=86121;
                        }
                        elseif (($vals[$i]["attributes"]["T"]>22)&&($sql2["ST"]<1)&&($sql2["MY_1C_NOM"]<>5931))
                        {  
                            $serv_cs_art_code=20324;
                        }
                       
                        if (((($vals[$io]["attributes"]["CTRIML"]>0)OR($vals[$io]["attributes"]["CTRIMW"]>0)))&&(!isset($serv_cs_art_code)))
                        {
                            $serv_cs_art_code=44744;
                        }
                    }
                    // p_($sql2);
                    // p_($vals[$io]);
                    // p_($serv_cs_art_code);
                    $service[$serv_cs_art_code]+=$vals[$io]["attributes"]["CTRIMLENGTH"];

                    $cs_mat[]=array($io=>$i);
                    $ip=$io+2;
                    while ($vals[$ip]["tag"]=="PART")
                    {
                        foreach ($index["PART"] as $ipp)
                        {
                            
                            if ($vals[$ipp]["attributes"]["ID"]==$vals[$ip]["attributes"]["ID"])
                            {
                                
                                if (in_array($ipp,$parts))
                                {
                                    $rt[]=$vals[$ipp]["attributes"]["ID"];
                                    $parts_s[$vals[$i]["attributes"]["CODE"]][$vals[$ipp]["attributes"]["ID"]]=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["DL"]*$vals[$ipp]["attributes"]["DW"]/1000/1000,2);
                                    if (isset($vals[$ipp]["attributes"]["NAME"]))$part[$vals[$ipp]["attributes"]["ID"]]["mat"]=$vals[$ipp]["attributes"]["NAME"];
                                    $part[$vals[$ipp]["attributes"]["ID"]]["service_porezka"][$serv_cs_art_code]=round($vals[$ipp]["attributes"]["COUNT"]*($vals[$ipp]["attributes"]["DL"]/1000*2+$vals[$ipp]["attributes"]["DW"]/1000*2)*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                
                                
                                }
                                elseif (in_array($ipp,$parts_d))
                                {
                                    $rt[]=$vals[$ipp]["attributes"]["ID"];
                                    $parts_s_d[$vals[$i]["attributes"]["NAME"]][$vals[$ipp]["attributes"]["ID"]]=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["DL"]*$vals[$ipp]["attributes"]["DW"]/1000/1000,2);
                                    if (isset($vals[$ipp]["attributes"]["NAME"])) $part[$vals[$ipp]["attributes"]["ID"]]["mat"]=$vals[$ipp]["attributes"]["NAME"];
                                    $part[$vals[$ipp]["attributes"]["ID"]]["service_porezka"][$serv_cs_art_code]=round($vals[$ipp]["attributes"]["COUNT"]*($vals[$ipp]["attributes"]["DL"]/1000*2+$vals[$ipp]["attributes"]["DW"]/1000*2)*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                    $vb1=$parts_s_d[$vals[$i]["attributes"]["NAME"]][$vals[$ipp]["attributes"]["ID"]]/$vals[$ipp]["attributes"]["COUNT"];
                                    if ($vb1<=1)
                                    {
                                        $vb="77350";
                                        $part[$vals[$ipp]["attributes"]["ID"]]["service_styagka"][$vb]=$vals[$ipp]["attributes"]["COUNT"]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                        $part[$vals[$ipp]["attributes"]["ID"]]["service_styagka_price"][$vb]=$vals[$ipp]["attributes"]["COUNT"]*$service_pr[$vb]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                        $service[$vb]+=$vals[$ipp]["attributes"]["COUNT"]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                    }
                                    if (($vb1>1)&&($vb1<=2))
                                    {
                                        $vb="77351";
                                        $part[$vals[$ipp]["attributes"]["ID"]]["service_styagka"][$vb]=$vals[$ipp]["attributes"]["COUNT"]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                        $part[$vals[$ipp]["attributes"]["ID"]]["service_styagka_price"][$vb]=$vals[$ipp]["attributes"]["COUNT"]*$service_pr[$vb]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                        $service[$vb]+=$vals[$ipp]["attributes"]["COUNT"]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                    }
                                    if ($vb1>2)
                                    {
                                        $vb="77352";
                                        $part[$vals[$ipp]["attributes"]["ID"]]["service_styagka"][$vb]=$vals[$ipp]["attributes"]["COUNT"]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                        $part[$vals[$ipp]["attributes"]["ID"]]["service_styagka_price"][$vb]=$vals[$ipp]["attributes"]["COUNT"]*$service_pr[$vb]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                        $service[$vb]+=$vals[$ipp]["attributes"]["COUNT"]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                    }
                                    unset($vb,$vb1);
                                }
                                
                                if(isset($sql2)&&(in_array($ipp,$parts))) {
                                    $vals[$part_con[$vals[$ipp]['attributes']['ID']]]['attributes']['MY_MAT']=$sql2['MATERIAL_ID'];
                                }
                               
                                if ((in_array($ipp,$parts_d))OR(in_array($ipp,$parts)))
                                {
                                    $side1=array("L","R","T","B");
                                    foreach ($side1 as $s1)
                                    {
                                        if ((isset($vals[$ipp]["attributes"]["MY_CUT_ANGLE_".$s1]))&&($vals[$ipp]["attributes"]["MY_CUT_ANGLE_".$s1]<>0))
                                        {
                                            
                                            if (($s1=="T")OR($s1=="B"))
                                            {
                                                $service[23351]+=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CL"]/1000*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                                $part[$vals[$ipp]["attributes"]["ID"]]["service_zarez"][$s1]=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CL"]/1000*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                                $part[$vals[$ipp]["attributes"]["ID"]]["service_zar"][23351]+=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CL"]/1000*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                                $part[$vals[$ipp]["attributes"]["ID"]]["service_zarez_price"][$s1]=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CL"]/1000,2)*$service_pr[23351]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                                
                                            }
                                            elseif (($s1=="L")OR($s1=="R"))
                                            {
                                                $service[23351]+=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CW"]/1000*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                                $part[$vals[$ipp]["attributes"]["ID"]]["service_zarez"][$s1]=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CW"]/1000*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                                $part[$vals[$ipp]["attributes"]["ID"]]["service_zar"][23351]+=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CW"]/1000*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'],2);
                                                $part[$vals[$ipp]["attributes"]["ID"]]["service_zarez_price"][$s1]=round($vals[$ipp]["attributes"]["COUNT"]*$vals[$ipp]["attributes"]["CW"]/1000,2)*$service_pr[23351]*$part[$vals[$ipp]["attributes"]["ID"]]['p_count'];
                                            }
                                            $part[$vals[$ipp]["attributes"]["ID"]]["service_zarez_angle"][$s1]=$vals[$ipp]["attributes"]["MY_CUT_ANGLE_".$s1];
                                            
                                        }
                                    }
                                }
                            }

                        }
                        $ip++;
                    }
                    
                    unset($rt,$r,$count,$per);
                    unset($sql2);
                   
                }
            }
            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_find_cs_fin_mat_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

// exit;

        }
        if ($vals[$i]["attributes"]["TYPEID"]=="band")
        {
            unset($dfd);
            foreach ($index["OPERATION"] as $io)
            {
                if (($vals[$io]["attributes"]["TYPEID"]=="CL")&&($vals[$io+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                {
                    $dfd=1;
                }
            }
            if (!isset($dfd))
            {
                if (is_numeric($vals[$i]['attributes']['CODE']))
                {
                    $band_w[$vals[$i]["attributes"]["CODE"]]=$vals[$i]["attributes"]["W"];
                    $sql_b = 'SELECT * from BAND WHERE CODE='.$vals[$i]['attributes']['CODE'];
                    $sql_b2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_b)['data'][0];
                    // p_($sql_b2);
                    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_start_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

                    foreach ($index["OPERATION"] as $io)
                    {
                        if (($vals[$io]["attributes"]["TYPEID"]=="EL")&&($vals[$io+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                        {
                            if ($vals[$io+1]["attributes"]["COUNT"]>0)
                            {
                                if ($p1_check[$vals[$io+1]["attributes"]["ID"]]<>1)
                                {   
                                    $p1[$vals[$i]["attributes"]["CODE"]]+=$vals[$io+1]["attributes"]["COUNT"];
                                    $p1_check[$vals[$io+1]["attributes"]["ID"]]=1;
                                }
                            } 
                            unset($el_in);
                            $el_mat[]=array($io=>$i);
                            $ip=$io+2;
                            if ($vals[$ip]["tag"]=="PART")
                            {
                                $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_el_found_part_start_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                                
                                // p_($e4);exit;
                                while ($vals[$ip]["tag"]=="PART")
                                {
                                    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_el_part_start_id_part='.$vals[$ip]["attributes"]["ID"].' '.'id_b='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                                    $e4=get_all_part($vals,$vals[$ip]["attributes"]["ID"]);
                                    // p_($e4);exit;
                                    unset($len_op_el);
                                    unset($quan_sves);
                                    if ($e4['part']['attributes']['CL']>0)
                                    {
                                        if ($e4['part']['attributes']['ELL']=="@operation#".$vals[$io]["attributes"]["ID"])
                                        {
                                            $part[$e4['part']['attributes']['ID']]["left_band"]=$vals[$i]["attributes"]["CODE"];
                                            $part[$e4['part']['attributes']['ID']]["left_band_name"]="[/".$vals[$i]["attributes"]["CODE"].'/] '.$sql_b2['NAME'];
                                            $part[$e4['part']['attributes']['ID']]["band"][$vals[$i]["attributes"]["CODE"]]+=round($e4['part']['attributes']['COUNT']*$e4['part']['attributes']['DW']/1000*$e4['product']['attributes']['COUNT'],2);
                                            $len_op_el+=round($e4['part']['attributes']["COUNT"]*$e4['part']["attributes"]["CW"]/1000*$e4['product']['attributes']['COUNT'],2);
                                            $vals[$e4['part_index']]["attributes"]["MY_DB_ELL"]=$sql_b2['BAND_ID'];
                                            
                                            if ($vals[$io]['attributes']['MY_CHECK_KL']<>1) $vals[$e4['part_index']]["attributes"]["MY_DB_ELL"]=$sql_b2['BAND_ID'];
                                            else $vals[$e4['part_index']]["attributes"]["MY_DB_KL_ELL"]=$sql_b2['BAND_ID'];
                                            $quan_sves++;
                                            $e5[4]=1;
                                        }
                                        if ($e4['part']['attributes']["ELR"]=="@operation#".$vals[$io]["attributes"]["ID"])
                                        {
                                            $part[$e4['part']['attributes']["ID"]]["band"][$vals[$i]["attributes"]["CODE"]]+=round($e4['part']['attributes']["COUNT"]*$e4['part']['attributes']["DW"]/1000*$e4['product']['attributes']['COUNT'],2);
                                            $part[$e4['part']['attributes']["ID"]]["right_band_name"]="[/".$vals[$i]["attributes"]["CODE"].'/] '.$sql_b2['NAME'];
                                            $part[$e4['part']['attributes']["ID"]]["right_band"]=$vals[$i]["attributes"]["CODE"];
                                            $len_op_el+=round($e4['part']['attributes']["COUNT"]*$e4['part']['attributes']['CW']/1000*$e4['product']['attributes']['COUNT'],2);
                                            $vals[$e4['part_index']]["attributes"]["MY_DB_ELR"]=$sql_b2['BAND_ID'];
                                            
                                            if ($vals[$io]['attributes']['MY_CHECK_KL']<>1) $vals[$e4['part_index']]["attributes"]["MY_DB_ELR"]=$sql_b2['BAND_ID'];
                                            else $vals[$e4['part_index']]["attributes"]["MY_DB_KL_ELR"]=$sql_b2['BAND_ID'];
                                            $quan_sves++;
                                            $e5[3]=1;

                                        }
                                        if ($e4['part']['attributes']["ELT"]=="@operation#".$vals[$io]["attributes"]["ID"])
                                        {
                                            $part[$e4['part']['attributes']["ID"]]["band"][$vals[$i]["attributes"]["CODE"]]+=round($e4['part']['attributes']["COUNT"]*$e4['part']['attributes']["DL"]/1000*$e4['product']['attributes']['COUNT'],2);
                                            $part[$e4['part']['attributes']["ID"]]["top_band_name"]="[/".$vals[$i]["attributes"]["CODE"].'/] '.$sql_b2['NAME'];
                                            $part[$e4['part']['attributes']["ID"]]["top_band"]=$vals[$i]["attributes"]["CODE"];
                                            $len_op_el+=round($e4['part']['attributes']["COUNT"]*$e4['part']['attributes']["CL"]/1000*$e4['product']['attributes']['COUNT'],2);
                                            $vals[$e4['part_index']]["attributes"]["MY_DB_ELT"]=$sql_b2['BAND_ID'];
                                            
                                            if ($vals[$io]['attributes']['MY_CHECK_KL']<>1) $vals[$e4['part_index']]["attributes"]["MY_DB_ELT"]=$sql_b2['BAND_ID'];
                                            else $vals[$e4['part_index']]["attributes"]["MY_DB_KL_ELT"]=$sql_b2['BAND_ID'];
                                            $quan_sves++;
                                            $e5[2]=1;

                                        }
                                        if ($e4['part']['attributes']["ELB"]=="@operation#".$vals[$io]["attributes"]["ID"])
                                        {
                                            $part[$e4['part']['attributes']["ID"]]["band"][$vals[$i]["attributes"]["CODE"]]+=round($e4['part']['attributes']["COUNT"]*$e4['part']['attributes']["DL"]/1000*$e4['product']['attributes']['COUNT'],2);
                                            $part[$e4['part']['attributes']["ID"]]["botton_band_name"]="[/".$vals[$i]["attributes"]["CODE"].'/] '.$sql_b2['NAME'];
                                            $part[$e4['part']['attributes']["ID"]]["botton_band"]=$vals[$i]["attributes"]["CODE"];
                                            $len_op_el+=round($e4['part']['attributes']["COUNT"]*$e4['part']['attributes']["CL"]/1000*$e4['product']['attributes']['COUNT'],2);
                                            $vals[$e4['part_index']]["attributes"]["MY_DB_ELB"]=$sql_b2['BAND_ID'];
                                            if ($vals[$io]['attributes']['MY_CHECK_KL']<>1) $vals[$e4['part_index']]["attributes"]["MY_DB_ELB"]=$sql_b2['BAND_ID'];
                                            else $vals[$e4['part_index']]["attributes"]["MY_DB_KL_ELB"]=$sql_b2['BAND_ID'];
                                            $quan_sves++;
                                            $e5[1]=1;

                                        }
                                        // p_($len_op_el);
                                        // if ($len_op_el==0)
                                        // {
                                        //     p_($vals[$e4['part_index']]["attributes"]);
                                        //     p_($e5);
                                        // }
                                        // p_($part[$e4['part']['attributes']["ID"]]['band']);
                                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_el_part_cs_start_id_part='.$vals[$ip]["attributes"]["ID"].' '.'id_b='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                                        // p_($e4);exit;
                                        // $sql21=$GL[$code]['res'];
                                        // if ($sql21["val"]==1) $sheet_gl=1;
                                        // else unset ($sheet_gl);
                                        
                                        if ((isset($e4['cs']['sheet']['attributes']['CODE']))&&($e4['cs']['sheet']['attributes']['CODE']>0)&&(is_numeric($e4['cs']['sheet']['attributes']['CODE'])))
                                        {
                                            $sql_a = 'SELECT * from MATERIAL WHERE CODE='.$e4['cs']['sheet']['attributes']['CODE'];
                                            $sql_a2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_a)['data'][0];
                                        }
                                        $sql21=$GL[$cs_mat[$e4['cs']['sheet']['attributes']["ID"]]]['res'];
                                        // if ($sql21==1) $sheet_gl=1;
                                        // else unset ($sheet_gl,$serv_cs_art_code);
                                        unset ($sheet_gl,$serv_cs_art_code);
                                        if ($sheet_gl>0)
                                        {
                                            $code_el=23355; 
                                            
                                        }
                                        else
                                        {
                                            if($sql_a2['MY_1C_NOM']==5931)
                                            {
                                                if ($vals[$io]["attributes"]["MY_CHECK_KL"]>0)
                                                {    
                                                    $code_el=86249;
                                                }
                                                else
                                                {
                                                    $code_el=86248;
                                                }
                                            }
                                            elseif($sql_b2['MY_1C_NOM']==6751)
                                            {
                                                if ($vals[$io]["attributes"]["MY_CHECK_KL"]>0)
                                                {    
                                                    $code_el=77354;
                                                }
                                                else
                                                {
                                                    $code_el=16320;
                                                }
                                            }
                                            else
                                            {
                                                if ($vals[$io]["attributes"]["MY_CHECK_KL"]>0)
                                                {  
                                                    if(($e4['cs']['sheet']['attributes']['T']>=25)&&($sql_a2['ST']<>1))
                                                    {
                                                        $code_el=16303;
                                                    }
                                                    elseif(($sql_a2['ST']==1)&&($sql_b2['MY_TYPE']=="пластик"))
                                                    {
                                                        $code_el=77354;
                                                    }
                                                    elseif($e4['cs']['sheet']['attributes']['T']<25)
                                                    {
                                                        $code_el=16302;
                                                    }
                                                    elseif($sql_a2['ST']==1)
                                                    {
                                                        $code_el=16303;
                                                    }
                                                }
                                                else
                                                {
                                                    if ($vals[$io]['attributes']['MY_EL_TYPE']=='laz')
                                                    {
                                                        $code_el=87266;
                                                    }
                                                    elseif ($vals[$io]['attributes']['MY_EL_TYPE']=='pur')
                                                    {
                                                        if(($e4['cs']['sheet']['attributes']['T']>=25)&&($sql_a2['ST']<>1))
                                                        {
                                                            $code_el=68045;
                                                        }
                                                        elseif($sql_a2['ST']==1)
                                                        {
                                                            $code_el=86122;
                                                        }
                                                        elseif($e4['cs']['sheet']['attributes']['T']<25)
                                                        {
                                                            $code_el=68044;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if(($e4['cs']['sheet']['attributes']['T']>=25)&&($sql_a2['ST']<>1))
                                                        {
                                                            $code_el=16305;
                                                        }
                                                        elseif(($sql_a2['ST']==1)&&($sql_b2['MY_TYPE']=="пластик"))
                                                        {
                                                            $code_el=16320;
                                                        }
                                                        elseif($sql_a2['ST']==1)
                                                        {
                                                            // exit('22');
                                                            $code_el=16304;
                                                            // test('strait',$code_el);
                                                        }
                                                        elseif($e4['cs']['sheet']['attributes']['T']<25)
                                                        {
                                                            //.. 02.01.2023 Тоценко
                                                            // Добавлено кромкование глянцевых панелей
                                                            if (stripos($sql_b2['NAME'], 'глянец') || stripos($sql_b2['NAME'], 'Глянец')) {
                                                                $code_el=23355;
                                                            } else {
                                                                $code_el=16301;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        if (($len_op_el>0)&&(isset($code_el)))
                                        {
                                        
                                            $service[$code_el]+= $len_op_el;
                                            $part[$e4['part']['attributes']['ID']]['kromkovanie'][$code_el]+= $len_op_el;
                                            
                                            
                                        }
                                        unset($len_op_el,$code_el);
                                        if ($vals[$i]["attributes"]["W"]-$e4['cs']['sheet']['attributes']['T']>5)
                                        {
                                            
                                            if(!isset($band_cut[$vals[$i]["attributes"]["CODE"]]))
                                            { 
                                                
                                                $band_cut[$vals[$i]["attributes"]["CODE"]]=1;
                                            }
                                        }
                                        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_el_cs_found_part_fin_in_op_cs_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
                                    }
                                    $ip++;
                                }
                                $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_el_found_part_fin_'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

                            }
                            
                        }
                    }
                    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate_band_end'.'id='.$vals[$i]["attributes"]["ID"].' CODE ='.$vals[$i]["attributes"]["CODE"].'  '.$vals[$i]["attributes"]["NAME"].$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
            
                }
            }
        }
    }
    
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate mat_band_end'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
         
    foreach($part as $r=>$r11)
    {
        foreach ($part[$r]["service_porezka"] as $trtt=>$trtt2)
        {
            $count[$trtt]+=$trtt2;
        }
        
    }
    foreach ($count as $trtt=>$trtt2)
    {
        $per[$trtt]=$service[$trtt]/$trtt2;
    }
    
    foreach($part as $r=>$r11)
    {
        foreach ($part[$r]["service_porezka"] as $trtt=>$trtt2)
        {
            $part[$r]["service_porezka"][$trtt]=round($part[$r]["service_porezka"][$trtt]*$per[$trtt],2);
            $part[$r]["service_porezka_price"][$trtt]=round($part[$r]["service_porezka"][$trtt]*$service_pr[$trtt],2);
        }
        
        
    }

    foreach ($s_size_sheet as $k=>$id12)
    {
        foreach($id12 as $id=>$kk)
        {
            
            if ((isset($s_size[$k][$id]))&&($s_size_sheet[$k][$id]['glass']<>1))
            {
                // Тут вычисляется, вписываемся в поллиста или нет
                //.. 08.05.2023 Тоценко
                // В рассчёт добавлен 1мм, т.к. поллиста считалось не правильно на пограничных размерах.
                //.. 13.09.2023 Тоценко
                // 1мм убран, и найдена настоящая причина. В расчёте были перепутаны длина с шириной для подрезки (было trimW, стало trimL)
                $ar = ($s_size[$k][$id]['W']) - ($kk['W'] / 2) - $kk['saw'] - $kk['trimL'];
                if ($ar>=0)
                {
                    $s_size_sheet[$k][$id]['COUNT']=$s_size_sheet[$k][$id]['COUNT']-0.5;
                    $s_size_sheet[$k][$id]['S']=round($s_size_sheet[$k][$id]['COUNT']*$s_size_sheet[$k][$id]['L']*$s_size_sheet[$k][$id]['W'],2);
                }
            }
            elseif ($s_size_sheet[$k][$id]['glass']==1)
            {
                foreach ($s_size[$k] as $k1=>$v1)
                {
                    $s_size_sheet[$k][$id]['COUNT']=$s_size_sheet[$k][$id]['COUNT']-$s_size[$k][$k1]['COUNT'];
                    $s_size_sheet[$k][$id]['S']=round($s_size_sheet[$k][$id]['COUNT']*$s_size_sheet[$k][$id]['L']*$s_size_sheet[$k][$id]['W'],2);
                }
            }
            
        }
    }

    foreach ($s_size_sheet as $k=>$id12)
    {
        foreach($id12 as $id=>$kk)
        {
            $s[$k]+=round($s_size_sheet[$k][$id]['S'],2);
            
        }
    }

    foreach ($part_cut_pf as $id=>$por_pf)
    {

        $part[$id]["service_porezka"][16367]+=round($por_pf,2);
        $part[$id]["service_porezka_price"][16367]+=round($por_pf*$service_pr[16367],2);
        $service[16367]+=round($por_pf,2);

    }
    // p_($part);
    // p_($service);
    
    // exit;
    
    foreach ($p1 as $code=>$q)
    {
        foreach($part as $id=>$v)
        {
            foreach ($v["band"] as $c=>$b)
            {
            
            if ($code==$c) $p11[$c]+=$b;
            }
        }
    }
    foreach($part as $id=>$v)
    {
        foreach ($v["band"] as $c=>$b)
        {
            $part[$id]["band"][$c]=round($part[$id]["band"][$c]/$p11[$c]*$p1[$c],2);
        }
    }
    unset($c,$b,$serv1,$id,$v);
    foreach($part as $id=>$v)
    {
        foreach ($v["kromkovanie"] as $co=>$b)
        {
            $serv1[$co]+=$b;
        }
    }
    // p_($part);exit;
    foreach($part as $id=>$v)
    {
        foreach ($v["band"] as $c=>$b)
        {
            
            if(($band_cut[$c]>0)&&($band_w[$c]-$v["T"]>6))
            {
                $part[$id]["service_por_kromki_kod"].=$c." ";
                $part[$id]["service_por_kromki"]+=$b;
                $service[23352]+=$b;
                $part[$id]["service_por_kromki_price"]+=round($b*$service_pr[23352],2);
                $part[$id]["por_kromki"][23352]+=$b;
            }
        }
        foreach ($v["kromkovanie"] as $co=>$b)
        {
            $part[$id]['kromkovanie'][$co]=round($b/$serv1[$co]*$service[$co],2);
            $part[$id]['kromkovanie_price'][$co]=round(round($b/$serv1[$co]*$service[$co],2)*$service_pr[$co],2);
        }
    }
    unset ($p11);
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate part_array_final'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);



    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=='GR')&&($vals[$i]['type']=='open')&&($vals[$i+1]['tag']=="PART"))
        {
            if ((($vals[$i]['attributes']['GROFFSETINCL']=="false")AND
                ($vals[$i]['attributes']['GROFFSET']==0)AND
                ($vals[$i]['attributes']['GRSAWTHICK']>4)AND
                ($vals[$i]['attributes']['GRWIDTH']<=8)
                )
                OR
                (
                    ($vals[$i]['attributes']['GROFFSETINCL']=="true")AND
                    ($vals[$i]['attributes']['GROFFSET']>4)AND
                    ($vals[$i]['attributes']['GRWIDTH']>4)AND
                    ($vals[$i]['attributes']['GRWIDTH']<=8)
                ))

            {
                $service[23357]+=$vals[$i]['attributes']['GRCUTTINGLENGTH']/2;
                foreach ($index['PART'] as $g)
                {
                    foreach (SIDE as $s33)
                    {
                        if ($vals[$g]['attributes']['GR'.mb_strtoupper($s)]=="@operation#".$vals[$i]['attributes']['ID'])
                        {
                            if (($s33=="l")OR($s33=="r"))
                            {
                                $part[$vals[$g]['attributes']['ID']]['GR_GR_ADV']+=$vals[$g]['attributes']['CW']*$vals[$g]['attributes']['COUNT']*$part[$vals[$g]['attributes']['ID']]['p_count']/1000;
                            }
                            else
                            {
                                $part[$vals[$g]['attributes']['ID']]['GR_GR_ADV']+=$vals[$g]['attributes']['CL']*$vals[$g]['attributes']['COUNT']*$part[$vals[$g]['attributes']['ID']]['p_count']/1000;
                            }
                            $part[$vals[$g]['attributes']['ID']]['GR_GR_ADV_price']+=$vals[$g]['attributes']['CL']*$vals[$g]['attributes']['COUNT']/1000*$service_pr[23357]*$part[$vals[$g]['attributes']['ID']]['p_count'];

                        }

                    }
                }
            }
            else
            {
                $service[16366]+=$vals[$i]['attributes']['GRCUTTINGLENGTH'];
                foreach ($index['PART'] as $g)
                {
                    foreach (SIDE as $S3)
                    {
                        if ($vals[$g]['attributes']['GR'.mb_strtoupper($S)]=="@operation#".$vals[$i]['attributes']['ID'])
                        {
                            if (($S3=="l")OR($S3=="r"))
                            {
                                $part[$vals[$g]['attributes']['ID']]['GR_GR']+=$vals[$g]['attributes']['CW']*$vals[$g]['attributes']['COUNT']/1000*$part[$vals[$g]['attributes']['ID']]['p_count'];
                            }
                            else
                            {
                                $part[$vals[$g]['attributes']['ID']]['GR_GR']+=$vals[$g]['attributes']['CL']*$vals[$g]['attributes']['COUNT']/1000*$part[$vals[$g]['attributes']['ID']]['p_count'];
                            }
                        }

                    }
                }
            }
            
            //*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']

            
        }
    }
    if ($service[16366]>0)
    {
        foreach ($part as $id=>$p_gr)
        {
            $gr_sum+=$p_gr['GR_GR'];
        }
        if ($gr_sum>0) $per_gr=$service[16366]/$gr_sum;
        else $per_gr=1;
        foreach ($part as $id=>$p_gr)
        {
            if (isset($part[$id]['GR_GR']))
            {
                $part[$id]['GR_GR']=$p_gr['GR_GR']*$per_gr;
                $part[$id]['GR_GR_price']=$part[$id]['GR_GR']*$service_pr[16366];

            }
        }
    }
    $index=make_index($vals);
    $vals=add_tag_print($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=='XNC')&&($vals[$i]['type']=='open')&&($vals[$i+1]['tag']=="PART")&&($vals[$i]['attributes']['operation.my_mpr_in']==1))
        {
            unset ($idkk);
            foreach ($vals[$i]['attributes'] as $kkk=>$vvv)
            {
                if (is_numeric(substr($kkk,17)))
                {
                   $idkk=substr($kkk,17);
                } 
            }
            // p_($vals[$i]); exit;
            if (($idkk>0)&&(is_numeric($idkk)))
            {
                $sql="SELECT * FROM ticket_mpr_price WHERE mpr_id=".$idkk;
                // echo $sql;
                $mpr=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                // p_($mpr);exit;

                foreach ($mpr as $mpr_pr)
                {
                    $sql2="SELECT * FROM SERVICE WHERE SERVICE_ID=".$mpr_pr['service_id'];
                    $serv_code=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql2)['data'][0]['CODE'];
                    $service[$serv_code]+=$mpr_pr['count']*$part[$vals[$i+1]['attributes']['ID']]['p_count']*$part[$vals[$i+1]['attributes']['ID']]['count'];
                    $part_mpr[$vals[$i+1]['attributes']['ID']][$serv_code]+=$mpr_pr['count'];
                }
            }
            $mpr_arr=array(
                85894=>'fr_450',
                85895=>'fr',
                16366=>'GR_GR',
                23357=>'GR_GR_ADV',
                26321=>'OTV_do_20',
                20323=>'prisadka_petlya',
                110429=>'prisadka_150',
                22160=>'prisdka_pod_ruchku'


            );
            foreach ($mpr_arr as $k4=>$v4)
            {
                if (isset($part_mpr[$vals[$i+1]['attributes']['ID']][$k4]))
                {
                    $part[$vals[$i+1]['attributes']['ID']][$v4]+=$part_mpr[$vals[$i+1]['attributes']['ID']][$k4]*$part[$vals[$i+1]['attributes']['ID']]['p_count']*$part[$vals[$i+1]['attributes']['ID']]['count'];
                    $part[$vals[$i+1]['attributes']['ID']]['XNC'][$k4]+=$part_mpr[$vals[$i+1]['attributes']['ID']][$k4]*$part[$vals[$i+1]['attributes']['ID']]['p_count']*$part[$vals[$i+1]['attributes']['ID']]['count'];
                    $part[$vals[$i+1]['attributes']['ID']][$v4.'_price']+=$part_mpr[$vals[$i+1]['attributes']['ID']][$k4]*$service_pr[$k4]*$part[$vals[$i+1]['attributes']['ID']]['p_count']*$part[$vals[$i+1]['attributes']['ID']]['count'];
                    unset ($part_mpr[$vals[$i+1]['attributes']['ID']][$k4]);
                }
            }
           
            
                   
            
            
            
        }
    }
    // p_($s);
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate gr_end'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

    foreach ($part_mpr as $k4=>$v4)
    {
        foreach ($v4 as $kk4=>$vv4)
        {
            if (!isset($aa)) $part[$k4]['serv_adv']=' | доп.услуги: ';
            $sql2="SELECT * FROM SERVICE WHERE SERVICE_ID=".$mpr_pr['service_id'];
            $serv_code=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql2)['data'][0]['NAME'];
            $part[$k4]['serv_adv'].=$kk4.' ('.$serv_code.') -'.$vv4.' шт ('.($vv4*$service_pr[$kk4]).' грн.) ';
            $part[$k4]['serv_adv_price']=$vv4*$service_pr[$kk4];
            
        }
        unset ($aa);
    }
    // p_($part_mpr);
    // p_($part);
    // exit;
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate adv_serv'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);


    foreach ($index["OPERATION"] as $i)
    {
       
        if (($vals[$i]['attributes']['TYPEID']=='XNC')&&($vals[$i]['type']=='open')&&($vals[$i+1]['tag']=="PART")&&((!isset($vals[$i]['attributes']['operation.my_mpr_in'])||($vals[$i]['attributes']['operation.my_mpr_in']==''))))
        {
            $p = xml_parser_create();
            xml_parse_into_struct($p, $vals[$i]['attributes']['PROGRAM'], $v, $v_index);
            xml_parser_free($p);
            if($vals[$i]['attributes']['COUNTCUT']>0)
            {
                // p_($v_index);
                foreach ($v_index['GR'] as $vi2)
                {
                    // p_($v[$vi2]);
                    if ($v[$vi2]['attributes']['Y1']==$v[$vi2]['attributes']['Y2'])
                    {
                        $len=abs($v[$vi2]['attributes']['X1']-$v[$vi2]['attributes']['X2'])*round($v[$vi2]['attributes']['T']/3,0)/1000;
                    }
                    elseif ($v[$vi2]['attributes']['X1']==$v[$vi2]['attributes']['X2'])
                    {
                        $len=abs($v[$vi2]['attributes']['Y1']-$v[$vi2]['attributes']['Y2'])*round($v[$vi2]['attributes']['T']/3,0)/1000;
                    }
                    if (
                        (($v[$vi2]['attributes']['T']>4)&&($v[$vi2]['attributes']['T']<=8))&&(
                        (($v[$vi2]['attributes']['X1']==$v[0]['attributes']['DX'])&&($v[$vi2]['attributes']['X2']==$v[0]['attributes']['DX']))OR
                        (($v[$vi2]['attributes']['X1']==0)&&($v[$vi2]['attributes']['X2']==0))OR
                        (($v[$vi2]['attributes']['Y1']==$v[0]['attributes']['DY'])&&($v[$vi2]['attributes']['Y2']==$v[0]['attributes']['DY']))OR
                        (($v[$vi2]['attributes']['Y1']==0)&&($v[$vi2]['attributes']['Y2']==0))
                        )
                    )
                    {
                        
                        $part[$vals[$i+1]['attributes']['ID']]['GR_GR_ADV']+=$len/2*$part[$vals[$i]['attributes']['ID']]['p_count'];
                        
                        $part[$vals[$i+1]['attributes']['ID']]['GR_GR_ADV_price']+=$len/2*$service_pr[23357]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[23357]+=$len/2*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    }
                    else
                    {
                        
                        
                        $part[$vals[$i+1]['attributes']['ID']]['GR_GR']+=$len*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['GR_GR_price']+=$len*$service_pr[16366]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[16366]+=$len*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        
                        
                    
                    }
                    
                }
            }
            // p_($part);
            // exit;
            if (($vals[$i]['attributes']['TYPEID']==$vals_shablon[$i]['attributes']['TYPEID'])&&($vals[$i]['attributes']['ID']==$vals_shablon[$i]['attributes']['ID']))
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals_shablon[$i]['attributes']['PROGRAM'], $vals_shablon, $v_index_shablon);
                xml_parser_free($p);
            }
            foreach ($v_index['TOOL'] as $vi)
            {
                $tool[$v[$vi]['attributes']['NAME']]=$v[$vi]['attributes']['D'];
            }
            // p_($v);
            $ar_c=array('MAC','MS','MA','ML');
            foreach ($ar_c as $a)
            {
                foreach ($v_index[$a] as $id_a)
                {
                    foreach ($v[$id_a]['attributes'] as $key_a=>$a_v)
                    {
                        if ($a_v<0) $v[$id_a]['attributes'][$key_a]=abs($a_v);
                    }
                }
            }
            // echo "1<hr>";

            // p_($v);
            foreach ($v_index['MS'] as $vi)
            {
                $j=0;
                if (($v[$vi+1]['tag']=='ML')OR($v[$vi+1]['tag']=='MA')OR($v[$vi+1]['tag']=='MAC'))
                {
                    $vi1=$vi+1;
                    while (($v[$vi1]['tag']=='ML')OR($v[$vi1]['tag']=='MA')OR($v[$vi1]['tag']=='MAC'))
                    {
                        $sx=$v[0]['attributes']['DX']/1000;
                        $sy=$v[0]['attributes']['DY']/1000;
                        $x=$v[$vi1-1]['attributes']['X']/1000;
                        $y=$v[$vi1-1]['attributes']['Y']/1000;
                        $x_now=$v[$vi1]['attributes']['X']/1000;
                        $y_now=$v[$vi1]['attributes']['Y']/1000;
                        $cx_now=$v[$vi1]['attributes']['CX']/1000;
                        $cy_now=$v[$vi1]['attributes']['CY']/1000;
                        $r_now=$v[$vi1]['attributes']['R']/1000;
                        $er=$v[$vi1]['tag'];
                        if ((($x>=0)&&($y>=0))&&(($x_now>=0)&&($y_now>=0)))
                        {
                            // p_($er);
                            
                            if(
                                (($x<>$x_now)OR(($x<>$sx)&&($x<>0)))
                                AND
                                ((($y<>$y_now)OR(($y<>$sy)&&($y<>0))))
                                //нижняя строка добавлена экспериментально - контроль просчёта фрезерного реза
                                OR(substr_count($er,"MAC")>0))
                            {
                                
                                if (substr_count($er,"ML")>0)
                                {
                                
                                    $fr_len+=sqrt(($x_now-$x)*($x_now-$x)+($y_now-$y)*($y_now-$y));
                                }
                                if (substr_count($er,"MAC")>0)
                                {
                                    $r=sqrt(abs(pow($cx_now-$x_now,2))+abs(pow($cy_now-$y_now,2)));
                                    $horda=sqrt(abs(pow($x-$x_now,2))+abs(pow($y-$y_now,2)));
                                    $h=$r-sqrt($r*$r-($horda*$horda/4));
                                    $d=$r-$h;
                                    $cut=2*acos($d/$r);
                                    $fr_len+=$r*$cut;
                                    // test ('fr',$fr_len);
                                }

                            }
                            else $j++;
                        }

                        unset($x,$y,$x_now,$y_now,$cx_now,$cy_now,$sx,$sy);
                        if($fr_len>0)$fr_length[$j]+=$fr_len;
                        unset($fr_len);
                        $vi1++;
                    }

                }
                // p_($fr_length);
                // exit;
                // echo "2<hr>";




                foreach ($fr_length as $f)
                {
                    if ($f<=0.45)
                    {
                        
                        
                        $part[$vals[$i+1]['attributes']['ID']]['fr_450']+=1*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['fr_450_price']+=$service_pr[85894]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[85894]+=$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    }
                    elseif ($f>0.45)
                    {
                        $part[$vals[$i+1]['attributes']['ID']]['fr']+=$f*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['fr_price']+=$f*$service_pr[85895]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[85895]+=round($f*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'],2);
                    }
                }
                unset($fr_length);
                
            }
            foreach ($v as $vi=>$vr)
            {
                if (($v[$vi]['tag']=='BF')OR($v[$vi]['tag']=='BL')OR($v[$vi]['tag']=='BR')or($v[$vi]['tag']=='BT')OR($v[$vi]['tag']=='BB'))
                {
                    if ((!isset($v[$vi]['attributes']['AC']))OR($v[$vi]['attributes']['AC']<1))$v[$vi]['attributes']['AC']=1;
                    if (($tool[$v[$vi]['attributes']['NAME']]<20)OR((($tool[$v[$vi]['attributes']['NAME']]<>26)&&($tool[$v[$vi]['attributes']['NAME']]<>35)&&($tool[$v[$vi]['attributes']['NAME']]==20)&&
                    ($part[$vals[$i+1]['attributes']['ID']]['T']-3>=$v[$vi]['attributes']['DP']))))
                    {
                        $part[$vals[$i+1]['attributes']['ID']]['OTV_do_20']+=$v[$vi]['attributes']['AC']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['OTV_do_20_price']+=$v[$vi]['attributes']['AC']*$service_pr[26321]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[26321]+=$v[$vi]['attributes']['AC']*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    }
                    elseif (($tool[$v[$vi]['attributes']['NAME']]<>26)&&($tool[$v[$vi]['attributes']['NAME']]<>35)&&($tool[$v[$vi]['attributes']['NAME']]>=20)&&
                    ($part[$vals[$i+1]['attributes']['ID']]['T']<=$v[$vi]['attributes']['DP']))
                    {
                        $part[$vals[$i+1]['attributes']['ID']]['prisadka_150s']+=$v[$vi]['attributes']['AC']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['prisadka_150s_price']+=$v[$vi]['attributes']['AC']*$service_pr[23360]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[23360]+=$v[$vi]['attributes']['AC']*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    }
                    elseif ((($tool[$v[$vi]['attributes']['NAME']]==26)OR($tool[$v[$vi]['attributes']['NAME']]==35))&&($v[$vi]['attributes']['DP']<=14.5)&&
                    ($part[$vals[$i+1]['attributes']['ID']]['T']-3>=$v[$vi]['attributes']['DP']))
                    {
                        $part[$vals[$i+1]['attributes']['ID']]['prisadka_petlya']+=$v[$vi]['attributes']['AC']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['prisadka_petlya_price']+=$v[$vi]['attributes']['AC']*$service_pr[20323]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[20323]+=$v[$vi]['attributes']['AC']*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    }
                
                    elseif ((($tool[$v[$vi]['attributes']['NAME']]<>26)&&($tool[$v[$vi]['attributes']['NAME']]<>35)&&($tool[$v[$vi]['attributes']['NAME']]>20)&&
                    ($part[$vals[$i+1]['attributes']['ID']]['T']-3>=$v[$vi]['attributes']['DP']))
                    OR
                    (((($tool[$v[$vi]['attributes']['NAME']]==26)OR($tool[$v[$vi]['attributes']['NAME']]==35))&&($v[$vi]['attributes']['DP']>14.5)&&
                    ($part[$vals[$i+1]['attributes']['ID']]['T']-3>=$v[$vi]['attributes']['DP']))))
                    {
                        $part[$vals[$i+1]['attributes']['ID']]['prisadka_150']+=$v[$vi]['attributes']['AC']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $part[$vals[$i+1]['attributes']['ID']]['prisadka_150_price']+=$v[$vi]['attributes']['AC']*$service_pr[110429]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                        $service[110429]+=$v[$vi]['attributes']['AC']*$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    }
                   
                }
                if ($v[$vi]['tag']=='MR')
                {
                    $part[$vals[$i+1]['attributes']['ID']]['prisdka_pod_ruchku']+=1*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    $part[$vals[$i+1]['attributes']['ID']]['prisdka_pod_ruchku_price']+=$service_pr[22160]*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                    $service[22160]+=$vals[$part_con[$vals[$i+1]['attributes']['ID']]]['attributes']['COUNT']*$part[$vals[$i+1]['attributes']['ID']]['p_count'];
                }
                // if ($vals[$i+1]['attributes']['ID']==83161){
                //     p_($v);
                //     p_($part[83161]);
                //     exit;
                // }
            }
            
        }
    }
    // $index=make_index($vals);
    // foreach ($index["OPERATION"] as $i)
    // {
    //     if ($vals[$i]['attributes']['TYPEID']=="XNC")
    //     {
    //         p_($vals[$i]['attributes']);
    //         p_($part[$vals[$i+1]['attributes']['ID']]);
    //     }
    // }
//    p_($service); 
//     exit;

// !!!
// p_($service);
// p_($part);
// exit;
$mpr_arr=array(
    85894=>'fr_450',
    85895=>'fr',
    26321=>'OTV_do_20',
    20323=>'prisadka_petlya',
    110429=>'prisadka_150',
    22160=>'prisdka_pod_ruchku'


);
foreach ($part as $id=>$p)
    {
        if ($part[$id]['GR_GR']>0) $part[$id]['GR'][16366]=$part[$id]['GR_GR'];
        if ($part[$id]['GR_GR_ADV']>0) $part[$id]['GR'][23357]=$part[$id]['GR_GR_ADV'];
    }
foreach ($part as $id=>$p)
{
    foreach ($mpr_arr as $k=>$v)
    {
        if (isset($part[$id][$v]))
        {
            $part[$id]['XNC'][$k]=$part[$id][$v];
        }
    }
}

$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate xnc'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);




    foreach ($s as $code=>$area)
    {
        foreach ($parts_s[$code] as $id=>$p_area)
        {
                $code_part_area[$code]+=$p_area;
        }
    }
    $s1=$s;

    foreach ($s1 as $m=>$m1)
    {
        $m2[]=$m;
    }
    foreach ($p1 as $m=>$m1)
    {
        $m2[]=$m;
    }
    foreach($index["GOOD"] as $i)
    {
        if ((($vals[$i]["attributes"]["TYPEID"]=="simple")&&($vals[$i]["attributes"]["COUNT"]>0))&&($vals[$i]["attributes"]["CODE"]>0)&&(is_numeric($vals[$i]["attributes"]["CODE"])))
        {
            $sql="SELECT * FROM SIMPLE WHERE CODE=".$vals[$i]["attributes"]["CODE"];
            $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            if (isset($sql4)) $m2[]=$vals[$i]["attributes"]["CODE"];
        }
    }
    if (($_SESSION['client_code']>0)&&($_SESSION['user_place']>0))
    {
        $m_get=get_actual_price($m2, $_SESSION['user_place'], $_SESSION['client_code']);
    } else {
        $m_get=get_actual_price($m2, $_SESSION['user_place']);
    }

    //.. 30.07.2022 Правки, касательно цен. Если цены не получены (по API из Акцента)
    // выводим ошибку и страницу с кнопками пересчёта и сохранения (файл calculate.php)
    // if(empty($m_get)) {
    //     $_SESSION['failed_prices'] = 1;
    // }


    foreach($index["GOOD"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="simple")&&($vals[$i]["attributes"]["COUNT"]>0))
        {
            // p_($vals[$i]);
            if ((is_numeric($vals[$i]["attributes"]["CODE"]))&&($vals[$i]["attributes"]["CODE"]>0)&&(!isset($vals[$i]["attributes"]["MY_RS_SERV"])))
            {
                $sql5="select * from SIMPLE where CODE=".$vals[$i]["attributes"]["CODE"];
                // echo $sql;
                $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql5)['data'][0];
                if (isset($sql2['SIMPLE_ID']))
                {

                
                    if (!isset($m_get[$vals[$i]["attributes"]["CODE"]]))
                    {
                        $sql="SELECT * FROM SIMPLE_PRICE WHERE SIMPLE_ID=".$sql2["SIMPLE_ID"]." AND PLACE_ID=".$place.";";
                        $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                        $sql4=check_price($sql4,"SIMPLE",$link,$sql2["SIMPLE_ID"]);
                    }
                    else $sql4["PRICE"]=$m_get[$vals[$i]["attributes"]["CODE"]]['PRICE'];
                    if(sql_data(__LINE__,__FILE__,__FUNCTION__,$sql5)['count']>0)
                    {
                        $simple_price[] = 
                        array(
                            'Name' =>$sql2["NAME"],
                            'Code' => $vals[$i]["attributes"]["CODE"],
                            'count' => $vals[$i]["attributes"]["COUNT"],
                            'price' => $sql4["PRICE"]
                        );
                    }
                }
                else
                {
                    $simple_price[] = 
                    array(
                        'Name' => $vals[$i]["attributes"]["NAME"],
                        'Code' => 0,
                        'count' => $vals[$i]["attributes"]["COUNT"],
                        'price' => 0
                    );
                }
                // p_( $simple_price);
            }
        
            elseif ((is_numeric($vals[$i]["attributes"]["SERVICE_ID"]))&&($vals[$i]["attributes"]["SERVICE_ID"]>0)&&($vals[$i]["attributes"]["MY_RS_SERV"]>0))
            {
                
                $sql34="select * from SERVICE where CODE=".$vals[$i]["attributes"]["CODE"];
                // echo $sql;
                $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql34)['data'][0];
                if (isset($sql2['SERVICE_ID']))
                {

                
                    if (!isset($m_get[$vals[$i]["attributes"]["CODE"]]))
                    {
                        $sql="SELECT * FROM SERVICE_PRICE WHERE SERVICE_ID=".$sql2["SERVICE_ID"]." AND PLACE_ID=".$place.";";
                        // echo $sql;
                        $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                        $sql4=check_price($sql4,"SERVICE",$link,$sql2["SERVICE_ID"]);
                        // p_($sql4);

                    }
                    else $sql4["PRICE"]=$m_get[$vals[$i]["attributes"]["CODE"]]['PRICE'];
                    // p_(sql_data(__LINE__,__FILE__,__FUNCTION__,$sql34)['res']);
                    if ((!isset($sql4["PRICE"]))||( $sql4["PRICE"]==0))  $sql4["PRICE"]=0.01;
                    if(sql_data(__LINE__,__FILE__,__FUNCTION__,$sql34)['res']>0)
                    {
                        $service_price[] = 
                        array(
                            'service_code' => $sql2["CODE"],
                            'service_name' => $sql2["NAME"],
                            'service_count' => $vals[$i]["attributes"]["COUNT"],
                            'service_unit' => $sql2['UNIT'],
                            'service_price' => $sql4["PRICE"]
                        );
                    }
                    // p_($service_price);
                }
                
            }
        }

    }
    // p_($_SESSION);
    foreach ($simple_price as $k=>$m)
    {
        // p_($simple_price[$k]);
        
        if ((is_numeric($_SESSION['koef_price'][$m['Code']]))&&($_SESSION['koef_price'][$m['Code']]>0))
        {
            if(rs_general('price_down_by_koef')==1) $simple_price[$k]['price']=round($simple_price[$k]['price']/$_SESSION['koef_price'][$m['Code']],2);
        }
        else $simple_price[$k]['price']=round($simple_price[$k]['price'],2);
        // p_($simple_price[$k]);

    }
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate simple'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
    unset($sql4);
    foreach ($s1 as $code=>$area)
    {
        $sql="select * from MATERIAL where CODE=".$code;
        $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        // p_($s_size);
        if ($sql2["L"]==0)$sql2["L"]=1000;
        if ($sql2["W"]==0)$sql2["W"]=1000;
        // p_($sql2);
        // p_($m_get);
        if (!isset($m_get[$code]))
        {
            $sql="SELECT * FROM MATERIAL_PRICE WHERE MATERIAL_ID=".$sql2["MATERIAL_ID"]." AND PLACE_ID=".$place.";";
            $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $sql4=check_price($sql4,"MATERIAL",$link,$sql2["MATERIAL_ID"]);
        }
        else  $sql4["PRICE"]=$m_get[$code]['PRICE'];
        // p_($sql4);

        // p_($sql2);
         $name= "[/".$sql2["CODE"] ."/] ".$sql2["NAME"];
         
        $s["p"][$code] = array(
            "price" => $sql4["PRICE"],
            "price_m2" => round($sql4["PRICE"] / ($sql2["L"] / 1000) / ($sql2["W"] / 1000), 2),
            "area" => $sql2["L"] / 1000 * $sql2["W"] / 1000,
            'per' => $s_size_sheet[$code]['L'] / $sql2["L"] / 1000 * $sql2["W"] / 1000
        );
        $s["n"][$code] = $name;
        foreach ($parts_s[$code] as $id=>$parea)
        {
            $part[$id]["mat_area"]=round($parea/$code_part_area[$code]*$s1[$code],2);
            $part[$id]["price_mat"]=round($s["p"][$code]["price_m2"]*$part[$id]["mat_area"],2);
            $part[$id]["mat_lose"]=round((1-$code_part_area[$code]/$s[$code])*100,2);
            $part[$id]["my_double"]=$parts_double[$id];
            $part[$id]["my_double_res"]=0;
            $part[$id]["count"]=$parts_count[$id];
            $part[$id]["code"]=$code;
            $vals[$part_con[$id]]['attributes']['MY_MAT_AREA_ST']=$part[$id]["mat_area"];
            $vals[$part_con[$id]]['attributes']['MY_MAT_AREA_LOSE']=$part[$id]["mat_lose"];

        }
    }
    // p_($s);exit;

    foreach ($parts_s_d as $name=>$p_area1)
    {
        foreach ($p_area1 as $id=>$pa)
        {
            $part[$id]["price_mat"]=0;
            $part[$id]["mat_area"]=round($parts_size_d[$id]["W"]*$parts_size_d[$id]["L"]/1000000,2);
            $part[$id]["mat_lose"]=round(((($parts_size_d[$id]["W"]-30)*($parts_size_d[$id]["L"]))/1000000)/$part[$id]["mat_area"],2);
            $part[$id]["my_double"]=$parts_double[$id];
            $part[$id]["my_double_res"]=1;
            $part[$id]["count"]=$parts_count[$id];
            $part[$id]["code"]="-";
            $vals[$part_con[$id]]['attributes']['MY_MAT_AREA_ST']=$part[$id]["mat_area"];
            $vals[$part_con[$id]]['attributes']['MY_MAT_AREA_LOSE']=$part[$id]["mat_lose"];
        }
    }
    unset($rtrtrr);

    foreach ($s as $k=>$v)
    {
        if (($k<>"p")&&($k<>"n"))
        {
            
            foreach ($s_size_sheet[$k] as $k1=>$kkk1)
            {
                if ($kkk1['mat_kl']==1)
                {
                    if (substr_count($s["n"][$k],'МАТЕРИАЛ КЛИЕНТА +')==0)$s["n"][$k]='МАТЕРИАЛ КЛИЕНТА + '.$s["n"][$k];
                }
                if ($p1["p"][$k][0]<$arr_st[$k]) $t34="<br><span style=\"color: green;\">(в наличии ".$arr_st[$k].")</span>";
                else
                {
                    $t34="<br><span style=\"color: red;\">(в наличии ".$arr_st[$k].")</span>";
                }
                if (!isset($kkk1['glass']))
                {
                    
                    $material_price[$k.$k1] = 
                    array(
                        'material_code' => $k,
                        'material_name' => $s["n"][$k]." (".$kkk1['L']."*".$kkk1['W'].")".$t34,
                        'sheet_count' => round($kkk1['COUNT']*$kkk1['L']*$kkk1['W']/$s["p"][$k]["area"],2),
                        'half_sheet_count' => 0,
                        'sheet_price' => round($s["p"][$k]["price"],2),
                        'id'=>$kkk1['ID']
                
                    );
                }
                elseif ($kkk1['glass']==1)
                {
                    $t34="<br><span style=\"color: red;\">(в наличии ".$arr_st[$k].")</span>";
                    $material_price[$k.$k1] = 
                    array(
                        'material_code' => $k,
                        'material_name' => $s["n"][$k]." (1*1)".$t34,
                        'sheet_count' => round($kkk1['COUNT']*$kkk1['L']*$kkk1['W'],2),
                        'half_sheet_count' => 0,
                        'sheet_price' => round($s["p"][$k]["price"],2),
                        'id'=>$kkk1['ID']
                
                    );
                }
            }
        }
    }
    // p_($material_price);
    // p_($_SESSION['koef_price']);
    // exit;
    foreach ($material_price as $k=>$m)
    {
        if ((is_numeric($_SESSION['koef_price'][$m['material_code']]))&&($_SESSION['koef_price'][$m['material_code']]>0))
        {
            if(rs_general('price_down_by_koef')==1) $material_price[$k]['sheet_price']=$material_price[$k]['sheet_price']/$_SESSION['koef_price'][$m['material_code']];
        }
    }
    // p_($_SESSION);
    // exit;
    // p_($s_size_sheet);  
    // p_($material_price);  
    // exit;
    unset( $code_part_area);
    foreach ($p1 as $code=>$area)
    {
        $sql="select * from BAND where CODE=".$code;
        $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

        if ((!isset($m_get[$code]))&&($sql2["BAND_ID"]>0)&&(is_numeric($sql2["BAND_ID"])))
        {
            $sql="SELECT * FROM BAND_PRICE WHERE BAND_ID=".$sql2["BAND_ID"]." AND PLACE_ID=".$place.";";
            $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $sql4=check_price($sql4,"BAND",$link,$sql2["BAND_ID"]);
        }
        else  $sql4["PRICE"]=$m_get[$code]['PRICE'];
        $p1["p"][$code]=array($p1[$code],$sql4["PRICE"]);
        $p1["n"][$code]= $sql2['NAME'];
        foreach ($part as $id=>$v) 
        {
            foreach($part[$id]["band"] as $c=>$v1)
            {
                if ($c==$code) $part[$id]["price_band"]+=round($sql4["PRICE"]*$v1,2);
            }
        }
    }
    foreach ($part as $id=>$v) 
    {
        $part[$id]["kromkovanie_price"]=0;
        if (isset($part[$id]["band"]))
        {
            foreach($part[$id]["band"] as $k=>$v)
            {
                $part[$id]["price_band_name"].=$k." - ".$v." м.пог.  ";
            }
            $part[$id]["price_band_name"].="**  ";
            if (isset($part[$id]["left_band"]))  $part[$id]["price_band_name"].="L-".$part[$id]["left_band"]." ";
            if (isset($part[$id]["right_band"]))  $part[$id]["price_band_name"].="R-".$part[$id]["right_band"]." ";
            if (isset($part[$id]["top_band"]))  $part[$id]["price_band_name"].="T-".$part[$id]["top_band"]." ";
            if (isset($part[$id]["botton_band"]))  $part[$id]["price_band_name"].="B-".$part[$id]["botton_band"]." ";
            $part[$id]["price_band_name"].="|";
        }
        if (isset($part[$id]["kromkovanie"]))
        {
            foreach($part[$id]["kromkovanie"] as $k=>$v)
            {
                $part[$id]["kromkovanie_name"].=$k." - ".$v." м.пог.  ";
                $part[$id]["kromkovanie_price"]+=$service_pr[$k]*$v;
            }
            $part[$id]["kromkovanie_name"].="|";
        }
        if($part[$id]['KLL']>0) $kl_text[$id]=" L ".$part[$id]['KLL'].' ';
        if($part[$id]['KLR']>0) $kl_text[$id]=" R ".$part[$id]['KLR'].' ';
        if($part[$id]['KLT']>0) $kl_text[$id]=" T ".$part[$id]['KLT'].' ';
        if($part[$id]['KLB']>0) $kl_text[$id]=" B ".$part[$id]['KLB'].' ';
        if (isset($kl_text[$id]))
        {
            $part[$id]["kromkovanie_name"].=" криволинейное кромкование ".$kl_text[$id]." | ";
        }
    }
    foreach ($p1 as $k=>$v)
    {
        if (($k<>"p")&&($k<>"n"))
        {
            if ($p1["p"][$k][0]<$arr_st[$k]) $t34="<br><span style=\"color: green;\">(в наличии ".$arr_st[$k].")</span>";
            else $t34="<br><span style=\"color: red;\">(в наличии ".$arr_st[$k].")</span>";
            $band_price[$k] = 
                array(
                    'band_code' => $k,
                    'band_name' => $p1["n"][$k].$t34,
                    'band_count' => $p1["p"][$k][0],
                    'band_unit' => "м.пог.",
                    'band_price' => round($p1["p"][$k][1],2)
            
                );
        }
    }
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate material_end'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);

    
    foreach ($part as $id=>$v)
    {
        if ($part[$id]["my_double_res"]==1)
        {
            if($part[$id]["mat"]<>'')
            {
                $des="(".$parts_size_d[$id]["DW"]."*".$parts_size_d[$id]["DL"]."*".$part[$id]["T"].") | ".$part[$id]["mat"];
            }
            else $des="(".$parts_size_d[$id]["DW"]."*".$parts_size_d[$id]["DL"]."*".$part[$id]["T"].") | БН ";
        }
        else
        {
            if($part[$id]["mat"]<>'')
            {
                $des.="(".round($parts_size[$id]["DW"],2)."*".round($parts_size[$id]["DL"],2)."*".$part[$id]["T"].") | ".$part[$id]["mat"];
            }
            else $des.="(".round($parts_size[$id]["DW"],2)."*".round($parts_size[$id]["DL"],2)."*".$part[$id]["T"].") | БН ";
        }
        // p_($des);exit;
        $ssid=array('l','r','b','t');
        $ssid_t=array('слева','справа','снизу','сверху');
        unset ($rr1);
        foreach ($ssid as $cv=>$ssid1)
        {
            if (isset($part[$id]['SIDE_TO_CUT_'.mb_strtoupper($ssid1)]))
            {
                $des.=" УРЕЗКА ".$ssid_t[$cv]." до ".$part[$id]['SIDE_TO_CUT_'.mb_strtoupper($ssid1)]."мм | ";
                if (($ssid1=="l")OR($ssid1=="r"))
                {
                    $rr1+=round($parts_size[$id]["W"],2)/1000*$part[$id]['count'];
                }
                else  $rr1+=round($parts_size[$id]["L"],2)/1000*$part[$id]['count'];
            }
        }
        if ($rr1>0)
        {
            foreach ($part[$id]['service_porezka'] as $klt=>$klv)
            {
                $part[$id]['service_porezka'][$klt]+=$rr1;
                $part[$id]['service_porezka_price'][$klt]+=$rr1*$service[$klt];
                $service[$klt]+=$rr1;
            }
        }

        $des.="| порезка ".key($part[$id]["service_porezka"])." - ".$part[$id]["service_porezka"][key($part[$id]["service_porezka"])]." ";
        if (isset($v["price_band_name"])) $des.=" | кромка ".$v["price_band_name"];
        if (isset($v["service_styagka"])) $des.=" | стяжка ".key($v["service_styagka"])." - ".$v["service_styagka"][key($v["service_styagka"])]." | ";
        if (isset($v["service_por_kromki"])) 
        {
            $des.=" порезка кромки 23352 - ".$v["service_por_kromki"]." ";
        }
        if (isset($v['kromkovanie'])) 
        {
            $des.=" кромкование - ".$v["kromkovanie_name"]." | ";
        }
        if (isset($v["service_zarez"])) 
        {
            $des.="| срез торца 23351 - ";
            foreach($v["service_zarez"] as $idz=>$vz)
            {
                $des.=" ".$idz." - (".$v["service_zarez_angle"][$idz].") ";
                $zar_pr+=$v["service_zarez"][$idz];
                $zar_count+=$vz;
            }
            $des.=" (". $zar_count.") | ";
        }
        if($part[$id]['GR_GR']>0)
        {
            $des.="| паз - 16366 - (".round($part[$id]['GR_GR'],2).") ";
        }
        if($part[$id]['GR_GR_ADV']>0)
        {
            $des.="| сложный паз, четверть - 23357 - (".round($part[$id]['GR_GR_ADV'],2).") ";
        }
        if($part[$id]['OTV_do_20']>0)
        {
            $des.="| отверстия диаметром до 20 мм - 26321 - (".round($part[$id]['OTV_do_20'],2).") ";
        }
        if($part[$id]['prisadka_petlya']>0)
        {
            $des.="| присадка под петлю - 20323 - (".round($part[$id]['prisadka_petlya'],2).") ";
        }
        if($part[$id]['prisadka_150']>0)
        {
            $des.="| присадка глухая от 20 до 150 мм - 110429 - (".round($part[$id]['prisadka_150'],2).") ";
        }
        if($part[$id]['prisadka_150s']>0)
        {
            $des.="| присадка сквозная от 20 до 150 мм - 23360 - (".round($part[$id]['prisadka_150s'],2).") ";
        }
        if($part[$id]['prisdka_pod_ruchku']>0)
        {
            $des.="| присадка под ручку - 22160 - (".round($part[$id]['prisdka_pod_ruchku'],2).") ";
        }


        
        if($part[$id]['fr_450']>0)
        {
            $des.="| фрезерование до 450 мм - 85894 - (".round($part[$id]['fr_450'],2).") ";
        }
        if($part[$id]['fr']>0)
        {
            $des.="| фрезерование  - 85895 - (".round($part[$id]['fr'],2).") ";
        }
        if (isset($part[$id]['serv_adv']))
        {
            $des.=$part[$id]['serv_adv'];
        }

    //     p_($part);
    // p_($p1);
    // p_($service);
    // p_($des);
    // exit;
        unset($zar_pr,$zar_count);
        $v["count"]=$v["count"]/1;
        $v["count"]=(int)$v["count"];
        if ($v['count']>0)
        {
            $part_price[$id]=
            array(
                'part_name' => $id,
                'material_code' => $v["code"],
                'description' => str_replace("||"," | ", str_replace("| |"," | ", $des)),
                'count' => $v["count"],
                'price' =>round(($v['price_mat']+$v['price_band']+$v["service_porezka_price"][key($v["service_porezka_price"])]
                +$v["service_styagka_price"][key($v["service_styagka_price"])]
                +$v["service_por_kromki_price"]
                +$v["service_kromkovanie_price"]
                +$zar_pr
                +$v["kromkovanie_price"]
                )/$v["count"]
                +$part[$id]['GR_GR_price']
                +$part[$id]['GR_GR_ADV_price']
                +$part[$id]['OTV_do_20_price']
                +$part[$id]['prisadka_petlya_price']
                +$part[$id]['prisadka_150_price']
                +$part[$id]['prisadka_150s_price']
                +$part[$id]['prisdka_pod_ruchku_price']
                +$part[$id]['fr_450_price']
                +$part[$id]['fr_price']
                +$part[$id]['serv_adv_price']
                ,2)
            );
            $part[$id]['description']= str_replace("||"," | ", str_replace("| |"," | ", $des));
            $part[$id]['description']=str_replace('=',' ',$part[$id]['description']);
            $part[$id]['description']=str_replace('%',' ',$part[$id]['description']);
            $part[$id]['description']=str_replace('*',' ',$part[$id]['description']);
            $vals[$part_con[$id]]['attributes']['DESCRIPTION']=$part[$id]['description'];

            
        }
        unset ($des);
    }
            // p_(strlen($part[27747]['description']));

//    p_($part);exit;
    
    foreach ($service as $id=>$s3)
    {
        if (!isset($service_pr[$id]))$service_pr[$id]=0.01;
        $service_price[] = 
                array(
                    'service_code' => $id,
                    'service_name' => $service_name[$id],
                    'service_count' => round($s3,2),
                    'service_unit' => $service_unit[$id],
                    'service_price' => $service_pr[$id]
                );
    }
    // p_($service_price);
    // p_($service_pr);
    
    // exit;
    unset($price_tot);
    foreach($service_price as $k=>$v)
    {
        $price_tot+=$v['service_price']*$v['service_count'];
        if ($v['service_count']==0) unset ($service_price[$k]);
    }
    foreach($material_price as $k=>$v)
    {
        $price_tot+=$v['sheet_price']*$v['sheet_count'];
        if ($v['sheet_count']==0) unset ($material_price[$k]);
    }
    foreach($band_price as $k=>$v)
    {
        $price_tot+=$v['band_price']*$v['band_count'];
        if ($v['band_count']==0) unset ($band_price[$k]);
    }
    unset($price_tot_part);
    foreach($part_price as $k=>$v)
    {
        $price_tot_part+=$v['price']*$v['count'];
    }
    foreach($part_price as $k=>$v)
    {
        $part_price[$k]['price']= round($part_price[$k]['price']/$price_tot_part*$price_tot,2);
    }
    // p_($vals);
    // p_($vals_z);
    // exit;
    $index=make_index($vals);
    if (isset($vals_back_sh)) $vals_z=$vals_back_sh;
    foreach ($vals_z as $k=>$v)
    {
        if ((isset($v['attributes']['PROGRAM'])))
        {
            foreach ($index['OPERATION'] as $i)
            {
                if (($vals[$i]['attributes']['TYPEID']=="XNC")&&($vals[$i]['attributes']['ID']==$v['attributes']['ID'])) $vals[$i]['attributes']['PROGRAM']=$v['attributes']['PROGRAM'];
            }
            
        }
    }
    ksort($service);
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate part_end'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);



    foreach($service as $k=>$s)
    {
        $sql="SELECT * FROM SERVICE WHERE CODE=".$k;

        if (is_numeric($k)) $serv=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        if (isset($serv))
        {
            $sql="SELECT * FROM TERMS WHERE SERVICE_TYPE=".$serv['SERVICE_TYPE']." AND PLACE_ID=".$place." ORDER BY DATE DESC LIMIT 1";
            $serv=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $time = strtotime($serv['READY']);
            // echo $time;exit;
            $s_time=date("d/m/y", $time);
            // echo date("d/m/y", mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            if ((isset($s_time))&&($time>date('U')))
            {
                $serv_time[$k]=$s_time;
                $serv_time2[$s_time]=$time;
                // p_($serv_time);
            }
            //.. 06.08.2021 Добавлено условие для корректировки даты (текущая + 3 дня).
            // В таблице TERMS даты не меняются, и я не понимаю логики отталкиваться от этих дат,
            // может просто этот момент недоработан и в будущем планировалось доработать...
            elseif ($time < date('U')) {
                $serv_time[$k] = date("d/m/y", mktime(0, 0, 0, date('m'), date('d') +3, date('Y')));
                $serv_time2[$serv_time[$k]] = date("U", mktime(0, 0, 0, date('m'), date('d') +3, date('Y')));
            }
        }
        else $serv_time[$k]= date("d/m/y", mktime(0, 0, 0, date('m'), date('d') +3, date('Y')));
    }
    unset ($serv_order_time);
    foreach ($serv_time2 as $k=>$s)
    {
        if ($serv_order_time<$s) $serv_order_time=$s;
        // p_($s2);
    }
    $serv_order_time=date("d/m/y", $serv_order_time);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'calculate terms_end'.$_SESSION['order_akcent_ids'].' '.$vals[0]['attributes']['project.my_order_name'], $_SESSION);
//  p_($service_price);

// p_($part);
// p_($simple_price);exit;

// p_($serv_order_time);
    // exit;
    // exit;
    $res['mat_kl']=$mat_kl;
    // p_($mat_kl);exit;
    $res['serv_time']=$serv_time;
    $res['serv_order_time']=$serv_order_time;
    ksort($service_price);
    ksort($part_price);
    ksort($material_price);
    ksort($band_price);
    ksort($simple_price);
    $res['part']=$part;
    $res['service_price']=$service_price;
    $res['simple_price']=$simple_price;
    $res['part_price']=$part_price;
    $res['material_price']=$material_price;
    $res['band_price']=$band_price;
    $res['vals']=$vals;
    // p_($res);exit;
    return $res;
}

?>