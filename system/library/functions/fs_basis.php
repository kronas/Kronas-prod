<?php

/**
 * Функция удаляет из массива блоки с типом "cdata" ([type] => cdata)
 **/
function delCdata($vals) {
	$clearVals = [];
	foreach ($vals as $key => $value) {
		if ($value['type'] !== 'cdata') {
			$clearVals[] = $value;
		}
	}
	return $clearVals;
}

/**
* Функция из массива $vals (все данные проекта) создаёт массив операций с индексами массива $vals.
*
* Формат на выходе:
* $index[PART][1] = 34 (34 ключ массива $vals, $vals[34]), $index[PART][N] = N,
* $index[OPERATION][1] = 47, $index[OPERATION][2] = 48, $index[OPERATION][N] = N,
* $index[GOOD][1] = 2, $index[GOOD][2] = 7, $index[GOOD][N] = N,
* $index[PROJECT][1] = 0, $index[PROJECT][2] = 277, $index[PROJECT][N] = N,
*
* Параметры: массив $vals
**/
function getIndex($vals) {
	$temp = [];
    foreach ($vals as $k => $v) $temp[] = $v['tag'];

    $in_key = array_unique($temp);

    foreach ($in_key as $v) {
        foreach ($vals as $k1 => $v1) {
            if($v == $v1['tag']) {
               $index[$v][] = $k1;
            }
        }
    }

    return $index;
}

/**
 * Функция создаёт деталь сшивки и две исходных детали
 **/
function getStDetPart($from, $toSource, $toStitching, $elNames, $sheetNames, $largestDetailId) {
	// На сшивку добавляется по 30 на обе оси
	$additionalSize = 30;
	$toFace = $toSource;
    $toback = $toSource;
	$tmpSrsFace = $toSource['attributes'];
	$tmpSrsBack = $toSource['attributes'];
	$tmpSt = $toStitching['attributes'];
	$from = $from['attributes'];
	$out = [ 'face' => [], 'back' => [], 'stitching' => [] ];

	// Сначала создаём парные детали (их идентификаторы будут использоваться в детали-сшивке)
	// Лицевая
	// <part id="1" name="№ Горизонтальная" dl="1204.00" dw="504.00" count="1" txt="true"/>
	$tmpSrsFace['l'] = ($from['dl'] + $additionalSize);
	$tmpSrsFace['w'] = ($from['dw'] + $additionalSize);
	$tmpSrsFace['dl'] = ($from['dl'] + $additionalSize);
	$tmpSrsFace['dw'] = ($from['dw'] + $additionalSize);
	$tmpSrsFace['cl'] = ($from['dl'] + $additionalSize);
	$tmpSrsFace['cw'] = ($from['dw'] + $additionalSize);
	$tmpSrsFace['jl'] = ($from['dl'] + $additionalSize);
	$tmpSrsFace['jw'] = ($from['dw'] + $additionalSize);
	$tmpSrsFace['count'] = $from['count'];
	$tmpSrsFace['txt'] = $from['txt'];
	$tmpSrsFace['id'] = $largestDetailId;
	$tmpSrsFace['MY_DOUBLE_ID'] = $largestDetailId;
	$tmpSrsFace['MY_DOUBLE_FACE'] = 1;
	$tmpSrsFace['MY_DOUBLE_PARENT'] = $from['id'];
	$tmpSrsFace['name'] = 'Ст.Л id=[/' . $from['id'] . '/] * 1';
	$tmpSrsFace['part.name1'] = 'Ст.Л id=[/' . $from['id'] . '/] * 1';

	// Изнаночная
	$tmpSrsBack['l'] = ($from['dl'] + $additionalSize);
	$tmpSrsBack['w'] = ($from['dw'] + $additionalSize);
	$tmpSrsBack['dl'] = ($from['dl'] + $additionalSize);
	$tmpSrsBack['dw'] = ($from['dw'] + $additionalSize);
	$tmpSrsBack['cl'] = ($from['dl'] + $additionalSize);
	$tmpSrsBack['cw'] = ($from['dw'] + $additionalSize);
	$tmpSrsBack['jl'] = ($from['dl'] + $additionalSize);
	$tmpSrsBack['jw'] = ($from['dw'] + $additionalSize);
	$tmpSrsBack['count'] = $from['count'];
	$tmpSrsBack['txt'] = $from['txt'];
	$tmpSrsBack['id'] = ($largestDetailId + 1);
	$tmpSrsBack['MY_DOUBLE_ID'] = $largestDetailId;
	$tmpSrsBack['MY_DOUBLE_PARENT'] = $largestDetailId;
	$tmpSrsBack['name'] = 'Ст.Н id=[/' . $from['id'] . '/]. 1/1';
	$tmpSrsBack['part.name1'] = 'Ст.Н id=[/' . $from['id'] . '/]. 1/1';


	// Создаём деталь-сшивку
	$tmpSt['id'] = $from['id'];
    $tmpSt['name'] = $from['name'];
	$tmpSt['_old_name'] = $from['name'];
    $tmpSt['l'] = $from['dl'];
	$tmpSt['w'] = $from['dw'];
	$tmpSt['dl'] = $from['dl'];
	$tmpSt['dw'] = $from['dw'];
	$tmpSt['jl'] = $from['dl'];
	$tmpSt['jw'] = $from['dw'];
	$tmpSt['cl'] = $from['dl'];
	$tmpSt['cw'] = $from['dw'];
	$tmpSt['_old_l'] = $from['dl'];
	$tmpSt['_old_w'] = $from['dw'];
	$tmpSt['_old_dl'] = $from['dl'];
	$tmpSt['_old_dw'] = $from['dw'];
	$tmpSt['_old_jl'] = $from['dl'];
	$tmpSt['_old_jw'] = $from['dw'];
	$tmpSt['_old_cl'] = $from['dl'];
	$tmpSt['_old_cw'] = $from['dw'];
    $tmpSt['count'] = $from['count'];
    $tmpSt['_old_count'] = $from['count'];
    $tmpSt['txt'] = $from['txt'];
	$tmpSt['_old_txt'] = $from['txt'];
	$tmpSt['part.my_double_par_first'] = $sheetNames[$from['id']]['name'];
	$tmpSt['part.my_double_par_second'] = $sheetNames[$from['id']]['name'];
	$tmpSt['MY_DOUBLE_ID'] = $largestDetailId;
	$tmpSt['_old_id'] = $largestDetailId;
	$tmpSt['part.my_double_par_part_id'] = $largestDetailId;
    if (isset($from['elt'])) {
    	$tmpSt['elt'] = $from['elt'];
    	$tmpSt['_old_elt'] = $from['elt'];
    	$opId = preg_replace('/^(@operation#)(\d+)$/', '${2}', $from['elt']);
    	$tmpSt['eltmat'] = $elNames[$opId]['name'];
    	$tmpSt['part.my_double_par_band_id_t'] = $elNames[$opId]['code'];
    } else unset($tmpSt['elt'], $tmpSt['eltmat'], $tmpSt['part.my_double_par_band_id_t']);

    if (isset($from['elr'])) {
    	$tmpSt['elr'] = $from['elr'];
    	$tmpSt['_old_elr'] = $from['elr'];
    	$opId = preg_replace('/^(@operation#)(\d+)$/', '${2}', $from['elr']);
    	$tmpSt['elrmat'] = $elNames[$opId]['name'];
    	$tmpSt['part.my_double_par_band_id_r'] = $elNames[$opId]['code'];
    } else unset($tmpSt['elr'], $tmpSt['elrmat'], $tmpSt['part.my_double_par_band_id_r']);

    if (isset($from['elb'])) {
    	$tmpSt['elb'] = $from['elb'];
    	$tmpSt['_old_elb'] = $from['elb'];
    	$opId = preg_replace('/^(@operation#)(\d+)$/', '${2}', $from['elb']);
    	$tmpSt['elbmat'] = $elNames[$opId]['name'];
    	$tmpSt['part.my_double_par_band_id_b'] = $elNames[$opId]['code'];
    } else unset($tmpSt['elb'], $tmpSt['elbmat'], $tmpSt['part.my_double_par_band_id_b']);

    if (isset($from['ell'])) {
    	$tmpSt['ell'] = $from['ell'];
    	$tmpSt['_old_ell'] = $from['ell'];
    	$opId = preg_replace('/^(@operation#)(\d+)$/', '${2}', $from['ell']);
    	$tmpSt['ellmat'] = $elNames[$opId]['name'];
    	$tmpSt['part.my_double_par_band_id_l'] = $elNames[$opId]['code'];
    } else unset($tmpSt['ell'], $tmpSt['ellmat'], $tmpSt['part.my_double_par_band_id_l']);
    $tmpSt['part.name1'] = 'Сшивка';

    $toFace['attributes'] = $tmpSrsFace;
    $toback['attributes'] = $tmpSrsBack;
    $toStitching['attributes'] = $tmpSt;
    $out['face'] = $toFace;
    $out['back'] = $toback;
    $out['stitching'] = $toStitching;

    return $out;
}

/**
 * Функция создаёт материалы сшивки
 **/
function getStSheet($params, $sheetTemplate, $partTemplate) {
	// params: material, stithingId, partId, goodId, l, w,
	$matThickness = 0;
	$matDoubleThickness = 0;

	// Сразу получим толщину материала
	// $q = "SELECT `T` FROM `MATERIAL` WHERE `CODE` = " . $params['material'];
	// $q = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);
	// if ($q['res'] === 1 && isset($q['data'][0])) {

	$q = DB::table('MATERIAL')->where(['CODE' => $params['material']])->first();
	if (!empty($q)) {
		$q = (array)$q;
		$matThickness = (int)$q['T'];
		$matDoubleThickness = (int)($q['T'] * 2);
	}
	$name = "СШ № [/" . $params['stithingId'] . "/] (" . $params['material'] . ")  / (" . $params['material'] . ")";

	// Заполняем материал
	$sheetTemplate['attributes']['id'] = $params['goodId'];
	$sheetTemplate['attributes']['l'] = $params['l'];
	$sheetTemplate['attributes']['w'] = $params['w'];
	$sheetTemplate['attributes']['t'] = $matDoubleThickness;
	$sheetTemplate['attributes']['t1'] = $matThickness;
	$sheetTemplate['attributes']['t2'] = $matThickness;
	$sheetTemplate['attributes']['name'] = $name;
	$sheetTemplate['attributes']['description'] = $name;
	$sheetTemplate['attributes']['part.name1'] = $name;
	$sheetTemplate['attributes']['MY_FIRST'] = $params['material'];
	$sheetTemplate['attributes']['MY_SECOND'] = $params['material'];
	$sheetTemplate['attributes']['MY_DOUBLE_ID'] = $params['stithingId'];

	// Заполняем внутренний PART
	$partTemplate['attributes']['id'] = $params['partId'];
	$partTemplate['attributes']['l'] = $params['l'];
	$partTemplate['attributes']['w'] = $params['w'];
	$partTemplate['attributes']['MY_DOUBLE_ID'] = $params['stithingId'];

	return ['sheet' => $sheetTemplate, 'innerPart' => $partTemplate];
}

/**
 * Функция создаёт операции сшивки
 **/
function getStCsOperation($params, $opCsTemplate, $opPartTemplate, $opMatTemplate) {
	$partDet = $opPartTemplate;
    $partSheet = $opPartTemplate;

    $opCsTemplate['attributes']['id'] = $params['opId'];
    $opCsTemplate['attributes']['w'] = $params['w'];
    $opCsTemplate['attributes']['l'] = $params['l'];
    $opCsTemplate['attributes']['t'] = $params['t'];
    $opCsTemplate['attributes']['tool1'] = $params['tool1'];
    $opCsTemplate['attributes']['MY_DOUBLE_ID'] = $params['MY_DOUBLE_ID'];

    $partDet['attributes']['id'] = $params['partDetailId'];
    $partSheet['attributes']['id'] = $params['partSheetId'];
    $opMatTemplate['attributes']['id'] = $params['materialId'];

	return ['operation' => $opCsTemplate, 'partDet' => $partDet, 'partSheet' => $partSheet, 'material' => $opMatTemplate,];
}

/**
 * Функция удаляет сшивки (полностью, со всеми деталями и операциями)
 **/
function delBasisStiching($vals, $delIds) {
	$prodParts = [];
	$delParts = [];
	$delPartsOnly = [];
	$csOps = [];
	// У операций раскроя по два PART-а, один - деталь изделия, второй - деталь материала (сшивки)
	// ключами массива $csOpsParts будут детали изделий, а значениями детали сшивок
	$csOpsParts = [];
	$xncOpsParts = [];
	$tools = [];
	$sheets = [];
	$index = make_index($vals);

	// Определяем идентификаторы деталей, которые нужно удалить
	foreach ($delIds as $delId) {
		foreach ($index['GOOD'] as $value) {
			if ($vals[$value]['attributes']['TYPEID'] === 'product') {
				$tmpPartKey = ($value + 1);
				while ($vals[$tmpPartKey]['tag'] === 'PART') {
					$prodParts[$vals[$tmpPartKey]['attributes']['ID']] = $tmpPartKey;
					if ($vals[$tmpPartKey]['attributes']['ID'] === $delId) {
						$tmpPartId = $vals[$tmpPartKey]['attributes']['ID'];
						$delParts[$tmpPartId]['id'] = $delId;
						$delParts[$tmpPartId]['id_valsKey'] = $tmpPartKey;
						$delParts[$tmpPartId]['face'] = $vals[$tmpPartKey]['attributes']['MY_DOUBLE_ID'];
					}
					$tmpPartKey++;
				}
			}
		}
	}
	foreach ($delIds as $delId) {
		foreach ($prodParts as $pKey => $pVal) {
			$tmpId = $vals[$pVal]['attributes']['ID'];
			$tmpDId = $vals[$pVal]['attributes']['MY_DOUBLE_ID'];

			if ($delParts[$delId]['face'] === $tmpDId && $delParts[$delId]['face'] === $tmpId) {
				$delParts[$delId]['face_valsKey'] = $pVal;
			}
			if ($delParts[$delId]['face'] === $tmpDId && $delParts[$delId]['face'] !== $tmpId) {
				$delParts[$delId]['back'] = $tmpId;
				$delParts[$delId]['back_valsKey'] = $pVal;
			}
		}
	}
	foreach ($delParts as $value) {
		$delPartsOnly[] = $value['id'];
		$delPartsOnly[] = $value['face'];
		$delPartsOnly[] = $value['back'];
	}

	// Собираем инструменты раскроя (ключ - ID, значение - порядковый номер массива $vals)
	foreach ($index['GOOD'] as $key => $value) {
		if ($vals[$value]['attributes']['TYPEID'] === 'tool.cutting') {
			$tools[$vals[$value]['attributes']['ID']] = $value;
		} elseif ($vals[$value]['attributes']['TYPEID'] === 'sheet') {
			$sheets[$vals[$value]['attributes']['ID']] = $value;
		}
	}

	// Собираем в блоки операции раскроя, материалы и инструменты раскроя
	// Также отдельно XNC-операции
	foreach ($index['OPERATION'] as $iKey => $vKey) {
		if ($vals[$vKey]['attributes']['TYPEID'] === 'CS') {
			$childKey = ($vKey + 1);
			$tmpId = $vals[$vKey]['attributes']['ID'];
			$tmpTool = $vals[$vKey]['attributes']['TOOL1'];
			$csOps[$tmpId]['id'] = $tmpId;
			$csOps[$tmpId]['id_valsKey'] = $vKey;
			$csOps[$tmpId]['tool'] = $tmpTool;
			while ($vals[$childKey]['tag'] === 'MATERIAL' || $vals[$childKey]['tag'] === 'PART') {
				if ($vals[$childKey]['tag'] === 'MATERIAL') $csOps[$tmpId]['material'] = $vals[$childKey]['attributes']['ID'];
				else {
					if (!isset($csOps[$tmpId]['part'])) $csOps[$tmpId]['part'] = $vals[$childKey]['attributes']['ID'];
					else $csOps[$tmpId]['stiching'] = $vals[$childKey]['attributes']['ID'];
				}
				$childKey++;
			}
			$csOpsParts[$csOps[$tmpId]['part']] = $csOps[$tmpId]['stiching'];

		} elseif ($vals[$vKey]['attributes']['TYPEID'] === 'XNC') {
			$tmpId = $vals[$vKey]['attributes']['ID'];
			$childKey = ($vKey + 1);
			if ($vals[$childKey]['tag'] === 'PART') {
				if (!isset($xncOpsParts[$vals[$childKey]['attributes']['ID']])) {
					$xncOpsParts[$vals[$childKey]['attributes']['ID']] = [];
				}
				array_push($xncOpsParts[$vals[$childKey]['attributes']['ID']], $vKey);
			}
		}
	}

	// Удаляем сшивки
	foreach ($delIds as $delId) {
		foreach ($csOps as $opId => $opArr) {
			if ($opArr['part'] === $delId) {
				$childKey = ($opArr['id_valsKey'] + 1);
				
				// Удаляем детали из изделия
				unset(
					$vals[$delParts[$delId]['id_valsKey']],
					$vals[$delParts[$delId]['face_valsKey']],
					$vals[$delParts[$delId]['back_valsKey']]
				);

				// Удаляем операцию раскроя
				unset($vals[$opArr['id_valsKey']]);
				while ($vals[$childKey]['tag'] === 'MATERIAL' || $vals[$childKey]['tag'] === 'PART') {
					unset($vals[$childKey]);
					$childKey++;
				}
				if ($vals[$childKey]['tag'] === 'OPERATION' && $vals[$childKey]['type'] === 'close') {
					unset($vals[$childKey]);
				}

				// Удаляем материал
				unset($vals[$sheets[$opArr['material']]]);
				$childKey = ($sheets[$opArr['material']] + 1);
				while ($vals[$childKey]['tag'] === 'PART') {
					unset($vals[$childKey]);
					$childKey++;
				}
				if ($vals[$childKey]['tag'] === 'GOOD' && $vals[$childKey]['type'] === 'close') {
					unset($vals[$childKey]);
				}

				// Удаляем инструмент
				unset($vals[$tools[$opArr['tool']]]);

				// Удаляем XNC-операции, (если есть для этой сшивки)
				if (isset($xncOpsParts[$delId])) {
					foreach ($xncOpsParts[$delId] as $key => $value) {
						unset($vals[$value]);

						$childKey = ($value + 1);
						while ($vals[$childKey]['tag'] === 'PART') {
							unset($vals[$childKey]);
							$childKey++;
						}
						if ($vals[$childKey]['tag'] === 'OPERATION' && $vals[$childKey]['type'] === 'close') {
							unset($vals[$childKey]);
						}
					}
				}
			}
		}
	}

	// И напоследок удалим детали (заготовки для сшивок) из раскроя, а также из кромкования
	foreach ($index['OPERATION'] as $vKey) {
		if ($vals[$vKey]['attributes']['TYPEID'] === 'CS' || $vals[$vKey]['attributes']['TYPEID'] === 'EL') {
			$childKey = ($vKey + 1);
			while ($vals[$childKey]['tag'] === 'MATERIAL' || $vals[$childKey]['tag'] === 'PART') {
				if ($vals[$childKey]['tag'] === 'PART') {
					if (in_array($vals[$childKey]['attributes']['ID'], $delPartsOnly)) {
						unset($vals[$childKey]);
					}
				}
				$childKey++;
			}
		}
	}

	return $vals;
}

/**
 * Функция принимает vals-массив проекта,
 * а возвращает XML-файл проекта
**/
function projectToXml($projectArray) {
	$text = '';
	$tab = '';
	foreach ($projectArray as $opKey => $opValue)
	{
		if ($opValue['level'] == 1) $tab = "";
		if ($opValue['level'] == 2) $tab = "\t";
		if ($opValue['level'] == 3) $tab = "\t\t";

		if (isset($opValue["attributes"])) {
		  	// Удаляем из массива атрибуты с большими буквами (если есть такой-же с маленькими)
			foreach ($opValue["attributes"] as $attrKey => $attrValue) {
				if (isset($opValue["attributes"][mb_strtolower($attrKey)]) && isset(($opValue["attributes"][mb_strtoupper($attrKey)]))) {
					unset ($opValue["attributes"][mb_strtoupper($attrKey)]);
				}   
			}
		}

		// Обработка элементов с двойными тегами
		if ($opValue["type"] == "open") {
			$text .= $tab . "<" . mb_strtolower($opValue["tag"]) . " ";
			foreach ($opValue["attributes"] as $attrKey => $attrValue) {
				if($opValue["tag"] <> "PROGRAM") $attrValue = htmlspecialchars($attrValue);

				if ($attrValue !== '' && $attrValue !== null) {
					$text .= $attrKey . "=\"" . $attrValue . "\" ";
				}
			}
			$text .= ">" . PHP_EOL;
		}

		// Обработка элементов в одинарными тегами
		elseif ($opValue["type"]=="complete") {
			$text .= $tab . "<" . mb_strtolower($opValue["tag"]) . " ";
			foreach ($opValue["attributes"] as $attrKey => $attrValue) {
				if($opValue["tag"] <> "PROGRAM") $attrValue = htmlspecialchars($attrValue);

				if ($attrValue !== '' && $attrValue !== null) {
					$text .= $attrKey . "=\"" . $attrValue . "\" ";
				}
			}
			$text .= "/>" . PHP_EOL;
		}
		// Обработка закрывающих элементов (двойных тегов)
		// Здесь только приведение к нижнему регистру
		elseif ($opValue["type"] == "close") $text .= $tab . "</" . mb_strtolower($opValue["tag"]) . ">" . PHP_EOL;
	}

	$text = trim(quot($text));

	return $text;
}

if (!function_exists('quot')) {
	function quot ($text)
	{
	    $text=str_replace("qquot","quot",$text);
	    $text=str_replace("ququot","quot",$text);
	    $text=str_replace("quoquot","quot",$text);
	    $text=str_replace("quotquot","quot",$text);
	    return $text;
	}
}

if (!function_exists('put_temp_file_data')) {
	/**
	* Функция записывает данные в сессию, сохранённую во временном файле
	* Параметры: $session_id - ID сессии(и название файла)  $data - данные;
	**/
	function put_temp_file_data($session_id, $data) {
	    $file = config('app.serv_main_dir').'kronasapp/storage/app/giblab_sessions/' . $session_id . '.txt';
	    
	    $file_data = unserialize(file_get_contents($file));
	    foreach ($data as $key => $value) {
	        $file_data[$key] = $value;
	    }
	    file_put_contents($file, serialize($file_data));
	}
}


if (!function_exists('dxml')) {
	/**
	 * Форматированный вывод программ и данных проектов (xml-данных)
	 * Если переменная $exit установлена, то функция останавливает выполнение кода.
	 * dxml - dump xml
	**/
	function dxml($xml, $exit = false){
	    if (!$exit) {
	        echo '<textarea rows="10" cols="45">' . str_replace("<", "\r\n<", $xml) . '</textarea>';
	    } else {
	        echo '<textarea rows="10" cols="45">' . str_replace("<", "\r\n<", $xml) . '</textarea>';exit();
	    }
	}

	/**
	 * Форматированный вывод
	 * da - dump all
	**/
	function da($val, $i = false, $allottedSpaceFlag = false){
	    echo '<br>';
	    if ($allottedSpaceFlag == true) {
	        echo '----------------------------------------------------------------------------------------<br>';
	    }
	    if(is_array($val) || is_object($val)){
	        echo "<pre>";
	        if($i == false) print_r($val);
	        else var_dump($val);
	        echo "</pre>";
	    }else{
	        if($i == false) echo $val;
	        else{
	            echo "<pre>";
	            var_dump($val);
	            echo "</pre>";
	        }
	    }
	    if ($allottedSpaceFlag == true) {
	        echo '<br>----------------------------------------------------------------------------------------<br>';
	    }
	}
}
?>