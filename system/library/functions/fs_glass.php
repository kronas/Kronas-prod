<?php

/**
 * 
**/
function put_band_glass($vals,$place,$f_size)
{
    $vals=vals_out($vals);

    $index=make_index($vals);
    $letter=array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    $arr=['shlif'=>'Шлифовка стекла','poli'=>'Полировка стекла'];
    $arr2=['no'=>'без обработки','shlif'=>'шлифовка стекла','poli'=>'полировка стекла'];
    foreach ($f_size as $f) $arr['facet'.$f[0].$f[1]]='Фацет шириною '.$f[0].' мм, обработка - '.$arr2[$f[1]];
    foreach ($arr as $key=>$band)
    {
        $ii++;
        $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
        inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
        where PLACE = ".$place." LIMIT 1";
        $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

        $tool=Array
            (
                "tag" => "GOOD",
                "type" => "complete",
                "level" => 2
            );
        foreach ($sql3 as $k3=>$v3)
        {
            if (!is_int($k3))
            {
                $tool["attributes"][$k3]=$v3;
            }
        }
        $tool["attributes"]['ELWIDTHPREJOINT']=0;
        $tool["attributes"]["ID"]=new_id($vals);
        $band1=Array
            (
                "tag" => "GOOD",
                "type" => "complete",
                "level" => 2,
                "attributes"=>['good.my_virtual'=>1,'good.my_glass_type'=>$key,'NAME'=>$band,'W'=>25,'T'=>0,'CODE'=>'V'.mt_rand(0,1000000),'COUNT'=>10000,'TYPEID'=>'band']
            );
        
        $band1["attributes"]["ID"]=new_id($vals)+1;
        $operation=Array
            (
                "tag" => "OPERATION",
                "type" => "open",
                "level" => 2
            );
        $operation["attributes"]["ID"]=new_id($vals)+2;
        $res['band'][$key]=$operation["attributes"]["ID"];
        $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
        $operation["attributes"]["W"]=$band1["attributes"]["W"];
        $operation["attributes"]["T"]=$band1["attributes"]["T"];
        $operation["attributes"]["L"]=10000;
        $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
        $operation["attributes"]["ELCALCMAT"]="true";
        $operation["attributes"]["ELROUNDMAT"]="true";
        $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
        $operation["attributes"]["ELSYMBOL"]=$letter[$ii];
        $operation["attributes"]["ELCOLOR"]="rgb(".mt_rand(0,5).",255,".mt_rand(0,5).")";
        $operation["attributes"]["ELLINEMARK"]="3";
        $operation["attributes"]["TYPEID"]="EL";
        $material=Array
            (
                "tag" => "MATERIAL",
                "type" => "complete",
                "level" => 2
            );
        $material["attributes"]["ID"]=new_id($vals)+1;
        $operation2=Array
        (
            "tag" => "OPERATION",
            "type" => "close",
            "level" => 2
        );
    
        $vals=vals_into_end($vals,$tool);
        $vals=vals_into_end($vals,$band1);
        $vals=vals_into_end($vals,$operation);
        $vals=vals_into_end($vals,$material);
        $vals=vals_into_end($vals,$operation2);
        $vals=vals_out($vals);
        ksort($vals);
        $index=make_index($vals);
    }
    ksort($vals);
    $res['vals']=$vals;
    return $res;
}

/**
 * 
**/
function glass_to_project ($ar,$client,$place)
{
    if (!isset($client)) $client=123855;
    if (!isset($place)) $place=1;
    
    $date = new DateTime($in[0]['attributes']['DATE']);
    $vals[0]=array('tag'=>'PROJECT','type'=>'open','level'=>1,'attributes'=>array(
        'CURRENCY'=>'грн',
        'VERSION'=>'',
        'DATE'=>$date->getTimestamp(),
        'NAME'=>'RS',
        'DESCRIPTION'=>'',
        'project.my_label'=>'new',
        'project.my_glass'=>1,
        'project.my_glass_key'=>"CfvjdfhjgfhfdjpDfhdfhbx"

    ));

    $vals[1]=array('tag'=>'PROJECT','type'=>'close','level'=>1);
    if(!isset($place)) $place=1;

    $vals=put_source_in($vals,$link,array('sheet_in'=>array($ar['order_st']['material'])),$place);
    ksort($vals);
    $w=[];
    foreach ($ar['parts'] as $p)
    {
        foreach (SIDE as $s)
        {
            if ($p[$s.'_edge']=='facet')
            {
                if (!isset($p[$s.'_facet'][$s.'_facet_side'])) $p[$s.'_facet'][$s.'_facet_side']='no';
                $w[]=[$p[$s.'_facet'][$s.'_length'],$p[$s.'_facet'][$s.'_facet_side']];
            }
        }
    }
    $res=put_band_glass($vals,$place,$w);
    $vals=$res['vals'];
    $band1=$res['band'];

    $sql='SELECT * FROM MATERIAL WHERE MATERIAL_ID='.$ar['order_st']['material'].';';
    $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
    $material_price = [
            array(
                'material_code' => $mat['CODE'],
                'material_name' => $mat['NAME'],
                'sheet_count' => 0,
                'half_sheet_count' => 0,
                'sheet_price' => '280',
    
            )];
    $serice_price=[];
    $part_price=[];

    foreach ($ar['parts'] as $k=>$p)
    {
        $ar_in=[
            'CL'=>$p['l'],
            'DL'=>$p['l'],
            'JL'=>$p['l'],
            'L'=>$p['l'],
            'CW'=>$p['w'],
            'DW'=>$p['w'],
            'JW'=>$p['w'],
            'W'=>$p['w'],
            'COUNT'=>$p['q'],
            'NAME'=>'#'.$k,
            'ID'=>new_id($vals)+1
        ];
        $area=$p['w']*$p['l']/1000/1000*$p['q'];
        $per=($p['w']+$p['l'])*2/1000;
        $material_price[0]['sheet_count']+=$area;
        $service_price=serv_get (113638,$per,$service_price,'service');
        if ($p['operations']['photo_print']<>'')  $service_price=serv_get (21010,$area,$service_price,'service');
        if ($p['operations']['sand_file_back']<>'')  $service_price=serv_get (20444,$area,$service_price,'service');
        if ($p['operations']['sand_file']<>'')  $service_price=serv_get (20444,$area,$service_price,'service');
        if ($p['operations']['mat']==1)
        {
            if ($area>=0.5) $service_price=serv_get (20445,$area,$service_price,'service');
            else $service_price=serv_get (20446,$area,$service_price,'service');
        }
        if ($p['operations']['gfh']==1)
        {
            $service_price=serv_get (20457,$area,$service_price,'service');
        }
        if ($p['operations']['zakalka']==1)
        {
            switch ($mat['T'])
            {
                case 10: $service_price=serv_get (61306,$area,$service_price,'service'); break;
                case 4: $service_price=serv_get (61303,$area,$service_price,'service'); break;
                case 5: $service_price=serv_get (61312,$area,$service_price,'service'); break;
                case 6: $service_price=serv_get (61304,$area,$service_price,'service'); break;
                case 8: $service_price=serv_get (61305,$area,$service_price,'service'); break;
                default: $service_price=serv_get (61303,$area,$service_price,'service'); break;
            }
            $service_price=serv_get (20457,$area,$service_price,'service');
        }
        if ($p['operations']['film']==1)
        {
            $service_price=serv_get (58850,$area,$service_price,'service');
            $simple_price=serv_get (82918,$area*rs_general('oracal_film_up'),$simple_price,'simple');
        }
        if ($p['operations']['plastina']==1)
        {
            $service_price=serv_get (22092,$area,$service_price,'service');
            $simple_price=serv_get (23275,1,$simple_price,'simple');
        }
        if ($p['operations']['film_art']==1)
        {
            $service_price=serv_get (27798,$area,$service_price,'service');
            $simple_price=serv_get (31846,$area*rs_general('film_frost_up'),$simple_price,'simple');
        }
        if ($p['operations']['oracal']>0)
        {
            $service_price=serv_get (20449,$area,$service_price,'service');
            $simple_price=serv_get (58848,$area*rs_general('oracal_film_up'),$simple_price,'simple');
        }
        if ($p['operations']['UF_print']<>'')
        {
            $service_price=serv_get (61583,$area,$service_price,'service');
            $simple_price=serv_get (58846,$area*rs_general('lux_film_transparent_up'),$simple_price,'simple');
        }
        if ($p['operations']['UF_print_white']<>'')
        {
            $service_price=serv_get (61584,$area,$service_price,'service');
        }
        unset($put_in,$ss,$s_curve);
        if (count($p['operations']['corner']['lu'])>0)
        {
            $s_curve['l']=1;
            $s_curve['u']=1;
        }
        if (count($p['operations']['corner']['ld'])>0)
        {
            $s_curve['l']=1;
            $s_curve['b']=1;
        }
        if (count($p['operations']['corner']['ru'])>0)
        {
            $s_curve['r']=1;
            $s_curve['u']=1;
        }
        if (count($p['operations']['corner']['rd'])>0)
        {
            $s_curve['r']=1;
            $s_curve['b']=1;
        }
        foreach (SIDE as $s)
        {
            if (($s=='l')||($s=='r')) $count = $p['w']/1000;
            else $count = $p['l']/1000;
            if ((isset($p[$s.'_edge']))&&($p[$s.'_edge']=='shlif'))
            {
                $ar_in['EL'.mb_strtoupper($s)]='@operation#'.$band1['shlif'];
                $put_in[$band1['shlif']]=1;
                $ss[$s]['shlif']=1;
                if (!isset($s_curve[$s]))
                {
                    switch ($mat['T'])
                    {
                        case 10: $service_price=serv_get (50049,$count,$service_price,'service'); break;
                        case 4: $service_price=serv_get (31890,$count,$service_price,'service'); break;
                        case 5: $service_price=serv_get (50046,$count,$service_price,'service'); break;
                        case 6: $service_price=serv_get (50047,$count,$service_price,'service'); break;
                        case 8: $service_price=serv_get (50048,$count,$service_price,'service'); break;
                        default: $service_price=serv_get (31890,$count,$service_price,'service'); break;
                    }
                }
                else
                {
                    switch ($mat['T'])
                    {
                        case 10: $service_price=serv_get (53030,$count,$service_price,'service'); break;
                        case 4: $service_price=serv_get (53034,$count,$service_price,'service'); break;
                        case 5: $service_price=serv_get (53033,$count,$service_price,'service'); break;
                        case 6: $service_price=serv_get (53032,$count,$service_price,'service'); break;
                        case 8: $service_price=serv_get (53031,$count,$service_price,'service'); break;
                        default: $service_price=serv_get (53034,$count,$service_price,'service'); break;
                    }
                }
                
                
            }
            elseif ((isset($p[$s.'_edge']))&&($p[$s.'_edge']=='poli'))
            {
                $ar_in['EL'.mb_strtoupper($s)]='@operation#'.$band1['poli'];
                $put_in[$band1['poli']]=1;

                $ss[$s]['poli']=1;
                if (!isset($s_curve[$s]))
                {
                    switch ($mat['T'])
                    {
                        case 10: $service_price=serv_get (20416,$count,$service_price,'service'); break;
                        case 4: $service_price=serv_get (20402,$count,$service_price,'service'); break;
                        case 5: $service_price=serv_get (20403,$count,$service_price,'service'); break;
                        case 6: $service_price=serv_get (20404,$count,$service_price,'service'); break;
                        case 8: $service_price=serv_get (20405,$count,$service_price,'service'); break;
                        default: $service_price=serv_get (20402,$count,$service_price,'service'); break;
                    }
                }
                else
                {
                    switch ($mat['T'])
                    {
                        case 10: $service_price=serv_get (20539,$count,$service_price,'service'); break;
                        case 4: $service_price=serv_get (20540,$count,$service_price,'service'); break;
                        case 5: $service_price=serv_get (20536,$count,$service_price,'service'); break;
                        case 6: $service_price=serv_get (20537,$count,$service_price,'service'); break;
                        case 8: $service_price=serv_get (20538,$count,$service_price,'service'); break;
                        default: $service_price=serv_get (20540,$count,$service_price,'service'); break;
                    }
                }
                
            }
            elseif ((isset($p[$s.'_edge']))&&($p[$s.'_edge']=='facet'))
            {
                if (!isset($p[$s.'_facet'][$s.'_facet_side']))$p[$s.'_facet'][$s.'_facet_side']='no';
                $ar_in['EL'.mb_strtoupper($s)]='@operation#'.$band1['facet'.$p[$s.'_facet'][$s.'_length'].$p[$s.'_facet'][$s.'_facet_side']];
                $put_in[$band1['facet'.$p[$s.'_facet'][$s.'_length'].$p[$s.'_facet'][$s.'_facet_side']]]=1;

                $ss[$s]['facet']=1;
                if (!isset($s_curve[$s]))
                {
                    switch ($p[$s.'_facet'][$s.'_length'])
                    {
                        case ($p[$s.'_facet'][$s.'_length']<=10): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31871,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31814,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31863,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31871,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=15)&&($p[$s.'_facet'][$s.'_length']>10)): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31872,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31854,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31864,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31872,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=20)&&($p[$s.'_facet'][$s.'_length']>15)): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31873,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31855,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31865,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31873,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=25)&&($p[$s.'_facet'][$s.'_length']>20)): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31874,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31856,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31866,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31874,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=30)&&($p[$s.'_facet'][$s.'_length']>25)): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31875,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31857,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31867,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31875,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=35)&&($p[$s.'_facet'][$s.'_length']>30)): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31876,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31858,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31868,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31876,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=40)&&($p[$s.'_facet'][$s.'_length']>40)): 
                        {
                            switch ($p[$s.'_facet'][$s.'_facet_side'])
                                {
                                    case 'no': $service_price=serv_get (31877,$count,$service_price,'service');break;
                                    case 'poli': $service_price=serv_get (31859,$count,$service_price,'service');break;
                                    case 'shlif': $service_price=serv_get (31869,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (31877,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        default: $service_price=serv_get (31871,$count,$service_price,'service');break;
                    }
                }
                else
                {
                    switch ($p[$s.'_facet'][$s.'_length'])
                    {
                        case ($p[$s.'_facet'][$s.'_length']<=10): 
                        {
                            switch($mat['T'])
                                {
                                    case 10: $service_price=serv_get (20418,$count,$service_price,'service');break;
                                    case 8: $service_price=serv_get (20410,$count,$service_price,'service');break;
                                    case 5: $service_price=serv_get (20407,$count,$service_price,'service');break;
                                    case 6: $service_price=serv_get (20408,$count,$service_price,'service');break;
                                    case 4: $service_price=serv_get (20406,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (20406,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=15)&&($p[$s.'_facet'][$s.'_length']>10)): 
                        {
                            switch($mat['T'])
                                {
                                    case 10: $service_price=serv_get (20461,$count,$service_price,'service');break;
                                    case 8: $service_price=serv_get (20460,$count,$service_price,'service');break;
                                    case 5: $service_price=serv_get (20462,$count,$service_price,'service');break;
                                    case 6: $service_price=serv_get (20463,$count,$service_price,'service');break;
                                    case 4: $service_price=serv_get (20464,$count,$service_price,'service');break;
                                    default: $service_price=serv_get (20464,$count,$service_price,'service');break;

                                }
                                
                            break;
                        }
                        case (($p[$s.'_facet'][$s.'_length']<=20)&&($p[$s.'_facet'][$s.'_length']>15)): 
                            {
                                switch($mat['T'])
                                    {
                                        case 10: $service_price=serv_get (20481,$count,$service_price,'service');break;
                                        case 8: $service_price=serv_get (20480,$count,$service_price,'service');break;
                                        case 6: $service_price=serv_get (20482,$count,$service_price,'service');break;
                                        case 5: $service_price=serv_get (20483,$count,$service_price,'service');break;
                                        case 4: $service_price=serv_get (20484,$count,$service_price,'service');break;
                                        default: $service_price=serv_get (20484,$count,$service_price,'service');break;
    
                                    }
                                    
                                break;
                            }
                        case (($p[$s.'_facet'][$s.'_length']<=25)&&($p[$s.'_facet'][$s.'_length']>20)): 
                            {
                                switch($mat['T'])
                                    {
                                        case 10: $service_price=serv_get (20493,$count,$service_price,'service');break;
                                        case 8: $service_price=serv_get (20492,$count,$service_price,'service');break;
                                        case 6: $service_price=serv_get (20494,$count,$service_price,'service');break;
                                        case 5: $service_price=serv_get (20495,$count,$service_price,'service');break;
                                        case 4: $service_price=serv_get (20496,$count,$service_price,'service');break;
                                        default: $service_price=serv_get (20496,$count,$service_price,'service');break;
    
                                    }
                                    
                                break;
                            }
                        case (($p[$s.'_facet'][$s.'_length']<=30)&&($p[$s.'_facet'][$s.'_length']>25)): 
                            {
                                switch($mat['T'])
                                    {
                                        case 10: $service_price=serv_get (20500,$count,$service_price,'service');break;
                                        case 8: $service_price=serv_get (20498,$count,$service_price,'service');break;
                                        case 6: $service_price=serv_get (20499,$count,$service_price,'service');break;
                                        case 5: $service_price=serv_get (20501,$count,$service_price,'service');break;
                                        case 4: $service_price=serv_get (20502,$count,$service_price,'service');break;
                                        default: $service_price=serv_get (20502,$count,$service_price,'service');break;
    
                                    }
                                    
                                break;
                            }
                        case (($p[$s.'_facet'][$s.'_length']<=35)&&($p[$s.'_facet'][$s.'_length']>30)): 
                            {
                                switch($mat['T'])
                                    {
                                        case 10: $service_price=serv_get (20506,$count,$service_price,'service');break;
                                        case 8: $service_price=serv_get (20505,$count,$service_price,'service');break;
                                        case 6: $service_price=serv_get (20504,$count,$service_price,'service');break;
                                        case 5: $service_price=serv_get (20507,$count,$service_price,'service');break;
                                        case 4: $service_price=serv_get (20508,$count,$service_price,'service');break;
                                        default: $service_price=serv_get (20508,$count,$service_price,'service');break;
    
                                    }
                                    
                                break;
                            }
                        case (($p[$s.'_facet'][$s.'_length']<=40)&&($p[$s.'_facet'][$s.'_length']>35)): 
                            {
                                switch($mat['T'])
                                    {
                                        case 10: $service_price=serv_get (20512,$count,$service_price,'service');break;
                                        case 8: $service_price=serv_get (20511,$count,$service_price,'service');break;
                                        case 6: $service_price=serv_get (20510,$count,$service_price,'service');break;
                                        case 5: $service_price=serv_get (20513,$count,$service_price,'service');break;
                                        case 4: $service_price=serv_get (20514,$count,$service_price,'service');break;
                                        default: $service_price=serv_get (20514,$count,$service_price,'service');break;
    
                                    }
                                    
                                break;
                            }
                        default: $service_price=serv_get (20406,$count,$service_price,'service');break;
                    }
                }
                
            }
        }

        $index=make_index($vals);
        foreach ($index['GOOD'] as $i)
        {
            if ($vals[$i]['attributes']['TYPEID']=='product')
            {
                $vals=move_vals($vals,$i+1,1);
                $vals[$i+1]=Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 3,
                    'attributes'=>$ar_in
                );
            }
        }
        ksort ($vals);
        $index=make_index($vals);
        foreach ($index['OPERATION'] as $i)
        {
            if ($vals[$i]['attributes']['TYPEID']=='CS')
            {
                $vals=move_vals($vals,$i+2,1);
                $vals[$i+2]=Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 3,
                    'attributes'=>['ID'=>$ar_in['ID']]
                );
            }
        }
        ksort($vals);
        $index=make_index($vals);

        foreach ($put_in as $pk=>$vv)
        {
            foreach ($index['OPERATION'] as $i)
            {
                if (($vals[$i]['attributes']['TYPEID']=='EL')&&($vals[$i]['attributes']['ID']==$pk))
                {
                    $vals=move_vals($vals,$i+2,1);
                    $vals[$i+2]=Array
                    (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 3,
                        'attributes'=>['ID'=>$ar_in['ID']]
                    );
                }
            }
            ksort($vals);
            $index=make_index($vals);
        }
        unset($put_xnc);
        if ((isset($p['operations']['corner']['ld']))&&($p['operations']['corner']['ld']['radius']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h48 обгоночная длинная&quot; name=&quot;Mill20L&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;1 радиусы/[/0001/] R1 Один радиус (на 1-м угле).xnc&quot;&gt;&lt;par comment=&quot;R1&quot; name=&quot;R1&quot; value=&quot;".$p['operations']['corner']['ld']['radius']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;1 радиусы/[/0001/] R1 Один радиус (на 1-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;Fuga&quot; name=&quot;F&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;par comment=&quot;R1&quot; name=&quot;R1&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if comment=&quot;Значение радиуса не должно превышать размера детали&quot; expand=&quot;true&quot; condition=&quot;R1&amp;gt;dx-F or R1&amp;gt;dy-F&quot;&gt;&lt;msg msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.035&quot;&gt;&lt;msg msg=&quot;'Площадь детали должна быть больше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Площадь детали должна быть больше'&quot;/&gt;&lt;/if&gt;&lt;if comment=&quot;Значение радиуса должно быть меньше&quot; expand=&quot;true&quot; condition=&quot;R1&amp;gt;dy or R1&amp;gt;dx&quot;&gt;&lt;msg msg=&quot;'Значение радиуса должно быть меньше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса должно быть меньше'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx&amp;lt;100 or dy&amp;lt;100&quot;&gt;&lt;msg msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;lt;=24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R1&amp;gt;0 and R1&amp;lt;=dy-F and R1&amp;lt;=dx-F&quot;&gt;&lt;ms x=&quot;(0+F)&quot; y=&quot;(dy+5)&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;(0+F)&quot; y=&quot;(R1+F)&quot;/&gt;&lt;ma x=&quot;(0+R1+F)&quot; y=&quot;(0+F)&quot; r=&quot;-R1&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;dx+5&quot; y=&quot;(0+F)&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;gt;24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R1&amp;gt;0 and R1&amp;lt;=dy-F and R1&amp;lt;=dx-F&quot;&gt;&lt;ms x=&quot;(0+F)&quot; y=&quot;(dy+5)&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20L&quot;/&gt;&lt;ml x=&quot;(0+F)&quot; y=&quot;(R1+F)&quot;/&gt;&lt;ma x=&quot;(0+R1+F)&quot; y=&quot;(0+F)&quot; r=&quot;-R1&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;dx+5&quot; y=&quot;(0+F)&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($p['operations']['corner']['ld']['radius']<=20)
            {
                if ($mat['T']<=6) $service_price=serv_get (31887,1,$service_price,'service');
                else $service_price=serv_get (31888,1,$service_price,'service');
            }
            else
            {
                if ($mat['T']<=6) $service_price=serv_get (34709,1,$service_price,'service');
                else $service_price=serv_get (34708,1,$service_price,'service');
            }
            
            
        }
        if ((isset($p['operations']['corner']['lu']))&&($p['operations']['corner']['lu']['radius']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h48 обгоночная длинная&quot; name=&quot;Mill20L&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;1 радиусы/[/0009/] R4 Один радиус (на 4-м угле).xnc&quot;&gt;&lt;par comment=&quot;R4&quot; name=&quot;R4&quot; value=&quot;".$p['operations']['corner']['lu']['radius']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;1 радиусы/[/0009/] R4 Один радиус (на 4-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;Fuga&quot; name=&quot;F&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;par comment=&quot;R4&quot; name=&quot;R4&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.035&quot;&gt;&lt;msg msg=&quot;'Площадь детали должна быть больше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Площадь детали должна быть больше'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;R4&amp;gt;dy-F or R4&amp;gt;dx-F&quot;&gt;&lt;msg msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx&amp;lt;150 or dy&amp;lt;100&quot;&gt;&lt;msg msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot;/&gt;&lt;/if&gt;&lt;if comment=&quot;Значение радиуса должно быть меньше&quot; expand=&quot;true&quot; condition=&quot;R4&amp;gt;dy or R4&amp;gt;dx&quot;&gt;&lt;msg msg=&quot;'Значение радиуса должно быть меньше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса должно быть меньше'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;lt;=24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R4&amp;gt;0 and R4&amp;lt;=dx-F and R4&amp;lt;=dy-F&quot;&gt;&lt;ms x=&quot;dx+5&quot; y=&quot;dy-F&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;0+R4+F&quot; y=&quot;dy-F&quot;/&gt;&lt;ma x=&quot;0+F&quot; y=&quot;(dy-R4-F)&quot; r=&quot;-R4&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;0+F&quot; y=&quot;(0-5)&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;gt;24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R4&amp;gt;0 and R4&amp;lt;=dx-F and R4&amp;lt;=dy-F&quot;&gt;&lt;ms x=&quot;dx+5&quot; y=&quot;dy-F&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20L&quot;/&gt;&lt;ml x=&quot;0+R4+F&quot; y=&quot;dy-F&quot;/&gt;&lt;ma x=&quot;0+F&quot; y=&quot;(dy-R4-F)&quot; r=&quot;-R4&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;0+F&quot; y=&quot;(0-5)&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($p['operations']['corner']['ld']['radius']<=20)
            {
                if ($mat['T']<=6) $service_price=serv_get (31887,1,$service_price,'service');
                else $service_price=serv_get (31888,1,$service_price,'service');
            }
            else
            {
                if ($mat['T']<=6) $service_price=serv_get (34709,1,$service_price,'service');
                else $service_price=serv_get (34708,1,$service_price,'service');
            }            
        }
        if ((isset($p['operations']['corner']['ru']))&&($p['operations']['corner']['ru']['radius']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h48 обгоночная длинная&quot; name=&quot;Mill20L&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;1 радиусы/[/0008/] R3 Один радиус (на 3-м угле).xnc&quot;&gt;&lt;par comment=&quot;R3&quot; name=&quot;R3&quot; value=&quot;".$p['operations']['corner']['ru']['radius']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;1 радиусы/[/0008/] R3 Один радиус (на 3-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;Fuga&quot; name=&quot;F&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;par comment=&quot;R3&quot; name=&quot;R3&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.035&quot;&gt;&lt;msg msg=&quot;'Площадь детали должна быть больше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Площадь детали должна быть больше'&quot;/&gt;&lt;/if&gt;&lt;if comment=&quot;Значение радиуса должно быть меньше&quot; expand=&quot;true&quot; condition=&quot;R3&amp;gt;dy-F or R3&amp;gt;dx-F&quot;&gt;&lt;msg msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx&amp;lt;100 or dy&amp;lt;100&quot;&gt;&lt;msg msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot;/&gt;&lt;/if&gt;&lt;if comment=&quot;Значение радиуса должно быть меньше&quot; expand=&quot;true&quot; condition=&quot;R3&amp;gt;dy or R3&amp;gt;dx&quot;&gt;&lt;msg msg=&quot;'Значение радиуса должно быть меньше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса должно быть меньше'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;lt;=24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R3&amp;gt;0 and R3&amp;lt;=dy-F and R3&amp;lt;=dx-F&quot;&gt;&lt;ms x=&quot;dx-F&quot; y=&quot;(0-5)&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;dx-F&quot; y=&quot;(dy-R3-F)&quot;/&gt;&lt;ma x=&quot;dx-R3-F&quot; y=&quot;dy-F&quot; r=&quot;-R3&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;0-5&quot; y=&quot;dy-F&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;gt;24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R3&amp;gt;0 and R3&amp;lt;=dy-F and R3&amp;lt;=dx-F&quot;&gt;&lt;ms x=&quot;dx-F&quot; y=&quot;(0-5)&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20L&quot;/&gt;&lt;ml x=&quot;dx-F&quot; y=&quot;(dy-R3-F)&quot;/&gt;&lt;ma x=&quot;dx-R3-F&quot; y=&quot;dy-F&quot; r=&quot;-R3&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;0-5&quot; y=&quot;dy-F&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($p['operations']['corner']['ld']['radius']<=20)
            {
                if ($mat['T']<=6) $service_price=serv_get (31887,1,$service_price,'service');
                else $service_price=serv_get (31888,1,$service_price,'service');
            }
            else
            {
                if ($mat['T']<=6) $service_price=serv_get (34709,1,$service_price,'service');
                else $service_price=serv_get (34708,1,$service_price,'service');
            }            
        }
        if ((isset($p['operations']['corner']['rd']))&&($p['operations']['corner']['rd']['radius']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h48 обгоночная длинная&quot; name=&quot;Mill20L&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;1 радиусы/[/0007/] R2 Один радиус (на 2-м угле).xnc&quot;&gt;&lt;par comment=&quot;R2&quot; name=&quot;R2&quot; value=&quot;".$p['operations']['corner']['rd']['radius']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;1 радиусы/[/0007/] R2 Один радиус (на 2-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;Fuga&quot; name=&quot;F&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;par comment=&quot;R2&quot; name=&quot;R2&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if comment=&quot;Значение радиуса не должно превышать размера детали&quot; expand=&quot;true&quot; condition=&quot;R2&amp;gt;dx-F or R2&amp;gt;dy-F&quot;&gt;&lt;msg msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса не должно превышать размера заготовки'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.035&quot;&gt;&lt;msg msg=&quot;'Площадь детали должна быть больше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Площадь детали должна быть больше'&quot;/&gt;&lt;/if&gt;&lt;if comment=&quot;Значение радиуса должно быть меньше&quot; expand=&quot;true&quot; condition=&quot;R2&amp;gt;dy or R2&amp;gt;dx&quot;&gt;&lt;msg msg=&quot;'Значение радиуса должно быть меньше'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Значение радиуса должно быть меньше'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx&amp;lt;100 or dy&amp;lt;100&quot;&gt;&lt;msg msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot; type=&quot;info&quot;/&gt;&lt;err msg=&quot;'Минимальный размер детали для фрезерования 350х100мм'&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;lt;=24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R2&amp;gt;0 and R2&amp;lt;=dx-F and R2&amp;lt;=dy-F&quot;&gt;&lt;ms x=&quot;(0-5)&quot; y=&quot;(0+F)&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;(dx-R2-F)&quot; y=&quot;(0+F)&quot;/&gt;&lt;ma x=&quot;dx-F&quot; y=&quot;(0+R2+F)&quot; r=&quot;-R2&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;dx-F&quot; y=&quot;(dy+5)&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dz&amp;gt;24&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;R2&amp;gt;0 and R2&amp;lt;=dx-F and R2&amp;lt;=dy-F&quot;&gt;&lt;ms x=&quot;(0-5)&quot; y=&quot;(0+F)&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20L&quot;/&gt;&lt;ml x=&quot;(dx-R2-F)&quot; y=&quot;(0+F)&quot;/&gt;&lt;ma x=&quot;dx-F&quot; y=&quot;(0+R2+F)&quot; r=&quot;-R2&quot; dir=&quot;true&quot;/&gt;&lt;ml x=&quot;dx-F&quot; y=&quot;(dy+5)&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($p['operations']['corner']['ld']['radius']<=20)
            {
                if ($mat['T']<=6) $service_price=serv_get (31887,1,$service_price,'service');
                else $service_price=serv_get (31888,1,$service_price,'service');
            }
            else
            {
                if ($mat['T']<=6) $service_price=serv_get (34709,1,$service_price,'service');
                else $service_price=serv_get (34708,1,$service_price,'service');
            }            
        }
        if ((isset($p['operations']['corner']['ld']))&&($p['operations']['corner']['ld']['zarez']['x']>0)&&($p['operations']['corner']['ld']['zarez']['y']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;3 зарезы/[/0019/] Зарез (на 1-м угле).xnc&quot;&gt;&lt;par comment=&quot;a&quot; name=&quot;a&quot; value=&quot;".$p['operations']['corner']['ld']['zarez']['y']."&quot;/&gt;&lt;par comment=&quot;b&quot; name=&quot;b&quot; value=&quot;".$p['operations']['corner']['ld']['zarez']['x']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;3 зарезы/[/0019/] Зарез (на 1-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;a&quot; name=&quot;a&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;par comment=&quot;b&quot; name=&quot;b&quot; type=&quot;double&quot; expr=&quot;200&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.045&quot;&gt;&lt;err msg=&quot;Площадь детали должна быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx-b&amp;lt;140&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dy-a&amp;lt;100&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;b&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;a&amp;gt;0&quot;&gt;&lt;ms x=&quot;0&quot; y=&quot;a&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;b&quot; y=&quot;a&quot;/&gt;&lt;ml x=&quot;b&quot; y=&quot;0&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            $service_price=serv_get (76997,1,$service_price,'service');          
        }
        if ((isset($p['operations']['corner']['lu']))&&($p['operations']['corner']['lu']['zarez']['x']>0)&&($p['operations']['corner']['lu']['zarez']['y']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;3 зарезы/[/0022/] Зарез (на 4-м угле).xnc&quot;&gt;&lt;par comment=&quot;i&quot; name=&quot;i&quot; value=&quot;".$p['operations']['corner']['lu']['zarez']['x']."&quot;/&gt;&lt;par comment=&quot;k&quot; name=&quot;k&quot; value=&quot;".$p['operations']['corner']['lu']['zarez']['y']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;3 зарезы/[/0022/] Зарез (на 4-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;i&quot; name=&quot;i&quot; type=&quot;double&quot; expr=&quot;200&quot;/&gt;&lt;par comment=&quot;k&quot; name=&quot;k&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.045&quot;&gt;&lt;err msg=&quot;Площадь детали должна быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx-i&amp;lt;50&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dy-k&amp;lt;50&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;i&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;k&amp;gt;0&quot;&gt;&lt;ms x=&quot;i&quot; y=&quot;dy&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;i&quot; y=&quot;dy-k&quot;/&gt;&lt;ml x=&quot;0&quot; y=&quot;dy-k&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            $service_price=serv_get (76997,1,$service_price,'service');          
        }
        if ((isset($p['operations']['corner']['ru']))&&($p['operations']['corner']['ru']['zarez']['x']>0)&&($p['operations']['corner']['ru']['zarez']['y']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;3 зарезы/[/0021/] Зарез (на 3-м угле).xnc&quot;&gt;&lt;par comment=&quot;f&quot; name=&quot;f&quot; value=&quot;".$p['operations']['corner']['ru']['zarez']['y']."&quot;/&gt;&lt;par comment=&quot;g&quot; name=&quot;g&quot; value=&quot;".$p['operations']['corner']['ru']['zarez']['x']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;3 зарезы/[/0021/] Зарез (на 3-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;f&quot; name=&quot;f&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;par comment=&quot;g&quot; name=&quot;g&quot; type=&quot;double&quot; expr=&quot;200&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.045&quot;&gt;&lt;err msg=&quot;Площадь детали должна быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx-g&amp;lt;50&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dy-f&amp;lt;50&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;f&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;g&amp;gt;0&quot;&gt;&lt;ms x=&quot;dx&quot; y=&quot;dy-f&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;dx-g&quot; y=&quot;dy-f&quot;/&gt;&lt;ml x=&quot;dx-g&quot; y=&quot;dy&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            $service_price=serv_get (76997,1,$service_price,'service');           
        }
        if ((isset($p['operations']['corner']['rd']))&&($p['operations']['corner']['rd']['zarez']['x']>0)&&($p['operations']['corner']['rd']['zarez']['y']>0))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза d20 h28 обгоночная короткая&quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;3 зарезы/[/0020/] Зарез (на 2-м угле).xnc&quot;&gt;&lt;par comment=&quot;c&quot; name=&quot;c&quot; value=&quot;".$p['operations']['corner']['rd']['zarez']['x']."&quot;/&gt;&lt;par comment=&quot;d&quot; name=&quot;d&quot; value=&quot;".$p['operations']['corner']['rd']['zarez']['y']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;3 зарезы/[/0020/] Зарез (на 2-м угле).xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;c&quot; name=&quot;c&quot; type=&quot;double&quot; expr=&quot;200&quot;/&gt;&lt;par comment=&quot;d&quot; name=&quot;d&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;(dx*dy/1000000)&amp;lt;0.045&quot;&gt;&lt;err msg=&quot;Площадь детали должна быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dx-c&amp;lt;50&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;dy-d&amp;lt;50&quot;&gt;&lt;err msg=&quot;Размер детали должен быть больше&quot;/&gt;&lt;/if&gt;&lt;if expand=&quot;true&quot; condition=&quot;c&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;d&amp;gt;0&quot;&gt;&lt;ms x=&quot;dx-c&quot; y=&quot;0&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;dx-c&quot; y=&quot;d&quot;/&gt;&lt;ml x=&quot;dx&quot; y=&quot;d&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            $service_price=serv_get (76997,1,$service_price,'service');           
        }
        
        if ((isset($p['operations']['corner']['ld']))&&isset($p['operations']['corner']['ld']['srez']['x1']))

        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза короткая d=20мм L=28мм &quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0089/] Срез левого нижнего угла по 2 параметрам.xnc&quot;&gt;&lt;par comment=&quot;Высота_среза_по_Y&quot; name=&quot;a&quot; value=&quot;".$p['operations']['corner']['ld']['srez']['x1']."&quot;/&gt;&lt;par comment=&quot;Ширина_среза_по_X &quot; name=&quot;b&quot; value=&quot;".$p['operations']['corner']['ld']['srez']['y2']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0089/] Срез левого нижнего угла по 2 параметрам.xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;Высота_среза_по_Y&quot; name=&quot;a&quot; type=&quot;double&quot; expr=&quot;50&quot;/&gt;&lt;par comment=&quot;Ширина_среза_по_X &quot; name=&quot;b&quot; type=&quot;double&quot; expr=&quot;75&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;a&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;b&amp;gt;0&quot;&gt;&lt;ms x=&quot;0&quot; y=&quot;a&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;b&quot; y=&quot;0&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($mat['T']<=6) $service_price=serv_get (34706,1,$service_price,'service'); 
            else  $service_price=serv_get (34707,1,$service_price,'service'); 
        }
        if ((isset($p['operations']['corner']['lu']))&&isset($p['operations']['corner']['lu']['srez']['x1']))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза короткая d=20мм L=28мм &quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0097/] Срез левого верхнего угла по 2 параметрам.xnc&quot;&gt;&lt;par comment=&quot;Ширина_среза_по_X4&quot; name=&quot;i&quot; value=&quot;".$p['operations']['corner']['lu']['srez']['x2']."&quot;/&gt;&lt;par comment=&quot;Высота_среза_по_Y4&quot; name=&quot;k&quot; value=&quot;".$p['operations']['corner']['lu']['srez']['y1']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0097/] Срез левого верхнего угла по 2 параметрам.xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;Ширина_среза_по_X4&quot; name=&quot;i&quot; type=&quot;double&quot; expr=&quot;200&quot;/&gt;&lt;par comment=&quot;Высота_среза_по_Y4&quot; name=&quot;k&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;i&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;k&amp;gt;0&quot;&gt;&lt;ms x=&quot;i&quot; y=&quot;dy&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;0&quot; y=&quot;dy-k&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($mat['T']<=6) $service_price=serv_get (34706,1,$service_price,'service'); 
            else  $service_price=serv_get (34707,1,$service_price,'service');           
        }
        if ((isset($p['operations']['corner']['ru']))&&isset($p['operations']['corner']['ru']['srez']['x1']))

        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза короткая d=20мм L=28мм &quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0088/] Срез правого верхнего угла по 2 параметрам.xnc&quot;&gt;&lt;par comment=&quot;Высота_среза_по_Y&quot; name=&quot;f&quot; value=&quot;".$p['operations']['corner']['ru']['srez']['y2']."&quot;/&gt;&lt;par comment=&quot;Ширина_среза_по_X&quot; name=&quot;g&quot; value=&quot;".$p['operations']['corner']['ru']['srez']['x1']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0088/] Срез правого верхнего угла по 2 параметрам.xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;Высота_среза_по_Y&quot; name=&quot;f&quot; type=&quot;double&quot; expr=&quot;300&quot;/&gt;&lt;par comment=&quot;Ширина_среза_по_X&quot; name=&quot;g&quot; type=&quot;double&quot; expr=&quot;175&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;f&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;g&amp;gt;0&quot;&gt;&lt;ms x=&quot;dx&quot; y=&quot;dy-f&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;dx-g&quot; y=&quot;dy&quot; dp=&quot;dz+3&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($mat['T']<=6) $service_price=serv_get (34706,1,$service_price,'service'); 
            else  $service_price=serv_get (34707,1,$service_price,'service');           
        }
        if ((isset($p['operations']['corner']['rd']))&&isset($p['operations']['corner']['rd']['srez']['x1']))
        {
            $put_xnc.="&lt;com comment=&quot;CREATE_IN_EDITOR&quot;/&gt;&lt;tool comment=&quot;Фреза короткая d=20мм L=28мм &quot; name=&quot;Mill20&quot; d=&quot;20&quot;/&gt;&lt;call name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0096/] Срез правого нижнего угла по 2 параметрам.xnc&quot;&gt;&lt;par comment=&quot;Ширина_среза_по_X2&quot; name=&quot;c&quot; value=&quot;".$p['operations']['corner']['rd']['srez']['y1']."&quot;/&gt;&lt;par comment=&quot;Высота_среза_по_Y2&quot; name=&quot;d&quot; value=&quot;".$p['operations']['corner']['rd']['srez']['x2']."&quot;/&gt;&lt;/call&gt;&lt;sub name=&quot;2 срезы углов для ЛДСП толщиной 10-22 мм/[/0096/] Срез правого нижнего угла по 2 параметрам.xnc&quot;&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;l123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;b123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;r123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;t123&quot; type=&quot;double&quot; expr=&quot;0&quot;/&gt;&lt;var comment=&quot;[MY]&quot; name=&quot;x123&quot; type=&quot;double&quot; expr=&quot;1&quot;/&gt;&lt;par comment=&quot;Ширина_среза_по_X2&quot; name=&quot;c&quot; type=&quot;double&quot; expr=&quot;200&quot;/&gt;&lt;par comment=&quot;Высота_среза_по_Y2&quot; name=&quot;d&quot; type=&quot;double&quot; expr=&quot;100&quot;/&gt;&lt;if expand=&quot;true&quot; condition=&quot;c&amp;gt;0&quot;&gt;&lt;if expand=&quot;true&quot; condition=&quot;d&amp;gt;0&quot;&gt;&lt;ms x=&quot;dx-c&quot; y=&quot;0&quot; dp=&quot;dz+3&quot; in=&quot;202&quot; out=&quot;202&quot; sxy=&quot;tool.dia/2&quot; c=&quot;2&quot; name=&quot;Mill20&quot;/&gt;&lt;ml x=&quot;dx&quot; y=&quot;d&quot;/&gt;&lt;/if&gt;&lt;/if&gt;&lt;/sub&gt;";
            if ($mat['T']<=6) $service_price=serv_get (34706,1,$service_price,'service'); 
            else  $service_price=serv_get (34707,1,$service_price,'service');            
        }
        foreach ($p['operations']['face']['bore'] as $b)
        {
            if ($b['zenk']==0) 
            {
                $put_xnc.='&lt;tool name=&quot;Bore'.$b['d'].'&quot; d=&quot;'.$b['d'].'&quot;/&gt;&lt;bf x=&quot;'.$b['x'].'&quot; y=&quot;'.$b['y'].'&quot; dp=&quot;dz+3&quot; ac=&quot;1&quot; av=&quot;false&quot; name=&quot;Bore'.$b['d'].'&quot;/&gt;';
                
            }
            else
            {
                if (($b['d']<20)&&($b['zenk']==1)) $service_price=serv_get (22882,1,$service_price,'service');
                else $service_price=serv_get (58849,1,$service_price,'service');
                $put_xnc.='&lt;tool name=&quot;Bore'.$b['d'].'&quot; d=&quot;'.$b['d'].'&quot;/&gt;&lt;bf x=&quot;'.$b['x'].'&quot; y=&quot;'.$b['y'].'&quot; dp=&quot;dz+3&quot; ac=&quot;1&quot; av=&quot;false&quot; name=&quot;Bore'.$b['d'].'&quot;/&gt;';
                $b['d']+=1.5;
                $put_xnc.='&lt;tool name=&quot;Bore'.$b['d'].'&quot; d=&quot;'.$b['d'].'&quot;/&gt;&lt;bf x=&quot;'.$b['x'].'&quot; y=&quot;'.$b['y'].'&quot; dp=&quot;3&quot; ac=&quot;1&quot; av=&quot;false&quot; name=&quot;Bore'.$b['d'].'&quot;/&gt;';
            }
            if ($mat['T']<=6)
            {
                if ($b['d']<=6)  $service_price=serv_get (20431,1,$service_price,'service');
                elseif (($b['d']>6)&&($b['d']<=12))  $service_price=serv_get (20433,1,$service_price,'service');
                elseif (($b['d']>12)&&($b['d']<60))  $service_price=serv_get (20435,1,$service_price,'service');
                elseif ($b['d']>=60)  $service_price=serv_get (20437,1,$service_price,'service');
            }
            else 
            {
                if ($b['d']<=6)  $service_price=serv_get (20432,1,$service_price,'service');
                elseif (($b['d']>6)&&($b['d']<=12))  $service_price=serv_get (20434,1,$service_price,'service');
                elseif (($b['d']>12)&&($b['d']<60))  $service_price=serv_get (20436,1,$service_price,'service');
                elseif ($b['d']>=60)  $service_price=serv_get (20438,1,$service_price,'service');
            }
        }
        foreach ($p['operations']['face']['oval'] as $b)
        {
            
            if ($b['x1']==$b['x2'])
            {
                $x=$b['x1'];
                if($b['y1']>$b['y2'])
                {
                    $y=$b['y2']+($b['y1']-$b['y2'])/2;
                    $len= $b['y1']-$b['y2']+60;
                }
                else 
                {
                    $y=$b['y1']+($b['y2']-$b['y1'])/2;
                    $len= $b['y2']-$b['y1']+60;

                }
                $put_xnc.='&lt;tool name=&quot;millS&quot; d=&quot;1&quot;/&gt;&lt;mr x=&quot;'.$x.'&quot; y=&quot;'.$y.'&quot; dp=&quot;dz+3&quot; in=&quot;0&quot; out=&quot;0&quot; sxy=&quot;tool.dia/2&quot; fwd=&quot;true&quot; l=&quot;'.$len.'&quot; w=&quot;60&quot; a=&quot;90&quot; r=&quot;30&quot; c=&quot;0&quot; name=&quot;millS&quot;/&gt;';
                
            }
            elseif ($b['y1']==$b['y2'])
            {
                $y=$b['y1'];
                if($b['x1']>$b['x2'])
                {
                    $x=$b['x2']+($b['x1']-$b['x2'])/2;
                    $len= $b['x1']-$b['x2']+60;
                }
                else 
                {
                    $x=$b['x1']+($b['x2']-$b['x1'])/2;
                    $len= $b['x2']-$b['x1']+60;

                }

                $put_xnc.='&lt;tool name=&quot;millS&quot; d=&quot;1&quot;/&gt;&lt;mr x=&quot;'.$x.'&quot; y=&quot;'.$y.'&quot; dp=&quot;dz+3&quot; in=&quot;0&quot; out=&quot;0&quot; sxy=&quot;tool.dia/2&quot; fwd=&quot;true&quot; l=&quot;'.$len.'&quot; w=&quot;60&quot; a=&quot;0&quot; r=&quot;30&quot; c=&quot;0&quot; name=&quot;millS&quot;/&gt;';
                
            }
            $service_price=serv_get (76997,1,$service_price,'service');
        }
        unset($empty);
        if (!isset($put_xnc)) 
        {
            $put_xnc="&lt;com comment=&quot;empty&quot;/&gt;&lt;msg msg=&quot;'нет сложных обработок'&quot; type=&quot;info&quot;/&gt;";
            $empty=1;
        }
        
        $xnc_in=Array
        (
            "tag" => "OPERATION",
            "type" => "open",
            "level" => 2,
        );
        if ($empty==1)$xnc_in['attributes']['operation.glass_empty']=1;
        $xnc_in['attributes'][mb_strtoupper('bySizeDetail')]='true';
        $xnc_in['attributes'][mb_strtoupper('mirHor')]='false';
        $xnc_in['attributes'][mb_strtoupper('mirVert')]='true';
        $xnc_in['attributes'][mb_strtoupper('typeId')]='XNC';
        $xnc_in['attributes'][mb_strtoupper('side')]='true';
        $xnc_in['attributes'][mb_strtoupper('typename')]='Деталь id='.$ar_in['ID'];
        $xnc_in['attributes'][mb_strtoupper('printable')]='true';
        $xnc_in['attributes'][mb_strtoupper('startNewPage')]='true';
        $xnc_in['attributes'][mb_strtoupper('code')]=mt_rand(0,1111111);
        $xnc_in['attributes'][mb_strtoupper('turn')]=0;
        $xnc_in['attributes']['ID']=new_id($vals)+1;
        $parts[$k]=$xnc_in['attributes']['ID'];
        $xnc_in['attributes'][mb_strtoupper('program')]=htmlspecialchars_decode("&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&lt;program dx=&quot;".$ar_in['DL']."&quot; dy=&quot;".$ar_in['DW']."&quot; dz=&quot;18&quot;&gt;".$put_xnc."&lt;/program&gt;");
        $vv=get_vals($xnc_in['attributes'][mb_strtoupper('program')]);
        $i1=make_index($vv);

        unset ($tool);
        foreach ($i1['TOOL'] as $t)
        {
            if (!isset($tool[$vv[$t]['attributes']['NAME']]))
            {
                $tool[$vv[$t]['attributes']['NAME']]=$vv[$t];
                
            }
            unset($vv[$t]);
        }

        foreach ($tool as $t)
        {
            $vv=move_vals($vv,1,1);
            $vv[1]=$t;
            ksort ($vv);
        }
        $xnc_in['attributes'][mb_strtoupper('program')]=vals_index_to_project($vv);
        $xnc_in_part=[
            'tag'=>'PART','type'=>'complete','level'=>3,'attributes'=>array(
            'ID'=>$ar_in['ID'])
            ];
        $xnc_in2=Array
        (
            "tag" => "OPERATION",
            "type" => "close",
            "level" => 2,
        
        );
        $vals=vals_into_end($vals,$xnc_in);
        ksort($vals);
        $vals=vals_into_end($vals,$xnc_in_part);

        $vals=vals_into_end($vals,$xnc_in2);
        
    }
    $ar_to_vals=['tag'=>'GlASS','type'=>'complete','level'=>2,'attributes'=>array(
        'DATA'=>base64_encode(serialize($ar)))
        ];
    $vals=vals_into_end($vals,$ar_to_vals);

    unset ($req);
    foreach ($material_price as $m)
    {
        $req[]=$m['material_code'];
    }
    foreach ($service_price as $m)
    {
        $req[]=$m['service_code'];
    }

    
    $serv_get=get_actual_price($req, 1, $client);
    if (count($serv_get)>0)
    {
        foreach ($serv_get as $c_srvvv=>$srvvv)
        {
            if($srvvv["PRICE"]>0) 
            {
                foreach ($serice_price as $s)
                {
                    if ($s['service_code']==$c_srvvv)
                    {
                        $s['service_price']==$srvvv["PRICE"];
                    }
                }
                if ($c_srvvv==$material_price[0]['material_code']) $material_price[0]['sheet_price']=$srvvv["PRICE"];
            }
        }
    }
    else
    {
        foreach ($service_price as $k=>$v)
        {
            $sql="SELECT * FROM SERVICE_PRICE WHERE SERVICE_ID=".$v["service_id"]." AND PLACE_ID=1;";
            $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
            $sql5=check_price($sql4,"SERVICE",$link,$v["service_id"]);
            if ($sql5["PRICE"]>0)$service_price[$k]['service_price']=$sql5["PRICE"];
            else $service_price[$k]['service_price']=0.01;
        }
        $sql="SELECT * FROM MATERIAL_PRICE WHERE MATERIAL_ID=".$mat['MATERIAL_ID']." AND PLACE_ID=1;";
        $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        $sql5=check_price($sql4,"MATERIAL",$link,$mat["MATERIAL_ID"]);
        if ($sql5["PRICE"]>0)$material_price[0]['sheet_price']=$sql5["PRICE"];
        else $material_price[0]['sheet_price']=0.01;
    }
    if (($material_price[0]['sheet_price']==0)||(!isset($material_price[0]['sheet_price'])))$material_price[0]['sheet_price']=0.01;
    foreach ($service_price as $k=>$v)
    {
        if(($service_price[$k]['service_price']==0.01)||(!isset($service_price[$k]['service_price'])))$service_price[$k]['service_price']=0.01;
    }

    return (['parts'=>$parts,'project'=> vals_index_to_project($vals),'vals'=>$vals,'material_price'=>$material_price,
    'service_price'=>$service_price,'part_price'=>$part_price,'simple_price'=>$simple_price]);
   

}

/**
 * 
**/
function get_glass_arr($project)
{
    $vals = get_vals($project);
    $index = make_index($vals);
    foreach($index['GLASS'] as $i) {
        if (isset($vals[$i]['attributes']['DATA'])) return unserialize(base64_decode($vals[$i]['attributes']['DATA']));
    }
}

?>