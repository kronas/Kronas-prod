<?php

/**
 * 
**/
function xml_echo ($v)
{
    header('Content-type: text/xml');
    echo $v;
    exit;
}

/**
 * 
**/
function xml_html ($vals)
{
    echo (vals_index_to_project($vals));
    exit;
}

/**
 * 
**/
function xml ($vals)
{
    header('Content-type: text/xml');
    echo vals_index_to_project($vals);
    exit;
}

/**
 * Функция удаляет атрибуты которые нужны для пересчётов,
 * но не нужны в конечном файле проекта (ну либо перестают быть нужными в какой-то момент)
**/
function my_uncet($a, $m)
{
    unset (
        $a['attributes'][$m],
        $a['attributes']['part.' . $m],
        $a['attributes'][mb_strtoupper($m)],
        $a['attributes'][mb_strtoupper('part.' . $m)]
    );
    return ($a);
}

/**
 * 
**/
function move_vals($vals,$start,$num)
{
    foreach($vals as $k => $v) {
	    if ($k >= $start) {
	        $vals1[$k+$num] = $vals[$k];
	        unset($vals[$k]);
	    } else $vals1[$k] = $vals[$k];
   	}
	return $vals1;
}

/**
 * 
**/
function delDir($dir)
{
    $files = array_diff(scandir($dir), ['.', '..']);
    foreach ($files as $file) {
        (is_dir($dir . '/' . $file)) ? delDir($dir . '/' . $file) : unlink($dir . '/' . $file);
    }
    return rmdir($dir);
}

/**
 * 
**/
function name_file($name)
{
    $n = explode('.', $name);
    $n[0] = rus2translit(substr(htmlspecialchars($n[0]), 0, 100));
    return implode('.',$n);
}

/**
 * 
**/
function my_copy_all($from, $to)
{
    if (is_dir($from)) {
        @mkdir($to, 0777, true);
        $d = dir($from);
        while (false !== ($entry = $d->read())) {
            if ($entry == "." || $entry == "..") continue;
            my_copy_all("$from/$entry", "$to/$entry");
        }
        $d->close();
    }
    else copy($from, $to);
}

/**
 * 
**/
function op_xnc_check_copy($v1)
{
    $i1 = make_index($v1);

    foreach($i1['OPERATION'] as $i) {
        if (($v1[$i]['attributes']['TYPEID'] == 'XNC')) $t++;
    }

    if ($t > 0) return true;
    else return false;
}

/**
 * 
**/
function del_filter_files($dir, $order_id)
{
    $files = array_diff(scandir($dir), ['.', '..']);
    foreach ($files as $file) {
        $pos = strpos($file, $order_id . '_');
        if ($pos !== false) {
            unlink($dir . $file);
        }
    }
    return rmdir($dir);
}
/**
 * 
**/
function del_part($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);

    foreach ($index["PART"] as $i) {
        if ($vals[$i]["attributes"]["ID"] == $id) {
            unset ($vals[$i]);
        }
    }

    ksort($vals);
    return ($vals);
}

/**
 * 
**/
function part_place_in_cs($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    foreach ($index["OPERATION"] as $i) {
        if ($vals[$i]["attributes"]["TYPEID"] == 'CS') {
            $g = $i + 2;
            $j = 0;
            
            while ($vals[$g]["tag"] == "PART") {
                $j++;
                if ($vals[$g]["attributes"]["ID"] == $id) {
                    return $j;
                }
                $g++;
            }

        }
    }
}

/**
 * 
**/
function get_vals_index($project_data) 
{
    $project_data = xml_parser($project_data);

    $p = xml_parser_create();
    xml_parse_into_struct($p, $project_data, $vals, $index);
    xml_parser_free($p);

    $_SESSION['vals'] = $vals;
    $_SESSION['index'] = $index;
    return [
        'vals' => $vals,
        'index' => $index
    ];
}

/**
 * 
**/
function ready_project($vals)
{
    $q = vals_index_to_project($vals);
    $q = gl_55(__LINE__,__FILE__,__FUNCTION__, $q);
    $q = gl_recalc(__LINE__,__FILE__,__FUNCTION__, $q );

    $new_vals = get_vals_index_without_session($q);
    $vals = vals_out($new_vals['vals']);
    ksort($vals);
    $vals = add_tag_print($vals);
    
    return $vals;
}

/**
 * 
**/
function p_part($vals)
{
    $vals = vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    foreach ($index['PART'] as $i)
    {
        if ($vals[$i]['attributes']['CL']>0)
        {
            echo 'id='.$vals[$i]['attributes']['ID'].'<hr>';
            foreach ($vals[$i]['attributes'] as $k=>$v)
            {
                if (substr_count($k,'MY_')>0) echo '<br>'.$k.'=>'.$v.'<br>';
            }
            echo '<hr>';

        } 
    }
}

/**
 * 
**/
function del_part_in_el_op($vals, $id, $id_op)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);

    foreach ($index["OPERATION"] as $i) {
        if (($vals[$i]["attributes"]["TYPEID"] == "EL") && ($vals[$i]["attributes"]["ID"] == $id_op)) {
            $j = $i + 2;

            while ($vals[$j]["tag"] == "PART") {
                if ($vals[$j]["attributes"]["ID"] == $id) unset ($vals[$j]);
                $j++;
            }

        }
    }

    ksort($vals);
    return ($vals);
}

/**
 * 
**/
function serv_get ($id,$count,$arr,$type)
{
	$upper_type = mb_strtoupper($type);
    $sql = "SELECT * FROM $upper_type WHERE `CODE` = $id";
    $s = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
    if (count($s) == 1) {
        unset($in);
        foreach ($arr as $k => $a) {
            if ($id == $a['service_id']) {
                $arr[$k]['service_count'] += $count;
                $in = 1;
            }
        }
        if (!isset($in))
        $arr[] = [
            $type.'_id' => $s[$upper_type.'_ID'],
            $type.'_code' => $s['CODE'],
            $type.'_name' => $s['NAME'],
            $type.'_count' => $count,
            $type.'_unit' => $s['UNIT'],
            $type.'_price' => 0,
        ];
        return $arr;
    }
    else return $arr;    
}

/**
 * 
**/
function st_from_akcent($vals,$place)
{
    $vals = vals_out($vals);
    ksort($vals);
    $index = make_index($vals);
    foreach ($index["GOOD"] as $i) {
        if (($vals[$i]["attributes"]["TYPEID"] == "band")
        	&& ($vals[$i]["attributes"]["CODE"] > 0)
        	&& (is_numeric($vals[$i]["attributes"]["CODE"]) && ($vals[$i]["attributes"]["MY_1C_NOM"] <> 6419))
        	|| ($vals[$i]["attributes"]["TYPEID"] == "sheet")
        	&& ($vals[$i]["attributes"]["CODE"] > 0)
        	&& (is_numeric($vals[$i]["attributes"]["CODE"]) && ($vals[$i]["attributes"]["MY_1C_NOM"] <> 5931)))
        {
            $codes[] = $vals[$i]["attributes"]["CODE"];
        }
    }
    return get_material_count($codes, $place);
}

/**
 * 
**/
function part_place_in_product($vals,$id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    foreach ($index["GOOD"] as $i) {
        if ($vals[$i]["attributes"]["TYPEID"] == "product") {
            $g = $i + 1;
            $j = 0;
            
            while ($vals[$g]["tag"] == "PART") {
                $j++;
                if ($vals[$g]["attributes"]["ID"] == $id) {
                    return $j;
                    exit;
                }
                $g++;
            }
        }
    }
    return null;
}

/**
* Функция парсит "project_data" формата .xml в массив $vals
* Параметры: project_data
**/
function get_vals($project_data)
{
    $project_data = xml_parser($project_data);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project_data, $vals, $index);
    xml_parser_free($p);

    $vals = vals_out($vals);
    $vals = add_tag_print($vals);
    return $vals;
}

/**
 * 
**/
function op_print($project)
{
    $project = xml_parser($project);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project, $v1, $i1);
    xml_parser_free($p);
    
    $v1 = vals_out($v1);
    ksort($v1);
    $i1 = make_index($v1);

    foreach($i1['OPERATION'] as $i111) {
        if ($v1[$i111]['type'] == 'open') $v1[$i111]['attributes']['PRINTABLE'] = 'true';
    }

    return vals_index_to_project($v1);
}

/**
 * 
**/
function part_get_edges($vals,$id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    $a = array("L","R","B","T");
    
    foreach ($index["PART"] as $i1) {
        if(($vals[$i1]["attributes"]["ID"] == $id) && ($vals[$i1]["attributes"]["L"] > 0)) {
            
            foreach ($a as $a1) {
                if(($vals[$i1]["attributes"]["EL".$a1] <> "") && ($vals[$i1]["attributes"]["EL".$a1])) {
                    $op_num = substr(($vals[$i1]["attributes"]["EL".$a1]), strpos(($vals[$i1]["attributes"]["EL".$a1]), "#") + 1);

                    foreach ($index["OPERATION"] as $i) {
                        if (($vals[$i]["attributes"]["TYPEID"] == "EL") && ($vals[$i]["attributes"]["ID"] == $op_num)) {
                            
                            foreach ($index["GOOD"] as $d) {
                                if (($vals[$d]["attributes"]["TYPEID"] == "band")
                                	&&($vals[$d]["attributes"]["ID"] == $vals[$i+1]["attributes"]["ID"]))
                                {
                                    $res[$a1] = $vals[$d];
                                    $res[$a1]['attributes']['ELCOLOR'] = $vals[$i]['attributes']['ELCOLOR'];
                                    $res[$a1]['attributes']['EL'] = $vals[$i]['attributes']['MY_EL_TYPE'];
                                    $res[$a1]['attributes']['ELSYMBOL'] = $vals[$i]['attributes']['ELSYMBOL'];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return $res;
}

/**
 * 
**/
function op_xnc_check($project)
{
    $project = xml_parser($project);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project, $v1, $i1);
    xml_parser_free($p);

    $v1 = vals_out($v1);
    ksort($v1);
    $i1 = make_index($v1);
    $v1 = add_tag_print($v1);
    
    foreach($i1['OPERATION'] as $i111) {
        if (($v1[$i111]['attributes']['TYPEID'] == 'XNC')
            && (!isset($v1[$i111]["attributes"]['operation.my_mpr_in']))
            && (!isset($v1[$i111]["attributes"][mb_strtoupper('operation.my_mpr_in')])))
        {
            $j111 = $i111;
            $t = 0;
            
            while (($v1[$j111]['tag'] <> "PART") && ($t < 10)) {
                $j111++;
                $t++;
            }
            unset ($t);

            while (($v1[$j111]['tag'] == 'PART') && ($t < 10)) {
              $op_xnc[$v1[$i111]['attributes']['ID']] = $v1[$i111]['attributes']['ID'];
              $j111++;
              $t++;
            } 
            unset ($t);
        }
    }
    
    if (isset($op_xnc)) return $op_xnc;
    else return false;
}

/**
 * 
**/
function part_get_material($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);

    foreach ($index["OPERATION"] as $i) {
        if ($vals[$i]["attributes"]["TYPEID"] == 'CS') {
            $g = $i + 2;

            while ($vals[$g]["tag"] == "PART") {
                if ($vals[$g]["attributes"]["ID"] == $id) {
                    
                    foreach ($index["GOOD"] as $r) {
                        if (($vals[$r]["attributes"]["TYPEID"] == "sheet")
                        	&& ($vals[$r]["attributes"]["ID"] == $vals[$i + 1]["attributes"]["ID"]))
                        {
                            $t = $r + 1;
                            while ($vals[$t]["tag"] == "PART") {
                                if ($vals[$t]['attributes']['L'] > $vals[$r]['attributes']['L']) {
                                	$vals[$r]['attributes']['L']=$vals[$t]['attributes']['L'];
                                }

                                if ($vals[$t]['attributes']['W'] > $vals[$r]['attributes']['W']) {
                                	$vals[$r]['attributes']['W']=$vals[$t]['attributes']['W'];
                                }

                                $t++;
                            }
                            return $vals[$r];
                        }
                    }
                }
                $g++;
            }
        }
    }
}

/**
 * 
**/
function part_get_tool($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);

    foreach ($index["OPERATION"] as $i) {
        if ($vals[$i]["attributes"]["TYPEID"] == 'CS') {
            $g = $i + 2;

            while ($vals[$g]["tag"] == "PART") {
                if ($vals[$g]["attributes"]["ID"] == $id) {
                    
                    foreach ($index["GOOD"] as $r) {
                        if (($vals[$r]["attributes"]["TYPEID"] == "tool.cutting")
                        	&&($vals[$r]["attributes"]["ID"] == $vals[$i]["attributes"]["TOOL1"]))
                        {
                            return $vals[$r];
                        }
                    }
                }
                $g++;
            }
        }
    }
}

/**
 * 
**/
function part_get_xnc($vals, $id)
{
    $vals = vals_out($vals);
    ksort($vals);
    $index = make_index($vals);

    foreach ($index["OPERATION"] as $i) {
        if (($vals[$i]["attributes"]["TYPEID"] == "XNC") && ($vals[$i]['type'] <> 'close')) {
            $o = $i;

            while (($vals[$o]['tag'] <> "PART") && (isset($vals[$o]))) {
                $o++;
            }

            while ($vals[$o]["tag"] == "PART") {
                if ($vals[$o]["attributes"]["ID"] == $id) {
                    $res[$i] = $vals[$i];
                }

                $o++;
            }
        }
    }
    return $res;
}

/**
 * 
**/
function part_get_gr($vals,$id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);

    foreach ($index["PART"] as $i) {
        if (($vals[$i]['attributes']['CL'] > 0) && ($vals[$i]['attributes']['ID'] == $id)) {
            
            foreach (SIDE as $s) {
                if (isset($vals[$i]['attributes']['GR'.mb_strtoupper($s)])) {
                    $op_gr[$s] = substr($vals[$i]['attributes']['GR'.mb_strtoupper($s)], 11);
                }
            }

        }
    }

    foreach (SIDE as $s) {

        foreach ($index["OPERATION"] as $i) {
            if (($vals[$i]["attributes"]["TYPEID"] == "GR") && ($vals[$i]["attributes"]["ID"] == $op_gr[$s])) {
                $res[$s]=$vals[$i];
            }
            
        }

    }
    return $res;
}

/**
 * 
**/
function mac_ch_calc_ld($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start)
{
    for ($new_cx = $cx - $r_d; $new_cx < $cx; $new_cx += $r_d2) {

        for ($new_cy = $cy - $r_d; $new_cy < $cy; $new_cy += $r_d2) {
        	$prev_x = $v1[$rr - 1]['attributes']['X'];
        	$prev_y = $v1[$rr - 1]['attributes']['Y'];
        	$this_x = $v1[$rr]['attributes']['X'];
        	$this_y = $v1[$rr]['attributes']['Y'];
            $r = cx_cy_calc($prev_x, $prev_y, $new_cx, $new_cy, $this_x, $this_y);

            if ($r['res'] == true) {
                $rrr[] = [
                    'new_cx' => $new_cx,
                    'new_cy' => $new_cy,
                    'r1' => $r['r1'],
                    'r2' => $r['r2']
                ];
            }
        }
    }

    $st_delt = $rrr_start['r2'] - $rrr[0]['r2'];
    $key = 0;

    foreach ($rrr as $f => $ff) {
        $delt = $rrr_start['r2'] - $ff['r2'];
        if ($delt < $st_delt) {
            $key = $f;
            $st_delt = $delt;
        }
    }
    return $rrr[$key];
}

/**
 * 
**/
function mac_ch_calc_lm($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start)
{
    for ($new_cx = $cx - $r_d; $new_cx < $cx; $new_cx = $new_cx + $r_d2) {
        for ($new_cy = $cy; $new_cy < $cy + $r_d; $new_cy = $new_cy + $r_d2) {
            $prev_x = $v1[$rr - 1]['attributes']['X'];
        	$prev_y = $v1[$rr - 1]['attributes']['Y'];
        	$this_x = $v1[$rr]['attributes']['X'];
        	$this_y = $v1[$rr]['attributes']['Y'];
            $r = cx_cy_calc ($prev_x, $prev_y, $new_cx, $new_cy, $this_x, $this_y);
            if ($r['res'] == true) {
                $rrr[] = [
                    'new_cx' => $new_cx,
                    'new_cy' => $new_cy,
                    'r1' => $r['r1'],
                    'r2' => $r['r2']
                ];
            }
        }
    }

    $st_delt = $rrr_start['r2'] - $rrr[0]['r2'];
    $key = 0;

    foreach ($rrr as $f => $ff) {
        $delt = $rrr_start['r2'] - $ff['r2'];
        if ($delt < $st_delt) {
            $key = $f;
            $st_delt = $delt;
        }
    }
    return $rrr[$key];
}

/**
 * 
**/
function mac_ch_calc_center($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start)
{
    for ($new_cx = $cx; $new_cx < $cx + $r_d; $new_cx = $new_cx + $r_d2) {

        for ($new_cy = $cy; $new_cy < $cy + $r_d; $new_cy = $new_cy + $r_d2) {
        	$prev_x = $v1[$rr - 1]['attributes']['X'];
        	$prev_y = $v1[$rr - 1]['attributes']['Y'];
        	$this_x = $v1[$rr]['attributes']['X'];
        	$this_y = $v1[$rr]['attributes']['Y'];
            $r = cx_cy_calc($prev_x, $prev_y, $new_cx, $new_cy, $this_x, $this_y);
            if ($r['res'] == true) {
                $rrr[] = [
                    'new_cx' => $new_cx,
                    'new_cy' => $new_cy,
                    'r1' => $r['r1'],
                    'r2' => $r['r2']
                ];
            }
        }

    }

    $st_delt = $rrr_start['r2'] - $rrr[0]['r2'];
    $key = 0;

    foreach ($rrr as $f => $ff) {
        $delt = $rrr_start['r2'] - $ff['r2'];
        if ($delt < $st_delt) {
            $key = $f;
            $st_delt = $delt;
        }
    }
    return $rrr[$key];
}

/**
 * 
**/
function mac_ch_calc_center_down($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start)
{
    for ($new_cx = $cx; $new_cx < $cx + $r_d; $new_cx = $new_cx + $r_d2) {

        for ($new_cy = $cy - $r_d; $new_cy < $cy; $new_cy = $new_cy + $r_d2) {
        	$prev_x = $v1[$rr - 1]['attributes']['X'];
        	$prev_y = $v1[$rr - 1]['attributes']['Y'];
        	$this_x = $v1[$rr]['attributes']['X'];
        	$this_y = $v1[$rr]['attributes']['Y'];
            $r = cx_cy_calc ($prev_x, $prev_y, $new_cx, $new_cy, $this_x, $this_y);
            if ($r['res'] == true) {
                $rrr[] = [
                    'new_cx' => $new_cx,
                    'new_cy' => $new_cy,
                    'r1' => $r['r1'],
                    'r2' => $r['r2']
                ];
            }
        }
    }

    $st_delt = $rrr_start['r2'] - $rrr[0]['r2'];
    $key = 0;

    foreach ($rrr as $f=>$ff) {
        $delt = $rrr_start['r2'] - $ff['r2'];
        if ($delt < $st_delt) {
            $key = $f;
            $st_delt = $delt;
        }
    }
    return $rrr[$key];
}

/**
 * 
**/
function mac_ch($v1,$rr,$part,$r_d,$r_d2,$op)
{
	$prev_x = $v1[$rr - 1]['attributes']['X'];
	$prev_y = $v1[$rr - 1]['attributes']['Y'];
	$this_x = $v1[$rr]['attributes']['X'];
	$this_y = $v1[$rr]['attributes']['Y'];
	$cx = $v1[$rr]['attributes']['CX'];
    $cy = $v1[$rr]['attributes']['CY'];
    $r = cx_cy_calc($prev_x, $prev_y, $cx, $cy, $this_x, $this_y);

    $ret = sqrt($part['el']['b'] * $part['el']['b'] + $part['el']['l'] * $part['el']['l']);
    $rrr_start=[
        'cx' => $cx,
        'cy' => $cy,
        'r1' => $r['r1'],
        'r2' => $r['r2'] - $ret
    ];

    if ($op == '-') {
        $rrr = mac_ch_calc_lm($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
        
        if (!isset($rrr)) $rrr = mac_ch_calc_ld($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
        if (!isset($rrr)) $rrr = mac_ch_calc_center($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
        if (!isset($rrr)) $rrr = mac_ch_calc_center_down($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
    } elseif ($op == '+') {
        $rrr = mac_ch_calc_center($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);

        if (!isset($rrr)) $rrr = mac_ch_calc_center_down($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
        if (!isset($rrr)) $rrr = mac_ch_calc_lm($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
        if (!isset($rrr)) $rrr = mac_ch_calc_ld($cx, $r_d, $r_d2, $cy, $v1, $rr, $rrr_start);
    }

    if (isset($rrr)) return $rrr;
    else return 1;
}

/**
 * 
**/
function cx_cy_calc($x, $y, $cx, $cy, $x1, $y1)
{
    if ($cx>$x) $dx=$cx-$x;
    elseif($cx<$x) $dx=$x-$cx;

    if ($cx > $x1) $dx1 = $cx - $x1;
    elseif ($cx < $x1) $dx1 = $x1 - $cx;

    if ($cy > $y) $dy = $cy - $y;
    elseif ($cy < $y) $dy = $y - $cy;

    if ($cy > $y1) $dy1 = $cy - $y1;
    elseif ($cy < $y1) $dy1 = $y1 - $cy;

    $r1 = sqrt($dx * $dx + $dy * $dy);
    $r2 = sqrt($dx1 * $dx1 + $dy1 * $dy1);
    $res['r1'] = $r1;
    $res['r2'] = $r2;

    if (round($r1, 2) == round($r2, 2)) {
        $res['res'] = true;
        return $res;
    } else return $res;
}

/**
 * 
**/
function get_type_double($w, $l, $link)
{
    $sql = "SELECT * FROM `DOUBLE_TYPE` WHERE `minW` <= " . $w . " AND minL <= " . $l;
    $connn = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'];
    foreach ($connn as $conn) {
        $arr[] = $conn;
    }

    foreach ($arr as $item) {
        $out .= '<option value="' . $item['DOUBLE_TYPE_ID'] . '"  data-description="' . $item['DESCRIPTION'] . '" data-same="' . $item['SAME'] . '" data-edge="' . $item['EDGE'] . '"  data-ell="' . $item['ell'] . '"  data-elr="' . $item['elr'] . '"  data-elt="' . $item['elt'] . '" data-elb="' . $item['elb'] . '">' . $item['NAME'] . '</option>';
    }

    return $out;
}

/**
 * 
**/
function get_material_double($vals, $code)
{
    
    $vals = vals_out($vals);
    $index = make_index($vals);

    foreach ($index['GOOD'] as $i) {
        if (($vals[$i]['attributes']['TYPEID'] == 'sheet')
        	&&(isset($vals[$i]['attributes']['CODE']))
        	&&($vals[$i]['attributes']['T'] < 28))
        {
            if ($vals[$i]['attributes']['CODE'] == $code) $selected = 'selected';
            else $selected = '';
            
            $materials .= '<option ' . $selected . ' value="' . $vals[$i]['attributes']['ID'] . '" data-t="' . $vals[$i]['attributes']['T'] . '" data-code="' . $vals[$i]['attributes']['CODE'] . '">' . $vals[$i]['attributes']['NAME'] . '</option>';
        }
    }
    return $materials;
}

/**
 * 
**/
function get_month_name($code)
{
    $month_name = '';
    switch($code) {
        case '01':
            $month_name = 'January';
            break;
        case '02':
            $month_name = 'February';
            break;
        case '03':
            $month_name = 'March';
            break;
        case '04':
            $month_name = 'April';
            break;
        case '05':
            $month_name = 'May';
            break;
        case '06':
            $month_name = 'June';
            break;
        case '07':
            $month_name = 'July';
            break;
        case '08':
            $month_name = 'August';
            break;
        case '09':
            $month_name = 'September';
            break;
        case '10':
            $month_name = 'October';
            break;
        case '11':
            $month_name = 'November';
            break;
        case '12':
            $month_name = 'December';
    }
    return $month_name;
}

/**
 * 
**/
function rus2translit($string)
{
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        '/' => '-'
    );
    return strtr($string, $converter);
}

/**
 * 
**/
function str2url($str)
{
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
}

/**
 * 
**/
function get_thikness_edge($vals, $id, $side)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);

    foreach ($index["PART"] as $i) {
        if (($vals[$i]['attributes']['CL'] > 0) && ($vals[$i]['attributes']['ID'] == $id)) {
            if ($side == "dl") $ar = array('L', 'R');
            elseif ($side == "dw") $ar = array('T', 'B');

            foreach ($ar as $s) {

                foreach($index["OPERATION"] as $j) {
                    if (($vals[$j]['attributes']['TYPEID'] == "EL")
                    	&&($vals[$i]['attributes']['EL'.$s] == '@operation#'.$vals[$j]['attributes']['ID']))
                    {
                        
                        foreach ($index['GOOD'] as $k) {
                            if (($vals[$k]['attributes']['TYPEID'] == "band")
                            	&&($vals[$k]['attributes']['ID'] == $vals[$j+1]['attributes']['ID']))
                            {
                                $vals[$j]['attributes']['T'] = $vals[$k]['attributes']['T'];
                            }
                        }

                        foreach ($index['GOOD'] as $k) {
                            if (($vals[$k]['attributes']['TYPEID'] == "tool.edgeline")
                            	&&($vals[$k]['attributes']['ID'] == $vals[$j]['attributes']['TOOL1']))
                            {
                                $thikness += $vals[$j]['attributes']['T'] - $vals[$k]['attributes'][mb_strtoupper('elWidthPreJoint')];
                            }
                        }

                    }
                }

            }

        }
    }
    return $thikness;
}

/**
 * 
**/
function get_thikness_edge_side($vals, $id, $side)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);

    foreach ($index["PART"] as $i) {
        if (($vals[$i]['attributes']['CL'] > 0) && ($vals[$i]['attributes']['ID'] == $id)) {
            if ($side == "l") $ar = array('L');
            elseif ($side == "r") $ar = array('R');
            elseif ($side == "b") $ar = array('B');
            elseif ($side == "t") $ar = array('T');

            foreach ($ar as $s) {

                foreach($index["OPERATION"] as $j) {
                    if (($vals[$j]['attributes']['TYPEID'] == "EL")
                    	&&($vals[$i]['attributes']['EL'.$s] == '@operation#'.$vals[$j]['attributes']['ID']))
                    {
                        
                        foreach ($index['GOOD'] as $k) {
                            if (($vals[$k]['attributes']['TYPEID'] == "band")
                            	&&($vals[$k]['attributes']['ID'] == $vals[$j+1]['attributes']['ID']))
                            {
                                $vals[$j]['attributes']['T'] = $vals[$k]['attributes']['T'];
                            }
                        }

                        foreach ($index['GOOD'] as $k) {
                            if (($vals[$k]['attributes']['TYPEID'] == "tool.edgeline")
                            	&&($vals[$k]['attributes']['ID'] == $vals[$j]['attributes']['TOOL1']))
                            {
                                $thikness = $vals[$j]['attributes']['T'] - $vals[$k]['attributes'][mb_strtoupper('elWidthPreJoint')];
                            }
                        }

                    }
                }

            }

        }
    }
    return $thikness;
}

/**
 * 
**/
function mr_change_vals ($vals)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);
    $vals = add_tag_print($vals);

    foreach ($index['OPERATION'] as $i) {
        if ($vals[$i]['attributes']['TYPEID'] == 'XNC') {

            if ((isset($vals[$i]["attributes"]["PROGRAM"])) && ($vals[$i]["attributes"]['operation.my_mpr_in'] <> 1))
            {
                $vals[$i]["attributes"]["PROGRAM"] = str_replace('\\','',$vals[$i]["attributes"]["PROGRAM"]);
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);

                if(mr_change($v1,$vals[$i]) <> false) {
                    $v1 = mr_change($v1, $vals[$i]);
                    $change = 1;
                }

                $vals[$i]["attributes"]["PROGRAM"] = vals_index_to_project($v1);
            }

        }
    }
    if ($change == 1) return $vals;
    else return false;
}

/**
 * 
**/
function add_tag_print($vals)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);

    foreach ($vals as $id => $v) {
        if (isset($v["attributes"])) {
            if ($vals[$id]["tag"] == "PROJECT") $tag = "project.";
            elseif ($vals[$id]["tag"] == "PART") $tag = "part.";
            elseif ($vals[$id]["tag"] == "GOOD") $tag = "good.";
            elseif ($vals[$id]["tag"] == "OPERATION") $tag = "operation.";
            
            foreach ($v["attributes"] as $key => $atr) {
                if (substr_count(mb_strtoupper($key), mb_strtoupper($tag)) > 0) {                
                    unset ($vals[$id]['attributes'][$key]);
                    $vals[$id]['attributes'][mb_strtolower($key)] = $atr;
                } elseif ((substr_count($key,"MY_") > 0)
                	&&(substr_count($key, $tag) == 0)
                	&&(substr_count($key, mb_strtoupper($tag)) == 0))
                {
                    $vals[$id]["attributes"][$tag.mb_strtolower($key)] = $atr;
                }
            }

        }
    }
    return $vals;
}

/**
 * 
**/
function calc_formula_from_db($x,$y)
{
    if (substr_count($x, "-") > 0) {
        $x1 = explode("-", $x);
        $x = $y - $x1[1];
        $z = $x;
    } elseif (($x == 'W') OR ($x == 'L') OR (strlen($x) < 2)) $z = $y;
    elseif(!isset($y)) $z = 0;
    else $z = $x;

    return $z;
}

/**
* Функция очищает "project_data" от мусора.
* Параметры: $project_data - XML-данные проекта.
**/
function xml_parser($project_data)
{
    $pr1 = substr($project_data, 0, strpos($project_data, "description=") + 13);
    $pr2 = substr($project_data, strlen($pr1), strpos($project_data, "\"", strlen($pr1)) - strlen($pr1));
    $pr3 = substr($project_data, strlen($pr1) + strlen($pr2));
    $pr4 = $pr1 . htmlspecialchars($pr2) . $pr3;
    $pr4 = str_replace("qquot", "quot", $pr4);
    $pr4 = str_replace("ququot", "quot", $pr4);
    $pr4 = str_replace("quoquot", "quot", $pr4);
    $pr4 = str_replace("quotquot", "quot", $pr4);
    return $pr4;
}

/**
 * 
**/
function get_vals_index_without_session($project_data, $test = null)
{
    $project_data = xml_parser($project_data);

    $p = xml_parser_create();
    xml_parse_into_struct($p, $project_data, $vals1, $index1);
    xml_parser_free($p);

    return [
        'vals' => $vals1,
        'index' => $index1
    ];
}

/**
 * 
**/
function get_cs_num($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    
    foreach ($index["OPERATION"] as $i) {
        if(($vals[$i]["attributes"]["TYPEID"] == 'CS') && ($vals[$i]["type"] == "open")) {
            if ($vals[$i+1]["attributes"]["ID"] == $id) return $i;
            break;
        }
    }
    return null;
}

/**
 * 
**/
function cs_print_off_for_blank ($v1)
{
    $v1 = vals_out($v1);
    $i1 = make_index($v1);
    ksort($v1);

    foreach ($i1['GOOD'] as $i111) {
    	if (($v1[$i111]['attributes']['TYPEID'] == 'product')
    		&& ($v1[$i111]['type'] == 'open')
    		&& ($v1[$i111+1]['tag'] == 'PART'))
    	{
    		$j111 = $i111 + 1;

    		while ($v1[$j111]['tag'] == 'PART') {
    			$partss[] = $v1[$j111]['attributes']['ID'];
    			$j111++;
    		}

    	}
    }

      foreach ($partss as $tt) {
          if (!in_array($tt, $ttt)) $ttt[] = $tt;
      }

      $partss = $ttt;
      unset($ttt);

    foreach($i1['OPERATION'] as $i111) {
        if (($v1[$i111]['attributes']['TYPEID'] == 'CS') && ($v1[$i111]['type'] == 'open')) {
            $j111 = $i111 + 2;
          while ($v1[$j111]['tag'] == 'PART') {
              if (in_array($v1[$j111]['attributes']['ID'], $partss)) $op_cs = 1;  
              $j111++;
          }

          if ($op_cs == 1) $v1[$i111]['attributes']['PRINTABLE'] = 'true';
          else $v1[$i111]['attributes']['PRINTABLE'] = 'false';

          unset($op_cs);
        }
    }
    
    return $v1;
}

/**
 * 
**/
function cs_clear_data($vals,$id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);
    foreach ($index['OPERATION'] as $i) {
        if (($vals[$i]['attributes']['TYPEID'] == 'CS') && ($vals[$i]['attributes']['ID'] == $id)) {
            unset($vals[$i]['attributes']['DATA'], $vals[$i]['attributes']['DRAW']);
            $j = $i + 2;

            while ($vals[$j]['tag'] == "PART") {

                foreach ($index['PART'] as $p) {
                    if (($vals[$j]['attributes']['ID'] == $vals[$p]['attributes']['ID'])
                        && ($vals[$p]['attributes']['CL'] > 0))
                    {
                        $vals[$p]['attributes']['USEDCOUNT'] = 0;
                    } elseif (($vals[$j]['attributes']['ID'] == $vals[$p]['attributes']['ID'])
                        && ($vals[$p]['attributes']['L'] > 0) && (!isset($vals[$p]['attributes']['CL']))
                        && (!isset($vals[$p]['attributes']['SHEETID'])))
                    {
                        $vals[$p]['attributes']['USEDCOUNT'] = 0;
                    } elseif (($vals[$j]['attributes']['ID'] == $vals[$p]['attributes']['ID'])
                        && ($vals[$p]['attributes']['L'] > 0)
                        && (!isset($vals[$p]['attributes']['CL']))
                        && ($vals[$p]['attributes']['SHEETID'] > 0))
                    {
                        unset ($vals[$p],$vals[$j]);
                    }
                }

                $j++;
            }

        }
    }
    ksort($vals);
    return $vals;
}

/**
 * 
**/
function get_cs_tool__sheet_part($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    foreach ($index["OPERATION"] as $i) {
        if(($vals[$i]["attributes"]["TYPEID"] == 'CS') && ($vals[$i]["type"] == "open")) {
            $j = $i + 2;

            while ($vals[$j]['tag'] == "PART") {
                if ($vals[$j]["attributes"]["ID"] == $id) {
                    $res['cs'] = $vals[$i];
                    $res['cs_index'] = $i;

                    foreach ($index['GOOD'] as $k) {
                        if (($vals[$k]['attributes']['TYPEID'] == 'tool.cutting')
                            && ($vals[$k]['attributes']['ID'] == $vals[$i]['attributes']['TOOL1']))
                        {
                            $res['tool'] = $vals[$k];
                            $res['tool_index'] = $k;
                        } elseif (($vals[$k]['attributes']['TYPEID'] == 'sheet')
                            && ($vals[$k]['attributes']['ID'] == $vals[$i+1]['attributes']['ID']))
                        {
                            $res['sheet'] = $vals[$k];
                            $res['sheet_index'] = $k;
                        }
                    }

                    return $res;
                }
                $j++;
            }
          
        }
    }
}

/**
 * 
**/
function cs_op_check ($project,$cond)
{
    $project = xml_parser($project);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project, $v1, $i1);
    xml_parser_free($p);

    $v1 = vals_out($v1);
    $v1 = add_tag_print($v1);
    ksort($v1);
    $i1 = make_index($v1);

    foreach ($i1['GOOD'] as $i111) {
        if (($v1[$i111]['attributes']['TYPEID'] == 'product') && ($v1[$i111]['type'] == 'open') && ($v1[$i111+1]['tag'] == 'PART')) {
            $j111 = $i111 + 1;

            while ($v1[$j111]['tag'] == 'PART') {
                if ($cond == "need") {     
                    if ($v1[$j111]['attributes']['COUNT'] > $v1[$j111]['attributes']['USEDCOUNT']) {
                        $partss[$v1[$j111]['attributes']['ID']] = $v1[$j111]['attributes']['ID'];
                    }
                } else $partss[$v1[$j111]['attributes']['ID']] = $v1[$j111]['attributes']['ID'];
                $j111++;
            }

        }
    }

    $i1 = make_index($v1);
    
    foreach($i1['OPERATION'] as $key => $io) {
        if ((($v1[$io]['attributes']['TYPEID'] == 'CS')
            && ($v1[$io]['type'] == 'open'))
            && (($v1[$io]['attributes']['operation.my_fix_card'] <> 1) || (($cond == "all2"))))
        {
            unset ($go);

            foreach ($i1['GOOD'] as $j) {
                if (($v1[$j]['attributes']['TYPEID'] == 'sheet') && ($v1[$j]['attributes']['ID'] == $v1[$io+1]['attributes']['ID'])) {
                    $go = 1;
                }
            }

            if ($go > 0) {
                $o = $io + 2;

                while ($v1[$o]['tag'] == 'PART') {
                    if ($cond == "need") {
                        if (isset($partss[$v1[$o]['attributes']['ID']])) $op_cs[$v1[$io]['attributes']['ID']] = $v1[$io]['attributes']['ID'];  
                    } elseif (($cond == "all") OR ($cond == "all2")) {
                        if (isset($partss[$v1[$o]['attributes']['ID']])) $op_cs[$v1[$io]['attributes']['ID']] = $v1[$io]['attributes']['ID'];  
                    }
                    $o++;
                }

            }
        } elseif ((($v1[$io]['attributes']['TYPEID'] == 'CL') && ($v1[$io]['type'] == 'open'))) {
            unset ($go);

            foreach ($i1['GOOD'] as $j) {
                if (($v1[$j]['attributes']['TYPEID'] == 'band') && ($v1[$j]['attributes']['ID'] == $v1[$io+1]['attributes']['ID'])) {
                    $go = 1;
                }
            }

            if ($go > 0) {
                $o = $io + 2;
                
                while ($v1[$o]['tag'] == 'PART') {
                    if (isset($partss[$v1[$o]['attributes']['ID']])) $op_cs[$v1[$io]['attributes']['ID']] = $v1[$io]['attributes']['ID'];  
                    $o++;
                }
            }
        }
    }
    
    if (!isset($op_cs)) return false;
    else return $op_cs;
}

/**
 * 
**/
function pic_get($project_data)
{
    $r = build_req_for_project($project_data, 'xnc_get_pic');
    $pdf_file = gib_serv($r, 'C4F6336B', 'Чертёж', __LINE__,__FILE__,__FUNCTION__, $project_data);

    $tt = mt_rand(111111111,999999999);
    $public_pdf_dir = $GLOBALS['serv_main_dir'] . '/kronasapp/public/users_pdf/' . $tt;
    mkdir($public_pdf_dir . '/jpg', 0777, true);

    foreach ($pdf_file as $k => $v) {
        file_put_contents($public_pdf_dir . '/' . $k, $v);
    }

    $jpg_images = [];

    foreach ($pdf_file as $k=>$file) {
        if(strlen($file) > 3) {
            $jpg_images[substr($k, 0, -4)]['storage'] = $public_pdf_dir . '/' . $k;
            $jpg_images[substr($k, 0, -4)]['public'] = str_replace($GLOBALS['serv_main_dir'] . '/kronasapp/public/', $GLOBALS['laravel_dir'] . '/', $public_pdf_dir . '/' . $k);
        }
    }

    return $jpg_images;
}

/**
 * 
**/
function get_el_tool__sheet_part($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);

    foreach ($index["OPERATION"] as $i) {
        if (($vals[$i]["attributes"]["TYPEID"] == "EL") && ($vals[$i]["type"] == "open")) {
            $j = $i + 2;
            while ($vals[$j]['tag'] == "PART") {
                if ($vals[$j]["attributes"]["ID"] == $id) {
                    $res[$i]['el'] = $vals[$i];
                    $res[$i]['el_index'] = $i;

                    foreach ($index['GOOD'] as $k) {
                        if (($vals[$k]['attributes']['TYPEID'] == 'tool.edgeline')
                        	&& ($vals[$k]['attributes']['ID'] == $vals[$i]['attributes']['TOOL1']))
                        {
                            $res[$i]['tool'] = $vals[$k];
                            $res[$i]['tool_index'] = $k;
                        } elseif (($vals[$k]['attributes']['TYPEID'] == 'band')
                        	&& ($vals[$k]['attributes']['ID'] == $vals[$i+1]['attributes']['ID']))
                        {
                            $res[$i]['band'] = $vals[$k];
                            $res[$i]['band_index'] = $k;
                        }
                    }
                   
                }
                $j++;
            }
          
        }
    }
    return $res;
}

/**
 * 
**/
function get_cl_tool__sheet_part($vals, $id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    foreach ($index["OPERATION"] as $i) {
        if(($vals[$i]["attributes"]["TYPEID"] == "CL") && ($vals[$i]["type"] == "open")) {
            $j = $i + 2;

            while ($vals[$j]['tag'] == "PART") {
                if ($vals[$j]["attributes"]["ID"] == $id) {
                    $res['cl'] = $vals[$i];
                    $res['cl_index'] = $i;

                    foreach ($index['GOOD'] as $k) {
                        if (($vals[$k]['attributes']['TYPEID'] == 'tool.cutting')
                        	&& ($vals[$k]['attributes']['ID'] == $vals[$i]['attributes']['TOOL1']))
                        {
                            $res['tool_cl'] = $vals[$k];
                            $res['tool_cl_index'] = $k;
                        } elseif (($vals[$k]['attributes']['TYPEID'] == 'band')
                        	&& ($vals[$k]['attributes']['ID'] == $vals[$i+1]['attributes']['ID']))
                        {
                            $res['cl_band'] = $vals[$k];
                            $res['cl_band_index'] = $k;
                        }
                    }

                    return $res;
                }
                $j++;
            }
          
        }
    }
}

/**
 * 
**/
function check_project_order_time($order_id)
{
    $request_order = 'SELECT * FROM `ORDER1C` WHERE `ID` = ' . $order_id;
    $data_order = sql_data(__LINE__,__FILE__,__FUNCTION__, $request_order)['data'][0];

    $request_project = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = ' . $order_id . ' ORDER BY `DATE` DESC';
    $data_project = sql_data(__LINE__,__FILE__,__FUNCTION__, $request_project)['data'][0];

    if($data_order['stanki_time'] < $data_project['DATE']) {
        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'check_stanki_time_need_update (order ' . $order_id . ' / ' . $data_order['DB_AC_NUM'] . ' / project_v - ' . $data_project['PROJECT_ID'] . ') ' . $data_order['stanki_time'] . ' - ' . $data_project['DATE'], $_SESSION);
        return 1;
    } else {
        return null;
    }
}

/**
 * 
**/
function generatePDF_specification($_main_dir, $_data, $_dbacid)
{
    $target = $_main_dir . '/files/accent/' . $_dbacid;
    if (!file_exists($target)) {
        mkdir($target, 0777, true);
        if ($GLOBALS['is_devserver'] == true ) {
            chown($target, 'i.kotok:apache');
        } else {
            chown($target, 'gibservice:gibservice');
        }
    }
    $target .= '/';
    $objSpec = generatePDF_getObject_specification($_data);

    $send_specification = array('data' => $objSpec,'target' => $target);
    include_once('pdf-crea.php');
}

/**
 * 
**/
function generatePDF_fullReport($_main_dir, $_data, $_dbacid)
{
    $target = $_main_dir . '/files/accent/' . $_dbacid;
    
    if (!file_exists($target)) {
        mkdir($target, 0777, true);
        if ($GLOBALS['is_devserver'] == true ) chown($target, 'i.kotok:apache');
        else chown($target, 'gibservice:gibservice');
    }

    $target .= '/';
    $_data['project_data'] = gl_55(__LINE__,__FILE__,__FUNCTION__, $_data['project_data']);
    $_data['project_data'] = gl_recalc(__LINE__,__FILE__,__FUNCTION__, $_data['project_data']);
    $r = build_req_for_project($_data['project_data'], '98123591');
    $pdf_file = gib_serv($r, '98123591', 'Получение файла pdf', __LINE__,__FILE__,__FUNCTION__, $_data);
    
    foreach ($pdf_file as $k => $file) {
        file_put_contents($target . 'full_report_' . $k . '.pdf', $file);
    }
    $r = build_req_for_project($_data['project_data'], 'xnc_get_card');
    $pdf_file=gib_serv($r, 'C4F6336B','Получение карт',__LINE__,__FILE__,__FUNCTION__, $_data);

    foreach ($pdf_file as $k => $file) {
        file_put_contents($target . '_XNC_report_' . $k . '.pdf', $file);
    }
}

/**
 * 
**/
function generatePDF_getObject_specification($_data)
{
    $simple_price = $_data['simple_price_extra'];
    $part_price = $_data['part_price'];
    $service_price = $_data['service_price'];
    $band_price = $_data['band_price'];
    $material_price = $_data['material_price'];
    $out = (object) array(
        'table1' => array(),
        'table2' => array(),
        'table3' => array(),
        'table4' => array(),
        'total' => array()
    );

    $full_total = 0;
    $resulttotal = 0;
    
    if (count($material_price) > 0 || count($band_price) > 0) {
        if (count($material_price) > 0) {
            foreach ($material_price as $value) {
                $out->table1[] = array(
                    $value['material_code'],
                    $value['material_name'],
                    $value['sheet_count'],
                    $value['sheet_price'],
                    number_format( $value['sheet_count'] * $value['sheet_price'], 2, ',', ' ')
                );
                $resulttotal = $resulttotal + ($value['sheet_count'] * $value['sheet_price']);
            }
        }

        if (count($band_price) > 0) {
            foreach ($band_price as $value) {
                $out->table1[] = array($value['band_code'],
                    $value['band_name'],
                    $value['band_count'],
                    $value['band_price'],
                    number_format( $value['band_count'] * $value['band_price'], 2, ',', ' ')
                );
                $resulttotal = $resulttotal + ($value['band_count'] * $value['band_price']);
            }
        }
        $out->total[0] = number_format($resulttotal, 2, ',', ' ') . ' грн.';
        $full_total = $full_total + $resulttotal;
    }

    if (count($service_price) > 0) {
        $resulttotal = 0;
        foreach ($service_price as $value) {
            $out->table2[] = array(
                $value['service_code'],
                $value['service_name'] . ' (срок готовности <b><i>' . $_SESSION['res']['serv_time'][$value['service_code']] . '</i></b>) ',
                $value['service_count'] . ' (' . $value['service_unit'] . ')',
                $value['service_price'],
                number_format( $value['service_count'] * $value['service_price'], 2, ',', ' ')
            );
            $resulttotal = $resulttotal + ( $value['service_count'] * $value['service_price']);
        }
        $out->total[1] = number_format($resulttotal, 2, ',', ' ') . ' грн.';
        $full_total = $full_total + $resulttotal;
    }

    if(count($part_price) > 0) {
        $resulttotal = 0;
        foreach ($part_price as $value) {
            $out->table3[] = array(
                'id=' . $value['part_name'],
                $value['material_code'],
                $value['description'],
                $value['count'],
                $value['price'],
                number_format($value['count'] * $value['price'], 2, ',', ' ')
            );
            $resulttotal = $resulttotal + $value['count'] * $value['price'];
        }
        $out->total[2] = number_format($resulttotal, 2, ',', ' ') . ' грн.';
        $full_total = $full_total + $resulttotal;
    }

    if (count($simple_price) > 0) {
        $resulttotal = 0;
        foreach ($simple_price as $value) {
            $out->table4[] = array(
                $value['Code'],
                $value['Name'],
                $value['count'],
                $value['sheet_price'],
                number_format($value['count'] * $value['sheet_price'], 2, ',', ' ')
            );
            $resulttotal = $resulttotal + $value['count'] * $value['sheet_price'];
        }
        $out->total[3] = number_format($resulttotal, 2, ',', ' ') . ' грн.';
        $full_total = $full_total + $resulttotal;
    }

    $out->total[4] = 'Итого: ' . number_format($full_total, 2, ',', ' ') . ' грн.';
    return $out;
}

/**
 * 
**/
function get_all_part($vals,$id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    $res['cs'] = get_cs_tool__sheet_part($vals, $id);
    $res['cl'] = get_cl_tool__sheet_part($vals, $id);
    $res['xnc'] = part_get_xnc($vals, $id);
    $res2 = get_el_tool__sheet_part($vals, $id);

    foreach ($res2 as $r) $res['el'] = $r;

    $res['el']['l'] = get_thikness_edge_side($vals, $id, 'l');
    $res['el']['r'] = get_thikness_edge_side($vals, $id, 'r');
    $res['el']['b'] = get_thikness_edge_side($vals, $id, 'b');
    $res['el']['t'] = get_thikness_edge_side($vals, $id, 't');

    foreach (SIDE as $s) if (!isset($res['el'][$s])) $res['el'][$s] = '0';

    foreach ($index['PART'] as $i) {
        if (($vals[$i]['attributes']['CL'] > 0) && ($vals[$i]['attributes']['ID'] == $id)) {
            $res['part'] = $vals[$i];
            $res['part_index'] = $i;
            $j = $i;

            while ($vals[$j]['tag'] <> 'GOOD') {
                $j--;
                if (($vals[$j]['tag'] == 'GOOD') && ($vals[$j]['attributes']['TYPEID'] == 'product')) {
                    $res['product'] = $vals[$j];
                } elseif (($vals[$j]['tag'] == 'GOOD') && ($vals[$j]['attributes']['TYPEID'] <> 'product')) $vals[$j]['tag'] = 'GOOD';
            }

        }
    }
    return $res;
}

/**
 * 
**/
function get_sheet_num($vals,$id)
{
    $vals = vals_out($vals);
    $index = make_index($vals);
    
    foreach ($index["OPERATION"] as $i) {
        if(($vals[$i]["attributes"]["TYPEID"] == 'CS') && ($vals[$i]["type"] == "open")) {
            $g = $i + 2;
            
            while ($vals[$g]["tag"] == "PART") {
                if ($vals[$g]["attributes"]["ID"] == $id) {

                    foreach ($index["GOOD"] as $e) {   
                        if (($vals[$e]["attributes"]["TYPEID"] == "sheet")
                        	&&($vals[$e]["attributes"]["ID"] == $vals[$i+1]["attributes"]["ID"]))
                        {
                            return $e;
                        }
                    }

                }
                $g++;
            }

        }
    }

}

/**
 * 
**/
function check_price($array, $table, $link, $id)
{
    if (($id > 0) && (is_numeric($id))) {
        if (!isset($array[$table."_ID"])) $array[$table."_ID"] = $id;
        if (!isset($array['PRICE']) OR ($array['PRICE'] == 0)) {
            $sql = "SELECT * FROM " . $table . "_PRICE WHERE " . $table . "_ID = " . $array[$table."_ID"];
            $res = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'];

            foreach ($res as $res2) {
                if ($res2['PRICE'] > 0) $price[] = $res2['PRICE'];
            }

            if (!isset($price)) $price[] = 0.01;
            $array['PRICE'] = max($price);
        }
    }

    return $array;
}

/**
 * 
**/
function get_actual_price($products, $place, $client = '85159')
{
    global $kronas_api_link;
    $req_codes = [];

    foreach($products as $item) {
        $req_codes[] = [
            'code1c' => $item
        ];
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $kronas_api_link . '/gibLabService/getPrices/' . $client . '/' . $place,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => json_encode($req_codes),
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));
    $array = curl_exec($curl);
    $array = json_decode($array, true);
    curl_close($curl);

    foreach ($array as $key => $value) {
        $array[$key]['PRICE'] = $array[$key]['Price'] * (1 - $array[$key]['Discount'] / 100);
        $array[$array[$key]['Art']] = $array[$key];
        unset ($array[$key]);
    }
    return $array;
}

/**
 * 
**/
function get_material_count($codes, $place)
{
    global $kronas_api_link;
    $req_codes = [];

    foreach($codes as $item) {
        $req_codes[] = [
            'code1c' => $item
        ];
    }

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $kronas_api_link . '/gibLabService/getRests/'.$place,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => json_encode($req_codes),
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
      ),
    ));
    $array = curl_exec($curl);
    $array = json_decode($array, true);
    curl_close($curl);

    foreach ($array as $m) {
        $result_array[$m['Art']] = $m['Count'];
    }

    return $result_array;
}

/**
 * 
**/
function quot($text)
{
    $text = str_replace("qquot", "quot", $text);
    $text = str_replace("ququot", "quot", $text);
    $text = str_replace("quoquot", "quot", $text);
    $text = str_replace("quotquot", "quot", $text);
    return $text;
}

/**
 * 
**/
function new_id($vals)
{
    foreach ($vals as $v) {
        if ($v["attributes"]["ID"]) $id[] = $v["attributes"]["ID"];
    }

    return max($id) + 1;
}

/**
 * Функция проверяет наличие криволинейного кромкования по переданному коду кромки
 * если находит, возвращает массив кромки (с id, индексом в проекте, названием кромки и кодом)
 * иначе нул
**/
function check_band_kl($vals, $code)
{

    $index = make_index($vals);
    foreach ($index["GOOD"] as $i) {
        if(($vals[$i]["attributes"]["TYPEID"] == "band") && ($vals[$i]["attributes"]["CODE"] == $code)) {

            foreach ($index["OPERATION"] as $i1) {
                if(($vals[$i1]["attributes"]["TYPEID"] == "EL")
                	&& ($vals[$i1+1]["attributes"]["ID"] == $vals[$i]["attributes"]["ID"]))
                {
                    if($vals[$i]["attributes"]["MY_CHECK_KL"] == 1) {
                        $res = array(
                        	"id" => $vals[$i1]["attributes"]["ID"],
                        	"index" => $i1,
                        	"name" => $vals[$i]["attributes"]["NAME"],
                        	"code" => $vals[$i]["attributes"]["CODE"]
                        );
                        return $res;
                    }
                }
            }

        }
    }
    return null;
}

/**
 * 
**/
function check_band($vals,$code)
{
	$vals =vals_out($vals);
	$index =make_index($vals);
	ksort($vals);

    foreach ($index["GOOD"] as $i) {
        if(($vals[$i]["attributes"]["TYPEID"] == "band") && ($vals[$i]["attributes"]["CODE"] == $code)) {

            foreach ($index["OPERATION"] as $i1) {
                if(($vals[$i1]["attributes"]["TYPEID"] == "EL")
                	&& ($vals[$i1+1]["attributes"]["ID"] == $vals[$i]["attributes"]["ID"]))
                {
                    if(!isset($vals[$i1]["attributes"]["MY_CHECK_KL"])) {
                        $res = array(
                        	"id" => $vals[$i1]["attributes"]["ID"],
                        	"index" => $i1,
                        	"name" => $vals[$i]["attributes"]["NAME"],
                        	"code" => $vals[$i]["attributes"]["CODE"]
                        );
                        return $res;
                    }
                }
            }

        }
    }
    return null;
}

/**
 * 
**/
function vals_check_out($vals)
{
    foreach ($vals as $i => $v) {
        
        foreach ($v["attributes"] as $k => $v1) {
            if (substr_count($k,"MY_CHECK_")) {
                unset ($vals[$i]["attributes"][$k]);
            }
        }

    }
    return ($vals);
}

/**
 * 
**/
function vals_into_end($vals, $array)
{
    $vals = vals_out($vals);
    $k = count($vals) - 1;
    $vals[$k+1] = $vals[$k];
    $vals[$k] = $array;
    ksort($vals);
    return $vals;
}

/**
 * 
**/
function sqlDoInsert ($value, $link, $db)
{
    if ($result = mysqli_query($link, $value)) {
        $id = mysqli_insert_id ($link);
    } else {
        echo "Error: <hr>" . $value . "<br>" . mysqli_error($link);
        mysqli_rollback($link);
        echo "<hr>";
        exit;
	}

	return ($id);
}

/**
 * 
**/
function updateTOsql($id, $array, $table, $link)
{
    $sql2 = "";
    $sql1 = " UPDATE " . $table . " SET ";
    
    foreach($array as $k => $a) {
        if((!is_numeric($a)) && ($a !== "")) $sql2 = $sql2 . " " . $k . " = \"" . $a . "\" ,"; 
        elseif (is_numeric($a)) $sql2 = $sql2 . " " . $k . " = " . $a . " ,"; 
        elseif ($a == "") $sql2 = $sql2 . " " . $k . " = NULL ,"; 
    }

    $sql2 = substr($sql2, 0, strlen($sql2) - 1);
    $sql3 = " WHERE " . $table . "_ID = " . $id . ";";
    $sql = $sql1 . $sql2 . $sql3;
    if ($sql2 <> "") $r = sqlDoselect($sql, $link);
    if ($r) return ("true");
    else "false";
}

/**
 * 
**/
function per ($text, $perem)
{
    if (strpos($text, " " . $perem . "=\"") > 0) {
        $text = substr($text, strpos($text, " " . $perem . "=\"") + strlen($perem) + 3);
        $text = substr($text, 0, strpos($text, "\""));
        return $text;
    } else return 0;
}

/**
 * 
**/
function text_programm($text)
{
    
    $t = str_replace("&lt;", "&amp;lt;", $text);
    $t2 = str_replace("&gt;", "&amp;gt;", $t);
    $t4 = str_replace("\"", "&quot;", $t2);
    $t5 = str_replace(">", "&gt;", $t4);
    $t6 = str_replace("<", "&lt;", $t5);

    return $t6;
}

/**
 * 
**/
function data ($date)
{
    if ($date > 0) {
        $date1 = date_create();
        date_timestamp_set($date1, substr($date, 0, strlen($date) - 3));
        return date_format($date1, 'Y-m-d H:i:s');
    } else return "1970-01-01 00:00:00";
}

/**
 * 
**/
function sqlDoselect ($value, $link)
{
    if ($result = mysqli_query($link, $value)) {
		$id = mysqli_insert_id ($link);
    } else {
        echo "Error: <hr>" . $value . "<br>" . mysqli_error($link);
        mysqli_rollback($link);
        echo "<hr>";
        exit;
    }

    return ($result);
}

/**
 * 
**/
function k($t, $gib_per)
{
	if ($gib_per[$t]) return $gib_per[$t];
	else return ($t);
}

/**
* Функция из массива $vals (все данные проекта) создаёт массив операций с индексами массива $vals.
*
* Формат на выходе:
* $index[PART][1] = 34 (34 ключ массива $vals, $vals[34]), $index[PART][N] = N,
* $index[OPERATION][1] = 47, $index[OPERATION][2] = 48, $index[OPERATION][N] = N,
* $index[GOOD][1] = 2, $index[GOOD][2] = 7, $index[GOOD][N] = N,
* $index[PROJECT][1] = 0, $index[PROJECT][2] = 277, $index[PROJECT][N] = N,
*
* Параметры: массив $vals
**/
function make_index($vals)
{
    foreach ($vals as $k => $v) {
        $temp[] = $v["tag"];
    }

    $in_key = array_unique($temp);

    foreach ($in_key as $v) {
        
        foreach ($vals as $k1 => $v1) {
            if($v == $v1["tag"]) {
               $index[$v][] = $k1;
            }
        }

    }

    return $index;
}

/**
* Функция фильтрует массив $vals (от чего конкретно я не разобрался) и подготавливает для дальнейшей обработки
* Параметры: $vals (project_data, сконвертированная в массив)
**/
function vals_out($vals)
{
    foreach ($vals as $k1=>$v1) {
        if(!is_int($k1))  {
            unset ($vals[$k1]);
            unset ($v1);
        } elseif (($vals[$k1]["tag"] == "OPERATION")
        	&&(!isset($vals[$k1]["attributes"]["TYPEID"]))
        	&& ($vals[$k1]["type"] <> "close"))
        {
            unset ($vals[$k1]);
            unset ($v1);
        } elseif (!isset($vals[$k1]["tag"])) {
            unset ($vals[$k1]);
            unset ($v1);
        } else {
            if (($v1["type"] <> "cdata") && ($v1)) {
                if($v1["value"]) unset($v1["value"]);

                if (($v1["attributes"]["TYPEID"] == "product") && ($v1["type"] == "complete")) {
                    $index = make_index($vals);

                    foreach ($index["GOOD"] as $i) {
                        if ($vals[$i]['attributes']["TYPEID"] == "product") $pr++;
                    }

                    if ($pr > 1) unset($v1);
                    else $vals1[] = $v1;
                } else $vals1[] = $v1;
                unset($pr);
            }
        }
        
    }

    return ($vals1);
}

/**
 * 
**/
function change_tool_cutting_per($array, $good_tool_cutting_, $table, $db, $link, $type, $place, $tool_cutting_for, $description_giblab)
{
    $value = "SELECT * FROM `TOOL_CUTTING` 
    	inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
    	inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
    	WHERE `type` = '" . $type . "' AND `product` = '" . $tool_cutting_for . "' AND `PLACE` = " . $place . " LIMIT 1";
    $result = sqlDoSelect ($value, $link, $db);
    $res = mysqli_fetch_array($result);

    if (mysqli_num_rows($result) == 1) {
        $tool_cutting_changes = 0;
        $error = "";
        
        foreach ($good_tool_cutting_ as $key => $value) {
            if ($key <> "ID") {
                if ($value == "false") $value = 0;
                elseif ($value=="true") $value = 1;

                if (($res[$key] <> $value) && (!in_array($key, $array))) {
                    $tool_cutting_changes++;

                    if ($tool_cutting_changes == 1) $error = $error . "<hr> Во внесённом распиловочном станке " . $res["NAME"] . " код (" . $good_tool_cutting_["ID"] . ") некоторые свойства внесены ошибочно: <hr>";
                    
                    $error = $error . "Атрибут " . $description_giblab[$key] . " (" . $key . ") с " . $value . " изменён на " . $res[$key] . "<hr> ";
                }
            }
        }

    }

    return $error;
}

/**
 * 
**/
function change_tool_cutting_per_new_values($array, $good_tool_cutting_, $table, $db, $link, $type, $place, $tool_cutting_for, $description_giblab)
{
    $value = "SELECT * FROM `TOOL_CUTTING` 
        inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
        inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
        where `type` = '" . $type . "' and `product` = '" . $tool_cutting_for . "' and `PLACE` = " . $place . " LIMIT 1";
    $result = sqlDoSelect ($value, $link, $db);
    $res = mysqli_fetch_array($result);
    if (mysqli_num_rows(($result)) == 1) {
        foreach ($good_tool_cutting_ as $key => $value) {
            if ($value == "false") $value = 0;
            elseif ($value == "true") $value = 1;
            if ($key <> "ID") {
                if (($res[$key] <> $value) && (!in_array($key, $array))) {
                    $good_tool_cutting_[$key] = $res[$key];
                }
            }
            
        }
    }
    return $good_tool_cutting_;
}

/**
 * 
**/
function change_tool_edgeline_per($array, $good_tool_edgeline_, $table, $db, $link, $place, $description_giblab)
{
    $value = "SELECT * FROM `TOOL_EDGELINE` 
        inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
        inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
        where PLACE = " . $place . " LIMIT 1";
    
    $result = sqlDoSelect($value, $link, $db);
    $res = mysqli_fetch_array($result);

    if (mysqli_num_rows(($result)) == 1) {
        $tool_edgeline_changes = 0;
        $error = "";
        if(!$good_tool_edgeline_["MY_CHECK_KL"]) {

            foreach ($good_tool_edgeline_ as $key => $value) {
                if ($value == "false") $value = 0;
                elseif ($value == "true") $value = 1;
                if ($key <> "ID") {
                    if (($res[$key] <> $value) && (!in_array($key, $array))) {
                        $tool_edgeline_changes++;
                        if($tool_edgeline_changes == 1) $error = $error . "<hr> Во внесённом кромкооблицовочном станке " . $res["NAME"] . " код (" . $good_tool_edgeline_["ID"] . ") некоторые свойства внесены ошибочно: <hr>";
                        $error = $error . "Атрибут " . $description_giblab[$key] . " (" . $key . ") изменён с " . $value . " на " . $res[$key] . "<hr>";
                    }
                }
            }

        }
    }
    return ($error);
   
}

/**
 * 
**/
function change_tool_edgeline_per_new_values($array, $good_tool_edgeline_, $table, $db, $link, $place, $description_giblab)
{
    $value = "SELECT * FROM `TOOL_EDGELINE` 
        inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
        inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
        where PLACE = ".$place." LIMIT 1";
    $result = sqlDoSelect($value, $link, $db);
    $res = mysqli_fetch_array($result);

    if (mysqli_num_rows(($result)) == 1) {
        if(!$good_tool_edgeline_["MY_CHECK_KL"]) {

            foreach ($good_tool_edgeline_ as $key => $value) {
                if ($key <> "ID") {
                    if (($res[$key] <> $value) && (!in_array($key, $array))) {
                        $good_tool_edgeline_[$key] = $res[$key];
                    }
                }
            }

        }
    }

    return ($good_tool_edgeline_);
}

/**
 * 
**/
function timer($duration, $file, $function, $line, $data, $session)
{
    if (!isset($session['data_id'])) $session['data_id'] = $session['session_id'];
    if (($duration == 1) && ($session["data_id"] > 0) && (isset($session["data_id"])) && ($session["data_id"] <> '')) {
        $sql = 'SELECT * FROM `log` WHERE sid = "' . $session["data_id"] . '" ORDER BY date DESC;';
        $time = sql_data($line, $file, $function, $sql)['data'][0]['date'];
        $currentTime = date();
        $date = new DateTime($currentTime);
        $t1 = $date->getTimestamp();
        $date = new DateTime($time);
        $t2 = $date->getTimestamp();
        $duration = $t1 - $t2;
    }
    
    if (!isset($session["manager_name"])) $session = $_SESSION;
    $data = str_replace('"', '', $data);
    $data = str_replace("'", "", $data);

    //.. 01.07.2021 Кто-то повадился хакнуть систему.
    // в $session["data_id"], комманды подставляются вместо data_id, а data_id это указатель на файл, в котором находится сохранённая сессия, и если по данному указателю файла нет, то теряется весь смысл. Соответственно можно сделать проверку на существование файла (при обычном использовании Сервиса он там будет всегда), и если файла нет, то в data_id что-то ненужное.
    // Потому если в data_id что-то ненужное, то заменяем на строку с информацией о пользователе (IP, данные авторизированного пользователя, если есть)
    if (preg_match('#[()\/\\+=~^,.!?<>\'"*-]#', $session["data_id"]) || !file_exists($GLOBALS['serv_main_dir'] . '/kronasapp/storage/app/giblab_sessions/' . trim(strip_tags(stripcslashes(htmlspecialchars($session["data_id"])))) . '.txt')) {
        $session["data_id"] = 'IP = ' . $_SERVER['REMOTE_ADDR'] . "\r\n";
        if (isset($_SESSION['user'])) {
            foreach ($_SESSION['user'] as $key => $value) {
                $session["data_id"] .= $key . ' = ' . $value . "\r\n";
            }
        }
    }

    $sql = "INSERT INTO `log` values (NOW()," . $duration . ",'" . $file . "','" . $function . "','" . $line . "', '" . htmlspecialchars($data) . "','" . $session["data_id"] . "','" . $session["manager_name"] . "','" . $session["client_fio"] . "' );";
    $res = sql_data($line, $file, $function, $sql);
    return $res;
}

/**
 * 
**/
function get_out_part_stock($project_data)
{
    
    $timer = timer(0, __FILE__,__FUNCTION__,__LINE__, 'get_part_out_start', $session);
    $data = get_vals_index_without_session($project_data);
    $vals = $data["vals"];
    $vals = vals_out($vals);
    ksort($vals);
    $index = make_index($vals);
    ksort($vals);

    foreach($index['GOOD'] as $i) {
        if (($vals[$i]['attributes']['TYPEID'] == "product") && ($vals[$i]['attributes']['MY_CLIENT_STOCK'] == 1)) {
            $j = $i + 1;

            while ($vals[$j]['tag'] == "PART") {
                if ($vals[$j]['attributes']['MY_CLIENT_STOCK'] == 1) {
                    $r = $vals[$j]['attributes']['ID'];

                    foreach ($index['OPERATION'] as $b) {
                        if ($vals[$b]['type'] == "open") {
                            $f = $b + 1;
                            if ($vals[$f]['tag'] <> "PART") {
                                $f = $f + 1;
                            }
                            $rt = $f;
                            unset($out);

                            while ($vals[$f]['tag'] == "PART") {
                                if ($vals[$f]['attributes']['ID'] == $r) {
                                    if (isset($vals[$b]['attributes']['DATA']))$vals[$b]['attributes']['DATA'] = '';
                                    if (isset($vals[$b]['attributes']['DRAW']))$vals[$b]['attributes']['DRAW'] = '';
                                    $out = 1;
                                    unset($vals[$f]);
                                }
                                $f++;
                            }

                            if ($out == 1) {
                                $f = $rt;

                                while ($vals[$f]['tag'] == "PART") {

                                    foreach ($index["PART"] as $p) {
                                        if (($vals[$p]['attributes']['CL'] > 0)
                                            &&($vals[$p]['attributes']['ID'] == $vals[$f]['attributes']['ID']))
                                        {
                                            $vals[$p]['attributes']['USEDCOUNT'] = 0;
                                        }
                                    }

                                    $f++;
                                }
                            }
                        }
                    }

                    foreach($index["PART"] as $d) {
                        if (($vals[$d]['attributes']['ID'] == $vals[$j]['attributes']['ID']) && (!isset($vals[$d]['attributes']['CL']))) {
                            unset($vals[$d]);
                        }
                    }
                }
                else $signal = 1;
                $j++;
            }
            if ($signal < 1) {
                unset($vals[$j]);
                unset($vals[$i]);
            }
            
        }
    }

    ksort($vals);
    $timer = timer(1, __FILE__,__FUNCTION__,__LINE__, 'get_part_out_finish', $session);
    return vals_index_to_project($vals);
}

/**
 * 
**/
function gl_55($line,$file,$function,$data_xml)
{
    $timer = timer(1, __FILE__,__FUNCTION__,__LINE__, 'optimize start', $_SESSION);
    $data_xml = str_replace('\\', '', $data_xml);
    $r = build_req_for_project($data_xml, '55C00560');
    
    if ($r == false) return $data_xml;
    
    $arr = gib_serv($r, '55C00560','Получение карт кроя',$line,$file,$function,$_SESSION);

    if (isset($arr['error'])) {
        $err[0] = $arr;
        $err[1] = $r;
        $err[2] = $data_xml;
        $_SESSION['error'] .= '<br>' . $arr['error'] . '<br><hr>';
        if ($_SESSION['check_shhet_part'] == 1) {
            echo 'Проблема при оптимизации раскроя: ' . $arr['error'] . '.<hr>Вернитесь назад к редактированию деталей поалуйста.';
        } else my_mail('NO Данные не прошли оптимизацию' . $line . ' / ' . $file . ' / ' . $function, '<pre>' . print_r($arr, true) . print_r($_SESSION, true) . '</pre>', $err, 'xml');
    } else {
        foreach ($arr as $a) $data_xml = $a;
    }

    $timer = timer(1, __FILE__,__FUNCTION__,__LINE__, 'optimize end', $_SESSION);
    return $data_xml;
}

/**
 * 
**/
function gl_cl($line, $file, $function, $data_xml)
{
    $timer = timer(1, __FILE__,__FUNCTION__,__LINE__, 'line.m optimize  start', $_SESSION);
    $data_xml = str_replace('\\', '', $data_xml);
    $r = build_req_for_project($data_xml, '807C17F9');

    if ($r == false) return $data_xml;
    $arr = gib_serv($r, '807C17F9','оптимизация погонажа', $line, $file, $function, $_SESSION);

    if (isset($arr['error'])) {
        $err[0] = $arr;
        $err[1] = $r;
        $err[2] = $data_xml;
        $_SESSION['error'] .= '<br>' . $arr['error'] . '<br><hr>';
        my_mail('NO Данные не прошли оптимизацию' . $line . ' / ' . $file . ' / ' . $function, '<pre>' . print_r($arr, true) . print_r($_SESSION, true) . '</pre>', $err, 'xml');
    } else {
        foreach ($arr as $a) $data_xml = $a;
    }

    $timer = timer(1, __FILE__,__FUNCTION__,__LINE__, 'line.m optimize end', $_SESSION);
    return $data_xml;
}

/**
 * 
**/
function gl_recalc($line, $file, $function, $data_xml)
{
    $data_xml = str_replace('\\', '', $data_xml);
    $data_xml = quot($data_xml);
    $r = build_req_for_project($data_xml, '4EC657AF');
    $arr = gib_serv($r, '4EC657AF','Депараметризация', $line, $file, $function, $_SESSION);
    if (isset($arr['error'])) {
        $_SESSION['error'] .= '<br>' . $arr['error'] . '<br><hr>';
        $_SESSION['check_index'] = 1;
    } else {
        unset ($_SESSION['check_index']);
        foreach ($arr as $a) $data_xml = $a;
    }
    return $data_xml;
}

/**
 * 
**/
function arrayTOsql($array,$table,$order1c,$product,$v)
{
    $sql1 = "INSERT INTO " . $table . " (";
    $sql11 = "";
    $sql21 = "";
    $i = 0;
    $ii = count($array);
    foreach ($array as $key) {
        $i++;
        if (isset($v[$key])) {
            if($v[$key] == 'true') $v[$key] = 1;
            elseif($v[$key] == 'false') $v[$key] = 0;
            elseif($v[$key] == '') $v[$key] = 'NULL';
            elseif(is_string($v[$key])) $v[$key] = "'" . $v[$key] . "'";

            if ($i < $ii) {
                $sql11 = $sql11 . " `" . mb_strtoupper($key) . "`,";
                $sql21 .= " " . $v[$key] . ",";
            } else {
                $sql11 .= " `" . mb_strtoupper($key) . "`";
                $sql21 .= " " . $v[$key];
            }
        }
    }
    if($product > 0) {
        if (substr($sql21, strlen($sql21) - 1, 1) !== ",") {
            $sql3 .= ", " . $product; 
            $sql2 .= ",`PRODUCT`";
        } else {
            $sql3 .= " " . $product; 
            $sql2 .= "`PRODUCT`";
        }
    }
    if($order1c > 0) {
        if (substr($sql3, strlen($sql3) - 1, 1) !== ",") {
            $sql3 .= ", " . $order1c . ");"; 
            $sql2 .= ",`ORDER1C`) VALUES ( ";
        } else {
            $sql3 .= $order1c . ");"; 
            $sql2 .= "`ORDER1C`) VALUES ( ";
        }
    } else {
        $sql3 .= ");";
        $sql2 .= ") VALUES ( ";
    }

    return ($sql1 . $sql11 . $sql2 . $sql21 . $sql3);    
}

/**
 * 
**/
function arrayUpdateTOsql($array, $table, $order1c, $product, $v, $id)
{  
    $sql1 = "UPDATE " . $table . " SET ";
    $sql11 = "";
    $sql21 = "";
    $i = 0;
    $ii = count($array);

    foreach ($array as $key) {
        $i++;
        if (isset($v[$key])) {
            if (is_numeric($v[$key])) $zn = $v[$key];
            else $zn = '"' . $v[$key] . '"';
            if ($i < $ii) {
                $sql1 .= " " . mb_strtoupper($key) . "=" . $v[$key] . ",";
            } else {
                $sql1 .= " " . mb_strtoupper($key) . "=" . $v[$key] . " ";
            }
        }
    }

    if($product > 0) {
        if (substr($sql1, strlen($sql1) - 1, 1) !== ",") {
            $sql1 .= ", PRODUCT=" . $product . " ";
        } else {
            $sql1 .= " PRODUCT=" . $product . " "; 
        }
    }

    if($order1c > 0) {
        if (substr($sql1, strlen($sql1) - 1, 1) !== ",") {
            $sql1 .= ", ORDER1C=" . $order1c . " ";
        } else {
            $sql1 .= " ORDER1C=" . $order1c . " "; 
        }
    }

    $sql1 .= " WHERE PART_ID=" . $id . ";";
    return ($sql1);
}

/**
 * 
**/
function set_1corder_goods($oper_id, $item, $link, $type)
{
    $sql = 'SELECT * FROM `ORDER1C` WHERE `ID` = ' . $oper_id;
    $res = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['res'];
    unset($request);

    if ($res == 1) {
        switch ($type) {
            case 'material':
                if (($item['material_code'] > 0) && (is_numeric($item['material_code']))) {
                    $start = 'SELECT * FROM `MATERIAL` WHERE `CODE` = ' . $item['material_code'];
                    $service_id = sql_data(__LINE__,__FILE__,__FUNCTION__, $start)['data'][0]['MATERIAL_ID'];
                    $request = "INSERT INTO `ORDER1C_GOODS`(`ORDER1C_GOODS_ID`, `MATERIAL_ID`, `COUNT`, `PRICE`) 
                        VALUES (" . $oper_id . ", " . $service_id . ", " . $item['sheet_count'] . ", " . $item['sheet_price'] . ")";
                }
                break;
            case 'band':
                if (($item['band_code']>0)&&(is_numeric($item['band_code']))) {
                    $start = 'SELECT * FROM `BAND` WHERE `CODE` = ' . $item['band_code'];
                    $service_id = sql_data(__LINE__,__FILE__,__FUNCTION__, $start)['data'][0]['BAND_ID'];
                    $request = "INSERT INTO `ORDER1C_GOODS`(`ORDER1C_GOODS_ID`, `BAND_ID`, `COUNT`, `PRICE`) 
                        VALUES (" . $oper_id . ", " . $service_id . ", " . $item['band_count'] . ", " . $item['band_price'] . ")";
                }
                break;
            case 'simple':
                if (($item['Code'] > 0) && (is_numeric($item['Code']))) {
                    $start = 'SELECT * FROM `SIMPLE` WHERE `CODE` = ' . $item['Code'];
                    $ser = sql_data(__LINE__,__FILE__,__FUNCTION__, $start)['data'][0];

                    if (isset($ser['SIMPLE_ID'])) {
                        unset($ersimple);
                        $service_id = $ser['SIMPLE_ID'];
                        $request = "INSERT INTO `ORDER1C_GOODS`(`ORDER1C_GOODS_ID`, `SIMPLE_ID`, `COUNT`, `PRICE`) 
                            VALUES (" . $oper_id . ", " . $service_id . ", " . $item['count'] . ", " . $item['price'] . ")";
                    }
                    else return false;
                }
                break;
            case 'service':
                if (($item['service_code'] > 0) && (is_numeric($item['service_code']))) {
                    $start = 'SELECT * FROM `SERVICE` WHERE `CODE` = ' . $item['service_code'];
                    $service_id = sql_data(__LINE__,__FILE__,__FUNCTION__, $start)['data'][0]['SERVICE_ID'];
                    $request = "INSERT INTO `ORDER1C_GOODS`(`ORDER1C_GOODS_ID`, `SERVICE_ID`, `COUNT`, `PRICE`) 
                        VALUES (" . $oper_id . ", " . $service_id . ", " . $item['service_count'] . ", " . $item['service_price'] . ")";
                }
                break;
            }

        if (isset($request)) $reqq = sql_data(__LINE__,__FILE__,__FUNCTION__, $request)['res'];
    }

}

/**
 * 
**/
function put_to_db_order_goods($link, $res, $order1c)
{
    if (($order1c > 0) && (is_numeric($order1c))) {
        $sql_d = 'DELETE FROM `ORDER1C_GOODS` WHERE `ORDER1C_GOODS_ID` = ' . $order1c;
        $sql_d = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql_d)['res'];
        if ($sql_d == 1) {

            foreach ($res['service_price'] as $item) {
                $sql = 'SELECT * FROM `SERVICE` WHERE `CODE` = ' . $item['service_code'];
                $r1 = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['count'];
                if($r1 == 1) set_1corder_goods($order1c, $item, $link, 'service');
            }

            foreach ($res['band_price'] as $item) {
                $sql = 'SELECT * FROM `BAND` WHERE `CODE` = ' . $item['band_code'];
                $r1 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['count'];
                if($r1 == 1) set_1corder_goods($order1c, $item, $link, 'band');
            }

            foreach ($res['simple_price'] as $item) {
                $sql = 'SELECT * FROM `SIMPLE` WHERE `CODE` = ' . $item['simple_code'];
                $r1 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['count'];
                if($r1 == 1) set_1corder_goods($order1c, $item, $link, 'simple');
            }

            foreach ($res['material_price'] as $item) {
                $sql = 'SELECT * FROM `MATERIAL` WHERE `CODE` = '.$item['material_code'];
                $r1 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['count'];
                if($r1 == 1) set_1corder_goods($order1c, $item, $link, 'material');
            }
        }
    }
    
}

?>