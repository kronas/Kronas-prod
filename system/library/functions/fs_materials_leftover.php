<?php

/**
 * Скрипт при подтверждении заказа записывает все остатки материалов в таблицу "materials_leftover"
 */

/**
 * Главная функция записи
 */
function writeMaterialsLeftovers($projectData, $accentOrderId) {
    
    $p = xml_parser_create();
    xml_parse_into_struct($p, $projectData, $vals, $index);
    xml_parser_free($p);

    $qo = "SELECT * FROM `ORDER1C` WHERE `DB_AC_ID` = " . $accentOrderId . " LIMIT 1";
    $delOldStrings = "DELETE FROM `materials_leftover` WHERE `DB_AC_ID` = $accentOrderId";
    
    if ($qo = sql_data(__LINE__,__FILE__,__FUNCTION__, $qo)) {
        if (isset($qo['data'][0])) {
            $qo = $qo['data'][0];
            $service_id = $qo['ID'];
            $accent_num = $qo['DB_AC_NUM'];

            $leftovers = getLeftovers($vals);
            if (!empty($leftovers)) {

                // Собираем все остатки и проверяем, если по этому заказу есть какие-то ещё,
                // значит они были записаны исходя из предыдущей версии, и значит удаляем их.
                foreach ($leftovers as $v) {
                    $addString = " AND `material_id` = '" . $v['material_id'] . "' AND (`length` <> '" . $v['length'] . "' OR `width` <> '" . $v['width'] . "' OR `thickness` <> '" . $v['thickness'] . "' OR `count` <> '" . $v['count'] . "')";
                    $delOldStrings .= $addString;
                }
                sql_data(__LINE__,__FILE__,__FUNCTION__, $delOldStrings);

                // А дальше записываем новые, если есть.
                foreach ($leftovers as $v) {
                    $qCheck = "SELECT * FROM `materials_leftover` WHERE `DB_AC_ID` = '" . $accentOrderId . "' AND `material_id` = '" . $v['material_id'] . "' AND `material_code` = '" . $v['material_code'] . "' AND `length` = '" . $v['length'] . "' AND `width` = '" . $v['width'] . "' AND `thickness` = '" . $v['thickness'] . "' AND `count` = '" . $v['count'] . "'";
                    $qCheck = sql_data(__LINE__,__FILE__,__FUNCTION__, $qCheck);

                    // Если остаток не записан, то записываем
                    if ($qCheck['res'] === 0) {
                        $q = "INSERT INTO `materials_leftover` (`order_id`, `DB_AC_ID`, `DB_AC_NUM`, `material_id`, `material_code`, `length`, `width`, `thickness`, `count`, `business`, `waste`) VALUES ($service_id, $accentOrderId, '" . $accent_num . "', " . $v['material_id'] . ", " . $v['material_code'] . ", " . $v['length'] . ", " . $v['width'] . ", " . $v['thickness'] . ", " . $v['count'] . ", " . $v['business'] . ", " . $v['waste'] . ")";
                        $q = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
                    }
                }
            }
        }
    }
    return null;
}

/**
 * Функция возвращает массив остатков (если найдены).
 * material_id
 * material_code
 * length
 * width
 * thickness
 * count
 * business
 * waste
 * 
 * Функция является вспомогательной для "writeMaterialsLeftovers()"
 */
function getLeftovers($vals) {

    $index = make_index($vals);
    $output = [];

    $m = 0;
    foreach ($index['GOOD'] as $i) {
        if ($vals[$i]['attributes']['TYPEID'] == 'sheet') {
            $j = $i + 1;
            while ($vals[$j]['tag'] == 'PART') {
                if (isset($vals[$j]['attributes']['SHEETID'])) {
                    $output[$m]['material_id'] = (int)$vals[$i]['attributes']['MATERIAL_ID'];
                    $output[$m]['material_code'] = (string)$vals[$i]['attributes']['CODE'];
                    $output[$m]['length'] = (float)$vals[$j]['attributes']['L'];
                    $output[$m]['width'] = (float)$vals[$j]['attributes']['W'];
                    $output[$m]['thickness'] = (float)$vals[$i]['attributes']['T'];
                    $output[$m]['count'] = (int)$vals[$j]['attributes']['COUNT'];
                    $output[$m]['business'] = ($vals[$j]['attributes']['BUSINESS'] == 'true') ? 1 : 0;
                    $output[$m]['waste'] = ($vals[$j]['attributes']['WASTE'] == 'true') ? 1 : 0;
                    $m++;
                }
                $j++;
            }
        }
    }

    return $output;
}


?>