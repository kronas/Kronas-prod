<?php

function build_req_for_project($project, $action, $manual = null)
{
    if ($action == '55C00560') {
        $req = <<<rr
              <request comment="optimize" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
              <param value="true" name="save"/>
              <param value="true" name="saveXmlCut"  />
            
              
          rr;
          if (!isset($_SESSION['op_ch']))  $op_cs=cs_op_check ($project,'need');
          else $op_cs=cs_op_check ($project,'all');
          if (isset($_SESSION['op_ch_sheet_part']))  $req.="<param value=\"false\" name=\"clear\"/>";
          if (($op_cs==false)OR(!isset($op_cs))) return false;
          $req.="<param value=\"".implode(",", $op_cs)."\" name=\"operationId\"/>";
    }
    elseif ($action == '807C17F9') {
        $req = <<<rr
              <request comment="optimize" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
              <param value="true" name="save"/>
              <param value="true" name="saveXmlCut"  />
          rr;
    }
     elseif ($action == '4EC657AF') {
        $req = <<<rr
              <request comment="RecalcXNC" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
              <param value="true" name="save"/>
              <param value="true" name="xncCalcExpr"/>
              
          rr;
          $project = xml_parser($project);
          $p = xml_parser_create();
          xml_parse_into_struct($p, $project, $v1, $i1);
          xml_parser_free($p);
          $v1=vals_out($v1);
          ksort($v1);
          $i1=make_index($v1);
          
          foreach($i1['OPERATION'] as $i11)
          {
              if ($v1[$i11]['attributes']['TYPEID']=='XNC')
              {
                  $op_xnc[]=$v1[$i11]['attributes']['ID'];
              }
          }
          $req.="<param value=\"".implode(",", $op_xnc)."\" name=\"operationId\"/>";
    } elseif ($action == '7A5D19E5') {
        $req = <<<rr
              <request comment="PDF Report" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
             
          rr;
    } elseif ($action == 'D401090B') {
        $req = <<<rr
              <request comment="pattern get" action="{$action}">
              <param value="false" name="printParts"/>
              <param value="true" name="printXNOriginAsOp"/>
              <param name="nameTemplate" value="operation.id"/>
          rr;
          $op_xnc=op_xnc_check ($project);
             if ($op_xnc==false) return false;
          $req.="<param value=\"".implode(",", $op_xnc)."\" name=\"operationId\"/>";
    } elseif ($action == '98123591') {

        $project = xml_parser($project);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $project, $vals, $index);
        xml_parser_free($p);
        $vals=vals_out($vals);
        ksort($vals);
        $index=make_index($vals);
        unset($p);
        $vals=add_tag_print($vals);
        foreach($index["PART"] as $i)
        {
            if ($vals[$i]['attributes']['CL']>0)
            {
                foreach(SIDE as $s)
                {
                    if (isset($vals[$i]['attributes']['EL'.mb_strtoupper($s)])) $p['el']=1;
                    if (isset($vals[$i]['attributes']['part.my_cut_angle_'.$s])) $p['cut']=1;
                }

                //.. 18.04.2023 Тоценко. Урезка прямоугольного выреза
                // Урезка на прямоугольном вырезе зависит от параметров "my_cutting_by_drawing_xxx" в отличие от всех других урезок,
                // которые зависят от параметра "my_side_to_cut_xxx", потому, что стандартная урезка, на вырезе отображается не корректно на этикетках.
                // Однако параметры урезки, отличные от стандартных, ломают отображение в PDF, в таблице, где отображается урезка для клиента.
                // Если же к вырезу подставить стандартные параметры, то они отобразятся на этикетке, и "поломают" деталь с вырезом.
                // Поэтому здесь, конкретно для PDF мы добавляем параметры урезки для таблицы, где отображается урезка "part.my_side_to_cut_table_xxx"
                // На основной поток формирования файла проекта эти параметры влиять не будут, потому что они только здесь, для печати PDF
                if ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_r'])) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_l']))) {
                    $vals[$i]['attributes']['part.my_side_to_cut_table_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_l'];
                    $vals[$i]['attributes']['part.my_side_to_cut_table_d_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_l'];
                }
                elseif ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_r'])) && (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_l']))) {
                    $vals[$i]['attributes']['part.my_side_to_cut_table_r'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_r'];
                    $vals[$i]['attributes']['part.my_side_to_cut_table_d_r'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_r'];
                }
                elseif (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_r']) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_l']))) {
                    $vals[$i]['attributes']['part.my_side_to_cut_table_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_l'];
                    $vals[$i]['attributes']['part.my_side_to_cut_table_d_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_l'];
                }

                if ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_t'])) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_b']))) {
                    $vals[$i]['attributes']['part.my_side_to_cut_table_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_b'];
                    $vals[$i]['attributes']['part.my_side_to_cut_table_d_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_b'];
                }
                elseif ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_t'])) && (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_b']))) {
                    $vals[$i]['attributes']['part.my_side_to_cut_table_t'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_t'];
                    $vals[$i]['attributes']['part.my_side_to_cut_table_d_t'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_t'];
                }
                elseif (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_t']) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_b']))) {
                    $vals[$i]['attributes']['part.my_side_to_cut_table_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_b'];
                    $vals[$i]['attributes']['part.my_side_to_cut_table_d_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_b'];
                }
                

                if ((isset($vals[$i]['attributes']['part.my_side_to_cut_r'])) || (isset($vals[$i]['attributes']['part.my_side_to_cut_l']))) {
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_r'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_r'] = $vals[$i]['attributes']['part.my_side_to_cut_r'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_r'] = $vals[$i]['attributes']['part.my_side_to_cut_d_r'];
                    }
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_l'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_l'] = $vals[$i]['attributes']['part.my_side_to_cut_l'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_l'] = $vals[$i]['attributes']['part.my_side_to_cut_d_l'];
                    }
                }
                if ((isset($vals[$i]['attributes']['part.my_side_to_cut_b'])) || (isset($vals[$i]['attributes']['part.my_side_to_cut_t']))) {
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_b'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_b'] = $vals[$i]['attributes']['part.my_side_to_cut_b'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_b'] = $vals[$i]['attributes']['part.my_side_to_cut_d_b'];
                    }
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_t'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_t'] = $vals[$i]['attributes']['part.my_side_to_cut_t'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_t'] = $vals[$i]['attributes']['part.my_side_to_cut_d_t'];
                    }
                }

                if ((isset($vals[$i]['attributes']['part.my_side_to_cut_table_r'])) || (isset($vals[$i]['attributes']['part.my_side_to_cut_table_l']))) $p['side_l'] = 1;
                if ((isset($vals[$i]['attributes']['part.my_side_to_cut_table_b'])) || (isset($vals[$i]['attributes']['part.my_side_to_cut_table_t']))) $p['side_w'] = 1;
            }
        }

        if ($manual=='req_get') $f_name='';else $f_name='';
        $req.=<<<rr
                <request comment="PDF Report" action="{$action}">
                <param value="{$f_name}project.name" name="nameTemplate"/>
                <param value="2" name="printCSpeb"/>
                <param value="true" name="printCSpel"/>
                <param value="true" name="printSPh"/>
                <param value="true" name="printSPp"/>
                <param value="true" name="printO"/>
                <param value="2" name="csSizeModeReport"/>
                <param value="3" name="patternPartForm"/>
                <param name="printPatternPartItems">
                    <item name="indexInOperation" caption="ДНВО"/>
                    <item name="part.id" caption="id"  width="10"/>
                    <item name="detail.length" caption="Дт. Дл." width="15"/>
                    <item name="detail.width" caption="Дт. Шр." width="15"/>
                    <item name="afterPreMilling.length" caption="Пф. Дл." width="15"/>
                    <item name="afterPreMilling.width" caption="Пф. Шр." width="15"/>
                    <item name="count" caption="Кол"/>
                    <item name="usedCount" caption="ИК"/>
            rr;
            if ($p['el']>0)
            {
                $req.=<<<rr
                    <item name="etn" caption="ОВ" width="5"/>
                    <item name="ebn" caption="ОН" width="5"/>
                    <item name="eln" caption="ОЛ" width="5"/>
                    <item name="ern" caption="ОП" width="5"/>
                    <item name="part.my_el_type" caption="EL" type="customer"/>
                rr;
            }
            if ($p['cut']>0)
            {
                $req.=<<<rr
                        <item width="14" name="part.test2" type="expression" typeValue="string" expression="if(empty(part.my_cut_angle_t),'','В['+part.my_cut_angle_t+']/'+part.my_cut_angle_sdvig_t+'\\n')+if(empty(part.my_cut_angle_b),'','Н['+part.my_cut_angle_b+']/'+part.my_cut_angle_sdvig_b+'\\n')+if(empty(part.my_cut_angle_l),'','Л['+part.my_cut_angle_l+']/'+part.my_cut_angle_sdvig_l+'\\n')+if(empty(part.my_cut_angle_r),'','П['+part.my_cut_angle_r+']/'+part.my_cut_angle_sdvig_r+'')" caption="СрТорца/Отступ"/>
                rr;
            }
            if ($p['side_l']>0)
            {
                $req.=<<<rr
                        <item width="10" name="part.cutL" type="expression" typeValue="string" expression="format('0.0',double(if(empty(part.my_side_to_cut_table_d_l),'0',part.my_side_to_cut_table_d_l))+double(if(empty(part.my_side_to_cut_table_d_r),'0',part.my_side_to_cut_table_d_r)))" caption="УрезД"/>
                rr;
            }
            if ($p['side_w']>0)
            {
                $req.=<<<rr
                        <item width="10" name="part.cutW" type="expression" typeValue="string" expression="format('0.0',double(if(empty(part.my_side_to_cut_table_d_t),'0',part.my_side_to_cut_table_d_t))+double(if(empty(part.my_side_to_cut_table_d_b),'0',part.my_side_to_cut_table_d_b)))" caption="УрезШ"/>
                rr;
            }

            $req.=<<<rr
                <item name="part.name1" caption="ИМЯ" type="customer" width="20"/>
                </param>
                <param value="true" name="patternOrient"/>
                <param value="true" name="patternPrintMark"/>
                <param value="true" name="patternPrintEdge"/>
                <param value="true" name="patternPrintName"/>
                <param value="2" name="patternPrintEdgeM"/>
                <param value="rgb(0,0,0)" name="patternPrintEdgeStrokeC"/>
                <param value="1" name="patternPrintEdgeMLineW"/>
                <param value="3" name="patternPrintEdgeMLineD"/>
                <param value="true" name="patternMR"/>
                <param value="3" name="patternFWH"/>
                <param value="yellow" name="patternFWC"/>
                <param value="true" name="printCSxnc"/>
                <param value="1" name="printXNOriginAsOp"/>
                <param value="6" name="printXNCorigin"/>
                <param value="1" name="printXNCmirSide"/>
                <param value="10" name="printML"/>
                <param value="10" name="printMR"/>
                <param value="10" name="printMT"/>
                <param value="10" name="printMB"/>
                <param value="true" name="printELp"/>
                <param value="1" name="patternLS"/>
                <param value="1" name="patternMS"/>
            rr;
          if ($_POST['is_admin'] <> 'true')
          {
                $req.=<<<rr
                <param value="false" name="printRS"/>
                <param value="false" name="printCSimpleAmount"/>
                <param value="false" name="printPLO"/>
                <param value="false" name="printCSh"/>
                <param value="false" name="printCSRS"/>
                <param value="false" name="printCSStat"/>
                <param value="false" name="printCSw"/>
                <param value="false" name="printCSs"/>
                <param value="false" name="printM"/>
                <param value="false" name="printRSWithELWork"/>
                <param value="false" name="printOL"/>
                <param value="false" name="printXNCStat"/>
                <param value="false" name="printPH"/>
                <param value="false" name="printMir"/>
                <param value="false" name="printELh"/>
                rr;
          }
        else
        { 
            $project=op_print ($project);
            $req.=<<<rr
            <param value="true" name="printPhLengthMat"/>
            <param value="true" name="printPhElLength"/>
            <param value="true" name="printRS"/>
            <param value="true" name="printCSimpleAmount"/>
            <param value="true" name="printPLO"/>
            <param value="true" name="printCSh"/>
            <param value="true" name="printCSRS"/>
            <param value="true" name="printCSStat"/>
            <param value="true" name="printCSw"/>
            <param value="true" name="printCSs"/>
            <param value="true" name="printM"/>
            <param value="true" name="printRSWithELWork"/>
            <param value="true" name="printOL"/>
            <param value="true" name="printXNCStat"/>
            <param value="true" name="printPH"/>
            <param value="true" name="printMir"/>
            <param value="true" name="printELh"/>
            rr;
        }
        // echo $req;exit;
              //   <item type ="expression" typeValue="string" expression="'test'" caption="УД"/>
        //   <item type="expression" typeValue="string" expression=format(0.00,part.my_side_to_cut_l+part.my_side_to_cut_r) caption="УД"/>
        //   <item type="expression" typeValue="double" expression=format(0.00,part.my_side_to_cut_t+part.my_side_to_cut_b) caption="УШ"/>
        //   наличие криволинейного кромкования формулой - да/нет
        //  1. Детали (Размер после прифуговки) - надпись убрать чтобы не путать

    } elseif ($action == '235F63BD') {
        $project = xml_parser($project);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $project, $vals, $index);
        xml_parser_free($p);
        $vals=vals_out($vals);
        ksort($vals);
        $index=make_index($vals);
        $vals=add_tag_print($vals);
        $vals[0]['attributes']['project.my_order_id']=strrev($vals[0]['attributes']['project.my_order_id']);
        $project=vals_index_to_project($vals);
        if ($manual=='req_get') $f_name='';else $f_name='';

        $req = <<<rr
              <request comment="PTX get" action="{$action}">
              <param value="2" name="cadmatic4Orient"/>
              <param value="0" name="cadmatic4Trim3Start"/>
              <param value="0" name="cadmatic4Trim3End"/>
              
          rr;
          $req.="<param name=\"nameTemplate\" value=\"".$f_name."project.my_order_id+'_'+project.name+'_'+operation.name+'_'+operation.id\" />";
          $op_cs=cs_op_check ($project,'all2');
          $req.="<param value=\"".implode(",", $op_cs)."\" name=\"operationId\"/>";
    } elseif ($action == 'F6F4A9CC') {
        $project = xml_parser($project);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $project, $vals, $index);
        xml_parser_free($p);
        $vals=vals_out($vals);
        ksort($vals);
        $index=make_index($vals);
        $vals=add_tag_print($vals);
        $vals[0]['attributes']['project.my_order_id']=strrev($vals[0]['attributes']['project.my_order_id']);
        $project=vals_index_to_project($vals);
        $req = <<<rr
              <request comment="LC4 get" action="{$action}">
              <param value="2" name="cadmatic4Orient"/>
              <param value="0" name="cadmatic4Trim3Start"/>
              <param value="0" name="cadmatic4Trim3End"/>
              
          rr;
        if ($manual=='req_get') $f_name='';else $f_name='';

          $req.="<param name=\"nameTemplate\" value=\"".$f_name."project.my_order_id+'_'+project.name+'_'+operation.name+'_'+operation.id\" />";
          $op_cs=cs_op_check ($project,'all2');
          $req.="<param value=\"".implode(",", $op_cs)."\" name=\"operationId\"/>";
    } elseif ($action == 'DAD1ABF3') {
        $project = xml_parser($project);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $project, $vals, $index);
        xml_parser_free($p);
        $vals=vals_out($vals);
        ksort($vals);
        $index=make_index($vals);
        $vals=add_tag_print($vals);
        $vals[0]['attributes']['project.my_order_id']=strrev($vals[0]['attributes']['project.my_order_id']);
        $project=vals_index_to_project($vals);
        $req = <<<rr
              <request comment="SAW get" action="{$action}">
              
          rr;
          if ($manual=='req_get') $f_name='';else $f_name='';

        $req.="<param name=\"nameTemplate\" value=\"".$f_name."project.my_order_id+'_'+project.name+'_'+operation.name+'_'+operation.id\" />";
        $op_cs=cs_op_check ($project,'all2');
        $req.="<param value=\"".implode(",", $op_cs)."\" name=\"operationId\"/>";
    } elseif ($action == '298DF080') {
        // if ($manual=='req_get') $f_name='mpr/';else $f_name='';
        
        $req = <<<rr
                <request action="C4F6336B" >
              <request action="4EC657AF" save="false">
                <param name="xncCalcExpr" value="true"/>
              </request>  
              <request comment="MPR get" action="{$action}">
              <param name="trackingStart" value="project.name+','+operation.code+','+part.id"/>
              <param name="trackingEnd" value="project.name+','+operation.code+','+part.id"/>
              <param name="trackingNCStop" value="false"/>
              <param name="headerOP" value="1"/>
              <param value="false" name="mirHor"/>
              <param value="true" name="mirVert"/>
              <param value="1" name="mirSide"/>
              rr;
        // if ($manual=='manual') $req.='<param value="0" name="prefLongDir"/>';
        // else $req.='<param value="1" name="prefLongDir"/>';
        
		// Корректировка от 17.05.2021
		// С предложенными от Кисиля Р. правками
		$req.= <<<rr
              <param value="1" name="xnm"/>
              <param value="800" name="xnx"/>
              <param value="true" name="grooveTypeOffset"/>
              <tool name="Mill20" diameter="20" plane="mill" number="128"/>
              <tool name="Mill20L" diameter="20" plane="mill" number="129"/>
              <tool name="LMill20L" diameter="20" plane="mill" number="135"/>
              <tool name="Mill12" diameter="12" plane="mill" number="130"/>
              <tool name="Mill4" diameter="4" plane="mill" number="131"/>
              <tool name="Mill8" diameter="8" plane="mill" number="132"/>
              <tool name="Mill6" diameter="6" plane="mill" number="133"/>
              <tool name="Dmill3" diameter="20" plane="mill" number="134"/>
              <tool name="Mill444" diameter="46" plane="mill" number="444"/>
          rr;
        //   $req.="<param name=\"nameTemplate\" value=\"project.name\" />"; 
          for ($u=36;$u<=120;$u++)
          {
            $req.="<tool name=\"Bore".$u."_throught\" diameter=\"".$u."\" plane=\"front\" number=\"128\" recess=\"true\"/>";
            $req.="<tool name=\"Bore".$u."\" diameter=\"".$u."\" plane=\"front\" number=\"128\" recess=\"true\"/>";
          }
          $op_xnc=op_xnc_check ($project);
        //   echo $project;
        //   p_($op_xnc);exit;
          if ($op_xnc<>false) $req.="<param value=\"".implode(",", $op_xnc)."\" name=\"operationId\"/>";
          else return false;
          $req .=  '</request>';
    }
    elseif ($action == 'xnc_get_card') {
        $req = <<<pdf
        <request action="C4F6336B">
            <request comment="RecalcXNC" action="4EC657AF">
                <param value="project.name" name="nameTemplate"/>
                <param value="true" name="save"/>
                <param value="true" name="xncCalcExpr"/>
            </request>
            <request action="D401090B">
                <param comment="XNC Report" action="D401090B"/>
                <param value="true" name="printInOneDoc"/>
                <param value="6" name="printXNCorigin"/>
                <param value="2" name="printXNCmirSide"/>
            </request>
        pdf;
    }
    elseif ($action == 'xnc_get_pic') {
        $req = <<<pdf
        <request action="C4F6336B">
            <request comment="RecalcXNC" action="4EC657AF">
                <param value="project.name" name="nameTemplate"/>
                <param value="true" name="save"/>
                <param value="true" name="xncCalcExpr"/>
            </request>
            <request action="D401090B">
                <param comment="XNC Report" action="D401090B"/>
                <param value="false" name="printInOneDoc"/>
                <param value="6" name="printXNCorigin"/>
                <param value="2" name="printXNCmirSide"/>
            </request>
        pdf;
    }
    elseif ($action == 'xnc_get_card_one') {
        $req = <<<pdf
                <request action="C4F6336B">
                    <request action="D401090B">
                        <param comment="XNC Report" action="D401090B"/>
                        <param value="false" name="printInOneDoc"/>
                        <param value="false" name="printParts"/>
                        <param value="6" name="printXNCorigin"/>
                        <param value="2" name="printXNCmirSide"/>
            pdf;
            $req.='     <param value="'.$GLOBALS['id'].'" name="operationId"/>
                    </request>';
            // p_(htmlspecialchars($req));
            // exit;
        
    }
    elseif ($action == 'stanki') {
        $req = <<<rr
        <request>     
            <request comment="PTX get" action="235F63BD">
                <param name="nameTemplate" value="'ptx/'+project.name+'_'+operation.code" />
            </request>
            <request comment="PTX get" action="F6F4A9CC">
                <param name="nameTemplate" value="'lc4/'+project.name+'_'+operation.code" />
            </request>
            <request comment="MPR get" action="298DF080">
                <param name="nameTemplate" value="'mpr/'+project.name+'_'+operation.code" />
            </request>
                   
            
        rr;
    }
    
    $project_data=$project;
    $vals = get_vals($project_data);

    //.. 18.04.2023 Тоценко. Урезка прямоугольного выреза
    // Урезка на прямоугольном вырезе зависит от параметров "my_cutting_by_drawing_xxx" в отличие от всех других урезок,
    // которые зависят от параметра "my_side_to_cut_xxx", потому, что стандартная урезка, на вырезе отображается не корректно на этикетках.
    // Однако параметры урезки, отличные от стандартных, ломают отображение в PDF, в таблице, где отображается урезка для клиента.
    // Если же к вырезу подставить стандартные параметры, то они отобразятся на этикетке, и "поломают" деталь с вырезом.
    // Поэтому здесь, конкретно для PDF мы добавляем параметры урезки для таблицы, где отображается урезка "part.my_side_to_cut_table_xxx"
    // На основной поток формирования файла проекта эти параметры влиять не будут, потому что они только здесь, для печати PDF
    foreach($index["PART"] as $i)
    {
        if ($vals[$i]['attributes']['CL']>0)
        {
            if ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_r'])) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_l']))) {
                $vals[$i]['attributes']['part.my_side_to_cut_table_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_l'];
                $vals[$i]['attributes']['part.my_side_to_cut_table_d_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_l'];
            }
            elseif ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_r'])) && (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_l']))) {
                $vals[$i]['attributes']['part.my_side_to_cut_table_r'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_r'];
                $vals[$i]['attributes']['part.my_side_to_cut_table_d_r'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_r'];
            }
            elseif (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_r']) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_l']))) {
                $vals[$i]['attributes']['part.my_side_to_cut_table_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_l'];
                $vals[$i]['attributes']['part.my_side_to_cut_table_d_l'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_l'];
            }

            if ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_t'])) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_b']))) {
                $vals[$i]['attributes']['part.my_side_to_cut_table_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_b'];
                $vals[$i]['attributes']['part.my_side_to_cut_table_d_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_b'];
            }
            elseif ((isset($vals[$i]['attributes']['part.my_cutting_by_drawing_t'])) && (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_b']))) {
                $vals[$i]['attributes']['part.my_side_to_cut_table_t'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_t'];
                $vals[$i]['attributes']['part.my_side_to_cut_table_d_t'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_t'];
            }
            elseif (!isset($vals[$i]['attributes']['part.my_cutting_by_drawing_t']) && (isset($vals[$i]['attributes']['part.my_cutting_by_drawing_b']))) {
                $vals[$i]['attributes']['part.my_side_to_cut_table_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_b'];
                $vals[$i]['attributes']['part.my_side_to_cut_table_d_b'] = $vals[$i]['attributes']['part.my_cutting_by_drawing_d_b'];
            }

            if ((isset($vals[$i]['attributes']['part.my_side_to_cut_r'])) || (isset($vals[$i]['attributes']['part.my_side_to_cut_l']))) {
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_r'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_r'] = $vals[$i]['attributes']['part.my_side_to_cut_r'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_r'] = $vals[$i]['attributes']['part.my_side_to_cut_d_r'];
                    }
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_l'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_l'] = $vals[$i]['attributes']['part.my_side_to_cut_l'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_l'] = $vals[$i]['attributes']['part.my_side_to_cut_d_l'];
                    }
                }
                if ((isset($vals[$i]['attributes']['part.my_side_to_cut_b'])) || (isset($vals[$i]['attributes']['part.my_side_to_cut_t']))) {
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_b'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_b'] = $vals[$i]['attributes']['part.my_side_to_cut_b'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_b'] = $vals[$i]['attributes']['part.my_side_to_cut_d_b'];
                    }
                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_t'])) {
                        $vals[$i]['attributes']['part.my_side_to_cut_table_t'] = $vals[$i]['attributes']['part.my_side_to_cut_t'];
                        $vals[$i]['attributes']['part.my_side_to_cut_table_d_t'] = $vals[$i]['attributes']['part.my_side_to_cut_d_t'];
                    }
                }
        }
    }
   
    if (substr_count($vals[0]['attributes']['NAME'],"/")>0) $vals[0]['attributes']['NAME']=str_replace("/","|",$vals[0]['attributes']['NAME']);
    if (isset($vals[0]['attributes']['project.my_order_name'])) $vals[0]['attributes']['project.my_order_name']=str_replace("/","|",$vals[0]['attributes']['project.my_order_name']);
    if (isset($vals[0]['attributes'][mb_strtoupper('project.my_order_name')])) $vals[0]['attributes'][mb_strtoupper('project.my_order_name')]=str_replace("/","|",$vals[0]['attributes'][mb_strtoupper('project.my_order_name')]);

    $project=vals_index_to_project($vals);
    $project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $project);
    $project = str_replace('&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;', '', $project);
    
    if ($manual<>'req_get') $req .= $project . '</request>';
    else $req .='</request>';;

    $text = $req;
    $text=str_replace("qquot","quot",$text);
    $text=str_replace("ququot","quot",$text);
    $text=str_replace("quoquot","quot",$text);
    $text=str_replace("quotquot","quot",$text);
    $req = $text;

    return $req;
};

?>