<?php

/**
 * Функция проверяет XNC-обработки на наличие производственных ограничений
 * по размеру деталей при наличии сверлений и/или фрезеровок и/или пазований
 * (пока что только для деталей больших размеров)
 * (и пока только критические ошибки, т.е. если не сработали проверки выше)
 */
function checkSizesRestrictions($vals, $xncOperations, $allDetails, $place = 1) {

    $errors = '';
    $smallSizeMin = null;
    $smallSizeMax = null;
    $bigSizeMin = null;
    $bigSizeMax = null;

    $detailsData = getDetailsForRestrictions($vals);

    foreach ($xncOperations as $i) {

        // Информация о детали
        $countertopFlag = 0;
        $detId = $vals[$i + 1]['attributes']['ID'];
        $dName = $allDetails[$detId]['detName'];
        $dW = $allDetails[$detId]['w'];
        $dL = $allDetails[$detId]['l'];
        $dT = $allDetails[$detId]['material']['t'];
        $dMatName = $allDetails[$detId]['material']['name'];

        // Проверка на столешницу
        // Если столешница, то пропускаем ограничение по максимальной величине
        if (isset($detailsData[$detId])
         && false !== strpos($detailsData[$detId]['material']['name'], 'ПФ ')
         || false !== strpos($detailsData[$detId]['material']['name'], 'Compact')
         || $dT >= 28)
        {
            $countertopFlag = 1;
        }

        $p = xml_parser_create();
        xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
        xml_parser_free($p);

        $detWidth = $v1[0]['attributes']['DY'];
        $detHeight = $v1[0]['attributes']['DX'];
        $bigDetSide = ($detWidth >= $detHeight) ? $detWidth : $detHeight;
        $SmallDetSide = ($detHeight <= $detWidth) ? $detHeight : $detWidth;

        // Определяем минимальные и максимальные ограничения сторон:
        // (минимальные размеры пока не обрабатываются, но сохраняем на будущее)
        // для сверлений (для сверлений приоритет выше, т.к. размеры меньше)
        if (isset($i1['BF']) || isset($i1['BL']) || isset($i1['BT']) || isset($i1['BR']) || isset($i1['BB'])) {
            $drillingRestrictions = getDrillingRestriction();

            $smallSizeMin = ($place == 2 || $place == 9) ? $drillingRestrictions['min']['enlargedSmallSide'] : $drillingRestrictions['min']['smallSide'];
            $smallSizeMax = ($place == 2 || $place == 9) ? $drillingRestrictions['min']['enlargedBigSide'] : $drillingRestrictions['min']['bigSide'];

            $bigSizeMin = ($place == 1 || $place == 3) ? $drillingRestrictions['max']['enlargedSmallSide'] : $drillingRestrictions['max']['smallSide'];
            $bigSizeMax = $drillingRestrictions['max']['bigSide'];
        // для фрезеровок и пазований
        } elseif ((isset($i1['MS']) && (isset($i1['ML']) || isset($i1['MA']) || isset($i1['MAC']))) || isset($i1['GR'])) {
            $millingRestrictions = getMillingRestriction();

            $smallSizeMin = $millingRestrictions['min']['smallSide'];
            $smallSizeMax = $millingRestrictions['min']['bigSide'];
            
            $bigSizeMin = ($place == 1 || $place == 3) ? $millingRestrictions['max']['enlargedSmallSide'] : $millingRestrictions['max']['smallSide'];
            $bigSizeMax = $millingRestrictions['max']['bigSide'];
        }

        // И дальше если есть какие-то из обработок выше проверяем ограничения
        if (!empty($bigSizeMin) && !empty($bigSizeMax)) {
            if ($SmallDetSide < $smallSizeMin) {
                $wxnc = ($detWidth >= $detHeight) ? $smallSizeMax : $smallSizeMin;
                $lxnc = ($detHeight <= $detWidth) ? $smallSizeMin : $smallSizeMax;

                $tmpErr = getErrorMessage('0', LANG);
                $tmpErr = str_replace('detName', $dName, $tmpErr);
                $tmpErr = str_replace('detId', $detId, $tmpErr);
                $tmpErr = str_replace('detMatName', $dMatName, $tmpErr);
                $tmpErr = str_replace('detCl', $dL, $tmpErr);
                $tmpErr = str_replace('detCw', $dW, $tmpErr);
                $tmpErr = str_replace('clSize', $lxnc, $tmpErr);
                $tmpErr = str_replace('cwSize', $wxnc, $tmpErr);

                $errors .= $tmpErr;
            } elseif (($SmallDetSide > $bigSizeMin || $bigDetSide > $bigSizeMax) && $countertopFlag === 0) {
                $wxnc = ($detWidth >= $detHeight) ? $bigSizeMax : $bigSizeMin;
                $lxnc = ($detHeight <= $detWidth) ? $bigSizeMin : $bigSizeMax;

                $tmpErr = getErrorMessage('1', LANG);
                $tmpErr = str_replace('detName', $dName, $tmpErr);
                $tmpErr = str_replace('detId', $detId, $tmpErr);
                $tmpErr = str_replace('detMatName', $dMatName, $tmpErr);
                $tmpErr = str_replace('detCl', $dL, $tmpErr);
                $tmpErr = str_replace('detCw', $dW, $tmpErr);
                $tmpErr = str_replace('clSize', $lxnc, $tmpErr);
                $tmpErr = str_replace('cwSize', $wxnc, $tmpErr);

                $errors .= $tmpErr;
            }
        }
    }

    return $errors;
}

?>