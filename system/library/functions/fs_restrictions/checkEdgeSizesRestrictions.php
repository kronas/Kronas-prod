<?php

/**
 * Функция увеличивает детали, которые меньше минимально допустимых размеров
 * и возвращает размер по умолчанию, если кромки были удалены
 * 
 * Возвращает пустую строку (к существующим критическим ошибкам)
 */
function checkEdgeSizesRestrictions($vals, $xncOperations, $allDetails) {

    $tmplNum = 'smallDetailWithEdge';
    $changes = 0;
    $warnings = '';

    $detSizesRestrictions = getEdgeSizeRestrictions();
    $detailsWithoutXnc = filterDetailsWithoutXnc($vals, $xncOperations, $allDetails);
    $detailsWithXnc = filterDetailsWithXnc($vals, $xncOperations, $allDetails);

    if (!empty($detailsWithoutXnc) || !empty($detailsWithXnc)) {

        $rollbackSizeDetails = getRollbackEdgeDetails($vals, $detailsWithoutXnc, $detailsWithXnc);

        if (!empty($rollbackSizeDetails)) {
            foreach ($rollbackSizeDetails as $detId => $detArr) {

                $i = $detArr['valsNum'];
                $attrs = $vals[$i]['attributes'];

                $detName = $detArr['detName'];
                $detId = $detArr['detId'];
                $detMatName = $detArr['material']['name'];
                $detL = $detArr['l'];
                $detW = $detArr['w'];

                if (isset($vals[$i]['attributes']['MY_XNC_CUT_DELTAX'])) {
                    $attrs['L'] -= $attrs['MY_XNC_CUT_DELTAX'];
                    $attrs['DL'] -= $attrs['MY_XNC_CUT_DELTAX'];
                    $attrs['JL'] -= $attrs['MY_XNC_CUT_DELTAX'];
                    $attrs['CL'] -= $attrs['MY_XNC_CUT_DELTAX'];

                    unset(
                        $attrs['my_xnc_cut_deltax'],
                        $attrs['part.my_xnc_cut_deltax'],
                        $attrs['my_small_edge_detail'],
                        $attrs['my_small_edge_detail_dl'],
                        $attrs['my_side_to_cut_l'],
                        $attrs['my_side_to_cut_d_l'],
                        $attrs['my_side_to_cut_r'],
                        $attrs['my_side_to_cut_d_r'],
                        $attrs['part.my_side_to_cut_l'],
                        $attrs['part.my_side_to_cut_d_l'],
                        $attrs['part.my_side_to_cut_r'],
                        $attrs['part.my_side_to_cut_d_r'],
                        $attrs[strtoupper('my_xnc_cut_deltax')],
                        $attrs[strtoupper('part.my_xnc_cut_deltax')],
                        $attrs[strtoupper('my_small_edge_detail')],
                        $attrs[strtoupper('my_small_edge_detail_dl')],
                        $attrs[strtoupper('my_side_to_cut_l')],
                        $attrs[strtoupper('my_side_to_cut_d_l')],
                        $attrs[strtoupper('my_side_to_cut_r')],
                        $attrs[strtoupper('my_side_to_cut_d_r')],
                        $attrs[strtoupper('part.my_side_to_cut_l')],
                        $attrs[strtoupper('part.my_side_to_cut_d_l')],
                        $attrs[strtoupper('part.my_side_to_cut_r')],
                        $attrs[strtoupper('part.my_side_to_cut_d_r')]
                    );

                    $detDirection = (LANG === 'Ru') ? '(по длинне)' : '(по довжині)';
                }

                if (isset($vals[$i]['attributes']['MY_XNC_CUT_DELTAY'])) {
                    $attrs['W'] -= $attrs['MY_XNC_CUT_DELTAY'];
                    $attrs['DW'] -= $attrs['MY_XNC_CUT_DELTAY'];
                    $attrs['JW'] -= $attrs['MY_XNC_CUT_DELTAY'];
                    $attrs['CW'] -= $attrs['MY_XNC_CUT_DELTAY'];

                    unset(
                        $attrs['my_xnc_cut_deltay'],
                        $attrs['part.my_xnc_cut_deltay'],
                        $attrs['my_small_edge_detail'],
                        $attrs['my_small_edge_detail_dw'],
                        $attrs['my_side_to_cut_b'],
                        $attrs['my_side_to_cut_d_b'],
                        $attrs['my_side_to_cut_t'],
                        $attrs['my_side_to_cut_d_t'],
                        $attrs['part.my_side_to_cut_b'],
                        $attrs['part.my_side_to_cut_d_b'],
                        $attrs['part.my_side_to_cut_t'],
                        $attrs['part.my_side_to_cut_d_t'],
                        $attrs[strtoupper('my_xnc_cut_deltay')],
                        $attrs[strtoupper('part.my_xnc_cut_deltay')],
                        $attrs[strtoupper('my_small_edge_detail')],
                        $attrs[strtoupper('my_small_edge_detail_dw')],
                        $attrs[strtoupper('my_side_to_cut_b')],
                        $attrs[strtoupper('my_side_to_cut_d_b')],
                        $attrs[strtoupper('my_side_to_cut_t')],
                        $attrs[strtoupper('my_side_to_cut_d_t')],
                        $attrs[strtoupper('part.my_side_to_cut_b')],
                        $attrs[strtoupper('part.my_side_to_cut_d_b')],
                        $attrs[strtoupper('part.my_side_to_cut_t')],
                        $attrs[strtoupper('part.my_side_to_cut_d_t')]
                    );

                    $detDirection = (LANG === 'Ru') ? '(по ширине)' : '(по ширині)';
                }

                // Если по какой-то оси обработки удалены, а по другой нет, значит возвращаем параметр MY_SMALL_EDGE_DETAIL
                if (isset($attrs['MY_XNC_CUT_DELTAX']) || isset($attrs['MY_XNC_CUT_DELTAY'])) {
                    $attrs['MY_SMALL_EDGE_DETAIL'] = 1;
                } else $detDirection = '';

                if (!isset($detArr['xncAdded']) && !isset($detArr['handChangeSize'])) {
                    $tmpErr = getWarningMessage($tmplNum . 'Rollback', LANG);
                    // Здесь заменяем маркеры на соответствующие параметры в сообщении об ошибке
                    $tmpErr = str_replace('detName', $detName, $tmpErr);
                    $tmpErr = str_replace('detId', $detId, $tmpErr);
                    $tmpErr = str_replace('detMatName', $detMatName, $tmpErr);
                    $tmpErr = str_replace('detCl', $detL, $tmpErr);
                    $tmpErr = str_replace('detCw', $detW, $tmpErr);
                    $tmpErr = str_replace('detDirection', $detDirection, $tmpErr);
                }
                

                $warnings .= $tmpErr;
                $vals[$i]['attributes'] = $attrs;
                $changes = 1;
            }
        }

        $smallDetails = getSmallEdgeDetails($vals, $detailsWithoutXnc, $detSizesRestrictions);
        if (!empty($smallDetails)) {

            foreach ($smallDetails as $detId => $detArr) {

                $deltaX = 0;
                $deltaY = 0;
                $i = $detArr['valsNum'];
                $attrs = $vals[$i]['attributes'];

                $detName = $detArr['detName'];
                $detId = $detArr['detId'];
                $detMatName = $detArr['material']['name'];
                $detL = $detArr['l'];
                $detW = $detArr['w'];

                if ($attrs['JL'] < $detSizesRestrictions) {

                    $side = (isset($detArr['edges']['L'])) ? 'L' : 'R';
                    $detDirection = (LANG === 'Ru') ? 'длинне' : 'довжині';

                    $deltaX = $detSizesRestrictions - $attrs['JL'];

                    $attrs['MY_XNC_CUT_DELTAX'] = $deltaX;
                    $attrs['MY_SIDE_TO_CUT_D_' . $side] = $attrs['DL'];
                    $attrs['MY_SIDE_TO_CUT_' . $side] = $attrs['CL'];
                    $attrs['PART.MY_XNC_CUT_DELTAX'] = $deltaX;
                    $attrs['PART.MY_SIDE_TO_CUT_D_' . $side] = $attrs['DL'];
                    $attrs['PART.MY_SIDE_TO_CUT_' . $side] = $attrs['CL'];
                    $attrs['MY_SMALL_EDGE_DETAIL'] = 1;

                    $attrs['L'] += $deltaX;
                    $attrs['DL'] += $deltaX;
                    $attrs['JL'] += $deltaX;
                    $attrs['CL'] += $deltaX;

                    // Здесь записываем увеличенный размер детали, если он будет изменён в конструкторе вручную,
                    // тогда при очередгой проверке проекта уберём параметры увеличения детали, урезки и дельты.
                    $attrs['MY_SMALL_EDGE_DETAIL_DL'] = $attrs['DL'];

                    $tmpErr = getWarningMessage($tmplNum, LANG);
                    // Здесь заменяем маркеры на соответствующие параметры в сообщении об ошибке
                    $tmpErr = str_replace('detName', $detName, $tmpErr);
                    $tmpErr = str_replace('detId', $detId, $tmpErr);
                    $tmpErr = str_replace('detMatName', $detMatName, $tmpErr);
                    $tmpErr = str_replace('detCl', $detL, $tmpErr);
                    $tmpErr = str_replace('detCw', $detW, $tmpErr);
                    $tmpErr = str_replace('detDirection', $detDirection, $tmpErr);

                    $warnings .= $tmpErr;
                }

                if ($attrs['JW'] < $detSizesRestrictions) {

                    $side = (isset($detArr['edges']['T'])) ? 'T' : 'B';
                    $detDirection = (LANG === 'Ru') ? 'ширине' : 'ширині';
                
                    $deltaY = $detSizesRestrictions - $attrs['JW'];

                    $attrs['MY_XNC_CUT_DELTAY'] = $deltaY;
                    $attrs['MY_SIDE_TO_CUT_D_' . $side] = $attrs['DW'];
                    $attrs['MY_SIDE_TO_CUT_' . $side] = $attrs['CW'];
                    $attrs['PART.MY_XNC_CUT_DELTAY'] = $deltaY;
                    $attrs['PART.MY_SIDE_TO_CUT_D_' . $side] = $attrs['DW'];
                    $attrs['PART.MY_SIDE_TO_CUT_' . $side] = $attrs['CW'];
                    $attrs['MY_SMALL_EDGE_DETAIL'] = 1;

                    $attrs['W'] += $deltaY;
                    $attrs['DW'] += $deltaY;
                    $attrs['JW'] += $deltaY;
                    $attrs['CW'] += $deltaY;

                    // Здесь записываем увеличенный размер детали, если он будет изменён в конструкторе вручную,
                    // тогда при очередгой проверке проекта уберём параметры увеличения детали, урезки и дельты.
                    $attrs['MY_SMALL_EDGE_DETAIL_DW'] = $attrs['DW'];

                    $tmpErr = getWarningMessage($tmplNum, LANG);
                    // Здесь заменяем маркеры на соответствующие параметры в сообщении об ошибке
                    $tmpErr = str_replace('detName', $detName, $tmpErr);
                    $tmpErr = str_replace('detId', $detId, $tmpErr);
                    $tmpErr = str_replace('detMatName', $detMatName, $tmpErr);
                    $tmpErr = str_replace('detCl', $detL, $tmpErr);
                    $tmpErr = str_replace('detCw', $detW, $tmpErr);
                    $tmpErr = str_replace('detDirection', $detDirection, $tmpErr);

                    $warnings .= $tmpErr;
                }

                if ($deltaX !== 0 || $deltaY !== 0) {
                    $vals[$i]['attributes'] = $attrs;
                    $changes = 1;
                }

            }
        }
        
    }

    if ($changes === 1) {
        $_SESSION['vals_after_change'] = $vals;
        $_SESSION['additions_errors_ok'] .= $warnings;
    }

    return '';
}

/**
 * Функция отфильтровывает детали, в которых нет XNC-обработок
 */
function filterDetailsWithoutXnc($vals, $xncOperations, $allDetails) {
    
    foreach ($xncOperations as $i) {
        $detId = $vals[$i + 1]['attributes']['ID'];

        if (isset($allDetails[$detId])) {
            unset($allDetails[$detId]);
        }
    }

    return $allDetails;
}

/**
 * Функция отфильтровывает детали, в которых есть XNC-обработки
 * Детали из этой функции используются для проверки увеличения размера.
 * Если размер детали был увеличен ранее (для возможности кромкования),
 * и на ней не было обработок, а потом (уже на увеличенную) их добавили,
 * то нужно размер детали вернуть к изначальному.
 */
function filterDetailsWithXnc($vals, $xncOperations, $allDetails) {
    
    $output = [];

    foreach ($xncOperations as $i) {
        $detId = $vals[$i + 1]['attributes']['ID'];
        $detValsNum = $allDetails[$detId]['valsNum'];

        if (isset($allDetails[$detId]) && isset($vals[$detValsNum]['attributes']['MY_SMALL_EDGE_DETAIL'])) {
            $output[$detId] = $allDetails[$detId];
        }
    }

    return $output;
}

/**
 * Функция отфильтровывает маленькие детали с кромками
 */
function getSmallEdgeDetails($vals, $detailsWithoutXnc, $detSizesRestrictions) {

    $output = [];

    foreach ($detailsWithoutXnc as $detId => $detArr) {

        if (isset($detArr['edges'])) {

            $jointingW = $vals[$detArr['valsNum']]['attributes']['JW'];
            $jointingL = $vals[$detArr['valsNum']]['attributes']['JL'];

            if ($jointingL < $detSizesRestrictions && (isset($detArr['edges']['L']) xor isset($detArr['edges']['R']))
             || $jointingW < $detSizesRestrictions && (isset($detArr['edges']['B']) xor isset($detArr['edges']['T']))) 
            {
                $output[$detId] = $detArr;
            }

        }
    }

    return $output;
}

/**
 * Функция отфильтровывает маленькие детали без кромок, на которых раньше была кромка
 * и которые были увеличены, но на которых убрали кромку и их размер нужно вернуть к изначальному.
 */
function getRollbackEdgeDetails($vals, $detailsWithoutXnc, $detailsWithXnc) {

    $output = [];

    if (!empty($detailsWithoutXnc)) {
        foreach ($detailsWithoutXnc as $detId => $detArr) {

            $i = $detArr['valsNum'];

            if (isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL']) || isset($vals[$i]['attributes']['my_small_edge_detail'])) {
                if ((isset($vals[$i]['attributes']['MY_XNC_CUT_DELTAX']) && (!isset($detArr['edges']['L']) && !isset($detArr['edges']['R'])))
                 || (isset($vals[$i]['attributes']['MY_XNC_CUT_DELTAY']) && (!isset($detArr['edges']['B']) && !isset($detArr['edges']['T'])))
                 // || (isset($vals[$i]['attributes']['MY_XNC_CUT_DELTAY']) && !empty($detailsWithXnc) && isset($detailsWithXnc[$detId]))
                 || (isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DL']) && $vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DL'] != $vals[$i]['attributes']['DL'])
                 || (isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DW']) && $vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DW'] != $vals[$i]['attributes']['DW']))
                {
                    // if (!empty($detailsWithXnc) && isset($detailsWithXnc[$detId])) {
                    //     // Если к детали добавлена операция, то отмечаем это, чтобы не выводить сообщение,
                    //     // что деталь возвращена к изначальному размеру, т.к. в этом случае будет выдаваться сообщение
                    //     // об увеличении детали уже в связи с XNC-обработками.
                    //     $detArr['xncAdded'] = 1;
                    // }
                    if ((isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DL']) && $vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DL'] != $vals[$i]['attributes']['DL'])
                     || (isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DW']) && $vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL_DW'] != $vals[$i]['attributes']['DW'])) {

                        // Признак того, что размер детали был изменён вручную
                        $detArr['handChangeSize'] = 1;
                    }
                    $output[$detId] = $detArr;
                }
            }
        }
    }
    // else {
    //     foreach ($detailsWithXnc as $detId => $detArr) {

    //         $i = $detArr['valsNum'];

    //         if (isset($vals[$i]['attributes']['MY_XNC_CUT_DELTAY'])) {

    //             // Если к детали добавлена операция, то отмечаем это, чтобы не выводить сообщение,
    //             // что деталь возвращена к изначальному размеру, т.к. в этом случае будет выдаваться сообщение
    //             // об увеличении детали уже в связи с XNC-обработками.
    //             $detArr['xncAdded'] = 1;
    //             $output[$detId] = $detArr;
    //         }
    //     }
    // }

    return $output;
}

?>