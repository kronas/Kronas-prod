<?php

// Функции для проверки производственных ограничений для кромкования радиусов

/**
 * Главная функция проверки диаметров
 */
function checkCutoutDiameters($vals, $detInfo, $tmplNum, $programVals, $radius = false) {

    $detId = $detInfo['detId'];
    $opName = '';
    $radiusValue = 10; // Стандартный радиус для шаблонов зарезов без параметра радиуса
    $edgesByDetail = [];
    $isEdgeSide = false;
    $edgeThickness = 0;
    $index = make_index($vals);
    $pIndex = make_index($programVals);

    $templateSides = [
        19 => ['L', 'B'],
        20 => ['B', 'R'],
        21 => ['R', 'T'],
        22 => ['T', 'L'],
        28 => ['B'],
        23 => ['L', 'B'],
        24 => ['B', 'R'],
        25 => ['R', 'T'],
        26 => ['T', 'L'],
        27 => ['B'],
        29 => [],
        95 => ['T']
    ];

    foreach ($pIndex['CALL'] as $arrNum) {
        if (isset($programVals[$arrNum]['attributes']['NAME'])) {
            $opName = str_replace(' .xnc', '', $programVals[$arrNum]['attributes']['NAME']);
            break;
        }
    }    
    $detInfo['opName'] = $opName;

    foreach ($index['PART'] as $arrNum) {
        if ($vals[$arrNum]['type'] === 'complete'
            && (isset($vals[$arrNum]['attributes']['DL']) && isset($vals[$arrNum]['attributes']['DW']))
            && $vals[$arrNum]['attributes']['ID'] == $detId) {

            $attr = $vals[$arrNum]['attributes'];
            $edgesByDetail = edgesByDetail($vals, $attr);
        }
    }

    if ($radius !== false) {
        $radiusValue = getTemplateRadius($programVals, $tmplNum, $templateSides);
    }

    foreach ($edgesByDetail as $key => $value) {
        if (in_array($key, $templateSides[$tmplNum])) {
            $isEdgeSide = true;
            $edgeThickness = $value;
            break;
        }
    }

    if ($isEdgeSide === false) {
        return '';
    } else {
        return checkThicknessRestriction($detInfo, $radiusValue, $edgeThickness, 'cornerNotchRadius');
    }
    
}

/**
 * Функция возвращает радиус зареза
 */
function getTemplateRadius($programVals, $tmplNum, &$templateSides) {
    $radius = 10;
    $pIndex = make_index($programVals);
    $radiusNotation = 'r';

    switch ($tmplNum) {
        case 23:
            $radiusNotation = 'RV1';
            break;
        case 24:
            $radiusNotation = 'RV2';
            break;
        case 25:
            $radiusNotation = 'RV3';
            break;
        case 26:
            $radiusNotation = 'RV4';
            break;
    }

    if ($tmplNum !== 29) {
        foreach ($pIndex['PAR'] as $i) {
            if ($programVals[$i]['attributes']['NAME'] == $radiusNotation && isset($programVals[$i]['attributes']['VALUE'])) {
                $radius = $programVals[$i]['attributes']['VALUE'];
                break;
            }
        }
    } else {
        $tmp = [];
        $tmpSide = ''; // Обозначение угла радиуса (RV1 - левый нижний, RV2 - правый нижний, RV3 - правый верхний, RV4 - левый верхний)
        $tmpVal = 1000; // значение радиуса (чтобы вернуть стороны кромкования самого маленького радиуса)
        foreach ($pIndex['PAR'] as $i) {
            if (false !== stripos($programVals[$i]['attributes']['NAME'], 'RV') && isset($programVals[$i]['attributes']['VALUE'])) {
                if ($tmpVal > $programVals[$i]['attributes']['VALUE']) {
                    $tmpVal = $programVals[$i]['attributes']['VALUE'];
                    $tmpSide = $programVals[$i]['attributes']['NAME'];
                }
            }
        }

        switch ($tmpSide) {
            case 'RV1':
                $templateSides[29] = ['L', 'B'];
                break;
            case 'RV2':
                $templateSides[29] = ['R', 'B'];
                break;
            case 'RV3':
                $templateSides[29] = ['R', 'T'];
                break;
            case 'RV4':
                $templateSides[29] = ['L', 'T'];
                break;
        }

        $radius = $tmpVal;
    }

    return $radius;
}

function checkThicknessRestriction($detInfo, $radiusValue, $edgeThickness, $errorKey) {
    $edgeRadiusRestriction = getEdgeRadiusRestriction();
    $edgeRadiusRestriction = $edgeRadiusRestriction['inner'];

    if ($radiusValue < $edgeRadiusRestriction[$edgeThickness]) {

        foreach ($edgeRadiusRestriction as $key => $value) {
            if ($radiusValue >= $value) $permEdgeT = $key;
            else break;
        }

        // Выводим сообщение об ошибке
        $tmpErr = getErrorMessage($errorKey, LANG);
        // Здесь заменяем маркеры на соответствующие параметры в сообщении
        $tmpErr = str_replace('opCode', $detInfo['opCode'], $tmpErr);
        $tmpErr = str_replace('detName', $detInfo['dName'], $tmpErr);
        $tmpErr = str_replace('detId', $detInfo['detId'], $tmpErr);
        $tmpErr = str_replace('detMatName', $detInfo['dMatName'], $tmpErr);
        $tmpErr = str_replace('detCl', $detInfo['dL'], $tmpErr);
        $tmpErr = str_replace('detCw', $detInfo['dW'], $tmpErr);
        $tmpErr = str_replace('edgeRadius', $radiusValue, $tmpErr);
        $tmpErr = str_replace('edgeThickness', $permEdgeT, $tmpErr);
        $tmpErr = str_replace('opName', $detInfo['opName'], $tmpErr);

        return $tmpErr;
    } else return '';
}
?>