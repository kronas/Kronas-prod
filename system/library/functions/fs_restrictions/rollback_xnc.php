<?php

/**
 * Функция меняет размер детали на изначальный, когда удаляется xnc-операция прямоугольного выреза
 */
function returnToOriginalSize($vals, $allDetails) {

    $message = '';
    $warningNum = '42_rollback';
    $new_vals = $vals;
    $allXncOperations = getXncOperations($vals);
    $allDetailsInfo = getDetailsForRestrictions($vals);

    if (!empty($allXncOperations)) {
        foreach ($allDetails as $valsNum => $oneDetail) {
            $detId = $oneDetail['attributes']['ID'];
            $attr = $oneDetail['attributes'];
            if (isset($attr['PART.MY_CUTTING_BY_DRAWING_L'])
             || isset($attr['PART.MY_CUTTING_BY_DRAWING_R'])
             || isset($attr['PART.MY_CUTTING_BY_DRAWING_B'])
             || isset($attr['PART.MY_CUTTING_BY_DRAWING_T']))
            {
                $tmpFlag = 0;
                foreach ($allXncOperations as $valsKey => $operationArr) {
                    if (false !== stripos($vals[$valsKey]['attributes']['NAME'], 'шабл. 42') || $vals[$valsKey]['attributes']['OPERATION.MY_TEMPLATE'] == 42) {
                        if ($vals[$valsKey + 1]['tag'] === 'PART' && (isset($vals[$valsKey + 1]['attributes']['ID']) && $vals[$valsKey + 1]['attributes']['ID'] == $detId)) {
                            $tmpFlag = 1;
                            break;
                        }
                    }
                }
                if ($tmpFlag === 0) {
                    $rollbackSize = rollbackSize($attr, $allDetailsInfo[$detId], $warningNum);
                    $new_vals[$valsNum]['attributes'] = $rollbackSize['attr'];
                    $message .= $rollbackSize['message'];
                }
            }
        }
    } else {
        foreach ($allDetails as $valsNum => $oneDetail) {
            $detId = $oneDetail['attributes']['ID'];
            $attr = $oneDetail['attributes'];

            if (isset($attr['PART.MY_CUTTING_BY_DRAWING_L'])
             || isset($attr['PART.MY_CUTTING_BY_DRAWING_R'])
             || isset($attr['PART.MY_CUTTING_BY_DRAWING_B'])
             || isset($attr['PART.MY_CUTTING_BY_DRAWING_T']))
            {
                $rollbackSize = rollbackSize($attr, $allDetailsInfo[$detId], $warningNum);
                $new_vals[$valsNum]['attributes'] = $rollbackSize['attr'];
                $message .= $rollbackSize['message'];
            }
        }
    }

    if (!empty($message)) {
        $_SESSION['vals_after_change'] = $new_vals;
        $_SESSION['additions_errors_ok'] .= $message;
    }

    return true;
}

function rollbackSize($attr, $detInfo, $warningNum) {

    $message = '';
    $output = [];

    if (isset($attr['PART.MY_XNC_CUTOUT_DELTAX'])) {
        $attr['L'] -= $attr['PART.MY_XNC_CUTOUT_DELTAX'];
        $attr['DL'] -= $attr['PART.MY_XNC_CUTOUT_DELTAX'];
        if (isset($attr['JL'])) $attr['JL'] -= $attr['PART.MY_XNC_CUTOUT_DELTAX'];
        if (isset($attr['CL'])) $attr['CL'] -= $attr['PART.MY_XNC_CUTOUT_DELTAX'];
    }
    if (isset($attr['PART.MY_XNC_CUTOUT_DELTAY'])) {
        $attr['W'] -= $attr['PART.MY_XNC_CUTOUT_DELTAY'];
        $attr['DW'] -= $attr['PART.MY_XNC_CUTOUT_DELTAY'];
        if (isset($attr['JW'])) $attr['JW'] -= $attr['PART.MY_XNC_CUTOUT_DELTAY'];
        if (isset($attr['CW'])) $attr['CW'] -= $attr['PART.MY_XNC_CUTOUT_DELTAY'];
    }
    unset(
        $attr['PART.MY_CUTTING_BY_DRAWING_SIDE'],
        $attr['PART.MY_CUTTING_BY_DRAWING_L'],
        $attr['PART.MY_CUTTING_BY_DRAWING_R'],
        $attr['PART.MY_CUTTING_BY_DRAWING_B'],
        $attr['PART.MY_CUTTING_BY_DRAWING_T'],
        $attr['PART.MY_CUTTING_BY_DRAWING_D_R'],
        $attr['PART.MY_CUTTING_BY_DRAWING_D_T'],
        $attr['PART.MY_CUTTING_BY_DRAWING_D_L'],
        $attr['PART.MY_CUTTING_BY_DRAWING_D_B'],
        $attr['PART.MY_XNC_CUTOUT_DELTAX'],
        $attr['PART.MY_XNC_CUTOUT_DELTAY']
    );

    $tmpErr = getWarningMessage($warningNum, LANG);
    // Здесь заменяем маркеры на соответствующие параметры в сообщении об ошибке
    $tmpErr = str_replace('detName', $detInfo['detName'], $tmpErr);
    $tmpErr = str_replace('detId', $detInfo['detId'], $tmpErr);
    $tmpErr = str_replace('detMatName', $detInfo['material']['code'] . ' ' . $detInfo['material']['name'], $tmpErr);
    $tmpErr = str_replace('detCl', $detInfo['l'], $tmpErr);
    $tmpErr = str_replace('detCw', $detInfo['w'], $tmpErr);
    $tmpErr = str_replace('detBackCl', $attr['DL'], $tmpErr);
    $tmpErr = str_replace('detBackCw', $attr['DW'], $tmpErr);
    $message = $tmpErr;

    $output['attr'] = $attr;
    $output['message'] = $message;
    return $output;
}


?>