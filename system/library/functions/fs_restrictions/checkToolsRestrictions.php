<?php

/**
 * Функция массив с несуществующими инструментами сверления,
 * (информацию о самих инструментах, деталях и материалах) и возвращает текст ошибки.
 * 
 * Это дополнительные проверки после основных, которые находятся в функции "change_bores()" файла "func_math.php"
 */
function getWrongBoreToolsErrors($wrongBoreTools) {

    $errors = '';

    foreach ($wrongBoreTools as $toolBlock) {
        $tmpErr = getErrorMessage('2', LANG);
        $tmpErr = str_replace('detName', $toolBlock['dName'], $tmpErr);
        $tmpErr = str_replace('detId', $toolBlock['detId'], $tmpErr);
        $tmpErr = str_replace('detMatName', $toolBlock['dMatName'], $tmpErr);
        $tmpErr = str_replace('detCl', $toolBlock['dL'], $tmpErr);
        $tmpErr = str_replace('detCw', $toolBlock['dW'], $tmpErr);
        $tmpErr = str_replace('toolName', $toolBlock['toolName'], $tmpErr);
        $tmpErr = str_replace('toolDiameter', $toolBlock['toolDiam'], $tmpErr);
        $errors .= $tmpErr;
    }

    return $errors;
}

/**
 * Функция записывает в переменную "$wrongBoreTools" несуществующие инструменты сверления, если таковые есть
 * возвращает всегда null
 */
function getWrongBoreTools($vals, $currentOperationIndex, $currentProgram, $allDetails, &$wrongBoreTools) {

    $programIndex = $currentProgram['Pindex'];
    $programVals = $currentProgram['Pvals'];

    // Информация о детали
    $detId = $vals[$currentOperationIndex + 1]['attributes']['ID'];
    $dName = $allDetails[$detId]['detName'];
    $dW = $allDetails[$detId]['w'];
    $dL = $allDetails[$detId]['l'];
    $dMatName = $allDetails[$detId]['material']['name'];

    $q = 'SELECT * FROM `tools`';
    $res = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
    $dbTools = $res['data'];
    $dbToolDiameters = [];
    foreach ($dbTools as $value) {
        $dbToolDiameters[] = $value['diameter'];
    }

    $x = (empty($wrongBoreTools)) ? 0 : (count($wrongBoreTools));
    foreach ($programIndex['TOOL'] as $i) {
        if (false !== stripos($programVals[$i]['attributes']['NAME'], 'bore') && !in_array($programVals[$i]['attributes']['D'], $dbToolDiameters)) {
            $wrongBoreTools[$x]['toolName'] = $programVals[$i]['attributes']['NAME'];
            $wrongBoreTools[$x]['toolDiam'] = $programVals[$i]['attributes']['D'];
            $wrongBoreTools[$x]['detId'] = $detId;
            $wrongBoreTools[$x]['dName'] = $dName;
            $wrongBoreTools[$x]['dW'] = $dW;
            $wrongBoreTools[$x]['dL'] = $dL;
            $wrongBoreTools[$x]['dMatName'] = $dMatName;
            $x++;
        }
    }

    return null;
}

?>