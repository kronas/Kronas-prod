<?php

// Функции для проверки производственных ограничений для сквозного прямоугольного выреза в детали

/**
 * Функция определяет допустимы ли размеры прямоугольного выреза детали
 * Параметры:
 *  $prVals - массив программы
 *  $callIndex - индекс тега CALL, внутри которого параметры
 *  $numStr - блок ограничений по вырезу
 */
function checkRectangularCutoutRestriction($prVals, $callIndex, $numStr) {

    $index = make_index($prVals);
    $Bxx = null;
    if (isset($index['BL']) || isset($index['BB']) || isset($index['BR']) || isset($index['BT'])) {
        $Bxx = 1;
    }

    $output = [
        'correctSizes' => [],
        'new_program_vals' => []
    ];
    $detSizes = [
        'detX' => $prVals[0]['attributes']['DX'], // Размер детали по X
        'detY' => $prVals[0]['attributes']['DY'] // Размер детали по Y
    ];
    
    $restrictionSizes = getTemplateRestriction($numStr); // 'bigSide' & 'smallSide'
    $distances = getSideSizes($prVals, $callIndex, $numStr);
    
    if ($distances === false) {
        return true;
    }

    $detailEnlargement = compareSizes($detSizes, $restrictionSizes, $distances);

    // Проверяем, если есть размеры для увеличения
    if ($detailEnlargement !== null) {
        // Увеличиваем
        if (detailCheckAfterEnlargement($detSizes, $detailEnlargement) && $Bxx === null) {
            $prValsResize = changeSize($prVals, $callIndex, $detailEnlargement);
            $output['correctSizes'] = $detailEnlargement;
            $output['new_program_vals'] = $prValsResize;

            return $output;
        // Иначе выдаём критическую ошибку
        } else {
            return false;
        }
        
    } else return true;
    
}

/**
 * Функция возвращает расстояния от краёв детали до краёв выреза (четыре стороны)
 * Либо false, если вырез не сквозной
 * Вспомогательная функция для checkRectangularCutoutRestriction()
 */
function getSideSizes($prVals, $callIndex, $numStr) {

    $index = make_index($prVals);

    $detX = $prVals[0]['attributes']['DX']; // Размер детали по X
    $detY = $prVals[0]['attributes']['DY']; // Размер детали по Y
    $detZ = $prVals[0]['attributes']['DZ']; // Размер детали по Y
    $needChanges = 0;

    $distanceCenterX = null; // Расстояние до центра выреза по оси X
    $distanceCenterY = null; // Расстояние до центра выреза по оси Y
    $cutoutSizeX = null; // Размер выреза по оси X
    $cutoutSizeY = null; // Размер выреза по оси Y

    // Расстояния от краёв детали до краёв выреза по сторонам
    $distances = [
        'leftSide' => null,
        'bottomSide' => null,
        'rightSide' => null,
        'topSide' => null
    ];

    foreach ($index['MS'] as $key => $i) {
        $dp = calcThickness($detZ, $prVals[$i]['attributes']['DP']);
        if ($dp < $detZ) {
            $needChanges = 1;
            break;
        }
    }

    if ($needChanges === 1) {
        return false;
    }

    $i = $callIndex + 1;
    while ($prVals[$i]['tag'] == 'PAR') {
        switch ($prVals[$i]['attributes']['NAME']) {
            case 'X':
                $distanceCenterX = $prVals[$i]['attributes']['VALUE'];
                break;
            case 'Y':
                $distanceCenterY = $prVals[$i]['attributes']['VALUE'];
                break;
            case 'VX':
                $cutoutSizeX = $prVals[$i]['attributes']['VALUE'];
                break;
            case 'VY':
                $cutoutSizeY = $prVals[$i]['attributes']['VALUE'];
                break;
        }
        $i++;
    }

    // Определяем расстояния от краёв детали до краёв выреза по сторонам (левая, нижняя, правая, верхняя)
    $distances['leftSide'] = $distanceCenterX - ($cutoutSizeX / 2);
    $distances['bottomSide'] = $distanceCenterY - ($cutoutSizeY / 2);
    $distances['rightSide'] = ($detX - $distanceCenterX) - ($cutoutSizeX / 2);
    $distances['topSide'] = ($detY - $distanceCenterY) - ($cutoutSizeY / 2);

    return $distances;
}

/**
 * Функция сравнивает размеры от краёв детали до краёв выреза с допустимыми размерами
 * Возвращает массив с размерами, на которые нужно увеличить стороны в случае,
 * если их нужно увеличивать.
 * Иначе возвращает NULL
 * Вспомогательная функция для checkRectangularCutoutRestriction()
 */
function compareSizes($detSizes, $restrictionSizes, $distances) {

    $output = [];
    $sides = [
        'leftSide' => null,
        'bottomSide' => null,
        'rightSide' => null,
        'topSide' => null
    ];

    $detX = $detSizes['detX'];
    $detY = $detSizes['detY'];

    // Определяем бОльшую и меньшую стороны
    // Если стороны детали равны, то параметры ниже это также описывают
    $bigDetSide = ($detX >= $detY) ? $detX : $detY;
    $smallDetSide = ($detY <= $detX) ? $detY : $detX;

    // Проверки в зависимости от поворота детали
    $r1 = $restrictionSizes['smallSide'];
    $r2 = $restrictionSizes['bigSide'];
    if ($bigDetSide === $detY) {
        $r1 = $restrictionSizes['bigSide'];
        $r2 = $restrictionSizes['smallSide'];
    }
 
    if ($distances['leftSide'] < $r1) $sides['leftSide'] = $r1 - $distances['leftSide'];
    if ($distances['bottomSide'] < $r2) $sides['bottomSide'] = $r2 - $distances['bottomSide'];
    if ($distances['rightSide'] < $r1) $sides['rightSide'] = $r1 - $distances['rightSide'];
    if ($distances['topSide'] < $r2) $sides['topSide'] = $r2 - $distances['topSide'];

    foreach ($sides as $side => $value) {
        if ($value !== null) {
            $output[$side] = $value;
        }
    }

    if (!empty($output)) return $output;
    else return null;
}

/**
 * Функция проверяет возможно ли увеличить деталь
 * (т.е. не будет ли деталь после увеличения больше допустимого размера)
 * Если можно увеличить, функция возвращает TRUE, иначе FALSE
 * Вспомогательная функция для checkRectangularCutoutRestriction()
 */
function detailCheckAfterEnlargement($detSizes, $detailEnlargement) {

    $place = $_SESSION['user_place'];

    $detX = $detSizes['detX'];
    $detY = $detSizes['detY'];

    $millRestriction = getMillingRestriction();
    $millRestriction = $millRestriction['max'];

    $bigSizeMin = ($place == 1 || $place == 3) ? $millRestriction['enlargedSmallSide'] : $millRestriction['smallSide'];
    $bigSizeMax = $millRestriction['bigSide'];

    foreach ($detailEnlargement as $side => $value) {
        if ($side === 'leftSide' || $side === 'rightSide') {
            $detX += $value;
        } elseif ($side === 'topSide' || $side === 'bottomSide') {
            $detY += $value;
        }
    }

    // Определяем бОльшую и меньшую стороны
    // Если стороны детали равны, то параметры ниже это также описывают
    $bigDetSide = ($detX >= $detY) ? $detX : $detY;
    $smallDetSide = ($detY <= $detX) ? $detY : $detX;

    // Если можно увеличить - TRUE, иначе FALSE
    if ($SmallDetSide > $bigSizeMin || $bigDetSide > $bigSizeMax) return false;
    else return true;
}

// Функция увеличивает размер детали и возвращает массив программы с новыми (увеличенными) размерами
function changeSize($prVals, $callIndex, $detailEnlargement) {

    $output = $prVals;
    $i = $callIndex + 1;
    while ($prVals[$i]['tag'] == 'PAR') {

        // Размер детали при увеличении растёт вверх и вправо, поэтому размеры сверху справа добавляем как есть,
        // а размеры слева и снизу добавляем с коррекцией расстояния до центра выреза
        switch ($prVals[$i]['attributes']['NAME']) {
            case 'X': // Расстояние до центра выреза по оси X
                foreach ($detailEnlargement as $side => $additionalSize) {
                    if ($side === 'leftSide' || $side === 'rightSide') {
                        $t = (strripos($output[0]['attributes']['DX'], '.')) ? (double)$output[0]['attributes']['DX'] : (int)$output[0]['attributes']['DX'];
                        $t += $additionalSize;
                        $output[0]['attributes']['DX'] = (string)$t;

                        if ($side === 'leftSide') {
                            $t = (strripos($output[$i]['attributes']['VALUE'], '.')) ? (double)$output[$i]['attributes']['VALUE'] : (int)$output[$i]['attributes']['VALUE'];
                            $t += $additionalSize;
                            $output[$i]['attributes']['VALUE'] = (string)$t;
                        }
                    }
                }
                break;
            case 'Y': // Расстояние до центра выреза по оси Y
                foreach ($detailEnlargement as $side => $additionalSize) {
                    if ($side === 'bottomSide' || $side === 'topSide') {
                        $t = (strripos($output[0]['attributes']['DY'], '.')) ? (double)$output[0]['attributes']['DY'] : (int)$output[0]['attributes']['DY'];
                        $t += $additionalSize;
                        $output[0]['attributes']['DY'] = (string)$t;

                        if ($side === 'bottomSide') {
                            $t = (strripos($output[$i]['attributes']['VALUE'], '.')) ? (double)$output[$i]['attributes']['VALUE'] : (int)$output[$i]['attributes']['VALUE'];
                            $t += $additionalSize;
                            $output[$i]['attributes']['VALUE'] = (string)$t;
                        }
                    }
                }
                break;
        }
        $i++;
    }
    return $output;
}

/**
 * Функция вычисляет глубину фрезерования
 */
function calcThickness($detZ, $msdp) {

    if (false !== stripos($msdp, 'dz')) {

        $additionalDp = preg_replace('#^[^\d]+#', '', $msdp);

        if (false !== stripos($msdp, '+')) {
            $dp = $detZ + $additionalDp;
        } elseif (false !== stripos($msdp, '-')) {
            $dp = $detZ - $additionalDp;
        }

        $msdp = $dp;
    }

    return $msdp;

}

?>