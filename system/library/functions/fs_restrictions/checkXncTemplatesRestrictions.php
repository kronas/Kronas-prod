<?php

require_once DIR_FUNCTIONS . 'fs_restrictions/0042.php';
require_once DIR_FUNCTIONS . 'fs_restrictions/check_min_radiuses.php';
require_once DIR_FUNCTIONS . 'fs_restrictions/radiusHandles.php';

/**
 * Функция обрабатывает ограничения XNC-шаблонов (вида [/0000/])
 * Параметры:
 *  1) массив проекта
 *  2) индекс текущей XNC-операции
 *  3) массив программы (в параметре массив индексов ("Pvals") и массив программы ("Pindex"))
 *  4) массив деталей с необходимыми данными
 *  5) филиал
 * 
 * Возвращает ошибки, либо пустую строку.
 */
function checkXncTemplatesRestrictions($vals, $currentOperationIndex, $currentProgram, $allDetails, $place = 1) {
    
    $errors = '';

    $index = make_index($vals);
    $programIndex = $currentProgram['Pindex'];
    $programVals = $currentProgram['Pvals'];

    // Информация о детали
    $detNumOfVals = null;
    $detId = $vals[$currentOperationIndex + 1]['attributes']['ID'];
    $dName = $allDetails[$detId]['detName'];
    $dW = $allDetails[$detId]['w'];
    $dL = $allDetails[$detId]['l'];
    $dT = $allDetails[$detId]['material']['t'];
    $dMatName = $allDetails[$detId]['material']['name'];

    // Порядковый номер детали в массиве $vals
    foreach ($index['PART'] as $valsIndex) {
        if ($vals[$valsIndex]['attributes']['ID'] == $detId) {
            $detNumOfVals = $valsIndex;
            break;
        }
    }

    // Информация о детали в виде массива
    $detInfo = [
        'detNumOfVals' => $detNumOfVals,
        'detId' => $detId,
        'dName' => $dName,
        'dW' => $dW,
        'dL' => $dL,
        'dT' => $dT,
        'dMatName' => $dMatName
    ];

    foreach ($programIndex['CALL'] as $vals_i) {
        if ($programVals[$vals_i]['type'] == 'open') {
            if (preg_match('#(/\d{4}/)#', $programVals[$vals_i]['attributes']['NAME'], $matches)) {
                $tmplStr = (string)str_replace('/', '', $matches[0]);
                $tmplNum = (int)str_replace('/', '', $matches[0]);
                switch ($tmplNum) {
                    // [/0042/] Отверстие прямоугольное для ЛДСП 18мм от 150x100мм.xnc
                    case 42:
                        if (false === strpos($dMatName, 'ПФ ') && false === strpos($dMatName, 'Compact') && $detInfo['dT'] < 28) {
                            $rSizes = getTemplateRestriction($tmplStr); // 'bigSide' & 'smallSide'

                            // Блок для вывода максимальных размеров детали в сообщении
                            $millRestriction = getMillingRestriction();
                            $millRestriction = $millRestriction['max'];
                            // Размеры в зависимости от филиала
                            $bigSizeMin = ($place == 1 || $place == 3) ? $millRestriction['enlargedSmallSide'] : $millRestriction['smallSide'];
                            $bigSizeMax = $millRestriction['bigSide'];
                            // Определяем бОльшую и меньшую стороны
                            $s1 = ($dL >= $dW) ? $bigSizeMax : $bigSizeMin;
                            $s2 = ($dW <= $dL) ? $bigSizeMin : $bigSizeMax;

                            // Параметры находятся внутри тега CALL,
                            // поэтому передаём его индекс и внутри фнкции пробежимся по параметрам
                            // а также передаём номер шаблона (type string 0042), в файле настроек под этим именем хранятся ограничения
                            $increase = checkRectangularCutoutRestriction($programVals, $vals_i, $tmplStr);
                            if ($increase === false) {
                                $tmpErr = getErrorMessage($tmplNum, LANG);
                                // Здесь заменяем маркеры на соответствующие параметры в сообщении об ошибке
                                $tmpErr = str_replace('detName', $dName, $tmpErr);
                                $tmpErr = str_replace('detId', $detId, $tmpErr);
                                $tmpErr = str_replace('detMatName', $dMatName, $tmpErr);
                                $tmpErr = str_replace('detCl', $dL, $tmpErr);
                                $tmpErr = str_replace('detCw', $dW, $tmpErr);
                                $tmpErr = str_replace('bigSide', $rSizes['bigSide'], $tmpErr);
                                $tmpErr = str_replace('smallSide', $rSizes['smallSide'], $tmpErr);
                                $tmpErr = str_replace('bigDetSide', $s1, $tmpErr);
                                $tmpErr = str_replace('smallDetSide', $s2, $tmpErr);

                                $errors = $tmpErr;
                            } elseif ($increase !== false && $increase !== true) {
                                $corrSizes = $increase['correctSizes'];
                                $new_vals = $vals;
                                $new_vals[$currentOperationIndex]['attributes']['PROGRAM'] = vals_index_to_project($increase['new_program_vals']);

                                foreach ($index['PART'] as $arrNum) {
                                    if ($new_vals[$arrNum]['type'] === 'complete'
                                        && (isset($new_vals[$arrNum]['attributes']['DL']) && isset($new_vals[$arrNum]['attributes']['DW']))
                                        && $new_vals[$arrNum]['attributes']['ID'] == $detId) {

                                        $attr = $new_vals[$arrNum]['attributes'];
                                        $edgesByDetail = edgesByDetail($vals, $attr);

                                        // Здесь вычисляется размер, на который увеличивается деталь
                                        $deltaX = 0;
                                        $deltaY = 0;
                                        if (isset($increase['correctSizes']['leftSide']) && isset($increase['correctSizes']['rightSide'])) {
                                            $deltaX = $increase['correctSizes']['leftSide'] + $increase['correctSizes']['rightSide'];
                                        } elseif (isset($increase['correctSizes']['leftSide']) || isset($increase['correctSizes']['rightSide'])) {
                                            $deltaX = isset($increase['correctSizes']['leftSide']) ? $increase['correctSizes']['leftSide'] : $increase['correctSizes']['rightSide'];
                                        }

                                        if (isset($increase['correctSizes']['bottomSide']) && isset($increase['correctSizes']['topSide'])) {
                                            $deltaY = $increase['correctSizes']['bottomSide'] + $increase['correctSizes']['topSide'];
                                        } elseif (isset($increase['correctSizes']['bottomSide']) || isset($increase['correctSizes']['topSide'])) {
                                            $deltaY = isset($increase['correctSizes']['bottomSide']) ? $increase['correctSizes']['bottomSide'] : $increase['correctSizes']['topSide'];
                                        }

                                        if ($deltaX !== 0) {
                                            
                                            $attr['PART.MY_XNC_CUTOUT_DELTAX'] = $deltaX;

                                            // ключ "PART.MY_CUTTING_BY_DRAWING_SIDE" нужен для корректного отображения на этикетке
                                            if (isset($corrSizes['leftSide']) || isset($corrSizes['rightSide']) || isset($corrSizes['bottomSide']) || isset($corrSizes['topSide'])) {
                                                $attr['PART.MY_CUTTING_BY_DRAWING_SIDE'] = 1;
                                            }

                                            if (isset($corrSizes['leftSide'])) {
                                                if (isset($edgesByDetail['L'])) {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_L'] = $attr['CL'];
                                                } else {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_L'] = $attr['DL'];
                                                }
                                                $attr['PART.MY_CUTTING_BY_DRAWING_L'] = round($attr['PART.MY_CUTTING_BY_DRAWING_L'], 1);
                                                $attr['PART.MY_CUTTING_BY_DRAWING_D_L'] = $attr['DL'];
                                            }
                                            if (isset($corrSizes['rightSide'])) {
                                                if (isset($edgesByDetail['R'])) {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_R'] = $attr['CL'] + (isset($corrSizes['leftSide']) ? $corrSizes['leftSide'] : 0);
                                                } else {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_R'] = $attr['DL'] + (isset($corrSizes['leftSide']) ? $corrSizes['leftSide'] : 0);
                                                }
                                                $attr['PART.MY_CUTTING_BY_DRAWING_R'] = round($attr['PART.MY_CUTTING_BY_DRAWING_R'], 1);
                                                $attr['PART.MY_CUTTING_BY_DRAWING_D_R'] = $attr['DL'];
                                            }

                                            $attr['L'] += $deltaX;
                                            $attr['DL'] += $deltaX;
                                            $attr['JL'] += $deltaX;
                                            $attr['CL'] += $deltaX;
                                        }

                                        if ($deltaY !== 0) {
                                            
                                            $attr['PART.MY_XNC_CUTOUT_DELTAY'] = $deltaY;

                                            if (isset($corrSizes['bottomSide'])) {
                                                if (isset($edgesByDetail['B'])) {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_B'] = $attr['CW'];
                                                } else {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_B'] = $attr['DW'];
                                                }
                                                $attr['PART.MY_CUTTING_BY_DRAWING_B'] = round($attr['PART.MY_CUTTING_BY_DRAWING_B'], 1);
                                                $attr['PART.MY_CUTTING_BY_DRAWING_D_B'] = $attr['DW'];
                                            }
                                            if (isset($corrSizes['topSide'])) {
                                                if (isset($edgesByDetail['T'])) {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_T'] = $attr['CW'] + (isset($corrSizes['bottomSide']) ? $corrSizes['bottomSide'] : 0);
                                                } else {
                                                    $attr['PART.MY_CUTTING_BY_DRAWING_T'] = $attr['DW'] + (isset($corrSizes['bottomSide']) ? $corrSizes['bottomSide'] : 0);
                                                }
                                                $attr['PART.MY_CUTTING_BY_DRAWING_T'] = round($attr['PART.MY_CUTTING_BY_DRAWING_T'], 1);
                                                $attr['PART.MY_CUTTING_BY_DRAWING_D_T'] = $attr['DW'];
                                            }

                                            $attr['W'] += $deltaY;
                                            $attr['DW'] += $deltaY;
                                            $attr['JW'] += $deltaY;
                                            $attr['CW'] += $deltaY;
                                        }

                                        $new_vals[$arrNum]['attributes'] = $attr;
                                        $new_vals[$currentOperationIndex]['attributes']['PROGRAM'] = vals_index_to_project($increase['new_program_vals']);
                                        $_SESSION['vals_after_change'] = $new_vals;
                                        
                                        $tmpErr = getWarningMessage($tmplNum, LANG);
                                        // Здесь заменяем маркеры на соответствующие параметры в сообщении об ошибке
                                        $tmpErr = str_replace('detName', $dName, $tmpErr);
                                        $tmpErr = str_replace('detId', $detId, $tmpErr);
                                        $tmpErr = str_replace('detMatName', $dMatName, $tmpErr);
                                        $tmpErr = str_replace('detCl', $dL, $tmpErr);
                                        $tmpErr = str_replace('detCw', $dW, $tmpErr);
                                        $tmpErr = str_replace('bigSide', $rSizes['bigSide'], $tmpErr);
                                        $tmpErr = str_replace('smallSide', $rSizes['smallSide'], $tmpErr);
                                        $_SESSION['additions_errors_ok'] .= $tmpErr;

                                        break;   
                                    }
                                }
                            }
                        }
                        break;
                    // [/0019/], [/0020/], [/0021/], [/0022/], [/0028/] Шаблоны зарезов без параметра радиуса (радиус стандартный, 10мм)
                    case 19: // Зарез на левом нижнем углу
                    case 20: // Зарез на нижнем правом углу
                    case 21: // Зарез на правом верхнем углу
                    case 22: // Зарез на верхнем левом углу
                    case 28: // П-образный зарез на нижней стороне
                        $detInfo['opCode'] = $vals[$currentOperationIndex]['attributes']['CODE'];
                        $errors = checkCutoutDiameters($vals, $detInfo, $tmplNum, $programVals);
                        break;
                    // [/0023/], [/0024/], [/0025/], [/0026/], [/0027/],, [/0029/], [/0095/] Шаблоны зарезов с параметром радиуса
                    case 23: // Зарез на левом нижнем углу
                    case 24: // Зарез на нижнем правом углу
                    case 25: // Зарез на правом верхнем углу
                    case 26: // Зарез на верхнем левом углу
                    case 27: // П-образный зарез на нижней стороне
                    case 29: // Зарезы на четырёх углах
                    case 95: // П-образный зарез на верхней стороне
                        $detInfo['opCode'] = $vals[$currentOperationIndex]['attributes']['CODE'];
                        $errors = checkCutoutDiameters($vals, $detInfo, $tmplNum, $programVals, true);
                        break;
                    case 32: // Радиусные ручки с установкой радиуса скругления
                        $detInfo['opCode'] = $vals[$currentOperationIndex]['attributes']['CODE'];
                        $errors = getRadiusHandlesRestrictions($vals, $detInfo, $tmplNum, $programVals);
                        break;
                    default:
                        // code...
                        break;
                }
            }
        }
    }

    return $errors;
}

?>