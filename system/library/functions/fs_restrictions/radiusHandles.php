<?php

/**
 * Функция проверяет соотношение радиуса к толщине кромки
 * в радиусных ручках с устанавливаемым радиусом скругления
 * шаблон [/0032/]
 */
function getRadiusHandlesRestrictions($vals, $detInfo, $tmplNum, $programVals) {

    $output = '';
    $bandT = 0;
    $handleRadius = 0;
    $allowBand = 0;
    $pIndex = make_index($programVals);
    $radRestrictions = getRadiusHandlesRestriction();
    $edgesByDetail = edgesByDetail($vals, $vals[$detInfo['detNumOfVals']]['attributes']);

    if (isset($edgesByDetail['B'])) {
        $bandT = $edgesByDetail['B'];
        foreach ($pIndex['PAR'] as $arrNum) {

            if (!isset($programVals[$arrNum]['attributes']['TYPE']) && $programVals[$arrNum]['attributes']['COMMENT'] === 'Радиус_скругления_переходов') {
                $handleRadius = $programVals[$arrNum]['attributes']['VALUE'];
                break;
            }

        }

        if ($handleRadius >= 20) {

            foreach ($radRestrictions['bands'] as $key => $value) {
                if (isset($radRestrictions['bands'][$key+1])) {
                    if ($bandT == $value) {
                        break;
                    } elseif ($bandT > $radRestrictions['bands'][$key] && $bandT <= $radRestrictions['bands'][$key+1]) {
                        $bandT = $radRestrictions['bands'][$key+1];
                        break;
                    }
                } else {
                    if ($bandT > $value) {
                        $bandT = $value;
                        break;
                    }
                }
            }

            foreach ($radRestrictions['bands'] as $key => $value) {
                if ($bandT == $value) {
                    if ($handleRadius < $radRestrictions['radiuses'][$key]) {
                        // Выводим сообщение об ошибке
                        $tmpErr = getErrorMessage($tmplNum, LANG);
                        // Здесь заменяем маркеры на соответствующие параметры в сообщении
                        $tmpErr = str_replace('opName', $detInfo['opCode'], $tmpErr);
                        $tmpErr = str_replace('detName', $detInfo['dName'], $tmpErr);
                        $tmpErr = str_replace('detId', $detInfo['detId'], $tmpErr);
                        $tmpErr = str_replace('detMatName', $detInfo['dMatName'], $tmpErr);
                        $tmpErr = str_replace('detCl', $detInfo['dL'], $tmpErr);
                        $tmpErr = str_replace('detCw', $detInfo['dW'], $tmpErr);
                        $tmpErr = str_replace('edgeRadius', $handleRadius, $tmpErr);
                        $tmpErr = str_replace('edgeThickness', $radRestrictions['bands'][$key-1], $tmpErr);
                        $output = $tmpErr;
                    }
                }
            }
        } else {
            // Выводим сообщение об ошибке
            $tmpErr = getErrorMessage($tmplNum . '_1', LANG);
            // Здесь заменяем маркеры на соответствующие параметры в сообщении
            $tmpErr = str_replace('opName', $detInfo['opCode'], $tmpErr);
            $tmpErr = str_replace('detName', $detInfo['dName'], $tmpErr);
            $tmpErr = str_replace('detId', $detInfo['detId'], $tmpErr);
            $tmpErr = str_replace('detMatName', $detInfo['dMatName'], $tmpErr);
            $tmpErr = str_replace('detCl', $detInfo['dL'], $tmpErr);
            $tmpErr = str_replace('detCw', $detInfo['dW'], $tmpErr);
            $output = $tmpErr;
        }


    }

    return $output;
}

?>