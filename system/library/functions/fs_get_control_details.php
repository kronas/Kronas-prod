<?php

$stickers = [
    'opOpen' => '<operation printmode="0" columns="1" rows="1" gap="0" margin="false" border="0" source="0" printparts="true" printwastes="true" parteach="false" partonz="false" partselected="false" wasteeach="false" producteach="false" productonz="false" fields="BASE64:PGZpZWxkcz48ZmllbGQgaWQ9IjEiIG5hbWU9InF0eSIgdHlwZT0iaW50ZWdlciIgd2lkdGg9IjEwIiBkZWZhdWx0PSIxIi8+PC9maWVsZHM+" data="AQAAAAEBAAAAAQ==" program="&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&lt;program printMode=&quot;0&quot; columns=&quot;1&quot; rows=&quot;1&quot; gap=&quot;0&quot; margin=&quot;false&quot; dx=&quot;210&quot; dy=&quot;297&quot;&gt;',
    'st0' => "&lt;image enable=&quot;true&quot; x=&quot;0&quot; y=&quot;0&quot; w=&quot;99&quot; h=&quot;58&quot; imgdata=&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAREAAACgAQAAAAADDmVHAAAACXBIWXMAAArEAAAKxAFmbYLUAAAKZmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNy4xLWMwMDAgNzkuYTg3MzFiOSwgMjAyMS8wOS8wOS0wMDozNzozOCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMy0wMy0wOFQxMTowNzo1NyswMjowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMy0wMy0wOFQxOTo1MDo0MSswMjowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjMtMDMtMDhUMTk6NTA6NDErMDI6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM1MDY5MjQyLThlNTUtN2Q0Ni1hMGEyLTU5NGUxNDZkZjQ3NSIgeG1wTU06RG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjEzNjIwMmViLWU4MjUtOGE0Mi05MjI3LTM3NjBmMzY3NjUyOSIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjI5Njc4Mzg4LThkYzEtMTY0MC05NTBiLWQwYWU4YmU3ZGY4YSIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMCIgdGlmZjpPcmllbnRhdGlvbj0iMSIgdGlmZjpYUmVzb2x1dGlvbj0iNzAwMDAwLzEwMDAwIiB0aWZmOllSZXNvbHV0aW9uPSI3MDAwMDAvMTAwMDAiIHRpZmY6UmVzb2x1dGlvblVuaXQ9IjIiIGV4aWY6Q29sb3JTcGFjZT0iNjU1MzUiIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSIyNzMiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSIxNjAiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjI5Njc4Mzg4LThkYzEtMTY0MC05NTBiLWQwYWU4YmU3ZGY4YSIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wOFQxMTowNzo1NyswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpjZGYzNTI3Yi0xYTJmLTU3NDYtYWJiMy1jOWM0Mjk5NGNjYTQiIHN0RXZ0OndoZW49IjIwMjMtMDMtMDhUMTE6MTU6NTIrMDI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMy4wIChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTI4Zjg2ODYtZTBhZS1jYTRkLTgyODgtOGExYjU4NWVmY2I1IiBzdEV2dDp3aGVuPSIyMDIzLTAzLTA4VDE5OjUwOjQxKzAyOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjMuMCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNvbnZlcnRlZCIgc3RFdnQ6cGFyYW1ldGVycz0iZnJvbSBhcHBsaWNhdGlvbi92bmQuYWRvYmUucGhvdG9zaG9wIHRvIGltYWdlL3BuZyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iZGVyaXZlZCIgc3RFdnQ6cGFyYW1ldGVycz0iY29udmVydGVkIGZyb20gYXBwbGljYXRpb24vdm5kLmFkb2JlLnBob3Rvc2hvcCB0byBpbWFnZS9wbmciLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjM1MDY5MjQyLThlNTUtN2Q0Ni1hMGEyLTU5NGUxNDZkZjQ3NSIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wOFQxOTo1MDo0MSswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MjhmODY4Ni1lMGFlLWNhNGQtODI4OC04YTFiNTg1ZWZjYjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Mjk2NzgzODgtOGRjMS0xNjQwLTk1MGItZDBhZThiZTdkZjhhIiBzdFJlZjpvcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6Mjk2NzgzODgtOGRjMS0xNjQwLTk1MGItZDBhZThiZTdkZjhhIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Szp7aQAAA3NJREFUWMPtmM9LFFEcwKcMN6HWwIvU5oQVREEkgmwXVyHqHNQtyvIgpK5FuCo1TmBgh9A/IMprUAgRTLlbzYbiHqK1U0KL+9JDRjo7boKbjL7Xm12qNzsz7z11Wgp6l2GXD9/fP96MIHAcGf06AFnPeuHxn3FjZjmYdP7RTmXUpmB17Rm6HPV88KtiMJiec5kVNuPPjrOYDrgSY/iV68iwmNm5YOBKs4e5eMjBhFnMYQ5GEG5yML1+DjkZbxhCV9ytl7l0QQ5djRxy0hy5YNrTzsEIwi2/N/nyiuGpwz64obynBY4akzjqR7oXuFwl0u2RIgujz/Vi5qggW5nXdgbn4iLJfDk5XEmPIZYDFZ3JjDKYdHAocL3K5xCf7UScIUefQo4eXJM336c8NTbE6mWcizmOOoSlYzY4o7bWg/1WJuPENFp7J+s0N5Ls3iHnjzR5aOdEhVjEHLPMHykWng6ndHo9Y6aDydS8v5qoZMr5/FGnzh8p1jkddmDI2RKcPHh3osL3x/Il95dqn9Zz1eEG7wmu+/QfvydsTRdzJuwv4T3BOqPeuN0Tan4zr3h2ZfmJltMHTsk0Rh5LKbHIgspm3qo0e+Q7+5SB8kchml/SmKGokXa6X2OpUbs91yz2jJcHL7D8Qshwjs8im+kVSCbrLGfNm73TB/+uviDscZkbR0q6K0s3e/niw5NTsp55dCU4GN2RabMw23zVrT78pk2xJ52JLjyIah8Mil+Y0R2ZrgzJ+Juji3XjxUzbEGFPFI1gOTFbjRE51Qv22Jh+Is6JwUDrYHVtczETJvOFJ4bqEMMWP5vpIuXg65G4qff33Z5+k1kegTe8YPJnPkRlcgkoYyZO+4a2JEKDpSsHYBmdWR7BjIHm6cwSwPboKl2XiIf3Jze/zmImB1AClRFyDNUmZ0lcDcEUtiddX/jz55sRweTACsAM9qvH1Z4cyjNZGYR3NdXF99TWbW+16ZKxRfNIU0FXREtqse6Usm6Toy6bRmJGjLxLas+6Uw02Jr+3jIKcJ8e1xzv2Pp21xweA72VYJeiJdCa1+92rykwxA0BcXDWQOowZLTlj2mNjpsRpAEPodihdj/16YfrVUsx8M7foFFJxnGWn+DRgZt38QWFMOTMAIqxrQDU1ujBxEVzCNr+k1YbpF/Yd0WseABxDNOzKcJwfprM2T80/ZJAAAAAASUVORK5CYII=&quot; expr=&quot;&quot; ratio=&quot;false&quot; iw=&quot;273&quot; ih=&quot;160&quot;/&gt;",
    'st1' => "&lt;image enable=&quot;true&quot; x=&quot;102&quot; y=&quot;0&quot; w=&quot;99&quot; h=&quot;58&quot; imgdata=&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAREAAACgAQAAAAADDmVHAAAACXBIWXMAAArEAAAKxAFmbYLUAAAKZmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNy4xLWMwMDAgNzkuYTg3MzFiOSwgMjAyMS8wOS8wOS0wMDozNzozOCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMy0wMy0wOFQxMTowNzo1NyswMjowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMy0wMy0wOFQxOTo1MDo0MSswMjowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjMtMDMtMDhUMTk6NTA6NDErMDI6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM1MDY5MjQyLThlNTUtN2Q0Ni1hMGEyLTU5NGUxNDZkZjQ3NSIgeG1wTU06RG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjEzNjIwMmViLWU4MjUtOGE0Mi05MjI3LTM3NjBmMzY3NjUyOSIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjI5Njc4Mzg4LThkYzEtMTY0MC05NTBiLWQwYWU4YmU3ZGY4YSIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMCIgdGlmZjpPcmllbnRhdGlvbj0iMSIgdGlmZjpYUmVzb2x1dGlvbj0iNzAwMDAwLzEwMDAwIiB0aWZmOllSZXNvbHV0aW9uPSI3MDAwMDAvMTAwMDAiIHRpZmY6UmVzb2x1dGlvblVuaXQ9IjIiIGV4aWY6Q29sb3JTcGFjZT0iNjU1MzUiIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSIyNzMiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSIxNjAiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjI5Njc4Mzg4LThkYzEtMTY0MC05NTBiLWQwYWU4YmU3ZGY4YSIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wOFQxMTowNzo1NyswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpjZGYzNTI3Yi0xYTJmLTU3NDYtYWJiMy1jOWM0Mjk5NGNjYTQiIHN0RXZ0OndoZW49IjIwMjMtMDMtMDhUMTE6MTU6NTIrMDI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMy4wIChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTI4Zjg2ODYtZTBhZS1jYTRkLTgyODgtOGExYjU4NWVmY2I1IiBzdEV2dDp3aGVuPSIyMDIzLTAzLTA4VDE5OjUwOjQxKzAyOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjMuMCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNvbnZlcnRlZCIgc3RFdnQ6cGFyYW1ldGVycz0iZnJvbSBhcHBsaWNhdGlvbi92bmQuYWRvYmUucGhvdG9zaG9wIHRvIGltYWdlL3BuZyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iZGVyaXZlZCIgc3RFdnQ6cGFyYW1ldGVycz0iY29udmVydGVkIGZyb20gYXBwbGljYXRpb24vdm5kLmFkb2JlLnBob3Rvc2hvcCB0byBpbWFnZS9wbmciLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjM1MDY5MjQyLThlNTUtN2Q0Ni1hMGEyLTU5NGUxNDZkZjQ3NSIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wOFQxOTo1MDo0MSswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MjhmODY4Ni1lMGFlLWNhNGQtODI4OC04YTFiNTg1ZWZjYjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Mjk2NzgzODgtOGRjMS0xNjQwLTk1MGItZDBhZThiZTdkZjhhIiBzdFJlZjpvcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6Mjk2NzgzODgtOGRjMS0xNjQwLTk1MGItZDBhZThiZTdkZjhhIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Szp7aQAAA3NJREFUWMPtmM9LFFEcwKcMN6HWwIvU5oQVREEkgmwXVyHqHNQtyvIgpK5FuCo1TmBgh9A/IMprUAgRTLlbzYbiHqK1U0KL+9JDRjo7boKbjL7Xm12qNzsz7z11Wgp6l2GXD9/fP96MIHAcGf06AFnPeuHxn3FjZjmYdP7RTmXUpmB17Rm6HPV88KtiMJiec5kVNuPPjrOYDrgSY/iV68iwmNm5YOBKs4e5eMjBhFnMYQ5GEG5yML1+DjkZbxhCV9ytl7l0QQ5djRxy0hy5YNrTzsEIwi2/N/nyiuGpwz64obynBY4akzjqR7oXuFwl0u2RIgujz/Vi5qggW5nXdgbn4iLJfDk5XEmPIZYDFZ3JjDKYdHAocL3K5xCf7UScIUefQo4eXJM336c8NTbE6mWcizmOOoSlYzY4o7bWg/1WJuPENFp7J+s0N5Ls3iHnjzR5aOdEhVjEHLPMHykWng6ndHo9Y6aDydS8v5qoZMr5/FGnzh8p1jkddmDI2RKcPHh3osL3x/Il95dqn9Zz1eEG7wmu+/QfvydsTRdzJuwv4T3BOqPeuN0Tan4zr3h2ZfmJltMHTsk0Rh5LKbHIgspm3qo0e+Q7+5SB8kchml/SmKGokXa6X2OpUbs91yz2jJcHL7D8Qshwjs8im+kVSCbrLGfNm73TB/+uviDscZkbR0q6K0s3e/niw5NTsp55dCU4GN2RabMw23zVrT78pk2xJ52JLjyIah8Mil+Y0R2ZrgzJ+Juji3XjxUzbEGFPFI1gOTFbjRE51Qv22Jh+Is6JwUDrYHVtczETJvOFJ4bqEMMWP5vpIuXg65G4qff33Z5+k1kegTe8YPJnPkRlcgkoYyZO+4a2JEKDpSsHYBmdWR7BjIHm6cwSwPboKl2XiIf3Jze/zmImB1AClRFyDNUmZ0lcDcEUtiddX/jz55sRweTACsAM9qvH1Z4cyjNZGYR3NdXF99TWbW+16ZKxRfNIU0FXREtqse6Usm6Toy6bRmJGjLxLas+6Uw02Jr+3jIKcJ8e1xzv2Pp21xweA72VYJeiJdCa1+92rykwxA0BcXDWQOowZLTlj2mNjpsRpAEPodihdj/16YfrVUsx8M7foFFJxnGWn+DRgZt38QWFMOTMAIqxrQDU1ujBxEVzCNr+k1YbpF/Yd0WseABxDNOzKcJwfprM2T80/ZJAAAAAASUVORK5CYII=&quot; expr=&quot;&quot; ratio=&quot;false&quot; iw=&quot;273&quot; ih=&quot;160&quot;/&gt;",
    'st2' => "&lt;image enable=&quot;true&quot; x=&quot;0&quot; y=&quot;61&quot; w=&quot;99&quot; h=&quot;58&quot; imgdata=&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAREAAACgAQAAAAADDmVHAAAACXBIWXMAAArEAAAKxAFmbYLUAAAKZmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNy4xLWMwMDAgNzkuYTg3MzFiOSwgMjAyMS8wOS8wOS0wMDozNzozOCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMy0wMy0wOFQxMTowNzo1NyswMjowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMy0wMy0wOFQxOTo1MDo0MSswMjowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjMtMDMtMDhUMTk6NTA6NDErMDI6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM1MDY5MjQyLThlNTUtN2Q0Ni1hMGEyLTU5NGUxNDZkZjQ3NSIgeG1wTU06RG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjEzNjIwMmViLWU4MjUtOGE0Mi05MjI3LTM3NjBmMzY3NjUyOSIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjI5Njc4Mzg4LThkYzEtMTY0MC05NTBiLWQwYWU4YmU3ZGY4YSIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMCIgdGlmZjpPcmllbnRhdGlvbj0iMSIgdGlmZjpYUmVzb2x1dGlvbj0iNzAwMDAwLzEwMDAwIiB0aWZmOllSZXNvbHV0aW9uPSI3MDAwMDAvMTAwMDAiIHRpZmY6UmVzb2x1dGlvblVuaXQ9IjIiIGV4aWY6Q29sb3JTcGFjZT0iNjU1MzUiIGV4aWY6UGl4ZWxYRGltZW5zaW9uPSIyNzMiIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSIxNjAiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjI5Njc4Mzg4LThkYzEtMTY0MC05NTBiLWQwYWU4YmU3ZGY4YSIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wOFQxMTowNzo1NyswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpjZGYzNTI3Yi0xYTJmLTU3NDYtYWJiMy1jOWM0Mjk5NGNjYTQiIHN0RXZ0OndoZW49IjIwMjMtMDMtMDhUMTE6MTU6NTIrMDI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMy4wIChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTI4Zjg2ODYtZTBhZS1jYTRkLTgyODgtOGExYjU4NWVmY2I1IiBzdEV2dDp3aGVuPSIyMDIzLTAzLTA4VDE5OjUwOjQxKzAyOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjMuMCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNvbnZlcnRlZCIgc3RFdnQ6cGFyYW1ldGVycz0iZnJvbSBhcHBsaWNhdGlvbi92bmQuYWRvYmUucGhvdG9zaG9wIHRvIGltYWdlL3BuZyIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iZGVyaXZlZCIgc3RFdnQ6cGFyYW1ldGVycz0iY29udmVydGVkIGZyb20gYXBwbGljYXRpb24vdm5kLmFkb2JlLnBob3Rvc2hvcCB0byBpbWFnZS9wbmciLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjM1MDY5MjQyLThlNTUtN2Q0Ni1hMGEyLTU5NGUxNDZkZjQ3NSIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wOFQxOTo1MDo0MSswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MjhmODY4Ni1lMGFlLWNhNGQtODI4OC04YTFiNTg1ZWZjYjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Mjk2NzgzODgtOGRjMS0xNjQwLTk1MGItZDBhZThiZTdkZjhhIiBzdFJlZjpvcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6Mjk2NzgzODgtOGRjMS0xNjQwLTk1MGItZDBhZThiZTdkZjhhIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Szp7aQAAA3NJREFUWMPtmM9LFFEcwKcMN6HWwIvU5oQVREEkgmwXVyHqHNQtyvIgpK5FuCo1TmBgh9A/IMprUAgRTLlbzYbiHqK1U0KL+9JDRjo7boKbjL7Xm12qNzsz7z11Wgp6l2GXD9/fP96MIHAcGf06AFnPeuHxn3FjZjmYdP7RTmXUpmB17Rm6HPV88KtiMJiec5kVNuPPjrOYDrgSY/iV68iwmNm5YOBKs4e5eMjBhFnMYQ5GEG5yML1+DjkZbxhCV9ytl7l0QQ5djRxy0hy5YNrTzsEIwi2/N/nyiuGpwz64obynBY4akzjqR7oXuFwl0u2RIgujz/Vi5qggW5nXdgbn4iLJfDk5XEmPIZYDFZ3JjDKYdHAocL3K5xCf7UScIUefQo4eXJM336c8NTbE6mWcizmOOoSlYzY4o7bWg/1WJuPENFp7J+s0N5Ls3iHnjzR5aOdEhVjEHLPMHykWng6ndHo9Y6aDydS8v5qoZMr5/FGnzh8p1jkddmDI2RKcPHh3osL3x/Il95dqn9Zz1eEG7wmu+/QfvydsTRdzJuwv4T3BOqPeuN0Tan4zr3h2ZfmJltMHTsk0Rh5LKbHIgspm3qo0e+Q7+5SB8kchml/SmKGokXa6X2OpUbs91yz2jJcHL7D8Qshwjs8im+kVSCbrLGfNm73TB/+uviDscZkbR0q6K0s3e/niw5NTsp55dCU4GN2RabMw23zVrT78pk2xJ52JLjyIah8Mil+Y0R2ZrgzJ+Juji3XjxUzbEGFPFI1gOTFbjRE51Qv22Jh+Is6JwUDrYHVtczETJvOFJ4bqEMMWP5vpIuXg65G4qff33Z5+k1kegTe8YPJnPkRlcgkoYyZO+4a2JEKDpSsHYBmdWR7BjIHm6cwSwPboKl2XiIf3Jze/zmImB1AClRFyDNUmZ0lcDcEUtiddX/jz55sRweTACsAM9qvH1Z4cyjNZGYR3NdXF99TWbW+16ZKxRfNIU0FXREtqse6Usm6Toy6bRmJGjLxLas+6Uw02Jr+3jIKcJ8e1xzv2Pp21xweA72VYJeiJdCa1+92rykwxA0BcXDWQOowZLTlj2mNjpsRpAEPodihdj/16YfrVUsx8M7foFFJxnGWn+DRgZt38QWFMOTMAIqxrQDU1ujBxEVzCNr+k1YbpF/Yd0WseABxDNOzKcJwfprM2T80/ZJAAAAAASUVORK5CYII=&quot; expr=&quot;&quot; ratio=&quot;false&quot; iw=&quot;273&quot; ih=&quot;160&quot;/&gt;",
    'orderText0' => "&lt;text enable=&quot;true&quot; x=&quot;34&quot; y=&quot;7&quot; w=&quot;47&quot; text=&quot;'text_order_0'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;false&quot; align=&quot;left&quot;/&gt;",
    'orderText1' => "&lt;text enable=&quot;true&quot; x=&quot;136&quot; y=&quot;7&quot; w=&quot;47&quot; text=&quot;'text_order_1'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;false&quot; align=&quot;left&quot;/&gt;",
    'orderText2' => "&lt;text enable=&quot;true&quot; x=&quot;34&quot; y=&quot;68&quot; w=&quot;47&quot; text=&quot;'text_order_2'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;false&quot; align=&quot;left&quot;/&gt;",
    'rot' => "&lt;rot comment=&quot;&quot; enable=&quot;true&quot; x=&quot;10&quot; y=&quot;40&quot; a=&quot;-90&quot;/&gt;",
    'txt0' => "&lt;text enable=&quot;true&quot; x=&quot;1&quot; y=&quot;40&quot; w=&quot;47&quot; text=&quot;'text_client_0'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;3&quot; y=&quot;46&quot; w=&quot;47&quot; text=&quot;'text_product_0'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;3&quot; y=&quot;53&quot; w=&quot;47&quot; text=&quot;'text_material_0'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;2&quot; y=&quot;60&quot; w=&quot;47&quot; text=&quot;'text_detail_0'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;",
    'txt1' => "&lt;text enable=&quot;true&quot; x=&quot;1&quot; y=&quot;142&quot; w=&quot;47&quot; text=&quot;'text_client_1'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;3&quot; y=&quot;148&quot; w=&quot;47&quot; text=&quot;'text_product_1'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;3&quot; y=&quot;155&quot; w=&quot;47&quot; text=&quot;'text_material_1'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;2&quot; y=&quot;162&quot; w=&quot;47&quot; text=&quot;'text_detail_1'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;",
    'txt2' => "&lt;text enable=&quot;true&quot; x=&quot;-60&quot; y=&quot;40&quot; w=&quot;47&quot; text=&quot;'text_client_2'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;-58&quot; y=&quot;46&quot; w=&quot;47&quot; text=&quot;'text_product_2'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;-58&quot; y=&quot;53&quot; w=&quot;47&quot; text=&quot;'text_material_2'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;
&lt;text enable=&quot;true&quot; x=&quot;-59&quot; y=&quot;60&quot; w=&quot;47&quot; text=&quot;'text_detail_2'&quot; fillColor=&quot;#000000&quot; fontName=&quot;Times New Roman&quot; fontSize=&quot;3&quot; fontBold=&quot;false&quot; fontItalic=&quot;false&quot; ratio=&quot;true&quot; align=&quot;left&quot;/&gt;",
    'opClose' => '&lt;/program&gt;" typeId="LB" costMaterial="0" costOperation="0" costTotal="0" printable="true" startNewPage="true" id="333333332" printMode="0" printParts="true" printWastes="true" partEach="false" partONZ="false" partSelected="false" wasteEach="false" productEach="false" productONZ="false"/>'
];

// require_once ("../../../func.php");
// $project_data = file_get_contents(DIR_CORE . 'dev/Два изделия и одинадцать деталей.project');
// $data = get_vals_index_without_session($project_data);

// $vals = $data['vals'];
// $index = make_index($vals);
// $LB = getControlDetails($stickers, $vals, 'Палпалыч');

/**
 * Функция собирает данные:
 *  1. клиент
 *  2. изделие
 *  3. материал (артикул и толщина)
 *  4. деталь (размеры, номер и ID)
 * и возвращает операцию с наклейками для контрольных деталей,
 * которая затем добавляется к проекту
 * 
 * Если в проекте до пяти деталей, одна наклейка на среднюю деталь,
 * если до пятнадцати - на первую и последнюю,
 * если свыше пятнадцати - на первую, среднюю и последнюю.
 * 
 * В блоках "st*" есть подстроки "cText(1,2,3,4)", которые и заменяются необходимыми данными.
 * Основная таблица, это изображение, а записи "st*" - это текст, по расположению подогнанный в нужные ячейки.
 * 
 * Т.к. этикеток может быть от одной до трёх,
 * я не стал заморачиваться с определением размеров их расположения на листе,
 * а сразу в конструкторе определил их места и по необходимости их можно брать из массива,
 * меняя только данные по детали, без подгонки расположения.
 * 
 * st1 располагается слева вверху
 * st2 - рядом, справа
 * st3 - ниже st1
 **/
function getControlDetails($stickers, $vals, $client = 'Интернет-магазин') {

    $index = make_index($vals);
    $order = $vals[0]['attributes']['project.my_order_id'];
    $output = [];
    $detailsById = [];
    $detailsByNum = [];
    $controlDetails = [];

    // Номер заказа
    if (0 !== (int)$order) {
        $q = sql_data(__LINE__,__FILE__,__FUNCTION__,'SELECT * FROM `ORDER1C` WHERE `DB_AC_ID` = '. $order);
        if ($q['res'] === 1) $order = $q['data'][0]['DB_AC_NUM'];
    }

    // Собираем сразу детали и частично заполняем массивы данными (название изделия, ID, номер, размеры и кол-во)
    $i = 0;
    foreach ($index['GOOD'] as $g_ind) {
        if ($vals[$g_ind]['attributes']['TYPEID'] === 'product') {
            $i = $g_ind + 1;
            while ($vals[$i]['tag'] === 'PART') {
                $detId = $vals[$i]['attributes']['ID'];
                $detailsById[$detId] = [
                    'client' => $client,
                    'detId' => $detId,
                    'detCount' => $vals[$i]['attributes']['COUNT'],
                    'product' => $vals[$g_ind]['attributes']['NAME'],
                    'size' => [
                        'l' => $vals[$i]['attributes']['L'],
                        'w' => $vals[$i]['attributes']['W']
                    ]
                ];
                $i++;
            }
        }
    }

    // Дозаполняем массивы деталей материалами и номерами
    foreach ($index['OPERATION'] as $op_ind) {

        if ($vals[$op_ind]['attributes']['TYPEID'] === 'CS') {

            // Определяем материал
            $matInfo = [
                'code' => null,
                'w' => null,
                'l' => null,
                't' => null
            ];
            for ($j = $op_ind; $j < ($op_ind + 1000); $j++) { 
                if ($vals[$j]['tag'] === 'MATERIAL') {
                    $matId = $vals[$j]['attributes']['ID'];

                    foreach ($index['GOOD'] as $g_ind) {
                        if ($vals[$g_ind]['attributes']['TYPEID'] === 'sheet' && $vals[$g_ind]['attributes']['ID'] === $matId) {
                            $matInfo['code'] = $vals[$g_ind]['attributes']['CODE'];
                            $matInfo['w'] = $vals[$g_ind]['attributes']['W'];
                            $matInfo['l'] = $vals[$g_ind]['attributes']['L'];
                            $matInfo['t'] = $vals[$g_ind]['attributes']['T'];
                            break 2;
                        }
                    }
                }
            }

            $i = $op_ind + 1;
            if ($vals[$i]['tag'] === 'MATERIAL') $i++;

            $j = 1;
            while ($vals[$i]['tag'] === 'PART') {
                $partId = $vals[$i]['attributes']['ID'];
                if (isset($detailsById[$partId])) {
                    $detailsById[$partId]['detNum'] = $j;
                    $detailsById[$partId]['material'] = $matInfo;

                    $detailsByNum[] = $detailsById[$partId];
                }
                $j++;
                $i++;
            }
        }
    }
    

    // Здесь определяем сколько будет контрольных деталей
    if (count($detailsById) === 1 || count($detailsById) === 2) {
        $controlDetails[] = $detailsByNum[0];
    } elseif (count($detailsById) > 2 && count($detailsById) < 6) {

        $cDet = floor(count($detailsByNum) / 2);
        $controlDetails[] = $detailsByNum[$cDet];
    } elseif (count($detailsById) > 5 && count($detailsById) < 16) {

        $controlDetails[] = $detailsByNum[0];
        $controlDetails[] = $detailsByNum[(count($detailsByNum) - 1)];
    } else {
        $cDet = floor(count($detailsByNum) / 2);

        $controlDetails[] = $detailsByNum[0];
        $controlDetails[] = $detailsByNum[$cDet];
        $controlDetails[] = $detailsByNum[(count($detailsByNum) - 1)];
    }

    $output = getLbOperation($controlDetails, $stickers, $order);
    return $output;
}

/**
 * Функция возвращает XML-операцию печати наклеек
 **/
function getLbOperation($controlDetails, $stickers, $order) {

    $output = $stickers['opOpen'];

    $tmp = '';
    foreach ($controlDetails as $key => $value) {
        $output .= $stickers['st' . $key];
    }
    foreach ($controlDetails as $key => $value) {

        $output .= str_replace('text_order_' . $key, 'Заказ: ' . $order, $stickers['orderText' . $key]);
    }

    $output .= $stickers['rot'];
    foreach ($controlDetails as $key => $value) {

        $matInfo = 'арт: ' . $value['material']['code'] . ' (' . (int)$value['size']['l'] . 'x' . (int)$value['size']['w'] . 'x' . (int)$value['material']['t'] . ')';
        $detInfo = '№ ' . $value['detNum'] . ' (id: ' . $value['detId'] . ') - ' . $value['detCount'] . 'шт.';

        $tmp = str_replace('text_client_' . $key, $value['client'], $stickers['txt' . $key]);
        $tmp = str_replace('text_product_' . $key, $value['product'], $tmp);
        $tmp = str_replace('text_material_' . $key, $matInfo, $tmp);
        $tmp = str_replace('text_detail_' . $key, $detInfo, $tmp);

        $output .= $tmp;
    }

    $output .= $stickers['opClose'];
    return $output;
}

?>