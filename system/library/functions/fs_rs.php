<?php

/**
 * 
**/
function rs_general($p)
{
    $sql = "SELECT * FROM `RS_general` WHERE name = ". $p;
    $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0]['value'];
    return $sql3;
}

/**
 * 
**/
function RS_from_project ($project_data)
{
    $project_data = xml_parser($project_data);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project_data, $vals, $index);
    xml_parser_free($p);
    $vals = vals_out($vals);
    $index = make_index($vals);
    foreach ($index["RS_DATA"] as $i) {
        $ar = unserialize(base64_decode($vals[$i]['attributes']['ARRAY']));
        return($ar);
    }
}

/**
 * 
**/
function RS_st_from_akcent($goods_pr, $place)
{

	foreach ($goods_pr as $k => $v) {

		$sql = "SELECT * FROM `SIMPLE` WHERE `SIMPLE_ID` = " . $k;
		$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];    
		$codes[] = $res['CODE'];
		$arr['code'][$res['CODE']] = $k;
		$sql = "SELECT * FROM `GOOD_PROPERTIES_SIMPLE` WHERE `PROPERTY_ID` = 252 AND `VALUEDIG` = " . $res['MY_1C_HAR'];
		$res2 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'];

		if (count($res2) > 0) {
			foreach ($res2 as $r) {
				$arr[$res['CODE']][$res['CODE']] = 0;
				$sql = "SELECT * FROM SIMPLE WHERE SIMPLE_ID = " . $r['SIMPLE_ID'];
				$res3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
				$codes[] = $res3['CODE'];
				$arr[$res['CODE']][$res3['CODE']] = 0;
				$arr['code'][$res3['CODE']] = $res3['SIMPLE_ID'];
			}
		} else $arr[$res['CODE']][$res['CODE']] = 0;
	}
	$arr_st = get_material_count($codes, $place);

	foreach ($arr as $k=>$v) {
		if ($k <> 'code') {
			foreach ($v as $k1 => $v1) {
				$arr[$k][$k1] = $arr_st[$k1];
			}
		}
	}

	return $arr;
}

/**
 * 
**/
function RS_calc($ar)
{
    // Проверка введённых значений - потом закоментить
    // check_db ();
    // закоментить выше строку
    if (!isset($_SESSION['user_place'])) $_SESSION['user_place'] = $arr['user_place'];
    $d = explode ('_',$ar['rs_count_doorsOverlap']);
    $sql='SELECT * FROM RS_profile WHERE id='.$ar['rs_profile'].';';
    $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
    $sql='SELECT * FROM RS_schetka WHERE `profile`='.$ar['rs_profile'].' AND decor='.$ar['profileDecors'].';';
    $res2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['glue'];
    $x_door=(($ar['rs_size_x']+$d[1]*$res['width_cross'])/$d[0])-$res2*5*$d[1];
    
    if ($dd==7)
    {
        $x_door_ar_in=[500,600,700];
        $d=[3,2];
        $ar['rs_size_x']=1800;
    }
    else
    {
        for ($i=0;$i<$d[0];$i++)
        {
            $x_door_ar_in[]=$ar['rs_size_x']/$d[0];
        }
    }
    
    

    $first_door=array_shift($x_door_ar_in);
    $end_door=array_pop($x_door_ar_in);
    $x_door_ar[]=$first_door+$res['width_cross']/2;
    foreach ($x_door_ar_in as $a) $x_door_ar[]=$a+$res['width_cross'];
    $x_door_ar[]=$end_door+$res['width_cross']/2;
    $dd=count($ar['doors'])-count($x_door_ar);
    foreach ($ar['doors'] as $k=>$v)
    {
        if (count($v)>0)
        {
            $ar['doors_width'][$k]['width_door']=round($x_door_ar[$k-$dd],2);
        }
    }


    $y_door=$ar['rs_size_y']-$res['koef_h_door'];
    $sql='SELECT * FROM RS_decor_profile WHERE profile_article='.$ar['rs_profile'].' AND decor='.$ar['profileDecors'].';';
    $res3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach($res3 as $v)
    {
        if (($v['type_profile_article']=='napr_up')||($v['type_profile_article']=='napr_down'))
        {
            $goods[$v['simple']]+=$ar['rs_size_x']/1000;
            $goods_pr[$v['simple']][]=[1=>$ar['rs_size_x']/1000];
            $goods_pr_type[$v['simple']]=$v['type_profile_article'];
        }
        elseif (($v['type_profile_article']=='hor_up')||($v['type_profile_article']=='hor_down'))
        {
            foreach ($x_door_ar as $x_door) 
            {
                $goods[$v['simple']]+=($x_door-$res['koef_l_hor_pofile'])*$d[0]/1000;
                $goods_pr[$v['simple']][]=[$d[0]=>($x_door-$res['koef_l_hor_pofile'])/1000];
            }
            $goods_pr_type[$v['simple']]=$v['type_profile_article'];

        }
        elseif ($v['type_profile_article']=='vert')
        {
            $goods[$v['simple']]+=$y_door*$d[0]*2/1000;
            $goods_pr[$v['simple']][]=[$d[0]*2=>$y_door/1000];
            $goods_pr_type[$v['simple']]=$v['type_profile_article'];
            $goods[$ar['profileSchetka']]+=$y_door*$d[0]*2/1000;
            $goods_pr[$ar['profileSchetka']][]=[$d[0]*2=>$y_door/1000];
            $goods_pr_type[$ar['profileSchetka']]='schetka';

        }
        elseif ($v['type_profile_article']=='connection')
        {
            unset($count,$door_count);
            foreach ($ar['doors'] as $k=>$door)
            {
              
                if (count ($door)>0)
                {
                    $count=count ($door)-2;
                    $goods[$v['simple']]+=$count*($ar['doors_width'][$k]['width_door']-$res['koef_l_hor_pofile'])/1000;
                    $goods_pr[$v['simple']][]=[$count=>($ar['doors_width'][$k]['width_door']-$res['koef_l_hor_pofile'])/1000];
                }
                    
            }
            $goods_pr_type[$v['simple']]=$v['type_profile_article'];

        }
    }

    $ar['goods_profiles']=$goods_pr;
    $ar['goods_profiles_type']=$goods_pr_type;
    $goods[35276]+=$door_count*4;
    if ($ar['profileDovodchik']>0) $goods[$ar['profileDovodchik']]=$door_count*$ar['countDovodchik'];
    if ($ar['profileRolick']>0) $goods[$ar['profileRolick']]=$door_count;
    if ($ar['profileStopor']>0) $goods[$ar['profileStopor']]=$door_count;

   
    $sql='SELECT * FROM RS_general;';
    $rs_general=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($rs_general as $v)
    {
        $gen[$v['name']]=$v['value'];
    }
    $rs_general=$gen;
    $date = new DateTime($in[0]['attributes']['DATE']);
    $vals[0]=array('tag'=>'PROJECT','type'=>'open','level'=>1,'attributes'=>array(
        'CURRENCY'=>'грн',
        'VERSION'=>'',
        'DATE'=>$date->getTimestamp(),
        'NAME'=>'RS',
        'DESCRIPTION'=>'',
        'project.my_label'=>'new',
        'project.my_rs'=>1,
        'project.my_rs_key'=>"CfvjdfhjgfhfdjpDfhdfhbx"

    ));

    $vals[1]=array('tag'=>'PROJECT','type'=>'close','level'=>1);

    if(!isset($place)) $place=1;

    foreach($ar['datalm'] as $code=>$v)
    {
        $sql='SELECT * FROM MATERIAL WHERE CODE='.$code.';';
        $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        $vals=put_source_in($vals,$link,array('sheet_in'=>array($mat['MATERIAL_ID'])),$place);

    }
    ksort($vals);

    $res1=put_source_in_cl($vals,$goods_pr,$goods_pr_type,$goods);
    $vals=$res1['vals'];
    $goods=$res1['goods'];
    $ar['koef_price']=$res1['koef_price'];
    

    foreach ($ar['doors_data'] as $k=>$door)
    {
        if (count($door)>0)
        {
            
            foreach($door as $k1=>$v1)
            {
                $sql='SELECT * FROM MATERIAL WHERE CODE='.$v1['filler'].';';
                $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
               
                if (!isset($mat['RS_MIRROR']))
                {
                    $door[$k1]['mirror']=0;
                    $ar['doors_data'][$k][$k1]['mirror']=0;
                }
                else
                {
                    $door[$k1]['mirror']=$mat['RS_MIRROR'];
                    $ar['doors_data'][$k][$k1]['mirror']=$mat['RS_MIRROR'];
                }
                $door[$k1]['mat_id']=$mat['MATERIAL_ID'];
                if (substr_count($mat['NAME'],'LACOBEL')>0)
                {
                    $door[$k1]['lacobel']=1;
                    $ar['doors_data'][$k][$k1]['lacobel']=1;

                }
                $ar['doors_data'][$k][$k1]['mat_id']=$mat['MATERIAL_ID'];
                $door[$k1]['t']=$mat['T'];
                $ar['doors_data'][$k][$k1]['t']=$mat['T'];
                unset ($start,$no_connection);
                if ($mat['RS_PLATE']<>1)
                {
                    $door[$k1]['mat']='glass';
                    $ar['doors_data'][$k][$k1]['mat']='glass';
                    $door[$k1]['section_x']=$ar['doors_width'][$k]['width_door']-$res['wdth_l_glass'];
                    $ar['doors_data'][$k][$k1]['section_x']=$ar['doors_width'][$k]['width_door']-$res['wdth_l_glass'];
                }
                elseif ($mat['RS_PLATE']==1)
                {
                    $door[$k1]['mat']='sheet';
                    $ar['doors_data'][$k][$k1]['mat']='sheet';
                    $door[$k1]['section_x']=$ar['doors_width'][$k]['width_door']-$res['width_l_sheet'];
                    $ar['doors_data'][$k][$k1]['section_x']=$ar['doors_width'][$k]['width_door']-$res['width_l_sheet'];
                }
                
                
                
            }
            foreach ($door as $i => $v1)
            {
                
                $section_y = $v1['height'] - ($ar['rs_size_y'] - $y_door) / count($door);
                    if (($door[$i-1]['mat']=='sheet')&&($door[$i]['mat']=='glass')) $section_y-=$rs_general['con_sheet_glass'];
                    elseif (($door[$i-1]['mat']=='glass')&&($door[$i]['mat']=='sheet')) $section_y-=$res['con_sheet_gl'];
                    elseif (($door[$i-1]['mat']=='sheet')&&($door[$i]['mat']=='sheet')) $section_y-=$res['con_sheet_sheet'];
                    elseif (($door[$i-1]['mat']=='glass')&&($door[$i]['mat']=='glass')) $section_y-=$res['con_mir_gl'];

                if ($door[$i]['mat']=='glass') $section_y-=$res['height_w_glass']/count($door);
                elseif ($door[$i]['mat']=='sheet') $section_y-=$res['height_w_sheet']/count($door);

                $door[$i]['section_y']=$section_y;
                $ar['doors_data'][$k][$i]['section_y']=$section_y;

            }

        
        }
    }
    
    unset ($doors);
    $i1=0;
    foreach ($ar['doors_data'] as $k=>$door)
    {
        if ($k>0)
        {
            foreach ($door as $i=>$section)
            {
                $i1++;
                $doors[$section['mat_id']][]=array(
                        'rs_id'=>$i1,
                        'op'=>$ar['doors'][$k][$i]['processing']['op'],
                        'selected'=>$ar['doors'][$k][$i]['processing']['selected'],
                        'image'=>$ar['doors'][$k][$i]['processing']['image'],
                        'lacobel'=>$section['lacobel'],
                        'txt'=>$section['txt_section'],
                        'oracal'=>$ar['doors'][$k][$i]['processing']['oracal'],
                    'mirror'=>$section['mirror'],'t'=>$section['t'],'type'=>$section['mat'],'door'=>$k,'l'=>$section['section_x'],'w'=>$section['section_y'],'mat_id'=>$section['mat_id'],'count'=>1);
                $ar['doors_data'][$k][$i]['op']=$ar['doors'][$k][$i]['processing']['op'];
                $ar['doors_data'][$k][$i]['selected']=$ar['doors'][$k][$i]['processing']['selected'];
                $ar['doors_data'][$k][$i]['image']=$ar['doors'][$k][$i]['processing']['image'];
                $ar['doors'][$k][$i]['processing']['txt_section']=$section['txt_section'];
                $ar['doors_data'][$k][$i]['lakobel']=$ar['doors'][$k][$i]['processing']['lakobel'];
                $ar['doors_data'][$k][$i]['oracal']=$ar['doors'][$k][$i]['processing']['oracal'];
                $ar['doors_data'][$k][$i]['width']=$ar['doors_width'][$k]['width_door'];
                $ar['doors_data'][$k][$i]['rs_id']=$i;
                $ar['doors_data'][$k][$i]['mat_name']=$ar['datalm'][$door[$i]['filler']];
                if ($ar['doors'][$k][$i]['processing']['op']=='') $ar['doors_data'][$k][$i]['color']='#ff9966';
                elseif ($ar['doors'][$k][$i]['processing']['op']=='M') $ar['doors_data'][$k][$i]['color']='#99cc00';
                elseif ($ar['doors'][$k][$i]['processing']['op']=='G') $ar['doors_data'][$k][$i]['color']='#3399ff';
                $ar['doors_data'][$k][$i]['rs_id']=$i1;
            }

        }
    }
    foreach ($doors as $mat_id=>$v)
    {
        foreach ($v as $p_id=>$d)
        {
            if ($d['op']=='M')
            {
                //Наклейка защитной плёнки на зеркало
                $goods_serv[58850]+=($d['l']*$d['w'])/1000000;
                 
            }
            if ((($d['op']=='M')&&($d['selected']==1))||(($d['op']=='G')&&($d['selected']==8)))
            {
                
                //Художественный пескоструй
                $goods_serv[20444]+=($d['l']*$d['w'])/1000000;
                 
            }
            elseif (($d['op']=='M')&&($d['selected']==2))
            {
                // Художественное снятие амальгамы
                $goods_serv[20447]+=($d['l']*$d['w'])/1000000;
                 
            }  
            elseif (($d['op']=='G')&&($d['selected']==1))
            {
                // Фотопечать с ламинацией на стекло (пленка)
                $goods_serv[21010]+=($d['l']*$d['w'])/1000000;
                 
            } 
            elseif (($d['op']=='G')&&(($d['selected']==7)||(d['selected']==8)))
            {
                // Поклейка пленки ORACAL (цветной,прозрачной)
                $goods_serv[20449]+=($d['l']*$d['w'])/1000000;
                // Пленка цветная / прозрачная ORACAL / RITRAMA 
                $goods[35738]+=($d['l']*$d['w'])/1000000*rs_general('oracal_film_up');
                $ar['koef_price'][35738]=rs_general('oracal_film_up');
                 
            } 
            elseif (($d['op']=='G')&&($d['selected']==2))
            {
                // УФ-фотопечать с ламинацией белой пленкой
                $goods_serv[61583]+=($d['l']*$d['w'])/1000000;
                 
            }  
            
            elseif (($d['op']=='G')&&($d['selected']==4))
            {
                // Поклейка пленки ORACAL (цветной,прозрачной)
                $goods_serv[20449]+=($d['l']*$d['w'])/1000000;
                // Выбор плёнки
                $sql='SELECT * FROM SIMPLE WHERE CODE='.$d['oracal'].';';
                $res3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['SIMPLE_ID'];
                $goods[$res3]+=($d['l']*$d['w'])/1000000*rs_general('oracal_film_up');
                $ar['koef_price'][$res3]=rs_general('oracal_film_up');
                 
            } 
            elseif (($d['op']=='G')&&($d['selected']==5))
            {
                // УФ-фотопечать с заливкой белого фона
                $goods_serv[61584]+=($d['l']*$d['w'])/1000000;
                 
            }   
            elseif ($d['selected']==6)
            {
                // Поклейка пленки Frost
                $goods_serv[27798]+=($d['l']*$d['w'])/1000000;
                // Пленка витражная "ФРОСТ" 
                $goods[35733]+=($d['l']*$d['w'])/1000000*rs_general('film_frost_up');
                $ar['koef_price'][35733]=rs_general('film_frost_up');
                 
            }   
            if (($d['type']=='sheet')&&($d['t']==8))
            {
                // Уплотнитель для раздвижных систем, для ДСП 8 мм 
                $goods[35715]+=($d['l']+$d['w'])*2/1000*rs_general('uplotnitel_up');
                $ar['koef_price'][35715]=rs_general('uplotnitel_up');

                 
            }
            elseif (($d['type']=='glass'))
            {
                // Уплотнитель для раздвижных систем, для стекла и зеркала (ребристый) 200 м 
                $goods[35722]+=($d['l']+$d['w'])*2/1000*rs_general('uplotnitel_gl_up');
                $ar['koef_price'][35722]=rs_general('uplotnitel_gl_up');

                 
            }
            if (($d['mirror']>0)&&($d['type']=='glass')&&($d['selected']<>7)&&($d['selected']<>2))
            {
                // Защитная пленка Lux 
                $goods[35727]+=($d['l']*$d['w'])/1000000*rs_general('lux_film_up');
                $ar['koef_price'][35727]=rs_general('lux_film_up');

            }
            elseif (($d['mirror']<1)&&($d['type']=='glass'))
            {
                if(($d['selected']==5)||($d['selected']==2))
                {
                    // Пленка цветная / прозрачная ORACAL / RITRAMA 
                    $goods[35738]+=($d['l']*$d['w'])/1000000*rs_general('lux_film_transparent_up');
                    $ar['koef_price'][35731]=rs_general('lux_film_transparent_up');
                    // Поклейка пленки ORACAL (цветной,прозрачной)
                    $goods_serv[20449]+=($d['l']*$d['w'])/1000000;
                }
                elseif ($d['selected']<>1)
                {
                    // Пленка цветная / прозрачная ORACAL / RITRAMA 
                    $goods[35738]+=($d['l']*$d['w'])/1000000*rs_general('lacobel_film_transparent_up');
                    $ar['koef_price'][35738]=rs_general('lacobel_film_transparent_up');
                }
            }
            $vals=RS_put_part_without_edge($vals,$mat_id,$d,$res);
            
        }
    }
    foreach ($ar['koef_price'] as $k=>$v)
    {
        $sql='SELECT * FROM SIMPLE WHERE CODE='.$k.';';
        if (is_numeric($k)) $res3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['CODE'];
        $ar['koef_price']['code'][$res3]=$v;
        $_SESSION['koef_price'][$res3]=$v;
    }
    ksort($goods);
    foreach($goods as $k=>$g)
    {
        $b=$g;
        unset ($nn);
        $sql='SELECT * FROM SIMPLE WHERE SIMPLE_ID='.$k.';';
        if (is_numeric($k)) $res3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        else $nn[$k]=$g;
        $goods[$k]=[
            'id'=>$res3['SIMPLE_ID'],
            'code'=>$res3['CODE'],
            'name'=>$res3['NAME'],
            'unit'=>$res3['UNIT'],
            'count'=>round($b,2)
        ];
        $vals=move_vals($vals,1,1);
        $vals[1]=[
            'tag'=>'GOOD','type'=>'complete','level'=>2,'attributes'=>array(
            'NAME'=>$goods[$k]['name'],
            'MY_RS'=>1,
            'MY_RS_GOODS'=>1,
            'COUNT'=>$goods[$k]['count'],
            'ID'=>new_id($vals)+1,
            'TYPEID'=>'simple',
            'CODE'=>$goods[$k]['code'],
            'RS_CODE'=>$goods[$k]['code'],
            'SIMPLE_ID'=>$goods[$k]['id'],
            'UNIT'=>$goods[$k]['unit'],

            )
            ];
        if (isset($nn))
        {
            $vals[1]['attributes']['CODE']=1111;
            $vals[1]['attributes']['NAME']=key($nn);
        }
        $goods[$k]['v_id']=$vals[1]['attributes']['ID'];
    }
    
    $vals=move_vals($vals,count($goods)+1,1);
    $vals[count($goods)+1]=[
        'tag'=>'OPERATION','type'=>'open','level'=>2,'attributes'=>array(
        'NAME'=>'RS_goods',
        'MY_RS'=>1,
        'MY_RS_GOODS'=>1,
        'PRINTABLE'=>'true',
        'STARTNEWPAGE'=>'false',
        'ID'=>new_id($vals)+1,
        'TYPEID'=>'O',
        'TYPENAME'=>'Материалы раздвижной системы',

        )
        ];
    $vals=move_vals($vals,count($goods)+2,1);
    $vals[count($goods)+2]=['tag'=>'OPERATION','type'=>'close','level'=>2];
    ksort($vals);
    $i=0;
    foreach($goods as $k=>$g)
    {
        $vals=move_vals($vals,count($goods)+2+$i,1);
        $vals[count($goods)+2+$i]=[
        'tag'=>'MATERIAL','type'=>'complete','level'=>3,'attributes'=>array(
        'ID'=>$g['v_id'],
        'COUNT'=>$g['count'],
        )
        ];
        $i++;
    }
    ksort($vals);
    $goods_serv[19889]=$door_count;
    $goods_serv[16404]=$door_count;

    foreach ($ar['doors_data'] as $k=>$door)
    {
        if (count($door)>1)
        {
            $goods_serv[121011]++;
        }
        elseif (count($door)==1)
        {
            if ($door[0]['mat']=='glass') $goods_serv[23362]++;
            elseif ($door[0]['mat']=='sheet') $goods_serv[19827]++;
        }
    }
    foreach($goods_serv as $k=>$g)
    {
        $b=$g;
        unset ($nn);
        $sql='SELECT * FROM SERVICE WHERE CODE='.$k.';';
        if (is_numeric($k)) $res3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        else $nn[$k]=$g;
        $goods_serv[$k]=[
            'id'=>$res3['SERVICE_ID'],
            'code'=>$res3['CODE'],
            'name'=>$res3['NAME'],
            'unit'=>$res3['UNIT'],
            'count'=>round($b,2)
        ];
        $vals=move_vals($vals,1,1);
        $vals[1]=[
            'tag'=>'GOOD','type'=>'complete','level'=>2,'attributes'=>array(
            'NAME'=>$goods_serv[$k]['name'],
            'MY_RS'=>1,
            'MY_RS_SERV'=>1,
            'COUNT'=>$goods_serv[$k]['count'],
            'ID'=>new_id($vals)+1,
            'TYPEID'=>'simple',
            'CODE'=>$goods_serv[$k]['code'],
            'RS_CODE'=>$goods_serv[$k]['code'],
            'SERVICE_ID'=>$goods_serv[$k]['id'],
            'UNIT'=>$goods_serv[$k]['unit'],

            )
            ];
        if (isset($nn))
        {
            $vals[1]['attributes']['CODE']=1111;
            $vals[1]['attributes']['NAME']=key($nn);
        }
        $goods_serv[$k]['v_id']=$vals[1]['attributes']['ID'];
    }
    $vals=move_vals($vals,count($goods_serv)+1,1);
    $vals[count($goods_serv)+1]=[
        'tag'=>'OPERATION','type'=>'open','level'=>2,'attributes'=>array(
        'NAME'=>'RS_serv',
        'MY_RS'=>1,
        'MY_RS_SERV'=>1,
        'PRINTABLE'=>'true',
        'STARTNEWPAGE'=>'false',
        'ID'=>new_id($vals)+1,
        'TYPEID'=>'O',
        'TYPENAME'=>'Услуги раздвижной системы',

        )
        ];
    $vals=move_vals($vals,count($goods_serv)+2,1);
    $vals[count($goods_serv)+2]=['tag'=>'OPERATION','type'=>'close','level'=>2];
    ksort($vals);
    $i=0;
    foreach($goods_serv as $k=>$g)
    {
        $vals=move_vals($vals,count($goods_serv)+2+$i,1);
        $vals[count($goods_serv)+2+$i]=[
        'tag'=>'MATERIAL','type'=>'complete','level'=>3,'attributes'=>array(
        'ID'=>$g['v_id'],
        'COUNT'=>$g['count'],
        )
        ];
        $i++;
    }
    ksort($vals);

    $vals=vals_out($vals);
    $ar['goods']=$goods;
    $ar['goods_serv']=$goods_serv;
    if (!isset($place)) $place=1;
    if (!isset($_SESSION['user_place'])) $_SESSION['user_place']=1;
    $project_data=vals_index_to_project($vals);
    // echo htmlspecialchars(vals_index_to_project($vals));exit;
    $project_data=gl_55(__LINE__,__FILE__,__FUNCTION__,$project_data);
    // exit ('11');
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project_data, $vals, $index);
    xml_parser_free($p);
    $index=make_index($vals);
    $vals=add_tag_print($vals);
    
    $res=calculate ($vals,$vals,$link,$place);
    
    foreach ($res['part'] as $part)
    {
        foreach ($ar['doors_data'] as $k=>$v)
        {
            if ($k>0)
            {
                foreach ($v as $k2=>$v2)
                {
                    if ($v2['rs_id']==$part['rs_section'])
                    {
                        $ar['doors_data'][$k][$k2]['calc']=$part;
                    }
                }
            }
        }
    }
    $vals=$res['vals'];

    $ar2=$ar;
    $array=base64_encode(serialize($ar));
    
    $v=[
        'tag'=>'RS_DATA','type'=>'complete','level'=>2,'attributes'=>array(
        'ARRAY'=>$array
        )
        ];
    $vals=vals_into_end($vals,$v);
    ksort($vals);
    $ar2['vals']=$vals;

    include ('../check_production_possibilities.php');
    if ((isset($error))||(isset($_SESSION['error'])))
    {
        $res['error']=$error;
        $res['error'].=$_SESSION['error'];
    }
    $res['array_form']=$ar2;
    $res['vals']=$vals;

    return $res;
}

/**
 * 
**/
function RS_put_part_without_edge($vals,$mat_id,$ar,$profile)
{
    $txt=$ar['txt'];
    if ($txt=='vert')
    {
        $w=$ar['l'];
        $l=$ar['w'];
    }
    else
    {
        $l=$ar['l'];
        $w=$ar['w'];
    }
    $count=$ar['count'];
    $product=$ar['door'];
    $name=$ar['rs_id'];
    $t=$ar['t'];
    $type=$ar['type'];
    $vals=vals_out($vals);
    $index=make_index($vals);

    foreach ($index["GOOD"] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=="product")&&($vals[$i]['attributes']['MY_RS']==$product))
        {
            $ii=$i+1;

        }
        elseif (($vals[$i]['attributes']['TYPEID']=="sheet")&&($vals[$i]['attributes']['CODE']>0))
        {
            $sheet[$vals[$i]['attributes']['MATERIAL_ID']]=$vals[$i]['attributes']['ID'];

        }
    }
    if (!isset($ii))
    {
        
        $vals=move_vals($vals,1,2);
        $vals[1]=array('tag'=>'GOOD','type'=>'open','level'=>1,'attributes'=>array(
            'NAME'=>'RS_door_'.$product,
            'MY_RS'=>$product,
            'TYPEID'=>'product',
            'COUNT'=>1,
            'ID'=>new_id($vals)+1,
            'product.my_rs'=>1,
    
        ));
        $vals[2]=array('tag'=>'GOOD','type'=>'close','level'=>1);
        $ii=2;
    }
    $vals=move_vals($vals,$ii,1);
    $vals[$ii]=[
            'tag'=>'PART','type'=>'complete','level'=>2,'attributes'=>array(
            'NAME'=>'RS_door_part_'.$name,
            'part.my_rs_section'=>$name,
            'part.my_rs'=>1,
            'COUNT'=>$count,
            'ID'=>new_id($vals)+1,
            'CL'=>$l,
            'CW'=>$w,
            'DL'=>$l,
            'TXT'=>'false',
            'DW'=>$w,
            'type'=>$type,
            't'=>$t
            )
    ];
    if (($txt<>'no')&&(isset($txt))) $vals[$ii]['attributes']['TXT']='true';

    $id=$vals[$ii]['attributes']['ID'];
    ksort($vals);
    $index=make_index($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=='CS')&&($vals[$i+1]['attributes']['ID']==$sheet[$mat_id]))
        {
            $vals=move_vals($vals,$i+2,1);
            $vals[$i+2]=[
            'tag'=>'PART','type'=>'complete','level'=>3,'attributes'=>array(
            'ID'=>$id)
            ];
        }
    }
    if (($type=='sheet')&&($t>10))
    {
        
        $xnc_in=Array
        (
            "tag" => "OPERATION",
            "type" => "open",
            "level" => 2,
            "attributes"=> Array ("ID"=>new_id($vals)+mt_rand(450,34500))
        );
        $xnc_in['attributes'][mb_strtoupper('bySizeDetail')]='true';
        $xnc_in['attributes'][mb_strtoupper('mirHor')]='false';
        $xnc_in['attributes'][mb_strtoupper('mirVert')]='true';
        $xnc_in['attributes'][mb_strtoupper('typeId')]='XNC';
        $xnc_in['attributes'][mb_strtoupper('side')]='true';
        $xnc_in['attributes'][mb_strtoupper('typename')]='Деталь id='.$id.' (лицо)';
        $xnc_in['attributes'][mb_strtoupper('printable')]='true';
        $xnc_in['attributes'][mb_strtoupper('startNewPage')]='true';
        $xnc_in['attributes'][mb_strtoupper('code')]=$id.'_'.$xnc_in['attributes']['ID'];
        $xnc_in['attributes'][mb_strtoupper('turn')]=0;
        if (!isset($profile['profile_fr_l']))$profile['profile_fr_l']=10;
        if (!isset($profile['profile_fr_r']))$profile['profile_fr_r']=10;
        if (!isset($profile['profile_fr_t']))$profile['profile_fr_t']=10;
        if (!isset($profile['profile_fr_b']))$profile['profile_fr_b']=10;
        $xnc_pr=array(
            array(
                'tag'=>'PROGRAM','type'=>'open','level'=>1,'attributes'=>array(
                    'DX'=>$l,
                    'DY'=>$w,
                    'DZ'=>$t
                )
            )
        );
        $xnc_pr[]=array(
            'tag'=>'TOOL','type'=>'complete','level'=>2,'attributes'=>array(
                'comment'=>"Фреза диаметром 20мм","name"=>"Mill20","d"=>"20"
            )
        );
        $xnc_pr[]=array(
            'tag'=>'MS','type'=>'complete','level'=>2,'attributes'=>array(
                'X'=>$profile['profile_fr_l']-20,
                'Y'=>$profile['profile_fr_b'],
                'IN'=>202,
                'OUT'=>202,
                'C'=>2,
                'NAME'=>'Mill20',
                'SXY'=>'tool.dia/2',
                "DP"=>$t-10,
            )
        );
        $xnc_pr[]=array(
            'tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array(
                'X'=>$l-$profile['profile_fr_r'],
                'Y'=>$profile['profile_fr_b'],
                "DP"=>$t-10,
            )
        );
        $xnc_pr[]=array(
            'tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array(
                'X'=>$l-$profile['profile_fr_r'],
                'Y'=>$w-$profile['profile_fr_t'],
                "DP"=>$t-10,
            )
        );
        $xnc_pr[]=array(
            'tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array(
                'X'=>$profile['profile_fr_l'],
                'Y'=>$w-$profile['profile_fr_t'],
                "DP"=>$t-10,
            )
        );
        $xnc_pr[]=array(
            'tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array(
                'X'=>$profile['profile_fr_l'],
                'Y'=>$profile['profile_fr_b']-20,
                "DP"=>$t-10,
            )
        );
        $xnc_pr[]=array(
            'tag'=>'PROGRAM','type'=>'close','level'=>1
        );
        $xnc_in['attributes'][mb_strtoupper('program')]=vals_index_to_project($xnc_pr);
        $xnc_in2=Array
        (
            "tag" => "OPERATION",
            "type" => "close",
            "level" => 2,
           
        );
        
        $vals=move_vals($vals,count($vals)-1,1);
        $vals[count($vals)-1]=$xnc_in;
        $vals=move_vals($vals,count($vals)-1,1);
        $vals[count($vals)-1]=[
        'tag'=>'PART','type'=>'complete','level'=>3,'attributes'=>array(
        'ID'=>$id)
        ];
        $vals=move_vals($vals,count($vals)-1,1);
        $vals[count($vals)-1]=$xnc_in2;

    }   

    ksort($vals);
    return $vals;
}

/**
 * 
**/


?>