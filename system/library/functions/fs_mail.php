<?php

require_once DIR_CORE . 'PHPMailer/Exception.php';
require_once DIR_CORE . 'PHPMailer/PHPMailer.php';
require_once DIR_CORE . 'PHPMailer/SMTP.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

/**
 * 
**/
function my_mail($subject, $error, $file, $ext)
{
    $mail_params = $GLOBALS['mail_params'];
    $mail = new PHPMailer;
    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->isSMTP();
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = $mail_params['Host'];
    $mail->Port = $mail_params['Port']; // typically 587
    $mail->SMTPSecure = $mail_params['SMTPSecure']; // ssl is depracated
    $mail->SMTPAuth = $mail_params['SMTPAuth'];
    $mail->Username = $mail_params['Username'];
    $mail->Password = $mail_params['Password'];

    $mail->setFrom($mail_params['From'], 'Service-Kronas: Reports');
    $mail->addAddress('gibservice.support@kronas.com.ua', 'GibService Support');

    $mail->Subject = $subject;
	// $mail->SMTPDebug  = 2;

    $text_erorr_email = 'Сообщение: ' . $error;
    $mail->msgHTML($text_erorr_email);
    
    if (!is_array($file)) {
        $file_name = mt_rand(0, 1000000) . '.' . $ext;
        file_put_contents($file_name, $file);
        $mail->addAttachment($file_name); //Attachment, can be skipped
    } else {
        unset($r);

        foreach ($file as $n => $f) {
            $file_name = $n . '_' . mt_rand(0, 1000000) . '.' . $ext;
            $r[] = $file_name;
            file_put_contents($file_name, $f);
            $mail->addAttachment($file_name); //Attachment, can be skipped
        }

    }
    $mail->send();
    if (!isset($r)) unlink($file_name);
    else {
        foreach ($r as $file_name) {
            unlink($file_name);
        }
    }

}

/**
 * 
**/
function send_post_data($project_data, $action)
{
    $rt = build_req_for_project($project_data, $action);

    if (($rt == 'false') OR (!isset($rt)) OR ($rt == '')) return json_encode(array('project' => $project_data));
    else {
        $client = new Client();
        $r = $client->request('POST', $GLOBALS['send_xml_link'] . '/send_xml', [
            'form_params' => [
                'action' => $action,
                'reqData' => $rt,
            ],
            'timeout' => 260,
            ///'debug' => true,
            'allow_redirects' => true
        ]);
        return $r->getBody()->getContents();
    }
}

/**
 * 
**/
function send_post_data_curl($project_data, $action)
{
    $rt = build_req_for_project($project_data, $action);
    
    if (($rt == false) OR (!isset($rt)) OR ($rt == '')) return json_encode(array('project' => $project_data));
    else {
        $client = new Client();
        $r = $client->request('POST', $GLOBALS['send_xml_link'] . '/send_xml', [
            'form_params' => [
                'action' => $action,
                'reqData' => $rt,
            ],
            'timeout' => 260,
            'allow_redirects' => true
        ]);
        return $r->getBody()->getContents();
    }
}

?>