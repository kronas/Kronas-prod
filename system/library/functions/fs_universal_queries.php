<?php

/**
 * Подключение к БД
**/
function dbConnect() {
    global $database_link_connect;

    $host = $database_link_connect['host'];
    $db   = $database_link_connect['database'];
    $user = $database_link_connect['user'];
    $pass = $database_link_connect['password'];

    $mysql_link = mysqli_connect($host, $user, $pass, $db) or die("Ошибка подключения: " . mysqli_error($mysql_link));

    // Установка кодировки соединения
    mysqli_set_charset($mysql_link, "utf8");

    return $mysql_link;
}

/**
 * Функция обновляет поле в таблице БД
**/
function updateField($table, $targetField, $targetValue, $field, $value){
    $res = [];
    $connect = dbConnect();
    $sql = 'UPDATE `' . $table . '` SET `' . $targetField . '` = "' . $targetValue . '" WHERE `' . $field . '` = "' . $value . '"';
    if ($result = mysqli_query($connect, $sql)) {
        $res = true;
        var_dump($result);
    }
    else $res = false;
    mysqli_close($connect);
    return $res;
}

/**
 * Функция выполняет запрос без возврата данных
 */
function executeQuery($sql) {
    $connect = db_connect();

    if ($result = mysqli_query($connect, $sql)) $status = true;
    else $status = 'Запрос ' . $sql . ' не выполнен!<hr>' . mysqli_error($connect);
    mysqli_close($connect);

    return $status;
}

/**
 * Выполнение SQL запросов с получением данных
 * Если функция не получает $key, то выдаёт нумерованный массив
 * Иначе функция выдаёт именованный массив по $key
**/
function getTableData($sql, $key = null){
    $res = [];
    $connect = dbConnect();
    if ($result = mysqli_query($connect, $sql)) {
        if (!$key) {
            while($row = mysqli_fetch_assoc($result)){
                $res[] = $row;
            }
        } else {
            while($row = mysqli_fetch_assoc($result)){
                $res[$row[$key]] = $row;
            }
        }
    }
    mysqli_close($connect);
    return $res;
}

/**
 * Выполнение SQL запросов с получением, записью (INSERT) и удалением (DELETE) данных.
 * Функция похожа на getTableData, но с расширенным функционалом.
 * getSqlData(__LINE__, __FILE__, __FUNCTION__, $sql)
**/
function getSqlData($line, $file, $function, $sql) {
    if (!isset($function) || empty($function)) $function = 'Запрос не из функции';

    $link = dbConnect();

    if ($result = mysqli_query($link, $sql)) {
        if (substr_count(mb_strtolower($sql),'select') > 0) {
            $rows = mysqli_num_rows($result);
            if ($rows > 0) $result_array['res'] = 1;
            else $result_array['res'] = 0;
            $result_array['count'] = $rows;
            $result_array['data'] = [];
            for($i = 0; $i < $rows; ++$i) $result_array['data'][] = mysqli_fetch_assoc($result);
        } else {
            // Условие для инсёрта (INSERT)
            $result_array['res'] = 1;
            $result_array['id'] = mysqli_insert_id($link);
        }

        if(is_object($result)) mysqli_free_result($result);
    } else {
        $result_array['data'] = 'Запрос не выполнен:' . "<br><b>" . $sql . "</b><br>Файл: " . $file . "<br>Функция: " . $function . "<br>Строка: " . $line . "<br><b>" . mysqli_error($link) . mysqli_sqlstate($link) . '</b><hr>';
        $result_array['res'] = 0;
        // $result_array['error'] = ' // запрос <b><u><i>'.$sql.' </b></u></i>не выполнен! (' . $file . ' // ' . $function . ' // ' . $line . ')<hr>' . mysqli_error($link) . mysqli_sqlstate($link) . '<hr>';
        // $result_array['text'] .= '<pre>' . print_r($_SESSION, true) . '</pre>';
        // my_mail('Проблема с БД' . $line . ' / ' . $file . ' / ' . $function, $result_array['error'], $result_array['text'], 'txt');
    }
    mysqli_close($link);
    return $result_array;
}

/**
 * Функция удаляет данные из БД
**/
function del_data($sql) {
    $link = dbConnect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $status = true;
    } else {
        $status = 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link).'<br>';
    }
    mysqli_close($link);
    return $status;
}

/**
 * 
**/
function sql_data($line, $file, $function, $sql)
{
    $link = db_connect();

    if ($result = mysqli_query($link, $sql))  {
        if (substr_count(mb_strtolower($sql),'select') > 0) {
            $rows = mysqli_num_rows($result);

            if ($rows > 0) $result_array['res'] = 1;
            else $result_array['res'] = 0;
            
            $result_array['count'] = $rows;
            $result_array['data'] = [];
            
            for($i = 0; $i < $rows; ++$i) {
                $result_array['data'][] = mysqli_fetch_assoc($result);
            }

        } else {
            $result_array['res'] = 1;
            $result_array['id'] = mysqli_insert_id($link);
        }

        if(is_object($result)) mysqli_free_result($result);

    } else {
        echo ' // запрос ' . $sql . ' не выполнен! (' . $file . ' // ' . $function . ' // ' . $line . ')<hr>' . mysqli_error($link) . mysqli_sqlstate($link) . '<hr>';
        $result_array['res'] = 0;
        $result_array['error'] = ' // запрос <b><u><i>' . $sql . ' </b></u></i>не выполнен! (' . $file . ' // ' . $function . ' // ' . $line . ')<hr>' . mysqli_error($link) . mysqli_sqlstate($link) . '<hr>';
        $result_array['text'] .= '<pre>' . print_r($_SESSION, true) . '</pre>';
        my_mail('Проблема с БД' . $line . ' / ' . $file . ' / ' . $function, $result_array['error'], $result_array['text'], 'txt');
    }
    mysqli_close($link);
    return $result_array;
}

?>