<?php

function import_vp($in_txt, $place, $link)
{
    if (!isset($place)){
        echo 'нет участка';
        return false;
    }
    if (!isset($in_txt)){
        echo 'нет входящего файла';
        return false;
    }
    $in_txt = str_replace('<operation ', '<operation1 ', $in_txt);
    $in_txt = str_replace('</operation>', '</operation1>', $in_txt);

    $p = xml_parser_create();
    xml_parse_into_struct($p, $in_txt, $in, $index);
    xml_parser_free($p);

    if (!isset($in)){
        echo 'файл не прошёл';
        return false;
    }

    $in = vals_out($in);
    $index = make_index($in);
    $date = new DateTime($in[0]['attributes']['DATE']);

    $vals[0] = array('tag' => 'PROJECT', 'type' => 'open', 'level' => 1, 'attributes' => array(
        'CURRENCY' => $in[0]['attributes']['CURRENCY'],
        'VERSION' => $in[0]['attributes']['VERSION'],
        'DATE' => $date->getTimestamp(),
        'NAME' => $in[0]['attributes']['NAME'],
        'DESCRIPTION' => $in[0]['attributes']['DESCRIPTION'],
        'project.my_client' => $in[2]['attributes']['NAME'],
        'project.my_client_tel' => $in[2]['attributes']['PHONE'],
        'project.my_client_email' => $in[2]['attributes']['EMAIL'],
        'project.my_label' => 'new',
    ));

    $vals[1] = array('tag' => 'PROJECT', 'type' => 'close', 'level' => 1);

    foreach($index["MATERIAL"] as $i) {
        if ($in[$i]['attributes']['TYPE'] == "sheet") {
            $res = vp_put_source_in($vals, $link, array('sheet_in' => array(array($in[$i]['attributes']['WIDTH'], $in[$i]['attributes']['HEIGHT'], $in[$i]['attributes']['THICKNESS'], $in[$i]['attributes']['NAME']))), $place);
            $vals = $res['vals'];
            $mat_con[$in[$i]['attributes']['ID']] = $res['cs_id'];
        } elseif ($in[$i]['attributes']['TYPE'] == "band") {
            $res = vp_put_source_in($vals, $link, array('band_in' => array(array($in[$i]['attributes']['HEIGHT'], $in[$i]['attributes']['THICKNESS'], $in[$i]['attributes']['NAME']))), $place);
            $vals = $res['vals'];
            $band_con[$in[$i]['attributes']['ID']] = $res['el_id'];
            $band_con_code[$in[$i]['attributes']['ID']] = $res['band_code'];
            $band_t[$in[$i]['attributes']['ID']] = $res['band_t'];
            
            if (!isset($res['el_elWidthPreJoint'])) $el_pj = $res['el_elWidthPreJoint'];
            else if ($el_pj < $res['el_elWidthPreJoint']) $el_pj = $res['el_elWidthPreJoint'];
        }
    }

    $vals = move_vals($vals, 1, count($index['DETAIL']) / 2 + 2);
    $vals[1] = Array (
            "tag" => "GOOD",
            "type" => "open",
            "level" => 2,
            "attributes" => Array (
                    "ID" => new_id($vals) + 1,
                    "NAME" => "product_new",
                    "TYPEID" => "product",
                    "COUNT" => 1
			)
	);
    $i = 1;

    foreach ($index['DETAIL'] as $j) {
        if ($in[$j]['type'] == "open") {
            $detail[$j] = $in[$j];
        }
    }

    $v_in = make_index($vals);
    ksort($vals);

    foreach ($detail as $key => $ar) {
        $i++;
        if (!isset($ar['attributes']["WIDTHFULL"])) $ar['attributes']["WIDTHFULL"] = $ar['attributes']["WIDTH"];
        if (!isset($ar['attributes']["HEIGHTFULL"])) $ar['attributes']["HEIGHTFULL"] = $ar['attributes']["HEIGHT"];
        $vals[$i] = Array (
            "tag" => "PART",
            "type" => "complete",
            "level" => 3,
            "attributes" => Array (
                    "COUNT" => $ar['attributes']["AMOUNT"],
                    "DL" => $ar['attributes']["WIDTHFULL"],
                    "DW" => $ar['attributes']["HEIGHTFULL"],
                    "ID" => $ar['attributes']["ID"] + 1000,
                    "NAME" => $ar['attributes']["DESCRIPTION"],
                    "USEDCOUNT" => 0
            )
        );

        foreach ($v_in['OPERATION'] as $g) {
            if (($vals[$g]['attributes']['TYPEID'] == 'CS') && ($vals[$g]['attributes']['ID'] == $mat_con[$ar['attributes']["MATERIAL"]])) {
                $ar_d_mat = $vals[$g+1]['attributes']['ID'];
                $vals = move_vals($vals, $g + 2, 1);
                $vals[$g+2] = Array (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes" => Array ("ID" => $vals[$i]['attributes']["ID"])
                );
                $vals[$i]['attributes']['T'] = $vals[$g]['attributes']['T'];
            }
            ksort ($vals);
        }

        $v_in = make_index($vals);
        $k = $key + 2;
        $side = array('LEFT' => 'L', 'BOTTOM' => 'B', 'TOP' => 'T', 'RIGHT' => 'R');

        while($k < $key + 6) {
            if ($in[$k]['attributes']['TYPE'] == "srez") {
                $vals[$i]['attributes']['part.my_cut_angle_' . mb_strtolower($side[$in[$k]['tag']])] = $in[$k]['attributes']['PARAM'];
                
                //.. 29.06.2021 Добавлены параметры для корректной обработки срезов торца
                // В Вияр сдвига нет, поэтому сдвиг устанавливаем в ноль (для корректного отбражения в доп. операциях)
                $vals[$i]['attributes']['part.my_cut_angle_sdvig_' . mb_strtolower($side[$in[$k]['tag']])] = 0;
                $vals[$i]['attributes']['my_cut_angle_' . mb_strtolower($side[$in[$k]['tag']])] = $in[$k]['attributes']['PARAM'];
                $vals[$i]['attributes']['MY_CUT_ANGLE_' . mb_strtoupper($side[$in[$k]['tag']])] = $in[$k]['attributes']['PARAM'];
                $vals[$i]['attributes']['my_cut_angle_sdvig_' . mb_strtolower($side[$in[$k]['tag']])] = 0;
                $vals[$i]['attributes']['MY_CUT_ANGLE_SDVIG_' . mb_strtoupper($side[$in[$k]['tag']])] = 0;

            } elseif ($in[$k]['attributes']['TYPE'] == "kromka") {
                
                $vals[$i]['attributes']['EL' . $side[$in[$k]['tag']]] = '@operation#' . $band_con[$in[$k]['attributes']['PARAM']];
                $vals[$i]['attributes']['part.el_side_'.$side[$in[$k]['tag']]] = $band_t[$in[$k]['attributes']['PARAM']] - 1;
                $dd[$side[$in[$k]['tag']]] = $band_con_code[$in[$k]['attributes']['PARAM']];

                foreach ($v_in['OPERATION'] as $g) {
                    if (($vals[$g]['attributes']['ID'] == $band_con[$in[$k]['attributes']['PARAM']])) {
                        $g1 = $g + 2;
                        unset($no);

                        while ($vals[$g1]['tag'] == "PART") {
                            if ($vals[$g1]['attributes']['ID'] == $vals[$i]['attributes']['ID']) $no = 1;
                            $g1++;
                        }

                        if (!isset($no)) {
                            $vals = move_vals($vals, $g + 2, 1);
                            $vals[$g+2] = Array (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes" => Array ("ID" => $vals[$i]['attributes']['ID'])
                            );
                            $v_in = make_index($vals);
                        }

                    }
                }

            }
            $k++;
        }

        $index = make_index($vals);

        if ($ar['attributes'][mb_strtoupper("multiplicity")] > 1) {
        	$ar_double[] = array (
        		$vals[$i]['attributes']["ID"] => array (
		            'part_id' => $vals[$i]['attributes']["ID"],
		            'band_id_l' => $dd['L'],
		            'band_id_r' => $dd['R'],
		            'band_id_t' => $dd['T'],
		            'band_id_b' => $dd['B'],
		            'type_double' => 1,
		            'back_double' => $ar_d_mat
        		)
        	);
        	unset($dd,$ar_d_mat);
        	$k++;
        	ksort($vals);
        	$v_in = make_index($vals);
        }

        if (($in[$k]['tag'] == "OPERATIONS") && ($in[$k]['type'] == "open")) {
            $k++;
            $k_st = $k;

            while ($in[$k]['tag'] == "OPERATION1") {
                if ($in[$k]['attributes']['TYPE'] == "clipping") {
                    if (($in[$k]['attributes']['CUTVSIZE'] < $vals[$i]['attributes']['DW']) && ($in[$k]['attributes']['CUTVSIZE'] > 0)) {
                        $vals[$i]['attributes']['DW'] = $in[$k]['attributes']['CUTVSIZE'];
                    } if (($in[$k]['attributes']['CUTHSIZE'] < $vals[$i]['attributes']['DL']) && ($in[$k]['attributes']['CUTHSIZE'] > 0)) {
                        $vals[$i]['attributes']['DL'] = $in[$k]['attributes']['CUTHSIZE'];
                    }
                }
                $k++;
            }

            $k = $k_st;
            $sides = array(1, 2, 3, 4, 5);
            unset($op_xnc_false, $op_xnc_true);

            while ($in[$k]['tag'] == "OPERATION1") {
                if ($in[$k]['attributes']['SIDE'] == 6) $op_xnc_false[] = $in[$k];
                elseif (in_array($in[$k]['attributes']['SIDE'], $sides)) $op_xnc_true[] = $in[$k];
                $k++;
            }

            if (isset($op_xnc_true)) {
                $xnc_in = Array (
                    "tag" => "OPERATION",
                    "type" => "open",
                    "level" => 2,
                    "attributes"=> Array ("ID" => new_id($vals) + mt_rand(450, 34500))
				);
                
                $xnc_in['attributes'][mb_strtoupper('bySizeDetail')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('mirHor')] = 'false';
                $xnc_in['attributes'][mb_strtoupper('mirVert')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('typeId')] = 'XNC';
                $xnc_in['attributes'][mb_strtoupper('side')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('typename')] = 'Деталь id=' . $vals[$i]['attributes']['ID'] . ' (лицо)';
                $xnc_in['attributes'][mb_strtoupper('printable')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('startNewPage')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('code')] = $vals[$i]['attributes']['ID'] . '_' . $xnc_in['attributes']['ID'];
                $xnc_in['attributes'][mb_strtoupper('turn')] = 0;
                $xnc_pr = array (
                    array (
                        'tag' => 'PROGRAM',
                        'type' => 'open',
                        'level' => 1,
                        'attributes' => array (
                            'DX' => $vals[$i]['attributes']['DL'],
                            'DY' => $vals[$i]['attributes']['DW'],
                            'DZ' => $vals[$i]['attributes']['T']
                        )
                    )
                );
                //.. Пила заменена с 4мм на 3.2мм
                // $xnc_pr[]=array(
                //     'tag'=>'TOOL','type'=>'complete','level'=>2,'attributes'=>array(
                //         'comment'=>"Пазовальная пила толщиной 4мм","name"=>"Cut4","d"=>"4"
                //     )
                // );
                $xnc_pr[] = array (
                    'tag' => 'TOOL',
                    'type' => 'complete',
                    'level' => 2,
                    'attributes' => array(
                        'comment' => "Пазовальная пила толщиной 3.2мм",
                        "name" => "Cut3.2",
                        "d" => "3.2"
                    )
                );

                foreach ($op_xnc_true as $xnc) {
                    if ($xnc['attributes']['TYPE'] == "grooving") {
                        //.. Расширен функционал (менеджерам для наглядности) в зависимости от сторон ($xnc['attributes']['SIDE'])
                        // для корректного переноса пазов
                        if (($xnc['attributes']['SUBTYPE'] == 0)) {
                            //horisont
                            $x1 = $xnc['attributes']['X'];

                            if ($xnc['attributes']['SIDE'] == 3) {
                                $y1 = $vals[$i]['attributes']['DW'] - $xnc['attributes']['Y'] + $xnc['attributes']['WIDTH'] / 2;
                            } elseif ($xnc['attributes']['SIDE'] == 5){
                                $y1 = $xnc['attributes']['Y'] + $xnc['attributes']['WIDTH'] / 2;
                            }

                            $x2 = $vals[$i]['attributes']['DL'];
                            $y2 = $y1;
                        } elseif ($xnc['attributes']['SUBTYPE'] == 1) {

                            //vert
                            if ($xnc['attributes']['SIDE'] == 2) {
                                $x1 = $xnc['attributes']['X'] + $xnc['attributes']['WIDTH'] / 2;
                            } elseif ($xnc['attributes']['SIDE'] == 4) {
                                $x1 = $vals[$i]['attributes']['DL'] - $xnc['attributes']['X'] + $xnc['attributes']['WIDTH'] / 2;
                            }
                            
                            $y1 = $xnc['attributes']['Y'];
                            $y2 = $vals[$i]['attributes']['DW'];
                            $x2 = $x1;
                        }

                        //.. Пила заменена с 4мм на 3.2мм
                        // if ($xnc['attributes']['WIDTH']<4)$xnc['attributes']['WIDTH']=4;
                        if ($xnc['attributes']['WIDTH'] < "3.2") $xnc['attributes']['WIDTH'] = "3.2";
                        $xnc_pr[] = array (
                            'tag' => 'GR',
                            'type' => 'complete',
                            'level' => 2,
                            'attributes' => array(
                                "name" => "Cut3.2",
                                "c" => "0",
                                "t" => $xnc['attributes']['WIDTH'],
                                "sidenum" => $xnc['attributes']['SIDE'],
                                "dp" => $xnc['attributes']['DEPTH'],
                                "x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2
                            )
						);
                    } elseif ($xnc['attributes']['TYPE'] == "drilling") {
                        $ar_dr = array (
                        	1 => 'BF',
                        	2 => 'BL',
                        	5 => 'BT',
                        	4 => 'BR',
                        	3 => 'BB'
                        );
                        if ($xnc['attributes']['SIDE'] == 1) {
                            if ($xnc['attributes']['DEPTH'] <= $vals[$i]['attributes']['T'] - 3) $bore = 'Bore' . $xnc['attributes']['D'];
                            else $bore = 'Bore' . $xnc['attributes']['D'] . '_through';
                            $xnc_pr[] = array (
                                'tag' => $ar_dr[$xnc['attributes']['SIDE']],
                                'type' => 'complete',
                                'level' => 2,
                                'attributes' => array (
                                    "name" => $bore,
                                    "dp" => $xnc['attributes']['DEPTH'],
                                    "x" => $xnc['attributes']['X'],
                                    "y" => $xnc['attributes']['Y'],
                                    'ac' => 1
                                )
                            );
                            $tool_bore[$bore] = $xnc['attributes']['D'];
                        } elseif (($xnc['attributes']['SIDE'] == 2) || ($xnc['attributes']['SIDE'] == 4)) {
                            $bore = 'Bore' . $xnc['attributes']['D'];
                            $xnc_pr[] = array (
                                'tag' => $ar_dr[$xnc['attributes']['SIDE']],
                                'type' => 'complete',
                                'level' => 2,
                                'attributes' => array (
                                    "name" => $bore,
                                    "dp" => $xnc['attributes']['DEPTH'],
                                    "z" => $xnc['attributes']['X'],
                                    "y" => $xnc['attributes']['Y'],
                                    'ver' => 2,
                                    'ac' => 0,
                                    'as' => 0,
                                    'm' => "false"
                                )
                            );
                            $tool_bore[$bore] = $xnc['attributes']['D'];
                        } elseif (($xnc['attributes']['SIDE'] == 3) || ($xnc['attributes']['SIDE'] == 5)) {
                            $bore = 'Bore' . $xnc['attributes']['D'];
                            $xnc_pr[] = array (
                                'tag' => $ar_dr[$xnc['attributes']['SIDE']],
                                'type' => 'complete',
                                'level' => 2,
                                'attributes' => array (
                                    "name" => $bore,
                                    "dp" => $xnc['attributes']['DEPTH'],
                                    "x" => $xnc['attributes']['X'],
                                    "z" => $xnc['attributes']['Y'],
                                    'ver' => 2,
                                    'ac' => 0,
                                    'as' => 0,
                                    'm' => "false"
                                )
                            );

                            $tool_bore[$bore] = $xnc['attributes']['D'];
                        }
                    } elseif ($xnc['attributes']['TYPE'] == "rabbeting") {
                        unset($r_w, $r_l);
                        if (($xnc['attributes']['EDGE'] == 2)) {

                            //horisont
                            $x1 = $xnc['attributes']['WIDTH'] / 2;
                            $y1 = 0;
                            $x2 = $x1;
                            $y2 = $vals[$i]['attributes']['DW'];

                        } elseif (($xnc['attributes']['EDGE'] == 3)) {

                            //horisont
                            $x1 = 0;
                            $y1 = $vals[$i]['attributes']['DW'] - $xnc['attributes']['WIDTH'] / 2;
                            $y2 = $y1;
                            $x2 = $vals[$i]['attributes']['DL'];

                        } elseif (($xnc['attributes']['EDGE'] == 4)) {
                            
                            //horisont
                            $x1 = $vals[$i]['attributes']['DL'] - $xnc['attributes']['WIDTH'] / 2;
                            $y1 = 0;
                            $x2 = $x1;
                            $y2 = $vals[$i]['attributes']['DW'] + 2;

                        } elseif (($xnc['attributes']['EDGE'] == 5)) {

                            //horisont
                            $x1 = 0;
                            $y1 = $xnc['attributes']['WIDTH'] / 2;
                            $y2 = $y1;
                            $x2 = $vals[$i]['attributes']['DL'] + 2;

                        }
                        //.. Пила заменена с 4мм на 3.2мм
                        $xnc_pr[] = array (
                            'tag' => 'GR',
                            'type' => 'complete',
                            'level' => 2,
                            'attributes' => array (
                                "name" => "Cut3.2",
                                "c" => "0",
                                "t" => $xnc['attributes']['WIDTH'],
                                "dp" => $xnc['attributes']['DEPTH'],
                                "x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2
                            )
                        );
                    }

                    //.. 17.06.2021 Добавлен новый блок. Это паз в торце, но с типом (TYPE => shapeByPattern)
                    // а идентификатор паза в торце "grooving" перенесён в атрибут "HANDLETYPE"
                    // Блок выполняет перенос xnc-операции, но по данной операции выдаётся исключение.
                    elseif ($xnc['attributes']['TYPE'] == "shapeByPattern" && $xnc['attributes']['HANDLETYPE'] == "grooving") {
                        if ($xnc['attributes']['SIDE'] == 3 || $xnc['attributes']['SIDE'] == 5) {

                            //horisont
                            $x1 = $xnc['attributes']['X'];
                            if ($xnc['attributes']['SIDE'] == 3) {
                                $y1 = $vals[$i]['attributes']['DW'] - $xnc['attributes']['SHIFTEDGE'] + $xnc['attributes']['WIDTH'] / 2;
                            } elseif ($xnc['attributes']['SIDE'] == 5){
                                $y1 = $xnc['attributes']['SHIFTEDGE'] + $xnc['attributes']['WIDTH'] / 2;
                            }

                            $x2 = $vals[$i]['attributes']['DL'];
                            $y2 = $y1;
                        } elseif ($xnc['attributes']['SIDE'] == 2 || $xnc['attributes']['SIDE'] == 4) {

                            //vert
                            if ($xnc['attributes']['SIDE'] == 2) {
                                $x1 = $xnc['attributes']['SHIFTEDGE'] + $xnc['attributes']['WIDTH'] / 2;
                            } elseif ($xnc['attributes']['SIDE'] == 4) {
                                $x1 = $vals[$i]['attributes']['DL'] - $xnc['attributes']['SHIFTEDGE'] + $xnc['attributes']['WIDTH'] / 2;
                            }

                            $y1 = $xnc['attributes']['Y'];
                            $y2 = $vals[$i]['attributes']['DW'];
                            $x2 = $x1;
                        }

                        if ($xnc['attributes']['WIDTH'] < "3.2") $xnc['attributes']['WIDTH'] = "3.2";

                        $xnc_pr[] = array (
                            'tag' => 'GR',
                            'type' => 'complete',
                            'level' => 2,
                            'attributes' => array (
                                "name" => "Cut3.2",
                                "c" => "0",
                                "t" => $xnc['attributes']['WIDTH'],
                                "sidenum" => $xnc['attributes']['SIDE'],
                                "dp" => $xnc['attributes']['DEPTH'],
                                "x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2
                            )
                        );
                    }

                    //.. 21.06.2021 Добавлен новый блок, врезные ручки
                    // Такой опции в Кронасе нет, так что сразу записываем в исключения
                    elseif ($xnc['attributes']['TYPE'] == "shapeByPattern" && $xnc['attributes']['HANDLETYPE'] == "borderAndRabbets") {
                        $tmp['ID'] = $xnc_in["attributes"]['ID'];
                        $tmp['CODE'] = $xnc_in["attributes"]['CODE'];
                        $tmp['TYPENAME'] = $xnc_in["attributes"]['TYPENAME'];
                        $_SESSION['tec_info']['borderAndRabbets_errors'][] = $tmp;
                    }
                }
                
                foreach ($tool_bore as $ktb => $tb) {
                    $ar_bt = array (
                        'tag' => 'TOOL',
                        'type' => 'complete',
                        'level' => 2,
                        'attributes' => array (
                            "name" => $ktb,
                            "d" => $tb
                        )
                    );
                    $xnc_pr = move_vals($xnc_pr, 2, 1);
                    $xnc_pr[2] = $ar_bt;
                }
                
                unset($xnc);
                ksort($xnc_pr);
                unset($tool_bore);
                $xnc_pr[] = array(
					'tag' => 'PROGRAM',
					'type' => 'close',
					'level' => 1
                );

                $xnc_in['attributes'][mb_strtoupper('program')] = vals_index_to_project($xnc_pr);
                $xnc_out = array (
                    'tag' => 'OPERATION',
                    'type' => 'close',
                    'level' => 1
                );
                
                $xnc_part = Array (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes" => Array ("ID" => $vals[$i]['attributes']['ID'])
                );
                $vals_in[] = $xnc_in;
                $vals_in[] = $xnc_part;
                $vals_in[] = $xnc_out;
                
                //.. 15.06.2021 Если есть пазы в торце, то "запоминаем" деталь в сессии
                if (preg_match_all('#sidenum\s?=\s?["|\']\d+["|\']#', $xnc_in["attributes"]["PROGRAM"], $matches)) {
                    foreach ($matches[0] as $val) {
                        $v = str_replace('sidenum', '', $val);
                        $v = str_replace('=', '', $v);
                        $v = str_replace(' ', '', $v);
                        $v = str_replace('"', '', $v);
                        $v = str_replace("'", '', $v);
                        if ($v != 1 && $v != 6) {
                            $tmp['ID'] = $xnc_in["attributes"]['ID'];
                            $tmp['CODE'] = $xnc_in["attributes"]['CODE'];
                            $tmp['TYPENAME'] = $xnc_in["attributes"]['TYPENAME'];
                            $_SESSION['tec_info']['grooving_errors'][] = $tmp;
                            break 1;
                        }
                    }
                }

            }
            
            if (isset($op_xnc_false)) {
                $xnc_in = Array (
                    "tag" => "OPERATION",
                    "type" => "open",
                    "level" => 2,
                    "attributes" => Array ("ID" => new_id($vals) + mt_rand(450, 34500))
                );
                $xnc_in['attributes'][mb_strtoupper('bySizeDetail')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('mirHor')] = 'false';
                $xnc_in['attributes'][mb_strtoupper('mirVert')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('typeId')] = 'XNC';
                $xnc_in['attributes'][mb_strtoupper('side')] = 'false';
                $xnc_in['attributes'][mb_strtoupper('typename')] = 'Деталь id=' . $vals[$i]['attributes']['ID'] . ' (обратная)';
                $xnc_in['attributes'][mb_strtoupper('printable')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('startNewPage')] = 'true';
                $xnc_in['attributes'][mb_strtoupper('code')] = $vals[$i]['attributes']['ID'] . '_' . $xnc_in['attributes']['ID'];
                $xnc_in['attributes'][mb_strtoupper('turn')] = 0;
                $xnc_pr = array (
                    array (
                        'tag' => 'PROGRAM',
                        'type' => 'open',
                        'level' => 1,
                        'attributes' => array (
                            'DX' => $vals[$i]['attributes']['DL'],
                            'DY' => $vals[$i]['attributes']['DW'],
                            'DZ' => $vals[$i]['attributes']['T']
                        )
                    )
                );

                //.. Пила заменена с 4мм на 3.2мм
                $xnc_pr[] = array (
                    'tag' => 'TOOL',
                    'type' => 'complete',
                    'level' => 2,
                    'attributes' => array (
                        'comment' => "Пазовальная пила толщиной 3.2мм",
                        "name" => "Cut3.2",
                        "d" => "3.2"
                    )
                );

                foreach ($op_xnc_false as $xnc) {
                    if ($xnc['attributes']['TYPE'] == "grooving") {
                        if (($xnc['attributes']['SUBTYPE'] == 0)) {

                            //horisont
                            $x1 = $xnc['attributes']['X'];
                            $y1 = $xnc['attributes']['Y'] + $xnc['attributes']['WIDTH'] / 2;
                            $x2 = $vals[$i]['attributes']['DL'];
                            $y2 = $y1;

                        } elseif ($xnc['attributes']['SUBTYPE'] == 1) {
                            
                            //vert
                            $x1 = $xnc['attributes']['X'] + $xnc['attributes']['WIDTH'] / 2;
                            $y1 = $xnc['attributes']['Y'];
                            $y2 = $vals[$i]['attributes']['DW'];
                            $x2 = $x1;

                        }

                        //.. Пила заменена с 4мм на 3.2мм
                        // if ($xnc['attributes']['WIDTH']<4)$xnc['attributes']['WIDTH']=4;
                        if ($xnc['attributes']['WIDTH'] < "3.2") $xnc['attributes']['WIDTH'] = "3.2";
                        $xnc_pr[] = array (
                            'tag' => 'GR',
                            'type' => 'complete',
                            'level' => 2,
                            'attributes' => array (
                                "name" => "Cut3.2",
                                "c" => "0",
                                "t" => $xnc['attributes']['WIDTH'],
                                "sidenum" => $xnc['attributes']['SIDE'],
                                "dp" => $xnc['attributes']['DEPTH'],
                                "x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2
                            )
                        );
                    } elseif ($xnc['attributes']['TYPE'] == "drilling") {
                        $ar_dr = array (
							6 => 'BF',
							2 => 'BL',
							5 => 'BT',
							4 => 'BR',
							3 => 'BB'
                        );

                        if ($xnc['attributes']['SIDE'] == 6) {
                            if ($xnc['attributes']['DEPTH'] <= $vals[$i]['attributes']['T'] - 3) $bore = 'Bore' . $xnc['attributes']['D'];
                            else $bore = 'Bore' . $xnc['attributes']['D'] . '_through';
                            $xnc_pr[] = array (
                                'tag' => $ar_dr[$xnc['attributes']['SIDE']],
                                'type' => 'complete',
                                'level' => 2,
                                'attributes' => array (
                                    "name" => $bore,
                                    "dp" => $xnc['attributes']['DEPTH'],
                                    "x" => $xnc['attributes']['X'],
                                    "y" => $xnc['attributes']['Y'],
                                    'ac' => 1
                                )
                            );
                            $tool_bore[$bore]=$xnc['attributes']['D'];
                        } elseif (($xnc['attributes']['SIDE'] == 2) || ($xnc['attributes']['SIDE'] == 4)) {
                            $bore = 'Bore' . $xnc['attributes']['D'];
                            $xnc_pr[] = array (
                                'tag' => $ar_dr[$xnc['attributes']['SIDE']],
                                'type' => 'complete',
                                'level' => 2,
                                'attributes' => array (
                                    "name" => $bore,
                                    "dp" => $xnc['attributes']['DEPTH'],
                                    "z" => $xnc['attributes']['X'],
                                    "y" => $xnc['attributes']['Y'],
                                    'ver' => 2,
                                    'ac' => 0,
                                    'as' => 0,
                                    'm' => "false"
                                )
                            );
                            $tool_bore[$bore]=$xnc['attributes']['D'];
                        } elseif (($xnc['attributes']['SIDE'] == 3) || ($xnc['attributes']['SIDE'] == 5)) {
                            $bore = 'Bore' . $xnc['attributes']['D'];
                            $xnc_pr[] = array (
                                'tag' => $ar_dr[$xnc['attributes']['SIDE']],
                                'type' => 'complete',
                                'level' => 2,
                                'attributes' => array (
									"name" => $bore,
									"dp" => $xnc['attributes']['DEPTH'],
									"x" => $xnc['attributes']['X'],
									"z" => $xnc['attributes']['Y'],
									'ver' => 2,
									'ac' => 0,
									'as' => 0,
									'm' => "false"
                                )
                            );
                            $tool_bore[$bore]=$xnc['attributes']['D'];
                        }
                    } elseif ($xnc['attributes']['TYPE'] == "rabbeting") {
                        unset($r_w, $r_l);
                        if ($vals[$i]['attributes']['part.el_side_T'] > 0) $r_w++;
                        if ($vals[$i]['attributes']['part.el_side_B'] > 0) $r_w++;
                        if ($vals[$i]['attributes']['part.el_side_L'] > 0) $r_l++;
                        if ($vals[$i]['attributes']['part.el_side_R'] > 0) $r_l++;

                        if (($xnc['attributes']['EDGE'] == 2)) {

                            //horisont
                            $x1 = $xnc['attributes']['WIDTH'] / 2;
                            $y1 = 0;
                            $x2 = $x1;
                            $y2 = $vals[$i]['attributes']['DW'];

                        } elseif (($xnc['attributes']['EDGE'] == 3)) {

                            //horisont
                            $x1 = 0;
                            $y1 = $vals[$i]['attributes']['DW'] - $xnc['attributes']['WIDTH'] / 2;
                            $y2 = $y1;
                            $x2 = $vals[$i]['attributes']['DL'];

                        } elseif (($xnc['attributes']['EDGE'] == 4)) {

                            //horisont
                            $x1 = $vals[$i]['attributes']['DL'] - $xnc['attributes']['WIDTH'] / 2;
                            $y1 = 0;
                            $x2 = $x1;
                            $y2 = $vals[$i]['attributes']['DW'] + 2;

                        } elseif (($xnc['attributes']['EDGE'] == 5)) {

                            //horisont
                            $x1 = 0;
                            $y1 = $xnc['attributes']['WIDTH'] / 2;
                            $y2 = $y1;
                            $x2 = $vals[$i]['attributes']['DL'];

                        }

                        //.. Пила заменена с 4мм на 3.2мм
                        $xnc_pr[] = array (
                            'tag' => 'GR',
                            'type' => 'complete',
                            'level' => 2,
                            'attributes' => array (
                                "name"=>"Cut3.2",
                                "c" => "0",
                                "t" => $xnc['attributes']['WIDTH'],
                                "dp" => $xnc['attributes']['DEPTH'],
                                "x1" => $x1,
                                "x2" => $x2,
                                "y1" => $y1,
                                "y2" => $y2
                            )
                        );
                    }

                    //.. 21.06.2021 Добавлен новый блок, врезные ручки
                    // Такой опции в Кронасе нет, так что сразу записываем в исключения
                    elseif ($xnc['attributes']['TYPE'] == "shapeByPattern" && $xnc['attributes']['HANDLETYPE'] == "borderAndRabbets") {
                        $tmp['ID'] = $xnc_in["attributes"]['ID'];
                        $tmp['CODE'] = $xnc_in["attributes"]['CODE'];
                        $tmp['TYPENAME'] = $xnc_in["attributes"]['TYPENAME'];
                        $_SESSION['tec_info']['borderAndRabbets_errors'][] = $tmp;
                    }
                }
                
                foreach ($tool_bore as $ktb => $tb) {
                    $ar_bt = array (
                        'tag' => 'TOOL',
                        'type' => 'complete',
                        'level' => 2,
                        'attributes' => array (
                            "name" => $ktb,
                            "d" => $tb
                        )
                    );
                    $xnc_pr = move_vals($xnc_pr, 2, 1);
                    $xnc_pr[2] = $ar_bt;
                }
                
                ksort($xnc_pr);
                unset($tool_bore, $xnc);
                $xnc_pr[] = array(
                    'tag' => 'PROGRAM',
                    'type' => 'close',
                    'level' => 1
                );
                $xnc_in['attributes'][mb_strtoupper('program')] = vals_index_to_project($xnc_pr);

                $xnc_out = array(
                    'tag' => 'OPERATION',
                    'type' => 'close',
                    'level' => 1
                );
                $xnc_part = Array (
					"tag" => "PART",
					"type" => "complete",
					"level" => 2,
					"attributes" => Array ( "ID" => $vals[$i]['attributes']['ID'] )
                );
				$vals_in[] = $xnc_in;
				$vals_in[] = $xnc_part;
				$vals_in[] = $xnc_out;

                //.. 15.06.2021 Если есть пазы в торце, то "запоминаем" деталь в сессии
                if (preg_match_all('#sidenum\s?=\s?["|\']\d+["|\']#', $xnc_in["attributes"]["PROGRAM"], $matches)) {
                    foreach ($matches[0] as $val) {
                        $v = str_replace('sidenum', '', $val);
                        $v = str_replace('=', '', $v);
                        $v = str_replace(' ', '', $v);
                        $v = str_replace('"', '', $v);
                        $v = str_replace("'", '', $v);
                        if ($v != 1 && $v != 6) {
                            $tmp['ID'] = $xnc_in["attributes"]['ID'];
                            $tmp['CODE'] = $xnc_in["attributes"]['CODE'];
                            $tmp['TYPENAME'] = $xnc_in["attributes"]['TYPENAME'];
                            $_SESSION['tec_info']['grooving_errors'][] = $tmp;
                            break 1;
                        }
                    }
                }
                unset ($xnc_pr, $xnc_part, $xnc_in, $xnc_out, $op_xnc_false, $op_xnc_true);
            } 
        }
    }
    
    foreach ($index['PART'] as $i) {
        if ($vals[$i]['attributes']['DL'] > 0) {
            if ((isset($vals[$i]['attributes']['part.el_side_L']))
            	|| (isset($vals[$i]['attributes']['part.el_side_R']))
            	|| (isset($vals[$i]['attributes']['part.el_side_B']))
            	|| (isset($vals[$i]['attributes']['part.el_side_T'])))
            {
                $vals[$i]['attributes']['CL'] = $vals[$i]['attributes']['DL'];
                $vals[$i]['attributes']['L'] = $vals[$i]['attributes']['DL'];
                $vals[$i]['attributes']['CW'] = $vals[$i]['attributes']['DW'];
                $vals[$i]['attributes']['W'] = $vals[$i]['attributes']['DW'];
                $vals[$i]['attributes']['JL'] = $vals[$i]['attributes']['DL'];
                $vals[$i]['attributes']['JW'] = $vals[$i]['attributes']['DW'];
                
                foreach (SIDE as $s) {
                    if (($s == 'l') || ($s == 'r')) $l = 'L';
                    else $l = 'W';
                    
                    if (isset($vals[$i]['attributes']['part.el_side_' . mb_strtoupper($s)])) {
                        $vals[$i]['attributes']['C'.$l] = $vals[$i]['attributes']['C'.$l] - $vals[$i]['attributes']['part.el_side_'.mb_strtoupper($s)] + $el_pj;
                        $vals[$i]['attributes'][$l] = $vals[$i]['attributes']['C'.$l];
                        $vals[$i]['attributes']['J'.$l] = $vals[$i]['attributes']['J'.$l] - $vals[$i]['attributes']['part.el_side_'.mb_strtoupper($s)];
                    }
                }
                
                
            } else {
                $vals[$i]['attributes']['CL'] = $vals[$i]['attributes']['DL'];
                $vals[$i]['attributes']['L'] = $vals[$i]['attributes']['DL'];
                $vals[$i]['attributes']['CW'] = $vals[$i]['attributes']['DW'];
                $vals[$i]['attributes']['W'] = $vals[$i]['attributes']['DW'];
                $vals[$i]['attributes']['JL'] = $vals[$i]['attributes']['DL'];
                $vals[$i]['attributes']['JW'] = $vals[$i]['attributes']['DW'];
            }
        }
    }

    $vals = vals_out($vals);
    ksort($vals);
    $i = 2;
    
    while ($vals[$i]['tag'] == "PART") $i++;
    $vals = move_vals($vals, $i, 1);
    $vals[$i] = Array (
        "tag" => "GOOD",
        "type" => "close",
        "level" => 2
    );

    ksort($vals);
    $vals = vals_out($vals);

    foreach ($vals_in as $v) $vals = vals_into_end($vals, $v);
    ksort($vals);
    $vals = vals_out($vals);
    $vals = sort_vals($vals);
    $vals = add_tag_print($vals);

    foreach ($ar_double as $k => $v) {
        foreach ($v as $k1 => $v1) $array = array($k1 => $v1);
        if (($v1["type_double"] > 0) && ($v1["back_double"] > 0)) {
            $vals = do_double($vals, $_SESSION["user_place"], $array, $link);
        }
    }

    $res = vals_index_to_project($vals);
    return $res;
}

?>