<?php

// 07.02.2023 Тоценко
// Добавление остатков материалов в базу
require_once DIR_FUNCTIONS . 'fs_materials_leftover.php';

/**
 * 
**/
function put_programm($vals,$place,$link,$project, $new_order_id, $serv_main_dir,$s,$dir)
{
    $index=make_index($vals);
    $vals=add_tag_print($vals);
    unset($_SESSION['stanki_mpr_folder_venture']);
    $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'put_progr_start', $_SESSION);

    if (($new_order_id>0)&&(is_numeric($new_order_id)))
    {
        $sql='SELECT * FROM ORDER1C WHERE DB_AC_ID='.$new_order_id;
        $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        $order1c_log=$sql2['ID'];

    }
    else
    {
        die ('Заказ new_order_id не распознан');
    }
    //.. 07.10.21 Добавил генерацию программ черновиков для локального сервера
    if ($sql2['status']<>'черновик' || (stripos($_SERVER['SERVER_NAME'], '.loc') && $_SESSION['user']['ID'] == 218))
    {
        unset($er_log);
        
        if (!isset($_SESSION['user_place'])) $_SESSION['user_place']=$sql2['PLACE'];
        if ($_SESSION['user_place']=='') $_SESSION['user_place']=1;
        // da($_SESSION['user_place']);
        $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'put_progr_calc_start', $_SESSION);

        $res=calculate ($vals,$vals,$link,$_SESSION['user_place']);
        $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'put_progr_calc_end', $_SESSION);

        $pr_send['start']=vals_index_to_project($vals);
        $vals[0]['attributes']['NAME']=str2url($sql2['DB_AC_NUM']);
        $vals[0]['attributes']['project.my_order_name']=str2url($sql2['DB_AC_NUM']);
        $vals[0]['attributes']['project.my_order_1c']=$sql2['ID'];
        $vals[0]['attributes']['project.my_order_id']=$sql2['DB_AC_ID'];
        $vals[0]['attributes']['project.my_order_put_programm_date']=date_create()->format('Y-m-d H:i:s');
        $vals[0]['attributes']['project.my_client']=$sql2['client_fio'];
        $vals[0]['attributes']['project.my_client_tel']=$sql2['client_phone'];
        $vals[0]['attributes']['project.my_client_email']=$sql2['client_email'];
        $dir=$vals[0]['attributes']['NAME'];
        foreach ($index['GOOD'] as $i)
        {
            if ($vals[$i]['attributes']['TYPEID']=='sheet') $vals[$i]['attributes']['NAME']=rus2translit(substr(htmlspecialchars($vals[$i]['attributes']['NAME']),0,100));
        }
        
        foreach ($index['PART'] as $i)
        {
            if ((strlen($vals[$i]['attributes']['NAME'])>0)&&($vals[$i]['attributes']['CL']>0)) $vals[$i]['attributes']['NAME']=substr($vals[$i]['attributes']['NAME'],0,15);
        }
        if(($_GET['old_code']<>1)&&($_GET['prod']<>1))
        {
            foreach ($res['material_price'] as $mat)
            {
                $mat2=explode('.',$mat['sheet_count']);
                if ($mat2[1]==5)
                {
                    $sql='SELECT * FROM MATERIAL WHERE CODE='.$mat['material_code'].';';
                    $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                    foreach ($index['GOOD'] as $i)
                    {
                        if (($vals[$i]['attributes']['TYPEID']=="sheet")&&($vals[$i]['attributes']['ID']==$mat['id']))
                        {
                            foreach ($index['OPERATION'] as $k2)
                            {
                                if (($vals[$k2]['attributes']['TYPEID']=="CS")&&($vals[$k2+1]['attributes']['ID']==$vals[$i]['attributes']['ID'])&&
                                ($vals[$k2]['attributes']['operation.my_fix_card']<>1))
                                {
                                    $j=$i+1;
                                    while ($vals[$j]['tag']<>"PART")
                                    {
                                        $j++;
                                    }
                                    if(($vals[$j]['tag']=="PART")&&($sql3['L']==$vals[$j]['attributes']['L'])&&($sql3['W']==$vals[$j]['attributes']['W'])&&(!isset($vals[$j]['attributes']['SHEETID'])))
                                    {
                                        $count=$vals[$j]['attributes']['USEDCOUNT']-1;
                                        $vals[$j]['attributes']['USEDCOUNT']=0;
                                        $vals[$j]['attributes']['COUNT']=$count;
                                        foreach ($index['OPERATION'] as $k)
                                        {
                                            if ($vals[$k]['attributes']['TYPEID']=="CS")
                                            {
                                                $vals[$k]['attributes']['CSDIRECTCUT']=3;
                                                $kk=$k+2;
                                                while ($vals[$kk]['tag']=="PART")
                                                {
                                                    if ($vals[$kk]['attributes']['ID']==$vals[$j]['attributes']['ID'])
                                                    {
                                                        $cs=$vals[$k]['attributes']['ID'];
                                                        //.. 15.09.2022 Правки, чтобы совпвдали раскрои при редактировании и оформлении проекта
                                                        // unset ($vals[$k]['attributes']['DATA']);
                                                        unset ($vals[$k]['attributes']['DRAW']);
                                                    }
                                                    $kk++;
                                                }
                                            }
                                        }
                                        $vals=move_vals($vals,$j+1,1);
                                        $vals[$j+1]=Array
                                        (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes"=> 
                                            Array (
                                                "L"=>$sql3['L'],
                                                "W"=>$sql3['W']/2-2.5,
                                                "COUNT"=>1,
                                                "USEDCOUNT"=>0,
                                                "ID"=>new_id($vals)+115
                                            )
                                        );
                                        // da($vals[$j]);

                                        $cs_in_id=$vals[$j+1]['attributes']['ID'];
                                        // ksort ($vals);
                                        $j=$j+2;
                                        $index=make_index($vals);

                                        while (($vals[$j]['tag']=="PART")&&(isset($vals[$j]['attributes']['SHEETID'])))
                                        {
                                            $id_part_del=$vals[$j]['attributes']['ID'];
                                            foreach ($index["PART"] as $p)
                                            {
                                                if ($vals[$p]['attributes']['ID']==$id_part_del) unset ($vals[$p]);
                                            }
                                            $j++;
                                        }
                                        $index=make_index($vals);
                                        foreach ($index['OPERATION'] as $k)
                                        {
                                            if (($vals[$k]['attributes']['TYPEID']=="CS")&&($vals[$k]['attributes']['ID']==$cs))
                                            {
                                                $t=$k+2;
                                                while ($vals[$t]['tag']=="PART")
                                                {
                                                    $t++;
                                                }

                                                $vals=move_vals($vals,$t,1);
                                                $vals[$t]=Array
                                                    (
                                                        "tag" => "PART",
                                                        "type" => "complete",
                                                        "level" => 2,
                                                        "attributes"=> Array ("ID"=>$cs_in_id)
                                                    );
                                            }
                                        }
                                        ksort($vals);
                                        $index=make_index($vals);
                                    }
                                }
                            }
                        }
                    }
                }
                // da($mat);
            }
            foreach ($index['OPERATION'] as $k)
            {
                if (($vals[$k]['attributes']['TYPEID']=="CS")&&($vals[$k+2]['tag']=='PART')&&($vals[$k]['attributes']['operation.my_fix_card']<>1)&&(!isset($vals[$k]['attributes']['DATA'])))
                {
                    //.. 15.09.2022 Правки, чтобы совпвдали раскрои при редактировании и оформлении проекта
                    // unset ($vals[$k]['attributes']['DATA']);
                    unset ($vals[$k]['attributes']['DRAW']);
                    $kk=$k+2;
                    while ($vals[$kk]['tag']=='PART')
                    {
                        unset($check);
                        foreach ($index['PART'] as $i)
                        {
                            if ((($vals[$i]['attributes']['CL']>0)||($vals[$i]['attributes']['L']>0))&&($vals[$i]['attributes']['ID']==$vals[$kk]['attributes']['ID'])) $check=1;
                        }
                        if (!isset($check)) unset ($vals[$kk]);
                    
                        $kk++;
                    }
                }
            }
        }
        ksort($vals);
        $index=make_index($vals);
        
        $pr_send['1after_calc']=vals_index_to_project($vals);
        $_SESSION['user_place']=$place;
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progr_prepared', $_SESSION);
        if (mr_change_vals ($vals)<>false) $vals=mr_change_vals ($vals);
        $vals_without_gr=change_gr($vals);
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_ready', $_SESSION);
        if(($_GET['old_code']<>1)&&($_GET['prod']<>1))
        {
            $pr_send['2_before_change_gr']=vals_index_to_project($vals);
            
            $vals_bhx=xnc_ready_for_get("BHX",$vals_without_gr);
            $vals_bhx=xnc_to_production ($vals_bhx,'bhx');
            
            $pr_send['3_ready_for_bhx']=vals_index_to_project($vals_bhx);
            
            $vals_venture=xnc_ready_for_get("VENTURE",$vals_without_gr);
            $vals_venture=xnc_to_production ($vals_venture,'venture');
            
            $pr_send['3_ready_for_vent']=vals_index_to_project($vals_venture);
            
            $project_bhx=vals_index_to_project($vals_bhx);
            $xnc_in=op_xnc_check ($project_bhx);
            $project_venture=vals_index_to_project($vals_venture);
        }
        elseif($_GET['prod']==1)
        {
            $vals_without_gr=xnc_to_production ($vals_without_gr);
        }
        
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_make_vals_for_operation', $_SESSION);
        $xnc_in_copy=op_xnc_check_copy ($vals);
        $_SESSION['op_ch']=1;
        unset($_SESSION['error']);
        if (!isset($vals_bhx)) $data_xml=gl_55 (__LINE__,__FILE__,__FUNCTION__,vals_index_to_project($vals));
        else $data_xml=gl_55 (__LINE__,__FILE__,__FUNCTION__,vals_index_to_project($vals_bhx));
        // $data_xml=vals_index_to_project($vals);

        // 07.02.2023 Тоценко
        // Добавление остатков материалов в базу
        if (isset($new_order_id) || $new_order_id !== '' || $new_order_id !== NULL) {
            writeMaterialsLeftovers($data_xml, $new_order_id);
        }

        if ((isset($_SESSION['error']))&&($_SESSION['error']<>''))
        {
            $er_log.= '<br> Проблема с раскроем:
                 
            '.$_SESSION['error'].'<br><br>
            ';
            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_optimize', $_SESSION);
        }
        else
        {
            $_SESSION["project_data"]=$data_xml;

            $pdf_get_project=$_SESSION['project_data'];
            $vals=get_vals($data_xml);
            $index=make_index($vals);
            $pr_send['4_after_gl55']=vals_index_to_project($vals);
                
            foreach ($index['GOOD'] as $k)
            {
                if (($vals[$k]['attributes']['TYPEID']=="sheet")&&($vals[$k+1]['tag']=='PART'))
                {
                    $log.= '<br> Данные по распилу:
                     
                    '.$vals[$k]['attributes']['NAME'].' ('.$vals[$k]['attributes']['CODE'].')<br><br>
                    ';
                    $k2=$k+1;
                    while ($vals[$k2]['tag']=="PART")
                    {
                        if ($vals[$k2]['attributes']['SHEETID']>0) $log.= ' - остаток  ('.$vals[$k2]['attributes']['L'].' / '.$vals[$k2]['attributes']['W'].' / '.$vals[$k]['attributes']['T'].' / кол-во '.$vals[$k2]['attributes']['COUNT'].')<br><br>';
                        else $log.= ' - использовано  ('.$vals[$k2]['attributes']['L'].' / '.$vals[$k2]['attributes']['W'].' / '.$vals[$k]['attributes']['T'].' / кол-во '.$vals[$k2]['attributes']['USEDCOUNT'].')<br><br>';
                        $k2++;
                    }
                }
            }
            unset ($ppp);
            check_opt_part ($vals, 1);
            if ($new_order_id>0)
            {
                $sql="select * from ticket where order_id=".$sql2['ID'].";";
                $ticket=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['id'];
                if ($ticket>0)
                {
                    $sql="select * from ticket_construct_mpr where ticket_id=".$ticket.";";
                    $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                }
                else
                {
                    $log.= 'MPR - файлов по заказу не найдено, так как отсутствует ticket (обращение).';
                    unset ($sql3);
                }
            }
            unset($mpr_copy);
            foreach ($sql3 as $file_mpr)
            {
                $mpr_copy[$file_mpr['mpr_id']]='http://service.kronas.com.ua/helpdesc/uploads/' . $file_mpr['ticket_id'] . '/mpr/' . $file_mpr['name'] . '.mpr';
            }
            if (count($mpr_copy)>0) {
                $log.= '<br>Копирование MPR: ';
                $log.=print_r($mpr_copy,true);
            }
            if ($xnc_in==false) ds('ЧПУ программ XNC в заказе нет.');
            if ($xnc_in_copy==false) ds('копированных MPR в заказе нет.');
            $vals11=get_vals($_SESSION['project_data']);
            $vals11[0]['attributes']['project.my_order_id']=strrev($vals11[0]['attributes']['project.my_order_id']);
            $_SESSION['project_data']=vals_index_to_project($vals11);
            // da($vals11);
            $r[]=build_req_for_project($_SESSION['project_data'], '235F63BD','req_get');
            $r[]=build_req_for_project($_SESSION['project_data'], 'F6F4A9CC','req_get');
            $r[]=build_req_for_project($_SESSION['project_data'], 'DAD1ABF3','req_get');
            $r[]=build_req_for_project($_SESSION['project_data'], '98123591','req_get');
            $_SESSION['project_data'] = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $_SESSION['project_data']);
            $_SESSION['project_data'] = str_replace('&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;', '', $_SESSION['project_data']);
            $r='<request action="C4F6336B">'.implode('',$r).$_SESSION['project_data'].'</request>';
            // da($r);exit;
            $r_main=gib_serv($r, 'pr_get','Получение файлов',$line,$file,$function,$_SESSION);
            // da($r_main);exit;
            if (!isset($r_main['error']))
            {
                // exit;
                if ($xnc_in<>false)
                {
                    if($_GET['old_code']<>1)
                    {
                        $r=build_req_for_project($project_bhx, '298DF080');
                        $xml_get['mpr_bhx']=$r;
                        $get_mpr['bhx']=gib_serv($r, 'C4F6336B','Получение файлов MPR BHX',$line,$file,$function,$_SESSION);
                        if (isset($get_mpr['bhx']['error']))$er_log.='[ Проблема с запросом mpr_bhx на гиблаб - '.$get_mpr['bhx']['error'].' ]';
                        else
                        {
                            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_bhx_get', $_SESSION);
                            $r=build_req_for_project($project_bhx, '98123591');
                            $get_mpr_pdf['bhx']=gib_serv($r, '98123591_pdf','Получение файлов MPR PDF_BHX',$line,$file,$function,$_SESSION);
                            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_bhx_get_pdf', $_SESSION);
                        }
                        
                        
            
                        $r=build_req_for_project($project_venture, '298DF080');
                        $xml_get['mpr_vent']=$r;
                        $get_mpr['venture']=gib_serv($r, 'C4F6336B','Получение файлов MPR VENTURE',$line,$file,$function,$_SESSION);
                        if (isset($get_mpr['venture']['error']))$er_log.='[ Проблема с запросом mpr_venture на гиблаб - '.$get_mpr['venture']['error'].' ]';
                        else
                        {
                            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_venture_get', $_SESSION);
            
                            $r=build_req_for_project($project_venture, '98123591');
                            $get_mpr_pdf['venture']=gib_serv($r, '98123591_pdf','Получение файлов MPR PDF_VENT',$line,$file,$function,$_SESSION);
                            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_vent_get_pdf', $_SESSION);
                        }
                        
            
                    }
                    else
                    {
                        $project_xnc=vals_index_to_project($vals_without_gr);
                        $r=build_req_for_project($project_xnc, '298DF080','manual');
                        $get_mpr['manual']=gib_serv($r, 'C4F6336B','Получение файлов MPR Manual',$line,$file,$function,$_SESSION);
                        if (isset($get_mpr['manual']['error'])) $er_log.='[ Проблема с запросом mpr_manual на гиблаб - '.$get_mpr['manual']['error'].' ]';
                        else
                        {
                            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_venture_get', $_SESSION);
                            $xml_get['mpr_manual']=$r;
                            $r=build_req_for_project($project_xnc, '98123591');
                            $get_mpr_pdf['manual']=gib_serv($r, '98123591_pdf','Получение файлов MPR PDF_manual',$line,$file,$function,$_SESSION);
                            $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_manual_get_pdf', $_SESSION);
                        }
                    }
                }
                
              
                
                $month_catalog = date('m').date('y');
                $sql = 'SELECT * FROM `PLACES` WHERE `PLACES_ID` ='.$place;
                $place_name=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        
                $place_name=$place_name['hdd'];
                ///////// Временная заглушка пока есть проблемы с акцентом /////////////
                // $new_order_id = $order1c;
                ///echo 'Ид заказа с Акцента :'.$new_order_id.'<br><br><br>';
                ///////////////////////////////////////////////////////////////////////
                $structure_del = $serv_main_dir.'files/programm/'.$place_name.'/'.$month_catalog.'/'.$new_order_id.'/progr/';
                $structure = $serv_main_dir.'files/programm/'.$place_name.'/'.$month_catalog.'/'.$new_order_id.'/';
                // da($structure);
                // chmod($structure_del,0777);
                
                if(is_dir($structure_del)) 
                {
                    // // exit('11');
                    // if ($GLOBALS['is_devserver']==true ) {
                    //     chown($structure_del, 'i.kotok:apache');
                    // }
                    // else
                    // {
                    //     chown($structure_del, 'gibservice:gibservice');
    
                    // }
                    
                    $log.=('<br>Удаляем папку: '.$structure_del.'<br><br>');
                    $dir = $structure_del;
                    $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
                    $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
                    foreach ( $ri as $file ) 
                    {
                        $file->isDir() ?  rmdir($file) : unlink($file);
                    }
                    // rmdir($structure_del);
                    // unlink($structure_del);
                    // da('asdasdas');
                }
                // exit;
                if(!is_dir($structure))
                if (mkdir($structure, 0777, true)) $log.= 'folder maked '.$structure;
                else  $er_log.= 'no folder maked';
    
                // da($structure.' - - !');
                if ($GLOBALS['is_devserver']==true ) {
                    chown($structure, 'apache:apache');
                }
                else
                {
                    chown($structure, 'gibservice:gibservice');
                }
                
                // exit;
                
                $zip_file=$structure.pathinfo($r_main)['filename'].'.zip';
                // da($r_main);
                $log.= '<br> основной zip: '.$zip_file;
                $log.=print_r($zip_file,true);
                copy($r_main,$zip_file);
                $zip = new ZipArchive;
                // da($get_mpr);exit;
                if ($zip->open($zip_file) === TRUE) 
                {
                    foreach ($pr_send as $k=>$v)
                    {
                        $zip->addFromString('project/'.$k.'.xml', $v);
                    }
                    if ($xnc_in<>false)
                    foreach ($get_mpr as $k=>$v)
                    {
                        foreach ($v as $k1=>$v1)
                        {
                            // da('mpr/'.$k.'/'.$k1);
                            $zip->addFromString('mpr/'.$k.'/'.$k1, $v1);
                        }
                        // $zip->addFile('/path/to/index.txt', 'newname.txt');
                    }
                    if ($xnc_in<>false)
                    foreach ($xml_get as $k=>$v)
                    {
                        $zip->addFromString('mpr_xml_get/'.$k.'.xml', $v);
                    }
                    if ($xnc_in<>false)
                    foreach ($get_mpr_pdf as $k=>$v)
                    {
                        $zip->addFile($v, 'pdf_xnc/'.$k.'.pdf');
                        
                    }
                    if (is_array($mpr_copy))
                    foreach ($mpr_copy as $k=>$v)
                    {
                        $t=file_get_contents($v);
                        // da($t);exit;
                        $zip->addFromString('mpr/venture/mpr_'.$k.'.mpr', $t);
                        $zip->addFromString('mpr/bhx/mpr_'.$k.'.mpr', $t);
                        unset($t);
                        // $zip->addFile($v, 'mpr/venture/'.$k.'.mpr');
                        // $zip->addFile($v, 'mpr/bhx/'.$k.'.mpr');
        
                    }
                    
                }
                else
                {
                    $er_log.= 'ошибка добавления проектов в zip';
                }
                $zip->close();
    
                
                if(is_dir($serv_main_dir.'files/accent/'.$sql2['DB_AC_ID'])) 
                {
                    $log.= ('<br>Удаляем папку: '.$serv_main_dir.'files/accent/'.$sql2['DB_AC_ID'].'<br><br>');
                    $dir = $serv_main_dir.'files/accent/'.$sql2['DB_AC_ID'];
                    $di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
                    $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
                    foreach ( $ri as $file ) 
                    {
                        $file->isDir() ?  rmdir($file) : unlink($file);
                    }
                }
                if (!is_dir($serv_main_dir.'files/accent/'.$sql2['DB_AC_ID']))
                if (mkdir($serv_main_dir.'files/accent/'.$sql2['DB_AC_ID'], 0777, true)) $log.= 'folder progr maked '.$serv_main_dir.'files/accent/'.$sql2['DB_AC_ID'];
                else $er_log.= 'no folder progr maked '.$serv_main_dir.'files/accent/'.$sql2['DB_AC_ID'];
                // exit('11');
                $zip1 = new ZipArchive;
                $res = $zip1->open($zip_file);
                if ($res === TRUE) {
                  $zip1->extractTo($structure_del);
                  $zip1->extractTo($serv_main_dir.'files/accent/'.$sql2['DB_AC_ID']);
                  $zip1->close();
                //   echo '<h1>папки разархивированы</h1>';
                } else {
                  $er_log.= '<h1>ошибка разархивирования папок</h1>';
                }
                
        
                $slq_get_tool_equip = 'SELECT * FROM `TOOL_CUTTING_EQUIPMENT` WHERE `place` = '.$place;
                $tools_equip = sql_data(__LINE__,__FILE__,__FUNCTION__,$slq_get_tool_equip)['data'];
                foreach ($tools_equip as $t)
                {
                    if ($t['lc4']==1)
                    {
                        // copy()
                        $d = dir($structure_del);
                        while (FALSE !== ($entry = $d->read())) 
                        {
                            if ($entry == '.' || $entry == '..') continue;
                            if (mb_strtoupper(pathinfo($entry)['extension']=='LC4'))
                            {
                                copy($structure_del.$entry, $serv_main_dir.$t['local_folder'].$entry);
                                $log.= '<br>Копирование пильных программ: ';
                                $log.=($serv_main_dir.$t['local_folder'].$entry);
                            }
                        }
                        $d->close();
                    }
                    elseif ($t['gibcut']==1)
                    {
                        // copy()
                        $d = dir($structure_del);
                        while (FALSE !== ($entry = $d->read())) 
                        {
                            if ($entry == '.' || $entry == '..') continue;
                            if (mb_strtolower(pathinfo($entry)['extension'])=='gibcut')
                            copy($structure_del.$entry, $serv_main_dir.$t['local_folder'].$entry);
                            $log.= '<br>Копирование пильных программ: ';
                            $log.=print_r($serv_main_dir.$t['local_folder'].$entry,true);
                        }
                        $d->close();
                    }
                    elseif ($t['ptx']==1)
                    {
                        // copy()
                        $d = dir($structure_del);
                        while (FALSE !== ($entry = $d->read())) 
                        {
                            if ($entry == '.' || $entry == '..') continue;
                            if (mb_strtolower(pathinfo($entry)['extension'])=='ptx')
                            copy($structure_del.$entry, $serv_main_dir.$t['local_folder'].$entry);
                            $log.= '<br>Копирование пильных программ: ';
                            $log.=print_r($serv_main_dir.$t['local_folder'].$entry,true);
                        }
                        $d->close();
                    }
                }
                $slq_get_tool_equip_xnc = 'SELECT * FROM `tool_xnc_equipment` WHERE `place` = '.$place;
                $tools_equip_xnc = sql_data(__LINE__,__FILE__,__FUNCTION__,$slq_get_tool_equip_xnc)['data'];
                $actual_year = date('Y');
                $actual_month_number = date('m');
                $actual_month1_string = get_month_name($actual_month_number);
                // da($log);    
                // exit;
                if ($xnc_in_copy==true) 
                {
                    $log.= 'Программы XNC есть в заказе';
                    foreach ($tools_equip_xnc as $t)
                    {
                        //.. 26.04.2023 Тоценко
                        // Здесь добавлено условие для филиалов Одессы
                        // Для них программы для станков идут в папку с названием идентификатора заказа
                        // (для всех остальных по умолчанию, в папку с названием номера заказа)
                        if ($place == 3 || $place == 5) {
                            $dir2=$serv_main_dir.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_ID']).'/';
                        } else {
                            $dir2=$serv_main_dir.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/';
                        }
                        
                        $ar=['bhx/','venture/'];
                        foreach ($ar as $a)
                        {
                            $log.=print_r($dir2.$a,true);
                            if(is_dir($dir2.$a)) 
                            {
                                $log.= ('<br>Удаляем папку: '.$dir2.$a.'<br><br>');
                                $dir3 = $dir2.$a;
                                $di = new RecursiveDirectoryIterator($dir3, FilesystemIterator::SKIP_DOTS);
                                $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
                                foreach ( $ri as $file3 ) 
                                {
                                    $file3->isDir() ?  rmdir($file3) : unlink($file3);
                                }
                            }
                            if (mkdir($dir2.$a, 0777, true))
                            {
                                $log.= '<br>Создание папки ЧПУ: ';
                                $log.=print_r($dir2.$a,true);
                            }
                            else
                            {
                                $log.= '<br>Не удалось создание папки ЧПУ: ';
                                $log.=print_r($dir2.$a,true);
                            }
                            if ($GLOBALS['is_devserver']==true ) {
                                chown($dir2.$a, 'apache:apache');
                            }
                            else
                            {
                                chown($dir2.$a, 'gibservice:gibservice');
                            }
                            //.. 04.01.2022 Здесь бывала ошибка, не было папки, поэтому предварительно создаём её
                            $d = $structure_del.'mpr/'.$a;
                            if (!file_exists($d)) {
                                if (!mkdir($d, 0777, true)) $er_log .= 'Не удалось создать директорию: ' . $d;
                            }
                            $d = dir($structure_del.'mpr/'.$a.'/');
                            while (FALSE !== ($entry = $d->read())) 
                            {
                                if ($entry == '.' || $entry == '..') continue;
                                copy($structure_del.'mpr/'.$a.$entry, $dir2.$a.$entry);
                                $log.= '<br>копирование файлов ЧПУ: ';
                                $log.=($structure_del.'mpr/'.$a.$entry);
                                $log.=($dir2.$a.$entry);
                            }
                            $d->close();
                        }
                    }
                }
            }
            else $er_log.='[ Проблема с основным запросом на гиблаб - '.$r_main['error'].' ]';
            if (isset($er_log))
            {
                $sql = 'SELECT * FROM `order1c_files_generate` WHERE `order` = '.$sql2['ID'];
                $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                if (count($res['data'])>0)
                {
                    $sql = 'UPDATE `order1c_files_generate` SET `log`="'. base64_encode((' - ['.date('Y-m-d H:i:s').' // '.$er_log.'] - ')
                    .base64_decode($res['data'][0]['log'])).'", `date`="'.date('Y-m-d H:i:s').'"        
                    WHERE `order` = '.$sql2['ID'];
                    $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                }
                else
                {
                    $sql = 'INSERT INTO `order1c_files_generate` (`order`,`log`,`date`) VALUES ('.$order1c_log.',"'.base64_encode(' - ['.date('Y-m-d H:i:s').' // '.$er_log.'] - ').'", "'.date('Y-m-d H:i:s').'");';
                    $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                }
            }
            else
            {
                $sql = 'SELECT * FROM `order1c_files_generate` WHERE `order` = '.$sql2['ID'];
                $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                if (count($res['data'])>0)
                {
                    $sql = 'UPDATE `order1c_files_generate` SET `log`="'. base64_encode((' - ['.date('Y-m-d H:i:s').' OK ] -'.$log.'   <hr>  ')
                    .base64_decode($res['data'][0]['log'])).'", `date`="'.date('Y-m-d H:i:s').'"        
                    WHERE `order` = '.$sql2['ID'];
                    $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                }
                else
                {
                    $sql = 'INSERT INTO `order1c_files_generate` (`order`,`log`,`date`) VALUES ('.$order1c_log.',"'.base64_encode(' ['.date('Y-m-d H:i:s').' OK ] - '.$log.'   <hr>  ').'", "'.date('Y-m-d H:i:s').'");';
                    $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                }
            }
        }


        
    }

    if ($xnc_in<>false) $log.= '<<XNC есть в заказе.>>';
    if (!isset($er_log))
    {
        
        return ['log'=>$log,'res'=>true];
    }
    else
    {
        //my_mail('При генерации программ ошибка: '.$order_data['DB_AC_NUM'].__LINE__.' / '.__FILE__.' / '.__FUNCTION__, 'Ошибка при генерации программ по заказу "'.$order_data['DB_AC_NUM'].'". ', $er_log, 'html');
        my_mail('Ошибка при генерации программ по заказу "'.$new_order_id.'" ['.$sql2['DB_AC_NUM'].']', '[LINE:'. __LINE__ .' / FILE:'. __FILE__ .' / FUNC:'. __FUNCTION__ .'<br /><br />' . 'DEBUG:<br />' . $log, $er_log, 'html');
        return $er_log;
    }

    // old from here

    //     $r=build_req_for_project($_SESSION['project_data'], '235F63BD');
    //     $xml_get['ptx']=$r;
    //     $get_ptx=gib_serv($r, '235F63BD','Получение файлов PTX',$line,$file,$function,$_SESSION);
    //     $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_ptx_get', $_SESSION);
    //     $r=build_req_for_project($_SESSION['project_data'], 'F6F4A9CC');
    //     $xml_get['lc4']=$r;
    //     $get_cadmatic=gib_serv($r, 'F6F4A9CC','Получение файлов LC4',$line,$file,$function,$_SESSION);
    //     $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_lc4_get', $_SESSION);
    //     $r=build_req_for_project($_SESSION['project_data'], 'DAD1ABF3');
    //     $xml_get['gibsaw']=$r;
    //     $get_gibsaw=gib_serv($r, 'DAD1ABF3','Получение файлов GIBSAW',$line,$file,$function,$_SESSION);
    //     $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_gibsaw_get', $_SESSION);
    //     if($_GET['old_code']<>1)
    //     {
    //         $r=build_req_for_project($project_bhx, '298DF080');
    //         $xml_get['mpr_bhx']=$r;

    //         $get_mpr_bhx=gib_serv($r, 'C4F6336B','Получение файлов MPR BHX',$line,$file,$function,$_SESSION);
    //         $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_bhx_get', $_SESSION);
    //         $r=build_req_for_project($project_venture, '298DF080');
    //         $xml_get['mpr_vent']=$r;

    //         $get_mpr_venture=gib_serv($r, 'C4F6336B','Получение файлов MPR VENTURE',$line,$file,$function,$_SESSION);
    //         $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_mpr_venture_get', $_SESSION);
    //     }
    //     else
    //     {
    //         $project_xnc=vals_index_to_project($vals_without_gr);
    //         $r=build_req_for_project($project_xnc, '298DF080','manual');
    //         $get_mpr_manual=gib_serv($r, 'C4F6336B','Получение файлов MPR Manual',$line,$file,$function,$_SESSION);
            
    //         $xml_get['mpr_manual']=$r;
    //     }
    //     if($_GET['old_code']<>1)
    //     {
    //         $project_files = [
    //         // 'mpr' => $get_mpr,
    //         'mpr_bhx' => $get_mpr_bhx,
    //         'mpr_venture' => $get_mpr_venture,
    //         'cadmatic' => $get_cadmatic,
    //         'ptx' => $get_ptx,
    //         'saw' => $get_gibsaw
    //         ];
    //     }
    //     else
    //     {
    //         $project_files = [
    //             // 'mpr' => $get_mpr,
    //             'mpr_manual' => $get_mpr_manual,
    //             'cadmatic' => $get_cadmatic,
    //             'ptx' => $get_ptx,
    //             'saw' => $get_gibsaw
    //             ];
    //     }


    //     $month_catalog = date('m').date('y');
    //     $sql = 'SELECT * FROM `PLACES` WHERE `PLACES_ID` ='.$place;
    //     $place_name=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

    //     $place_name=$place_name['hdd'];
    //     ///////// Временная заглушка пока есть проблемы с акцентом /////////////
    //     // $new_order_id = $order1c;
    //     ///echo 'Ид заказа с Акцента :'.$new_order_id.'<br><br><br>';
    //     ///////////////////////////////////////////////////////////////////////
    //     $_SESSION['zip_catalog'] = $place_name.'/'.$month_catalog.'/'.$new_order_id;
    //     $structure = $serv_main_dir.'files/programm/'.$place_name.'/'.$month_catalog.'/'.$new_order_id;

    //     $sql_structure_update = 'UPDATE `ORDER1C` SET `programm` = "'.$structure.'" WHERE `DB_AC_ID` = '.$new_order_id;
    //     if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_structure_update)['res']==1) echo '<br>Структура файлов записана в БД.<br>';
        
    //     /// echo $sql_structure_update;
    //     // test('oid',$new_order_id);
    //     $_SESSION['structure'] = $structure;
    //     $structure_mpr = $structure.'/mpr';
    //     if($_GET['old_code']<>1)
    //     {
    //         $structure_mpr_bhx = $structure.'/mpr_bhx/';
    //         $structure_mpr_venture = $structure.'/mpr_venture/';
    //     }
    //     else
    //     {
    //         $structure_mpr_manual = $structure.'/mpr_manual/';
    //     }
    //     $structure_ptx = $structure.'/ptx';
    //     $structure_cadmatic = $structure.'/cadmatic';
    //     $structure_saw = $structure.'/gibsaw';
    //     if($_GET['old_code']<>1)
    //     {
    //         $_SESSION['all_programms'] = [
    //             // 'mpr' => $structure_mpr,
    //             'mpr_bhx' => $structure_mpr_bhx,
    //             'mpr_venture' => $structure_mpr_venture,
    //             'ptx' => $structure_ptx,
    //             'cadmatic' => $structure_cadmatic,
    //             'saw' => $structure_saw
    //         ];
    //     }
    //     else
    //     {
    //         $_SESSION['all_programms'] = [
    //             // 'mpr' => $structure_mpr,
    //             // 'mpr_bhx' => $structure_mpr_bhx,
    //             'mpr_manual' => $structure_mpr_manual,
    //             'ptx' => $structure_ptx,
    //             'cadmatic' => $structure_cadmatic,
    //             'saw' => $structure_saw
    //         ];
    //     }





    //     mkdir($structure, 0777, true);
    //     if ($GLOBALS['is_devserver']==true ) {
    //         chown($structure, 'i.kotok:apache');
    //     }
    //     else
    //     {
    //         chown($structure, 'gibservice:gibservice');
    //     }

    //     if(file_exists($structure)) {
    //         delDir($structure);
    //     }
 

    //     ///echo $new_order_id; 
    //     ///echo $new_order_id1; 
    //     ///
    //     if (!mkdir($structure, 0777, true)) {
    //         echo $structure;
    //         die('Не удалось создать директории Order');
    //     }
    //     if ($GLOBALS['is_devserver']==true ) {
    //         chown($structure, 'i.kotok:apache');
    //     }
    //     else
    //     {
    //         chown($structure, 'gibservice:gibservice');
    //     }
        
    //     // da($s);
    //     if(file_put_contents($structure.'/'.$project.'.kronas', base64_encode(strrev(serialize($s))))==false) echo "Запись файла не удалась";
    //     if(file_put_contents($structure.'/'.$project.'.project', $_SESSION['project_data'])==false) echo "Запись файла не удалась";
    //     file_put_contents($structure.'/ptx.xml', build_req_for_project($_SESSION['project_data'],'235F63BD'));
    //     file_put_contents($structure.'/cadmatic.xml', build_req_for_project($_SESSION['project_data'],'F6F4A9CC'));
    //     file_put_contents($structure.'/gibsaw.xml', build_req_for_project($_SESSION['project_data'],'F6F4A9CC'));
    //     if($_GET['old_code']<>1)
    //     {
    //         file_put_contents($structure.'/bhx.xml', build_req_for_project($project_bhx,'298DF080'));
    //         file_put_contents($structure.'/venture.xml', build_req_for_project($project_venture,'298DF080'));
    //     }
    //     else
    //     {
    //         file_put_contents($structure.'/xnc_manual.xml', build_req_for_project($project_xnc,'298DF080','manual'));
    //     }
    //     $r=build_req_for_project($pdf_get_project, '98123591');
    //     $pdf_file=gib_serv($r, '98123591','Получение файла pdf',$line,$file,$function,$_SESSION);
    //     unset ($del_order);
    //     // $create_pdf_file = send_post_data($pdf_get_project, '98123591');
    //     // $create_pdf_file = str_replace($serv_main_dir.'/kronasapp/storage/data/giblab/', $serv_main_dir.'/kronasapp/storage/app/data/giblab/', $create_pdf_file);
    //     foreach($pdf_file as $name=>$pdf) {
    //       $name=name_file($name);

    //       if ($name=='error')
    //       $log.=date('Y-m-d H:i:s').'
    //         <br>
    //         Проблема с генерацией - ошибка:'.$pdf.'
    //         <br>';
    //         if ($name=='error') $del_order=1;
    //       $r=file_put_contents($structure.'/'.$project.'_'.$name,$pdf);
    //       $log.=date('Y-m-d H:i:s').'
    //         <br>
    //         pdf_'.$r.'_'.$structure.'/'.$project.'_'.$name.'
    //         <br>';
    //     }
    //     $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_xml_put_to_file', $_SESSION);
    //     //..05.01.2021 Два файла отчёта по РС
        
    //     $f=$structure.'/'.$project.'_'.$name.'_RS.pdf';
    //     $r=file_put_contents($f, getRSPDF($sql2['ID']));
    //     if (!$r)
    //       $log.=date('Y-m-d H:i:s').'
    //         <br>
    //         Проблема с генерацией - ошибка записи файла:'.$f.'
    //         <br>';
    //     else  $log.=date('Y-m-d H:i:s').'
    //     <br>
    //     pdf 2_'.$r.'_'.$f.'
    //     <br>';
    //     $f=$structure.'/'.$project.'_'.$name.'_RS_for_production.pdf';
    //     $r=file_put_contents($f, getRSRfPPDF($sql2['ID']));
    //     if (!$r)
    //       $log.=date('Y-m-d H:i:s').'
    //         <br>
    //         Проблема с генерацией - ошибка записи файла:'.$f.'
    //         <br>';
    //     else  $log.=date('Y-m-d H:i:s').'
    //     <br>
    //     pdf 2_'.$r.'_'.$f.'
    //     <br>';



    //     //if (!mkdir($structure_mpr, 0777, true)) {
    //     //    die('Не удалось создать директории MPR');
    //     //}

    //     //foreach (json_decode($project_files['mpr'])->files as $file) {
    //     //    $file = str_replace($serv_main_dir.'/kronasapp/storage/data/giblab/', $serv_main_dir.'/kronasapp/storage/app/data/giblab/', $file);
    //     //    $new_file = str_replace($serv_main_dir.'/kronasapp/storage/app/data/giblab/', $structure.'/mpr/', $file);
    //     //    copy($file, $new_file);
    //     //}


    //     if ($new_order_id>0)
    //     {
    //         $sql="select * from ticket where order_id=".$sql2['ID'].";";
    //         // da($sql);
    //         $ticket=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['id'];
    //         // da($ticket);
    //         if ($ticket>0)
    //         {
    //             $sql="select * from ticket_construct_mpr where ticket_id=".$ticket.";";
    //             // da($sql);
    //             $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    //             // da($sql3);
    //         }
    //         else
    //         {
    //             echo 'MPR - файлов по заказу не найдено, так как отсутствует ticket (обращение).';
    //             unset ($sql3);
    //         }
    //     }
    //     if($_GET['old_code']<>1)
    //     {
    //         if (!mkdir($structure_mpr_bhx, 0777, true)) {
    //             die('Не удалось создать директории MPR_bhx');
    //         }
    //     }
    //     else
    //     {
    //         if (!mkdir($structure_mpr_manual, 0777, true)) {
    //             die('Не удалось создать директории MPR_bhx');
    //         }
    //     }

    //     $slq_get_tool_equip = 'SELECT * FROM `TOOL_CUTTING_EQUIPMENT` WHERE `place` = '.$place;
    //     $tools_equip = sql_data(__LINE__,__FILE__,__FUNCTION__,$slq_get_tool_equip)['data'];

    //     $slq_get_tool_equip_xnc = 'SELECT * FROM `tool_xnc_equipment` WHERE `place` = '.$place;
    //     $tools_equip_xnc = sql_data(__LINE__,__FILE__,__FUNCTION__,$slq_get_tool_equip_xnc)['data'];



    //     $_SESSION['delete_bhx_files'] = 1;

    //     if($_GET['old_code']<>1)
    //     {
    //         foreach ($project_files['mpr_bhx'] as $name=>$file) 
    //         {
    //             $name=name_file($name);
    //             // da($name);
    //             if ($name=='error')
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             Проблема с генерацией mpr_bhx - ошибка:'.$project_files['mpr_bhx']['error'].'
    //             <br>';
    //             if ($name=='error') $del_order=2;
                
    //             $name1 = $structure.'/mpr_bhx/'.$dir.'/'.$name;
    //             $r=file_put_contents($name1,$file);
                
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             mpr_bhx_'.$r.'_'.$name1.'
    //             <br>';
            
    //             foreach ($tools_equip_xnc as $t) {
    //                 if($t['sverlo_bhx']) {
    //                     $actual_year = date('Y');
    //                     $actual_month_number = date('m');
    //                     $actual_month1_string = get_month_name($actual_month_number);

    //                     // $equip_folder = $serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/';
    //                     // if(is_dir($serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/') && $_SESSION['delete_bhx_files']) {
    //                     //     delDir($serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/');
    //                     // }
    //                     // mkdir($equip_folder, 0777, true);
    //                     // $_SESSION['stanki_mpr_folder_bhx'] = $equip_folder;
    //                     // $equip_file = $equip_folder.str_replace(str2url($sql2['DB_AC_NUM']).'/', '', $name);
    //                     // // da($equip_file);
    //                     // $r=file_put_contents($equip_file,$file);
    //                     // $log.=date('Y-m-d H:i:s').'
    //                     // <br>
    //                     // mpr_bhx_stanki_folder_bhx'.$r.'_'.$equip_file.'
    //                     // <br>';

    //                     $equip_folder_new = $serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/';
    //                     if(is_dir($serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/') && $_SESSION['delete_bhx_files']) {
    //                         delDir($serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/');
    //                     }
    //                     mkdir($equip_folder_new, 0777, true);
    //                     $_SESSION['stanki_mpr_folder_bhx_new'] = $equip_folder_new;
    //                     $equip_file = $equip_folder_new.str_replace(str2url($sql2['DB_AC_NUM']).'/', '', $name);
    //                     // da($name);
    //                     $r=file_put_contents($equip_file,$file);
    //                     $log.=date('Y-m-d H:i:s').'
    //                     <br>
    //                     mpr_bhx_stanki_folder_bhx_new'.$r.'_'.$equip_file.'
    //                     <br>';
    //                 }
    //             }



    //             if($structure_mpr_bhx != '/var/www/public_html/files' && $structure_mpr_bhx != '/var/www/public_html/files/' && $structure_mpr_bhx != '/var/www/public_html/files/') {
    //                 file_put_contents($structure_mpr_bhx.$name, $file);
    //             }


    //             $_SESSION['delete_bhx_files'] = 0;
    //         }
    //         // da($sql3);
    //         foreach ($sql3 as $file_mpr)
    //         {
    //             if($file_mpr['by_size'] == 0) {

    //                 if(!$_SESSION['stanki_mpr_folder_bhx'] || !$_SESSION['stanki_mpr_folder_bhx_new']) {
    //                     foreach ($tools_equip_xnc as $t) {
    //                         if ($t['sverlo_bhx']) {
    //                             $actual_year = date('Y');
    //                             $actual_month_number = date('m');
    //                             $actual_month1_string = get_month_name($actual_month_number);

    //                             // $equip_folder = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($sql2['DB_AC_NUM']) . '/BHX/';
    //                             // $_SESSION['stanki_mpr_folder_bhx'] = $equip_folder;
    //                             $equip_folder_new = $serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/BHX/';
    //                             $_SESSION['stanki_mpr_folder_bhx_new'] = $equip_folder_new;
    //                             // echo $equip_folder_new.'<hr>';

    //                         }
    //                     }
    //                 }
    //                 // da($sql2);
    //                 $new_file = $structure . '/mpr_bhx/' . rus2translit($sql2['DB_AC_NUM']) . '/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 $old_file='http://service.kronas.com.ua/helpdesc/uploads/' . $file_mpr['ticket_id'] . '/mpr/' . $file_mpr['name'] . '.mpr';
    //                 $r=copy($old_file, $new_file);
    //                 $log.=date('Y-m-d H:i:s').'
    //                     <br>
    //                     mpr_bhx_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                        
    //                     '.$old_file.'<br>
    //                     to: 
    //                     '.$new_file.'
    //                     <br>';

    //                 // mkdir($_SESSION['stanki_mpr_folder_bhx'], 0777, true);
    //                 // $old_file='http://service.kronas.com.ua/helpdesc/uploads/' . $file_mpr['ticket_id'] . '/mpr/' . $file_mpr['name'] . '.mpr';
    //                 // $new_file=$_SESSION['stanki_mpr_folder_bhx'].'/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 // $r=copy($old_file, $new_file);
    //                 // $log.=date('Y-m-d H:i:s').'
    //                 // <br>
    //                 // mpr_bhx_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 // '.$old_file.'<br>
    //                 // to: 
    //                 // '.$new_file.'
    //                 // <br>';
    //                 // unset($_SESSION['stanki_mpr_folder_bhx']);

    //                 mkdir($_SESSION['stanki_mpr_folder_bhx_new'], 0777, true);
    //                 $old_file='http://service.kronas.com.ua/helpdesc/uploads/' . $file_mpr['ticket_id'] . '/mpr/' . $file_mpr['name'] . '.mpr';
    //                 $new_file=$_SESSION['stanki_mpr_folder_bhx_new'].'/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 $r=copy($old_file, $new_file);

    //                 if($structure_mpr_bhx != '/var/www/public_html/files' && $structure_mpr_bhx != '/var/www/public_html/files/' && $structure_mpr_bhx != '/var/www/public_html/files/') {
    //                     copy($old_file, $structure_mpr_bhx.'mpr_' . $file_mpr['mpr_id'] . '.mpr');
    //                 }

    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 mpr_bhx_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 '.$old_file.'<br>
    //                 to: 
    //                 '.$new_file.'
    //                 <br>';
    //                 unset($_SESSION['stanki_mpr_folder_bhx_new']);

    //             }
    //         }
    //         if (!mkdir($structure_mpr_venture, 0777, true)) {
    //             die('Не удалось создать директории MPR_venture');
    //         }
    //         $_SESSION['delete_venture_files'] = 1;
    //         foreach ($project_files['mpr_venture'] as $name=>$file) {
    //             $name=name_file($name);
                
    //             if ($name=='error')
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             Проблема с генерацией mpr_venture - ошибка:'.$project_files['mpr_venture']['error'].'
    //             <br>';
    //             if ($name=='error') $del_order=3;

    //             // $name = str_replace($serv_main_dir.'/kronasapp/storage/data/giblab/', $serv_main_dir.'/kronasapp/storage/app/data/giblab/', $name);
    //             $name1 = $structure.'/mpr_venture/'.$dir.'/'.$name;
    //             // $new_file = preg_replace('/---[0-9][0-9][0-9][0-9]---/','',$new_file);
    //             $r=file_put_contents($name1,$file);
                
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             venture_bhx_'.$r.'_'.$name1.'
    //             <br>';
    //             foreach ($tools_equip_xnc as $t) {
    //                 if($t['frezer_venture']) {
    //                     $actual_year = date('Y');
    //                     $actual_month_number = date('m');
    //                     $actual_month1_string = get_month_name($actual_month_number);

    //                     // $equip_folder = $serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/';
            
    //                     // if(is_dir($serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/') && $_SESSION['delete_venture_files']) {
    //                     //     delDir($serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/');
    //                     // }
    //                     // $_SESSION['stanki_mpr_folder_venture'] = $equip_folder;
    //                     // mkdir($equip_folder, 0777, true);
    //                     // $equip_file = $equip_folder.str_replace(str2url($sql2['DB_AC_NUM']).'/', '', $name);
    //                     // $r=file_put_contents($equip_file,$file);
    //                     // $log.=date('Y-m-d H:i:s').'
    //                     // <br>
    //                     // mpr_venture_stanki_folder_venture'.$r.'_'.$equip_file.'
    //                     // <br>';


    //                     $equip_folder_new = $serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/';
    //                     if(is_dir($serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/') && $_SESSION['delete_venture_files']) {
    //                         delDir($serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/');
    //                     }
    //                     $_SESSION['stanki_mpr_folder_venture_new'] = $equip_folder_new;
    //                     // da($equip_folder_new);
    //                     mkdir($equip_folder_new, 0777, true);
    //                     $equip_file = $equip_folder_new.str_replace(str2url($sql2['DB_AC_NUM']).'/', '', $name);
    //                     $r=file_put_contents($equip_file,$file);
    //                     $log.=date('Y-m-d H:i:s').'
    //                     <br>
    //                     mpr_venture_stanki_folder_venture_new'.$r.'_'.$equip_file.'
    //                     <br>';

    //                 }
    //             }

    //             if($structure_mpr_venture != '/var/www/public_html/files' && $structure_mpr_venture != '/var/www/public_html/files/' && $structure_mpr_venture != '/var/www/public_html/files/') {
    //                 file_put_contents($structure_mpr_venture.$name, $file);
    //             }

    //             $_SESSION['delete_venture_files'] = 0;
    //         }
    //         foreach ($sql3 as $file_mpr)
    //         {
    //             if($file_mpr['by_size'] == 1) {


    //                 if(!$_SESSION['stanki_mpr_folder_venture'] || !$_SESSION['stanki_mpr_folder_venture_new']) {
    //                     foreach ($tools_equip_xnc as $t) {
    //                         if ($t['frezer_venture']) {
    //                             $actual_year = date('Y');
    //                             $actual_month_number = date('m');
    //                             $actual_month1_string = get_month_name($actual_month_number);

    //                             // $equip_folder = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($sql2['DB_AC_NUM']) . '/Venture/';
    //                             // $_SESSION['stanki_mpr_folder_venture'] = $equip_folder;
    //                             $equip_folder_new = $serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/Venture/';
    //                             $_SESSION['stanki_mpr_folder_venture_new'] = $equip_folder_new;
    //                         }
    //                     }
    //                 }
                   
    //                 $new_file = $structure.'/mpr_venture/'.rus2translit($sql2['DB_AC_NUM']).'/'.'mpr_'.$file_mpr['mpr_id'].'.mpr';
    //                 $old_file='http://service.kronas.com.ua/helpdesc/uploads/'. $file_mpr['ticket_id'].'/mpr/'.$file_mpr['name'].'.mpr';
    //                 $r=copy($old_file, $new_file);
    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 mpr_venture_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 '.$old_file.'<br>
    //                 to: 
    //                 '.$new_file.'
    //                 <br>';

    //                 // mkdir($_SESSION['stanki_mpr_folder_venture'], 0777, true);
    //                 // $old_file='http://service.kronas.com.ua/helpdesc/uploads/'. $file_mpr['ticket_id'].'/mpr/'.$file_mpr['name'].'.mpr';
    //                 // $new_file= $_SESSION['stanki_mpr_folder_venture'].'/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 // $r=copy($old_file, $new_file);
    //                 // $log.=date('Y-m-d H:i:s').'
    //                 // <br>
    //                 // mpr_venture_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 // '.$old_file.'<br>
    //                 // to: 
    //                 // '.$new_file.'
    //                 // <br>';
    //                 // unset($_SESSION['stanki_mpr_folder_venture']);

    //                 mkdir($_SESSION['stanki_mpr_folder_venture_new'], 0777, true);
    //                 $old_file='http://service.kronas.com.ua/helpdesc/uploads/'. $file_mpr['ticket_id'].'/mpr/'.$file_mpr['name'].'.mpr';
    //                 $new_file=$_SESSION['stanki_mpr_folder_venture_new'].'/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 $r=copy($old_file, $new_file);

    //                 if($structure_mpr_venture != '/var/www/public_html/files' && $structure_mpr_venture != '/var/www/public_html/files/' && $structure_mpr_venture != '/var/www/public_html/files/') {
    //                     copy($old_file, $structure_mpr_venture.'mpr_' . $file_mpr['mpr_id'] . '.mpr');
    //                 }

    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 mpr_venture_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 '.$old_file.'<br>
    //                 to: 
    //                 '.$new_file.'
    //                 <br>';
    //                 unset($_SESSION['stanki_mpr_folder_venture_new']);


    //             }
    //         }
    //     }
    //     else
    //     {
    //         // exit('11');
    //         foreach ($project_files['mpr_manual'] as $name=>$file) {
    //             $name=name_file($name);
                
    //             if ($name=='error')
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             Проблема с генерацией mpr_manual - ошибка:'.$project_files['mpr_manual']['error'].'
    //             <br>';
    //             if ($name=='error') $del_order=3;

    //             // $name = str_replace($serv_main_dir.'/kronasapp/storage/data/giblab/', $serv_main_dir.'/kronasapp/storage/app/data/giblab/', $name);
    //             $name1 = $structure.'/mpr_manual/'.$dir.'/'.$name;
    //             // $new_file = preg_replace('/---[0-9][0-9][0-9][0-9]---/','',$new_file);
    //             $r=file_put_contents($name1,$file);
                
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             manual_mpr_'.$r.'_'.$name1.'
    //             <br>';
    //             foreach ($tools_equip_xnc as $t) {
    //                 if($t['frezer_venture']) {
    //                     $actual_year = date('Y');
    //                     $actual_month_number = date('m');
    //                     $actual_month1_string = get_month_name($actual_month_number);

    //                     // $equip_folder = $serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/';
            
    //                     // if(is_dir($serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/') && $_SESSION['delete_venture_files']) {
    //                     //     delDir($serv_main_dir.'/'.$t['net_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/');
    //                     // }
    //                     // $_SESSION['stanki_mpr_folder_venture'] = $equip_folder;
    //                     // mkdir($equip_folder, 0777, true);
    //                     // $equip_file = $equip_folder.str_replace(str2url($sql2['DB_AC_NUM']).'/', '', $name);
    //                     // $r=file_put_contents($equip_file,$file);
    //                     // $log.=date('Y-m-d H:i:s').'
    //                     // <br>
    //                     // mpr_manual'.$r.'_'.$equip_file.'
    //                     // <br>';


    //                     $equip_folder_new = $serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/';
    //                     if(is_dir($serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/') && $_SESSION['delete_venture_files']) {
    //                         delDir($serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/');
    //                     }
    //                     $_SESSION['stanki_mpr_folder_venture_new'] = $equip_folder_new;
    //                     mkdir($equip_folder_new, 0777, true);
    //                     $equip_file = $equip_folder_new.str_replace(str2url($sql2['DB_AC_NUM']).'/', '', $name);
    //                     $r=file_put_contents($equip_file,$file);
    //                     $log.=date('Y-m-d H:i:s').'
    //                     <br>
    //                     mpr_manual'.$r.'_'.$equip_file.'
    //                     <br>';

    //                 }
    //             }

    //             if($structure_mpr_manual != '/var/www/public_html/files' && $structure_mpr_manual != '/var/www/public_html/files/' && $structure_mpr_manual != '/var/www/public_html/files/') {
    //                 file_put_contents($structure_mpr_manual.$name, $file);
    //             }

    //             $_SESSION['delete_venture_files'] = 0;
    //         }
    //         foreach ($sql3 as $file_mpr)
    //         {
    //             if($file_mpr['by_size'] == 1) {


    //                 if(!$_SESSION['stanki_mpr_folder_venture'] || !$_SESSION['stanki_mpr_folder_venture_new']) {
    //                     foreach ($tools_equip_xnc as $t) {
    //                         if ($t['frezer_venture']) {
    //                             $actual_year = date('Y');
    //                             $actual_month_number = date('m');
    //                             $actual_month1_string = get_month_name($actual_month_number);

    //                             // $equip_folder = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($sql2['DB_AC_NUM']) . '/manual/';
    //                             // $_SESSION['stanki_mpr_folder_venture'] = $equip_folder;
    //                             $equip_folder_new = $serv_main_dir.'/'.$t['local_folder'].$actual_year.'/'.$actual_month_number.'_'.$actual_month1_string.'/'.str2url($sql2['DB_AC_NUM']).'/manual/';
    //                             $_SESSION['stanki_mpr_folder_venture_new'] = $equip_folder_new;
    //                         }
    //                     }
    //                 }

    //                 $new_file = $structure.'/mpr_manual/'.rus2translit($sql2['DB_AC_NUM']).'/'.'mpr_'.$file_mpr['mpr_id'].'.mpr';
    //                 $old_file='http://service.kronas.com.ua/helpdesc/uploads/'. $file_mpr['ticket_id'].'/mpr/'.$file_mpr['name'].'.mpr';
    //                 $r=copy($old_file, $new_file);
    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 mpr_manual_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 '.$old_file.'<br>
    //                 to: 
    //                 '.$new_file.'
    //                 <br>';

    //                 // mkdir($_SESSION['stanki_mpr_folder_venture'], 0777, true);
    //                 // $old_file='http://service.kronas.com.ua/helpdesc/uploads/'. $file_mpr['ticket_id'].'/mpr/'.$file_mpr['name'].'.mpr';
    //                 // $new_file= $_SESSION['stanki_mpr_folder_venture'].'/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 // $r=copy($old_file, $new_file);
    //                 // $log.=date('Y-m-d H:i:s').'
    //                 // <br>
    //                 // mpr_venture_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 // '.$old_file.'<br>
    //                 // to: 
    //                 // '.$new_file.'
    //                 // <br>';
    //                 // unset($_SESSION['stanki_mpr_folder_venture']);

    //                 mkdir($_SESSION['stanki_mpr_folder_venture_new'], 0777, true);
    //                 $old_file='http://service.kronas.com.ua/helpdesc/uploads/'. $file_mpr['ticket_id'].'/mpr/'.$file_mpr['name'].'.mpr';
    //                 $new_file=$_SESSION['stanki_mpr_folder_venture_new'].'/' . 'mpr_' . $file_mpr['mpr_id'] . '.mpr';
    //                 $r=copy($old_file, $new_file);

    //                 if($structure_mpr_venture != '/var/www/public_html/files' && $structure_mpr_venture != '/var/www/public_html/files/' && $structure_mpr_venture != '/var/www/public_html/files/') {
    //                     copy($old_file, $structure_mpr_venture.'mpr_' . $file_mpr['mpr_id'] . '.mpr');
    //                 }

    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 mpr_manual_copy_stanki_folder_bhx_new'.$r.'_from:<br>
                    
    //                 '.$old_file.'<br>
    //                 to: 
    //                 '.$new_file.'
    //                 <br>';
    //                 unset($_SESSION['stanki_mpr_folder_venture_new']);


    //             }
    //         }
    //     }

    //     if (!mkdir($structure_cadmatic, 0777, true)) {
    //         die('Не удалось создать директории CADMATIC');
    //     }
    //     $_SESSION['delete_cadmatic_files'] = 1;
    //     foreach ($project_files['cadmatic'] as $name=>$file) {
    //         // $file = str_replace($serv_main_dir.'/kronasapp/storage/data/giblab/', $serv_main_dir.'/kronasapp/storage/app/data/giblab/', $name);
    //         // $name=name_file($name);
            
    //         if ($name=='error')$log.=date('Y-m-d H:i:s').'
    //         <br>
    //         Проблема с генерацией cadmatic_LC4 - ошибка:'.$project_files['cadmatic']['error'].'
    //         <br>';
    //         if ($name=='error') $del_order=4;

    //         $name1 = $structure.'/cadmatic/'.$name;
    //         // $new_file = preg_replace('/---[0-9][0-9][0-9][0-9]---/','',$new_file);
    //         $r=file_put_contents($name1,$file);
            
    //         $log.=date('Y-m-d H:i:s').'
    //         <br>
    //         cadmatic_'.$r.'_'.$name1.'
    //         <br>';
    //         foreach ($tools_equip as $t) {
    //             if($t['net_folder'] && $t['lc4'] == 1) {
    //                 if($_SESSION['delete_cadmatic_files']) {
    //                     del_filter_files($serv_main_dir.'/'.$t['net_folder'], $new_order_id);
    //                 }
    //                 $equip_folder = $serv_main_dir.'/'.$t['net_folder'];
    //                 $equip_file = $equip_folder.$name;
    //                 $r=file_put_contents($equip_file,$file);
    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 cadmatic_stanki_net_folder'.$r.'_'.$equip_file.'
    //                 <br>';
    //             }
    //             if($t['local_folder'] && $t['lc4'] == 1) {
    //                 if($_SESSION['delete_cadmatic_files']) {
    //                     del_filter_files($serv_main_dir . '/' . $t['local_folder'], $new_order_id);
    //                 }
    //                 $equip_folder = $serv_main_dir.'/'.$t['local_folder'];
    //                 $equip_file = $equip_folder.$name;
    //                 $r=file_put_contents($equip_file,$file);
    //                 $log.=date('Y-m-d H:i:s').'
    //                 <br>
    //                 cadmatic_stanki_local_folder'.$r.'_'.$equip_file.'
    //                 <br>';
    //             }
    //         }
    //         $_SESSION['delete_cadmatic_files'] = 0;
    //     }





    //         if (!mkdir($structure_saw, 0777, true)) {
    //             die('Не удалось создать директории GIBSAW');
    //         }
    //         $_SESSION['delete_saw_files'] = 1;
    //         foreach ($project_files['saw'] as $name=>$file) {
    //             // $name=name_file($name);

    //             // $name = str_replace($serv_main_dir.'/kronasapp/storage/data/giblab/', $serv_main_dir.'/kronasapp/storage/app/data/giblab/', $file);
    //             if ($name=='error')
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             Проблема с генерацией gibsaw - ошибка:'.$project_files['saw']['error'].'
    //             <br>';
    //             if ($name=='error') $del_order=5;

    //             // da($name);
    //             $name1 = $structure.'/gibsaw/'.$name;
    //             // $new_file = preg_replace('/---[0-9][0-9][0-9][0-9]---/','',$new_file);
    //             $r=file_put_contents($name1,$file);
    //             $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             saw_'.$r.'_'.$name1.'
    //             <br>';
               
    //             foreach ($tools_equip as $t) {

    //                 if($t['net_folder'] && $t['gibcut'] == 1) {
    //                     if($_SESSION['delete_saw_files']) {
    //                         del_filter_files($serv_main_dir.'/'.$t['net_folder'], $new_order_id);
    //                     }
    //                     $equip_folder = $serv_main_dir.'/'.$t['net_folder'];
    //                     $equip_file = $equip_folder.$name;
    //                     $r=file_put_contents($equip_file,$file);
    //                     $log.=date('Y-m-d H:i:s').'
    //                     <br>
    //                     saw_stanki_net_folder'.$r.'_'.$equip_file.'
    //                     <br>';
    //                 }
    //                 if($t['local_folder'] && $t['gibcut'] == 1) {
    //                     if($_SESSION['delete_saw_files']) {
    //                         del_filter_files($serv_main_dir.'/'.$t['local_folder'], $new_order_id);
    //                     }
    //                     $equip_folder = $serv_main_dir.'/'.$t['local_folder'];
    //                     $equip_file = $equip_folder.$name;
    //                     $r=file_put_contents($equip_file,$file);
    //                     $log.=date('Y-m-d H:i:s').'
    //                     <br>
    //                     saw_stanki_local_folder'.$r.'_'.$equip_file.'
    //                     <br>';
    //                 }
    //             }
    //             $_SESSION['delete_saw_files'] = 0;
    //         }







    //     if (!mkdir($structure_ptx, 0777, true)) {
    //         die('Не удалось создать директории PTX');
    //     }
    //     foreach ($project_files['ptx'] as $name=>$file) {
    //         // $name=name_file($name);
    //         // echo $name;
    //         if ($name=='error')$log.=date('Y-m-d H:i:s').'
    //         <br>
    //         Проблема с генерацией cadmatic_LC4 - ошибка:'.$project_files['ptx']['error'].'
    //         <br>';
    //         if ($name=='error') $del_order=6;

    //         $name = $structure.'/ptx/'.$name;
    //         $r=file_put_contents($name,$file);
            
    //         $log.=date('Y-m-d H:i:s').'
    //             <br>
    //             ptx_'.$r.'_'.$name1.'
    //             <br>';

    //     }
    //     if(is_dir($GLOBALS['serv_main_dir'].'/files/accent/'.$new_order_id)) {
    //         delDir($GLOBALS['serv_main_dir'].'/files/accent/'.$new_order_id);
    //     }
    //     my_copy_all($structure, $GLOBALS['serv_main_dir'].'/files/accent/'.$new_order_id);
    //     // echo '/var/www/public_html/files/accent/'.$new_order_id.'/log.html';  
    //     file_put_contents($GLOBALS['serv_main_dir'].'/files/accent/'.$new_order_id.'/log.html',$log);
    //     echo '<hr>'.$log.'<hr>';

    //     $sql = 'SELECT * FROM `order1c_files_generate` WHERE `order` = '.$order1c_log;
    //     $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    //     if (count($res['data'])>0)
    //     {
    //         $sql = 'UPDATE `order1c_files_generate` SET `log`="'. base64_encode($log.'
    //         <br><hr><br>
    //         ').$res['data'][0]['log'].'", `date`="'.date('Y-m-d H:i:s').'"        
    //         WHERE `order` = '.$order1c_log;
    //         $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    //     }
    //     else
    //     {
    //         $sql = 'INSERT INTO `order1c_files_generate` (`order`,`log`,`date`) VALUES ('.$order1c_log.',"'. base64_encode($log).'", "'.date('Y-m-d H:i:s').'");';
    //         $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    //     }
    //     // echo $sql;

    // }
    // else
    // {
    //     echo 'На черновик программы не делаются!';
    //     exit;
    // }
    // if ($del_order>0)
    // {
    //     if (($del_order<>3)&&($del_order<>2)) delete_order_programm($sql2['ID']);
    //     my_mail('При генерации программ ошибка: '.$sql2['DB_AC_NUM'].__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'Ошибка при генерации программ '.$sql2['DB_AC_NUM'].' код ошибки del_order='.$del_order.'<hr>'.$log,$xml_get,'xml');
    //     my_mail('Project ошибка: '.$sql2['DB_AC_NUM'].__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'Ошибка при генерации программ '.$sql2['DB_AC_NUM'].' код ошибки del_order='.$del_order.'<hr>'.$log,$pr_send,'xml');
    // }
    // $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'put_progrgr_xml_put_program_file_finish', $_SESSION);
    


}

?>