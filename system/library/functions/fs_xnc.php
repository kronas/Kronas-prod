<?php

/**
 * 
 **/
function xnc_to_turn($x, $y, $op, $v1, $dx, $dy, $turn)
{
    if ($turn == 1) {
        $a = str_replace('dx', 'dy', $v1['attributes'][$x]);
        $b = str_replace('dy', 'dx', $v1['attributes'][$y]);
        if (($v1['tag'] == 'BF')
        	|| ($v1['tag'] == 'GR')
        	|| ($v1['tag'] == 'MS')
        	|| ($v1['tag'] == 'ML')
        	|| ($v1['tag'] == 'MA')
        	|| ($v1['tag'] == 'MAC')
        	|| ($v1['tag'] == 'MA3P')
        	|| ($v1['tag'] == 'ME')
        	|| ($v1['tag'] == 'MR'))
        {   
            $v1['attributes'][$x] = $b;
            $v1['attributes'][$y] = 'dy-(' . $a . ')';
            
            if ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'true')) {
                $v1['attributes']['AV'] = 'false';
            } elseif ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'false')) {
                $v1['attributes']['AV'] = 'true';
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            }

            if ($v1['tag'] == 'MR') {
                $l = $v1['attributes']['L'];
                $w = $v1['attributes']['W'];
                $v1['attributes']['L'] = $w;
                $v1['attributes']['W'] = $l;
            }
        } elseif ($v1['tag'] == 'BL') {

            $v1['tag'] = 'BB';
            $v1['attributes']['X'] = $b;
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag'] == 'BT') {

            $v1['tag'] = 'BL';
            $v1['attributes']['Y'] = 'dy-(' . $a . ')';
            unset($v1['attributes']['X']);

        } elseif ($v1['tag'] == 'BR') {

            $v1['tag'] = 'BT';
            $v1['attributes']['X'] = $b;
            unset($v1['attributes']['Y']);
        } elseif ($v1['tag'] == 'BB') {

            $v1['tag'] = 'BR';
            $v1['attributes']['Y'] = 'dy-(' . $a . ')';
            unset($v1['attributes']['X']);

        }
    }

    if ($turn == 3) {
        $a = str_replace('dx', 'dy', $v1['attributes'][$x]);
        $b = str_replace('dy', 'dx', $v1['attributes'][$y]);

        if (($v1['tag'] == 'BF')
        	|| ($v1['tag'] == 'GR')
        	|| ($v1['tag'] == 'MS')
        	|| ($v1['tag'] == 'ML')
        	|| ($v1['tag'] == 'MA')
        	|| ($v1['tag'] == 'MAC')
        	|| ($v1['tag'] == 'MA3P')
        	|| ($v1['tag'] == 'ME')
        	|| ($v1['tag'] == 'MR'))
        {
            $v1['attributes'][$x] = 'dx-(' . $b . ')';
            $v1['attributes'][$y] = $a;
            
            if ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'true')) {
                $v1['attributes']['AV'] = 'false';
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            } elseif ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'false')) {
                $v1['attributes']['AV'] = 'true';
            }

            if ($v1['tag'] == 'MR') {
                $l = $v1['attributes']['L'];
                $w = $v1['attributes']['W'];
                $v1['attributes']['L'] = $w;
                $v1['attributes']['W'] = $l;
            }
        } elseif ($v1['tag']=='BL') {

            $v1['tag'] = 'BT';
            $v1['attributes']['X'] = 'dx-(' . $b . ')';
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag']=='BT') {

            $v1['tag'] = 'BR';
            $v1['attributes']['Y'] = $a;
            unset($v1['attributes']['X']);

        } elseif ($v1['tag']=='BR') {

            $v1['tag'] = 'BB';
            $v1['attributes']['X'] = 'dx-(' . $b . ')';
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag']=='BB') {

            $v1['tag'] = 'BL';
            $v1['attributes']['Y'] = $a;
            unset($v1['attributes']['X']);

        }
    } elseif ($turn == 2) {
        $a=$v1['attributes'][$x];
        $b=$v1['attributes'][$y];
        if (($v1['tag'] == 'BF')
        	|| ($v1['tag'] == 'GR')
        	|| ($v1['tag'] == 'MS')
	        || ($v1['tag'] == 'ML')
	        || ($v1['tag'] == 'MA')
	        || ($v1['tag'] == 'MAC')
	        || ($v1['tag'] == 'MA3P')
	        || ($v1['tag'] == 'ME')
	        || ($v1['tag'] == 'MR'))
        {
            $v1['attributes'][$x] = 'dx-(' . $a . ')';
            $v1['attributes'][$y] = 'dy-(' . $b . ')';
            
            if ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'true')) {
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            } elseif ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'false')) {
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            }
        } elseif ($v1['tag']=='BL') {

            $v1['tag'] = 'BR';
            $v1['attributes']['Y'] = 'dy-(' . $b . ')';

        } elseif ($v1['tag']=='BR') {

            $v1['tag'] = 'BL';
            $v1['attributes']['Y'] = 'dy-(' . $b . ')';

        } elseif ($v1['tag']=='BB') {

            $v1['tag'] = 'BT';
            $v1['attributes']['X'] = 'dx-(' . $a . ')';

        } elseif ($v1['tag']=='BT') {

            $v1['tag'] = 'BB';
            $v1['attributes']['X'] = 'dx-(' . $a . ')';

        }
    }

    foreach ($v1['attributes'] as $k => $v) {
        if (substr_count($v,'()') > 0) {
            $v1['attributes'][$k] = str_replace('()', '(0)', $v1['attributes'][$k]);
        }
    }
    return $v1;
}

/**
 * 
 **/
function xnc_turn_change($x, $y, $op, $v1, $dx, $dy)
{
    if ($op['attributes']['TURN'] == 1) {
        $a = str_replace('dx', 'dy', $v1['attributes'][$x]);
        $b = str_replace('dy', 'dx', $v1['attributes'][$y]);

        if (($v1['tag'] == 'BF')
        	|| ($v1['tag'] == 'GR')
        	|| ($v1['tag'] == 'MS')
	        || ($v1['tag'] == 'ML')
	        || ($v1['tag'] == 'MA')
	        || ($v1['tag'] == 'MAC')
	        || ($v1['tag'] == 'MA3P')
	        || ($v1['tag'] == 'ME')
	        || ($v1['tag'] == 'MR'))
        {
            $v1['attributes'][$x] = 'dx-(' . $b . ')';
            $v1['attributes'][$y] = $a;
            
            if ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'true')) {
                $v1['attributes']['AV'] = 'false';
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            } elseif ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'false')) {
                $v1['attributes']['AV'] = 'true';
            }

            if ($v1['tag'] == 'MR') {
                $l = $v1['attributes']['L'];
                $w = $v1['attributes']['W'];
                $v1['attributes']['L'] = $w;
                $v1['attributes']['W'] = $l;
            }
        } elseif ($v1['tag'] == 'BL') {

            $v1['tag'] = 'BT';
            $v1['attributes']['X'] = 'dx-(' . $b . ')';
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag'] == 'BT') {

            $v1['tag'] = 'BR';
            $v1['attributes']['Y'] = $a;
            unset($v1['attributes']['X']);

        } elseif ($v1['tag'] == 'BR') {

            $v1['tag'] = 'BB';
            $v1['attributes']['X'] = 'dx-(' . $b . ')';
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag'] == 'BB') {

            $v1['tag'] = 'BL';
            $v1['attributes']['Y'] = $a;
            unset($v1['attributes']['X']);

        }
    } elseif ($op['attributes']['TURN'] == 3) {
        $a = str_replace('dx', 'dy', $v1['attributes'][$x]);
        $b = str_replace('dy', 'dx', $v1['attributes'][$y]);
        if (($v1['tag'] == 'BF')
        	|| ($v1['tag'] == 'GR')
        	|| ($v1['tag'] == 'MS')
	        || ($v1['tag'] == 'ML')
	        || ($v1['tag'] == 'MA')
	        || ($v1['tag'] == 'MAC')
	        || ($v1['tag'] == 'MA3P')
	        || ($v1['tag'] == 'ME')
	        || ($v1['tag'] == 'MR'))
        {
            $v1['attributes'][$x] = $b;
            $v1['attributes'][$y] = 'dy-(' . $a . ')';
            
            if ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'true')) {
                $v1['attributes']['AV'] = 'false';
            } elseif ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'false')) {
                $v1['attributes']['AV'] = 'true';
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            }

            if ($v1['tag'] == 'MR') {
                $l=$v1['attributes']['L'];
                $w=$v1['attributes']['W'];
                $v1['attributes']['L']=$w;
                $v1['attributes']['W']=$l;
            }
        } elseif ($v1['tag'] == 'BL') {

            $v1['tag'] = 'BB';
            $v1['attributes']['X'] = $b;
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag'] == 'BT') {

            $v1['tag'] = 'BL';
            $v1['attributes']['Y'] = 'dy-(' . $a . ')';
            unset($v1['attributes']['X']);

        } elseif ($v1['tag'] == 'BR') {

            $v1['tag'] = 'BT';
            $v1['attributes']['X'] = $b;
            unset($v1['attributes']['Y']);

        } elseif ($v1['tag'] == 'BB') {

            $v1['tag'] = 'BR';
            $v1['attributes']['Y'] = 'dy-(' . $a . ')';
            unset($v1['attributes']['X']);

        }
    } elseif ($op['attributes']['TURN'] == 2) {
        $a = $v1['attributes'][$x];
        $b = $v1['attributes'][$y];

        if (($v1['tag'] == 'BF')
        	|| ($v1['tag'] == 'GR')
        	|| ($v1['tag'] == 'MS')
	        || ($v1['tag'] == 'ML')
	        || ($v1['tag'] == 'MA')
	        || ($v1['tag'] == 'MAC')
	        || ($v1['tag'] == 'MA3P')
	        || ($v1['tag'] == 'ME')
	        || ($v1['tag'] == 'MR'))
        {
            $v1['attributes'][$x] = 'dx-(' . $a . ')';
            $v1['attributes'][$y] = 'dy-(' . $b . ')';
            
            if ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'true')) {
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';

            } elseif ((isset($v1['attributes']['AS'])) && ($v1['attributes']['AC'] > 1) && ($v1['attributes']['AV'] == 'false')) {
                $v1['attributes']['AS'] = '-1*(' . $v1['attributes']['AS'] . ')';
            }
        } elseif ($v1['tag'] == 'BL') {
            $v1['tag'] = 'BR';
            $v1['attributes']['Y'] = 'dy-(' . $b . ')';
        } elseif ($v1['tag'] == 'BR') {
            $v1['tag'] = 'BL';
            $v1['attributes']['Y'] = 'dy-(' . $b . ')';
        } elseif ($v1['tag'] == 'BB') {
            $v1['tag'] = 'BT';
            $v1['attributes']['X'] = 'dx-(' . $a . ')';
        } elseif ($v1['tag'] == 'BT') {
            $v1['tag'] = 'BB';
            $v1['attributes']['X'] = 'dx-(' . $a . ')';
        }
    }

    foreach ($v1['attributes'] as $k => $v) {
        if (substr_count($v,'()') > 0) {
            $v1['attributes'][$k] = str_replace('()', '(0)', $v1['attributes'][$k]);
        }
    }
    return $v1;
}

/**
 * Где-то в этой функции повороты деталей
 **/
function xnc_to_production($vals, $type)
{
    $data_b=['min'=>800,'l'=>[4.5,8],'r'=>[4.5,8],'t'=>[4.5],'b'=>[8],'min_det'=>200,'side_b'=>1];
    $data_v=['min'=>1200,'l'=>[4.5,8],'r'=>[4.5,8],'t'=>[4.5],'b'=>[8],'min_det'=>350];
    ksort($vals);
    $vals=vals_out($vals);

    $project_data=gl_recalc(__LINE__,__FILE__,__FUNCTION__,vals_index_to_project($vals));
    $vals=get_vals($project_data);
    //.. 24.05.2021 добавлена строка ниже
    $index=make_index($vals);
    
    // Здесь обработка только XNC-обработок
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            
            $v1=get_vals($vals[$i]["attributes"]["PROGRAM"]);
            foreach ($v1 as $iii=>$vvv)
            {
                // Если есть пазование (GR)
                if ($v1[$iii]['tag']=='GR') 
                {
                    if (is_numeric($v1[$iii]['attributes']['X1'])) $v1[$iii]['attributes']['X1']=round($v1[$iii]['attributes']['X1'],1);
                    if (is_numeric($v1[$iii]['attributes']['X2'])) $v1[$iii]['attributes']['X2']=round($v1[$iii]['attributes']['X2'],1);
                    if (is_numeric($v1[$iii]['attributes']['Y1'])) $v1[$iii]['attributes']['Y1']=round($v1[$iii]['attributes']['Y1'],1);
                    if (is_numeric($v1[$iii]['attributes']['Y2'])) $v1[$iii]['attributes']['Y2']=round($v1[$iii]['attributes']['Y2'],1);
                }
            }
            // Функция принимает vals-массив программы, а возвращает XML-программы
            $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
            
        }
    }
    
    $vals_start=$vals;
    $index=make_index($vals);

    $arr=['X','CX','X1','X2','BX'];
    $arr2=['Y','CY','Y1','Y2','BY'];
    $arr3=['BL','BR','BT','BB','PROGRAM','COM','TOOL'];
    
    // Здесь обработка только XNC-обработок и только для не развёрнутых деталей
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            if ($vals[$i]["attributes"]["TURN"]==0)
            {
                // Здесь переходим на следующий блок (внутри OPERATION находятся блоки PART).
                $j=$i+1;
                
                // Перебираем блоки в цикле while пока не попадём на PART
                while ($vals[$j]['tag']<>'PART') $j++;

                // Функция возвращает в массиве все связи по детали (материал, кромка, толщина среза края для кромки, операции, раскрой)
                $part = get_all_part($vals,$vals[$j]["attributes"]["ID"]);

                unset($need_change);

                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);

                $v1_start=$v1;
                if ($type=='bhx') $data=$data_b;
                elseif ($type=='venture') $data=$data_v;
                else
                {
                    if (!isset($i1['MS'])) $data=$data_b;
                    else $data=$data_v;
                }

                unset ($in_bore);
                $a_2=['f','l','t','r','b'];

                // Если есть сверление, $in_bore устанавливаем в 1
                foreach($a_2 as $a_1)
                {
                    if (isset($i1['B'.mb_strtoupper($a_1)])) {
                        $in_bore=1;
                        break;
                    }
                }

                // Блок обработки детали со сверлением
                if ($in_bore==1)
                {
                    // Если длина больше или равно ширине
                    if ($part['part']['attributes']['CL'] >= $part['part']['attributes']['CW']) {
                        $small_side=$part['part']['attributes']['CW'];
                        $big_side=$part['part']['attributes']['CL'];
                    } else {
                        $small_side=$part['part']['attributes']['CL'];
                        $big_side=$part['part']['attributes']['CW'];
                        //.. 01.09.2021 Удаляем условие и принудительно разворачиваем если CW больше CL
                        // if ($big_side > $data['min']) $need_change=1;
                        $need_change=1;
                    }

                    // Если длина детали меньше, чем минимальный установленный размер детали для обработки
                    if ($part['part']['attributes']['CL']<$data['min_det']) $need_change=2;
                    
                    // Здесь вычисляем, есть ли в операциях диаметры инструментов,
                    // которые выходят за рамки установленных по умолчанию,
                    // (для верхнего сверления только 4.5мм, для нижнего только 8мм)
                    // и если есть, то переменную $need_change устанавливаем в 3
                    foreach (SIDE as $s) {
                        // Если на детали есть сверление (фронтальное, слева, справа, сверху, снизу)
                        if (isset($i1['B'.mb_strtoupper($s)])) {
                            // Перебираем массив операции сверления
                            foreach ($i1['B'.mb_strtoupper($s)] as $i11)
                            {
                                unset($d);
                                // Перебираем массив инструмента сверления
                                foreach ($i1['TOOL'] as $i21)
                                {
                                    // Если имена операции сверления и инструмента сверления совпадают
                                    if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                                }

                                // Если на конкретной стороне есть недопустимый для станка диаметр
                                if (!in_array($d,$data[$s])) $need_change=3;
                            }
                        }
                    }

                    if ($need_change>0)
                    {
                        $turn=1;
                        // Если длина меньше ширины полотна станка, а ширина больше (или равна) минимально допустимого размера детали
                        if (($part['part']['attributes']['CL']<=$data['min'])&&($part['part']['attributes']['CW']>=$data['min_det']))
                        {
                            // $v1 это всё ещё сконвертированный массив программы
                            foreach ($v1 as $k=>$v)
                            {
                                $t=$v;
                                // $arr=['X','CX','X1','X2','BX'];
                                // $arr2=['Y','CY','Y1','Y2','BY'];
                                // $arr3=['BL','BR','BT','BB','PROGRAM','COM','TOOL'];
                                foreach($arr as $k1=>$a) 
                                {
                                    if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) {
                                        // Функция выполняет пересчёт программы при повороте детали
                                        $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                                    }
                                }
                                if ($v['tag']=='ERR') unset ($v1[$k]);
                            }

                            if (($turn==1)||($turn==3))
                            {
                                $r=$v1[0]['attributes']['DY'];
                                $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                                $v1[0]['attributes']['DX']=$r;
                            }
                            $vals[$i]["attributes"]["TURN"]=$turn;
                            // Функция "vals_index_to_project" принимает vals-массив программы,
                            // а возвращает XML-файл программы
                            $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                            $i1=make_index($v1);
                            unset($need_change);
                            foreach (SIDE as $s){
                                if (isset($i1['B'.mb_strtoupper($s)])) {
                                    // Перебираем массив операции сверления
                                    foreach ($i1['B'.mb_strtoupper($s)] as $i11)
                                    {
                                        unset($d);
                                        // Перебираем массив инструмента сверления
                                        foreach ($i1['TOOL'] as $i21)
                                        {
                                            // Если имена операции сверления и инструмента сверления совпадают
                                            if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                                        }
                                        // Если диаметр выходит за установленные рамки
                                        if (!in_array($d,$data[$s])) 
                                        {
                                            $need_change=3;
                                            $v1=$v1_start;
                                        }
                                    }
                                }
                            }
                        }

                        if ($need_change>0)
                        {
                            $turn=2;
                            if ((($part['part']['attributes']['CW']<=$data['min'])&&($part['part']['attributes']['CL']>=$data['min_det']))
                                && !($part['part']['attributes']['CW'] > $part['part']['attributes']['CL']))
                            {
                                foreach ($v1 as $k=>$v)
                                {
                                    $t=$v;
                                    foreach($arr as $k1=>$a) 
                                    {
                                        if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                                    }
                                    if ($v['tag']=='ERR') unset ($v1[$k]);
                                }
                                if (($turn==1)||($turn==3))
                                {
                                    $r=$v1[0]['attributes']['DY'];
                                    $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                                    $v1[0]['attributes']['DX']=$r;
                                }
                                $vals[$i]["attributes"]["TURN"]=$turn;
                                $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                                unset($need_change);
                                $i1=make_index($v1);
                                foreach (SIDE as $s)
                                if (isset($i1['B'.mb_strtoupper($s)]))
                                foreach ($i1['B'.mb_strtoupper($s)] as $i11)
                                {
                                    unset($d);
                                    foreach ($i1['TOOL'] as $i21)
                                    {
                                        if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                                    }
                                    if (!in_array($d,$data[$s])) 
                                    {
                                        $need_change=4;
                                        $v1=$v1_start;
                                    }
                                }
                            }
                            if ($need_change>0)
                            {
                                $turn=3;
                                
                                if (($part['part']['attributes']['CL']<=$data['min'])&&($part['part']['attributes']['CW']>=$data['min_det']))
                                {
                                    foreach ($v1 as $k=>$v)
                                    {
                                        $t=$v;
                                        foreach($arr as $k1=>$a) 
                                        {
                                            if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                                        }
                                        if ($v['tag']=='ERR') unset ($v1[$k]);
                                    }
                                    
                                    $r=$v1[0]['attributes']['DY'];
                                    $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                                    $v1[0]['attributes']['DX']=$r;
                                    $vals[$i]["attributes"]["TURN"]=$turn;
                                    $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                                    unset($need_change);
                                    $i1=make_index($v1);
                                    foreach (SIDE as $s)
                                    if (isset($i1['B'.mb_strtoupper($s)]))
                                    foreach ($i1['B'.mb_strtoupper($s)] as $i11)
                                    {
                                        unset($d);
                                        foreach ($i1['TOOL'] as $i21)
                                        {
                                            if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                                        }
                                        if (!in_array($d,$data[$s])) 
                                        {
                                            $need_change=5;
                                            $v1=$v1_start;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($need_change>0)
                    {

                        $vals[$i]=$vals_start[$i];
                        unset($need_change);
                        if ($part['part']['attributes']['CL']>=$part['part']['attributes']['CW'])
                        {
                            $small_side=$part['part']['attributes']['CW'];
                            $big_side=$part['part']['attributes']['CL'];
                        }
                        else
                        {
                            
                            $small_side=$part['part']['attributes']['CL'];
                            $big_side=$part['part']['attributes']['CW'];
                            //.. 01.09.2021 Удаляем условие и принудительно разворачиваем если CW больше CL
                            // if ($big_side>$data['min']) $need_change=1;
                            $need_change=1;
                            
                        }
                        if ($part['part']['attributes']['CL']<$data['min_det']) $need_change=2;
                        $turn=0;
                        if ($need_change>0)
                        {
                            $turn=1;

                            //.. 01.09.2021 Убрано дополнительное условие для поворота детали когда X меньше Y.
                            // Теперь поворачиваются все детали подряд у которых X меньше Y.
                            // if ($part['part']['attributes']['CL']<=$data['min'])
                            // {
                                foreach ($v1 as $k=>$v)
                                {
                                    $t=$v;
                                    foreach($arr as $k1=>$a) 
                                    {
                                        if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                                    }
                                    if ($v['tag']=='ERR') unset ($v1[$k]);
                                }
                                if (($turn==1)||($turn==3))
                                {
                                    $r=$v1[0]['attributes']['DY'];
                                    $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                                    $v1[0]['attributes']['DX']=$r;
                                }
                                $vals[$i]["attributes"]["TURN"]=$turn;
                                $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                                $i1=make_index($v1);
                            // }
                        }
                        // xml ($vals);
                        // вписываем разбитие программ через enable
                        unset($need_change);
                        $p = xml_parser_create();
                        xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                        xml_parser_free($p);
                        foreach (SIDE as $s)
                        if (isset($i1['B'.mb_strtoupper($s)]))
                        foreach ($i1['B'.mb_strtoupper($s)] as $i11)
                        {
                            unset($d);
                            foreach ($i1['TOOL'] as $i21)
                            {
                                if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                            }
                            if (!in_array($d,$data[$s]))
                            {
                                $v1[$i11]['attributes']['ENABLE']='false';
                                $need_change=7;
                            }
                            else  $v1[$i11]['attributes']['ENABLE']='true';
                        }
                        $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                        
                        if ($need_change>0)
                        {
                            $v2=$v1;
                            $p = xml_parser_create();
                            xml_parse_into_struct($p, $vals_start[$i]["attributes"]["PROGRAM"], $v1, $i1);
                            xml_parser_free($p);
                            if ($turn==0) $turn=2;
                            elseif ($turn==1) $turn=3;

                            foreach ($v1 as $k=>$v)
                            {
                                $t=$v;
                                foreach($arr as $k1=>$a) 
                                {
                                    if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                                }
                                if ($v['tag']=='ERR') unset ($v1[$k]);
                            }
                            if (($turn==1)||($turn==3))
                            {
                                $r=$v1[0]['attributes']['DY'];
                                $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                                $v1[0]['attributes']['DX']=$r;
                            }
                            $ar_new_in=$vals_start[$i];
                            $ar_new_in["attributes"]["TURN"]=$turn;
                            $i1=make_index($v1);
                            unset($need_change);
                            foreach (SIDE as $s)
                            if (isset($i1['B'.mb_strtoupper($s)]))
                            foreach ($i1['B'.mb_strtoupper($s)] as $i11)
                            {
                                unset($d);
                                foreach ($i1['TOOL'] as $i21)
                                {
                                    if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                                }
                                if ($v2[$i11]['attributes']['ENABLE']=='false')
                                {
                                    $v1[$i11]['attributes']['ENABLE']='true';
                                    $need_change=8;
                                    // exit('11');
                                } 
                                else $v1[$i11]['attributes']['ENABLE']='false';
                            }
                            foreach ($v1 as $nn=>$nnm)
                            {
                                if ((($nnm['tag']<>'BL')&&($nnm['tag']<>'BB')&&($nnm['tag']<>'BR')&&($nnm['tag']<>'BT')&&($nnm['tag']<>'TOOL')&&($nnm['tag']<>'PROGRAM'))||
                                ($v1[$nn]['attributes']['ENABLE']=='false'))
                                {
                                    unset($v1[$nn]);
                                }
                            }
                            $xnc_num++;
                            $ar_new_in["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                            $ar_new_in["attributes"]["CODE"]=$ar_new_in["attributes"]["CODE"].'_'.$xnc_num;
                            $ar_new_in["attributes"]["ID"]=new_id($vals)+124;
                            $vals=vals_into_end($vals,$ar_new_in);
                            $vals=vals_into_end($vals,$vals_start[$i+1]);
                            $vals=vals_into_end($vals,$vals_start[$i+2]);
                            // xml($vals);

                        }
                    }
                }
                // Блок обработки детали без сверления
                else
                {
                    if ($part['part']['attributes']['CW']>=$part['part']['attributes']['CL'])
                    {
                        $turn=1;
                        //.. 01.09.2021 Убрано дополнительное условие для поворота детали когда X меньше Y.
                        // Теперь поворачиваются все детали подряд у которых X меньше Y.
                        // if (($part['part']['attributes']['CL']<=$data['min'])&&($part['part']['attributes']['CW']>=$data['min_det']))
                        // {
                            foreach ($v1 as $k=>$v)
                            {
                                $t=$v;
                                foreach($arr as $k1=>$a) 
                                {
                                    if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                                }
                                if ($v['tag']=='ERR') unset ($v1[$k]);
                            }
                            if (($turn==1)||($turn==3))
                            {
                                $r=$v1[0]['attributes']['DY'];
                                $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                                $v1[0]['attributes']['DX']=$r;
                            }
                            $vals[$i]["attributes"]["TURN"]=$turn;
                            $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                        // }
                    }
                }
                
            }
        }
    }
    $project_data=gl_recalc(__LINE__,__FILE__,__FUNCTION__,vals_index_to_project($vals));
    $vals=get_vals($project_data);

    $vals_turn_0=xnc_right($vals);

    $index=make_index($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            $v_start=$vals[$i];
            unset($need_change,$nn);
            $p = xml_parser_create();
            xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
            xml_parser_free($p);
            foreach ($i1['BF'] as $i11)
            {
                unset($need_change);
                foreach ($i1['TOOL'] as $i21)
                {
                    if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                }
                if (!isset($i1['MS'])) $data=$data_b;
                else $data=$data_v;

                if (($data['side_l']==1)&&(!isset($data['side_r'])))
                {
                    if ($v1[$i11]['attributes']['X']<$d/2) $need_change=1;
                }
                if (($data['side_r']==1)&&(!isset($data['side_l'])))
                {
                    if ($v1[$i11]['attributes']['X']>$v1[0]['attributes']['DX']-$d/2) $need_change=2;
                }
                if (($data['side_b']==1)&&(!isset($data['side_t'])))
                {
                    if ($v1[$i11]['attributes']['Y']<$d/2) $need_change=3;

                }
                if (($data['side_t']==1)&&(!isset($data['side_b'])))
                {
                    if ($v1[$i11]['attributes']['Y']>$v1[0]['attributes']['DY']-$d/2) $need_change=4;
                }

                if ($need_change>0) 
                {
                    $v1[$i11]['attributes']['ENABLE']='false';
                    $nn=1;
                }
                else  $v1[$i11]['attributes']['ENABLE']='true';

                
            }
            $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
            if ($nn>0)
            {
                $v2=$v1;
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals_turn_0[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);
                $turn=$vals[$i]["attributes"]["TURN"];
                if ($turn==0) $turn=2;
                elseif ($turn==2) $turn=0;
                elseif ($turn==1) $turn=3;
                elseif ($turn==3) $turn=1;


                foreach ($v1 as $k=>$v)
                {
                    $t=$v;
                    foreach($arr as $k1=>$a) 
                    {
                        if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_to_turn($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY'],$turn);
                    }
                    if ($v['tag']=='ERR') unset ($v1[$k]);
                }
                $ar_new_in=$vals_turn_0[$i];
                $ar_new_in["attributes"]["TURN"]=$turn;
                $i1=make_index($v1);
                unset($need_change);
                foreach ($i1['BF'] as $i11)
                {
                    unset($d);
                    foreach ($i1['TOOL'] as $i21)
                    {
                        if ($v1[$i21]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $d=$v1[$i21]['attributes']['D'];
                    }
                    if ($v2[$i11]['attributes']['ENABLE']=='false')
                    {
                        $v1[$i11]['attributes']['ENABLE']='true';
                        // exit('11');
                    } 
                    else $v1[$i11]['attributes']['ENABLE']='false';
                }

                foreach ($v1 as $nn=>$nnm)
                {
                    if (($nnm['tag']=='BF')&&
                    ($v1[$nn]['attributes']['ENABLE']=='false'))
                    {
                        unset($v1[$nn]);
                    }
                }
                if (($turn==1)||($turn==3))
                {
                    $r=$v1[0]['attributes']['DY'];
                    $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                    $v1[0]['attributes']['DX']=$r;
                }
                $xnc_num++;
                $ar_new_in["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                $ar_new_in["attributes"]["CODE"]=$ar_new_in["attributes"]["CODE"].'_'.$xnc_num;
                $ar_new_in["attributes"]["ID"]=new_id($vals)+124;
                $vals=vals_into_end($vals,$ar_new_in);
                $vals=vals_into_end($vals,$vals_start[$i+1]);
                $vals=vals_into_end($vals,$vals_start[$i+2]);
                // xml($vals);

            }


        }
    }
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            $v_start=$vals[$i];
            unset($need_change);
            $p = xml_parser_create();
            xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
            xml_parser_free($p);
            $ar=[
                'BF','BR','BB','BT','BL','GR','MA','MAC','ML','MS'
            ];
            //.. 24.05.2021 Удаление переменной $ok приводит к удалению ключей массива
            // с XNC-операциями (), поэтому строка ниже закомментирована и добавлено условие
            // если XNC-операции пустые, но прикреплены .mpr-ы, то устанавливаем переменную $ok в 1.
            // unset ($ok);
            if (stripos($vals[$i]["attributes"]["TYPENAME"], 'mpr')) {
                $ok = 1;
            }
            foreach($ar as $a) if (isset($i1[$a])) 
            {
                foreach($i1[$a] as $ia) if ($v1[$ia]['attributes']['ENABLE']<>'false') $ok=1;
            }
            if (!isset($ok))
            {
                unset ($vals[$i],$vals[$i+1],$vals[$i+2]);
            }
        }
    }
    ksort($vals);
    $index=make_index($vals);
    foreach ($index['PART'] as $i)
    {
        if ($vals[$i]['attributes']['CL']>0) 
        {
            $part=get_all_part($vals,$vals[$i]['attributes']['ID']);
            unset ($code);
            if (count($part['xnc'])>2)
            {
                foreach ($part['xnc'] as $ar) $code.=' ['.$ar['attributes']['CODE'].'] ';
            }
            $vals[$i]['attributes']['part.xnc_many_op']=$code;
        }
    }

    return $vals;
}

/**
 * 
 **/
function xnc_right ($vals)
{
    ksort($vals);
    $vals=vals_out($vals);
    $index=make_index($vals);
    $arr=['X','CX','X1','X2','BX'];
    $arr2=['Y','CY','Y1','Y2','BY'];
    $arr3=['BL','BR','BT','BB','PROGRAM','COM','TOOL'];
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            $p = xml_parser_create();
            xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
            xml_parser_free($p);
            foreach ($v1 as $iii=>$v)
            {
                $ar=[
                    'BB'=>'X',
                    'BT'=>'X',
                    'BL'=>'Y',
                    'BR'=>'Y'
                ];
                // p_($v1[$iii]);
                foreach ($ar as $aa=>$aab)
                {
                    if ($v1[$iii]['tag']==$aa)
                    {
                        if (!isset($v1[$iii]['attributes'][$aab]))
                        {
                            $rr=$iii-1;
                            while ((!isset($v1[$rr]['attributes'][$aab]))&&($rr>0)) $rr--;
                            $v1[$iii]['attributes'][$aab]=$v1[$rr]['attributes'][$aab];
                        }
                    }
                }
                // p_($v1[$iii]);
            }
            $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
            
            if ($vals[$i]["attributes"]["MIRHOR"]=='true')
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);
                
                foreach ($v1 as $k=>$v)
                {
                    foreach ($arr as $a)
                    {
                        if (isset($v['attributes'][$a])) $v1[$k]['attributes'][$a]='dx-('.$v1[$k]['attributes'][$a].')';


                    }
                    if ($v['tag']=='GR')
                    {
                        
                        if ($v1[$k]['attributes']['X1']==$v1[$k]['attributes']['X2']) 
                        {
                            if ($v1[$k]['attributes']['C']==1)$v1[$k]['attributes']['C']=2;
                            if ($v1[$k]['attributes']['C']==2)$v1[$k]['attributes']['C']=1;
                        }
                    }
                    if ($v['tag']=='MA')
                    {
                        $v1[$k]['attributes']['R']='-1*('.$v1[$k]['attributes']['R'].')';
                        if ($v1[$k]['attributes']['DIR']=='true') $v1[$k]['attributes']['DIR']='false';
                        elseif ($v1[$k]['attributes']['DIR']=='false') $v1[$k]['attributes']['DIR']='true';
                    }
                    elseif (($v['tag']=='ME')||($v['tag']=='MR'))
                    {
                        $v1[$k]['attributes']['A']='-1*('.$v1[$k]['attributes']['A'].')';
                    }
                    elseif ($v['tag']=='MAC')
                    {
                        if ($v1[$k]['attributes']['DIR']=='true') $v1[$k]['attributes']['DIR']='false';
                        elseif ($v1[$k]['attributes']['DIR']=='false') $v1[$k]['attributes']['DIR']='true';
                    }
                    elseif ($v['tag']=='MS')
                    {
                        
                        if ($v1[$k]['attributes']['C']=='1') $v1[$k]['attributes']['C']='2';
                        elseif ($v1[$k]['attributes']['C']=='2') $v1[$k]['attributes']['C']='1';
                    }
                    
                    elseif ($v['tag']=='BL') $v1[$k]['tag']='BR';
                    elseif ($v['tag']=='BR') $v1[$k]['tag']='BL';
                    
                    if ((isset($v1[$k]['attributes']['AS']))&&($v1[$k]['attributes']['AC']>1)&&($v1[$k]['attributes']['AV']=='false')&&($v1[$k]['tag']=='BF'))
                    {
                        $v1[$k]['attributes']['AS']='-1*('.$v1[$k]['attributes']['AS'].')';
                    }
                }
                $vals[$i]["attributes"]["MIRHOR"]='false';
                $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                $_SESSION['error_ok'].='<br> В операции XNC '.$vals[$i]["attributes"]["CODE"].' начало координат по оси X переведено в нижний левый угол.<hr>';

            }
            if ($vals[$i]["attributes"]["MIRVERT"]=='false')
            {
                
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);
                // p_($v1);
                // p_($vals[$i]);
                // p_($vals[$i]["attributes"]["PROGRAM"]);exit;
                foreach ($v1 as $k=>$v)
                {
                    foreach ($arr2 as $a)
                    {
                        if (isset($v['attributes'][$a])) $v1[$k]['attributes'][$a]='dy-('.$v1[$k]['attributes'][$a].')';

                    }
                    if ($v['tag']=='GR')
                    {
                        
                        // p_($v1);

                        if ($v1[$k]['attributes']['Y1']==$v1[$k]['attributes']['Y2']) 
                        {
                            // p_($v1[$k]);

                            if ($v1[$k]['attributes']['C']==1) $v1[$k]['attributes']['C']=2;
                            elseif ($v1[$k]['attributes']['C']==2)$v1[$k]['attributes']['C']=1;
                            // p_($v1[$k]);

                        }
                    }
                    if ($v['tag']=='MA')
                    {
                        $v1[$k]['attributes']['R']='-1*('.$v1[$k]['attributes']['R'].')';
                        if ($v1[$k]['attributes']['DIR']=='true') $v1[$k]['attributes']['DIR']='false';
                        elseif ($v1[$k]['attributes']['DIR']=='false') $v1[$k]['attributes']['DIR']='true';
                    }
                    elseif (($v['tag']=='ME')||($v['tag']=='MR'))
                    {
                        $v1[$k]['attributes']['A']='-1*('.$v1[$k]['attributes']['A'].')';
                    }
                    elseif ($v['tag']=='MAC')
                    {
                        if ($v1[$k]['attributes']['DIR']=='true') $v1[$k]['attributes']['DIR']='false';
                        elseif ($v1[$k]['attributes']['DIR']=='false') $v1[$k]['attributes']['DIR']='true';
                    }
                    elseif ($v['tag']=='MS')
                    {
                        
                        if ($v1[$k]['attributes']['C']=='1') $v1[$k]['attributes']['C']='2';
                        elseif ($v1[$k]['attributes']['C']=='2') $v1[$k]['attributes']['C']='1';
                    }
                    
                    elseif ($v['tag']=='BB') $v1[$k]['tag']='BT';
                    elseif ($v['tag']=='BT') $v1[$k]['tag']='BB';
                    if ((isset($v1[$k]['attributes']['AS']))&&($v1[$k]['attributes']['AC']>1)&&($v1[$k]['attributes']['AV']=='true')&&($v1[$k]['tag']=='BF'))
                    {
                        $v1[$k]['attributes']['AS']='-1*('.$v1[$k]['attributes']['AS'].')';
                    }
                }
                $vals[$i]["attributes"]["MIRVERT"]='true';
                $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                $_SESSION['error_ok'].='<br> В операции XNC '.$vals[$i]["attributes"]["CODE"].' начало координат по оси Y переведено в нижний левый угол.<hr>';
            }
        }
    }
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            if ($vals[$i]["attributes"]["TURN"]<>0)
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);
                // p_($vals[$i]);

                foreach ($v1 as $k=>$v)
                {
                    $t=$v;
                    foreach($arr as $k1=>$a) 
                    {
                        if ((isset($v['attributes'][$a]))||(isset($v['attributes'][$arr2[$k1]]))) $v1[$k]=xnc_turn_change($a,$arr2[$k1],$vals[$i],$v1[$k],$v1[0]['attributes']['DX'],$v1[0]['attributes']['DY']);
                    }
                    if ($v['tag']=='ERR') unset ($v1[$k]);
                }

                if (($vals[$i]["attributes"]["TURN"]==1)||($vals[$i]["attributes"]["TURN"]==3))
                {
                    $r=$v1[0]['attributes']['DY'];
                    $v1[0]['attributes']['DY']=$v1[0]['attributes']['DX'];
                    $v1[0]['attributes']['DX']=$r;
                }
                $vals[$i]["attributes"]["TURN"]=0;
                $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                $_SESSION['error_ok'].='<br> В операции XNC '.$vals[$i]["attributes"]["CODE"].' убран поворот детали. Он применяется для шаблонов, при дальнейшем редактировании он не нужен. В дальнейшем поворот детали выставляется автоматически при выгрузке программ на оборудование.<hr>';
            }
        }
    }

    foreach ($index["PART"] as $i)
    {
        $ar=['PROGRAM','BF','TOOL'];
        if ($vals[$i]["attributes"]["CL"]>0)
        {
            $part=get_all_part($vals,$vals[$i]["attributes"]["ID"]);
            // p_($part);
            unset ($op_in,$op_in_tool,$op_tr);
            if (count($part['xnc'])>1)
            {
                foreach($part['xnc'] as $k=>$op)
                {
                    if ($op['attributes']['SIDE']=='true')
                    {
                        $op_tr=$k;
                    }
                }
                if ($op_tr>0)
                foreach($part['xnc'] as $k=>$op)
                {
                    unset ($no);
                    if ($op['attributes']['SIDE']=='false')
                    {
                        $v1=get_vals ($op['attributes']['PROGRAM']);
                        $i1=make_index ($v1);
                        foreach ($i1 as $r=>$rrrr) if (!in_array($r,$ar))
                        {
                            // p_($r);
                            $no=1;
                        }
                        // p_($no);
                        foreach ($i1['BF'] as $b)
                        {

                            $v1[$b]['attributes']['DP']=str_replace ('dz',$part['cs']['sheet']['attributes']['T'],$v1[$b]['attributes']['DP']);
                            // p_($v1[$b]);
                            // exit('11');
                            // try {
                            //     eval ("\$v1[$b]['attributes']['DP'] = ".$v1[$b]['attributes']['DP'].";");
                            //     // eval ("\$v1[$b]['attributes']['DP'] = \"$v1[$b]['attributes']['DP']\";");
                            // } catch (Exception $e) {
                            //     $no=1;
                            // }
                            if ($v1[$b]['attributes']['DP']<$part['cs']['sheet']['attributes']['T']) $no=1;
                            if (!isset($no)) $op_in[]=$v1[$b];
                            // p_($v1[$b]);
                        }
                        // p_($no);
                        if (!isset($no)&&(count($op_in)>0))
                        {
                            foreach ($i1['TOOL'] as $b) $op_in_tool[]=$v1[$b];
                            $_SESSION['error_ok']='
                            <br>
                                Программа XNC id='.$op['attributes']['ID'].' code = '.$op['attributes']['CODE'].' была совмещена с программой
                                XNC id = '.$vals[$op_tr]['attributes']['ID'].'  code =  '.$vals[$op_tr]['attributes']['ID'].' .
                            <br>';
                            unset($vals[$k],$vals[$k+1],$vals[$k+2]);
                            $v1=get_vals ($vals[$op_tr]['attributes']['PROGRAM']);
                            $i1=make_index ($v1);
                            foreach($op_in as $oo) $v1=vals_into_end ($v1,$oo);
                            foreach($op_in_tool as $oo)
                            {
                                unset ($t_in);
                                foreach ($i1['TOOL'] as $x)
                                {
                                    if (($v1[$x]['attributes']['NAME']==$oo['attributes']['NAME'])&&
                                    ($v1[$x]['attributes']['D']==$oo['attributes']['D'])) $t_in=1;
                                }
                                if (!isset($t_in))
                                {
                                    $v1=move_vals($v1,1,1);
                                    $v1[1]=$oo;
                                }
                            }
                            ksort($v1);
                            // p_($v1);
                            $vals[$op_tr]['attributes']['PROGRAM']=vals_index_to_project ($v1);
                        }

                    }
                }
            }
            
        }
    }
    // p_($vals);
    return $vals;
}

/**
 * 
 **/
function xnc_ready_for_get($type,$vals)
{
    $vals=xnc_right ($vals);
    $vals=vals_out($vals);
    ksort($vals);
    $project_data=gl_recalc(__LINE__,__FILE__,__FUNCTION__,vals_index_to_project($vals));
    $vals=get_vals($project_data);
    $index=make_index($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=="open"))
        {
            $j=$i+1;
            if (isset($vals[$i]["attributes"]["PROGRAM"]))
            {
                $vals[$i]["attributes"]["PROGRAM"]=str_replace('\\','',$vals[$i]["attributes"]["PROGRAM"]);
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                xml_parser_free($p);
                if (isset($i1['ERR']))
                {
                    foreach ($i1['ERR']as $d)
                    {
                        unset($v1[$d]);
                    }
                    ksort ($v1);
                }
                if(mr_change ($v1,$vals[$i])<>false) $v1=mr_change ($v1,$vals[$i]);
            }
            
            while ($vals[$j]['tag']<>"PART")
            {
                $j++;
            }
            foreach ($index['PART'] as $p)
            {
                if (($vals[$p]['attributes']['ID']==$vals[$j]["attributes"]["ID"])&&($vals[$p]["attributes"]["CL"]>0))
                {
                    $pp=$p;
                }
            }
            unset($change);
            
            $dl=$vals[$pp]["attributes"]["DL"];
            $dw=$vals[$pp]["attributes"]["DW"];
            $cl=$vals[$pp]["attributes"]["CL"]-$vals[$pp]["attributes"][mb_strtoupper("allowancel")]-$vals[$pp]["attributes"][mb_strtoupper("allowancer")];
            $cw=$vals[$pp]["attributes"]["CW"]-$vals[$pp]["attributes"][mb_strtoupper("allowancet")]-$vals[$pp]["attributes"][mb_strtoupper("allowanceb")];
            $sql='DELETE FROM part_xnc WHERE part='.$vals[$pp]["attributes"]["ID"];
            $res1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
            if (($res1['res']==1)&&($vals[$i]["attributes"]["ID"]>0)&&($vals[$i]["attributes"]["CODE"]<>''))
            {
                $sql='SELECT * FROM PART WHERE `PART_ID`='.$vals[$pp]["attributes"]["ID"].';';
                $res1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                if ($res1['count']==1)
                {
                    $sql="SELECT * FROM part_xnc WHERE `part`=".$vals[$pp]["attributes"]["ID"]." AND `id`=".$vals[$i]["attributes"]["ID"]." AND `code`='".$vals[$i]["attributes"]["CODE"]."';";
                    $res1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                    if ($res1['count']<1)
                    {
                        $sql="INSERT INTO part_xnc (`part`,`id`,`code`) VALUES (".$vals[$pp]["attributes"]["ID"].",".$vals[$i]["attributes"]["ID"].",'".$vals[$i]["attributes"]["CODE"]."');";
                        $res1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                    }
                }
            }
            $delta_x=$dl-$cl;
            $delta_y=$dw-$cw;
            $part=get_all_part($vals,$vals[$pp]["attributes"]["ID"]);
            
            if (($type=="BHX")&&($vals[$i]["attributes"]["BYSIZEDETAIL"]=='true'))
            {
                $v1[0]["attributes"]["DY"]=$dw;
                $v1[0]["attributes"]["DX"]=$dl;
            }
            elseif (($type=="VENTURE")&&($vals[$i]["attributes"]["BYSIZEDETAIL"]=='false'))
            {
                $v1[0]["attributes"]["DY"]=$cw;
                $v1[0]["attributes"]["DX"]=$cl;
            }
            elseif (($type=="BHX")&&($vals[$i]["attributes"]["BYSIZEDETAIL"]=='false'))
            {
                $v1[0]["attributes"]["DY"]=$dw;
                $v1[0]["attributes"]["DX"]=$dl;
                $vals[$i]["attributes"]["BYSIZEDETAIL"]='true';
                $v1=xnc_swith_all($part,$v1,$i1,'+',$cl,$cw);
                
            }
            elseif (($type=="VENTURE")&&($vals[$i]["attributes"]["BYSIZEDETAIL"]=='true'))
            {
                $v1[0]["attributes"]["DY"]=$cw;
                $v1[0]["attributes"]["DX"]=$cl;
                $vals[$i]["attributes"]["BYSIZEDETAIL"]='false';
                $v1=xnc_swith_all($part,$v1,$i1,'-',$cl,$cw);

                
            }
            
            $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);

            if ($vals[$i]["attributes"]['BYSIZEDETAIL']=='true') 
            {
                $op[$vals[$i]["attributes"]['CODE']]['габарит']['dx']=$v1[0]['attributes']['DX'];
                $op[$vals[$i]["attributes"]['CODE']]['габарит']['dy']=$v1[0]['attributes']['DY'];
            }
            else
            {
                $op[$vals[$i]["attributes"]['CODE']]['пильный']['dx']=$v1[0]['attributes']['DX'];
                $op[$vals[$i]["attributes"]['CODE']]['пильный']['dy']=$v1[0]['attributes']['DY'];
            }
        }

    }
    
    return $vals;
}

/**
 * 
 **/
function xnc_swith_all($part,$v1,$i1,$op,$cl,$cw)
{
    // ini_set('memory_limit', '10024M');
    // p_($v1[51]);
    $part['part']['attributes']['CL']=$cl;
    $part['part']['attributes']['CW']=$cw;
    foreach ($i1['GR'] as $r)
    {
        if ($op=='+')
        {
            // $v1[$r]['attributes']['X1']=str_replace('dx',$part['part']['attributes']['CL'],$v1[$r]['attributes']['X1']);
            // $v1[$r]['attributes']['Y1']=str_replace('dy',$part['part']['attributes']['CW'],$v1[$r]['attributes']['Y1']);
            // $v1[$r]['attributes']['X2']=str_replace('dx',$part['part']['attributes']['CL'],$v1[$r]['attributes']['X2']);
            // $v1[$r]['attributes']['Y2']=str_replace('dy',$part['part']['attributes']['CW'],$v1[$r]['attributes']['Y2']);
            // if ((($v1[$r]['attributes']['X1']>0)&&(is_numeric($v1[$r]['attributes']['X1'])))||(($v1[$r]['attributes']['X1']<>'')&&(!is_numeric($v1[$r]['attributes']['X1'])))) $v1[$r]['attributes']['X1'].='+'.$part['el']['l'];
            // if ((($v1[$r]['attributes']['Y1']>0)&&(is_numeric($v1[$r]['attributes']['Y1'])))||(($v1[$r]['attributes']['Y1']<>'')&&(!is_numeric($v1[$r]['attributes']['Y1'])))) $v1[$r]['attributes']['Y1'].='+'.$part['el']['b'];
            // if ((($v1[$r]['attributes']['X2']>0)&&(is_numeric($v1[$r]['attributes']['X2'])))||(($v1[$r]['attributes']['X2']<>'')&&(!is_numeric($v1[$r]['attributes']['X2'])))) $v1[$r]['attributes']['X2'].='+'.$part['el']['l'];
            // if ((($v1[$r]['attributes']['Y2']>0)&&(is_numeric($v1[$r]['attributes']['Y2'])))||(($v1[$r]['attributes']['Y2']<>'')&&(!is_numeric($v1[$r]['attributes']['Y2'])))) $v1[$r]['attributes']['Y2'].='+'.$part['el']['b'];

            //.. 07.07.2021 Деление в условии отрабатывало не корректно:
            // str_replace($part['part']['attributes']['CL']/1,$part['part']['attributes']['DL']/1,$v1[$r]['attributes']['X1'])
            // Не могу придумать зачем здесь деление, поэтому по-хорошему его бы удалить, 
            // но мало ли... Чтобы ничего не сломать и в то же время для корректности операции переменную,
            // в которой производится замена, приводим к типу integer
            $v1[$r]['attributes']['X1'] = str_replace($part['part']['attributes']['CL']/1, $part['part']['attributes']['DL']/1, (int)$v1[$r]['attributes']['X1']);
            $v1[$r]['attributes']['X2'] = str_replace($part['part']['attributes']['CL']/1, $part['part']['attributes']['DL']/1, (int)$v1[$r]['attributes']['X2']);
            $v1[$r]['attributes']['Y1'] = str_replace($part['part']['attributes']['CW']/1, $part['part']['attributes']['DW']/1, (int)$v1[$r]['attributes']['Y1']);
            $v1[$r]['attributes']['Y2'] = str_replace($part['part']['attributes']['CW']/1, $part['part']['attributes']['DW']/1, (int)$v1[$r]['attributes']['Y2']);
            if (($v1[$r]['attributes']['X1']>0)&&($v1[$r]['attributes']['X1']<>$part['part']['attributes']['DL'])) $v1[$r]['attributes']['X1'].='+'.$part['el']['l'];
            if (($v1[$r]['attributes']['X2']>0)&&($v1[$r]['attributes']['X2']<>$part['part']['attributes']['DL'])) $v1[$r]['attributes']['X2'].='+'.$part['el']['l'];
            if (($v1[$r]['attributes']['Y1']>0)) $v1[$r]['attributes']['Y1'].='+'.$part['el']['b'];
            if (($v1[$r]['attributes']['Y2']>0)) $v1[$r]['attributes']['Y2'].='+'.$part['el']['b'];
        
        }
        elseif ($op=='-')
        {
            // p_($v1[$r]);

            // $v1[$r]['attributes']['X1']=str_replace('dx',$part['part']['attributes']['DL'],$v1[$r]['attributes']['X1']);
            // $v1[$r]['attributes']['Y1']=str_replace('dy',$part['part']['attributes']['DW'],$v1[$r]['attributes']['Y1']);
            // $v1[$r]['attributes']['X2']=str_replace('dx',$part['part']['attributes']['DL'],$v1[$r]['attributes']['X2']);
            // $v1[$r]['attributes']['Y2']=str_replace('dy',$part['part']['attributes']['DW'],$v1[$r]['attributes']['Y2']);

            //.. 07.07.2021 Деление в условии отрабатывало не корректно:
            // str_replace($part['part']['attributes']['CL']/1,$part['part']['attributes']['DL']/1,$v1[$r]['attributes']['X1'])
            // Не могу придумать зачем здесь деление, поэтому по-хорошему его бы удалить, 
            // но мало ли... Чтобы ничего не сломать и в то же время для корректности операции переменную,
            // в которой производится замена, приводим к типу integer
            $v1[$r]['attributes']['X1'] = str_replace($part['part']['attributes']['DL']/1, $part['part']['attributes']['CL']/1, (int)$v1[$r]['attributes']['X1']);
            $v1[$r]['attributes']['X2'] = str_replace($part['part']['attributes']['DL']/1, $part['part']['attributes']['CL']/1, (int)$v1[$r]['attributes']['X2']);
            $v1[$r]['attributes']['Y1'] = str_replace($part['part']['attributes']['DW']/1, $part['part']['attributes']['CW']/1, (int)$v1[$r]['attributes']['Y1']);
            $v1[$r]['attributes']['Y2'] = str_replace($part['part']['attributes']['DW']/1, $part['part']['attributes']['CW']/1, (int)$v1[$r]['attributes']['Y2']);
            
            // if ((($v1[$r]['attributes']['X1']>0)&&(is_numeric($v1[$r]['attributes']['X1'])))||(($v1[$r]['attributes']['X1']<>'')&&(!is_numeric($v1[$r]['attributes']['X1'])))) $v1[$r]['attributes']['X1'].='-'.$part['el']['l'];
            // if ((($v1[$r]['attributes']['Y1']>0)&&(is_numeric($v1[$r]['attributes']['Y1'])))||(($v1[$r]['attributes']['Y1']<>'')&&(!is_numeric($v1[$r]['attributes']['Y1'])))) $v1[$r]['attributes']['Y1'].='-'.$part['el']['b'];
            // if ((($v1[$r]['attributes']['X2']>0)&&(is_numeric($v1[$r]['attributes']['X2'])))||(($v1[$r]['attributes']['X2']<>'')&&(!is_numeric($v1[$r]['attributes']['X2'])))) $v1[$r]['attributes']['X2'].='-'.$part['el']['l'];
            // if ((($v1[$r]['attributes']['Y2']>0)&&(is_numeric($v1[$r]['attributes']['Y2'])))||(($v1[$r]['attributes']['Y2']<>'')&&(!is_numeric($v1[$r]['attributes']['Y2'])))) $v1[$r]['attributes']['Y2'].='-'.$part['el']['b'];
            
            if (($v1[$r]['attributes']['X1']>0)&&($v1[$r]['attributes']['X1']<>$part['part']['attributes']['CL'])) $v1[$r]['attributes']['X1'].='-'.$part['el']['l'];
            if (($v1[$r]['attributes']['X2']>0)&&($v1[$r]['attributes']['X2']<>$part['part']['attributes']['CL'])) $v1[$r]['attributes']['X2'].='-'.$part['el']['l'];
            if (($v1[$r]['attributes']['Y1']>0)) $v1[$r]['attributes']['Y1'].='-'.$part['el']['b'];
            if (($v1[$r]['attributes']['Y2']>0)) $v1[$r]['attributes']['Y2'].='-'.$part['el']['b'];
            
            //.. 20.12.2021 Пазованиие при нулевом отступе высчитывалось не правильно (четыре строки выше)
            // Начало по X
            // if ($v1[$r]['attributes']['X1'] > 0) {
            //     if (($v1[$r]['attributes']['X1'] <> $part['part']['attributes']['CL'])) {
            //         $v1[$r]['attributes']['X1'] .= '-' . $part['el']['l'];
            //     } else {
            //         if (($v1[$r]['attributes']['X1'] == $part['part']['attributes']['CL']) &&
            //             (($v1[$r]['attributes']['X1'] + $part['el']['l'] + $part['el']['r']) > $part['part']['attributes']['CL']))
            //         {
            //             $v1[$r]['attributes']['X1'] .= '+' . $part['el']['l'];
            //             // $v1[$r]['attributes']['X1'] = ($v1[$r]['attributes']['X1'] + 1);
            //         }
            //     }
            // } elseif (($v1[$r]['attributes']['X1'] == 0) &&
            //          (($v1[$r]['attributes']['X1'] + $part['el']['l'] + $part['el']['r']) > $v1[$r]['attributes']['X1']))
            // {
            //     $v1[$r]['attributes']['X1'] .= '-' . $part['el']['r'];
            //     // $v1[$r]['attributes']['X1'] = ($v1[$r]['attributes']['X1'] - 1);
            // }

            // // Конец по X
            // if ($v1[$r]['attributes']['X2'] > 0) {
            //     if (($v1[$r]['attributes']['X2'] <> $part['part']['attributes']['CL'])) {
            //         $v1[$r]['attributes']['X2'] .= '-' . $part['el']['l'];
            //     } else {
            //         if (($v1[$r]['attributes']['X2'] == $part['part']['attributes']['CL']) &&
            //             (($v1[$r]['attributes']['X2'] + $part['el']['l'] + $part['el']['r']) > $part['part']['attributes']['CL']))
            //         {
            //             $v1[$r]['attributes']['X2'] .= '+' . $part['el']['l'];
            //             // $v1[$r]['attributes']['X2'] = ($v1[$r]['attributes']['X2'] + 1);
            //         }
            //     }
            // } elseif (($v1[$r]['attributes']['X2'] == 0) &&
            //          (($v1[$r]['attributes']['X2'] + $part['el']['l'] + $part['el']['r']) > $v1[$r]['attributes']['X2']))
            // {
            //     $v1[$r]['attributes']['X2'] .= '-' . $part['el']['r'];
            //     // $v1[$r]['attributes']['X2'] = ($v1[$r]['attributes']['X2'] - 1);
            // }

            // // Начало по Y
            // if ($v1[$r]['attributes']['Y1'] > 0) {
            //     if (($v1[$r]['attributes']['Y1'] <> $part['part']['attributes']['CW'])) {
            //         $v1[$r]['attributes']['Y1'] .= '-' . $part['el']['t'];
            //     } else {
            //         if (($v1[$r]['attributes']['Y1'] == $part['part']['attributes']['CW']) &&
            //             (($v1[$r]['attributes']['Y1'] + $part['el']['t'] + $part['el']['b']) > $part['part']['attributes']['CW']))
            //         {
            //             $v1[$r]['attributes']['Y1'] .= '+' . $part['el']['t'];
            //             // $v1[$r]['attributes']['Y1'] = ($v1[$r]['attributes']['Y1'] + 1);
            //         }
            //     }
            // } elseif (($v1[$r]['attributes']['Y1'] == 0) &&
            //          (($v1[$r]['attributes']['Y1'] + $part['el']['t'] + $part['el']['b']) > $v1[$r]['attributes']['Y1']))
            // {
            //     $v1[$r]['attributes']['Y1'] .= '-' . $part['el']['b'];
            //     // $v1[$r]['attributes']['Y1'] = ($v1[$r]['attributes']['Y1'] - 1);
            // }
            
            // // Конец по Y
            // if ($v1[$r]['attributes']['Y2'] > 0) {
            //     if (($v1[$r]['attributes']['Y2'] <> $part['part']['attributes']['CW'])) {
            //         $v1[$r]['attributes']['Y2'] .= '-' . $part['el']['t'];
            //     } else {
            //         if (($v1[$r]['attributes']['Y2'] == $part['part']['attributes']['CW']) &&
            //             (($v1[$r]['attributes']['Y2'] + $part['el']['t'] + $part['el']['b']) > $part['part']['attributes']['CW']))
            //         {
            //             $v1[$r]['attributes']['Y2'] .= '+' . $part['el']['t'];
            //             // $v1[$r]['attributes']['Y2'] = ($v1[$r]['attributes']['Y2'] + 1);
            //         }
            //     }
            // } elseif (($v1[$r]['attributes']['Y2'] == 0) &&
            //          (($v1[$r]['attributes']['Y2'] + $part['el']['t'] + $part['el']['b']) > $v1[$r]['attributes']['Y2']))
            // {
            //     $v1[$r]['attributes']['Y2'] .= '-' . $part['el']['b'];
            //     // $v1[$r]['attributes']['Y2'] = ($v1[$r]['attributes']['Y2'] - 1);
            // }

            // p_($v1[$r]);

            
        }
    }
    foreach ($i1['BF'] as $r)
    {
        if ($op=='+')
        {
            
            $v1[$r]['attributes']['X']=str_replace('dx',$part['part']['attributes']['CL'],$v1[$r]['attributes']['X']);
            $v1[$r]['attributes']['Y']=str_replace('dy',$part['part']['attributes']['CW'],$v1[$r]['attributes']['Y']);
            if ((($v1[$r]['attributes']['X']>0)&&(is_numeric($v1[$r]['attributes']['X'])))||(($v1[$r]['attributes']['X']<>'')&&(!is_numeric($v1[$r]['attributes']['X'])))) $v1[$r]['attributes']['X'].='+'.$part['el']['l'];
            if ((($v1[$r]['attributes']['Y']>0)&&(is_numeric($v1[$r]['attributes']['Y'])))||(($v1[$r]['attributes']['Y']<>'')&&(!is_numeric($v1[$r]['attributes']['Y'])))) $v1[$r]['attributes']['Y'].='+'.$part['el']['b'];

            
        }
        elseif ($op=='-')
        {
            $v1[$r]['attributes']['X']=str_replace('dx',$part['part']['attributes']['DL'],$v1[$r]['attributes']['X']);
            $v1[$r]['attributes']['Y']=str_replace('dy',$part['part']['attributes']['DW'],$v1[$r]['attributes']['Y']);
            if ((($v1[$r]['attributes']['X']>0)&&(is_numeric($v1[$r]['attributes']['X'])))||(($v1[$r]['attributes']['X']<>'')&&(!is_numeric($v1[$r]['attributes']['X'])))) $v1[$r]['attributes']['X'].='-'.$part['el']['l'];
            if ((($v1[$r]['attributes']['Y']>0)&&(is_numeric($v1[$r]['attributes']['Y'])))||(($v1[$r]['attributes']['Y']<>'')&&(!is_numeric($v1[$r]['attributes']['Y'])))) $v1[$r]['attributes']['Y'].='-'.$part['el']['b'];

        }
    }
    foreach ($i1['BL'] as $r)
    {
        if ($op=='+')
        {
            $v1[$r]['attributes']['Y']=str_replace('dy',$part['part']['attributes']['CW'],$v1[$r]['attributes']['Y']);
            if ((($v1[$r]['attributes']['Y']>0)&&(is_numeric($v1[$r]['attributes']['Y'])))||(($v1[$r]['attributes']['Y']<>'')&&(!is_numeric($v1[$r]['attributes']['Y'])))) $v1[$r]['attributes']['Y'].='+'.$part['el']['b'];

        }
        elseif ($op=='-')
        {
            $v1[$r]['attributes']['Y']=str_replace('dy',$part['part']['attributes']['DW'],$v1[$r]['attributes']['Y']);
            if ((($v1[$r]['attributes']['Y']>0)&&(is_numeric($v1[$r]['attributes']['Y'])))||(($v1[$r]['attributes']['Y']<>'')&&(!is_numeric($v1[$r]['attributes']['Y'])))) $v1[$r]['attributes']['Y'].='-'.$part['el']['b'];

        }
    }
    foreach ($i1['BR'] as $r)
    {
        if ($op=='+')
        {
            $v1[$r]['attributes']['Y']=str_replace('dy',$part['part']['attributes']['CW'],$v1[$r]['attributes']['Y']);
            if ((($v1[$r]['attributes']['Y']>0)&&(is_numeric($v1[$r]['attributes']['Y'])))||(($v1[$r]['attributes']['Y']<>'')&&(!is_numeric($v1[$r]['attributes']['Y'])))) $v1[$r]['attributes']['Y'].='+'.$part['el']['b'];

            
        }
        elseif ($op=='-')
        {
            $v1[$r]['attributes']['Y']=str_replace('dy',$part['part']['attributes']['DW'],$v1[$r]['attributes']['Y']);
            if ((($v1[$r]['attributes']['Y']>0)&&(is_numeric($v1[$r]['attributes']['Y'])))||(($v1[$r]['attributes']['Y']<>'')&&(!is_numeric($v1[$r]['attributes']['Y'])))) $v1[$r]['attributes']['Y'].='-'.$part['el']['b'];

            
        }
    }
    foreach ($i1['BB'] as $r)
    {
        if ($op=='+')
        {
            $v1[$r]['attributes']['X']=str_replace('dx',$part['part']['attributes']['CL'],$v1[$r]['attributes']['X']);
            if ((($v1[$r]['attributes']['X']>0)&&(is_numeric($v1[$r]['attributes']['X'])))||(($v1[$r]['attributes']['X']<>'')&&(!is_numeric($v1[$r]['attributes']['X'])))) $v1[$r]['attributes']['X'].='+'.$part['el']['l'];

            
        }
        elseif ($op=='-')
        {
            $v1[$r]['attributes']['X']=str_replace('dx',$part['part']['attributes']['DL'],$v1[$r]['attributes']['X']);
            if ((($v1[$r]['attributes']['X']>0)&&(is_numeric($v1[$r]['attributes']['X'])))||(($v1[$r]['attributes']['X']<>'')&&(!is_numeric($v1[$r]['attributes']['X'])))) $v1[$r]['attributes']['X'].='-'.$part['el']['l'];

        }
    }
    foreach ($i1['BT'] as $r)
    {
        if ($op=='+')
        {
            $v1[$r]['attributes']['X']=str_replace('dx',$part['part']['attributes']['CL'],$v1[$r]['attributes']['X']);
            if ((($v1[$r]['attributes']['X']>0)&&(is_numeric($v1[$r]['attributes']['X'])))||(($v1[$r]['attributes']['X']<>'')&&(!is_numeric($v1[$r]['attributes']['X'])))) $v1[$r]['attributes']['X'].='+'.$part['el']['l'];

        }
        elseif ($op=='-')
        {
            
            $v1[$r]['attributes']['X']=str_replace('dx',$part['part']['attributes']['DL'],$v1[$r]['attributes']['X']);
            if ((($v1[$r]['attributes']['X']>0)&&(is_numeric($v1[$r]['attributes']['X'])))||(($v1[$r]['attributes']['X']<>'')&&(!is_numeric($v1[$r]['attributes']['X'])))) $v1[$r]['attributes']['X'].='-'.$part['el']['l'];

        }
    }
    // $r_d=1;$r_d2=0.001;
    if ($op=='-')
    {
        foreach ($i1['MS'] as $r)
        {
            //.. 11.10.2021 Определяем ручку улыбку:
            // В операции "ручка-улыбка" такая структура: ms, ml, mac, mac, mac, ml
            // поэтому если порядок блоков такой, то это ручка-улыбка и соответственно её не пересчитываем.
            // Пример обработки ручки-улыбки:
            // -------------------------------------------------------------------------------------
            // <ms c="2" dp="21.0" in="2" name="Mill20" out="2" sxy="tool.dia/2" x="0.0" y="0.0"/>
            // <ml dp="21.0" x="273.508894" y="0.0"/>
            // <mac cx="273.508894" cy="100.0" dir="true" dp="21.0" x="318.416979" y="10.650888"/>
            // <mac cx="400.0" cy="-151.666667" dir="false" dp="21.0" x="481.583021" y="10.650888"/>
            // <mac cx="526.491106" cy="100.0" dir="true" dp="21.0" x="526.491106" y="0.0"/>
            // <ml dp="21.0" x="800.0" y="0.0"/>
            // -------------------------------------------------------------------------------------
            $DoorsKnobSmileFlag = false;
            if ( $v1[$r+1]['tag'] == mb_strtoupper('ml') && $v1[$r+2]['tag'] == mb_strtoupper('mac') &&
                 $v1[$r+3]['tag'] == mb_strtoupper('mac') && $v1[$r+4]['tag'] == mb_strtoupper('mac') &&
                 $v1[$r+5]['tag'] == mb_strtoupper('ml')) {

                $DoorsKnobSmileFlag = true;
            
            }

            $rr=$r;
            if ($DoorsKnobSmileFlag === false) {
                do 
                {
                    if (is_numeric($v1[$rr]['attributes']['X']))
                    {
                        if (round($v1[$rr]['attributes']['X'],3)==$part['part']['attributes']['DL']) $v1[$rr]['attributes']['X']=$part['part']['attributes']['CL'];
                        elseif ($v1[$rr]['attributes']['X']>0) $v1[$rr]['attributes']['X']-=$part['el']['l'];
                    }
                    if (is_numeric($v1[$rr]['attributes']['Y'])) 
                    {
                        if (round($v1[$rr]['attributes']['Y'],3)==$part['part']['attributes']['DW']) $v1[$rr]['attributes']['Y']=$part['part']['attributes']['CW'];
                        elseif ($v1[$rr]['attributes']['Y']>0) $v1[$rr]['attributes']['Y']-=$part['el']['b'];
                    }
                    if (is_numeric($v1[$rr]['attributes']['CX'])) 
                    {
                        $v1[$rr]['attributes']['CX']-=$part['el']['l'];
                    }
                    if (is_numeric($v1[$rr]['attributes']['CY'])) 
                    {
                       $v1[$rr]['attributes']['CY']-=$part['el']['b'];
                    }
                    if (is_numeric($v1[$rr]['attributes']['BX'])) 
                    {
                        $v1[$rr]['attributes']['BX']-=$part['el']['l'];
                    }
                    if (is_numeric($v1[$rr]['attributes']['BY'])) 
                    {
                       $v1[$rr]['attributes']['BY']-=$part['el']['b'];
                    }
                    // p_($v1[$rr]);
                    if (($v1[$rr]['tag']=='MAC')&&
                    (is_numeric($v1[$rr]['attributes']['CX']))&&
                    (is_numeric($v1[$rr]['attributes']['CY']))&&
                    (is_numeric($v1[$rr]['attributes']['X']))&&
                    (is_numeric($v1[$rr]['attributes']['Y']))&&
                    (is_numeric($v1[$rr-1]['attributes']['X']))&&
                    (is_numeric($v1[$rr-1]['attributes']['Y'])))
                    {
                        // unset($rrr);
                        // $r_d2=0.1;
                        // for ($r_d=1;$r_d<5;$r_d+0.1)
                        // {
                              
                        //     for ($rrer=0;$rrer<3;$rrer+1)
                        //     {
                        //         $r_d2=$r_d2/10;
                        //         if (!isset($rrr)) $rrr=mac_ch($v1,$rr,$part,$r_d,$r_d2,$op);
                        //         elseif (is_array($rrr)) break;
                        //         // p_($rrr);exit;
                        //         if ($rrr==1) unset($rrr);
                        //         // p_($rrr);exit('34');

                        //     }
                            
                        //     if (isset($rrr)) break;

                        //     // echo "<hr>!!! r_d <hr>$r_d";

                            
                        // } 
                        // exit ('11');
                        // p_($rrr);
                        // if (isset($rrr))
                        // {
                        //     $v1[$rr]['attributes']['CX']=$rrr['new_cx'];
                        //     $v1[$rr]['attributes']['CY']=$rrr['new_cy'];
                        // }
                        $x1=$v1[$rr-1]['attributes']['X'];
                        $y1=$v1[$rr-1]['attributes']['Y'];
                        $x2=$v1[$rr]['attributes']['X'];
                        $y2=$v1[$rr]['attributes']['Y'];
                        $cx=$v1[$rr]['attributes']['CX'];
                        $cy=$v1[$rr]['attributes']['CY'];
                        // echo "x1 $x1 x2 $x2 y1 $y1 y2 $y2 cx $cx cy $cy <hr>";

                        $r=sqrt(($cx-$x1)*($cx-$x1)+($cy-$y1)*($cy-$y1));
                        // test ('r',$r);
                        $d=sqrt (($x1-$x2)*($x1-$x2) + ($y1-$y2)*($y1-$y2));
                        // test ('d',$d/2);
                        // $r+=5;
                        // test ('r',$r);

                        $h = sqrt($r * $r - ($d/2) * ($d/2));
                        if (is_nan($h))
                        {
                            $d=$d-1;
                            $h = sqrt($r * $r - ($d/2) * ($d/2));
                        }
                        // test ('h',$h);

                        $x01 = $x1 + ($x2 - $x1)/2 + $h * ($y2 - $y1) / $d;
                        $y01 = $y1 + ($y2 - $y1)/2 - $h * ($x2 - $x1) / $d;
                        $x02 = $x1 + ($x2 - $x1)/2 - $h * ($y2 - $y1) / $d;
                        $y02 = $y1 + ($y2 - $y1)/2 + $h * ($x2 - $x1) / $d;
                        if ($v1[$rr]['attributes']['DIR']=='false')
                        {
                            $v1[$rr]['attributes']['CX']=$x01;
                            $v1[$rr]['attributes']['CY']=$y01;
                            // test ('x01',$x01);
                            // test ('y01',$y01);
                        }
                        else
                        {
                            $v1[$rr]['attributes']['CX']=$x02;
                            $v1[$rr]['attributes']['CY']=$y02;
                            // test ('x02',$x02);
                            // test ('y02',$y02);
                        }
                        unset ($rrr,$rrr_err);
                        // exit;
                    }
                    
                    // p_($v1[$rr]);
                    // exit;

                    $rr=$rr+1;

                } while (($v1[$rr]['tag']=='ML')||($v1[$rr]['tag']=='MAC'));
            } // Условие для ручек-улыбок

        }
    }
    if ($op=='+')
    {

        foreach ($i1['MS'] as $r)
        {
            $rr=$r;
            do 
            {
                
                if (is_numeric($v1[$rr]['attributes']['X']))
                {
                    if (round($v1[$rr]['attributes']['X'],3)==$part['part']['attributes']['CL']) $v1[$rr]['attributes']['X']=$part['part']['attributes']['DL'];
                    elseif ($v1[$rr]['attributes']['X']>0) $v1[$rr]['attributes']['X']+=$part['el']['l'];
                }
                
                if (is_numeric($v1[$rr]['attributes']['Y'])) 
                {
                    if (round($v1[$rr]['attributes']['Y'],3)==$part['part']['attributes']['CW']) $v1[$rr]['attributes']['Y']=$part['part']['attributes']['DW'];
                    elseif ($v1[$rr]['attributes']['Y']>0) $v1[$rr]['attributes']['Y']+=$part['el']['b'];
                }
                if (is_numeric($v1[$rr]['attributes']['CX'])) 
                {
                    $v1[$rr]['attributes']['CX']+=$part['el']['l'];
                }
                if (is_numeric($v1[$rr]['attributes']['CY'])) 
                {
                   $v1[$rr]['attributes']['CY']+=$part['el']['b'];
                }
                if (is_numeric($v1[$rr]['attributes']['BX'])) 
                {
                    $v1[$rr]['attributes']['BX']+=$part['el']['l'];
                }
                if (is_numeric($v1[$rr]['attributes']['BY'])) 
                {
                   $v1[$rr]['attributes']['BY']+=$part['el']['b'];
                }

                if (($v1[$rr]['tag']=='MAC')&&
                (is_numeric($v1[$rr]['attributes']['CX']))&&
                (is_numeric($v1[$rr]['attributes']['CY']))&&
                (is_numeric($v1[$rr]['attributes']['X']))&&
                (is_numeric($v1[$rr]['attributes']['Y']))&&
                (is_numeric($v1[$rr-1]['attributes']['X']))&&
                (is_numeric($v1[$rr-1]['attributes']['Y'])))
                {
                    // $r_d=1;$r_d2=0.1;
                    // unset($rrr);
                    // $r_d=1;
                    // $r_d2=0.1;
                    // for ($r_d=1;$r_d<5;$r_d+0.1)
                    // {
                          
                    //     for ($rrer=0;$rrer<3;$rrer+1)
                    //     {
                    //         $r_d2=$r_d2/10;
                    //         if (!isset($rrr)) $rrr=mac_ch($v1,$rr,$part,$r_d,$r_d2,$op);
                    //         elseif (is_array($rrr)) break;
                    //         // p_($rrr);exit;
                    //         if ($rrr==1) unset($rrr);
                    //         // p_($rrr);exit('34');

                    //     }
                        
                    //     if (isset($rrr)) break;

                    //     // echo "<hr>!!! r_d <hr>$r_d";

                        
                    // } 
                    // // exit('12');
                    //     // exit;
                    // if (isset($rrr))
                    // {
                    //     $v1[$rr]['attributes']['CX']=$rrr['new_cx'];
                    //     $v1[$rr]['attributes']['CY']=$rrr['new_cy'];
                    // }

                    // p_($v1[$rr]);echo '!!<hr>';
                    $x1=$v1[$rr-1]['attributes']['X'];
                    $y1=$v1[$rr-1]['attributes']['Y'];
                    $x2=$v1[$rr]['attributes']['X'];
                    $y2=$v1[$rr]['attributes']['Y'];
                    $cx=$v1[$rr]['attributes']['CX'];
                    $cy=$v1[$rr]['attributes']['CY'];
                    // echo "x1 $x1 x2 $x2 y1 $y1 y2 $y2 cx $cx cy $cy <hr>";

                    $r=sqrt(($cx-$x1)*($cx-$x1)+($cy-$y1)*($cy-$y1));
                    // test ('r',$r);
                    $d=sqrt (($x1-$x2)*($x1-$x2) + ($y1-$y2)*($y1-$y2));
                    // test ('d',$d);
                    $h = sqrt($r * $r - ($d/2) * ($d/2));
                    $x01 = $x1 + ($x2 - $x1)/2 + $h * ($y2 - $y1) / $d;
                    $y01 = $y1 + ($y2 - $y1)/2 - $h * ($x2 - $x1) / $d;
                    $x02 = $x1 + ($x2 - $x1)/2 - $h * ($y2 - $y1) / $d;
                    $y02 = $y1 + ($y2 - $y1)/2 + $h * ($x2 - $x1) / $d;
                    if ($v1[$rr]['attributes']['DIR']=='false')
                    {
                        $v1[$rr]['attributes']['CX']=$x01;
                        $v1[$rr]['attributes']['CY']=$y01;
                        // test ('x01',$x01);
                        // test ('y01',$y01);
                    }
                    else
                    {
                        $v1[$rr]['attributes']['CX']=$x02;
                        $v1[$rr]['attributes']['CY']=$y02;
                        // test ('x02',$x02);
                        // test ('y02',$y02);
                    }
    
                    //  p_($v1[$rr]);exit;
                    

                    // exit;
                    unset ($rrr,$rrr_err);
                }
                


// d = sqr ((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
// h = sqr(r * r - (d/2) * (d/2));

// x01 = x1 + (x2 - x1)/2 + h * (y2 - y1) / d
// y01 = y1 + (y2 - y1)/2 - h * (x2 - x1) / d

// x02 = x1 + (x2 - x1)/2 - h * (y2 - y1) / d
// y02 = y1 + (y2 - y1)/2 + h * (x2 - x1) / d

                $rr=$rr+1;
            } while (($v1[$rr]['tag']=='ML')||($v1[$rr]['tag']=='MAC'));
        }
    }
    // p_($v1);exit;
    return $v1;
}

/**
 * 
 **/
function xnc_code($vals,$session)
{
    $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'xnc_code_start_clear_vals', $session);

    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'xnc_code_start', $session);
    
    // exit("qq");
    foreach ($index["PART"] as $i)
    {
        
        if ($vals[$i]['attributes']['CL']>0)
        {
            foreach (SIDE as $s)
            {
                if (isset($vals[$i]['attributes']['part.my_side_to_cut_d_'.$s]))
                {
                    if (($s=="l")OR($s=="r")) $side='dl';
                    else $side='dw';
                    $thikness=get_thikness_edge($vals,$vals[$i]['attributes']['ID'],$side);
                    // p_($thikness);echo $s;
                    $vals[$i]['attributes']['part.my_side_to_cut_'.$s]=$vals[$i]['attributes']['part.my_side_to_cut_d_'.$s]-$thikness;
                }
            }
            $vals[$i]=my_uncet($vals[$i],mb_strtolower('MY_XNC_SIDE_L'));
            $vals[$i]=my_uncet($vals[$i],mb_strtolower('MY_XNC_SIDE_B'));
            $vals[$i]=my_uncet($vals[$i],mb_strtolower('MY_XNC_SIDE_R'));
            $vals[$i]=my_uncet($vals[$i],mb_strtolower('MY_XNC_SIDE_T'));
            foreach(SIDE as $h)
            {
                //.. 24.04.2023 Тоценко
                // К проверке добавлен параметр MY_SMALL_EDGE_DETAIL, который запрещает возвращать размер детали к изначальному.
                // Этот параметр устанавливается в файле system/library/functions/fs_restrictions/checkEdgeSizesRestrictions.php
                // когда деталь слишком маленькая для кромкования и на ней нет XNC-обработок
                if ($vals[$i]['attributes']['part.my_side_to_cut_'.$h]>0 && !isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL'])) $change=1;    
                elseif ($vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_'.$h)]>0 && !isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL'])) $change=1;    
                elseif ($vals[$i]['attributes'][mb_strtoupper('my_side_to_cut_'.$h)]>0 && !isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL'])) $change=1;    
            }
            if ($change==1)
            {
                foreach ($index["OPERATION"] as $j)
                {
                    if (($vals[$j]["attributes"]["TYPEID"]=="XNC")&&($vals[$j]['type']=='open'))
                    {
                        $jj=$j+1;
                        while ($vals[$jj]['tag']=="PART")
                        {
                            if ($vals[$jj]['attributes']['ID']==$vals[$i]['attributes']['ID']) $change=2;
                            $jj++;
                        }
                    }
                }
            }
            // p_($change);exit;
            if ($change==1)
            {
                
                // exit('qq');
                foreach(SIDE as $s22)
                {
                    if (isset($vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_'.$s22)])) $vals[$i]['attributes']['part.my_side_to_cut_'.$s22]=$vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_'.$s22)];
                    if ((($s22=="l")&&isset($vals[$i]['attributes']['part.my_side_to_cut_l']))OR(($s22=="r")&&(isset($vals[$i]['attributes']['part.my_side_to_cut_r']))))
                    {
                        $delta=$vals[$i]['attributes']['CL']-$vals[$i]['attributes']['part.my_side_to_cut_'.$s22];
                        $vals[$i]['attributes']['CL']-=$delta;
                        $vals[$i]['attributes']['L']-=$delta;
                        $vals[$i]['attributes']['DL']-=$delta;
                        $vals[$i]['attributes']['JL']-=$delta;
                        $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_'.$s22);
                        $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_'.$s22);
                        $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltax');
                    }
                    elseif ((($s22=="t")&&isset($vals[$i]['attributes']['part.my_side_to_cut_t']))OR(($s22=="b")&&(isset($vals[$i]['attributes']['part.my_side_to_cut_b']))))
                    {
                        $delta=$vals[$i]['attributes']['CW']-$vals[$i]['attributes']['part.my_side_to_cut_'.$s22];
                        $vals[$i]['attributes']['CW']-=$delta;
                        $vals[$i]['attributes']['W']-=$delta;
                        $vals[$i]['attributes']['DW']-=$delta;
                        $vals[$i]['attributes']['JW']-=$delta;
                        $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_'.$s22);
                        $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_'.$s22);
                        $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltay');
                    }
                }
                $e4=part_get_material($vals, $vals[$i]["attributes"]["ID"]);
                $_SESSION['error_ok'].="<br>Деталь '".$vals[$i]["attributes"]["NAME"]."' (id='".$vals[$i]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (DW ".$vals[$i]["attributes"]["DW"]." * DL ".$vals[$i]["attributes"]["DL"].")
                    - так как отсутствует XNC операция, а ранее деталь была увеличена из-за неё, размеры детали изменены к первоначальным.<hr>";
                    // exit;
            }
            unset($change);
        }
    }
    // xml($vals); 
    foreach ($index['OPERATION'] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=="XNC"))
        {
            $r=$vals[$i];
            
            if (($vals[$i+1]['tag']<>'PART')||(($vals[$i+1]['tag']=='OPERATION')&&($vals[$i+1]['attributes']['TYPEID']<>"XNC")))
            {
                $_SESSION['error_ok'].="<br>Обработка '".$r["attributes"]["CODE"]."' (id='".$r["attributes"]["ID"]."') была удалена, так как не привязана ни к одной детали.<hr>";
                // p_($vals[$i]);
                if ($vals[$i]['type']=='open') unset ($vals[$i+1]);
                unset ($vals[$i]);
                // echo($i);

            }
            elseif($vals[$i]['attributes']['operation.glass_empty']==1)
            {
                unset($vals[$i],$vals[$i+1],$vals[$i+2]);
            }
            else
            {
                // p_($vals[$i]);
                
                $j=$i+1;
                unset ($p_ok);
                while ((($vals[$j]['tag']<>'OPERATION')&&($vals[$j]['type']<>'close')&&($vals[$j]['type']<>'open'))||
                        (($vals[$j]['tag']=='OPERATION')&&($vals[$j]['type']=='complete'))&&(isset($vals[$j])))
                {
                    // p_($vals[$j]);
                    // exit ('12');
                    if ($vals[$j]['tag']=='PART')
                    {
                        $e4=get_all_part($vals, $vals[$j]['attributes']['ID']);
                        // p_($e4);
                        
                        $p_ok=1;
                        $part[$vals[$j]['attributes']['ID']][]=$vals[$i]['attributes']['ID'];
                        $part[$vals[$j]['attributes']['ID']]['i']['name']=$e4['part']['attributes']['NAME'];
                        $part[$vals[$j]['attributes']['ID']]['i']['dw']=$e4['part']['attributes']['DW'];
                        $part[$vals[$j]['attributes']['ID']]['i']['dl']=$e4['part']['attributes']['DL'];
                    }
                    $j++;
                }
                // !123
                if ($vals[$i]['attributes']['BYSIZEDETAIL']<>"true")
                {
                    if (($session['manager_id']>0)&&(is_numeric($session['manager_id'])))
                    {   
                        $sql='SELECT * FROM manager WHERE id='.$session['manager_id'];
                        $is_admin=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['admin'];
                    }
                    if ($is_admin<>1)
                    {
                        if (!isset($e4['el']))
                        {
                            $vals[$i]['attributes']['BYSIZEDETAIL']='true';
                        }
                        // else  $_SESSION['error_ok'].="<br>Обработка '".$r["attributes"]["CODE"]."' (id='".$r["attributes"]["ID"]."') сделана по пильному размеру. Пожалуйста переведите обработку в формат габаритного размера. Для этого поставьте галочку 'по размеру детали' и пересчитайте координаты точек с учётом того, что координаты считаются по детали с кромкой. При использовании шаблонов координаты пересчитаются автоматически.<hr>";
                
                    }
                }
                // test ('j',$j);
                if (!isset($p_ok))
                {
                    // exit('11');
                    while ($j>=$i)
                    {
                        // p_($vals[$j]);
                        unset ($vals[$j]);
                        $j--;
                    }
                    // $e4=part_get_material($vals, $vals[$j]["attributes"]["ID"]);

                    $_SESSION['error_ok'].="<br>Обработка '".$r["attributes"]["CODE"]."' (id='".$r["attributes"]["ID"]."') была удалена, так как не привязана ни к одной детали.<hr>";
                }
                else $ok=1;
            }
        }
        
        

    }
    // p_($_SESSION['error_ok']);
    // p_($vals);
    // exit;
    ksort ($vals);
    $index=make_index($vals);
    $sql="SELECT * FROM `manager` WHERE id='".$session['manager_id']."'";
    $admin=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['admin'];
    if ($admin<>1)
    {
        foreach ($part as $id=>$p)
        {
            // echo count($p).'  ';
            if (count($p)>3)
            {
                $e4=part_get_material($vals, $id);
                $_SESSION['error'].="<br>Деталь '".$part[$id]['i']['name']."' (id='".$id."') (материал '".$e4['attributes']['NAME']."') (DW ".$part[$id]['i']['dw']." * DL ".$part[$id]['i']['dl'].")
                    - применено более 2 XNC операций. Пожалуйста объедините обработки деталей. Если всё же нужно более 2 обработок на деталь, воспользуйтесь кнопкой 'Помощь обработчика', наши конструкторы смогут запустить более 2 обработок на деталь..<hr>";
            }
        }
    }
    
    if (!isset($ok))
    {
        foreach ($index['PART'] as $i)
        {
            //.. 24.04.2023 Тоценко
            // К проверке добавлен параметр MY_SMALL_EDGE_DETAIL, который запрещает возвращать размер детали к изначальному.
            // Этот параметр устанавливается в файле system/library/functions/fs_restrictions/checkEdgeSizesRestrictions.php
            // когда деталь слишком маленькая для кромкования и на ней нет XNC-обработок
            if (($vals[$i]['type']=="complete")&&($vals[$i]['attributes']['CL']>0) && !isset($vals[$i]['attributes']['MY_SMALL_EDGE_DETAIL']))
            {
                $vals[$i]=my_uncet($vals[$i],'my_xnc_side_l');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_side_b');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_side_r');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_side_t');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltax');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltay');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_l');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_b');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_r');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_t');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_l');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_b');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_r');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_t');
            }
        }
    }

    // test ('ok',$ok);
    if (isset($ok))
    {
        
        // p_($update_project);exit;
        ksort($vals);
        $index=make_index($vals);
        
        foreach ($index['OPERATION'] as $i)
        {
            if ($vals[$i]['attributes']['TYPEID']=='XNC')
            {
                if ((isset($vals[$i]["attributes"]["PROGRAM"]))&&($vals[$i]["attributes"]['operation.my_mpr_in']<>1))
                {
                    $vals[$i]["attributes"]["PROGRAM"]=str_replace('\\','',$vals[$i]["attributes"]["PROGRAM"]);
                    $p = xml_parser_create();
                    xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                    xml_parser_free($p);
                    unset ($xnc_t_sp);
                    foreach ($v1 as $iii=>$rtt)
                    {
                        if ($v1[$iii]['tag']=='XY') unset($v1[$iii]);
                        foreach ($v1[$iii]['attributes'] as $xnc_t_k=>$xnc_t_v)
                        if ($xnc_t_v=='edge_first_before_frez')$xnc_t_sp=1;
                        

                    }
                    // exit;
                    // p_($v1);
                    ksort ($v1);
                    $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                    $i1=make_index($v1);
                    foreach ($index['PART'] as $p)
                    {
                        if (($vals[$p]['attributes']['ID']==$vals[$i+1]["attributes"]["ID"])&&($vals[$p]["attributes"]["CL"]>0))
                        {
                            $pp=$p;
                            if (isset($xnc_t_sp)) $vals[$p]["attributes"]["part.my_frez_after_edge"]=1;
                        }
                    }
                }
            }
        }
        $update_project = vals_index_to_project($vals);
        // echo $update_project;

        $project_data=gl_recalc(__LINE__,__FILE__,__FUNCTION__,$update_project);
        // p_($project_data);    
    
        // echo $project_data;
        $query1112="SELECT * FROM `tools`";
        $result1112=sql_data(__LINE__,__FILE__,__FUNCTION__,$query1112)['data'];

        
        foreach($result1112 as $tools_from_DB)
        {
            if ($tools_from_DB['plane']==0) $diameter_tools_from_DB['f'][strval($tools_from_DB['diameter'])]=strval($tools_from_DB['diameter']);
            if ($tools_from_DB['plane']==1) $diameter_tools_from_DB['l'][strval($tools_from_DB['diameter'])]=strval($tools_from_DB['diameter']);
            if ($tools_from_DB['plane']==2) $diameter_tools_from_DB['r'][strval($tools_from_DB['diameter'])]=strval($tools_from_DB['diameter']);
            if ($tools_from_DB['plane']==3) $diameter_tools_from_DB['t'][strval($tools_from_DB['diameter'])]=strval($tools_from_DB['diameter']);
            if ($tools_from_DB['plane']==4) $diameter_tools_from_DB['b'][strval($tools_from_DB['diameter'])]=strval($tools_from_DB['diameter']);
        }
        $vals_s = get_vals($project_data);
        $index_s = make_index($vals_s);
        // p_($vals_s);
        $rr_temp=array('l','r','t','b');
        

        foreach ($index["OPERATION"] as $i)
        {
            if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&($vals[$i]['type']=='open'))
            {
                if ((isset($vals[$i]["attributes"]["PROGRAM"]))&&($vals[$i]["attributes"]['operation.my_mpr_in']==1))
                {
                    unset ($id);
                    foreach ($vals[$i]['attributes'] as $k=>$v)
                    {
                        if (is_numeric(substr($k,17)))
                        {
                           $id=substr($k,17);
                        } 
                    }
                    $j=$i+1;
                    while ($vals[$j]['tag']<>"PART")
                    {
                        $j++;
                    }
                    $e4=part_get_material($vals, $vals[$j]["attributes"]["ID"]);
                    if (($id>0)&&(is_numeric($id)))
                    {
                        $sql='SELECT * FROM ticket_construct_mpr WHERE mpr_id='.$id;
                        $mpr=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                        // if ($mpr['by_size']==1)
                        // {
                        //     $vals[$i]['attributes'][mb_strtoupper('bySizeDetail')]='false';
                        //     $x_t=' /D/M/';
                        // }
                        // else
                        // {
                            $vals[$i]['attributes'][mb_strtoupper('bySizeDetail')]='true';
                            

                        // }
                        $vals[$i]['attributes'][mb_strtoupper('mirHor')]='false';
                        $vals[$i]['attributes'][mb_strtoupper('mirVert')]='true';
                        $vals[$i]['attributes'][mb_strtoupper('typeId')]='XNC';
                        if ($mpr['side']==0)
                        {
                            $vals[$i]['attributes'][mb_strtoupper('side')]='true';
                            $ss=' (лицо)';

                        }
                        else
                        {
                            $vals[$i]['attributes'][mb_strtoupper('side')]='false';
                            $ss=' (обратная)';
                        }
                        $vals[$i]['attributes'][mb_strtoupper('typename')]='!mpr_'.$mpr['mpr_id'].' деталь id='.$vals[$j]["attributes"]["ID"].$ss.' НЕ РЕДАКТИРОВАТЬ ';
                        $vals[$i]['attributes'][mb_strtoupper('printable')]='false';
                        $vals[$i]['attributes'][mb_strtoupper('startNewPage')]='false';
                        $vals[$i]['attributes'][mb_strtoupper('code')]='mpr_'.$mpr['mpr_id'];
                        $vals[$i]['attributes'][mb_strtoupper('turn')]=0;
                        $vals[$i]['attributes']['operation.my_mpr_'.$mpr['mpr_id']]=1;
                        foreach (SIDE as $s)
                        {
                            $vals[$i]['attributes']['operation.my_mpr_kl'.$s]=$mpr['kl'.$s];
                            // $vals[$j]=my_uncet($vals[$j].'my_xnc_side_'.$s);
                            if ($mpr['kl'.$s]==1)
                            {
                                $vals[$j]["attributes"]["MY_XNC_SIDE_".mb_strtoupper($s)]=1;
                                $part_xnc[$vals[$j]["attributes"]['ID']][$s]=1;
                            }
                        }
                        $vals[$i]['attributes']['operation.my_mpr_in']=1;

                        $xnc_pr=array(
                            array(
                                'tag'=>'PROGRAM','type'=>'open','level'=>1,'attributes'=>array(
                                'DX'=>$mpr['L'],
                                'DY'=>$mpr['W'],
                                'DZ'=>$e4['attributes']['T']
                            ),

                            )
                        );
                        if (!isset($mpr['file'])) $mpr['file']='без названия';
                        $xnc_pr[1]=
                            array(
                                'tag'=>'MSG','type'=>'complete','level'=>2,'attributes'=>array(
                                'MSG'=>"'НЕ РЕДАКТИРОВАТЬ! id = ".$mpr['mpr_id']." (".$mpr['L']."*".$mpr['W'].").'",
                                'TYPE'=>'info'
                            ),

                            );
                        $xnc_pr[2]=Array
                        (
                            "tag" => "PROGRAM",
                            "type" => "close",
                            "level" => 2,
                        );
                        // p_(vals_index_to_project($xnc_pr));exit;
                        $vals[$i]['attributes']['PROGRAM']=vals_index_to_project($xnc_pr);
                    }
                }
                elseif ((isset($vals[$i]["attributes"]["PROGRAM"]))&&($vals[$i]["attributes"]['operation.my_mpr_in']<>1))
                {
                    $vals[$i]["attributes"]["PROGRAM"]=str_replace('\\','',$vals[$i]["attributes"]["PROGRAM"]);
                    $p = xml_parser_create();
                    xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                    xml_parser_free($p);

                    $vals_s[$i]["attributes"]["PROGRAM"]=str_replace('\\','',$vals_s[$i]["attributes"]["PROGRAM"]);
                    $p = xml_parser_create();
                    xml_parse_into_struct($p, $vals_s[$i]["attributes"]["PROGRAM"], $v_n, $i_n);
                    xml_parser_free($p);
                    // p_($v_n);
                    $kl=$i+1;
                    if($vals[$i]["attributes"]["SIDE"]=='true') $sides='f';
                    else $sides='b';
                    $vals[$i]["attributes"]["CODE"]=($vals[$i]['attributes']['ID']/1).'_'.$sides;
                    while ($vals[$kl]['tag']<>"PART")
                    {
                        $kl++;
                    }
                    $e4=part_get_material($vals, $vals[$kl]["attributes"]["ID"]);
                    if (count($v_n)==1)
                    {
                        
                        // p_($v_n);
                        // p_($vals[$i]);
                        $_SESSION['error'].="<br>Деталь id=".$vals[$kl]["attributes"]["ID"]." (".$e4['attributes']['NAME']."), обработка XNC код ".$vals[$i]["attributes"]["CODE"]." (имя -".$vals[$i]["attributes"]["TYPENAME"].")
                        содержит ошибки или пустая. Исправьте или удалите данную обработку.<hr>";
                        // exit;
                        // echo $_SESSION['error'];
                    }
                    else
                    {
                        $v1[0]['attributes']['DZ']=$e4['attributes']['T'];
                    }
                    // if (isset($res['error_ok'])) p_($res);
                    if(($vals[$i]["attributes"]["CODE"]=="")AND(!isset($vals[$i]["attributes"]["CODE"]))AND(!is_integer($vals[$i]["attributes"]["CODE"])))
                    {
                        if($vals[$i]["attributes"]["SIDE"]=='true') $sides='f';
                        else $sides='b';
                        $vals[$i]["attributes"]["CODE"]=$vals[$i]['attributes']['ID'].'_'.$sides;
                    }
                    $name=$vals[$i]["attributes"]["TYPENAME"];
                    if (substr_count($vals[$i]["attributes"]["TYPENAME"],"|")>0)
                    {
                    
                        $name=substr($name,strpos($name,"|")+1);
                    
                        $vals[$i]["attributes"]["NAME"]=$name;
                        $vals[$i]["attributes"]["TYPENAME"]=$name;
                    }
                    else $name=$vals[$i]["attributes"]["TYPENAME"];
                    
                    if (substr_count($vals[$i]["attributes"]["TYPENAME"],"|")<1)
                    {
                        
                        foreach ($i1['CALL'] as $i2)
                        {
                            
                            if (isset($v1[$i2]['attributes']['NAME']))
                            {
                                // p_($v1[$i2]['attributes']['NAME']."++"); 
                                $tt=substr($v1[$i2]['attributes']['NAME'],strpos($v1[$i2]['attributes']['NAME'],'[/')+2,strpos($v1[$i2]['attributes']['NAME'],'/]')-strpos($v1[$i2]['attributes']['NAME'],'[/')+2);  
                                $xnc_s[$i2]=$tt/1;
                                $sql="SELECT * FROM xnc_template WHERE id=".$tt/1;
                                // echo $sql;
                                $xnc_s_t[$i2]=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                                
                            } 
                        }
                        if (count($xnc_s)>0)
                        {
                            // p_($xnc_s);
                            unset($vals[$i]["attributes"]["NAME"],$vals[$i]["attributes"]["TYPENAME"]);
                            $vals[$i]["attributes"]["NAME"]="Деталь (id=".$vals[$i+1]["attributes"]["ID"].") шабл. ".implode(',',$xnc_s)." | ".trim($name);
                            $vals[$i]["attributes"]["TYPENAME"]=$vals[$i]["attributes"]["NAME"];
                            $vals[$i]["attributes"]["operation.my_template"]=implode(',',$xnc_s);
                        }
                        else
                        {
                            $vals[$i]["attributes"]["NAME"]="Деталь (id=".$vals[$i+1]["attributes"]["ID"].") без шабл.";
                            $vals[$i]["attributes"]["TYPENAME"]=$vals[$i]["attributes"]["NAME"];
                        }
                        
                    }
                    foreach ($index['PART'] as $p)
                    {
                        if (($vals[$p]['attributes']['ID']==$vals[$i+1]["attributes"]["ID"])&&($vals[$p]["attributes"]["CL"]>0))
                        {
                            $pp=$p;
                        }
                    }
                    
                    $e4=part_get_material($vals,$vals[$pp]["attributes"]["ID"]);
                    // Корректировка от 17.05.2021
					// Старый формат строки
					// $ar_wt_xnc=array(84,85,86,87);
					// Предложенные корректировки от Кисиля Р.
					$ar_wt_xnc=array(84,85,86,87,90,94,118,119,132,133,134,135);
					
                    // $i1=make_index($v1);
                    foreach ($i1['TOOL'] as $t)
                    {
                        if ($v1[$t]['attributes']['NAME']=="MillN128") $v1[$t]['attributes']['D']=20;
                        // подмена диаметра фрезы с 10 на 20 
                    }
                    // p_($i1);
                    
                    foreach (SIDE_ALL as $s1)
                    {
                        $s1=mb_strtoupper($s1);
                        // p_($s1);
                        foreach ($i1['B'.$s1] as $i11)
                        {
                            foreach ($i1['TOOL'] as $t)
                            {
                                // p_($v1[$t]);
                                if ($v1[$t]['attributes']['NAME']==$v1[$i11]['attributes']['NAME']) $diam=$v1[$t]['attributes']['D'];
                                // подмена диаметра фрезы с 10 на 20 
                            }
                            // test ('d',$diam);
                            if ( $v1[$i11]['attributes']['DP']>36)
                            {
                                $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$vals[$pp]["attributes"]["L"]." * W ".$vals[$pp]["attributes"]["W"].")
                                глубина ".SIDE_T_ALL[mb_strtolower($s1)]." сверления ".$v1[$i11]['attributes']['DP']." мм не может быть более 36 мм. В программу сверления внесены изменения.
                                <hr>";
                                $v1[$i11]['attributes']['DP']=36;
                            }
                            elseif (($v1[$i11]['attributes']['DP']>4)&&($diam<=3))
                            {
                                $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$vals[$pp]["attributes"]["L"]." * W ".$vals[$pp]["attributes"]["W"].")
                                глубина ".SIDE_T_ALL[mb_strtolower($s1)]." сверления ".$v1[$i11]['attributes']['DP']." мм не может быть более 4 мм при диаметре 2мм. В программу сверления внесены изменения.
                                <hr>";
                                $v1[$i11]['attributes']['DP']=4;
                            }
                        }
                    }
                    foreach ($xnc_s as $xnc_temp)
                    {
                    
                        if (($e4['attributes']['ST']>0)&&(!in_array($xnc_temp,$ar_wt_xnc))&&(!isset($vals[$pp]['attributes']['part.my_postforming_cut_top'])))
                        {
                            // p_($e4);
                            $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$vals[$pp]["attributes"]["L"]." * W ".$vals[$pp]["attributes"]["W"].")
                            - шаблон № ".$xnc_temp." не предназначен для обработки деталей столешниц с закруглением.  
                            Используйте пожалуйста для столешниц шаблоны из соответствующей папки XNC обработок.<br>
                            При необходимости использовать шаблоны обработки листового материала на деталях столешницы с закруглением, необходимо в 
                            дополнительных операциях установить срез закругления столешницы.
                            <hr>";
                            // Нижние 2 строки раскоментировать позднее, когда решим с шаблонами.
                            // $error_xnc=1;
                            // unset($vals[$i],$vals[$i+1],$vals[$i+2]);
                        }
                        elseif (($e4['attributes']['ST']==0)&&(in_array($xnc_temp,$ar_wt_xnc)))
                        {
                            $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$vals[$pp]["attributes"]["L"]." * W ".$vals[$pp]["attributes"]["W"].")
                            - шаблон № ".$xnc_temp." не предназначен для обработки деталей листового материала, это шаблон для обработки столешниц.
                            Обработка ЧПУ удалена из проекта. 
                            <hr>";
                            $error_xnc=1;
                            unset($vals[$i],$vals[$i+1],$vals[$i+2]);
                        }
                    }
                    unset($xnc_s);

                    if (!isset($error_xnc))
                    {
                        
                        unset($cccheck,$ar_side);
                        if ((isset($i1['MS']))OR(isset($i1['MAC']))OR(isset($i1['ML']))OR(isset($i1['MA'])))
                        {
                            unset ($edge_not_check);
                            foreach ($i1['VAR'] as $u)
                            {
                                if (($v1[$u]['attributes']['COMMENT']=="[MY]")&&($v1[$u]['attributes']['EXPR']>0))
                                {
                                    if (substr($v1[$u]['attributes']['NAME'],0,1)<>"x")
                                    {
                                        $ar_side[substr($v1[$u]['attributes']['NAME'],0,1)]=1;
                                    }
                                    elseif (substr($v1[$u]['attributes']['NAME'],0,1)=="x")
                                    {
                                        $edge_not_check=1;
                                    }
                                }
                                elseif(($v1[$u]['attributes']['COMMENT']=="[MY]")&&($v1[$u]['attributes']['EXPR']==0)) $cccheck=1;
                                unset ($edge_xnc);
                                
                               
                            }
                            // p_($ar_side);
                            if ((!isset($ar_side))&&(!isset($cccheck)))
                            {
                                $ar_side['r']=1; $ar_side['b']=1; $ar_side['l']=1; $ar_side['t']=1;
                                $edge_not_check=1;
                                if ((isset($vals[$pp]["attributes"]["ELL"])&&(!isset($vals[$pp]["attributes"]["part.my_kl_ell"])))OR
                                (isset($vals[$pp]["attributes"]["ELR"])&&(!isset($vals[$pp]["attributes"]["part.my_kl_elr"])))OR
                                (isset($vals[$pp]["attributes"]["ELT"])&&(!isset($vals[$pp]["attributes"]["part.my_kl_elt"])))OR
                                (isset($vals[$pp]["attributes"]["ELB"])&&(!isset($vals[$pp]["attributes"]["part.my_kl_elb"]))))
                                {
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$vals[$pp]["attributes"]["L"]." * W ".$vals[$pp]["attributes"]["W"].")
                                    Так как в программе ЧПУ есть фрезерная обработка, заданная без использования стандартных шаблонов, кромкование для данной детали может быть
                                    использовано только криволинейное. <br> При использовании стандартных шаблонов криволинейное кромкование используется только по сторонам, затрагиваемым фрезером.
                                    <hr>";
                                    
                                    // $update_project = vals_index_to_project($vals);
                                    // $response = send_post_data_curl($update_project, '4EC657AF');
                                    // $array = json_decode($response, true);
                                    // if (!isset($array['project'])) {
                                    //     $project_data = $update_project;
                                    //     $message['curl'] = "Данные не прошли пересчет ///4EC657AF";
                                    //     echo json_encode($message);
                                    //     echo $response;
                                    //     exit;
                                    // } else {
                                    //     $project_data = $array['project'];
                                    // }
                                    // $new_vals = get_vals_index($project_data);
                                    // $vals_s = vals_out($new_vals['vals']);
                                    // $index_s = make_index($new_vals['vals']);
                                    // unset($change);
                                    // $p = xml_parser_create();
                                    // xml_parse_into_struct($p, $vals_s[$i]["attributes"]["PROGRAM"], $v1, $i1);
                                    // xml_parser_free($p);
                                    
                                    // $dx=$v1[0]['attributes']['DX'];
                                    // $dy=$v1[0]['attributes']['DY'];
                                    // foreach($v1 as $b)
                                    // {
                                        
                                    //     $tochka=0;
                                    //     if (($b["tag"]=="ML")OR($b["tag"]=="MS")OR($b["tag"]=="MAC"))
                                    //     {
                                    //         $x=$b['attributes']['X'];
                                    //         $y=$b['attributes']['Y'];
                                    //         // p_($vals_s[$i+1]['attributes']['ID']);
                                    //         // p_($b);
                                    //         if ((($x==0)OR($x==$dx))AND(($y==0)OR($y==$dy)))
                                    //         {
                                    //             //угловая точка
                                    //         }
                                    //         else
                                    //         {
                                    //             if (($x<$dx)AND($x>0)AND($y==0))
                                    //             {
                                    //                 $ar_side['b']=1;
                                    //             }
                                    //             elseif(!isset( $ar_side['b'])) $ar_side['b']=0;
                                    //             if (($x<$dx)AND($x>0)AND($y==$dy))
                                    //             {
                                    //                 $ar_side['t']=1;
                                    //             }
                                    //             elseif(!isset( $ar_side['t'])) $ar_side['t']=0;
                                    //             if (($y<$dy)AND($y>0)AND($x==0)) 
                                    //             {
                                    //                 $ar_side['l']=1;
                                    //             }
                                    //             elseif(!isset( $ar_side['l'])) $ar_side['l']=0;
                                    //             if (($y<$dy)AND($y>0)AND($x==$dx)) 
                                    //             {
                                    //                 $ar_side['r']=1;
                                    //             }
                                    //             elseif(!isset( $ar_side['r'])) $ar_side['r']=0;
                                                
                                    //         }
                                        // }
                                    // }
                                }
                            }
                        }
                        // if ($ar_side['r']+$ar_side['l']+$ar_side['t']+$ar_side['b']==1)
                        // {
                        //     $ar_side['r']=1; $ar_side['b']=1; $ar_side['l']=1; $ar_side['t']=1;
                        //     // p_($ar_side);
                        // }
                        
                        // if (($ar_side['r']>0)&&($ar_side['l']>0)&&($ar_side['b']==0)&&($ar_side['t']==0))
                        // {
                        //     $ar_side['r']=1; $ar_side['b']=1; $ar_side['l']=1; $ar_side['t']=1;
                        // }
                        // elseif (($ar_side['r']==0)&&($ar_side['l']==0)&&($ar_side['b']>0)&&($ar_side['t']>0))
                        // {
                        //     $ar_side['r']=1; $ar_side['b']=1; $ar_side['l']=1; $ar_side['t']=1;
                        // }
                        
                        // if ($vals[$i]["attributes"]["TURN"]==0) 
                        // { 
                        if(isset($ar_side['l'])) $xnc_side['l']=$ar_side['l'];
                        if(isset($ar_side['t']))$xnc_side['t']=$ar_side['t'];
                        if(isset($ar_side['r']))$xnc_side['r']=$ar_side['r'];
                        if(isset($ar_side['b']))$xnc_side['b']=$ar_side['b'];
                        // }   
                        // elseif ($vals[$i]["attributes"]["TURN"]==1)
                        // {
                        // // p_($ar_side);
                        //     if(isset($ar_side['b']))$xnc_side['l']=$ar_side['b'];
                        //     if(isset($ar_side['l']))$xnc_side['t']=$ar_side['l'];
                        //     if(isset($ar_side['t']))$xnc_side['r']=$ar_side['t'];
                        //     if(isset($ar_side['r']))$xnc_side['b']=$ar_side['r'];
                        //     // p_($xnc_side);

                        // }
                        // elseif ($vals[$i]["attributes"]["TURN"]==2)
                        // {
                        //     if(isset($ar_side['r']))$xnc_side['l']=$ar_side['r'];
                        //     if(isset($ar_side['b']))$xnc_side['t']=$ar_side['b'];
                        //     if(isset($ar_side['l']))$xnc_side['r']=$ar_side['l'];
                        //     if(isset($ar_side['t']))$xnc_side['b']=$ar_side['t'];
                        // }
                        // elseif ($vals[$i]["attributes"]["TURN"]==3)
                        // { 
                        //     if(isset($ar_side['t']))$xnc_side['l']=$ar_side['t'];
                        //     if(isset($ar_side['r']))$xnc_side['t']=$ar_side['r'];
                        //     if(isset($ar_side['b']))$xnc_side['r']=$ar_side['b'];
                        //     if(isset($ar_side['l']))$xnc_side['b']=$ar_side['l'];
                        // }
                        unset($text,$el_op,$el_name,$el_my_kl,$el_my_kl_code);
                        // p_($xnc_side);
                        foreach ($xnc_side as $sss=>$sssssssss)
                        {
                            $text[$sss]=SIDE_T[$sss];
                            if (isset($vals[$pp]["attributes"]["EL".mb_strtoupper($sss)]))
                            {
                                $edge_xnc[$sss]=1;
                                $el_op=$vals[$pp]["attributes"]["EL".mb_strtoupper($sss)];
                                $el_name=$vals[$pp]["attributes"]["EL".mb_strtoupper($sss).'MAT'];
                                if (isset($vals[$pp]["attributes"]["part.my_kl_el".$sss])) $el_my_kl[$sss]=$vals[$pp]["attributes"]["part.my_kl_el".$sss];
                                if (isset($vals[$pp]["attributes"]["part.my_kl_el".$sss."_code"])) $el_my_kl_code[$sss]=$vals[$pp]["attributes"]["part.my_kl_el".$sss."_code"];
                            }
                        }
                        if ((isset ($edge_xnc))&&(!isset($edge_not_check)))
                        {
                            // $get_edges=part_get_edges($vals,$vals[$pp]["attributes"]['ID']);
                            foreach ($xnc_side as $sss=>$sssssssss)
                            {
                                if (!isset($edge_xnc[$sss]))
                                {
                                    $vals[$pp]["attributes"]["EL".mb_strtoupper($sss)]=$el_op;
                                    if (isset($el_my_kl[$sss])) $vals[$pp]["attributes"]["part.my_kl_el".$sss]=$el_my_kl[$sss];
                                    if (isset($el_my_kl_code[$sss])) $vals[$pp]["attributes"]["part.my_kl_el".$sss."_code"]=$el_my_kl_code[$sss];
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$vals[$pp]["attributes"]["L"]." * W ".$vals[$pp]["attributes"]["W"].")
                                    - шаблон № ".$xnc_temp." предполагает кромкование (".implode(',',$text).").
                                    Добавлена кромка для кромкования (".SIDE_T[$sss].") - ".$el_name."
                                    <hr>";
                                }
                            }
                        }
                        foreach($xnc_side as $h=>$f)
                        {
                            if ($f>0)
                            {
                                $vals[$pp]["attributes"]["MY_XNC_SIDE_".mb_strtoupper($h)]=1;
                                $part_xnc[$vals[$pp]["attributes"]['ID']][$h]=1;

                            }
                        }
                        // p_($vals[$pp]);
                        // p_($xnc_side);
                        // if ($ar_side)
                        //     {
                        //         p_($vals[$pp]);
                        //         p_($ar_side);
                        //     }
                        unset($change,$ar_side,$xnc_side);
                        // if (isset($vals[$pp]['attributes']['part.my_side_to_cut_l']))$cl=$vals[$pp]['attributes']['part.my_side_to_cut_l'];
                        // elseif (isset($vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_l')]))$cl=$vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_l')];
                        // elseif (isset($vals[$pp]['attributes']['part.my_side_to_cut_r']))$cl=$vals[$pp]['attributes']['part.my_side_to_cut_r'];
                        // elseif (isset($vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_r')]))$cl=$vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_r')];
                        // else $cl=$vals[$pp]['attributes']['CL'];
                        // if (isset($vals[$pp]['attributes']['part.my_side_to_cut_t']))$cw=$vals[$pp]['attributes']['part.my_side_to_cut_t'];
                        // elseif (isset($vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_t')]))$cw=$vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_t')];
                        // elseif (isset($vals[$pp]['attributes']['part.my_side_to_cut_b']))$cw=$vals[$pp]['attributes']['part.my_side_to_cut_b'];
                        // elseif (isset($vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_b')]))$cw=$vals[$pp]['attributes'][mb_strtoupper('part.my_side_to_cut_b')];
                        // else $cw=$vals[$pp]['attributes']['CW'];
                        // p_($v1[$i1['MR'][0]]);
                        
                        
                       
                        $cl=$vals[$pp]['attributes']['CL'];
                        $cw=$vals[$pp]['attributes']['CW'];

                       unset($deltax,$deltay);
                       foreach ($i1['GR'] as $gr)
                       {
                           if ((is_numeric($v1[$gr]['attributes']['T']))&&($v1[$gr]['attributes']['T']<4))
                           {
                                $v1[$gr]['attributes']['T']=4;
                                $change=1;
                           }
                       }
                       unset ($need_ch,$fr_in);
                       if ((!$i1["MS"])AND(!$i1["ML"])AND(!$i1["MA"])AND(!$i1["MAC"])AND(!$i1["GR"]))
                        {
                            $lxnc=200;
                            $wxnc=70;
                            $fr_in=1;
                        }
                        elseif (($i1["MS"])OR($i1["ML"])OR($i1["MA"])OR($i1["MAC"])OR($i1["GR"]))
                        {
                            $lxnc=350;
                            $wxnc=150;
                            if (isset($xnc_s_t))
                            {
                                foreach ($xnc_s_t as $xnc)
                                {
                                    if ($xnc['W']<$wxnc) $wxnc=$xnc['W'];
                                    if ($xnc['L']<$lxnc) $lxnc=$xnc['L'];
                                    $vals[$pp]['attributes']['part.my_xnc_w']=$wxnc;
                                    $vals[$pp]['attributes']['part.my_xnc_l']=$lxnc;
                                }
                            }
                            unset ($xnc_s_t);
                            
                            if (!isset($lxnc)) $lxnc=350;
                            if (!isset($wxnc)) $wxnc=150;
                        }
                        
                        // if(mr_change ($v1,$vals[$i])<>false) $v1=mr_change ($v1,$vals[$i]);
                        //.. 26.08.2021 В условиях добавлены условия с +1 (компенсация урезки детали на размер кромки(?))
                        if  (($cl<$lxnc)&&($cw>=$wxnc || ($cw+1)>=$wxnc)) {
                                if (($cl>=$wxnc)&&($cw>=$lxnc)) {}
                                else $need_ch=1;
                        }
                        elseif (($cl>=$lxnc || ($cl+1)>=$lxnc)&&($cw<$wxnc)) {
                            if (($cl>=$wxnc)&&($cw>=$lxnc)) {}
                            else  $need_ch=2;
                        }
                        elseif ($cl<$lxnc || $cw<$wxnc) {
                            $need_ch=3;
                        }
                       
                        
                        if ($need_ch>0)
                        {
                            $p_r=get_all_part ($vals,$vals[$pp]['attributes']['ID']);
                            $p_r_[$vals[$pp]['attributes']['ID']][]=1;

                        }
                        else unset ($p_r);
                        if ((count($p_r['xnc'])>1)&&(count( $p_r_[$vals[$pp]['attributes']['ID']])==1))
                        {
                            $_SESSION['error'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                            - недопустимый размер для обработки ЧПУ. Деталь должна быть увеличена и урезана в размер после обработки. Это возможно если на детали есть
                            только одна обработка. На этой детали ".count($p_r['xnc'])."  обработок. Уменьшите их количество.<hr>
                           ";
                        }
                            //  }
                        if (($need_ch>0)&&(!isset($part_can_not_be_changed[$vals[$pp]['attributes']['ID']]))&&(count($p_r['xnc'])==1))    
                        {  
                            //.. Получаем минимальный размер
                            $minSizeXnc = ($wxnc < $lxnc) ? $wxnc : $lxnc;
                            $minSideSize = ($vals[$pp]['attributes']['CL'] < $vals[$pp]['attributes']['CW']) ? $vals[$pp]['attributes']['CL'] : $vals[$pp]['attributes']['CW'];

                            //.. 26.08.2021 В условии добавлено условие с +1 (компенсация урезки детали на размер кромки(?))
                            if(($vals[$pp]['attributes']['CL']<$lxnc)&&($vals[$pp]['attributes']['CW']>=$wxnc || ($vals[$pp]['attributes']['CW']+1)>=$wxnc))
                            {
                            
                                if((!isset($i1["BL"]))&&((!$vals[$pp]["attributes"]["MY_XNC_SIDE_L"])OR(($fr_in>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_L"]))))
                                {
                                    // p_($v1);    
                                    
                                    $vals[$pp]['attributes']['part.my_side_to_cut_d_r']=$vals[$pp]['attributes']['DL'];
                                    $thikness_x=get_thikness_edge($vals,$vals[$pp]['attributes']['ID'],'dl');
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_L'])&&($vals[$pp]['attributes']['ELL']<>''))$thikness_x++;
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_R'])&&($vals[$pp]['attributes']['ELR']<>''))$thikness_x++;
                                    $vals[$pp]['attributes']['part.my_side_to_cut_r']=$vals[$pp]['attributes']['DL']-$thikness;
                                    $vals[$pp]['attributes']['CL']=$lxnc+10;
                                    $deltax=$vals[$pp]['attributes']['CL']-$vals[$pp]['attributes']['part.my_side_to_cut_r'];
                                    $vals[$pp]['attributes']['L']+=$deltax;
                                    $vals[$pp]['attributes']['DL']+=$deltax;
                                    $vals[$pp]['attributes']['JL']+=$deltax;
                                    $vals[$pp]['attributes'][mb_strtoupper('my_xnc_cut_deltax')]=$deltax;
                                    $vals[$pp]['attributes']['part.my_xnc_cut_deltax']=$deltax;
                                                                                                                                                                                                        
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                    - недопустимый размер.
                                    Так как в программе ЧПУ есть сверление, размер CL детали должен быть минимум ".$vals[$pp]['attributes']['CL']." мм с учётом ширины пилы.
                                    Свойства детали были скорректированы. Размер CL установлен ".$vals[$pp]['attributes']['CL']." мм. После обработки на ЧПУ деталь будет прирезана в размер.<hr>";
                                    $change=1;
                                    
                                }
                                elseif((!isset($i1["BR"]))&&((!$vals[$pp]["attributes"]["MY_XNC_SIDE_R"])OR(($fr_in>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_R"]))))
                                {
                                    
                                    $vals[$pp]['attributes']['part.my_side_to_cut_d_l']=$vals[$pp]['attributes']['DL'];
                                    $thikness_x=get_thikness_edge($vals,$vals[$pp]['attributes']['ID'],'dl');
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_L'])&&($vals[$pp]['attributes']['ELL']<>''))$thikness_x++;
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_R'])&&($vals[$pp]['attributes']['ELR']<>''))$thikness_x++;
                                    $vals[$pp]['attributes']['part.my_side_to_cut_l']=$vals[$pp]['attributes']['DL']-$thikness;
                                    $vals[$pp]['attributes']['CL']=$lxnc+10;
                                    $delta=$vals[$pp]['attributes']['CL']-$vals[$pp]['attributes']['part.my_side_to_cut_l'];
                                    $vals[$pp]['attributes']['L']+=$delta;
                                    $vals[$pp]['attributes']['DL']+=$delta;
                                    $vals[$pp]['attributes']['JL']+=$delta;
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                    - недопустимый размер.
                                    Так как в программе ЧПУ есть сверление, размер CL детали должен быть минимум ".$vals[$pp]['attributes']['CL']." мм с учётом ширины пилы.
                                    Свойства детали были скорректированы. Размер CL установлен ".$vals[$pp]['attributes']['CL']." мм. После обработки на ЧПУ деталь будет прирезана в размер.<hr>";
                                    $change=2;
                                
                                }
                                elseif(
                                    (((isset($i1["BR"]))OR(($vals[$pp]["attributes"]["MY_XNC_SIDE_R"])))
                                    AND
                                    ((isset($i1["BL"]))OR(($vals[$pp]["attributes"]["MY_XNC_SIDE_L"]))))
                                    AND
                                    ((($vals[$i]["attributes"]["TURN"]==0)OR($vals[$i]["attributes"]["TURN"]==2)))
                                    AND
                                    ((((!isset($i1["BT"])&&($vals[$i]['attributes']['MIRVERT']=="true"))OR((!isset($i1["BB"])&&($vals[$i]['attributes']['MIRVERT']=="false"))))
                                    &&((!$vals[$pp]["attributes"]["MY_XNC_SIDE_B"])OR(($fr_in>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_B"]))))
                                    )AND($vals[$pp]['attributes']['CW']<$lxnc))
                                {
                                    // exit('11');
                                    // if (($vals[$pp]['attributes']['ID']==89379))
                                    // {
                                    //     p_($vals[$pp]['attributes']);
            
                                    // } 

                                    // $vals[$i]["attributes"]["TURN"]=3;
                                    $vals[$pp]['attributes']['part.my_side_to_cut_d_t']=$vals[$pp]['attributes']['DW'];
                                    $thikness_y=get_thikness_edge($vals,$vals[$pp]['attributes']['ID'],'dw');
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_L'])&&($vals[$pp]['attributes']['ELL']<>''))$thikness_y++;
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_R'])&&($vals[$pp]['attributes']['ELR']<>''))$thikness_y++;
                                    $vals[$pp]['attributes']['part.my_side_to_cut_t']=$vals[$pp]['attributes']['DW']-$thikness;
                                    if ($lxnc+10-$vals[$pp]['attributes']['CW']<5) $vals[$pp]['attributes']['CW']=$lxnc+10;
                                    else $vals[$pp]['attributes']['CW']=$lxnc+5;
                                    $deltayt=$vals[$pp]['attributes']['CW']-$vals[$pp]['attributes']['part.my_side_to_cut_t'];
                                    $vals[$pp]['attributes']['W']+=$deltayt;
                                    $vals[$pp]['attributes']['DW']+=$deltayt;
                                    $vals[$pp]['attributes']['JW']+=$deltayt;
                                    $vals[$pp]['attributes'][mb_strtoupper('my_xnc_cut_deltay')]=$deltayt;
                                    $vals[$pp]['attributes']['part.my_xnc_cut_deltay']=$deltayt;

                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                    - недопустимый размер.
                                    Так как в программе ЧПУ есть сверление, размер CW детали должен быть минимум ".$vals[$pp]['attributes']['CW']." мм с учётом ширины пилы.
                                    Свойства детали были скорректированы. Размер CW установлен ".$vals[$pp]['attributes']['CW']." мм. После обработки на ЧПУ деталь будет прирезана в размер.<hr>";
                                    $change=5;
                                    // if (($vals[$pp]['attributes']['ID']==89379))
                                    // {
                                    //     p_($vals[$pp]['attributes']);
            
                                    // } 
                                    // p_($vals[$pp]);
                                }
                                elseif(
                                    (((isset($i1["BR"]))OR(($vals[$pp]["attributes"]["MY_XNC_SIDE_R"])))
                                    AND
                                    ((isset($i1["BL"]))OR(($vals[$pp]["attributes"]["MY_XNC_SIDE_L"]))))
                                    AND
                                    ((($vals[$i]["attributes"]["TURN"]==0)OR($vals[$i]["attributes"]["TURN"]==2)))
                                    AND
                                    ((((!isset($i1["BB"])&&($vals[$i]['attributes']['MIRVERT']=="true"))OR((!isset($i1["BT"])&&($vals[$i]['attributes']['MIRVERT']=="false"))))
                                    &&((!$vals[$pp]["attributes"]["MY_XNC_SIDE_T"])OR(($fr_in>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_T"]))))
                                    )AND($cw<$wxnc))
                                {
                                    // p_($v1);
                                    
                                    $vals[$pp]['attributes']['part.my_side_to_cut_d_b']=$vals[$pp]['attributes']['DW'];
                                    $thikness=get_thikness_edge($vals,$vals[$pp]['attributes']['ID'],'dw');
                                    // p_($thikness);

                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_L'])&&($vals[$pp]['attributes']['ELL']<>''))$thikness++;
                                    // p_($thikness);

                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_R'])&&($vals[$pp]['attributes']['ELR']<>''))$thikness++;
                                    // p_($thikness);
                                    $vals[$pp]['attributes']['part.my_side_to_cut_b']=$vals[$pp]['attributes']['DW']-$thikness;
                                    if ($lxnc+10-$vals[$pp]['attributes']['CW']<5) $vals[$pp]['attributes']['CW']=$lxnc+10;
                                    else $vals[$pp]['attributes']['CW']=$lxnc+5;
                                    $deltayb=$vals[$pp]['attributes']['CW']-$vals[$pp]['attributes']['part.my_side_to_cut_b'];
                                    $vals[$pp]['attributes']['W']+=$deltayb;
                                    $vals[$pp]['attributes']['DW']+=$deltayb;
                                    $vals[$pp]['attributes']['JW']+=$deltayb;
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                    - недопустимый размер.
                                    Так как в программе ЧПУ есть сверление, размер CW детали должен быть минимум ".$vals[$pp]['attributes']['CW']." мм с учётом ширины пилы.
                                    Свойства детали были скорректированы. Размер CW установлен ".$vals[$pp]['attributes']['CW']." мм. После обработки на ЧПУ деталь будет прирезана в размер.<hr>";
                                    $change=6;

                                }
                                // if (($vals[$i]["attributes"]["TURN"]<>1)&&($vals[$i]["attributes"]["TURN"]<>3)) $v1[0]["attributes"]["DX"]=$vals[$pp]["attributes"]["CL"];
                                // else  $v1[0]["attributes"]["DX"]=$vals[$pp]["attributes"]["CW"];
                            }
                            //.. 26.08.2021 В условии добавлено условие с +1 (компенсация урезки детали на размер кромки(?))
                            elseif(($vals[$pp]['attributes']['CW']<$wxnc)&&($vals[$pp]['attributes']['CL']>=$lxnc || ($vals[$pp]['attributes']['CL']+1)>=$lxnc))
                            {
                                
                                    // p_($vals[$i]);
                                
                                if(((!isset($i1["BT"])&&($vals[$i]['attributes']['MIRVERT']=="true"))OR(!isset($i1["BB"])&&($vals[$i]['attributes']['MIRVERT']=="false")))
                                &&
                                (((!$vals[$pp]["attributes"]["MY_XNC_SIDE_B"])OR(($fr_in>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_B"])))))
                                {
                                    // if($vals[$i]["attributes"]["CODE"]=='122x1x1') p_($v1);
                                    // p_($fr_in);
                                    // p_($vals[$pp]);
                                    
                                    $vals[$pp]['attributes']['part.my_side_to_cut_d_t']=$vals[$pp]['attributes']['DW'];
                                    $thikness=get_thikness_edge($vals,$vals[$pp]['attributes']['ID'],'dw');
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_T'])&&($vals[$pp]['attributes']['ELT']<>''))$thikness++;
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_B'])&&($vals[$pp]['attributes']['ELB']<>''))$thikness++;
                                    $vals[$pp]['attributes']['part.my_side_to_cut_t']=$vals[$pp]['attributes']['DW']-$thikness;
                                    if ($wxnc+10-$vals[$pp]['attributes']['CL']<5) $vals[$pp]['attributes']['CW']=$wxnc+10;
                                    else $vals[$pp]['attributes']['CW']=$wxnc+5;
                                    $deltayt=$vals[$pp]['attributes']['CW']-$vals[$pp]['attributes']['part.my_side_to_cut_t'];
                                    $vals[$pp]['attributes']['W']+=$deltayt;
                                    $vals[$pp]['attributes']['DW']+=$deltayt;
                                    $vals[$pp]['attributes']['JW']+=$deltayt;
                                    $vals[$pp]['attributes'][mb_strtoupper('my_xnc_cut_deltay')]=$deltay;
                                    $vals[$pp]['attributes']['part.my_xnc_cut_deltay']=$deltayt;

                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                    - недопустимый размер.
                                    Так как в программе ЧПУ есть сверление, размер CW детали должен быть минимум ".$vals[$pp]['attributes']['CW']." мм с учётом ширины пилы.
                                    Свойства детали были скорректированы. Размер CW установлен ".$vals[$pp]['attributes']['CW']." мм. После обработки на ЧПУ деталь будет прирезана в размер.<hr>";
                                    $change=3;
                                    
                                }
                                elseif(((!isset($i1["BB"])&&($vals[$i]['attributes']['MIRVERT']=="true"))OR(!isset($i1["BT"])&&($vals[$i]['attributes']['MIRVERT']=="false")))
                                &&(((!$vals[$pp]["attributes"]["MY_XNC_SIDE_T"])OR(($fr_in>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_T"])))))
                                {
                                    $vals[$pp]['attributes']['part.my_side_to_cut_d_b']=$vals[$pp]['attributes']['DW'];
                                    $thikness=get_thikness_edge($vals,$vals[$pp]['attributes']['ID'],'dw');
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_T'])&&($vals[$pp]['attributes']['ELT']<>''))$thikness++;
                                    if (($vals[$pp]['attributes']['MY_XNC_SIDE_B'])&&($vals[$pp]['attributes']['ELB']<>''))$thikness++;
                                    $vals[$pp]['attributes']['part.my_side_to_cut_b']=$vals[$pp]['attributes']['DW']-$thikness;
                                    if ($wxnc+10-$vals[$pp]['attributes']['CL']<5) $vals[$pp]['attributes']['CW']=$wxnc+10;
                                    else $vals[$pp]['attributes']['CW']=$wxnc+5;
                                    $deltayb=$vals[$pp]['attributes']['CW']-$vals[$pp]['attributes']['part.my_side_to_cut_b'];
                                    $vals[$pp]['attributes']['W']+=$deltayb;
                                    $vals[$pp]['attributes']['DW']+=$deltayb;
                                    $vals[$pp]['attributes']['JW']+=$deltayb;
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                    - недопустимый размер.
                                    Так как в программе ЧПУ есть сверление, размер CW детали должен быть минимум ".$vals[$pp]['attributes']['CW']." мм с учётом ширины пилы.
                                    Свойства детали были скорректированы. Размер CW установлен ".$vals[$pp]['attributes']['CW']." мм. После обработки на ЧПУ деталь будет прирезана в размер.<hr>";
                                    $change=4;
                                
                                }
                                
                                
                                
                                

                            }
                            elseif(((($vals[$pp]['attributes']['CW']<$wxnc))&&(($vals[$pp]['attributes']['CL']<$lxnc)))&&(isset($vals[$pp]['attributes']['CW'])))
                            {
                                $_SESSION['error'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                - недопустимый размер. Так как в программе ЧПУ есть сверление и/или фрезерование, размер детали должен быть минимум ".$lxnc."*".$wxnc." мм.
                                Если один из размеров достаточен, деталь при технической возможности может быть увеличена по второй стороне, обработана, потом урезана.
                                В данной детали оба размера недостаточны.
                                <hr>";
                            }

                            if(!isset($change)&&(($vals[$pp]["attributes"]["MY_XNC_SIDE_L"]>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_R"]>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_T"]>0)&&($vals[$pp]["attributes"]["MY_XNC_SIDE_B"]>0)) && ($vals[$pp]["attributes"]["CL"] < 330 && $vals[$pp]["attributes"]["CW"] < 330))
                            {
                                // echo '1111';
                                $_SESSION['error'].="<br>Деталь '".$vals[$pp]["attributes"]["NAME"]."' (id='".$vals[$pp]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$cl." * W ".$cw.")
                                Размер детали должен быть минимум ".$lxnc."*".$wxnc." мм для выбранной обработки на ЧПУ.
                                Сейчас по всем сторонам детали установлен признак обработки фрезером. 
                                Скорее всего это значит, что не использованы стандартные шаблоны. Размер детали не может быть увеличен для обработки на ЧПУ. Измените фрезерование на стандартное и повторите процедуру.
                                Деталь будет увеличена.
                                <hr>";
                            }
                        }
                        // if (!isset($fr_in)) $vals[$i]["attributes"]["BYSIZEDETAIL"]='true';
                        // else $vals[$i]["attributes"]["BYSIZEDETAIL"]='false';
                        if(($change>0))
                        {
                                $p = xml_parser_create();
                                xml_parse_into_struct($p, $vals[$i]["attributes"]["PROGRAM"], $v1, $i1);
                                xml_parser_free($p);
                               unset ($fr_inside);
                                $kl=$i+1;
                                while ($vals[$kl]['tag']<>"PART")
                                {
                                    $kl++;
                                }
                                $e4=part_get_material($vals, $vals[$kl]["attributes"]["ID"]);
                                $v1[0]['attributes']['DZ']=$e4['attributes']['T'];
                                
                                foreach ($v1 as $kr=>$v3)
                                {
                                    if ((!$i1["MS"])AND(!$i1["ML"])AND(!$i1["MA"])AND(!$i1["MAC"])AND(!$i1["GR"]))
                                    {
                                        if($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_L')]>0) $dx1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_L')];
                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_R')]>0) $dx1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_R')];

                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_T')]>0) $dy1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_T')];

                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_B')]>0) $dy1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_d_B')];
                                    }
                                    else
                                    {
                                        if ($vals[$pp]['attributes']['MY_SIDE_TO_CUT_L']>0) $dx1=$vals[$pp]['attributes']['MY_SIDE_TO_CUT_L'];
                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_L')]>0) $dx1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_L')];
                                        elseif ($vals[$pp]['attributes']['MY_SIDE_TO_CUT_R']>0) $dx1=$vals[$pp]['attributes']['MY_SIDE_TO_CUT_R'];
                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_R')]>0) $dx1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_R')];

                                        if ($vals[$pp]['attributes']['MY_SIDE_TO_CUT_T']>0) $dy1=$vals[$pp]['attributes']['MY_SIDE_TO_CUT_T'];
                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_T')]>0) $dy1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_T')];

                                        elseif ($vals[$pp]['attributes']['MY_SIDE_TO_CUT_B']>0) $dy1=$vals[$pp]['attributes']['MY_SIDE_TO_CUT_B'];
                                        elseif($vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_B')]>0) $dy1=$vals[$pp]['attributes']['part.'.mb_strtolower('MY_SIDE_TO_CUT_B')];
                                    }
                                    if (($v3['tag']=='BF')OR($v3['tag']=='BL')OR($v3['tag']=='BR')OR($v3['tag']=='BT')OR($v3['tag']=='BB')OR($v3['tag']=='MS')OR($v3['tag']=='ML')OR($v3['tag']=='MAC'))
                                    {
                                        if (isset($v3['attributes']['X'])&&($deltax>0))
                                        {
                                            if ($vals_s[$i]["attributes"]["MIRHOR"]=='false')
                                            {
                                                $v1[$kr]['attributes']['X'].='+'.$deltax;
                                            } 
                                            $v1[$kr]['attributes']['X']=str_replace('dx',$dx1,$v1[$kr]['attributes']['X']);
                                            $ch_xnc=1;
                                        }
                                        if (isset($v3['attributes']['Y'])&&($deltayt>0))
                                        {
                                            // if($vals[$i]["attributes"]["CODE"]=='122x1x1') p_($v3);
                                            if ($vals_s[$i]["attributes"]["MIRVERT"]=='true') 
                                            {
                                                $v1[$kr]['attributes']['Y'].='+'.$deltayt;
                                            }
                                            $v1[$kr]['attributes']['Y']=str_replace('dy',$dy1,$v1[$kr]['attributes']['Y']);
                                            $ch_xnc=1;
                                            // if($vals[$i]["attributes"]["CODE"]=='122x1x1'){p_($v3);}

                                        }
                                        if (isset($v3['attributes']['CY'])&&($deltayt>0))
                                        {
                                            if ($vals_s[$i]["attributes"]["MIRVERT"]=='true') 
                                            {
                                                $v1[$kr]['attributes']['CY'].='+'.$deltayt;
                                            }
                                            $v1[$kr]['attributes']['CY']=str_replace('dy',$dy1,$v1[$kr]['attributes']['CY']);
                                            $ch_xnc=1;
                                        }
                                        if (isset($v3['attributes']['Y'])&&($deltayb>0))
                                        {
                                            if ($vals_s[$i]["attributes"]["MIRVERT"]=='false') 
                                            {
                                                $v1[$kr]['attributes']['Y'].='+'.$deltayb;
                                            }
                                            $v1[$kr]['attributes']['Y']=str_replace('dy',$dy1,$v1[$kr]['attributes']['Y']);
                                            $ch_xnc=1;
                                        }
                                        if (isset($v3['attributes']['CX'])&&($deltax>0))
                                        {
                                            if ($vals_s[$i]["attributes"]["MIRHOR"]=='false') 
                                            {
                                                $v1[$kr]['attributes']['CX'].='+'.$deltax;
                                            }
                                            $v1[$kr]['attributes']['CX']=str_replace('dx',$dx1,$v1[$kr]['attributes']['CX']);
                                            $ch_xnc=1;
                                        }
                                        
                                        if (isset($v3['attributes']['CY'])&&($deltayb>0))
                                        {
                                            if ($vals_s[$i]["attributes"]["MIRVERT"]=='false') 
                                            {
                                                $v1[$kr]['attributes']['CY'].='+'.$deltayb;
                                            }
                                            $v1[$kr]['attributes']['CY']=str_replace('dy',$dy1,$v1[$kr]['attributes']['CY']);
                                            $ch_xnc=1;
                                        }
                                    }
                                    elseif ($v3['tag']=="GR") 
                                    {
                                        if (($deltax>0) && ($vals_s[$i]["attributes"]["MIRHOR"]=='false'))
                                        {
                                           $v1[$kr]['attributes']['X1'].='+'.$deltax;
                                           $v1[$kr]['attributes']['X1']=str_replace('dx',$dx1,$v1[$kr]['attributes']['X1']);
                                            $v1[$kr]['attributes']['X2'].='+'.$deltax;
                                           $v1[$kr]['attributes']['X2']=str_replace('dx',$dx1,$v1[$kr]['attributes']['X2']);
                                            $ch_xnc=1;
                                        }
                                        elseif (($deltayt>0)&&($vals_s[$i]["attributes"]["MIRVERT"]=='true'))
                                        {
                                             $v1[$kr]['attributes']['Y1'].='+'.$deltayt;
                                             $v1[$kr]['attributes']['Y1']=str_replace('dy',$dy1,$v1[$kr]['attributes']['Y1']);
                                             $v1[$kr]['attributes']['Y2'].='+'.$deltayt;
                                             $v1[$kr]['attributes']['Y2']=str_replace('dy',$dy1,$v1[$kr]['attributes']['Y2']);
                                            $ch_xnc=1;
                                        }
                                        elseif (($deltayb>0)&&($vals_s[$i]["attributes"]["MIRVERT"]=='false'))
                                        {
                                             $v1[$kr]['attributes']['Y1'].='+'.$deltayb;
                                             $v1[$kr]['attributes']['Y1']=str_replace('dy',$dy1,$v1[$kr]['attributes']['Y1']);
                                            $v1[$kr]['attributes']['Y2'].='+'.$deltayb;
                                            $v1[$kr]['attributes']['Y2']=str_replace('dy',$dy1,$v1[$kr]['attributes']['Y2']);
                                            $ch_xnc=1;
                                        }
                                    }	
                                }
                                // if($vals[$i]["attributes"]["CODE"]=='122x1x1')
                                // {
                                //     p_($v1);
                                //     test ('cl',$cl);
                                //     test ('l', $lxnc);
                                //     test ('cw',$cw);
                                //     test ('w', $wxnc);
                                //     test ('!', $need_ch);
                                //     exit;
                                // }
                        }
                        else $part_can_not_be_changed[$vals[$pp]['attributes']['ID']]=$vals[$pp]['attributes']['NAME'];
                    }
                    unset($deltayt,$deltayb,$deltax);
                    $i1=make_index($v1);
                    // p_($vals[$pp]);exit;
                    if (($vals[$i]["attributes"]["TURN"]==1)||($vals[$i]["attributes"]["TURN"]==3))
                    {
                        if ($vals[$i]["attributes"]["BYSIZEDETAIL"]=='true')
                        {
                            $v1[0]['attributes']['DX']=$vals[$pp]["attributes"]["DW"];
                            $v1[0]['attributes']['DY']=$vals[$pp]["attributes"]["DL"];
                        }
                        elseif ($vals[$i]["attributes"]["BYSIZEDETAIL"]=='false')
                        {
                            $v1[0]['attributes']['DX']=$vals[$pp]["attributes"]["CW"];
                            $v1[0]['attributes']['DY']=$vals[$pp]["attributes"]["CL"];
                        }
                    }
                    elseif (($vals[$i]["attributes"]["TURN"]==0)||($vals[$i]["attributes"]["TURN"]==2))
                    {
                        if ($vals[$i]["attributes"]["BYSIZEDETAIL"]=='true')
                        {
                            $v1[0]['attributes']['DX']=$vals[$pp]["attributes"]["DL"];
                            $v1[0]['attributes']['DY']=$vals[$pp]["attributes"]["DW"];
                        }
                        elseif ($vals[$i]["attributes"]["BYSIZEDETAIL"]=='false')
                        {
                            $v1[0]['attributes']['DX']=$vals[$pp]["attributes"]["CL"];
                            $v1[0]['attributes']['DY']=$vals[$pp]["attributes"]["CW"];
                        }
                    }
                    if(($change==1)||($ch_xnc==1))
                    {
                        $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                    }
                    $res=change_bores ($link,$v1,$diameter_tools_from_DB,$vals[$pp]['attributes'],$e4);
                    $v1=$res['v1'];
                    $_SESSION['error_ok'].=$res['error_ok'];
                    $vals[$i]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                    // p_($vals[$pp]);
                            // p_($v1);
                            // exit;
                }
            }

        }
        
    }
    // p_($vals);
    // p_($part_xnc);
    // exit;
    
    foreach ($index['PART'] as $i)
    {
        if ($vals[$i]['attributes']['CL']>0)
        {
            if (!isset($part_xnc[$vals[$i]['attributes']['ID']]))
            {
                foreach (SIDE as $s)
                {
                    $vals[$i]=my_uncet($vals[$i],"my_xnc_side_".$s);
                }
            }
            else
            {
                foreach (SIDE as $s)
                {
                    if ($part_xnc[$vals[$i]['attributes']['ID']][$s]==1)
                    {
                        $vals[$i]["attributes"]["MY_XNC_SIDE_".mb_strtoupper($s)]=1;
                    }
                    else
                    {
                        $vals[$i]=my_uncet($vals[$i],"my_xnc_side_".$s);

                    }
                }
            }
        }
    }
    $vals=add_tag_print($vals);

//    p_($vals);
    // foreach ($bbb['s'] as $kb=>$bbbb)
    // {
        
    //     p_($bbbb);
    //     p_($bbb['e'][$kb]);
    //     echo "<hr>";

    // }
    // // p_($bbb);
    // exit;
    // xml($vals);
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'xnc_code_end', $session);

    return $vals;
}

/**
 * 
 **/
function xnc_mpr_price ($project)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'mpr_xnc_pricr start', $_SESSION);
    $data = get_vals_index_without_session($project);
    $vals = $data["vals"];
    $vals=vals_out($vals);
    $vals=add_tag_print($vals);
    ksort ($vals);
    $index=make_index($vals);
    // p_($vals);exit;
    foreach ($index["OPERATION"] as $i)
    {
       
        if (($vals[$i]['attributes']['TYPEID']=='XNC')&&((!isset($vals[$i]['attributes']['operation.my_mpr_in'])||($vals[$i]['attributes']['operation.my_mpr_in']==''))))
        {
            $p = xml_parser_create();
            xml_parse_into_struct($p, $vals[$i]['attributes']['PROGRAM'], $v, $v_index);
            xml_parser_free($p);
            $vals[$i]['attributes']['CODE']=trim($vals[$i]['attributes']['CODE']);
            $part2[$vals[$i]['attributes']['CODE']]['size']['DX']=$v[0]['attributes']['DX'];
            $part2[$vals[$i]['attributes']['CODE']]['size']['DY']=$v[0]['attributes']['DY'];
            $part2[$vals[$i]['attributes']['CODE']]['size']['DZ']=$v[0]['attributes']['DZ'];
            $part2[$vals[$i]['attributes']['CODE']]['side']=$vals[$i]['attributes']['SIDE'];
            if ((!isset($v_index['MA']))&&(!isset($v_index['MS']))&&(!isset($v_index['MAC']))&&(!isset($v_index['ML']))) $part2[$vals[$i]['attributes']['CODE']]['type']='sverl';
            else $part2[$vals[$i]['attributes']['CODE']]['type']="frez/sverlenie";

            if($vals[$i]['attributes']['COUNTCUT']>0)
            {
                foreach ($v_index['GR'] as $vi2)
                {
                    foreach ($v_index['TOOL'] as $t) if($v[$t]['attributes']['NAME']==$v[$vi2]['attributes']['NAME']) $ww=$v[$t]['attributes']['D'];
                    $part2[$vals[$i]['attributes']['CODE']]['op_paz']=1;
                    $x1=$v[$vi2]['attributes']['X1'];
                    $x2=$v[$vi2]['attributes']['X2'];
                    $y1=$v[$vi2]['attributes']['Y1'];
                    $y2=$v[$vi2]['attributes']['Y2'];
                    $t=$v[$vi2]['attributes']['T'];
                    if (
                        (($v[$vi2]['attributes']['T']>4)&&($v[$vi2]['attributes']['T']<=8))&&(
                        (($v[$vi2]['attributes']['X1']==$v[0]['attributes']['DX'])&&($v[$vi2]['attributes']['X2']==$v[0]['attributes']['DX']))OR
                        (($v[$vi2]['attributes']['X1']==0)&&($v[$vi2]['attributes']['X2']==0))OR
                        (($v[$vi2]['attributes']['Y1']==$v[0]['attributes']['DY'])&&($v[$vi2]['attributes']['Y2']==$v[0]['attributes']['DY']))OR
                        (($v[$vi2]['attributes']['Y1']==0)&&($v[$vi2]['attributes']['Y2']==0))
                        )
                    )
                    {
                        if ($x1==$x2)
                        $part[$vals[$i]['attributes']['CODE']][23357]+=(abs($y1-$y2)/1000)*ceil($t/($ww-1));
                        else $part[$vals[$i]['attributes']['CODE']][23357]+=(abs($x1-$x2)/1000)*ceil($t/($ww-1));
                    }
                    else
                    {
                        if ($x1==$x2)
                        $part[$vals[$i]['attributes']['CODE']][16366]+=(abs($y1-$y2)/1000)*ceil($t/($ww-1));
                        else $part[$vals[$i]['attributes']['CODE']][16366]+=(abs($x1-$x2)/1000)*ceil($t/($ww-1));
                    }
                    
                }
            }

            foreach ($v_index['TOOL'] as $vi)
            {
                $tool[$v[$vi]['attributes']['NAME']]=$v[$vi]['attributes']['D'];
            }
            // p_($v);
            $ar_c=array('MAC','MS','MA','ML');
            foreach ($ar_c as $a)
            {
                foreach ($v_index[$a] as $id_a)
                {
                    $part2[$vals[$i]['attributes']['CODE']]['op_frez']=1;
                    
                    foreach ($v[$id_a]['attributes'] as $key_a=>$a_v)
                    {
                        if ($a_v<0) $v[$id_a]['attributes'][$key_a]=abs($a_v);
                    }
                }
            }

            // p_($v);
            foreach ($v_index['MS'] as $vi)
            {
                $j=0;

                if (($v[$vi+1]['tag']=='ML')OR($v[$vi+1]['tag']=='MA')OR($v[$vi+1]['tag']=='MAC'))
                {
                    
                    $vi1=$vi+1;
                    while (($v[$vi1]['tag']=='ML')OR($v[$vi1]['tag']=='MA')OR($v[$vi1]['tag']=='MAC'))
                    {
                        
                        $sx=$v[0]['attributes']['DX']/1000;
                        $sy=$v[0]['attributes']['DY']/1000;
                        $x=$v[$vi1-1]['attributes']['X']/1000;
                        $y=$v[$vi1-1]['attributes']['Y']/1000;
                        $x_now=$v[$vi1]['attributes']['X']/1000;
                        $y_now=$v[$vi1]['attributes']['Y']/1000;
                        $cx_now=$v[$vi1]['attributes']['CX']/1000;
                        $cy_now=$v[$vi1]['attributes']['CY']/1000;
                        $r_now=$v[$vi1]['attributes']['R']/1000;
                        $er=$v[$vi1]['tag'];
                        if ((($x>=0)&&($y>=0))&&(($x_now>=0)&&($y_now>=0)))
                        {
                            // p_($er);
                            
                            if(
                                (($x<>$x_now)OR(($x<>$sx)&&($x<>0)))
                                AND
                                ((($y<>$y_now)OR(($y<>$sy)&&($y<>0))))
                                //нижняя строка добавлена экспериментально - контроль просчёта фрезерного реза
                                OR(substr_count($er,"MAC")>0))
                            {
                                
                                if (substr_count($er,"ML")>0)
                                {
                                
                                    $fr_len+=sqrt(($x_now-$x)*($x_now-$x)+($y_now-$y)*($y_now-$y));
                                }
                                if (substr_count($er,"MAC")>0)
                                {
                                    $r=sqrt(abs(pow($cx_now-$x_now,2))+abs(pow($cy_now-$y_now,2)));
                                    $horda=sqrt(abs(pow($x-$x_now,2))+abs(pow($y-$y_now,2)));
                                    $h=$r-sqrt($r*$r-($horda*$horda/4));
                                    $d=$r-$h;
                                    $cut=2*acos($d/$r);
                                    $fr_len+=$r*$cut;
                                    // test ('fr',$fr_len);
                                }

                            }
                            else $j++;
                        }
                        // p_($fr_len);

                        unset($x,$y,$x_now,$y_now,$cx_now,$cy_now,$sx,$sy);
                        if($fr_len>0) $fr_length[$j]+=$fr_len;
                        unset($fr_len);
                        $vi1++;
                    }

                }
                // exit;
                // echo "2<hr>";




                foreach ($fr_length as $f)
                {
                    if ($f<=0.45)
                    {
                        
                        
                        $part[$vals[$i]['attributes']['CODE']][85894]+=1;
                    }
                    elseif ($f>0.45)
                    {
                        $part[$vals[$i]['attributes']['CODE']][85895]+=round($f,2);
                    }
                }
                unset($fr_length);
                
            }

            foreach ($v as $vi=>$vr)
            {
                if (($v[$vi]['tag']=='BF')OR($v[$vi]['tag']=='BL')OR($v[$vi]['tag']=='BR')or($v[$vi]['tag']=='BT')OR($v[$vi]['tag']=='BB'))
                {
                    $part2[$vals[$i]['attributes']['CODE']]['op_bore']=1;
                    
                    if ((!isset($v[$vi]['attributes']['AC']))OR($v[$vi]['attributes']['AC']<1))$v[$vi]['attributes']['AC']=1;
                    if (($tool[$v[$vi]['attributes']['NAME']]<20)OR((($tool[$v[$vi]['attributes']['NAME']]<>26)&&($tool[$v[$vi]['attributes']['NAME']]<>35)&&($tool[$v[$vi]['attributes']['NAME']]==20)&&
                    (($v[0]['attributes']['DZ']-3>=$v[$vi]['attributes']['DP'])))))
                    {
                        $part[$vals[$i]['attributes']['CODE']][26321]+=$v[$vi]['attributes']['AC'];
                    }
                    elseif ((($tool[$v[$vi]['attributes']['NAME']]==26)OR($tool[$v[$vi]['attributes']['NAME']]==35))&&($v[$vi]['attributes']['DP']<=14.5)&&
                    ($v[0]['attributes']['DZ']-3>=$v[$vi]['attributes']['DP']))
                    {
                        $part[$vals[$i]['attributes']['CODE']][20323]+=$v[$vi]['attributes']['AC'];
                    }
                
                    elseif ((($tool[$v[$vi]['attributes']['NAME']]<>26)&&($tool[$v[$vi]['attributes']['NAME']]<>35)&&($tool[$v[$vi]['attributes']['NAME']]>20)&&
                    ($v[0]['attributes']['DZ']-3>=$v[$vi]['attributes']['DP']))
                    OR
                    (((($tool[$v[$vi]['attributes']['NAME']]==26)OR($tool[$v[$vi]['attributes']['NAME']]==35))&&($v[$vi]['attributes']['DP']>14.5)&&
                    ($v[0]['attributes']['DZ']-3>=$v[$vi]['attributes']['DP']))))
                    {
                        $part[$vals[$i]['attributes']['CODE']][110429]+=$v[$vi]['attributes']['AC'];
                    }
                    elseif (($tool[$v[$vi]['attributes']['NAME']]<>26)&&($tool[$v[$vi]['attributes']['NAME']]<>35)&&($tool[$v[$vi]['attributes']['NAME']]>=20)&&
                    ($v[0]['attributes']['DZ']<=$v[$vi]['attributes']['DP']))
                    {
                        $part[$vals[$i]['attributes']['CODE']][23360]+=$v[$vi]['attributes']['AC'];
                    }
                  
                }
                if ($v[$vi]['tag']=='MR')
                {
                    $part[$vals[$i]['attributes']['CODE']][22160]+=1;
                    $part2[$vals[$i]['attributes']['CODE']]['op_frez']=1;

                }



                


            }
            foreach ($v as $vi=>$vr)
            {
                if ($v[$vi]['tag']=='MR')
                {
                    $part[$vals[$i]['attributes']['CODE']][22160]+=1;
                }
            }

            
        }
    }
    $sql="select * from SERVICE";
    $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

    foreach ($part as $k=>$v)
    {
        foreach ($sql2 as $v1)
        {
            if (isset($v[$v1['CODE']]))
            {
                $part2[$k]['serv_id'][$v1['SERVICE_ID']]=$v[$v1['CODE']];
                $part2[$k]['code'][$v1['CODE']]=$v[$v1['CODE']];
            }
        }
        
    }

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'mpr_xnc_price fin', $_SESSION);
    return ($part2);
}

?>