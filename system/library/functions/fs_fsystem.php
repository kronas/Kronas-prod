<?php

/**
* Функция записывает данные в сессию, сохранённую во временном файле
* Параметры: $session_id - ID сессии(и название файла)  $data - данные;
**/
function put_temp_file_data($session_id, $data)
{
    //.. 30.07.2022 Правки, касательно цен. Если цены не получены (по API из Акцента),
    // то $_SESSION['failed_prices'] устанавливается в 1 и здесь его нужно сбрасывать
    if (isset($data['failed_prices'])) unset($data['failed_prices']);

    $file = $GLOBALS['serv_main_dir'] . '/kronasapp/storage/app/giblab_sessions/' . $session_id . '.txt';
    $file_data = unserialize(file_get_contents($file));

    foreach ($data as $key => $value) {
        $file_data[$key] = $value;
    }
    file_put_contents($file, serialize($file_data));
}

/**
* Функция читает данные сессии, сохранённой во временном файле, и перепроверяет проект
**/
function get_temp_file_data($session_id)
{
    $link = db_connect();
    $file = $GLOBALS['serv_main_dir'] . '/kronasapp/storage/app/giblab_sessions/' . $session_id . '.txt';
    $data = step_1_check(unserialize(file_get_contents($file)), $link, unserialize(file_get_contents($file)));
    mysqli_close($link);
    return $data;
}

/**
* Функция читает файл проекта с диска 'files/project_out/номер_подпапки'
**/
function get_Project_file($id){
    global $serv_main_dir, $_SESSION;
    $storage_dir = $serv_main_dir . 'files/project_out';

    $dirdig = ceil($id / 1000) - 1;
    $start_project_file = $storage_dir . '/' . $dirdig . '/' . abs($id) . ".txt";
    $start_project_file_old = $storage_dir . '/' . abs($id) . ".txt";
    if(file_exists($start_project_file)){
        $data_from_file = file_get_contents($start_project_file);
        $return['file_time'] = filemtime($start_project_file) ;
    }else if (file_exists($start_project_file_old)) {
        $data_from_file = file_get_contents($start_project_file_old); 
        $return['file_time'] = filemtime($start_project_file_old) ;
    } else return false;

    $start_project = base64_decode($data_from_file);
    $start_project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $start_project);
    $start_project = str_replace("\r", "", $start_project);
    $start_project = str_replace("\n", "", $start_project);
    $return['project_data'] = stripslashes($start_project);
    return $return;
}

/**
* Функция записывает файл проекта на диск 'files/project_out/номер_подпапки'
**/
function put_Project_file($id, $data){
    global $serv_main_dir, $_SESSION;
    $storage_dir = $serv_main_dir . 'files/project_out';
    //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
    $storage_dir_copy = $serv_main_dir . 'files/ps_share/project_out';

    $dirdig = ceil($id / 1000) - 1;

    $file_project = $storage_dir . '/' . $dirdig . '/' . abs($id) . ".txt";
    if (!opendir($storage_dir . '/' . $dirdig . '/')) mkdir($storage_dir . '/' . $dirdig , 0777);
    $_SESSION['debug'] = $file_project;
    file_put_contents($file_project, $data);
    
    //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
    $file_project = $storage_dir_copy . '/' . $dirdig . '/' . abs($id) . ".txt";
    if (!opendir($storage_dir_copy . '/' . $dirdig . '/')) mkdir($storage_dir_copy . '/' . $dirdig , 0777);
    file_put_contents($file_project, $data);

    return $file_project;
}

/**
* Получение названий каталогов и файлов в каталоге
**/
function GetFileNames($dir, $flag=false){
    $result = null;
    $result = scandir($dir);
    $data = array();
    foreach ($result as $file){
            if($file != '.' && $file != '..'){
            $data[] = $file;
            }
    }
    if (!$flag){return $data;}
    else {return foldersAndFiles($data);}
}
/**
* Используется в связке с GetFileNames
* Очищает массив от каталогов и точек после "scandir"
**/
function foldersAndFiles($array){
    foreach ($array as $value) {
        if (preg_match('#([.][a-z]{2,4}$)#', $value)) {
            $result[] = $value;
        }
    }
    return $result;
}

?>