<?php

/**
 * 
 **/
function step_1_check($project, $link, $session)
{
   
    $_SESSION['error'] = '';
    $_SESSION['error_ok'] = '';
    $content = $project;
    $content["project_data"] = str_replace('\\', '', $content["project_data"]);
    
    // ---------------------------------------------------------------------
    //.. 07.02.2022 Предварительная проверка, если проект из "Мебель про"
    // В этих проектах есть блок "CREATOR", из-за которого возникает ошибка,
    // связанная с ID XNC-операции, поэтому этот блок нужно удалять
    if (stripos($content["project_data"], '<creator')) {
        $content['project_data'] = preg_replace("#\n?<creator.*\/>\n?#", '', $content['project_data']);
    }
    // ---------------------------------------------------------------------

    $vals = get_vals($content["project_data"]);

    if ($vals[0]['attributes']['project.my_glass'] <> 1) {
        $vals[0]['attributes']['saveXmlCut'] = 'true';
        $vals[0]['attributes']['project.my_label'] = 'new';
        $vals = vals_out($vals);
        $index = make_index($vals);

        if (isset($index['VIYAR'])) {
            $i = $index['VIYAR'][0] + 1;
            unset ($vals[$i-1]);

            while ($vals[$i]['tag'] <> 'VIYAR') {
                unset($vals[$i]);
                $i++;
            }

            unset($vals[$i]);
            ksort($vals);
            $index = make_index($vals);
        }
        if (isset($index['RS_DATA'])) {
            $vals[0]['attributes']['my_rs'] = 1;
            $vals[0]['attributes']['project.my_rs_key'] = "CfvjdfhjgfhfdjpDfhdfhbx";
        }
        ksort($vals);
        $date = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
        $sql = 'DELETE FROM log WHERE date < "' . $date . '";';
        $res_b = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql);
        $timer = timer(0,__FILE__,__FUNCTION__,__LINE__, 'step_1 start', $session);
        
        $vals = add_tag_print($vals);
        $vals = cs_print_off_for_blank($vals);

        if (!isset($vals[0]['attributes']['project.my_client'])) $vals[0]['attributes']['project.my_client'] = $session['client_fio'];
        if (!isset($vals[0]['attributes']['project.my_order_name'])) $vals[0]['attributes']['project.my_order_name'] = 'Новый';
        if (!isset($vals[0]['attributes']['project.my_order_id'])) $vals[0]['attributes']['project.my_order_id'] = 'Новый';
        
        foreach($index['GOOD'] as $d) {
            if ($vals[$d]['attributes']['TYPEID'] == "sheet") {
                
                foreach ($index['OPERATION'] as $i) {
                    if (($vals[$i]['attributes']['TYPEID'] == 'CS')
                        && ($vals[$i+1]['attributes']['ID'] == $vals[$d]['attributes']['ID'])) {
                        
                        $mat_ok = 1;
                    }
                }

                if (!isset($mat_ok)) {
                    unset ($vals[$d]);
                    $d++;
                    
                    while ($vals[$d]['tag'] == "PART") {
                        unset($vals[$d]);
                        $d++;
                    }

                    unset($vals[$d]);
                } else unset($mat_ok);
            }
            if ($vals[$d]['attributes']['TYPEID'] == "band") {
                
                foreach ($index['OPERATION'] as $i) {
                    if (($vals[$i]['attributes']['TYPEID'] == "EL")
                        && ($vals[$i+1]['attributes']['ID'] == $vals[$d]['attributes']['ID']))
                    {
                        $mat_ok = 1;
                        
                        foreach ($index['GOOD'] as $g) {
                            if (($vals[$g]['attributes']['TYPEID'] == "tool.edgeline")
                                && ($vals[$g]['attributes']['ID'] == $vals[$i]['attributes']['TOOL1']))
                            {
                                if ($vals[$i]['attributes']['MY_CHECK_KL'] == 1) $vals[$g]['attributes'][mb_strtoupper('elWidthPreJoint')] = 0;
                                else {
                                    if (!isset($session['user_place'])) $session['user_place'] = 1;
                                    $sql = "SELECT * FROM `TOOL_EDGELINE` 
                                        inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
                                        inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
                                        where PLACE = " . $session['user_place'] . ";";
                                    $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'];
                                    $vals[$g]['attributes'][mb_strtoupper('elWidthPreJoint')] = 0;
                                    
                                    foreach ($sql3 as $r => $t) {
                                        if ($t['ELWIDTHPREJOINT'] > $vals[$g]['attributes'][mb_strtoupper('elWidthPreJoint')]) {
                                            $vals[$g]['attributes'][mb_strtoupper('elWidthPreJoint')]=$t['ELWIDTHPREJOINT'];
                                        }
                                    }

                                }
                                
                            }
                        }

                        
                    } elseif (($vals[$i]['attributes']['TYPEID'] == "CL")
                        && ($vals[$i+1]['attributes']['ID'] == $vals[$d]['attributes']['ID']))
                    {
                        $mat_ok=1;
                    }
                }
                if (!isset($mat_ok)) unset($vals[$d]);
                else unset($mat_ok);
            }
        }
        ksort($vals);
        $index = make_index($vals);

        foreach ($index["GOOD"] as $i) {
            if (($vals[$i]["attributes"]["TYPEID"] == "sheet")
                && (!isset($vals[$i]["attributes"]["MY_FIRST"]))
                && (!isset($vals[$i]["attributes"]['good.my_client_material']))
                && (!isset($vals[$i]["attributes"][mb_strtoupper('good.my_client_material')])))
            {
                unset($sql2);
                $nt = $vals[$i]["attributes"]["NAME"];
                if ((isset($vals[$i]['attributes']['CODE']))
                    && ($vals[$i]['attributes']['CODE'] > 0)
                    && (is_numeric($vals[$i]['attributes']['CODE'])))
                {    
                    $sql = "select * from MATERIAL where CODE=" . $vals[$i]['attributes']['CODE'];
                    $sql2 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                    
                }

                if ($sql2['CODE'] <> $vals[$i]['attributes']['CODE']) {
                    if ($vals[$i]["attributes"]["T"] > 0) {
                        $sql = "select * from MATERIAL where MY_1C_NOM=5931 and T=" .$vals[$i]['attributes']['T'];
                        $sql2 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                        if (sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['count'] < 1) {
                            $vals[$i]["attributes"]["T"] = 18;
                            $vals[$i]["attributes"]["CODE"] = 100008252;
                            $vals[$i]["attributes"]["NAME"] = 'Материал клиента  (толщина плиты 18  мм)';
                            $vals[$i]["attributes"]["UNIT"] = 'м.кв.';
                            $vals[$i]["attributes"]["TYPENAME"] = 'Листовой материал';
                        } else {

                            foreach($sql2 as $key => $v) {
                                if(!is_integer($key)) $vals[$i]["attributes"][$key] = $v;
                            }

                            $rtttt = array(28, 38, 39);
                            if (in_array( $vals[$i]["attributes"]['T'], $rtttt)) {
                                $vals[$i]["attributes"]['CODE'] = $sql2['CODE'];
                                $vals[$i]["attributes"]['ST'] = 1;
                                $vals[$i]["attributes"]['NAME'] = "Столешница клиента толщиною " . $vals[$i]["attributes"]['T'] . " мм";
                                $_SESSION['error_ok'] .= '<br>Материал "' . $nt . '" заменен на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                            } elseif ($vals[$i]["attributes"]['T'] < 5) {
                                $vals[$i]["attributes"]['CODE'] = $sql2['CODE'];
                                $vals[$i]["attributes"]['NAME'] = "HDF/ДВП клиента толщиною " . $vals[$i]["attributes"]['T'] . " мм";
                                $_SESSION['error_ok'] .= '<br>Материал "' . $nt . '" заменен на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                            }
                        }
                    } else {
                        $nt = $vals[$i]["attributes"]["NAME"];
                        $vals[$i]["attributes"]["T"] = 18;
                        $vals[$i]["attributes"]["CODE"] = 100008252;
                        $vals[$i]["attributes"]["NAME"] = 'Материал клиента  (толщина плиты 18  мм)';
                        $vals[$i]["attributes"]["UNIT"] = 'м.кв.';
                        $vals[$i]["attributes"]["TYPENAME"] = 'Листовой материал';
                    }
                    $_SESSION['error_ok'] .= '<br>Материал "' . $nt . '" заменен на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                } else {

                    foreach($sql2 as $key => $v) {
                        $vals[$i]["attributes"][$key] = $v;
                    }

                    $jj = $i + 1;

                    while ($vals[$jj]['tag'] == "PART") {
                        if ((!isset($vals[$jj]['attributes']["DBID"]))
                            && (!isset($vals[$jj]['attributes']["part.my_client_material"])))
                        {
                            if ((($sql2['L'] <> $vals[$jj]['attributes']["L"])
                                || ($sql2['W'] <> $vals[$jj]['attributes']["W"]))
                                && (!isset($vals[$jj]['attributes']["SHEETID"]))
                                && ($sql2['W'] <> 0)
                                && ($sql2['L'] <> 0))
                            {
                                //.. 17.11.2022 Тоценко. Добавлена возможность изменять размер листа для Коровина Виталия по его просьбе, потому что в наличии на тот момент только половинки листов (не уверен, что всё будет отрабатывать корректно)
                                if ($session['manager_id'] != 218 && $session['manager_id'] != 26) {
                                    $_SESSION['error_ok'] .= "<br>Материал [/" . $vals[$i]["attributes"]['CODE'] . "/] " . $vals[$i]["attributes"]['NAME'] . "  
                                    - размер листа был ошибочно изменён на (L " . $vals[$jj]["attributes"]["L"] . " * W " . $vals[$jj]["attributes"]["W"] . ").
                                    Внесён верный размер (L " . $sql2['L'] . " * W " . $sql2['W'] . ")
                                    <hr>";
                                    $vals[$jj]['attributes']["L"] = $sql2['L'];
                                    $vals[$jj]['attributes']["W"] = $sql2['W'];

                                    foreach ($index["OPERATION"] as $fr) {
                                        if (($vals[$fr]['attributes']['TYPEID'] == 'CS')
                                            && ($vals[$fr+1]['attributes']['ID'] == $vals[$i]['attributes']['ID']))
                                        {
                                            unset ($vals[$fr]['attributes']['DATA']);
                                            unset ($vals[$fr]['attributes']['DRAW']);
                                        }
                                    }
                                }

                            }
                        }
                        $jj++;
                    }
                }

                if ($vals[$i]['attributes']['MY_1C_NOM'] == 5931) {
                    $rtttt = array(28, 38, 39);
                    if ((in_array( $vals[$i]["attributes"]['T'], $rtttt))
                        && (substr_count($vals[$i]["attributes"]['NAME'],'Столешница') < 1))
                    {
                        $vals[$i]["attributes"]['CODE'] = $sql2['CODE'];
                        $vals[$i]["attributes"]['ST'] = 1;
                        $vals[$i]["attributes"]['NAME'] = "Столешница клиента толщиною " . $vals[$i]["attributes"]['T'] . " мм";
                        $_SESSION['error_ok'] .= '<br>Материал "' . $nt . '" заменен на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                    } elseif (($vals[$i]["attributes"]['T'] < 5)
                        && (substr_count($vals[$i]["attributes"]['NAME'],'HDF') < 1))
                    {
                        $vals[$i]["attributes"]['CODE'] = $sql2['CODE'];
                        $vals[$i]["attributes"]['NAME'] = "HDF/ДВП клиента толщиною " . $vals[$i]["attributes"]['T'] . " мм";
                        $_SESSION['error_ok'] .= '<br>Материал "' . $nt . '" заменен на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                    }
                }
            } elseif (($vals[$i]["attributes"]["TYPEID"] == "sheet") && ($vals[$i]["attributes"]["MY_FIRST"] > 0)) {
                if ($vals[$i+1]['tag'] == 'PART') $vals[$i+1]['attributes']['COUNT'] = 1000;
            } elseif ($vals[$i]["attributes"]["TYPEID"] == "band") {
                unset($ddf);

                foreach ($index['OPERATION'] as $df) {
                    if (($vals[$df]["attributes"]["TYPEID"] == "CL")
                        && ($vals[$df+1]["attributes"]["ID"] == $vals[$i]["attributes"]["ID"]))
                    {
                        $dfd = 1;
                        // минимальный размер остатка, мм
                        $vals[$df]["attributes"]["CMINDIMREGWASTE1"] = 600;
                        // Длина делового остатка, м
                        $vals[$df]["attributes"]["CBUSINESSWASTE"] = 0.5;
                        // минимальный размер детали, мм
                        $vals[$df]["attributes"]["CMINDIMDETAIL"] = 30;
                    }
                }

                if (!isset($dfd)) {
                    if ((isset($vals[$i]['attributes']['CODE'])) && (is_numeric($vals[$i]['attributes']['CODE']))) {
                        $sql = "select * from BAND where CODE=" . $vals[$i]['attributes']['CODE'];
                        $sql2 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                    } else unset ($sql);

                    if ((count($sql2) < 1) || (!isset($sql2))) {
                        if ((strval($vals[$i]["attributes"]["W"]) > 0) && (strval($vals[$i]["attributes"]["T"]) > 0)) {

                            $sql = "select * from BAND where MY_1C_NOM=6149 and T=" . $vals[$i]['attributes']['T'] . " and W=" . $vals[$i]['attributes']['W'];
                            $sql2 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                            if (count($sql2) < 1) {
                                $nt = $vals[$i]["attributes"]["NAME"];
                                $vals[$i]["attributes"]["T"] = 0.5;
                                $vals[$i]["attributes"]["W"] = 22;
                                $vals[$i]["attributes"]["CODE"] = 100009431;
                                $vals[$i]["attributes"]["NAME"] = 'Кромка клиента 22 * 0,5 *';
                                $vals[$i]["attributes"]["UNIT"] = 'м.п.';
                                $vals[$i]["attributes"]["TYPENAME"] = 'Кромка';
                                $_SESSION['error_ok'] .= '<br>Кромка "' . $nt . '" заменена на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                            } else {
                                foreach($sql2 as $key => $v) {
                                    if (!is_integer($key)) $vals[$i]["attributes"][$key] = $v;
                                }
                            }
                        } else {
                            $nt = $vals[$i]["attributes"]["NAME"];
                            $vals[$i]["attributes"]["T"] = 0.5;
                            $vals[$i]["attributes"]["W"] = 22;
                            $vals[$i]["attributes"]["CODE"] = 100009431;
                            $vals[$i]["attributes"]["NAME"] = 'Кромка клиента 22 * 0,5 *';
                            $vals[$i]["attributes"]["UNIT"] = 'м.п.';
                            $vals[$i]["attributes"]["TYPENAME"] = 'Кромка';
                            $_SESSION['error_ok'] .= '<br>Кромка "' . $nt . '" заменена на "' . $vals[$i]["attributes"]["NAME"] . '".<hr>';
                        }
                    }
                } else {
                    foreach($sql2 as $key => $v) {
                        if (!is_integer($key)) $vals[$i]["attributes"][$key] = $v;
                    }
                }
            }
            unset($sql2);
        }

        $comment = '/*- ';
        foreach($index['GOOD'] as $d) {
            if (($vals[$d]['attributes']['TYPEID'] == "sheet")
                && (!isset($vals[$d]['attributes']['CODE']))
                && ($vals[$d]['attributes']['good.my_client_material'] <> 1))
            {
                $d1 = $d + 1;
                while ($vals[$d1]['tag'] == "PART") {
                    unset($change);
                    if (!isset($vals[$d1]['attributes']['SHEETID'])) {

                        foreach ($index['PART'] as $p) {
                            if (($vals[$p]['attributes']['CL'] > 0)
                                && ($vals[$p]['attributes']['ID'] == $vals[$d1]['attributes']['ID']))
                            {
                                $change = 1;
                            } 
                        }

                        if ($change == 1) {
                            $id = new_id($vals) + mt_rand(1000, 522220);
                            $old_id = $vals[$d1]['attributes']['ID'];
                            $vals[$d1]['attributes']['ID'] = $id;
                            
                            foreach ($index['OPERATION'] as $o) {
                                if (($vals[$o]['attributes']['TYPEID'] == 'CS')
                                    && ($vals[$o+1]['attributes']['ID'] == $vals[$d]['attributes']['ID']))
                                {
                                    $d2 = $o + 2;
                                    
                                    while ($vals[$d2]['tag'] == "PART") {
                                        if ($vals[$d2]['attributes']['ID'] == $old_id) $vals[$d2]['attributes']['ID'] = $id;
                                        $d2++;
                                    }

                                }
                            }

                        }
                    }
                    $d1++;
                }
            }
        }

        foreach($index['GOOD'] as $d) {
            unset($dfd);
            foreach ($index['OPERATION'] as $df) {
                if (($vals[$df]["attributes"]["TYPEID"] == "CL") && ($vals[$df+1]["attributes"]["ID"] == $vals[$d]["attributes"]["ID"])) {
                    $dfd = $df;
                }
            }

            if ($dfd > 0) {
                
                foreach($index['GOOD'] as $d1) {
                    if (($vals[$d1]['attributes']['TYPEID'] == "tool.cutting")
                        && ($vals[$d1]['attributes']['ID'] == $vals[$dfd]["attributes"]["TOOL1"]))
                    {
                        $vals[$d1]["attributes"]["SWSAWTHICK"] = "5";
                        $vals[$d1]["attributes"]["SWMAXLENGTHBAND"] = "5000";
                        $vals[$d1]["attributes"][mb_strtoupper('swPackageHeight')] = 18;
                    }
                }

            } else {
                if (!is_integer($vals[$d]['attributes']['CODE']/1)&&isset($vals[$d]['attributes']['CODE'])) {
                    unset($vals[$d]['attributes']['CODE']);
                }
                if ($vals[$d]['attributes']['TYPEID'] == "tool.cutting") {

                    $vals[$d]["attributes"]["SWMAXTURNS"] = "6";
                    $vals[$d]["attributes"]["SWCOMPLEXBAND"] = "false";
                    $vals[$d]["attributes"][mb_strtoupper('swPackageHeight')] = 55;

                }
                if (($vals[$d]['attributes']['TYPEID'] == "band")
                    && ($vals[$d]['attributes']['CODE'] > 0)
                    && (is_numeric($vals[$d]['attributes']['CODE'] > 0)))
                {
                    $sql = 'SELECT * FROM BAND WHERE CODE = ' . $vals[$d]["attributes"]["CODE"];
                    $res_b = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                    
                    if (sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['count'] == 1) {

                        foreach ($res_b as $key => $mass) {
                            if (($key <> "NAME") && (!is_integer($key))) $vals[$d]['attributes'][$key] = $mass;
                            elseif (($key == "NAME") && (!isset($vals[$d]['attributes']['MY_CHECK_KL']))) $vals[$d]['attributes'][$key] = $mass;
                        }

                    }
                } elseif (($vals[$d]['attributes']['TYPEID'] == "sheet")
                    && ($vals[$d]['attributes']['CODE'] > 0)
                    && ($vals[$d]['attributes']['MY_1C_NOM'] <> 5931)
                    && ($vals[$d]['attributes']['good.my_client_material'] <> 1))
                {
                    $sql = 'SELECT * FROM MATERIAL WHERE CODE = ' . $vals[$d]["attributes"]["CODE"];
                    $res_b = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                    if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['count'] == 1) {
                        
                        foreach ($res_b as $key => $mass) {
                            if (($key <> "NAME") && (!is_integer($key))) $vals[$d]['attributes'][$key] = $mass;
                            elseif (($key == "NAME") && (!isset($vals[$d]['attributes']['MY_FIRST']))) $vals[$d]['attributes'][$key] = $mass;
                        }

                    }
                    if ($vals[$d]['attributes']['L'] == 0) $vals[$d]['attributes']['L'] = 2750;
                    if ($vals[$d]['attributes']['W'] == 0) $vals[$d]['attributes']['W'] = 1830;
                }
            }   
            if(($vals[$d]['attributes']['good.my_client_material'] == 1) && ($vals[$d]['attributes']['TYPEID'] == 'sheet')) {
                if ($vals[$d]['attributes']['TYPEID'] == "sheet") {
                    
                    $sql = 'SELECT * FROM MATERIAL WHERE MATERIAL_ID = ' . $vals[$d]['attributes']['good.my_client_material_id'];
                    $res_b = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                    $vals[$d]['attributes']['good.my_client_material_l'] = $res_b['L'];
                    $vals[$d]['attributes']['good.my_client_material_w'] = $res_b['W'];
                    $vals[$d]['attributes']['good.my_client_material_t'] = $res_b['T'];
                    
                    if ($res_b['T'] > 0) {
                        $sql = 'SELECT * FROM MATERIAL WHERE T = ' . $res_b['T'] . ' AND MY_1C_NOM = 5931';
                        $res = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                        
                        while (count($res) < 1) {
                            $res_b['T'] = ceil($res_b['T']) + 1;
                            $sql = 'SELECT * FROM MATERIAL WHERE T = ' . $res_b['T'] . ' AND MY_1C_NOM=5931';
                            $res = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                        }

                        $vals[$d]['attributes']['good.my_client_material_code']=$res['CODE'];
                    }
                        
                    $vals[$d]['attributes']['L'] = $vals[$d]['attributes']['good.my_client_material_l'];
                    $vals[$d]['attributes']['W'] = $vals[$d]['attributes']['good.my_client_material_w'];
                    $vals[$d]['attributes']['T'] = $vals[$d]['attributes']['good.my_client_material_t'];

                    if (($vals[$d]['attributes']['T'] == 28)
                        || ($vals[$d]['attributes']['T'] == 38)
                        || ($vals[$d]['attributes']['T'] == 39)) 
                    {
                        $vals[$d]['attributes']['ST'] = 1;
                    }

                    $vals[$d]['attributes']['CODE'] = $vals[$d]['attributes']['good.my_client_material_code'];
                    $vals[$d]['attributes']['COUNT'] = 10000;
                    $id = $vals[$d]['attributes']['good.my_client_material_id'];
                    $cl_c = $vals[$d]['attributes']['good.my_client_material_client_id'];

                    if ($cl_c > 0) {
                        $sql = 'SELECT * from client WHERE client_id = ' . $cl_c;
                        $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                        
                        if ($sql3['code'] > 0) {
                            $sql='SELECT * from client WHERE code='.$sql3['code'];
                            $sql22=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                            
                            if ($vals[$d]['attributes']['good.my_client_material_place'] > 0) {
                                $sql = 'SELECT * from MATERIAL_HALF WHERE place_id = ' . $vals[$d]['attributes']['good.my_client_material_place'] . ' AND material_id = ' . $res_b['MATERIAL_ID'];
                                $half_m_k = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['count'];
                            }
                        }
                    }
                    unset($code_client);

                    foreach ($sql22 as $sql3) {
                        $code_client[] = $sql3['client_id'];
                    }

                    if (($session['user_place'] <> $vals[$d]['attributes']['good.my_client_material_place'])
                        OR (!in_array($vals[$d]['attributes']['good.my_client_material_client_id'],$code_client)))
                    {
                        if (($vals[$d]['attributes']['good.my_client_material_place'] > 0)
                            && ($vals[$d]['attributes']['good.my_client_material_client_id']))
                        {
                            $sql = "SELECT * FROM PLACES WHERE PLACES_ID = " . $vals[$d]['attributes']['good.my_client_material_place'];
                            $place_name = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];

                            $sql = "SELECT * FROM client WHERE client_id = " . $vals[$d]['attributes']['good.my_client_material_client_id'];
                            $client_name = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                            
                            $_SESSION['error_ok'] .= 'Материал клиента [/' . $id . '/] и все детали из него удаляются с проекта, так как материал клиента привязан к клиенту и участку, 
                            где он находится на складе. Произошла смена участка либо смена клиента.
                            При внесении остатка в проект был авторизован клиент "' . $client_name['name'] . '" и выбран участок "' . $place_name['NAME'] . ' (' . $place_name['ADRESS'] . ')".<hr>
                            ';
                        } else {
                            $_SESSION['error_ok'] .= 'Материал клиента [/' . $id . '/] и все детали из него удаляются с проекта, так как материал клиента привязан к клиенту и участку, 
                            где он находится на складе. Произошла смена участка либо смена клиента.
                            <hr>
                            ';
                        }

                        foreach ($index['GOOD'] as $i) {
                            if(($vals[$i]['attributes']['TYPEID'] == "tool.cutting")
                                &&($vals[$i]['attributes']['good.my_client_material_id'] == $id))
                            {
                                unset($vals[$i]);

                            } elseif(($vals[$i]['attributes']['TYPEID'] == "sheet")
                                && ($vals[$i]['attributes']['good.my_client_material_id'] == $id))
                            {
                                unset($vals[$i]);
                                $ii = $i + 1;
                                
                                while ($vals[$ii]['tag'] == "PART") {
                                    unset ($vals[$ii]);
                                    $ii++;
                                }

                                unset ($vals[$ii]);
                            }
                        }

                        foreach ($index['OPERATION'] as $i) {
                            if(($vals[$i]['attributes']['TYPEID'] == 'CS')
                                && ($vals[$i]['attributes']['operation.my_client_material_id'] == $id))
                            {
                                unset($vals[$i], $vals[$i+1]);
                                $j = $i + 2;
                                
                                while ($vals[$j]["tag"] == "PART") {
                                    $id = $vals[$j]["attributes"]["ID"];
                                    
                                    foreach ($index["PART"] as $dd) {
                                        if ($vals[$dd]["attributes"]["ID"] == $id) unset ($vals[$dd]);
                                    }

                                    $j++;
                                }

                                unset($vals[$j], $id, $j);
                            }
                        }
                    } else {
                        if (is_numeric($vals[$d]['attributes']['good.my_client_material_client_id'])) {
                            $sql = 'SELECT * FROM client WHERE client_id = ' . $vals[$d]['attributes']['good.my_client_material_client_id'];
                            $code = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0]['code'];
                        }
                        unset($cc, $cc_1);

                        if ((is_numeric($code)) && ($code > 0)) {
                            $sql = 'SELECT * FROM client WHERE code = ' . $code;
                            $code = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'];
                            
                            foreach ($code as $c) {
                                $cc .= ' OR Client = ' . $c['client_id'] . ' ';    
                            }

                        }
                        $sql = 'SELECT * FROM MATERIAL_CLIENT WHERE CODE_MAT = ' . $vals[$d]['attributes']['good.my_client_material_id'];
                        $code = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                        $re = ' AND (Material_client = ' . $code[0]['mc_id'];

                        for ($r = 1; $r < count($code); $r++) {
                            $re .= ' OR Material_client = ' . $code[$r]['mc_id'];
                        }

                        $re .= ') ';
                        $sql = 'SELECT * FROM MATERIAL_KL_STOCK_COUNT WHERE Count > 0 AND Place = ' . $vals[$d]['attributes']['good.my_client_material_place'] . ' AND (Client =' . $vals[$d]['attributes']['good.my_client_material_client_id'] . $cc .')' . $re;
                        $mat_kl = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'];
                        unset($ar_name);

                        foreach ($mat_kl as $km => $mk) {
                            $sql = 'SELECT * FROM MATERIAL_CLIENT WHERE mc_id = ' . $mk['Material_client'];
                            $m1 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                            if ($m1['CODE_MAT'] <> $vals[$d]['attributes']['good.my_client_material_id']) {
                                unset ($mat_kl[$km]);
                            } else {
                                $mat_kl[$km]['L'] = $m1['L'];
                                $mat_kl[$km]['W'] = $m1['W'];
                                $mat_kl[$km]['T'] = $m1['T'];
                            }
                            $cc_1[] = $mk['Material_client'];
                        }

                        $mat_kl_t = $mat_kl;
                        $dd = $d + 1;
                        unset($not_found);

                        while ($vals[$dd]['tag'] == "PART") {
                            if (!isset($vals[$dd]['attributes']['SHEETID'])) {
                                unset ($found);

                                foreach ($mat_kl_t as $kk => $vv) {
                                    if (($vals[$dd]['attributes']['L'] == $vv['L'])
                                        && ($vals[$dd]['attributes']['W'] == $vv['W'])
                                        && ($vals[$dd]['attributes']['COUNT'] == $vv['Count'])
                                        && (in_array($vals[$dd]['attributes']['part.my_client_material_id'], $cc_1)))
                                    
                                    {
                                        unset($mat_kl_t[$kk]);
                                        $found = 1;
                                        $ar_name[$vv['Material_client']] = $vals[$dd]['attributes']['USEDCOUNT'];
                                    }
                                }

                                if ($half_m_k > 0) {
                                    if(($vals[$dd]['attributes']['L'] == $res_b['L'])
                                        && ($vals[$dd]['attributes']['W'] == $res_b['W']))
                                    {
                                        $vals[$dd]['attributes']['DESCRIPTION'] = 'лист со склада';
                                        $found = 1;
                                        $ar_name['ЛИСТ'] = $vals[$dd]['attributes']['USEDCOUNT'];
                                    }
                                    if(($vals[$dd]['attributes']['L'] == $res_b['L'])
                                        && ($vals[$dd]['attributes']['W'] == ($res_b['W'] / 2 - 5)))
                                        {
                                            $vals[$dd]['attributes']['DESCRIPTION'] = 'поллиста со склада';
                                            $found = 1;
                                            $ar_name['ПОЛЛИСТА'] = $vals[$dd]['attributes']['USEDCOUNT'];
                                        }
                                } elseif (($vals[$dd]['attributes']['L']==$res_b['L'])
                                    && ($vals[$dd]['attributes']['W']==$res_b['W'])) {

                                    $vals[$dd]['attributes']['DESCRIPTION'] = 'лист со склада';
                                    $found = 1;
                                    $ar_name['ЛИСТ'] = $vals[$dd]['attributes']['USEDCOUNT'];
                                }
                                if (!isset($found)) {
                                    $not_found = 1;
                                    $mat_kl_t[] = array("L" => $vals[$dd]['attributes']['L'], "W" => $vals[$dd]['attributes']['W']);
                                }
                            }
                            $dd++;
                        }

                        if (($not_found > 0) || (count($mat_kl_t) > 0)) {
                            unset($er, $list);

                            foreach ($mat_kl_t as $aa => $bb) {
                                if ($bb['Material_client'] > 0) {
                                    $er .= ' - остаток клиента [/' . $bb['Material_client'] . '/] (' . $bb['L'] . '*' . $bb['W'] . ') не внесён в листы;<br>';
                                } else $er .= ' - лист (' . $bb['L'] . '*' . $bb['W'] . ') не является целым или половиной листа, на скаладе все остатки с кодом, в этом листе также не указан код;<br>';
                            }

                            foreach ($mat_kl as $aa => $bb) {
                                $list .= ' [/' . $bb['Material_client'] . '/] (' . $bb['L'] . '*' . $bb['W'] . ') - ' . $bb['Count'] . ' шт;<br>';                           
                            }

                            $sql = "SELECT * FROM PLACES WHERE PLACES_ID = " . $vals[$d]['attributes']['good.my_client_material_place'];
                            $place_name = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                            $sql = "SELECT * FROM client WHERE client_id = " . $vals[$d]['attributes']['good.my_client_material_client_id'];
                            $client_name = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                            $_SESSION['error_ok'] .= "<br>В проект добавлен материал '" . $res_b['NAME'] . "' клиента '" . $client_name['name'] . "' по участку '" . $place_name['NAME'] . " (" . $place_name['ADRESS'] . ")' . 
                            <br><br>
                            На складе есть в наличии детали клиента в этом материале:
                            <br><br>
                            " . $list . "
                            <br><br>
                            Проблемы:
                            <br><br>
                            " . $er . "<br><br>
                            Также в этот же плитный материал внесены листы со склада, если материал нужно докупить у нас. Если материал продаётся по поллиста, также возможно внесение остатка поллиста.
                            <br><br>
                            Из вкладки 'листы и остатки' был убран остаток клиента либо добавлен остаток, которого нет на складе. Остатки материала приведены к реальным, раскрой удалён.
                            <hr>";

                            foreach ($index['OPERATION'] as $rt) {
                                if (($vals[$rt]['attributes']['TYPEID'] == 'CS')
                                    && ($vals[$rt+1]['attributes']['ID'] == $vals[$d]['attributes']['ID']))
                                {
                                    $cs_id = $vals[$rt]['attributes']['ID'];
                                    $sheet_id = $vals[$d]['attributes']['ID'];
                                    $vals = cs_clear_data($vals, $vals[$rt]['attributes']['ID']);
                                }
                            }

                            ksort($vals);
                            $index = make_index($vals);

                            foreach ($index['GOOD'] as $rt) {
                                if (($vals[$rt]['attributes']['TYPEID'] == 'sheet')
                                    && ($vals[$rt]['attributes']['ID'] == $sheet_id)) {
                                    $rt1 = $rt + 1;

                                    while ($vals[$rt1]['tag'] == "PART") {
                                        unset ($vals[$rt1]);
                                        $rt1++;
                                    }

                                    ksort($vals);
                                    $index = make_index($vals);
                                    $id = new_id($vals);
                                    foreach ($mat_kl as $kl => $mmat) {
                                        $id++;
                                        $id = $id + mt_rand(555, 250000);
                                        $array_in[$id] = Array (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes"=> Array (
                                                "L" => $mmat["L"],
                                                "W" => $mmat["W"],
                                                "COUNT" => $mmat["Count"],
                                                "USEDCOUNT" => 0,
                                                "ID" => $id,
                                                'part.my_client_material' => 1,
                                                'part.my_client_material_id' => $mmat["Material_client"],

                                            )
                                        );
                                        
                                    }

                                    if ($half_m_k > 0) {
                                        $id++;
                                        $id = $id + mt_rand(555,250000);
                                        $array_in[$id] = Array (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes" => Array (
                                                "L" => $res_b["L"],
                                                "W" => ($res_b["W"] / 2 - 5),
                                                "COUNT" => 1,
                                                "USEDCOUNT" => 0,
                                                "ID" => $id,
                                                'NAME' => 'поллиста со склада',
                                                'DESCRIPTION' => 'поллиста со склада'
                                            )
                                        );
                                        
                                        $id++;
                                        $id = $id + mt_rand(555,250000);
                                        $array_in[$id] = Array (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes" => Array (
                                                "L" => $res_b["L"],
                                                "W" => $res_b["W"],
                                                "COUNT" => 10000,
                                                "USEDCOUNT" => 0,
                                                "ID" => $id,
                                                'NAME' => 'лист со склада',
                                                'DESCRIPTION' => 'лист со склада'
                                            )
                                        );
                                    } else {
                                        $id++;
                                        $id = $id + mt_rand(555,250000);
                                        $array_in[$id] = Array (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes" => Array (
                                                "L" => $res_b["L"],
                                                "W" => $res_b["W"],
                                                "COUNT" => 10000,
                                                "USEDCOUNT" => 0,
                                                "ID" => $id,
                                                'NAME' => 'лист со склада',
                                                'DESCRIPTION' => 'лист со склада'
                                            )
                                        );
                                    }
                                    unset($id1);

                                    foreach ($array_in as $df => $rf) {
                                        $vals = move_vals($vals, $rt+1+$id1, 1);
                                        $vals[$rt+1+$id1] = $rf;
                                        $id1++;
                                        ksort($vals);
                                        $index = make_index($vals);
                                    }

                                    unset ($id1);
                                }
                            }

                            ksort($vals);
                            $index = make_index($vals);
                            foreach ($index['OPERATION'] as $rt) {
                                if (($vals[$rt]['attributes']['TYPEID'] == 'CS')
                                    && ($vals[$rt]['attributes']['ID'] == $cs_id))
                                {
                                    $rt1 = $rt + 2;
                                    
                                    while ($vals[$rt1]['tag'] == "PART") {
                                        unset($no_del);

                                        foreach ($index['PART'] as $pp) {
                                            if (($vals[$pp]['attributes']['ID'] == $vals[$rt1]['attributes']['ID'])
                                                &&($vals[$pp]['attributes']['CL'] > 0))
                                            {
                                                $no_del = 1;
                                            }
                                        }

                                        if (!isset($no_del)) unset ($vals[$rt1]);
                                        $rt1++;
                                    }

                                    ksort($vals);
                                    $index = make_index($vals);
                                    unset ($id1);

                                    foreach ($array_in as $ar_in) {
                                        $ar_in2 = Array (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes" => Array ("ID" => $ar_in["attributes"]["ID"])
                                        );
                                        $id1++;
                                        $vals = move_vals($vals, $rt+2+$id1, 1);
                                        $vals[$rt+2+$id1] = $ar_in2;
                                    }

                                }
                            }
                        }
                        ksort($vals);
                        $index = make_index($vals);
                        unset($name);

                        foreach ($ar_name as $aa => $bb) {
                            if (($bb > 0) || ((is_string($bb)) && ($bb <> '0'))) $name .= ' [/' . $aa . ' - ' . $bb . '/] ';
                        }

                        $vals[$d]["attributes"]["NAME"] = $name . ' --МатКл-- ' . $res_b['NAME'];
                        $vals[$d]["attributes"]["good.name1"] = $vals[$d]["attributes"]["NAME"];
                        $vals[$d]["attributes"]["good.my_client_material_name"] = $vals[$d]["attributes"]["NAME"];
                        
                        $comment .= 'МАТЕРИАЛ КЛИЕНТА ' . $vals[$d]['attributes']['good.my_client_material_name'] . ' - ' . $vals[$d+1]['attributes']['USEDCOUNT'] . ' шт; ';
                    }
                }
            }
        }
        ksort($vals);

        $vals[0]['attributes']['DESCRIPTION'] = substr($vals[0]['attributes']['DESCRIPTION'], 0, strpos($vals[0]['attributes']['DESCRIPTION'], '/*-'));
        if ($comment <> '/*- ') $vals[0]['attributes']['DESCRIPTION'] .= $comment . '-*/';

        $vals[0]['attributes']['NAME'] = str2url($vals[0]['attributes']['NAME']);
        $vals[0]['attributes']['DESCRIPTION'] = htmlspecialchars($vals[0]['attributes']['DESCRIPTION']);

        if (isset($vals[0]['attributes']['project.my_order_name'])) {
            $vals[0]['attributes']['project.my_order_name'] = str2url($vals[0]['attributes']['project.my_order_name']);
        }
        if (isset($vals[0]['attributes'][mb_strtoupper('project.my_order_name')])) {
            $vals[0]['attributes'][mb_strtoupper('project.my_order_name')] = str2url($vals[0]['attributes'][mb_strtoupper('project.my_order_name')]);
        }
        $index = make_index($vals);
        
        if (!isset($index['RS_DATA'])) {

            foreach($index["OPERATION"] as $i1) {
                if ($vals[$i1]["attributes"]["TYPEID"] == "CL") {
                    
                    $vals[$i1]["attributes"]["TYPEID"] = 'CS';
                    $vals[$i1]["attributes"]["CTRIMW"] = "0";
                    $vals[$i1]["attributes"]["CTRIML"] = "0";

                    foreach($index["GOOD"] as $rttttt) {
                        if (($vals[$rttttt]['attributes']['TYPEID'] == "band") && ($vals[$rttttt]['attributes']['ID'] == $vals[$i1+1]["attributes"]["ID"]))
                        {
                            $vals[$rttttt]['attributes']['TYPEID'] = 'sheet';
                        }
                    }

                }
            }

        }

        foreach ($index["PART"] as $i) {
            $vals[$i] = my_uncet($vals[$i], 'my_db_id');
            if ($vals[$i]['attributes']['CL'] > 0) {

                if ($vals[$i]['attributes']['MY_DOUBLE_ID'] == 'NULL') $vals[$i] = my_uncet($vals[$i], 'my_double_id');
                if ($vals[$i]['attributes']['part.my_double_id'] == 'NULL') unset($vals[$i]['attributes']['part.my_double_id']);
                if (!isset($vals[$i]['attributes']['part.my_gr_type'])) $vals[$i]['attributes']['part.my_gr_type'] = 'true';
                if ($vals[$i]['attributes']['_old_part.my_double_id'] == 'NULL') unset($vals[$i]['attributes']['_old_part.my_double_id']);
                if ($vals[$i]['attributes'][mb_strtoupper('_old_part.my_double_id')] == 'NULL') unset($vals[$i]['attributes'][mb_strtoupper('_old_part.my_double_id')]);
                if ($vals[$i]['attributes']['MY_DOUBLE_PARENT'] == 'NULL') $vals[$i] = my_uncet($vals[$i], 'my_double_parent');
                if ($vals[$i]['attributes']['part.my_double_parent'] == 'NULL') unset($vals[$i]['attributes']['part.my_double_parent']);
                if ($vals[$i]['attributes']['_old_part.my_double_parent'] == 'NULL')  unset($vals[$i]['attributes']['_old_part.my_double_parent']);
                if ($vals[$i]['attributes'][mb_strtoupper('_old_part.my_double_parent')] == 'NULL') unset($vals[$i]['attributes'][mb_strtoupper('_old_part.my_double_parent')]);

                foreach (SIDE as $h1) {
                    $vals[$i] = my_uncet($vals[$i],'el_side_' . $h1);
                    $vals[$i] = my_uncet($vals[$i],'my_kl_el' . $h1);
                    $vals[$i] = my_uncet($vals[$i],'my_kl_el' . $h1 . "_code");
                    $vals[$i] = my_uncet($vals[$i],'my_db_el' . $h1);
                    $vals[$i] = my_uncet($vals[$i],'my_db_kl_el' . $h1);
                }

            }
        }
        unset($d_id);
        $vals = add_tag_print($vals);
        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'step_1 data correction', $session);

        foreach ($index['PART'] as $i) {
            if (!in_array($vals[$i]['attributes']['MY_DOUBLE_ID'], $d_id)) $d_id[] = $vals[$i]['attributes']['MY_DOUBLE_ID'];
        }
        
        foreach ($d_id as $did) {
            if(isset($did)) {

                foreach ($index['PART'] as $i) {
                    if (($vals[$i]['attributes']['MY_DOUBLE_ID'] == $did) && ($vals[$i]['attributes']['MY_DOUBLE_RES'] == 1)) {
                        $id_m = $vals[$i]['attributes']['ID'];
                    } elseif (($vals[$i]['attributes']['MY_DOUBLE_ID'] == $did) && ($vals[$i]['attributes']['MY_DOUBLE_FACE'] == 1)) {
                        $id_f = $vals[$i]['attributes']['ID'];
                        $id_f_i = $i;
                    }
                }

                if ($id_m == $id_f) {
                    $vals[$id_f_i]['attributes']['ID'] = new_id($vals) + mt_rand(1000, 1200000);
                    $new_id_f = $vals[$id_f_i]['attributes']['ID'];
                    
                    if ($vals[$id_f_i]['attributes']['MY_MAT'] > 0) {
                        
                        foreach ($index['GOOD'] as $g) {
                            if (($vals[$g]['attributes']['TYPEID'] == "sheet")
                                && ($vals[$g]['attributes']['material_id'] == $vals[$id_f_i]['MY_MAT']))
                            {
                                $id_m = $vals[$g]['attributes']['ID'];

                                foreach ($index['OPERATION'] as $op) {
                                    if (($vals[$op]['attributes']['TYPEID'] == 'CS')
                                        && ($vals[$op+1]['attributes']['ID'] == $id_m))
                                    {
                                        $vals = move_vals($vals, $op+2, 1);
                                        $vals[$op+2] = Array (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes" => Array ("ID" => $new_id_f)
                                        );
                                        ksort($vals);
                                        $index = make_index($vals);
                                    }
                                }

                            }
                        }

                        foreach ($index['PART'] as $i1) {
                            if (($vals[$i1]['attributes']['MY_DOUBLE_ID'] == $did)
                                && ($vals[$i1]['attributes']['MY_DOUBLE_FACE'] == 0))
                            {
                                $vals[$i1]['attributes']['MY_DOUBLE_PARENT'] = $new_id_f;
                                $vals[$i1]['attributes'][mb_strtolower('part.MY_DOUBLE_PARENT')] = $new_id_f;
                            }
                            if ((!isset($vals[$i1]['attributes']['CL']))
                                && ($vals[$i1]['attributes']['L'] > 0)
                                && ($vals[$i1]['attributes']['MY_DOUBLE_ID'] == $did))
                            {
                                $id = $vals[$i1]['attributes']['ID'];
                                $vals[$i1]['attributes']['ID'] = new_id($vals) + mt_rand(10000, 20000);

                                foreach ($index['PART'] as $j) {
                                    if ((!isset($vals[$j]['attributes']['CL']))
                                        && !isset($vals[$j]['attributes']['L'])
                                        && ($vals[$j]['attributes']['ID'] == $id))
                                    {
                                        $vals[$j]['attributes']['ID'] = $vals[$i1]['attributes']['ID'];
                                    }
                                }

                            }
                        }

                    }
                    
                }
            }
        }
        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'step_1 double correction', $session);
        ksort($vals);
        $index = make_index($vals);
        $vals = xnc_right ($vals);
        $vals = xnc_code($vals,$session);
        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'step_1 xnc_code', $session);

        $content["project_data"] = vals_index_to_project($vals);

        foreach ($index["OPERATION"] as $i) {
            if ($vals[$i]["attributes"]["TYPEID"] == 'CS') {
                if ($vals[$i+1]['tag'] == "MATERIAL") {
                    
                    foreach ($index['GOOD']as $j) {
                        if (($vals[$i+1]['attributes']['ID'] == $vals[$j]['attributes']['ID'])
                            && ($vals[$j]['attributes']['TYPEID'] == 'sheet'))
                        {
                            if (($vals[$j]['attributes']['CODE'] > 0) && (is_numeric($vals[$j]['attributes']['CODE']))) {

                                $sql = "SELECT * FROM MATERIAL WHERE CODE = " . $vals[$j]['attributes']['CODE'];
                                $mat_id = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0]['MATERIAL_ID'];
                                if (($mat_id > 0) && ($session['user_place'] > 0)) {
                                    $sql = "SELECT * FROM MATERIAL_HALF WHERE material_id = " . $mat_id . ' AND place_id=' . $session['user_place'];
                                    $half = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['count'];

                                    if (($half == 1)
                                        && ((!isset( $vals[$i]['attributes']['operation.my_krat_sheet']))
                                        OR ( $vals[$i]['attributes']['operation.my_krat_sheet'] == 'half')))
                                    {
                                        $vals[$i]["attributes"]["CSDIRECTCUT"] = "3";

                                    } else $vals[$i]["attributes"]["CSDIRECTCUT"] = "0";

                                } else $vals[$i]["attributes"]["CSDIRECTCUT"] = "0";

                            } else $vals[$i]["attributes"]["CSDIRECTCUT"] = "0";
                            
                        }
                    }
                }

                $test1111++;
                if ($test1111 == 1) $vals[$i]["attributes"]["STARTNEWPAGE"] = "false";
                else $vals[$i]["attributes"]["STARTNEWPAGE"] = "true";
                $vals[$i]["attributes"]["CFILLREP"] = "0.9";
                $vals[$i]["attributes"]["CCOSTTYPE"] = "0";
                $vals[$i]["attributes"]["CCOSTCUT"] = "0";
                $vals[$i]["attributes"]["SWMAXTURNS"] = "6";
                $vals[$i]["attributes"]["CCOSTBYITEM"] = "false";
                $vals[$i]["attributes"]["CCOSTBYITEMROUND"] = "22";
                $vals[$i]["attributes"]["CMINDIMDETAIL"] = "30";

                if (($vals[$i]["attributes"]["operation.my_fix_card"] == 1) && (!isset($vals[$i]['attributes']['DATA']))) {
                    
                    foreach ($index['GOOD'] as $v11) {
                        if ($vals[$v11]['attributes']['ID'] == $vals[$i+1]['attributes']['ID']) $e4 = $vals[$v11];
                    }

                    $_SESSION['error'] .= "<br>Операция раскроя (id='" . $e4["attributes"]["ID"] . "') (материал '" . $e4['attributes']['NAME'] . "') (L " . $e4["attributes"]["L"] . " * W " . $e4["attributes"]["W"] . ")
                        - был установлен признак фиксации раскроя для производства. В операции пока не создан раскрой. Создайте раскрой либо уберите признак фиксации в дополнительных операциях.
                        <hr>";
                }
                $vals[$i]["attributes"][mb_strtoupper("cSaveDrawCut")] = "true";

                if ((!isset($vals[$i]['attributes']['DATA'])) || ($vals[$i]['attributes']['DATA'] == '')) {
                    unset ($vals[$i]['attributes']['DRAW']);
                    $i1 = $i + 2;
                    
                    while ($vals[$i1]['tag'] == "PART") {
                        unset($check);

                        foreach ($index['PART'] as $r) {
                            if (($vals[$r]['attributes']['SHEETID'] > 0)
                                && ($vals[$r]['attributes']['ID'] == $vals[$i1]['attributes']['ID']))
                            {
                                $rr = $vals[$i1]['attributes']['ID'];

                                foreach ($index['PART'] as $j) {
                                    if ($vals[$j]['attributes']['ID'] == $rr) unset($vals[$j]);
                                }

                                unset ($vals[$i1]);
                            } elseif (($vals[$r]['attributes']['ID'] == $vals[$i1]['attributes']['ID'])
                                && ($vals[$r]['attributes']['L'] > 0))
                            {
                                $vals[$r]['attributes']['USEDCOUNT'] = 0;
                                $check = 1;
                            } elseif (!isset($vals[$r]['attributes']['SHEETID'])
                                && ($vals[$r]['attributes']['L'] > 0))
                            {
                                $check = 1;
                            }
                        }

                        if (!isset($check)) unset($vals[$i1]);
                        $i1++;
                        ksort($vals);
                        $index = make_index($vals);
                    }

                }
            } elseif ($vals[$i]["attributes"]["TYPEID"] == "EL") {
                
                $vals[$i]["attributes"]["PRINTABLE"] = 'false';
                
                if($vals[$i]['attributes']['MY_CHECK_KL']) $vals[$i]["attributes"]["ELWASTEPRC"] = '0.15';
                else $vals[$i]["attributes"]["ELWASTEPRC"] = '0.1';

                $vals[$i]["attributes"]["ELROUNDLENGTH"] = '1';
                $vals[$i]["attributes"]["ELMINDIMDETAIL"] = '30';

            } elseif ($vals[$i]["attributes"]["TYPEID"] == "GR") {
                
                if ($vals[$i]["attributes"]["GRWIDTH"] < 4)$vals[$i]["attributes"]["GRWIDTH"] = 4;
                if ($vals[$i]["attributes"][mb_strtolower("grSawthick")] <> 4)$vals[$i]["attributes"][mb_strtolower("grSawthick")] = 4;

            } elseif (($vals[$i]["attributes"]["TYPEID"] == "XNC") && ($vals[$i]["type" == "open"])) {
                $xnc_op_12345 = "1";
            }
            unset($j);

            if ($vals[$i+1]['tag'] == "MATERIAL") $j = $i + 2;
            elseif ($vals[$i+1]['tag'] == "PART") $j = $i + 1;
            
            if($j > 0) {
                $jjj = $j;

                while ($vals[$j]['tag'] == "PART") {
                    $new_part[] = $vals[$j];
                    unset($vals[$j]);                
                    $j++;
                }

                foreach($new_part as $ppp) {
                    if (!in_array($ppp, $pppp)) $pppp[] = $ppp;
                }

                $new_part = $pppp;
                unset ($ppp, $pppp);

                foreach ($new_part as $value) {
                    $vals[$jjj] = $value;
                    $jjj++;
                }

                unset ($jjj,$new_part);
            }
        }
        ksort ($vals);

        $index = make_index($vals);
        $ar_temp = array('l', 'r', 't', 'b');
        $ar_temp_text = array('левое', 'правое', 'верхнее', 'нижнее');
        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'step_1 correct _ operation', $session);

        foreach ($index["PART"] as $i) {
            if (isset($vals[$i]['attributes']['NAME'])) {
                $vals[$i]['attributes']['part.name1'] = $vals[$i]['attributes']['NAME'];
                $vals[$i]['attributes']['NAME'] = str_replace('=', ' ', $vals[$i]['attributes']['NAME']);
                $vals[$i]['attributes']['NAME'] = str_replace('%', ' ', $vals[$i]['attributes']['NAME']);
                $vals[$i]['attributes']['NAME'] = str_replace('*', ' ', $vals[$i]['attributes']['NAME']);
            }
            if ($vals[$i]['attributes']['CL'] > 0) {
                $vals[$i]['attributes']['part.my_label'] = 'new';
                $e4 = part_get_material($vals, $vals[$i]["attributes"]["ID"]);
                $vals[$i]['attributes']['T'] = $e4['attributes']['T'];

                if (isset($e4['attributes']['MATERIAL_ID'])) $vals[$i]['attributes']['part.my_material_id'] = $e4['attributes']['MATERIAL_ID'];
                if (isset($e4['attributes']['CODE'])) $vals[$i]['attributes']['part.my_material_code'] = $e4['attributes']['CODE'];
                if ($vals[$i]['attributes']['part.my_postforming_cut_top'] == 1) {

                    if ($vals[$i]['attributes']['CW'] > $e4['attributes']['W']) {

                        $dddd = $vals[$i]['attributes']['CW'] - $e4['attributes']['W'];
                        
                        $_SESSION['error_ok'] .= "<br>Деталь '" . $vals[$i]["attributes"]["NAME"] . "' (id='" . $vals[$i]["attributes"]["ID"] . "') (материал '" . $e4['attributes']['NAME'] . "') (L " . $vals[$i]["attributes"]["CL"] . " * W " . $vals[$i]["attributes"]["CW"] . ")
                        - был установлен срез закругления столешницы для использования шаблонов ЧПУ в дополнительных операциях. Это подразумевает срез 20 мм детали сверху. 
                        Был ошибочно изменён размер детали (увеличен без учёта среза закругления). Размеры детали приведены в соответствие размеру материала и срезу закругления.
                        Если необходимо убрать срез закругления и использовать шаблоны ЧПУ только для столешниц для этой детали,
                        перейдите в дополнительные операции и уберите опцию среза закругления.
                        <hr>";

                        $vals[$i]['attributes']['CW'] -= $dddd;
                        $vals[$i]['attributes']['DW'] -= $dddd;
                        $vals[$i]['attributes']['JW'] -= $dddd;
                        $vals[$i]['attributes']['W'] -= $dddd;
                    }
                    if ($vals[$i]['attributes'][mb_strtoupper('allowanceT')] <> 20) {

                        $dddd = $vals[$i]['attributes'][mb_strtoupper('allowanceT')] - 20;
                        $vals[$i]['attributes'][mb_strtoupper('allowanceT')] = $vals[$i]['attributes'][mb_strtoupper('allowanceT')] - $dddd;
                        $vals[$i]['attributes']['DW'] -= 20;
                        $vals[$i]['attributes']['JW'] -= 20;
                        $vals[$i]['attributes']['W'] -= 20;

                        $_SESSION['error_ok'] .= "<br>Деталь '" . $vals[$i]["attributes"]["NAME"] . "' (id='" . $vals[$i]["attributes"]["ID"] . "') (материал '" . $e4['attributes']['NAME'] . "') (L " . $vals[$i]["attributes"]["CL"] . " * W " . $vals[$i]["attributes"]["CW"] . ")
                        - был установлен срез закругления столешницы для использования шаблонов ЧПУ в дополнительных операциях. Это подразумевает срез 20 мм детали сверху. 
                        Срез был восстановлен, размеры детали уменьшены на 20 мм. Если необходимо убрать срез закругления и использовать шаблоны ЧПУ только для столешниц для этой детали,
                        перейдите в дополнительные операции и уберите опцию среза закругления.
                        <hr>";
                    }
                }
                $get_edges = part_get_edges($vals, $vals[$i]['attributes']['ID']);
                
                foreach ($ar_temp as $h2 => $h1) {
                    //.. В условие добавлено, что если сдвиг при срезе торца больше 3-х мм, то кромку не удаляем
                    if ((isset($vals[$i]['attributes']['MY_CUT_ANGLE_' . mb_strtoupper($h1)]))
                        &&(isset($vals[$i]['attributes']['EL' . mb_strtoupper($h1)]))
                        && isset($vals[$i]['attributes']['MY_CUT_ANGLE_SDVIG_' . mb_strtoupper($h1)])
                        && $vals[$i]['attributes']['MY_CUT_ANGLE_SDVIG_' . mb_strtoupper($h1)] < 1)
                    {
                        foreach ($ar_temp as $h11) {
                            if ($h11 <> $h1) {
                                if ($vals[$i]['attributes']['EL' . mb_strtoupper($h1)] == $vals[$i]['attributes']['EL' . mb_strtoupper($h11)])
                                {
                                    $no = 1;
                                }
                            }
                        }

                        if (!isset($no)) {
                            $vals = del_part_in_el_op($vals, $vals[$i]['attributes']['ID'], substr($vals[$i]['attributes']['EL' . mb_strtoupper($h1)], 11));
                        }
                        unset ($no);

                        $vals[$i] = my_uncet($vals[$i], 'el' . $h1);
                        $vals[$i] = my_uncet($vals[$i], 'my_kl_el' . $h1);
                        $vals[$i] = my_uncet($vals[$i], 'my_kl_el' . $h1 . "_code");
                        $vals[$i] = my_uncet($vals[$i], 'my_db_el' . $h1);
                        $vals[$i] = my_uncet($vals[$i], 'my_db_kl_el' . $h1);
                        $_SESSION["error_ok"].="<br>Для детали id= " . $vals[$i]['attributes']['ID'] . " " . $ar_temp_text[$h2] . " кромкование (" . $vals[$i]["attributes"]["ELTMAT"] . ") убрано по стороне, где есть зарез под углом (и сдвиг меньше, чем 3мм).. Увидеть изменения можно в дополнительных операциях.<hr>"; 
                    }

                    if(isset($vals[$i]['attributes']['part.my_side_to_cut_'.$h1])) {
                    
                        $vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_'.$h1)] = $vals[$i]['attributes']['part.my_side_to_cut_'.$h1];
                        $vals[$i]['attributes'][mb_strtoupper('my_side_to_cut_'.$h1)] = $vals[$i]['attributes']['part.my_side_to_cut_'.$h1];
                    
                    } elseif(isset($vals[$i]['attributes'][mb_strtoupper('my_side_to_cut_'.$h1)])) {

                        $vals[$i]['attributes']['part.my_side_to_cut_'.$h1] = $vals[$i]['attributes'][mb_strtoupper('my_side_to_cut_'.$h1)];

                    }

                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_l']))
                    {
                        $cl = $vals[$i]['attributes']['part.my_side_to_cut_l'];
                    
                    } elseif (isset($vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_l')]))
                    {
                        $cl=$vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_l')];
                    
                    } elseif (isset($vals[$i]['attributes']['part.my_side_to_cut_r']))
                    {
                        $cl=$vals[$i]['attributes']['part.my_side_to_cut_r'];

                    } elseif (isset($vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_r')]))
                    {
                        $cl = $vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_r')];

                    } else $cl=$vals[$i]['attributes']['CL'];

                    if (isset($vals[$i]['attributes']['part.my_side_to_cut_t']))
                    {
                        $cw = $vals[$i]['attributes']['part.my_side_to_cut_t'];

                    } elseif (isset($vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_t')]))
                    {
                        $cw = $vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_t')];

                    } elseif (isset($vals[$i]['attributes']['part.my_side_to_cut_b']))
                    {
                        $cw = $vals[$i]['attributes']['part.my_side_to_cut_b'];
                    } elseif (isset($vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_b')]))
                    {
                        $cw = $vals[$i]['attributes'][mb_strtoupper('part.my_side_to_cut_b')];
                    } else $cw = $vals[$i]['attributes']['CW'];

                    if (isset($vals[$i]['attributes']['EL'.mb_strtoupper($h1)])) {
                        if ((($cw <= 60)
                            && (($h1 == "t")
                            OR ($h1 == "b")))
                            OR (($cl <= 60)
                            && (($h1 == "l")
                            OR ($h1 == "r")))
                            OR ($vals[$i]['attributes']['MY_XNC_SIDE_'.mb_strtoupper($h1)] > 0))
                        {
                            
                            foreach ($index["OPERATION"] as $j) {
                                if (($vals[$j]["attributes"]["TYPEID"] == "EL")
                                    && ($vals[$i]['attributes']['EL'.mb_strtoupper($h1)] == "@operation#" . $vals[$j]["attributes"]["ID"]))
                                {
                                    
                                    foreach($index['GOOD'] as $d) {
                                        if (($vals[$d]['attributes']['TYPEID'] == "band")
                                            && ($vals[$d]['attributes']['ID'] == $vals[$j+1]['attributes']['ID'])
                                            && ($vals[$d]['attributes']['CODE'] > 0))
                                        {
                                            $arr_out[$vals[$i]['attributes']['ID']]['part_id'] = $vals[$i]['attributes']['ID'];
                                            $arr_out[$vals[$i]['attributes']['ID']]['xnc_band_'.$h1] = $vals[$d]["attributes"]["CODE"];
                                            
                                            if (($h1 == "r") OR ($h1 == "l")) {
                                                $rr = $cl;
                                            } else $rr = $cw;

                                            if (!isset($vals[$j]['attributes']['MY_CHECK_KL'])) {
                                                if ($rr < 60)
                                                {
                                                    $_SESSION["error_ok"] .= "<br>Для детали id= " . $vals[$i]['attributes']['ID'] . " " . $ar_temp_text[$h2] . " кромкование (" . $vals[$d]["attributes"]["NAME"] . ") заменено на криволинейное, так как размер детали по указанной стороне менее 60 мм , текущий пильный размер  " . $rr . " мм. Название криволинейного кромкования начинается с 'КЛ'. Вы можете изменить размер детали и сменить кромкование на обычное. Криволинейное кромкование делается только ЭВА.<hr>";

                                                } elseif (isset($vals[$i]['attributes']['MY_XNC_SIDE_'.mb_strtoupper($h1)]))
                                                {
                                                    $_SESSION["error_ok"] .= "<br>Для детали id= " . $vals[$i]['attributes']['ID'] . " " . $ar_temp_text[$h2] . " кромкование (" . $vals[$d]["attributes"]["NAME"] . ") заменено на криволинейное, так как эта сторона затрагивается фрезерным резом. Название криволинейного кромкования начинается с 'КЛ'. Криволинейное кромкование делается только ЭВА.<hr>"; 
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        } else {

                            foreach ($index["OPERATION"] as $j) {
                                if (($vals[$j]["attributes"]["TYPEID"] == "EL")
                                    && ($vals[$i]['attributes']['EL'.mb_strtoupper($h1)] == "@operation#" . $vals[$j]["attributes"]["ID"]))
                                
                                {
                                    
                                    foreach($index['GOOD'] as $d) {
                                        if (($vals[$d]['attributes']['TYPEID'] == "band")
                                            && ($vals[$d]['attributes']['ID'] == $vals[$j+1]['attributes']['ID'])
                                            && ($vals[$d]['attributes']['CODE'] > 0))
                                        {
                                            if (isset($vals[$j]['attributes']['MY_CHECK_KL'])) {

                                                $arr_out_kl_del[$vals[$i]['attributes']['ID']]['part_id']=$vals[$i]['attributes']['ID'];
                                                $arr_out_kl_del[$vals[$i]['attributes']['ID']]['band_'.$h1]=$vals[$d]["attributes"]["CODE"];
                                                $_SESSION["error_ok"].="<br>Для детали id= ".$vals[$i]['attributes']['ID']." ".$ar_temp_text[$h2]." кромкование (".$vals[$d]["attributes"]["NAME"].") заменено на прямолинейное, так как нет необходимости в криволинейном кромковании по этой стороне.<hr>"; 
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        ksort ($vals);
        unset($mat_kl_in);
        
        foreach ($index["GOOD"] as $i){
            if (($vals[$i]["attributes"]["TYPEID"] == "sheet")
                && ($vals[$i]['attributes']['MY_1C_NOM'] == 5931)
                && ($vals[$i]['attributes']['good.my_client_material'] <> "1")
                && (!isset($vals[$i]['attributes']['MY_FIRST'])))
            {
                $mat_kl_in[] = $vals[$i]["attributes"]["ID"];
            }
        }

        if (count($mat_kl_in) > 0) {
            $_SESSION["error_ok"] .= "<br>
            Материал клиента (id = " . implode(',', $mat_kl_in) . ") использован для выставления счёта, он не привязан к реальному остатку на складе. Заказ не может быть размещён в производство без внесения нужных остатков. 
            Для размещения в производство необходимо оприходовать материал клиента на складе, внести реальный остаток в проект. 
            <hr> "; 
        }
        
        if (count($arr_out) > 0) $vals = change_kl_edge($vals, $arr_out, $link);
        ksort($vals);

        if (count($arr_out_kl_del) > 0) $vals = change_st_edge($vals, $arr_out_kl_del, $link);
        $vals = edge_calc($vals);
        ksort($vals);

        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'step_1 correct part_band', $session);

        ksort($vals);
        $index = make_index($vals);

        $vals[0]['attributes']['project.my_label'] = "new";
        $vals[0]['attributes']['MY_LABEL'] = "new";
        $vals[0]['attributes']['saveXmlCut'] = 'true';
        $vals = vals_out($vals);
        ksort($vals);

        $index = make_index($vals);

        $vals = add_tag_print($vals);
        $timer = timer(1,__FILE__,__FUNCTION__,__LINE__, 'step_1 correct material_band_client', $session);
        
        foreach ($index['PART'] as $i) {
            if ($vals[$i]['attributes']['CL'] > 0) {
                $part = get_all_part($vals,$vals[$i]['attributes']['ID']);
                unset ($code);
                
                if (count($part['xnc']) > 2) {
                    foreach ($part['xnc'] as $ar) $code .= ' [' . $ar['attributes']['CODE'] . '] ';
                }

                $vals[$i]['attributes']['part.xnc_many_op'] = $code;
            }
        }
        
        $_SESSION['edge_mass'] = edge_mass($vals, $link);
        $vals = change_edge_from_mass($vals, $_SESSION['edge_mass'], $session, $link);
        $timer = timer(1, __FILE__,__FUNCTION__,__LINE__, 'step_1 correct end_step1', $session);
        ksort($vals);
        $content["vals"] = $vals;
        $content["project_data"] = vals_index_to_project($vals);
    }
    return $content;
}

?>