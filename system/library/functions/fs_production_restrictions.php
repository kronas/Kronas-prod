<?php

require_once DIR_FUNCTIONS . 'fs_restrictions/checkEdgeSizesRestrictions.php';
require_once DIR_FUNCTIONS . 'fs_restrictions/checkSizesRestrictions.php';
require_once DIR_FUNCTIONS . 'fs_restrictions/checkXncTemplatesRestrictions.php';
require_once DIR_FUNCTIONS . 'fs_restrictions/checkToolsRestrictions.php';
require_once DIR_FUNCTIONS . 'fs_restrictions/rollback_xnc.php';
$_SESSION['additions_errors_ok'] = '';

// Этот подмассив меняется обработчиком шаблона прямоугольного выреза.
// Сюда записывается изменённый массив проекта (меняются только размеры детали с вырезом, и параметры обрабртки программы выреза)
// Порядок ключей массива, айдишники, коды операций и прочее НЕ МЕНЯЮТСЯ!
$_SESSION['vals_after_change'] = '';

/**
 * Функция проверяет XNC-обработки на наличие производственных ограничений
 * Это главная функция проверок, которая подключает другие проверки, разбитые по типам
 * и расположенных в папке "fs_restrictions", которая расположена "system/library/functions/".
 * 
 * (пока только критические ошибки, т.е. если не сработали проверки выше)
 */
function checkRestrictions($vals) {

    // $startTimeLabel = strtotime('now');

    $errors = '';
    $place = $_SESSION['user_place'];
    $allDetails = getDetailsForRestrictions($vals);
    $xncOperations = getXncOperationIndexes($vals);
    $index = make_index($vals);
    $wrongBoreTools = []; // Сюда записываются все инструменты сверления, присутствующие в проекте

    // Здесь проверяем ограничения по минимальным размерам кромкуемой детали, на которой нет XNC-обработок
    // Функция увеличивает детали, либо возвращает к изначальному размеру, если кромка была удалена
    // Возвращает пустую строку, так что к $errors ничего не добавляется, просто как заготовка для возможных возвратов.
    // Функция может менять массив $vals, в таком случае изменённый массив записывается в $_SESSION['vals_after_change']
    // Отключено по просьбе производства
    // $errors .= checkEdgeSizesRestrictions($vals, $xncOperations, $allDetails);

    if (!empty($xncOperations)) {

        // Сначала проверяем ограничения по размерам, если какая-либо деталь не проходит проверку,
        // то дальше идти нет смысла, и просто возврвщаем ошибки
        $tmp = '';
        $tmp = checkSizesRestrictions($vals, $xncOperations, $allDetails, $place);
        if (!empty($tmp)) {
            $errors = $tmp;
            return $errors;
        }

        foreach ($xncOperations as $vIndex) {

            // Если функция checkXncTemplatesRestrictions() изменяет массив проекта,
            // то дальше работаем уже с изменённым
            if (!empty($_SESSION['vals_after_change'])) $vals = $_SESSION['vals_after_change'];

            $program = getParsedProgram($vals[$vIndex]['attributes']['PROGRAM']);
            // Проверяем операции проекта на несуществующие инструменты сверления
            getWrongBoreTools($vals, $vIndex, $program, $allDetails, $wrongBoreTools);
            $programIndex = $program['Pindex'];
            $programVals = $program['Pvals'];

            // Здесь проверяем наличие шаблонов (признак - наличие четырёхзначного числа вида [/0000/])
            // Если есть, то вызываем обработчик
            if (isset($programIndex['CALL'])) {
                foreach ($programIndex['CALL'] as $vals_i) {
                    if ($programVals[$vals_i]['type'] == 'open') {
                        if (preg_match('#(/\d{4}/)#', $programVals[$vals_i]['attributes']['NAME'])) {
                            $errors .= checkXncTemplatesRestrictions($vals, $vIndex, $program, $allDetails, $place);
                            break;
                        }
                    }
                }
            }
        }

        // Если в проекте есть несуществующие инструменты сверления
        if (!empty($wrongBoreTools)) {
            $errors .= getWrongBoreToolsErrors($wrongBoreTools);
        }
    }

    // Здесь проверяем детали, если для детали была операция с вырезом (шаблон [/0042/])
    // и менялись размеры детали, а потом операция была удалена, то возвращаем размеры такой детали к прежним.
    $allDetailsFromVals = getAllDetails($vals);
    returnToOriginalSize($vals, $allDetailsFromVals);
    // de('Stop');
    // de(strtotime('now') - $startTimeLabel);

    return $errors;
}

/**
 * Функция парсит XML-строку XNC-программы и возвращает массив
 */
function getParsedProgram($program) {
    $p = xml_parser_create();
    xml_parse_into_struct($p, $program, $Pvals, $Pindex);
    xml_parser_free($p);
    return ['Pvals' => $Pvals, 'Pindex' => $Pindex];
}

/**
 * Функция возвращает массив всех деталей
 * Ключом каждого массива является порядковый номер детали в массиве $vals
 */
function getAllDetails($vals) {

    $output = [];
    $index = make_index($vals);

    foreach ($index['PART'] as $i) {
        if ($vals[$i]['type'] === 'complete' && isset($vals[$i]['attributes']['CL']) && isset($vals[$i]['attributes']['CW'])) {
            $output[$i] = $vals[$i];
        }
    }
    return $output;
}

/**
 * Функция возвращает массив деталей (ключи массива - ID деталей) с необходимыми данными:
 *  название детали
 *  ID детали
 *  размеры детали
 *  материал детали
 * 
 * Функция является вспомогательной для "checkRestrictions()"
 */
function getDetailsForRestrictions($vals) {

    $index = make_index($vals);

    $output = [];
    $detailsById = [];
    $detailsByNum = [];

    // Собираем сразу детали и частично заполняем массивы данными (название изделия, ID, номер, размеры и кол-во)
    $i = 0;
    foreach ($index['GOOD'] as $g_ind) {
        if ($vals[$g_ind]['attributes']['TYPEID'] === 'product') {
            $i = $g_ind + 1;
            while ($vals[$i]['tag'] === 'PART') {
                $detId = $vals[$i]['attributes']['ID'];
                $detailsById[$detId] = [
                    'valsNum' => $i,
                    'detId' => $detId,
                    'detName' => (isset($vals[$i]['attributes']['NAME'])) ? $vals[$i]['attributes']['NAME'] : '',
                    'l' => $vals[$i]['attributes']['DL'],
                    'w' => $vals[$i]['attributes']['DW']
                ];
                $edges = edgesByDetail($vals, $vals[$i]['attributes']);
                if (!empty($edges)) $detailsById[$detId]['edges'] = $edges;
                $i++;
            }
        }
    }

    // Тутэчки дозаполняем массивы деталей материалами и номерами
    foreach ($index['OPERATION'] as $op_ind) {

        if ($vals[$op_ind]['attributes']['TYPEID'] === 'CS') {

            // Определяем материал
            $matInfo = [
                'code' => null,
                'name' => null,
                'w' => null,
                'l' => null,
                't' => null
            ];
            for ($j = $op_ind; $j < ($op_ind + 100); $j++) { 
                if ($vals[$j]['tag'] === 'MATERIAL') {
                    $matId = $vals[$j]['attributes']['ID'];

                    foreach ($index['GOOD'] as $g_ind) {
                        if ($vals[$g_ind]['attributes']['TYPEID'] === 'sheet' && $vals[$g_ind]['attributes']['ID'] === $matId) {
                            $matInfo['code'] = $vals[$g_ind]['attributes']['CODE'];
                            $matInfo['name'] = $vals[$g_ind]['attributes']['NAME'];
                            $matInfo['w'] = $vals[$g_ind]['attributes']['W'];
                            $matInfo['l'] = $vals[$g_ind]['attributes']['L'];
                            $matInfo['t'] = $vals[$g_ind]['attributes']['T'];
                            break 2;
                        }
                    }
                }
            }

            $i = $op_ind + 1;
            if ($vals[$i]['tag'] === 'MATERIAL') $i++;

            $j = 1;
            while ($vals[$i]['tag'] === 'PART') {
                $partId = $vals[$i]['attributes']['ID'];
                if (isset($detailsById[$partId])) {
                    $detailsById[$partId]['detNum'] = $j;
                    $detailsById[$partId]['material'] = $matInfo;

                    $detailsByNum[] = $detailsById[$partId];
                }
                $j++;
                $i++;
            }
        }
    }
    
    $output = $detailsById;

    return $output;
}

/**
 * Функция возвращает индексы XNC-операций массива $vals
 */
function getXncOperationIndexes($vals) {

    $index = make_index($vals);
    $output = [];

    foreach ($index['OPERATION'] as $k => $i) {
        if ($vals[$i]['type'] == 'open' && $vals[$i]['attributes']['TYPEID'] == 'XNC') {
            $output[$k] = $i;
        }
    }

    return $output;
}

/**
 * Функция возвращает массивы XNC-операций массива $vals
 * Ключом каждого массива является порядковый номер операции в массиве $vals
 */
function getXncOperations($vals) {

    $index = make_index($vals);
    $output = [];

    foreach ($index['OPERATION'] as $k => $i) {
        if ($vals[$i]['type'] == 'open' && $vals[$i]['attributes']['TYPEID'] == 'XNC') {
            $output[$i] = $vals[$i];
        }
    }

    return $output;
}

/**
 * Функция возвращает толщины и имена (отдельным массивом) кромок для сторон детали
 * Параметры:
 * $vals - общий массив проекта
 * $attr - атрибуты детали
 */
function edgesByDetail($vals, $attr) {
    $index = make_index($vals);
    $tmp = [];
    $output = [];

    foreach ($index['OPERATION'] as $k => $i) {
        if ($vals[$i]['type'] == 'open' && $vals[$i]['attributes']['TYPEID'] == 'EL') {
            $opId = $vals[$i]['attributes']['ID'];
            $tmp[$opId]['opId'] = $opId;

            $j = $i + 1;
            while ($vals[$j]['attributes']['TYPEID'] == 'MATERIAL') {
                $j++;
            }
            $bandMat = $vals[$j]['attributes']['ID'];
            foreach ($index['GOOD'] as $key => $band) {
                if ($vals[$band]['type'] == 'complete' && $vals[$band]['attributes']['TYPEID'] == 'band' && $vals[$band]['attributes']['ID'] == $bandMat) {
                    $tmp[$opId]['bandCode'] = $vals[$band]['attributes']['CODE'];
                    $tmp[$opId]['bandName'] = $vals[$band]['attributes']['NAME'];
                    $tmp[$opId]['bandW'] = $vals[$band]['attributes']['W'];
                    $tmp[$opId]['bandT'] = $vals[$band]['attributes']['T'];
                    break;
                }
            }
        }
    }

    if (isset($attr['ELL'])) {
        $opId = str_replace('@operation#', '', $attr['ELL']);
        $output['L'] = $tmp[$opId]['bandT'];
        $output['names']['L'] = $tmp[$opId]['bandName'];
    }
    if (isset($attr['ELB'])) {
        $opId = str_replace('@operation#', '', $attr['ELB']);
        $output['B'] = $tmp[$opId]['bandT'];
        $output['names']['B'] = $tmp[$opId]['bandName'];
    }
    if (isset($attr['ELT'])) {
        $opId = str_replace('@operation#', '', $attr['ELT']);
        $output['T'] = $tmp[$opId]['bandT'];
        $output['names']['T'] = $tmp[$opId]['bandName'];
    }
    if (isset($attr['ELR'])) {
        $opId = str_replace('@operation#', '', $attr['ELR']);
        $output['R'] = $tmp[$opId]['bandT'];
        $output['names']['R'] = $tmp[$opId]['bandName'];
    }
    ksort($output);

    return $output;
}

/**
 * Функция возвращает текст ошибки, указанной в параметре
 */
function getErrorMessage($num, $lang = 'Ua') {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $restrictionErrors[$lang][$num];
}

/**
 * Функция возвращает текст предупреждения, указанного в параметре
 */
function getWarningMessage($num, $lang = 'Ua') {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $restrictionWarnings[$lang][$num];
}

// Далее идут функции, каждая из которых возвращает параметры для конкретного ограничения
// ################################################################################################################

/**
 * Функция возвращает массив ограничений (минимальные размеры для деталей)
 * по кромкованным деталям
 */
function getEdgeSizeRestrictions() {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $minEdgeDetSizeRestrictions;
}

/**
 * Функция возвращает массив ограничений (минимальные и максимальные размеры для деталей)
 * по фрезерным обработкам
 */
function getMillingRestriction() {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $millingRestrictions;
}

/**
 * Функция возвращает массив ограничений (минимальные и максимальные размеры для деталей)
 * по сверлильным обработкам
 */
function getDrillingRestriction() {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $drillingRestrictions;
}

/**
 * Функция возвращает массив ограничений (минимальные размеры для радиусов)
 */
function getEdgeRadiusRestriction() {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $edgeRadius;
}

/**
 * Функция возвращает массив ограничений (минимальные размеры для радиусов)
 */
function getRadiusHandlesRestriction() {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $edgeRadiusHandles;
}

/**
 * Функция возвращает ограничения по шаблонам
 * $num type = string
 */
function getTemplateRestriction($num) {
    require DIR_SYSTEM . 'config/production_restrictions.php';
    return $templatesRestrictions[$num];
}

?>