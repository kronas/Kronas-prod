<?php
/**
 * st(St) - сокращение от stitching
 * mat(Mat) - сокращение от material
 * det(Det) - сокращение от detail
 **/
// Массив для кромок (поменялась логика, не используется)
$sides = ['t', 'r', 'b', 'l'];

$basisTemplates = [
	'startXml' => '<?xml version="1.0" encoding="UTF-8"?>',
	'startProject' => [
		'tag' => 'project',
		'type' => 'open',
		'level' => 1,
		'attributes' => [
			'name' => 'New',
			'currency' => 'грн',
			'version' => 1,
			'costOperation' => 0,
			'costMaterial' => 0,
			'cost' => 0
		]
	],
	// Блок помещается внутрь операции раскроя
	'simplePart' => [
		'tag' => 'PART',
        'type' => 'complete',
        'level' => 3,
        'attributes' => [
			'id' => null
        ]
	],
	// Блок помещается внутрь операции раскроя
	'simpleMaterial' => [
		'tag' => 'MATERIAL',
        'type' => 'complete',
        'level' => 3,
        'attributes' => [
			'id' => null
        ]
	],
	// Не парный GOOD для порезки сшивки
	// Не используется, вместо него используется 'stMatToolCutting'
	'stMatToolCuttingFull' => [
		'tag' => 'good',
		'type' => 'complete',
		'level' => 2,
		'attributes' => [
			'typeId' => 'tool.cutting',
			'swMinSizeBand' => 0,
			'swMinPruning' => 0,
			'swTrimIncludeSaw' => false,
			'swMaxSizeBand' => 0,
			'swSortInBand' => 0,
			'swSort' => 2,
			'swComplexBand' => false,
			'swPackageHeight' => 60,
			'swMaxLengthBand' => 4500,
			'swSawthick' => 4.8,
			'swMaxturns' => 4,
			'id' => null,
			'type' => 'auto',
			'product' => 'sh',
			'name' => '380 Holtzma пильное производство',
			'typeName' => 'Раскроечный станок',
			'tool_cutting_equipment_id' => 1,
			'year' => 2006,
			'place' => 1,
			'lc4' => 1,
			'gibcut' => 0,
			'local_folder' => 'files/ps_share/SAW/KV3_HPP380/',
			'description' => 'Порезка основной части сшивки',
			'MY_DOUBLE_ID' => 4
		]
	],
	'stMatToolCutting' => [
		'tag' => 'good',
		'type' => 'complete',
		'level' => 2,
		'attributes' => [
			'typeId' => 'tool.cutting',
			'swMinSizeBand' => 0,
			'swMinPruning' => 0,
			'swTrimIncludeSaw' => 'false',
			'swMaxSizeBand' => 0,
			'swSortInBand' => 0,
			'swSort' => 2,
			'swComplexBand' => 'false',
			'swPackageHeight' => 65,
			'swMaxLengthBand' => 5500,
			'swSawthick' => 4.8,
			'swMaxturns' => 4,
			'id' => null,
			'description' => 'Порезка основной части сшивки',
			'type' => 'auto',
			'product' => 'sh',
			'MY_DOUBLE_ID' => 4
		]
	],
	// Парная деталь-заготовка для сшивки для парного GOOD.product
	'stSrcDetPart' => [
		'tag' => 'PART',
        'type' => 'complete',
        'level' => 3,
        'attributes' => [
        	'l' => null,
			'w' => null,
			'dl' => null,
			'dw' => null,
			'cl' => null,
			'cw' => null,
			'jl' => null,
			'jw' => null,
			'count' => 1,
			'usedCount' => 0,
			'minuscount' => 0,
			'txt' => true,
			'id' => null,
			'MY_DOUBLE' => 1,
			'MY_DOUBLE_ID' => null,
			'MY_DOUBLE_TYPE' => 1,
			'MY_DOUBLE_FACE' => 0,
			'MY_DOUBLE_PARENT' => null,
			'name' => null,
			'part.name1' => null,
			'part.my_double_type' => 1,
			'part.my_el_type' => "eva"
        ]
	],
	// Деталь-сшивка для парного GOOD.product
	'stDetPart' => [
		'tag' => 'PART',
        'type' => 'complete',
        'level' => 3,
        'attributes' => [
            'l' => 0,
            'w' => 0,
            'dl' => 0,
            'dw' => 0,
            'jl' => 0,
            'jw' => 0,
            'cl' => 0,
            'cw' => 0,
            'minusCount' => 0,
            'count' => 1,
            'usedCount' => 0,
            'txt' => 'true',
            'id' => null,
            'name' => null,
            'part.name1' => null,
            'part.my_double_par_first' => null,
            'part.my_double_par_second' => null,
            'part.my_el_type' => 'eva',
            'MY_DOUBLE' => 1,
            'MY_DOUBLE_ID' => null,
            'MY_DOUBLE_RES' => 1,
            'MY_DOUBLE_TYPE' => 1,
            'MY_DOUBLE_PARENT' => 0,
            'part.my_double_type' => 1,
            'part.my_double_par_part_id' => null,
            'part.my_double_par_type_double' => 1,
            'part.my_double_par_back_double' => 3, // Неизвестный параметр
            'part.my_double_par_band_edge' => 0,
            'part.my_double_par_band_id_t' => null,
            'part.my_double_par_band_id_r' => null,
            'part.my_double_par_band_id_b' => null,
            'part.my_double_par_band_id_l' => null,
            'elt' => null,
            'eltmat' => null,
            'elr' => null,
            'elrmat' => null,
            'elb' => null,
            'elbmat' => null,
            'ell' => null,
            'ellmat' => null,
            '_old_l' => 0,
            '_old_w' => 0,
            '_old_dl' => 0,
            '_old_dw' => 0,
            '_old_jl' => 0,
            '_old_jw' => 0,
            '_old_cl' => 0,
            '_old_cw' => 0,
            '_old_minuscount' => 0,
            '_old_count' => 1,
            '_old_usedcount' => 0,
            '_old_txt' => true,
            '_old_id' => 0,
            '_old_part.my_el_type' => 'eva'
        ]
	],
	// Материал для сшивки (<good typeId="sheet")
	'stMatSheet' => [
		'tag' => 'good',
		'type' => 'open',
		'level' => 2,
		'attributes' => [
			'typeId' => 'sheet',
			'id' => null,
			'l' => null,
			'w' => null,
			't' => null,
			'count' => null,
			'cost' => null,
			't1' => null,
			't2' => null,
			'unit' => "м.кв.",
			'typeName' => 'Листовой',
			'name' => null,
			'description' => null,
			'part.name1' => null,
			'MY_DOUBLE' => 1,
			'MY_FIRST' => null,
			'MY_SECOND' => null,
			'MY_DOUBLE_ID' => null
		]
	],
	// Деталь для парного GOOD.sheet сшивки
	'stMatPart' => [
		'tag' => 'PART',
        'type' => 'complete',
        'level' => 3,
        'attributes' => [
        	'id' => null,
			'l' => null,
			'w' => null,
			'count' => 1,
			'usedCount' => 0,
			'MY_DOUBLE_TYPE' => 1,
			'MY_DOUBLE' => 1,
			'MY_DOUBLE_ID' => null,
			'part.my_double_type' => 1
    	]
	],
	'csOpOpen' => [
		'tag' => 'operation',
		'type' => 'open',
		'level' => 2,
		'attributes' => [
           'id'                     => null,  // id операции
           'typeId'                 => 'CS',
           'tool1'                  => null,  // id раскроенного станка (typeId="tool.cutting")
           'l'                      => null,  // Длина материала
           'w'                      => null,  // Ширина материала
           't'                      => null,  // Толщина материала
           'cTrimL'                 => 8,     // Торцовка по длине. По умолчанию: 10мм.
           'cTrimW'                 => 8,     // Торцовка по ширине. По умолчанию: 10мм.
           'csDirectCut'            => 0,     // Начальное направление резов: По умолчанию - 0
           'csTexture'              => true,  // Учет текстуры. По умолчанию: true
           'cFillRep'               => 0.9,   // Процент заполнения для кратности. По умолчанию: 0.9
           'cAllowanceWorkpiece'    => 0,     // Припуск на обработку заготовки. По умолчанию: 0мм.
           'cCostType'              => 0,     // Расчет стоимости операции по: По умолчанию - 0
           'cCostCut'               => 0,     // Цена операции (метр/рез). По умолчанию: 0
           'cCostByItem'            => false, // Расчет стоимости по листам. По умолчанию: false
           'cCostByItemRound'       => 22,    // Округление - Листы. Учитывается с параметром: Расчет стоимости по листам.По умолчанию: 11
           'cCPF'                   => 0.9,   // Расчетный процент заполнения. Предварительный расчет до формирования карт раскроя ((1.0-cCPF)*100). По умолчанию: 0.1
           'cMinDimRegWaste1'       => 300,   // Мин. размер остатка первой стороны. По умолчанию: 100мм.
           'cMinDimRegWaste2'       => 300,   // Мин. размер остатка второй стороны. По умолчанию: 100мм.
           'cBusinessWaste'         => 0.0,   // Площадь делового остатка. По умолчанию: 1м2.
           'cTrimWaster'            => true,  // Торцовка остатков. По умолчанию: true
           'cMinDimDetail'          => 30,    // Мин. размер детали. По умолчанию: 0мм.
           'cFilling'               => 0.08,  // Процент заполнения.
           'cBusinessWasteAmount'   => 0.0,   // Общая площадь делового остатка.
           // 'cTrimLength'            => null,  // Общая длина реза.
           'cTrimLengthP'           => null,  // Предварительный расчет. Общая длина реза. Считается по периметру деталей.
           // 'cTrimCount'             => null,  // Общее количество резов.
           'cTrimCountP'            => null,  // Предварительный расчет. Общее количество резов. Считается по 4 реза на деталь.
           // 'cPartCount'             => null,  // Общее количество деталей вошедших в раскрой.
           'cPartCountP'            => null,  // Предварительный расчет. Общее количество деталей вошедших в раскрой.
           // 'cPartAmount'            => null,  // Общая площадь деталей вошедших в раскрой.
           'cPartsAmountP'          => null,  // Предварительный расчет. Общая площадь деталей вошедших в раскрой.
           'cWasteCount'            => null,  // Общее количество остатков.
           // 'cWasteAmount'           => null,  // Общая площадь остатков.
           'cWasteAmountP'          => null,  // Предварительный расчет. Общая площадь остатков.
           // 'cMaterialCount'         => null,  // Количество листов вошедшие в раскрой.
           // 'cMaterialAmount'        => null,  // Площадь материала вошедшего в раскрой.
           'cMaterialAmountP'       => null,  // Предварительный расчет. Площадь материала вошедшего в раскрой.
           // 'data'                   => null,  // Карты раскроя.
           'cSaveDrawCut'           => false, // Сохранение чертежа карт кроя. По умолчанию: false
           // 'draw'                   => null,  // Рисунок карт кроя.Теги
           'cSizeMode'              => 1      // Режим ввода размеров. 0=По размеру заготовки (пильный размер); 1=По размеру детали после кромкования; 2=По размеру загтовки после прифуговки;
       ]
	],
	// Закрытие парного GOOD
	'goodClose' => [
		'tag' => 'good',
		'type' => 'close',
		'level' => 2
	],
   	'opClose' => [
		'tag' => 'operation',
		'type' => 'close',
		'level' => 2
   	],
	'endProject' => [
		'tag' => 'project',
		'type' => 'close',
		'level' => 1,
	]
];

?>