<?php

/*
 * Скрипт генерации программы (по запросу)
 * Версия: 2021-11-17
 */
ini_set('memory_limit', '3096M');
ini_set('max_execution_time', '600');// 300 - 5мин // 1800 - 30мин

$run_mode = php_sapi_name();

date_default_timezone_set('Europe/Kiev');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));

if ($run_mode != 'cli') {
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Credentials: true");
	header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");
	header("Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization");
	header("Access-Control-Allow-Headers: *");

    ini_set('display_startup_errors', 0);
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
} else {
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
    ini_set('log_errors', 1);
}

session_start();
require_once '_1/config.php';
require_once DIR_CORE . 'func.php';

//.. Сохраняем сессию (пользователя)
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

//.. 20.08.2022 Лог автогенерации
// Ключ объявляется в "generate_project_programs_mod.php" логируем только автогенерацию
if (isset($_SESSION['cycle_generate'])) {
    file_put_contents(dirname(__FILE__) . '/log/auto_generate_programm_projects/' . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . ' --- Order_id - ' . $order_id . "\r\n", FILE_APPEND);
}

if(isset($_GET['project'])) {
    $order_id = $_GET['project'];
}

if (($order_id>0)&&(is_numeric($order_id)))
{
    $request_order = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$order_id;
    
    $data_order=sql_data(__LINE__,__FILE__,__FUNCTION__,$request_order)['data'][0];
	
	if (!empty($data_order)) $_SESSION['project_restore']['order_data'] = $data_order;
    else $_SESSION['project_restore']['order_data'] = 'Order data failed.';
    if (!isset($_SESSION['order_akcent_ids'])) {
        $_SESSION['order_akcent_ids'] = $data_order['DB_AC_ID'] . '(' . $data_order['DB_AC_NUM'] . ')';
    }

    $request_project = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$order_id.' ORDER BY `DATE` DESC LIMIT 1';
    $data_project=sql_data(__LINE__,__FILE__,__FUNCTION__,$request_project)['data'][0];
    if (isset($_POST['project_data'])) 
    {
        $_SESSION['project_data'] = $_POST['project_data'];
        $vals=get_vals($_POST['project_data']);
		echo '<h1>Генерация текущего состояния!</h1>';
    }
    else
    {
		if ($run_mode != 'cli') {
			echo '<h1>Версия PROJECT: '.$data_project['PROJECT_ID'].'</h1>';
		} else {
            echo "[" . $order_id . "] PROJECT_ID: " . $data_project['PROJECT_ID'] . "\n";
        }
        
		if ($run_mode != 'cli') {
			echo '<p>Считываем данные по проекту...';
		} else {
			echo "[" . $order_id . "] Считываем данные по проекту...";
		};
		
        //..23.11.2020 Читаем из файла
        if($p_data = get_Project_file($data_project['PROJECT_ID'])){
			if ($run_mode != 'cli') {
				echo ' OK</p>';
			} else {
				echo " OK\n";
			};
            $_SESSION['project_data'] = $p_data['project_data'];
        }
    }
}
else
{
    die ("Заказ order_id не распознан");
}

$_SESSION['user_place'] = $data_order['PLACE'];

$project = $data_project['PROJECT_ID'];
$new_order_id = $data_order['DB_AC_ID'];

$_SESSION["project_data"] = str_replace("qquot","quot", $_SESSION["project_data"]);
$_SESSION["project_data"] = str_replace("ququot","quot", $_SESSION["project_data"]);
$_SESSION["project_data"] = str_replace("quoquot","quot", $_SESSION["project_data"]);
$_SESSION["project_data"] = str_replace("quotquot","quot", $_SESSION["project_data"]);
$_SESSION['project_data'] = str_replace(' ', ' ', $_SESSION['project_data']);

if((isset($_GET['old_code']) && $_GET['old_code'] <> 1) && (isset($_GET['prod']) && $_GET['prod'] <> 1))
{
	//-- 15.04.2021 Записываем данные в сессию, сохранённую во временном файле
    put_temp_file_data($_SESSION["data_id"], $_SESSION);
	
	//-- 15.04.2021 И записываем данные из файла обратно в сессию
    $_SESSION = get_temp_file_data($_SESSION["data_id"]);

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    
	if ($run_mode != 'cli') {
		echo '<hr>обработка проекта по проверке.<hr>';
	} else {
		echo "[" . $order_id . "] Обработка проекта по проверке...\n";
	} 
}

$_SESSION['project_data'] = gl_55 (__LINE__,__FILE__,__FUNCTION__,$_SESSION['project_data']);
$_SESSION['project_data'] = xml_parser($_SESSION['project_data']);

$p = xml_parser_create();
xml_parse_into_struct($p, $_SESSION['project_data'], $vals, $index);
xml_parser_free($p);
$vals_without_gr = change_gr($vals);

if ($run_mode == 'cli') { echo "Processing put_programm()... \n"; }
$t = put_programm($vals_without_gr,$_SESSION['user_place'],$link, $project, $new_order_id, $serv_main_dir,$_SESSION,str2url($data_order['DB_AC_NUM']));

if ($run_mode != 'cli') {
	if (!isset($t['res'])) echo '<mark>Есть проблема:</mark> ' . $t;
	else echo '<b>Результат обработки:</b> ' . $t['log'];
} else {
	if (!isset($t['res'])) echo "[" . $order_id . "] Есть проблема:\n" . $t . "\n";
	else echo "[" . $order_id . "] Операция завершена успешно\n";
}

//.. 20.08.2022 Лог автогенерации
// Ключ объявляется в "generate_project_programs_mod.php" логируем только автогенерацию
if (isset($_SESSION['cycle_generate'])) {
    file_put_contents(dirname(__FILE__) . '/log/auto_generate_programm_projects/' . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . ' --- Генерация завершена для заказа ' . $order_id . "\r\n", FILE_APPEND);
    file_put_contents(dirname(__FILE__) . '/log/auto_generate_programm_projects/' . date('Y-m-d') . '.log', '################################################################################' . "\r\n\r\n\r\n\r\n", FILE_APPEND);
    unset($_SESSION['cycle_generate']);

    // Очистка связанных с автогенерацией логов (которые старше месяца)
    require_once DIR_CORE . 'func_mini.php';

    $cycleLog = GetFileNames(DIR_LOGS . 'auto_generate_programm_cycle', true);
    $generateProgramLog = GetFileNames(DIR_LOGS . 'auto_generate_programm_projects', true);
    $createProjectsLog = GetFileNames(DIR_LOGS . 'create_new_order', true);

    if (!empty($cycleLog)) {
        foreach ($cycleLog as $value) {
            if (filemtime(DIR_LOGS . 'auto_generate_programm_cycle/' . $value) < (mktime() - 2678400)) {
                unlink(DIR_LOGS . 'auto_generate_programm_cycle/' . $value);
            } else break;
        }
    }

    if (!empty($generateProgramLog)) {
        foreach ($generateProgramLog as $value) {
            if (filemtime(DIR_LOGS . 'auto_generate_programm_projects/' . $value) < (mktime() - 2678400)) {
                unlink(DIR_LOGS . 'auto_generate_programm_projects/' . $value);
            } else break;
        }
    }

    if (!empty($createProjectsLog)) {
        foreach ($createProjectsLog as $value) {
            if (filemtime(DIR_LOGS . 'create_new_order/' . $value) < (mktime() - 2678400)) {
                unlink(DIR_LOGS . 'create_new_order/' . $value);
            } else break;
        }
    }
}
