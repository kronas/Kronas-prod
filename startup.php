<?php
date_default_timezone_set('Europe/Kiev');

// define('DEBUG_MODE', true);
define('DEBUG_MODE', false);

// Константа с текущим доменом
define('HOST_NAME', $main_dir);
define('DIR_ADMIN', HOST_NAME . '/admin/');
define('GENERAL_CSS', HOST_NAME . '/css');
define('GENERAL_JS', HOST_NAME . '/js');
define('LOGIN_PAGE', HOST_NAME . '/login.php');
define('HTTP_FILES', HOST_NAME . '/files');

// Формирование констант с путями 
define('DIR_CORE', $serv_main_dir);
define('DIR_FILES', DIR_CORE . 'files/');
define('DIR_HDESK', DIR_CORE . 'helpdesc/');
define('DIR_SYSTEM', DIR_CORE . 'system/');
define('DIR_LIBRARY', DIR_SYSTEM . 'library/');
define('DIR_TEMPLATES', DIR_SYSTEM . 'templates/');
define('DIR_HANDLERS', DIR_LIBRARY . 'handlers/');
define('DIR_FUNCTIONS', DIR_LIBRARY . 'functions/');
define('DIR_LOGS', DIR_CORE . 'log/');

define('LANG', 'Ru');


const SIDE = array('l', 'r', 'b','t');
const SIDE_ALL = array('l', 'r', 'b','t','f');
const SIDE_T = array('l'=>'левое','r'=> 'правое','b'=> 'нижнее','t'=>'верхнее');
const SIDE_ST = array('l'=>'левая','r'=> 'правая','b'=> 'нижняя','t'=>'верхняя');
const SIDE_T_ALL = array('l'=>'левое','r'=> 'правое','b'=> 'нижнее','t'=>'верхнее','f'=>'фронтальное');

$pipe = ' | KRONAS SERVICE';

// Функции вывода на экран (строк, массивов, XML-данных)
include_once $serv_main_dir . 'func_prints.php';




// ###############################################################################
// Блок настроек отладки
if(isset($_GET) && isset($_GET['debug'])) {
    $_SESSION["debug"] = $_GET['debug'];
}

// Зарузка вспомогательных классов и компонентов
if (is_file(DIR_CORE . 'system/vendor/autoload.php')) {
    require_once(DIR_CORE . 'system/vendor/autoload.php');
}

// use DebugBar\StandardDebugBar;

// Error Reporting
if ( (defined('DEBUG_MODE') && DEBUG_MODE == true) || (isset($_SESSION['debug']) && ($_SESSION['debug'] == "1")) ) {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    
    // $debugbar = new StandardDebugBar();
    // $debugbarRenderer = $debugbar->getJavascriptRenderer();
    // $debugbarRenderer->setIncludeVendors(true);
    
    // $_['DEBUG_RUNTIME'] = true;
} else {
    ini_set('error_reporting', 0);
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    
    $_['DEBUG_RUNTIME'] = false;
}
// Блок настроек отладки
// ###############################################################################

error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');


if(!isset($link)) {
    date_default_timezone_set('Europe/Kiev');
    $link = db_connect();
}

function db_connect()
{
    global $database_link_connect;
    $link = $database_link_connect;
    $mysql_link = mysqli_connect('p:' . $link['host'], $link['user'], $link['password'], $link['database'])
        or die ("Ошибка подключения: " . mysqli_connect_error($mysql_link));
    // Установка кодировки соединения
    mysqli_set_charset($mysql_link, "utf8");
    return $mysql_link;
}



// Блок доступов

// Массив пользователей, которым доступен просмотр наклеек:
$PDF_access = [
    0 => 300, // Подгорецкий Владислав
    1 => 218, // Тоценко Антон (админ)
    2 => 4, // Пресич Дмитрий
    3 => 362, // Тоценко Антон
];

// Адреса для рассылки по заказам, оформленным на Интернет-магазин
$ordersMailing = [
    'Evtushenko.Vadim@kronas.com.ua',
    'Polyusevich.Yuliya@kronas.com.ua',
    'Fomenko.Evgeniy@kronas.com.ua',
    'Shmigirovskiy.Vladislav@kronas.com.ua',
    'Oleynik.Lesya@kronas.com.ua',
    'Ischenko.Yaroslav@kronas.com.ua',
    'mihaylenko.nataliya@kronas.com.ua',
    'Totsenko.Anton@kronas.com.ua'
];

?>
