<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <title>Подтверждение заказа</title>
    <style>
        .erase-tr {
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }

        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }

        .loading-search {
            position: absolute;
            right: 22px;
            top: 3px;
            display: none;
        }

        #searchOnCode-box {
            padding: 10px;
            display: none;
        }

        #searchOnCode-box li {
            cursor: pointer;
        }

        form {
            margin: 10px;
        }

        body {
            padding: 35px 5px;
        }

        .nav.nav-tabs {
            margin-top: 75px;
        }

        .tab-content h3 {
            padding-top: 30px;
        }
    </style>


</head>
<body>
<?php
header('Access-Control-Allow-Origin: *');
session_start();

include_once("func.php");

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

if($_GET['data_id']) {

    $session_id = $_GET['data_id'];

    $helpdesc_auth = $_SESSION['user'];

    $_SESSION = get_temp_file_data($session_id);

    $_SESSION['user'] = $helpdesc_auth;

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}


if ($_SESSION["project_data"]) {

    get_vals_index($_SESSION["project_data"]);
    $vals = $_SESSION["vals"];
    unset ($_SESSION["vals"]);
    $vals = vals_out($vals);
    
    $index = make_index($vals);
    ksort($vals);
    $vals_in=$vals;
    foreach ($index["GOOD"] as $i) {
        if ($vals[$i]["attributes"]["TYPEID"] == "product") {
            $ip = $i + 1;
            while ($vals[$ip]["tag"] == "PART") {
                $part[$vals[$ip]["attributes"]["ID"]]['ID'] = $vals[$ip]["attributes"]["ID"];
                $part[$vals[$ip]["attributes"]["ID"]]['L'] = $vals[$ip]["attributes"]["L"];
                $part[$vals[$ip]["attributes"]["ID"]]['DL'] = $vals[$ip]["attributes"]["DL"];
                $part[$vals[$ip]["attributes"]["ID"]]['JL'] = $vals[$ip]["attributes"]["JL"];
                $part[$vals[$ip]["attributes"]["ID"]]['JW'] = $vals[$ip]["attributes"]["JW"];
                $part[$vals[$ip]["attributes"]["ID"]]['W'] = $vals[$ip]["attributes"]["W"];
                $part[$vals[$ip]["attributes"]["ID"]]['DW'] = $vals[$ip]["attributes"]["DW"];
                $part[$vals[$ip]["attributes"]["ID"]]['BARCODE'] = $vals[$ip]["attributes"]["PART.BARCODE"];
                $part[$vals[$ip]["attributes"]["ID"]]['PART.CODE'] = $vals[$ip]["attributes"]["PART.CODE"];
                $part[$vals[$ip]["attributes"]["ID"]]['NAME'] = $vals[$ip]["attributes"]["NAME"];
                $part[$vals[$ip]["attributes"]["ID"]]['N_IZD'] = part_place_in_product($vals, $vals[$ip]["attributes"]["ID"]);
                $part[$vals[$ip]["attributes"]["ID"]]['N_CS'] = part_place_in_cs($vals, $vals[$ip]["attributes"]["ID"]);
                $ip++;
            }
        }
    }
    $vals_in = $vals;

    $xnc_in = $_SESSION['xnc_in'];

    if($_GET['is_handlers'] == 1) {
        $ticket_id = $_GET['ticket_id'];

        $get_ticket_sql = 'SELECT * FROM `ticket` WHERE `id` = '.$ticket_id;
        $get_ticket = sql_data(__LINE__,__FILE__,__FUNCTION__,$get_ticket_sql)['data'][0];

        $xnc_in_recalc= gl_recalc(__LINE__,__FILE__,__FUNCTION__,$xnc_in);
        $xnc_price = xnc_mpr_price($xnc_in_recalc);

        $mpr_catalog = $serv_main_dir.'/helpdesc/uploads/'.$ticket_id.'/mpr/';
        foreach($xnc_price as $k=>$v) {
            $file_path = $mpr_catalog.$k.'.mpr';
            $side = $v['side'] == 'true' ? 1 : 0;
            $by_size = $v['type'] == 'sverl' ? 0 : 1;
            if (($by_size==1)&&($side==1))
            {
                $kll=1;
                $klr=1;
                $klb=1;
                $klt=1;
            }
            else
            {
                $kll="NULL";
                $klr="NULL";
                $klb="NULL";
                $klt="NULL";
            }
            if (!isset($v['op_frez'])) $v['op_frez']='NULL';
            if (!isset($v['op_paz'])) $v['op_paz']='NULL';
            if (!isset($v['op_bore'])) $v['op_bore']='NULL';
            $sql = 'INSERT INTO `ticket_construct_mpr`(`name`, `ORDER1C`, `ticket_id`, `file`, `side`, `L`, `W`, `by_size`,`kll`,`klr`,`klt`,`klb`,`op_frez`,`op_paz`,`op_bore`) 
                    VALUES ("'.$k.'", '.$get_ticket['order_id'].', '.$ticket_id.', "'.$file_path.'", '.$side.', '.$v['size']['DY'].', '.$v['size']['DX'].', '.$by_size.','.$kll.','.$klr.','.$klt.','.$klb.'
                    ,'.$v['op_frez'].'  ,'.$v['op_paz'].' ,'.$v['op_bore'].')';
            $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
            $mpr_insert_ids = $query['id'];

            if($mpr_insert_ids) {
                foreach ($v['serv_id'] as $k=>$s) {
                    $sql_insert_table = 'INSERT INTO `ticket_mpr_price`(`mpr_id`, `service_id`, `count`) 
                                     VALUES ('.$mpr_insert_ids.','.$k.', '.$s.')';
                    $insert_table_price = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_insert_table);
                }
            }
        }

        //..17.02.2021 На локальном сервере код не выполняется ("Заголовки отправлены"), поэтому такой фикс
        if (stripos($_SERVER['SERVER_NAME'], '.loc') || stripos($_SERVER['SERVER_NAME'], '.local')) {
            echo '<meta http-equiv="refresh" content="0; url='.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw'].'">';
        } else {
            header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
        }
        exit;

    }

    if (isset($_SESSION['xnc_in'])) {
        $xnc_in = $_SESSION['xnc_in'];
        get_vals_index($xnc_in);
        $vals = $_SESSION["vals"];
        unset ($_SESSION["vals"]);
        $vals = vals_out($vals);
        $index = make_index($vals);
        ksort($vals);


        foreach ($index["OPERATION"] as $i) {
            if( $vals[$i]['attributes']['TYPEID']=="XNC")
            {
                $vals[$i]['attributes']['PROGRAM'] = xml_parser($vals[$i]['attributes']['PROGRAM']);
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]['attributes']['PROGRAM'], $pr, $in_pr);
                xml_parser_free($p);
    // p_($pr);exit;
                $ar=array('/dx/','/dy/','/dz/','/--/');
                $ar1=array($pr[0]['attributes']["DX"],$pr[0]['attributes']["DY"],$pr[0]['attributes']["DZ"],'-');
                // p_($ar1);
                // p_($ar);
                foreach ($pr as $kj=>$j)
                {
                    foreach ($pr[$kj]['attributes'] as $kl=>$l)
                    {
                        if ((substr_count($l,'z')>0)OR(substr_count($l,'--')))
                        {
                            // echo $kl." = ".$l;
                            $pr[$kj]['attributes'][$kl]=preg_replace($ar, $ar1, $l);
                            // $pr[$kj]['attributes'][$kl]=val($pr[$kj]['attributes'][$kl]);
                            // p_($pr[$kj]['attributes'][$kl]);
                            // echo "<hr>";
                        }
                    }
                }
                $op[$vals[$i]['attributes']['ID']]['id'] = $vals[$i]['attributes']['ID'];
                $op[$vals[$i]['attributes']['ID']]['v'] = $i;
                $op[$vals[$i]['attributes']['ID']]['DX'] = $pr[0]['attributes']["DX"];
                $op[$vals[$i]['attributes']['ID']]['DY'] = $pr[0]['attributes']["DY"];
                $op[$vals[$i]['attributes']['ID']]['DZ'] = $pr[0]['attributes']["DZ"];
                $op[$vals[$i]['attributes']['ID']]['turn'] = $vals[$i]['attributes']['TURN'];
                $op[$vals[$i]['attributes']['ID']]['side'] = $vals[$i]['attributes']['SIDE'];
                $op[$vals[$i]['attributes']['ID']]['code'] = $vals[$i]['attributes']['CODE'];
                if (!isset($op[$vals[$i]['attributes']['ID']]['side'])) $op[$vals[$i]['attributes']['ID']]['side'] = 'true';
                if (($vals[$i]['attributes']['TURN'] == 1) OR ($vals[$i]['attributes']['TURN'] == 3)) {
                    $op[$vals[$i]['attributes']['ID']]['DY'] = $pr[0]["DX"];
                    $op[$vals[$i]['attributes']['ID']]['DX'] = $pr[0]["DY"];
                }
                // p_($vals[$i]['attributes']['PROGRAM']);
                $vals[$i]['attributes']['PROGRAM']=vals_index_to_project($pr);
                // p_($vals[$i]['attributes']['PROGRAM']);exit;
            }
        }
    }

    // Спрашиваем пользователя по какому полю сопостовлять с именем файла (он потом вернётся в поле code операции):
    // BARCODE, PART.CODE, (NAME) Наименование, (N_IZD) Номер детали в изделии, (N_CS) Номер детали в раскрое, (ID) Код детали

    if (!isset($_POST['compare_data']) && !isset($_POST['changed'])) {

        ?>
        <form action="<?php echo $main_dir; ?>/step_import_mpr.php<?='?nw='.$_GET['nw']?>" method="POST">
            <p><b>Выберете поле, по которому провести сопоставление:</b></p>
            <div class="input-group" style="max-width: 350px;">
                <select class="custom-select" name="compare_data" id="">
                    <option value="BARCODE">BARCODE</option>
                    <option value="PART.CODE">PART.CODE</option>
                    <option value="NAME">Наименование</option>
                    <option value="N_IZD">Номер детали в изделии</option>
                    <option value="N_CS">Номер детали в раскрое</option>
                    <option value="ID">Код детали</option>
                </select>
                <div class="input-group-append">
                    <button class="btn btn-success">Выбрать</button>
                </div>

            </div>
        </form>
        <?php
    }

    if (isset($_POST['compare_data'])) {

        $user = $_POST['compare_data']; // например

        $op_part = [];
// p_($op);
// p_($part);
        foreach ($op as $ido => $vo) {
            $variants = [];
            foreach ($part as $id => $v) {
                if (!isset($op_part[$vo['v']])) {
                    if (((
                        (round($v['L'],2) == round($vo['DX'],2)) && (round($v['W'],2) == round($vo['DY'],2))) OR
                        ((round($v['DL'],2) == round($vo['DX'],2)) && (round($v['DW'],2) == round($vo['DY'],2))OR
                        ((round($v['JL'],2) == round($vo['DX'],2)) && (round($v['JW'],2) == round($vo['DY'],2))
                        
                        ))OR
                        (((round($v['W'],2) == round($vo['DX'],2)) && (round($v['L'],2) == round($vo['DY'],2))) OR 
                        ((round($v['DW'],2) == round($vo['DX'],2)) && (round($v['DL'],2) == round($vo['DY'],2))) OR
                        ((round($v['JW'],2) == round($vo['DX'],2)) && (round($v['JL'],2) == round($vo['DY'],2)))
                        )
                        ))
                    
                    
                    {
                        $variants[$id] = $v;
                        
                        if (((
                            (round($v['L'],2) == round($vo['DX'],2)) && (round($v['W'],2) == round($vo['DY'],2))) OR
                            ((round($v['DL'],2) ==round( $vo['DX'],2)) && (round($v['DW'],2) == round($vo['DY'],2))OR
                            ((round($v['JL'],2) == round($vo['DX'],2)) && (round($v['JW'],2) == round($vo['DY'],2))))))
                            {
                                $variants[$id]['turn'] = 0;
                            }
                            else $variants[$id]['turn'] = 1;
                       
                        // if (trim($vo['code']) == trim($v[$user])) {
                        //     // $rrr++;
                            
                        // }
                    }
                }
            }
            $op_part[$vo['v']] = array('code'=>$vo['code'],$vo['side']=> $v['ID'], 'variants' => $variants);
            

                $op_part[$vo['v']]['dx']= $vo['DX'];
                $op_part[$vo['v']]['dy']= $vo['DY'];
        }

// p_($op_part);
// p_($_POST);
        // print_r_($xnc_in);
// exit;
        ?>


        <?php if (isset($_POST['compare_data'])): ?>
            <h3 style="padding-bottom: 30px;">Cопоставленные детали:</h3>
            <hr>
            <?php if(count($op_part) < 1): ?>
                <h4 style="color: red; text-align: center; font-weight: 400;">Нет сопоставимых деталей</h4>
            <?php else: ?>
            <form action="<?php echo $main_dir; ?>/step_import_mpr.php<?='?nw='.$_GET['nw']?>" method="POST">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th>
                            Операция
                        </th>
                        <th>
                            Сопоставленные детали
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                        <?php foreach ($op_part as $i => $v): ?>
                            <tr>
                                <td><?= $i.' -<b><i> '.$v['code'].'</b></i>' ?></td>
                                <td>
                            <?php if(count($v['variants']) < 1): ?>
                                <p>Нет подходящий операций</p>
                            <?php else: ?>
                                    <select name="<?= $i ?>" id="">
                                    <option value="none">
                                            Не применять операции
                                        </option>
                                        <?php foreach ($v['variants'] as $o): ?>
                                            <option value="<?= $o['ID'] ?>" <?php if (substr_count($v['code'],$o[$_POST['compare_data']])>0) echo 'selected="selected"' ?> >
                                                (<?= $o['ID'] ?>) <?= $o['NAME'] ?>
                                            </option>
                                        <?php endforeach; ?>
                                        
                                    </select>
                            <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
                <input type="hidden" name="changed" value="1">
                <button class="btn btn-success float-right" style="margin-right: 15%;">Подтвердить</button>
            </form>
            <?php endif; ?>
        <?php endif; ?>

        <?php


        echo '<div>

                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item active"><a class="nav-link active" href="#operations" aria-controls="home" role="tab" data-toggle="tab">Операции</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#parts" aria-controls="profile" role="tab" data-toggle="tab">Детали</a></li>
                  </ul>
                
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="operations">
                    ';
        echo '<div><h3>Операции:</h3><br>
    <table class="table">
        <thead class="thead-dark">
            <tr>
               <th>id</th>
                <th>v</th>
                <th>DX</th>
                <th>DY</th>
                <th>DZ</th>
                <th>turn</th>
                <th>side</th>
                <th>code</th>
            </tr>
    </thead>
    <tbody>';
        ////print_r_($op);
        foreach ($op as $o) {
            echo <<<ooo
            <tr>
                <td>{$o['id']}</td>
                <td>{$o['v']}</td>
                <td>{$o['DX']}</td>
                <td>{$o['DY']}</td>
                <td>{$o['DZ']}</td>
                <td>{$o['turn']}</td>
                <td>{$o['side']}</td>
                <td>{$o['code']}</td>
            </tr>
ooo;
        }
        echo '</tbody></table></div>';


        echo '</div>
                    <div role="tabpanel" class="tab-pane" id="parts">';

        echo '<div><h3>Детали:</h3><br>
    <table class="table">
        <thead class="thead-dark">
            <tr>
               <th>(ID) Код детали</th>
                <th>L</th>
                <th>DL</th>
                <th>W</th>
                <th>DW</th>
                <th>BARCODE</th>
                <th>PART.CODE</th>
                <th>Наименование</th>
                <th>Номер детали в изделии</th>
                <th>Номер детали в раскрое</th>
            </tr>
    </thead>
    <tbody>';
        ////print_r_($part);
        foreach ($part as $o) {
            echo <<<ooo
            <tr>
                <td>{$o['ID']}</td>
                <td>{$o['L']}</td>
                <td>{$o['DL']}</td>
                <td>{$o['W']}</td>
                <td>{$o['DW']}</td>
                <td>{$o['BARCODE']}</td>
                <td>{$o['PART.CODE']}</td>
                <td>{$o['NAME']}</td>
                <td>{$o['N_IZD']}</td>
                <td>{$o['N_CS']}</td>
            </tr>
ooo;
        }
        echo '</tbody></table></div>';
        echo '</div>
                  </div>
                
                </div>';


        echo '<div class="d-flex justify-content-around">';


        echo '</div>';
//    echo 'Массив $op_part:';
//    print_r_($op_part);
//    exit;

    }

    ?>


    <?php


    if ($_POST['changed']) {
        $data = $_POST;
//        print_r_($data);
        $new_data = [];
        foreach ($data as $k => $v) {
            if ($k != 'changed' && $v != 'none') {
                $new_data[$k] = array('true' => $v);
            }
        }

        // p_($_SESSION);
        // // p_($_POST);
        // print_r_($vals);
        // exit;




    // здесь перебор $op_part. Показваем пользователю детали и их соответствие, которое нашли.
    // Пользователь может поменять соответствие вручную. Для выставления соответствия условия:
    // DX операции должен быть равен X или DX детали, DY операции должен быть равен Y или DY операции.
    // Далее пишу программу исходя что пользователь либо принял автоматический подбор, либо руками расставил программы.
    // На выходе исчерпывающий массив $op_part[номер массива в парсе проекта с программами] = массив[сторона = > id детали]

        foreach ($new_data as $vo => $array) {
            $vals[$vo]['type']="open";
            $vals[$vo]['attributes']['ID']=new_id($vals)+mt_rand(1200,5000);
            $vals[$vo]['attributes']['MY_XNC_IMPORT']='true';
            $vals_in = vals_into_end($vals_in, $vals[$vo]);
            
            foreach ($array as $side => $pid) {
                foreach($in_d["PART"] as $ind)
                {
                    if (($vals_in[$ind]['attributes']['CL']>0)&&($vals_in[$ind]['attributes']['ID']==$pid))
                    {
                        $p = xml_parser_create();
                        xml_parse_into_struct($p, $vals[$vo]['attributes']['PROGRAM'], $pr2, $in_pr2);
                        xml_parser_free($p);
                        // p_($vals[$vo]['attributes']['CODE']);
                        // p_($pr2[0]);
                        // 
                        
                        if (($vals_in[$ind]['attributes']['DL']<>$pr2[0]['attributes']['DX'])AND($vals_in[$ind]['attributes']['L']<>$pr2[0]['attributes']['DX'])AND($vals_in[$ind]['attributes']['JL']<>$pr2[0]['attributes']['DX']))
                        {
                            $vals_in[count($vals_in)-2]['attributes']['TURN']=1;
                            // p_( $vals_in[count($vals_in)-2]);exit;
                        }
                        else
                        {
                            $vals_in[count($vals_in)-2]['attributes']['TURN']=0;
                        }
                    }
                }
                $par = Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes" => Array("ID" => $pid, "MY_XNC_IMPORT" => 'true')
                );
                $vals_in = vals_into_end($vals_in, $par);
                $in_d=make_index($vals_in);
               

            }
           
            $op_cl = Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
            $vals_in = vals_into_end($vals_in, $op_cl);
            
        }
        // p_($vals_in);exit;
        $vals = $vals_in;
        $_SESSION["vals"] = $vals;
        $_SESSION["project_data"] = vals_index_to_project($vals_in);
//        print_r_($_SESSION["vals"]);
///        echo $_SESSION["project_data"];
//        echo 'asssadsasdasdasdasdasdcv';
    //    echo htmlspecialchars($_SESSION["project_data"]);exit;
        put_temp_file_data($_SESSION["data_id"], $_SESSION);
        if (stripos($_SERVER['SERVER_NAME'], '.loc') || stripos($_SERVER['SERVER_NAME'], '.local')) {
            echo '<meta http-equiv="refresh" content="0; url=/step_1?file='.$session_id.'&nw='.$_GET['nw'].'">';
        } else {
            header("Location: ".$laravel_dir."/step_1?file=" . $session_id.'&nw='.$_GET['nw']);
        }

    }

} else {
    echo 'Project data not exist!';
}
// print_r_($_SESSION['vals']);
//echo $_SESSION['project_data'];
///


?>
<script>
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
</body>
</html>
