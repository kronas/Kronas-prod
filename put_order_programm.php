<?php
session_start();

include_once("func.php");
include_once("description.php");

function get_data($sql, $count = 0) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        $result_array['count'] = $rows;
        if($count != 1) {
            $result_array['orders'] = [];
            for($i = 0; $i < $rows; ++$i) {
                $result_array['orders'][] = mysqli_fetch_assoc($result);
            }
        }
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $result_array;
}
//echo 'work';
$sql = 'SELECT * FROM `ORDER1C` as `o` JOIN `PROJECT` as `p` ON (`o`.`ID` = `p`.`ORDER1C`) WHERE `stanki` IS NULL AND `o`.`ID` = `p`.`ORDER1C` AND `o`.`production` = 1 AND `o`.`status` != "черновик" AND `o`.`comment` != "checked" AND `o`.`DB_AC_ID` > 0 GROUP BY `o`.`ID` ORDER BY `o`.`ID` DESC';
$order_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

if($order_data['ID'] > 0) {
    $sql_update_order = 'UPDATE `ORDER1C` SET `comment`= "checked" WHERE `ID` = '.$order_data['ID'];
    $update_order_comment = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_order);
}

print_r_($order_data);
//..23.11.2020 Меняем условие, читаем из файла, а не из базы:
if(file_exists($serv_main_dir.'/files/project_out/'.$order_data['PROJECT_ID'].'.txt')){
    //.. Читаем из файла
    $MPO = file_get_contents($serv_main_dir.'/files/project_out/'.$order_data['PROJECT_ID'].'.txt');
} else {
    $MPO = $order_data['MY_PROJECT_OUT'];
}
if($MPO) {
    $order_id = $order_data['ID'];
    include('refresh_project_programm.php');
    $sql_update = 'UPDATE `ORDER1C` SET `stanki`=1 WHERE `ID` = '.$order_id;
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update);
}

//$sql = 'SELECT * FROM `ORDER1C` as `o` JOIN `PROJECT` as `p` ON (`o`.`ID` = `p`.`ORDER1C`) WHERE `stanki` IS NULL AND `o`.`ID` = `p`.`ORDER1C` AND `o`.`readonly` IS NULL GROUP BY `o`.`ID` ORDER BY `o`.`ID` DESC';
//$order_data = get_data($sql);
//foreach($order_data['orders'] as $order) {
//    $sql_update = 'UPDATE `ORDER1C` SET `stanki`=1, `production`=1 WHERE `ID` = '.$order['ID'];
//    mysqli_query($link, $sql_update);
//}

?>
