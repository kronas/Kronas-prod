<?php

session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

include_once("func.php");
include_once("description.php");

// 28.01.2022 Обработка сшивок, созданных при импорте проекта из Базиса
if ($_SESSION["basisProject"] === 1) {
    // include_once DIR_FUNCTIONS . "fs_basis.php";
    include_once $serv_main_dir . "/system/library/functions/fs_basis.php";
}

if ($_GET["data_id"]) {
    $_SESSION = get_temp_file_data($_GET["data_id"]);
    $_SESSION["data_id"] = $_GET["data_id"];

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}
if (isset($_SESSION['project_data'])&&(!isset($_SESSION['vals'])))
{
    // $_SESSION['project_data'] = xml_parser($_SESSION['project_data']);
    // $_SESSION['project_data']=get_out_part_stock($_SESSION['project_data']);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $_SESSION['project_data'], $_SESSION['vals'], $index);
    $_SESSION['index']=make_index($_SESSION['vals']);
    xml_parser_free($p);
}
$_SESSION["vals"]=vals_out($_SESSION["vals"]);

if (isset($_SESSION["vals"])) $_SESSION["vals"]=xnc_code($_SESSION["vals"],$_SESSION);
$vals=$_SESSION["vals_in_gr"];
// xml ($vals);    

$arr_out=$_SESSION["arr_out"];
// p_($arr_out);
// p_($vals);
// exit;
$arr_cut_angle=$_SESSION["arr_cut_angle"];
// p_($_SESSION);exit;
// if (!isset($_SESSION["user_place"])OR($_SESSION["user_place"]<1)) $_SESSION["user_place"]=1;
// $place=$_SESSION['user_place'];
$dar=array('r','l','b','t');
$index=make_index($vals);
foreach($index['PART'] as $i)
{
    if ($vals[$i]['attributes']['CL']>0)
    {
        if ((!isset($_SESSION['arr_cut_side'][$vals[$i]['attributes']['ID']]['MY_CUT_SIDE']))&&(!isset($vals[$i]['attributes']['MY_DOUBLE_RES'])))
        {
            foreach($dar as $h)
            {
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_'.$h);
                $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltax');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltay');
                $vals[$i]=my_uncet($vals[$i],'my_db_'.'my_side_to_cut_'.$h);
                $vals[$i]=my_uncet($vals[$i],'my_db_'.'my_xnc_cut_deltax');
                $vals[$i]=my_uncet($vals[$i],'my_db_'.'my_xnc_cut_deltay');
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_'.$h);
                $vals[$i]=my_uncet($vals[$i],'my_side_to_cut_d_'.$h);
                $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltax');
                $vals[$i]=my_uncet($vals[$i],'my_xnc_cut_deltay');
            }
        }
        if ((!isset($_SESSION['arr_cut_angle'][$vals[$i]['attributes']['ID']]))&&(!isset($vals[$i]['attributes']['MY_DOUBLE_RES'])))
        {
            foreach($dar as $h)
            {
                $vals[$i]=my_uncet($vals[$i],'my_cut_angle_'.$h);
            }
        }
    }
}

if ((isset($vals))AND(count($_SESSION["out_del"])>0))
{
    //удаление сшивок по запросу из формы
    // 28.01.2022 Обработка сшивок, созданных при импорте проекта из Базиса
    if ($_SESSION["basisProject"] === 1) $vals = delBasisStiching($vals, $_SESSION["out_del"]);
    else $vals = double_del($vals, $_SESSION["out_del"], $link);
}

if ((isset($vals))AND(count($_SESSION["arr_out"])>0))
{

// создание сшивок
unset($ch_dd);
    foreach ($_SESSION["arr_out"] as $k=>$v)
    {
        $array=array($k=>$v);
        if (($v["type_double"]>0)&&($v["back_double"]>0))
        {
            // p_($_SESSION);exit;
            $vals=do_double ($vals,$_SESSION["user_place"],$array,$link);
            $ch_dd=1;
        }
    }
// exit('11');
//    if ($ch_dd>0) $vals=sort_vals($vals);
    // xml($vals);

    // header('Content-type: text/xml');
    // echo (vals_index_to_project($vals));
    // exit;
        
// обработка криволинейки (добавим станки, кромки, поправим операции)
    // p_($_SESSION["arr_out"]);
    // $vals=change_kl_edge ($vals,$_SESSION["arr_out"],$link);
   
    // p_($vals);
    // exit;
}
// MY_CUT_SIDE

    // p_($vals);exit;

//   exit ('11');  
// xml ($vals);
if((isset($vals))AND(count(($_SESSION["arr_cut_angle"])>0)))
{
// обработка зарезов (проверяем есть ли кромка по стороне зарезов)
// p_($_SESSION);exit;
    $vals=check_engle_edge ($vals,$_SESSION["arr_cut_angle"]);
   
}
if (isset($vals))
{
// добавляем тэги для этикеток
   
    ksort($vals);
    $vals=vals_out($vals);
    $_SESSION["project"]["double_after"]=vals_index_to_project($vals);

    $_SESSION["project_data"]=$_SESSION["project"]["double_after"];
// p_( $_SESSION["project_data"]);
    unset($check_error_part);
    $check_index=1;
    include ('check_production_possibilities.php');
    unset($check_index);
        
    if(!$error)
    {

        $new_vals = get_vals_index($_SESSION["project_data"]);
        $vals = vals_out($new_vals['vals']);
        $index = make_index($new_vals['vals']);
        $check_index=1;
        include ('check_production_possibilities.php');
        unset($check_index);
    } else {
        print_r($error);
        print_r($error_ok);
        echo '<div><a href="'.$main_dir.'/index.php?data_id='.$_SESSION["data_id"].'" style="display: inline-block; padding: 9px 15px; background-color: tomato; color: white;">Вернутся к редактированию</a></div>';
        exit;
    }


    if(!$error) {
        $_SESSION['vals']=add_tag_print($vals);
        $content= step_1_check($_SESSION,$link,$_SESSION);
        $_SESSION['project_data']=$content['project_data'];
        put_temp_file_data($_SESSION["data_id"], $_SESSION);

        $sides_s = ['t','r','b','l'];
        $sides_w = ['t' => 'верхнее','r' => 'правое','b' => 'нижнее','l' => 'левое'];
        $_SESSION["error_ok"] = '';
        foreach ($vals as $value) {
            if ($value['tag'] == 'PART' && count($value['attributes']) > 1) {
                foreach ($sides_s as $v) {
                    if (isset($value['attributes']['MY_CUT_ANGLE_'.mb_strtoupper($v)])
                        && isset($value['attributes']['EL'.mb_strtoupper($v)])
                        && isset($value['attributes']['MY_CUT_ANGLE_SDVIG_'.mb_strtoupper($v)])
                        && ($value['attributes']['MY_CUT_ANGLE_SDVIG_'.mb_strtoupper($v)] < 1))
                    {
                        $value = my_uncet($value,'el'.$v);
                        $value = my_uncet($value,'my_kl_el'.$v);
                        $value = my_uncet($value,'my_kl_el'.$v."_code");
                        $value = my_uncet($value,'my_db_el'.$v);
                        $value = my_uncet($value,'my_db_kl_el'.$v);
                        $_SESSION["error_ok"].="<br>Для детали id= ".$value['attributes']['ID']." ".$sides_w[$v]." кромкование (".$value["attributes"]["ELTMAT"].") будет убрано по стороне, где есть зарез под углом (и сдвиг меньше, чем 1мм). Увидеть изменения можно в дополнительных операциях.<hr>";
                    }
                }
            }
        }
        if (isset($_SESSION["error_ok"]) && !empty($_SESSION["error_ok"])) {
            ?>

            <div style="box-shadow:1px 1px 10px #333;border:3px solid #ff9744;position:absolute;top:10%;left:20%;right:20%;background:#eee;padding:1rem 3rem;">
                <div style="position:relative;margin-bottom:110px;">
                    <?= $_SESSION["error_ok"] ?>
                </div>
                <div style="width:150px;margin:20px auto;">
                    <a style="background:#28a745;color:#fff;text-decoration:none;font-size:1.2rem;font-weight:bold;letter-spacing:1px;padding:0.5rem 1.4rem;box-shadow:1px 1px 10px #777;border-radius:9px;" onmouseover="this.style.backgroundColor='#218838';" onmouseout="this.style.backgroundColor='#28a745';" href="<?= $laravel_dir ?>/step_1?file=<?= $_SESSION["data_id"] ?>&nw=<?= $_GET['nw'] ?>">Понятно</a>
                </div>
            </div>

            <?php
            exit();
        } else {
            header("Location: ".$laravel_dir."/step_1?file=".$_SESSION["data_id"].'&nw='.$_GET['nw']);
        }
    } else {
        print_r($error);
        print_r($error_ok);
        echo '<div><a href="'.$main_dir.'/index.php?data_id='.$_SESSION["data_id"].'" style="display: inline-block; padding: 9px 15px; background-color: tomato; color: white;">Вернутся к редактированию</a></div>';
        exit;
    }
}
else echo "нет входных данных для расчёта!";
?>