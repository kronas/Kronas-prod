<?php
$project=array();
$good_product_=array();
foreach($vals[0]["attributes"] as $value)
{
    $project[key($vals[0]["attributes"])]=htmlspecialchars ($value);
    next($vals[0]["attributes"]);
}
if ($_SESSION["manager_id"]) $vals[0]["attributes"]["part.my_manager_accent_code"]=$_SESSION["manager_id"];
if ($_SESSION["manager_name"]) $vals[0]["attributes"]["part.my_manager_name"]=$_SESSION["manager_name"];
if ($_SESSION["client_fio"]) $vals[0]["attributes"]["part.my_client_fio"]=$_SESSION["client_fio"];
if ($_SESSION["client_tel"]) $vals[0]["attributes"]["part.my_client_tel"]=$_SESSION["client_tel"];
if ($_SESSION["client_email"]) $vals[0]["attributes"]["part.my_client_email"]=$_SESSION["client_email"];
if ($_SESSION["user_place"]) $vals[0]["attributes"]["part.my_user_place"]=$_SESSION["place"];
if ($_SESSION["user_type_edge"]) $vals[0]["attributes"]["part.my_user_type_edge"]=$_SESSION["place"];

foreach ($index["GOOD"] as $i)
{
    if ($vals[$i]["attributes"]["TYPEID"]=="simple")
    {
        $id=$vals[$i]["attributes"]["ID"]; 
        foreach ($vals[$i]["attributes"] as $k1=>$v1)
        {      
             $good_simple_{$id}[$k1] = $v1;
        }
    }
    if ($vals[$i]["attributes"]["TYPEID"]=="product")
    {
        $id=$vals[$i]["attributes"]["ID"];   
       
        foreach ($vals[$i]["attributes"] as $k2=>$v2)
        {      
            $good_product_{$id}[$k2] = $v2;
        }
        if ($vals[$i]["type"]=="open")
        {
            $j=1;
            if($vals[$i+$j]["tag"]=="PART")
            {
                
                while (($vals[$i+$j]["tag"]=="PART")&& ($vals[$i+$j]["type"]=="complete"))
                {
                   
                    $id_=$vals[$i+$j]["attributes"]["ID"];
                    foreach ($vals[$i+$j]["attributes"] as $k3=>$v3)
                    {
                        $part_{$id_}[$k3] = $v3;
                    }
                    //$part_{$id_}["MY_DATE"]=date_create()->format('Y-m-d H:i:s');
                    $id_part{$id_} = $id;
                    $j++;
                    
                }
            }    
        }
    }
    
   if ($vals[$i]["attributes"]["TYPEID"]=="tool.cutting")
   {
        $id_=$vals[$i]["attributes"]["ID"];
        foreach ($vals[$i]["attributes"] as $k4=>$v4)
        {      
            $good_tool_cutting_{$id_}[$k4] = $v4;
        }
        foreach ($index["OPERATION"] as $i6)
        {
            if (($vals[$i6]["attributes"]["TYPEID"]=="CS")AND($vals[$i6]["attributes"]["TOOL1"]==$id_))
            {
                $id_sheet=$vals[$i6+1]["attributes"]["ID"];
            }
        }
        foreach ($index["GOOD"] as $i6)
        {
            if (($vals[$i6]["attributes"]["TYPEID"]=="sheet")AND($vals[$i6]["attributes"]["ID"]==$id_sheet))
            {
                $index_sheet=$i6;
            }
        }
        if ($index_sheet)
        {
            $i=$index_sheet;
            unset ($index_sheet);
            $id=$vals[$i]["attributes"]["ID"];
            $material_code = $vals[$i]["attributes"]["CODE"];
            if($vals[$i]["attributes"]["MY_DOUBLE"]==1)
            {
                $double_mat[get_cs_num($vals,$vals[$i]["attributes"]["ID"])]=$vals[$i];
            }
           // echo $material_code;
            if (($material_code)AND(!in_array($id,$mat_check_errors))AND($vals[$i]["attributes"]["MY_DOUBLE"]<1))
            {
                if ($vals[$i]["attributes"]["MY_DOUBLE"]<>1) $sql_mat_get = "SELECT * FROM MATERIAL WHERE CODE=". $material_code.";";
                else $sql_mat_get = "SELECT * FROM MATERIAL_DOUBLE WHERE CODE=". $material_code.";";
                $material_result = sqlDoselect($sql_mat_get, $link);
                if (mysqli_num_rows($material_result) < 1)
                {
                    $error = $error . "<hr>плитного материала ".$vals[$i]["attributes"]["NAME"]." с кодом (".$vals[$i]["attributes"]["CODE"].") в БД не обнаружено, обратитесь к системному администратору.<hr>";
                    $mat_lose = 1;
                    $material_result=array("0"=>1);
                } 
                elseif (mysqli_num_rows($material_result) > 1) 
                {
                    $error = $error . "<hr> плитных материалов с таким кодом в БД более одного, обратитесь к администратору.<hr>";
                    $mat_lose = 1;
                    $material_result=array("0"=>1);
                } 
                else 
                {
                    $material_result = mysqli_fetch_array($material_result);
                    $mat_lose=0;
                   
                }
            }
            else{
                $mat_lose = 1;
                $material_result=array("0"=>1);
            }
            foreach ($vals[$i]["attributes"] as $k5=>$v5)
            {      
                $good_sheet_{$id}[$k5] = $v5;
                if ($mat_lose>0) 
                {
                    $material_result[$k5] ==  $good_sheet_{$id}[$k5];
                }
            }
            if ($mat_lose>0) 
            {
                $material_result["MATERIAL_ID"] ="NULL";
            }
            $mat_changes_text=0;
            if (!in_array($id,$mat_check_errors)) $mat_sheet_con_id{$id}=array("id_in"=>$id,"id_db"=>$material_result["MATERIAL_ID"]);
           //print_r_($mat_sheet_con_id);
           foreach ($good_sheet_[$id] as $k6=>$v6) 
            {
                if ((!in_array($k6,$array_temp_sheet)&&($mat_lose==0))AND(!in_array($id,$mat_check_errors))AND($vals[$i]["attributes"]["MY_DOUBLE"]<1))
                {
                    
                    if (($v6 <> $material_result[$k6]) && ($k6 <> "ID"))
                    {
                    
                        $mat_changes_text++;
                        if ($mat_lose==0)
                        {
                            if($mat_changes_text==1)
                            {
                                $error_ok.="<hr> В обрабатываемом материале ".$material_result["NAME"]." (".$material_result["CODE"].", id=".$material_result["ID"].") некоторые свойства внесены ошибочно: <hr>";
                            }
                            $error_ok.= "Меняем свойство материала " . $description_giblab[$k6] . " с " . $v6 . " на ". $material_result[$k6] . "<hr>";
                        }
                        $good_sheet_{$id}[$k6] = $material_result[$k6];
                    }
                }
            }
            $mat_check_errors[]=$id;
            if ($mat_lose==0)
            {
                if ($material_result["ST"] > 0) $tool_cutting_for = "wt";
                else $tool_cutting_for = "sh";
            }
            else $tool_cutting_for = "wt";
            if(!in_array($id_,$id_sheet_t))
            {
                $error_ok.=change_tool_cutting_per($array_temp_sheet,$good_tool_cutting_{$id_}, "TOOL_CUTTING", $db, $link, $type, $place, $tool_cutting_for, $description_giblab);
                $good_tool_cutting_{$id_}=change_tool_cutting_per_new_values($array_temp_sheet,$good_tool_cutting_{$id_}, "TOOL_CUTTING", $db, $link, $type, $place, $tool_cutting_for, $description_giblab);
            }
            
            if ($vals[$i]["type"]=="open")
            {
                $j=1;
                while (($vals[$i+$j]["tag"]=="PART")&& ($vals[$i+$j]["type"]=="complete"))
                {
                    $id__=$vals[$i+$j]["attributes"]["ID"];
                    if(!in_array($id_,$id_sheet_t))
                    {
                        foreach ($vals[$i+$j]["attributes"] as $k7=>$v7)
                        {
                            $good_sheet_part_{$id__}[$k7] = $v7;
                        }
                    
                        if($good_sheet_part_{$id__}["SHEETID"]>0)
                        {
                            $error_ok.= "<hr> В материале код (".$material_code.") есть остаток! ".$good_sheet_part_{$id__}["W"].
                            " x ".$good_sheet_part_{$id__}["L"]."<hr>";
                            $parts_material_to_put_in{$material_code}["w".$id__]=$good_sheet_part_{$id__}["W"];
                            $parts_material_to_put_in{$material_code}["l".$id__]=$good_sheet_part_{$id__}["L"];
                        }
                        $good_sheet_part_{$id__}["MATERIAL_ID"]=$material_result["MATERIAL_ID"];
                    }
                    $j=$j+1;
                    $cut_mat_part_{$id_}{$id}[]=$id__;
                    
                }
            }
            $id_sheet_t[]=$id_;
        }
    }
    if ($vals[$i]["attributes"]["TYPEID"]=="tool.edgeline")
    {
        $id_=$vals[$i]["attributes"]["ID"];
        foreach ($vals[$i]["attributes"] as $k7=>$v7)
        {      
            $good_tool_edgeline_{$id_}[$k7] = $v7;
        }
        if(!in_array($id_,$id_band_t))
        {
            $error_ok.=change_tool_edgeline_per($array_temp_sheet,$good_tool_edgeline_{$id_},"TOOL_EDGELINE",$db,$link,$place,$description_giblab);
            $good_tool_edgeline_{$id_}=change_tool_edgeline_per_new_values($array_temp_sheet,$good_tool_edgeline_{$id_},"TOOL_EDGELINE",$db,$link,$place,$description_giblab);
        }
        $id_band_t[]=$id_;

        foreach ($index["OPERATION"] as $i6)
        {
            if (($vals[$i6]["attributes"]["TYPEID"]=="EL")AND($vals[$i6]["attributes"]["TOOL1"]==$id_))
            {
                $id_band=$vals[$i6+1]["attributes"]["ID"];
            }
        }
        foreach ($index["GOOD"] as $i6)
        {
            if (($vals[$i6]["attributes"]["TYPEID"]=="band")AND($vals[$i6]["attributes"]["ID"]==$id_band))
            {
                $index_band=$i6;
            }
        }
        if ($index_band)
        {
            $i=$index_band;
            unset ($index_band);
            $id=$vals[$i]["attributes"]["ID"];
            $material_code = $vals[$i]["attributes"]["CODE"];
            $sql_mat_edge_get = "SELECT * FROM BAND WHERE CODE=\"" . $material_code . "\";";
            //test ("sql",$sql_mat_edge_get);
            $material_edge_result = sqlDoselect($sql_mat_edge_get, $link);
            
            if (mysqli_num_rows($material_edge_result) < 1)
            {
                if (!in_array($id,$band_id_temp_check))
                {
                    $edge_error=" <hr>кромки ".$vals[$i]["attributes"]["NAME"]." с кодом (".$vals[$i]["attributes"]["CODE"].") в БД не обнаружено, обратитесь к системному администратору.<hr>";
                    $error = $error .$edge_error;
                }
                $mat_edge_lose = 1;
                $material_edge_result="";
            } 
            elseif (mysqli_num_rows($material_edge_result) > 1) 
            {
                if (!in_array($id,$band_id_temp_check))
                {
                    $error = $error . "<hr> кромок с кодом (".per($block1, "id").") в БД более одного, обратитесь к админимтсратору.<hr>";
                }
                $mat_edge_lose = 1;
                $material_edge_result="";
            }
            else
            {
                $material_edge_result = mysqli_fetch_array($material_edge_result);
                $mat_edge_lose=0;
            }
            $band_id_temp_check[]=$id;
            $id=$vals[$i]["attributes"]["ID"];
            foreach ($vals[$i]["attributes"] as $k8=>$v8)
            {      
                $good_band_{$id}[$k8] = $v8;
                if ($mat_edge_lose>0) 
                {
                    $material_edge_result[$k] ==   $good_band_{$id}[$k];
                }
                
            }
            if ($mat_edge_lose>0) 
            {
                $material_edge_result["BAND_ID"] ="NULL";
            }
            $mat_edge_changes_text=0;
            $mat_band_con_id{$id}=array("id_in"=>$id,"id_db"=>$material_edge_result["BAND_ID"]);
            foreach ($good_band_{$id} as $k9=>$v9) 
            {
                if (($k9<>"ID")&&($mat_edge_lose==0))
                {
                    
                    if (($v9 <> $material_edge_result[$k9]) && (!in_array($k9,$array_temp_sheet)))
                    {
                        $mat_edge_changes_text++;
                        if ($mat_edge_lose==0)
                        {
                            if ($mat_edge_changes_text=1)
                            {
                                $error_ok.= "<hr> Во внесенной в проект кромке (".$good_band_{$id}["NAME"].", id=".$good_band_{$id}["ID"].") есть неточности: <hr>";
                            }
                            $error_ok.= "Меняем свойство кромки " . $description_giblab[$k9] . " (".$k9.") с " . $v9 . " на " 
                            . $material_edge_result[$k9] . "<hr>";
                        }
                        $good_band_{$id}[$k9]=$material_edge_result[$k9];
                    }
                }
            }
            $good_band_{$id}["MATERIAL_ID"]=$material_edge_result["BAND_ID"]; 
            $band_array_con[$id_]=$id;
            }
    }
}
$a=array("CS","EL","XNC","GR");
foreach ($index["OPERATION"] as $i)
{
   if(($vals[$i]["type"]<>"complete")&&(in_array($vals[$i]["attributes"]["TYPEID"],$a)))
   {
        $op=$vals[$i]["attributes"]["TYPEID"];
        $id=$vals[$i]["attributes"]["ID"];
        foreach ($vals[$i]["attributes"] as $k1=>$v1)
        {      
            $operation_[$op]{$id}[$k1] = text_programm($v1);
        }
        if ($vals[$i]["type"]=="open")
        {
            if ($vals[$i+1]["tag"]=="MATERIAL")
            {
               
                $operation_mat_[$op]{$id}["MATERIAL_ID"]=$vals[$i+1]["attributes"]["ID"];
                $operation_mat_[$op]{$id}["MATERIAL_COUNT"]=$vals[$i+1]["attributes"]["COUNT"];
                
                $i++;
                if ($op=="CS")
                {
                    //print_r_($mat_sheet_con_id);
                    foreach($mat_sheet_con_id as $value)
                    {
                        if($value["id_in"]==$operation_mat_{$op}{$id}["MATERIAL_ID"])
                        {
                            $mat_sheet_id{$id}=$value["id_db"];
                        }
                    }
                }
                if ($op=="EL")
                {
                    foreach($mat_band_con_id as $value)
                    {
                        if($value["id_in"]==$operation_mat_{$op}{$id}["MATERIAL_ID"])
                        {
                            $mat_band_id{$id}=$value["id_db"];
                        }
                    }
                }
            }
            $operation_mat_{$op}{$id}["type"]=$op;
            
            if ($vals[$i+1]["tag"]=="PART")
            {
                $i=$i+1;
                $ii=0;
                $operation_mat_{$op}{$id}["PARTS"] = "";
                $operation_mat_{$op}{$id}["OP_ID"]=$operation_{$op}{$id}["ID"];
                while(($vals[$i]["tag"]=="PART")&&($vals[$i]["type"]<>"close"))
                {
                    if ($ii > 0) $operation_mat_{$op}{$id}["PARTS"] = $operation_mat_{$op}{$id}["PARTS"] . "__" . $vals[$i]["attributes"]["ID"];
                    else $operation_mat_{$op}{$id}["PARTS"] = $vals[$i]["attributes"]["ID"];
                    $i=$i+1;
                    $ii++;
                }
                $parts_op_{$op}{$id}=explode("__",$operation_mat_{$op}{$id}["PARTS"]);
            }
              
        }   
   }
   elseif(($vals[$i]["type"]=="complete")&&(in_array($vals[$i]["attributes"]["TYPEID"],$a)))
   {
        $op=$vals[$i]["attributes"]["TYPEID"];
        $id=$vals[$i]["attributes"]["ID"];
        foreach ($vals[$i]["attributes"] as $k1=>$v1)
        {      
            $operation_[$op]{$id}[$k1] = text_programm($v1);
        }
        $operation_mat_{$op}{$id}["type"]=$op;
        if ($vals[$i+2]["tag"]=="PART")
        {
            $i=$i+2;
            $ii=0;
            $operation_mat_{$op}{$id}["PARTS"] = "";
            $operation_mat_{$op}{$id}["OP_ID"]=$operation_{$op}{$id}["ID"];
            while(($vals[$i]["tag"]=="PART")&&($vals[$i]["type"]<>"close"))
            {
                if ($ii > 0) $operation_mat_{$op}{$id}["PARTS"] = $operation_mat_{$op}{$id}["PARTS"] . "__" . $vals[$i]["attributes"]["ID"];
                else $operation_mat_{$op}{$id}["PARTS"] = $vals[$i]["attributes"]["ID"];
                $i=$i+1;
                $ii++;
            }
            $parts_op_{$op}{$id}=explode("__",$operation_mat_{$op}{$id}["PARTS"]);
        }
   }
}
?>