<?php
// Отчёт по РС для производства
require_once 'dompdf/lib/html5lib/Parser.php';
require_once 'dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'dompdf/src/Autoloader.php';
require_once __DIR__ . '/../func_mini.php';

Dompdf\Autoloader::register();
use Dompdf\Dompdf;

// $res = getRSRfPPDF(252452);
// $res = getRSRfPPDF(252454);
// $res = getRSRfPPDF(37562);

function getRSRfPPDF($id) {
	$data = RSDetailToPDF($id);
	$html = getHTMLl($data, $data['project_id']);
	// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->set_option("isRemoteEnabled",true);
	$dompdf->loadHtml($html);
	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
	// Render the HTML as PDF
	$dompdf->render();
	// Output the generated PDF to Browser
	$ret = $dompdf->output('RSDetailFP', ['Attachment' => 0]);
	return $ret;
}

function writeRSRfPPDF($id) {
	$data = RSDetailToPDF($id);
	$html = getHTMLl($data, $data['project_id']);
	// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->set_option("isRemoteEnabled",true);
	$dompdf->loadHtml($html);
	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
	// Render the HTML as PDF
	$dompdf->render();
	// Output the generated PDF to Browser
	$ret = $dompdf->output();
	return $ret;
}
// ------------------------------------Функции-----------------------------------------

/**
* Подготовка HTML для вывода PDF
**/
function getHTMLl($array, $id) {
	global $main_dir;
	$data    = null;
	$clAndpl = getPlaceAndClient($id);
	$RSArray = $array;
	// $doors = $RSArray['doors_data'];
	// Массив-маска видов декора
	$decor = array();
	if ($result = getTableData('SELECT * FROM `rs_decor`')) {
		foreach ($result as $value) {
			$decor[$value['id']] = $value['name'];
		}
	}
	// Массив-маска услуг по дверям
	$DSPattern = array(
		0 => 'Порезка раздвижных систем',
		1 => 'Засверловка раздвижных систем',
		2 => 'Сборка раздвижных дверей'
	);
	// Массив-маска видов обработки
	$secOptions = array(
	    'M' => array(
	        "0" => 'Без обработки',
	        "1" => 'Пескоструй',
	        "2" => 'Снятие амальгамы',
	        "6" => 'Плёнка Frost'
	    ),
	    'G' => array(
	        "0" => 'Без обработки',
	        "1" => 'Фотопечать с ламинацией на стекло (пленка)',
	        "2" => 'УФ-фотопечать с ламинацией белой пленкой',
	        "5" => 'УФ-фотопечать с заливкой белого фона',
	        "4" => 'Стекло – прозрачное + оракал сзади',
	        "6" => 'Плёнка Frost'
	    ),
	);
	$client = null;

    // Комплектующие а так-же общее количество штук и погонных метров
    $accessories 	= array();
    $c = 0;
    $sht = 0;
    $mp = 0;
    foreach ($RSArray['goods'] as $key => $value) {
    	if (isset($RSArray['goods_profiles'][$key][0])) {
    		$accessories[$c]['cnt'] = key($RSArray['goods_profiles'][$key][0]);
    		$accessories[$c]['size'] = $RSArray['goods_profiles'][$key][0][$accessories[$c]['cnt']];
    	} else {
    		$accessories[$c]['cnt'] = '';
    		$accessories[$c]['size'] = '';
    	}
    	$accessories[$c]['name'] = $value['name'];
    	$accessories[$c]['unit'] = $value['unit'];
    	$accessories[$c]['count'] = $value['count'];
    	if ($value['unit'] == 'шт' || $value['unit'] == 'к-т' || $value['unit'] == 'комплект' || $value['unit'] == 'упак' || $value['unit'] == 'упаковка'){
    		$sht += $value['count'];
    	} else $mp += $value['count'];
    	$sht += (int)$accessories[$c]['cnt'];
    	$c++;
    }

    // Услуги
    $services	= array();
    $c 			= 0;
    foreach ($RSArray['goods_serv'] as $value) {
    	if (stristr($value['name'], (string)$DSPattern[0]) || stristr($value['name'], (string)$DSPattern[1]) || stristr($value['name'], (string)$DSPattern[2])) {
    		$services[$c]['name'] = $value['name'];
    		$services[$c]['unit'] = $value['unit'];
    		$services[$c]['count'] = $value['count'];
    		$c++;
    	}
    }
    // Двери (стекло/ДСП) количество
    $doors = array(
    	'glass' => 0,
    	'sheet' => 0,
    	'mixed' => 0
    );
    // Наполнение
	$filling = array();
	$c = 0;
    foreach ($RSArray['doors_data'] as $key => $value) {
    	if ($key > 0) {
    		$mat_g = null; $mat_s = null;
    		foreach ($value as $val) {
    			if ($val['mat'] == 'glass') $mat_g = $val['mat'];
    			elseif ($val['mat'] == 'sheet') $mat_s = $val['mat'];
    			$filling[$c]['name'] = $val['mat_name'];
    			$filling[$c]['width'] = $val['section_x'];
    			$filling[$c]['lenght'] = $val['section_y'];
    			$filling[$c]['count'] = $val['calc']['count'];
    			$filling[$c]['size'] = $val['calc']['mat_area'];
    			$c++;
    		}
    		if (!empty($mat_g) && !empty($mat_s)) {
    			$doors['mixed']++;
    		} else {
    			if (!empty($mat_g)) $doors['glass']++;
    			elseif (!empty($mat_s)) $doors['sheet']++;
    		}
    	}
    }

    // Количество дверей
    $count_doors	= count($doors)-1;
    // Количество перехлёстов
    $count_overlaps	= substr($RSArray['rs_count_doorsOverlap'], -1, 1);

    // Дата заказа
    $orderdate 		= date("d.m.Y", substr($_SESSION['vals'][0]['attributes']['ORDERDATE'], 0, 10));
    // Профиль РС
    $profile		= $decor[$RSArray['profileDecors']];
	// Услуги по наполнению
	$fillServ = array();
	$c = 0;
	foreach ($RSArray['goods_serv'] as $value) {
    	if (!stristr($value['name'], (string)$DSPattern[0]) && !stristr($value['name'], (string)$DSPattern[1]) && !stristr($value['name'], (string)$DSPattern[2])) {
    		$fillServ[$c]['name'] = $value['name'];
    		$fillServ[$c]['unit'] = $value['unit'];
    		$fillServ[$c]['count'] = $value['count'];
    		$c++;
    	}
    }
    // Блок комплектующие (часть таблицы)
    $c = 1;
    $acc = null;
    foreach ($accessories as $value){
    	if (($value['unit'] == 'шт' || $value['unit'] == 'к-т' || $value['unit'] == 'комплект' || $value['unit'] == 'упак' || $value['unit'] == 'упаковка') && !empty($value['count'])) {
    		$cu = $value['count'];
    	} else $cu = $value['cnt'];
    	if ($value['unit'] == 'м.п.' || $value['unit'] == 'м.кв.'){
    		$cs = round($value['count'], 2);
    	} else $cs = null;
    	if ($value['size']) {
    		$value['size'] = round($value['size'], 2);
    	}
    	$acc .= '<tr>
				    <td>'.$c.'</td>
				    <td>'.$value['name'].'</td>
				    <td>'.$value['unit'].'</td>
				    <td>'.$value['size'].'</td>
				    <td>'.$cu.'</td>
				    <td>'.$cs.'</td>
				</tr>';
	    $c++;
    }

    // Блок наполнение (часть таблицы)
    $c = 1; $cnt = 0; $s = 0;
    $fil = null;
    foreach ($filling as $key => $value){
    	$fil .= '<tr>
				    <td>'.$c.'</td>
				    <td>'.$value['name'].'</td>
				    <td>м.кв.</td>
				    <td>'.round($value['width'], 2).'</td>
					<td>'.round($value['lenght'], 2).'</td>
				    <td>'.$value['count'].'</td>
				    <td>'.$value['size'].'</td>
				</tr>';
	    $c++; $cnt += $value['count']; $s += $value['size'];
    }

    // Блок услуги по наполнению (часть таблицы)
    $c = 1; $s = 0;
    $fiS = null;
    foreach ($fillServ as $key => $value){
    	$fiS .= '<tr>
				    <td>'.$c.'</td>
				    <td>'.$value['name'].'</td>
				    <td>'.$value['unit'].'</td>
					<td>'.$value['count'].'</td>
				</tr>';
	    $c++; $s += $value['count'];
    }

    // Блок услуги по дверям (часть таблицы)
    $c = 1; $s = 0;
    $srv = null;
    foreach ($services as $key => $value){
    	$srv .= '<tr>
				    <td>'.$c.'</td>
				    <td>'.$value['name'].'</td>
				    <td>'.$value['unit'].'</td>
					<td>'.$value['count'].'</td>
				</tr>';
	    $c++; $s += $value['count'];
    }

	$data_header = '
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<style>
			body{font-family: DejaVu Sans;}
			.AC{text-align: center;}
			.BD{font-weight:bold;}
			.mnpn{margin: 0;padding: 0;}
			.MA{margin-left: auto;margin-right: auto;}
			.MTAMB{margin-top: 20px;margin-bottom: 60px;}
			.FLL{float: left;}
			.FLR{float: right;}
			.w50{width: 50%;}
			.header-container span{font-size: 1.3rem;color: #666;}
			.headers span{color:#777;}
			.RS-table{margin: 10px auto 20px;}
			.RS-table .one-line{border: 1px solid #444;border-bottom: unset;}
			.RS-table .one-line:last-child{border-bottom: 1px solid #444;}
			.col-left{border-right: 1px solid #444;}
			.one-line .col p{margin: 0;}
			.rs-detail-image{max-width: 126px;height: auto;}
			.container {padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;}
			@media (min-width: 768px) {.container {width: 750px;}}@media (min-width: 992px) { .container {width: 970px;}}@media (min-width: 1200px) {.container {width: 1170px;}}
			.row{display: flex;margin-right: -15px;margin-left: -15px;}
			.col{padding: 5px;}
			.col-md-4 {width: 33.33333333%;}
			.col-md-8 {width: 66.66666667%;}
			table {border:1px solid #000;font-size:0.7rem;margin-bottom:20px;}
			table tr td{border-bottom:1px solid #000;border-right:1px solid #000;padding-left:5px;padding-right:5px;}
			table tr td:last-child{border-right:none;}
			table tr:last-child td{border-bottom:none;}
			.footer-text p{font-size:0.7rem;}
		</style>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Документ</title>
	</head>
	<body>';

	$data = '
		<h1 class="AC">Заказ Раздвижных систем</h1>
		<div>
			<div style="margin-bottom:20px;" class="headers">
				<h5 style="margin:0;padding:0;">'.$clAndpl[1][0].' '.$clAndpl[1][1].' '.$clAndpl[1][2].'</h5>
				<h5 style="margin:0;padding:0;">Раздвижная система: <span>'.$RSArray["rs_profile_val"].'</span></h5>
				<h5 style="margin:0;padding:0;">Цвет: <span>'.$profile.'</span></h5>
			</div>
			<div>
				<table>
				  <tr>
				    <td class="BD">Заказчик</td>
				    <td>телефон</td>
				    <td>Дата заказа</td>
				  </tr>
				  <tr>
				    <td>'.$clAndpl[0]['name'].'</td>
				    <td>'.$clAndpl[0]['phone'].'</td>
				    <td>'.date('d.m.Y', strtotime($clAndpl[0]['date'])).'</td>
				  </tr>
				</table>
			
				<table>
				  <tr>
				    <td colspan="6" class="AC BD">Размеры проема</td>
				  </tr>
				  <tr>
				    <td>Высота, мм.</td>
				    <td>Ширина, мм.</td>
				    <td>Дверей ДСП, шт.</td>
				    <td>Дверей стекло, шт.</td>
				    <td>Дверей комб-х, шт.</td>
				    <td>Перех-в</td>
				  </tr>
				  <tr>
				    <td>'.round($RSArray['rs_size_y'], 2).'</td>
				    <td>'.round($RSArray['rs_size_x'], 2).'</td>
				    <td>'.$doors['sheet'].'</td>
				    <td>'.$doors['glass'].'</td>
				    <td>'.$doors['mixed'].'</td>
				    <td>'.$count_overlaps.'</td>
				  </tr>
				  <tr>
				    <td colspan="4">ИТОГО Дверей, шт.</td>
				    <td>'.$count_doors.'</td>
				    <td></td>
				  </tr>
				</table>

				<table>
				  <tr>
				    <td colspan="3" class="AC BD">Габариты двери</td>
				  </tr>
				  <tr>
				    <td>Высота, мм.</td>
				    <td>Ширина, мм.</td>
				    <td>Количество дверей</td>
				  </tr>
				  <tr>
				    <td>'.round($RSArray['doors_data'][1][0]['height'], 2).'</td>
				    <td>'.round($RSArray['doors_data'][1][0]['width'], 2).'</td>
				    <td>'.$count_doors.'</td>
				  </tr>
				</table>

				<table>
				  <tr>
				    <td></td>
				    <td class="BD">Комплектующие</td>
				    <td>Ед. изм.</td>
				    <td>Размер, мм.</td>
				    <td>Кол-во шт.</td>
				    <td>Кол-во м.п.</td>
				  </tr>'
				  .$acc.
				  '<tr>
					<td colspan="4">ИТОГО</td>
					<td>'.$sht.'</td>
					<td>'.$mp.'</td>
				   </tr>
				</table>

				<table>
				  <tr>
				    <td></td>
				    <td class="BD">Наполнение</td>
				    <td>Ед. изм.</td>
				    <td>Высота, мм.</td>
				    <td>Ширина, мм.</td>
				    <td>Кол-во, шт.</td>
				    <td>Площадь, м.кв.</td>
				  </tr>'
				  .$fil.
				  '<tr>
					<td colspan="5">ИТОГО</td>
					<td>'.$cnt.'</td>
					<td>'.$s.'</td>
				   </tr>
				</table>

				<table>
				  <tr>
				    <td></td>
				    <td class="BD">Услуги по наполнению</td>
				    <td>Ед. изм.</td>
				    <td>Кол-во, шт.</td>
				  </tr>'
				  .$fiS.
				  '<tr>
					<td colspan="3">ИТОГО</td>
					<td>'.$s.'</td>
				   </tr>
				</table>

				<table>
				  <tr>
				    <td></td>
				    <td class="BD">Услуги по дверям</td>
				    <td>Ед. изм.</td>
				    <td>Кол-во, шт.</td>
				  </tr>'
				  .$srv.
				  '<tr>
					<td colspan="3">ИТОГО</td>
					<td>'.$s.'</td>
				   </tr>
				</table>
			</div>
		</div>
	';

	$foot_text = '
		<div class="footer-text">
			<div style="margin-bottom:20px;" class="MTAMB headers">
				<h5 style="margin:0;padding:0;">Менеджер: <span>'.$clAndpl['manager']['name'].'</span></h5>
			</div>
			<div style="margin-bottom:20px;" class="MTAMB">
				<p class="mnpn">Размеры деталей принимаются в миллиметрах. Подпись заказчика___________________</p>
				<p class="mnpn">После оформления заказа дополнения и изменения принимаются в виде дополнительного заказа.</p>
				<p class="mnpn">Претензии по качеству изделий принимаются только при наличии оригинального бланка заказа и в момент отгрузки заказа клиенту.</p>
			</div>
			<div style="margin-bottom:20px;" class="MTAMB">
				<p class="mnpn">С момента изготовления, срок хранения заказа на складе составляет 10 рабочих дней.</p>
				<p class="mnpn">Товар изготовленный по индивидуальному заказу клиента, обмену и возврату не подлежит согласно Постановлению КМУ №172 от 19.03.1994 г.</p>
				<p class="mnpn">Заказ считается утверждённым с момента его оплаты.</p>
			</div>
			<div style="margin-bottom:20px;" class="MTAMB">
				<p class="mnpn">Претензий по качеству и количеству не имею, заказ получил(а)</p>
			</div>
			<div style="margin-bottom:20px;" class="MTAMB MA">
				<p class="mnpn FLL">ДАТА___________________</p>
				<p class="mnpn FLR">ПОДПИСЬ___________________</p>
			</div>
		</div>
	';

	$data_footer = '</body></html>';
	return $data_header.$data.$foot_text.$data_footer;
}

?>