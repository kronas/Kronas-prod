<?php
require_once 'dompdf/lib/html5lib/Parser.php';
require_once 'dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'dompdf/src/Autoloader.php';
require_once __DIR__ . '/../func_mini.php';

Dompdf\Autoloader::register();
use Dompdf\Dompdf;

// $res = getRSPDF(252452);

function getRSPDF($id) {
	$data = RSDetailToPDF($id);
	$html = getHTML($data);
	// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->set_option("isRemoteEnabled",true);
	$dompdf->loadHtml($html);
	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
	// Render the HTML as PDF
	$dompdf->render();
	// Output the generated PDF to Browser
	$ret = $dompdf->output('RSDetail', ['Attachment' => 0]);
	return $ret;
}

function writeRSPDF($id) {
	$data = RSDetailToPDF($id);
	$html = getHTML($data);
	// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->set_option("isRemoteEnabled",true);
	$dompdf->loadHtml($html);
	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4', 'portrait');
	// Render the HTML as PDF
	$dompdf->render();
	// Output the generated PDF to Browser
	$ret = $dompdf->output();
	return $ret;
}
// ------------------------------------Функции-----------------------------------------

/**
* Подготовка HTML для вывода PDF
**/
function getHTML($array) {
	global $main_dir;
	$RSArray = $array;
	$doors = $RSArray['doors_data'];

	// Массивы видов обработки
	$secOptions = array(
	    'M' => array(
	        "0" => 'Без обработки',
	        "1" => 'Пескоструй',
	        "2" => 'Снятие амальгамы',
	        "6" => 'Плёнка Frost'
	    ),
	    'G' => array(
	        "0" => 'Без обработки',
	        "1" => 'Фото печать',
	        "2" => 'УФ печать',
	        "5" => 'УФ печать с заливкой белого фона',
	        "4" => 'Стекло – прозрачное + оракал сзади',
	        "6" => 'Плёнка Frost'
	    ),

	);

	$data = '
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<style>
			body{font-family: DejaVu Sans;}
			.AC{text-align: center;}.header-container span{font-size: 1.3rem;color: #666;}.RS-table{margin: 10px auto 20px;}.RS-table .one-line{border: 1px solid #444;border-bottom: unset;}.RS-table .one-line:last-child{border-bottom: 1px solid #444;}.col-left{border-right: 1px solid #444;}.one-line .col p{margin: 0;}.rs-detail-image{max-width: 126px;height: auto;}
			.container {padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;}@media (min-width: 768px) {.container {width: 750px;}}@media (min-width: 992px) { .container {width: 970px;}}@media (min-width: 1200px) {.container {width: 1170px;}}.row{display: flex;margin-right: -15px;margin-left: -15px;}.col{padding: 5px;}.col-md-4 {width: 33.33333333%;}.col-md-8 {width: 66.66666667%;}
			table {border:1px solid #000;}
			table tr{width:300px}
			table tr td:first-child{width:130mm;padding-top:1mm;padding-bottom:1mm;padding-left:3mm;border-right: 1px solid #000;border-bottom: 1px solid #000;}
			table tr td:last-child{width:50mm;padding-top:1mm;padding-bottom:1mm;padding-left:3mm;border-bottom: 1px solid #000;}
			table tr:last-child td{border-bottom:none;}
		</style>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Документ</title>
	</head>
	<body>
	    <h1 class="AC">Спецификация раздвижной системы</h1>
		<table>
		  <tr>
		    <td>Название профиля</td>
		    <td>'.$RSArray['rs_profile_val'].'</td>
		  </tr>
		  <tr>
		    <td>Количество дверей_перехлёстов</td>
		    <td>'.$RSArray['rs_count_doorsOverlap'].'</td>
		  </tr>
		  <tr>
		    <td>Ширина раздвижной системы</td>
		    <td>'.round($RSArray['rs_size_x'], 2).'</td>
		  </tr>
		  <tr>
		    <td>Высота раздвижной системы</td>
		    <td>'.round($RSArray['rs_size_y'], 2).'</td>
		  </tr>
		</table>
	';

	foreach ($doors as $key => $value) {
		if ($key != 0) {
			$countDoors = count($doors) - 1;
			$countSecs = count($value);
			$build = '<h2>Дверь '.$key.' (из '.$countDoors.')<span style="font-size:18px"> секций: '.$countSecs.'</span></h2>';
			foreach ($value as $sec_key => $val) {
				$thisSec = $sec_key+1;
				$build .= '<h3>Секция '.$thisSec.'</h3>';
				//.. Определим тип секции и вид обработки:
				$processing = null;
                if (!$val['op']) {
                    $typeSec = 'Плитный материал';
                } else {
                    if ($val['op'] == 'M') {
                        $typeSec = 'Зеркало';
                    } elseif ($val['op'] == 'G') {
                        $typeSec = 'Стекло';
                    }
                    $processing = '<tr>
										<td>Обработка</td>
										<td>'.$secOptions[$val['op']][$val['selected']].'</td>
									</tr>';
                }
				//.. Наличие картинки:
				if (isset($val['image']) && !empty($val['image'])) {
	                if (preg_match('#[pic_rs].*$#', $val['image'], $res)) {
	                	$val['image'] = '<img src="'.$main_dir.'/'.$res[0].'" class="rs-detail-image">';
	                    // $val['image'] = '<img src="'.$serv_main_dir.'/'.$res[0].'" class="rs-detail-image">';
	                    // $val['image'] = '<div style="background-image:'.$serv_main_dir.'/'.$res[0].';width:70mm;"></div>';
	                } else $val['image'] = '';
	            } else {
	                $val['image'] = '';
	            }
	            $build .= '<table>';
				$build .= '
					<tr>
						<td>Высота скекции</td>
						<td>'.round($val['height'], 2).'</td>
					</tr>
					<tr>
						<td>Ширина секции</td>
						<td>'.round($val['width'], 2).'</td>
					</tr>
					<tr>
						<td>Высота вставки в секцию</td>
						<td>'.round($val['section_y'], 2).'</td>
					</tr>
					<tr>
						<td>Ширина вставки в секцию</td>
						<td>'.round($val['section_x'], 2).'</td>
					</tr>
					<tr>
						<td>Тип секции</td>
						<td>'.$typeSec.'</td>
					</tr>
					'.$processing.'
					<tr>
						<td>Наименование материала секции</td>
						<td>'.$val['mat_name'].'</td>
					</tr>
					<tr>
						<td>Цвет фона</td>
						<td><div style="width:25mm;height:20mm;background:'.$val['color'].';"></div></td>
					</tr>
					<tr>
						<td>Картинка для наполнителя секции</td>
						<td>'.$val['image'].'</td>
					</tr>
					<tr>
						<td>Описание</td>
						<td>'.$value[$sec_key]['calc']['description'].'</td>
					</tr>
				';
				$build .= '</table>';
			}
			$data .= $build;
		}
	}
	$data .= '</body></html>';
	return $data;
}

?>