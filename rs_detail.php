<?php
session_start();
$title = 'Спецификация раздвижной системы | Кронас';
include_once("header.php");
include_once("func_mini.php");

$temp_storage = $serv_main_dir.'/files/temp/';
$project_storage = $serv_main_dir.'/files/project_out/';

// В GET передаётся параметр, с помошью которого мы получаем данные проекта
// Передаём идентификатор
if (isset($_GET['ip']) && !empty($_GET['ip'])) {
    if (preg_match('#^[\d]+$#', $_GET['ip'])) {
        // Если проект есть в базе (файловое хранилище либо база данных)
        if ($p_data = get_Project_file($_GET['ip'])) {
            $RSArray = RS_from_project($p_data['project_data']);
            $doors = $RSArray['doors_data'];
        } elseif ($result = getTableData('SELECT `MY_PROJECT_OUT` FROM `PROJECT` WHERE `PROJECT_ID` = "'.$_GET['ip'].'"')) {
            $RSArray = RS_from_project(base64_decode($result[0][$fieldOfProject]));
            $doors = $RSArray['doors_data'];
            // _print($doors);
        }
    // Условие если вновь созданный проект идёт из zzz.php
    // zzz.php создаёт временный файл и в GET передаёт его имя (начинается с приставки st_)
    } elseif (preg_match('#^st_[\d]+$#', $_GET['ip'])) {
        if (file_exists($temp_storage.$_GET['ip'].'.tmp')) {
            $_SESSION['RSArray'] = file_get_contents($temp_storage.$_GET['ip'].'.tmp');
        }
        $RSArray = unserialize(base64_decode($_SESSION['RSArray']));
        $doors = $RSArray['doors_data'];
    }
}

// Массивы видов обработки
$secOptions = array(
    'M' => array(
        "0" => 'Без обработки',
        "1" => 'Пескоструй',
        "2" => 'Снятие амальгамы',
        "6" => 'Плёнка Frost'
    ),
    'G' => array(
        "0" => 'Без обработки',
        "1" => 'Фото печать',
        "2" => 'УФ печать',
        "5" => 'УФ печать с заливкой белого фона',
        "4" => 'Стекло – прозрачное + оракал сзади',
        "6" => 'Плёнка Frost'
    ),

);

?>
    <h1 class="AC">Спецификация раздвижной системы</h1>
    <div class="container">
        <div class="container RS-table">
            <div class="row one-line">
                <div class="col col-left col-md-8">
                    <p>Название профиля</p>
                </div>
                <div class="col col-md-4">
                    <p><?=$RSArray['rs_profile_val']?></p>
                </div>
            </div>
            <div class="row one-line">
                <div class="col col-left col-md-8">
                    <p>Количество дверей_перехлёстов</p>
                </div>
                <div class="col col-md-4">
                    <p><?=$RSArray['rs_count_doorsOverlap']?></p>
                </div>
            </div>
            <div class="row one-line">
                <div class="col col-left col-md-8">
                    <p>Ширина раздвижной системы</p>
                </div>
                <div class="col col-md-4">
                    <p><?=round($RSArray['rs_size_x'], 2)?></p>
                </div>
            </div>
            <div class="row one-line">
                <div class="col col-left col-md-8">
                    <p>Высота раздвижной системы</p>
                </div>
                <div class="col col-md-4">
                    <p><?=round($RSArray['rs_size_y'], 2)?></p>
                </div>
            </div>
        </div>
    </div>


<?php foreach ($doors as $key => $value): ?>
    <?php if ($key != 0): ?>
        <div class="container header-container">
            <h2>Дверь <?= $key ?> (из <?= count($doors) - 1 ?>)<span> секций: <?= count($value) ?></span></h2>
        </div>
        <div class="container">
            <?php foreach ($value as $sec_key => $val): ?>
                <?php
                    $processing = null;
                    if (!$val['op']) {
                        $typeSec = 'Плитный материал';
                    } else {
                        if ($val['op'] == 'M') {
                            $typeSec = 'Зеркало';
                            $processing = $secOptions[$val['op']][$val['selected']];
                        } elseif ($val['op'] == 'G') {
                            $typeSec = 'Стекло';
                            $processing = $secOptions[$val['op']][$val['selected']];
                        }
                    }
                    
                ?>
                <h5>Секция <?=$sec_key+1?></h5>
                <div class="container RS-table">
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Высота скекции</p>
                        </div>
                        <div class="col col-md-4">
                            <p><?=round($val['height'], 2)?></p>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Ширина секции</p>
                        </div>
                        <div class="col col-md-4">
                            <p><?=round($val['width'], 2)?></p>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Высота вставки в секцию</p>
                        </div>
                        <div class="col col-md-4">
                            <p><?=round($val['section_y'], 2)?></p>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Ширина вставки в секцию</p>
                        </div>
                        <div class="col col-md-4">
                            <p><?=round($val['section_x'], 2)?></p>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Тип секции</p>
                        </div>
                        <div class="col col-md-4">
                            <p><?=$typeSec?></p>
                        </div>
                    </div>
                    <?php if ($processing !== null): ?>
                        <div class="row one-line">
                            <div class="col col-left col-md-8">
                                <p>Обработка</p>
                            </div>
                            <div class="col col-md-4">
                                <p><?=$processing?></p>
                            </div>
                        </div>
                    <?php endif ?>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Наименование материала секции</p>
                        </div>
                        <div class="col col-md-4">
                            <p><?=$val['mat_name']?></p>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Цвет фона</p>
                        </div>
                        <div class="col col-md-4">
                            <div style="width: 45%; height: 50px; background: <?=$val['color']?>;">

                            </div>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Картинка для наполнителя секции</p>
                        </div>
                        <div class="col col-md-4">
                            <?php
                            if (isset($val['image']) && !empty($val['image'])) {
                                //.. После 28.02.2021 закомментированные строки можно удалять
                                // if (preg_match('#(pic_rs.*)$#', $val['image'], $res)) {
                                //     $res = $res[0];
                                //     $val = '<img src="/'.$res.'" class="rs-detail-image">';
                                // } elseif (preg_match('#(pic_custom.*)$#', $val['image'], $res)) {
                                //     $res = 'clients/RS/clients/'.trim($res[0], "/");
                                //     $val = '<img src="/'.$res.'" class="rs-detail-image">';
                                // } else $val = '';
                                if (preg_match('#(pic_custom.*)$#', $val['image'], $res)) {
                                    $res = 'clients/RS/clients/'.trim($res[0], "/");
                                    $val = '<img src="/'.$res.'" class="rs-detail-image">';
                                } else $val = '<img src="/'.$val['image'].'" class="rs-detail-image">';
                                // $val = $val['image'];
                            } else {
                                $val = '';
                            }
                            echo $val;
                            ?>
                        </div>
                    </div>
                    <div class="row one-line">
                        <div class="col col-left col-md-8">
                            <p>Описание</p>
                        </div>
                        <div class="col col-md-4">
                            <div><?=$value[$sec_key]['calc']['description']?></div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
<?php endforeach ?>

<?php include_once("footer.php"); ?>