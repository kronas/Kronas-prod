<?php
session_start();
require_once '_1/config.php';
require_once DIR_CORE . 'func.php';

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

if (!$link) {
    $db = $database_link_connect['database'];
    date_default_timezone_set('Europe/Kiev');
    $link = mysqli_connect($database_link_connect['host'], $database_link_connect['user'], $database_link_connect['password'], $db);
    mysqli_set_charset($link, "utf8");
}

if (!$type) $type = "auto";
// exit;
if ($_GET["data_id"]) {
    $_SESSION = get_temp_file_data($_GET["data_id"]);
    $_SESSION["data_id"] = $_GET["data_id"];
    
    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}

if(!$_SESSION['project_data']) {
    header("Location: ".$laravel_dir.'?nw='.$_GET['nw']);
}else $place=$_SESSION["user_place"];

// 28.01.2022 Если проект импортирован из Базиса
if (strripos($_SESSION["project_data"], 'importbmv')) {
    $_SESSION['basisProject'] = 1;
    $basisProject = 1;
} else {
    $_SESSION['basisProject'] = null;
    $basisProject = null;
}

if ($_GET["data_d"]) {
    unset($_SESSION, $_POST);
    session_destroy();

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}
if ($_SESSION["project_data"]) {



    put_temp_file_data($_SESSION["data_id"], $_SESSION);

    $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'index_start', $session);



    $_SESSION['project_data']=get_out_part_stock($_SESSION['project_data']);

    get_vals_index($_SESSION["project_data"]);
    $vals = $_SESSION["vals"];
    unset ($_SESSION["vals"]);
    $vals=vals_out($vals);
    if (!isset($_SESSION['vals_in_gr']))$_SESSION['vals_in_gr']=$vals;
    $vals=change_gr($vals);
    // xml ($vals);

    $new_images = pic_get(vals_index_to_project($vals));
    // p_($new_images);exit;
    // p_($_SESSION["vals"]);exit;

    $_SESSION['part_images'] = $new_images;
    $index=make_index($vals);
    ksort($vals);
    if (!isset($_SESSION["user_place"])) $place=1;
    else $place = $_SESSION["user_place"];
    // include("file_to_array.php");
    // $vals=vals_out($vals);
    // $index = make_index($vals);
    // ksort($vals);
    // include("array_to_file.php");
    // get_vals_index($main_text);
    // $vals = $_SESSION["vals"];
    $main_text=vals_index_to_project($vals);
    $_SESSION["project_data"] = $main_text;
    $_POST["project_data"] = $_SESSION["project_data"];
    //echo htmlspecialchars($main_text);
    //exit;
}

if ($_GET['go_back'] == 1) {

    $_SESSION['vals']=$_SESSION['vals_in_gr'];
    // p_($_SESSION);exit;
    $index = make_index($_SESSION['vals']);
    
    $_SESSION['project_data'] = vals_index_to_project($_SESSION['vals_in_gr']);
    unset($_SESSION['vals_in_gr']);
    put_temp_file_data($_SESSION["data_id"], $_SESSION);
    header("Location: ".$laravel_dir."/step_1?file=" . $_SESSION["data_id"].'&nw='.$_GET['nw']);
    exit();
}


if (isset($_POST) && isset($_POST['form_step']) && (int)$_POST['form_step'] == 2) {

   
    $_SESSION['project_data']=vals_index_to_project($_SESSION['vals_in_gr']);
    $vals =  $_SESSION['vals_in_gr'];
    $arr_parts = $_SESSION['arr_parts'];
    $arr_out = array();
    $arr_cut_angle = array();


    foreach ($arr_parts as $key => $value) {
        $_id = $value['ID'];
        if (isset($_POST['xnc_countmill_' . $_id]) || isset($_POST['stitching_' . $_id])) {
            $arr_out[$_id] = array();
            if (isset($_POST['xnc_countmill_' . $_id])) {
                $arr_out[$_id]['xnc_countmill'] = $_POST['xnc_countmill_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_KL'] = 1;
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_BAND_L'] = $_POST['xnc_band_l_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_BAND_R'] = $_POST['xnc_band_r_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_BAND_T'] = $_POST['xnc_band_t_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_BAND_B'] = $_POST['xnc_band_b_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELL'] = $_POST['xnc_el_' . $_id . '_l'] == 1 ? '1' : '0';
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELR'] = $_POST['xnc_el_' . $_id . '_r'] == 1 ? '1' : '0';
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELT'] = $_POST['xnc_el_' . $_id . '_t'] == 1 ? '1' : '0';
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELB'] = $_POST['xnc_el_' . $_id . '_b'] == 1 ? '1' : '0';
            }
            $arr_out[$_id]['part_id'] = $_id;
            if (isset($_POST['xnc_band_' . $_id . '_l'])) $arr_out[$_id]['xnc_band_l'] = $_POST['xnc_band_' . $_id . '_l'] == 'DB' ? $_POST['slectedxncid-l-' . $_id] : $_POST['xnc_band_' . $_id . '_l'];
            if (isset($_POST['xnc_band_' . $_id . '_r'])) $arr_out[$_id]['xnc_band_r'] = $_POST['xnc_band_' . $_id . '_r'] == 'DB' ? $_POST['slectedxncid-r-' . $_id] : $_POST['xnc_band_' . $_id . '_r'];
            if (isset($_POST['xnc_band_' . $_id . '_t'])) $arr_out[$_id]['xnc_band_t'] = $_POST['xnc_band_' . $_id . '_t'] == 'DB' ? $_POST['slectedxncid-t-' . $_id] : $_POST['xnc_band_' . $_id . '_t'];
            if (isset($_POST['xnc_band_' . $_id . '_b'])) $arr_out[$_id]['xnc_band_b'] = $_POST['xnc_band_' . $_id . '_b'] == 'DB' ? $_POST['slectedxncid-b-' . $_id] : $_POST['xnc_band_' . $_id . '_b'];
            if (isset($_POST['xnc_id_' . $_id])) $arr_out[$_id]['xnc_id'] = $_POST['xnc_id_' . $_id];

            if (isset($_POST['stitching_' . $_id])) {
                $_stitching_band = array('l', 'r', 't', 'b');
                foreach ($_stitching_band as $s) {
                    if (isset($_POST['stitching_band_' . $s . '_' . $_id])) {
                        $arr_out[$_id]['band_id_' . $s] = $_POST['stitching_band_' . $s . '_' . $_id] == 'DB' ? $_POST['slectedid-' . $s . '-' . $_id] : $_POST['stitching_band_' . $s . '_' . $_id];
                    }
                }
                if (isset($_POST['slectedi' . $_id])) $arr_out[$_id]['slected'] = $_POST['slectedi' . $_id];
                if (isset($_POST['type_double_' . $_id])) $arr_out[$_id]['type_double'] = $_POST['type_double_' . $_id];
                if (isset($_POST['back_double_' . $_id])) $arr_out[$_id]['back_double'] = $_POST['back_double_' . $_id];
                if (isset($_POST['band_inner_' . $_id]) && $_POST['band_inner_' . $_id] != 'none')
                {
                    if($_POST['band_inner_' . $_id]=='DB')
                    $arr_out[$_id]['band_edge'] = $_POST['slectedid-inner-' . $_id];
                    else
                    $arr_out[$_id]['band_edge'] = $_POST['band_inner_' . $_id];
                }
            }
        }
        // p_($_POST['my_cut_angle_' . $_id . '_l']);
        foreach (SIDE as $s)
        if (($_POST['my_cut_angle_sdvig_' . $_id . '_'.$s]>0)&&($_POST['my_cut_angle_' . $_id . '_'.$s]==0)) $_POST['my_cut_angle_sdvig_' . $_id . '_'.$s]=0;
        if ($_POST['my_cut_angle_' . $_id . '_l'] != 0 || $_POST['my_cut_angle_' . $_id . '_r'] != 0 || $_POST['my_cut_angle_' . $_id . '_t'] != 0 || $_POST['my_cut_angle_' . $_id . '_b'] != 0) {
           
            foreach (SIDE as $s)
            {
                $arr_cut_angle[$_id]['MY_CUT_ANGLE_'.mb_strtoupper($s)] =isset($_POST['xnc_el_' . $_id . '_'.$s]) ? 0 : $_POST['my_cut_angle_' . $_id . '_'.$s];
                $arr_cut_angle[$_id]['MY_CUT_SDVIG_ANGLE_'.mb_strtoupper($s)] =isset($_POST['xnc_el_' . $_id . '_'.$s]) ? 0 : $_POST['my_cut_angle_sdvig_' . $_id . '_'.$s];
                $arr_out[$_id]['angle_cut_'.$s] = isset($_POST['xnc_el_' . $_id . '_'.$s]) ? 0 : $_POST['my_cut_angle_' . $_id . '_'.$s];
                $arr_out[$_id]['angle_cut_sdvig_'.$s] = isset($_POST['xnc_el_' . $_id . '_'.$s]) ? 0 : $_POST['my_cut_angle_sdvig_' . $_id . '_'.$s];
            }
        }
        // p_($_POST);
        // p_($arr_cut_angle);
        // p_($arr_out);
        // exit;

        if ($_POST['my_cut_side_' . $_id . '_l'] != 0 || $_POST['my_cut_side_' . $_id . '_r'] != 0 || $_POST['my_cut_side_' . $_id . '_t'] != 0 || $_POST['my_cut_side_' . $_id . '_b'] != 0) {

            $arr_cut_side[$_id] = array(
                'MY_CUT_SIDE_T' => $_POST['my_cut_side_' . $_id . '_t'],
                'MY_CUT_SIDE_L' => $_POST['my_cut_side_' . $_id . '_l'],
                'MY_CUT_SIDE_R' => $_POST['my_cut_side_' . $_id . '_r'],
                'MY_CUT_SIDE_B' => $_POST['my_cut_side_' . $_id . '_b'],
                'MY_CUT_SIDE' => $_POST['cut_side_' . $_id ],

            );
        }
        
    }
        $_SESSION['arr_out'] = $arr_out;
        $_SESSION['arr_cut_angle'] = $arr_cut_angle;
        $_SESSION['arr_cut_side'] = $arr_cut_side;
        $_SESSION['arr_parts'] = $arr_parts;
        $_SESSION['out_del'] = isset($_POST['out_del']) ? $_POST['out_del'] : array();
        $vals=$_SESSION["vals_in_gr"];
        $index = make_index($vals);
        // p_($_POST);exit;
        foreach ($index['OPERATION'] as $i)
        {
            if (($vals[$i]['attributes']['TYPEID']=="CS")&&(isset($_POST['triml_'.$vals[$i]['attributes']['ID']])))
            {
                $vals[$i]['attributes']['CTRIML']=$_POST['triml_'.$vals[$i]['attributes']['ID']];
            }
            if (($vals[$i]['attributes']['TYPEID']=="CS")&&(isset($_POST['fix_card_'.$vals[$i]['attributes']['ID']])))
            {
                $vals[$i]['attributes']['operation.my_fix_card']=$_POST['fix_card_'.$vals[$i]['attributes']['ID']];
            }
            elseif (($vals[$i]['attributes']['TYPEID']=="CS")&&(!isset($_POST['fix_card_'.$vals[$i]['attributes']['ID']])))
            {
                unset($vals[$i]['attributes']['operation.my_fix_card']);
                unset($vals[$i]['attributes'][mb_strtoupper('operation.my_fix_card')]);
                // p_($vals[$i]);exit;
            }
            if (($vals[$i]['attributes']['TYPEID']=="CS")&&(isset($_POST['trimw_'.$vals[$i]['attributes']['ID']])))
            {
                $vals[$i]['attributes']['CTRIMW']=$_POST['trimw_'.$vals[$i]['attributes']['ID']];
            }
            if (($vals[$i]['attributes']['TYPEID']=="CS")&&(isset($_POST['cs_krat_'.$vals[$i]['attributes']['ID']])))
            {
                if ($_POST['cs_krat_'.$vals[$i]['attributes']['ID']]=='sheet')
                {
                    $vals[$i]['attributes']['operation.my_krat_sheet']='sheet';
                    $vals[$i]['attributes']['CSDIRECTCUT']='0';
                    
                }
                elseif ($_POST['cs_krat_'.$vals[$i]['attributes']['ID']]=='half')
                {
                    $vals[$i]['attributes']['operation.my_krat_sheet']='half';
                    $vals[$i]['attributes']['CSDIRECTCUT']='1';
                }
                unset ($vals[$i]['attributes']['DRAW'],$vals[$i]['attributes']['DATA']);
                
            }
        }
        foreach ($index['PART'] as $i) {
            if (($vals[$i]['attributes']['CL']>0)&&($_POST['pf_no_'.$i.'_'.$vals[$i]['attributes']['ID']]>0)) {
                //.. Срез закругления столешницы.
                //  Ширин (width) в атрибутах несколько:
                // DW - фактическая
                // W - фактическая
                // JW - фактическая, минус кромки с двух сторон
                // CW - фактическая, минус толщина последней установленной кромки
                //      не работает с кромками, меньше миллиметра.
                // Если кромки нет, сравниваем фактическую ширину (DW) с "CW" (CW не меняется)
                // А если есть, сравниваем (фаактическую ширину (DW), минус толщина кромок с двух сторон) с "JW"
                $round_off_flag = 0;
                $double_round_off_flag = 0;
                if ($vals[$i]['attributes']['CW'] == $vals[$i]['attributes']['DW']) {
                    $round_off_flag = 1;
                    if (($_POST['double_pf_no_'.$i.'_'.$vals[$i]['attributes']['ID']] > 0) && (($vals[$i]['attributes']['DW'] + 20 - $edges_thickness) >= $vals[$i]['attributes']['JW'])) {
                        $double_round_off_flag = 1;
                    }
                } elseif (($vals[$i]['attributes']['DW'] > $vals[$i]['attributes']['CW'])
                    || ($vals[$i]['attributes']['DW'] > ($vals[$i]['attributes']['CW'] - 3))
                    || ((($vals[$i]['attributes']['DW'] + 23) > $vals[$i]['attributes']['CW'])
                            && $_POST['double_pf_no_'.$i.'_'.$vals[$i]['attributes']['ID']]>0))
                {
                    //.. А здесь вычитаем кромку, если есть
                    $edge_index = make_index($vals);
                    $edges_thickness = 0;
                    // Нижнюю
                    if (isset($vals[$i]['attributes'][mb_strtoupper('elbmat')])) {
                        foreach ($edge_index['GOOD'] as $edges => $edge) {
                            if ($vals[$edge]['attributes']['TYPEID'] == 'band' && $vals[$edge]['attributes'][mb_strtoupper('name')] == $vals[$i]['attributes'][mb_strtoupper('elbmat')]) {
                                $edges_thickness += $vals[$edge]['attributes']['T'];
                                // break 1;
                            }
                        }
                    }
                    // Верхнюю
                    if (isset($vals[$i]['attributes'][mb_strtoupper('eltmat')])) {
                        foreach ($edge_index['GOOD'] as $edges => $edge) {
                            if ($vals[$edge]['attributes']['TYPEID'] == 'band' && $vals[$edge]['attributes'][mb_strtoupper('name')] == $vals[$i]['attributes'][mb_strtoupper('eltmat')]) {
                                $edges_thickness += $vals[$edge]['attributes']['T'];
                                break 1;
                            }
                        }
                    }
                    if (($vals[$i]['attributes']['DW'] - $edges_thickness) >= $vals[$i]['attributes']['JW']) {
                        $round_off_flag = 1;
                    }
                    if (($_POST['double_pf_no_'.$i.'_'.$vals[$i]['attributes']['ID']] > 0) && (($vals[$i]['attributes']['DW'] + 20 - $edges_thickness) >= $vals[$i]['attributes']['JW'])) {
                        $double_round_off_flag = 1;
                    }
                }
            
                if ($round_off_flag == 1) {
                    $vals[$i]['attributes']['part.my_postforming_cut_top'] = 1;
                    $vals[$i]['attributes'][mb_strtoupper('allowanceT')] = 20;
                    $vals[$i]['attributes']['DW'] -= $vals[$i]['attributes'][mb_strtoupper('allowanceT')];
                }
                if ($double_round_off_flag == 1) {
                    //.. 18.08.2021 Добавлено условие для второго закругления
                    $vals[$i]['attributes']['part.my_postforming_cut_bottom'] = 1;
                    $vals[$i]['attributes'][mb_strtoupper('allowanceB')] = 20;
                    $vals[$i]['attributes']['DW'] -= $vals[$i]['attributes'][mb_strtoupper('allowanceB')];
                }

                if ((!isset($_POST['double_pf_no_'.$i.'_'.$vals[$i]['attributes']['ID']]))
                    && ($vals[$i]['attributes'][mb_strtoupper('part.my_postforming_cut_bottom')] > 0)) {
                    //.. Возврат среза закругления столешницы (второго).
                    $vals[$i] = my_uncet($vals[$i],'my_postforming_cut_bottom');
                    if ( $vals[$i]['attributes'][mb_strtoupper('allowanceB')] == 20) {
                        $vals[$i]['attributes']['DW'] += $vals[$i]['attributes'][mb_strtoupper('allowanceB')];
                        $vals[$i]['attributes'][mb_strtoupper('allowanceB')] = 0;
                    }
                }
            } elseif (($vals[$i]['attributes']['CL'] > 0)
                && (!isset($_POST['pf_no_'.$i.'_'.$vals[$i]['attributes']['ID']]))
                && ($vals[$i]['attributes'][mb_strtoupper('part.my_postforming_cut_top')] > 0))
            {
                $vals[$i]['attributes'][mb_strtoupper('allowanceT')] = 0;
                $vals[$i]['attributes'][mb_strtoupper('allowanceB')] = 0;
                $vals[$i]['attributes']['DW'] = $vals[$i]['attributes']['JW'];
                $vals[$i] = my_uncet($vals[$i],'my_postforming_cut_top');
                $vals[$i] = my_uncet($vals[$i],'my_postforming_cut_bottom');
            }
            // exit();
            if (($vals[$i]['attributes']['CL']>0)&&(isset($_POST['paz_'.$vals[$i]['attributes']['ID']]))) {
                $vals[$i]['attributes']['part.my_gr_type']=$_POST['paz_'.$vals[$i]['attributes']['ID']];       
            }
        }
        $vals=add_tag_print($vals);
        $_SESSION["vals_in_gr"]=$vals;
        $_SESSION['project_data']=vals_index_to_project($vals);
              put_temp_file_data($_SESSION["data_id"], $_SESSION);
              //..12.07.2021 На локальном сервере код не выполняется "Заголовки отправлены", поэтому такой фикс
              // Было:
              // header("Location: double_do.php?data_id=".$_SESSION["data_id"].'&nw='.$_GET['nw']);
              // Стало:
              if (stripos($_SERVER['SERVER_NAME'], '.loc') || stripos($_SERVER['SERVER_NAME'], '.local')) {
                  echo '<meta http-equiv="refresh" content="0; url=/double_do.php?data_id='.$_GET["data_id"].'&nw='.$_GET['nw'].'">';
              } else {
                  header("Location: double_do.php?data_id=".$_SESSION["data_id"].'&nw='.$_GET['nw']);
              }
              exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/fontawesome/all.min.css">
    <link rel="stylesheet" href="css/jsLists.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/jsLists.js"></script>
    <script src="a1.js"></script>
    <script>
        $(document).ready(function () {
            $('.part_name_container').hover(async function (e) {
                if(!$(e.target).closest('.part_name_container').attr('data-loaded')) {
                    $(e.target).closest('.part_name_container').append(`
                        <div class="part_image_details_container">
                            <div style="position:relative">
                                <img src="<?php echo $main_dir; ?>/download.gif" alt="">
                            </div>
                        </div>
                    `);
                    let part = $(e.target).closest('.part_name_container').attr('data-img');
                    $(e.target).closest('.part_name_container').attr('data-loaded', true);
                    let request = await fetch(`<?php echo $main_dir; ?>/get_details_image_test.php?part_id=${part}&data_id=<?php echo $_SESSION['data_id'] ?>`, { method: 'GET' });
                    let body = await request.text();
                    $(e.target).closest('.part_name_container').find('.part_image_details_container').html(`<img src="${body}">`);
                }
                
            });
        });
    </script>
    <title>Внесение расширенной информации по деталям в проект</title>
    <style>
        .my-cut-angle-box {
            float: none;
        }
        #checks_lodaer_containers {
            position: fixed;
            top: 0;
            left: 0;
            background: rgba(255,255,255,0.4);
            width: 100%;
            height: 100%;
            display: none;
            z-index: 99999;
        }
        #checks_lodaer_containers > div {
            width: 100%;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            z-index: 9999;
            height: 100%;
        }
        body {
            padding: 35px;
        }
        #modal_checks_lodaer_containers {
            position: absolute;
            top: 0;
            left: 0;
            background: rgba(255,255,255,0.4);
            width: 100%;
            height: 100%;
            display: block;
            z-index: 99999;
        }
        #modal_checks_lodaer_containers > div {
            width: 100%;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            z-index: 9999;
            height: 100%;
        }
        #extra_info_dialog .modal-content {
            min-height: 400px;
            max-width: 70vw;
            text-align: left;
        }
        #extra_info_dialog .modal-dialog {
            min-height: 400px;
            max-width: 70vw;
            text-align: left;
        }
        #extra_info_dialog .extra_modal_image img {
            display: block;
            margin: 5px auto;
            text-align: center;
            max-width: 100%;
        }
        .part_image_details_container {
            position: absolute;
            width: 781px;
            height: 537px;
            border: 1px solid lightgray;
            display: none;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            background: white;
            overflow: hidden;
            bottom: -74%;
            left: 15vw;
            z-index: 9999;
        }
        .part_name_container:hover .part_image_details_container {
            display: flex;
        }
        .part_name_container {
            position: relative;
        }
        .part_image_details_container img {
            max-width: 100%;
        }
        .detail_image_specification_inner {
            height: auto;
            background: lightgray;
            border: 1px dashed red;
            margin: 0 auto;
            display: flex;
            flex-flow: column;
            justify-content: center;
            align-items: center;
        }
        .left-kromka-container h3 {
            transform: rotate(-180deg);
            -webkit-writing-mode: vertical-rl;
            writing-mode:vertical-rl;
            margin: 0;
            font-size: 18px;
        }
        .left-kromka-container {
            text-align: center;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            max-width: 54px;
        }
        .right-kromka-container h3 {
            transform: rotate(0deg);
            -webkit-writing-mode: vertical-rl;
            writing-mode:vertical-rl;
            margin: 0;
        }
        .right-kromka-container {
            text-align: center;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            max-width: 54px;
            display: none;
        }
        .left_background_kromka > div {
            background: red;
            width: 100%;
            padding: 0;
            height: 94%;
        }
        .left_background_kromka {
            max-width: 10px;
            width: 100%;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            padding: 0;
        }
        .right_background_kromka > div {
            background: green;
            width: 100%;
            padding: 0;
            height: 94%;

        }
        .right_background_kromka {
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            max-width: 10px;
            padding: 0;
        }
        .top_background_kromka {
            max-width: 100%!important;
            border-top: 15px solid blue;
            width: 100%;
            padding-top: 15px!important;
            margin: 0!important;
            margin-bottom: 15px!important;
            text-align: center;
        }
        .bottom_background_kromka {
            border-bottom: 15px solid yellow;
            max-width: 100%!important;
            width: 100%;
            padding-bottom: 15px!important;
            margin: 0!important;
            margin-top: 15px!important;
            text-align: center;
        }
        .main_container_row {
            justify-content: center;
            position: relative;
            margin-right: 51px;
        }
        .kromka_name_marker {
            max-width: 24px;
            padding: 0;
            text-align: center;
            margin-left: 15px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .left_kromka_name_marker {
            max-width: 34px;
            padding: 0;
            text-align: center;
            margin-left: 15px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .right_kromka_name_marker {
            max-width: 34px;
            padding: 0;
            text-align: center;
            margin-right: 15px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .kromka_marker_text {
            text-align: center;
            font-size: 18px;
            font-weight: bold;
        }
        .vertical_kromka_title {
            min-height: 54px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            font-size: 18px;
        }
        .detail_kromka_history {
            display: flex;
            flex-flow: column;
            justify-content: flex-start;
            align-items: stretch;
            margin-top: 31px;
        }
        .detail_images_links {
            display: flex;
            flex-flow: column;
            justify-content: center;
            align-items: center;
            margin-top: 31px;
        }
        .detail_kromka_extra {
            display: flex;
            flex-flow: row nowrap;
            justify-content: space-between;
            align-items: stretch;
        }
        .add_sshivka_container {
            width: 315px!important;
        }
        .stitching_bands select {
            width: 230px;
        }
        .detail_image_specification_inner_background {
            width: 90%;
            height: 5px;
            background: gray;
            margin: 5%;
        }
        .detail_image_specification_container {
            max-width: 607px;
        }
    </style>
</head>
<body>
<!-- Social networks -->
<script>
    const script = document.createElement('script');
    script.src = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.umd.js';
    script.onload = function () {
        const body = document.querySelector('body');
        const widgetEl = document.createElement('div');
        widgetEl.setAttribute('id', 'wt-omnichannel-widget');
        body.appendChild(widgetEl);            

        const config = {
            "view": {
                "lang": "en",
                "btnOpacity": "1",
                "logoUrl": "https://kronas.com.ua/Media/pic/icons/KR_logo_25x25px.svg",
                "accentColor": "hsl(292, 56.99999999999999%, 21%)"
            },
            "chat": {
                "url": "wss://cloud.webitel.ua/chat/hddvjbgfoitieuhtgvaygashqutmuan"
            },
            "alternativeChannels": {
                "viber": "viber://pa?chatURI=kronasbot",
                "telegram": "https://t.me/Kronascomua_bot",
                "facebook": "https://www.facebook.com/kronas.com.ua/"
            }
        };

        const app = new WtOmniWidget('#wt-omnichannel-widget', config);
    };
    document.head.appendChild(script);

    const link = document.createElement('link');
    link.href = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.css';
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.media = 'screen,print';
    document.head.appendChild(link);
</script>
<!-- Social networks -->

<!--</tbody>-->
<div class="modal fade" id="extra_info_dialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">

                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
<!--            <div class="modal-footer">-->
<!--                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
<!--                <button type="button" class="btn btn-primary">Save changes</button>-->
<!--            </div>-->
            <div id="modal_checks_lodaer_containers">
                <div>
                    <img src="<?php echo $main_dir; ?>/download.gif" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<?php
// p_($_SESSION);exit;
if (!isset($_GET["data_id"]))$_GET["data_id"]=$_SESSION["data_id"];
// echo $_GET["data_id"];exit;
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'index_form_start', $_SESSION);

echo '
<form method="post" enctype="multipart/form-data" action="index.php?data_id='.$_GET['data_id'].'&nw='.$_GET['nw'].'">';
   
    $sheet_not_one = array();
    $band_not_one = array();
    require_once DIR_CORE . 'functions1.php';
    if ((!$_POST["project_data"]) && (!$_FILES)) {
        // echo "!!!";
        if (!$_FILES && !isset($_POST['form_step'])) {
            echo "Выберите файл: <input type='file' name='filename' size='10' ><br>
            <input type='submit' value='Загрузить' >";
        }
        //exit;
    }
    if (($_FILES && $_FILES['filename']['error'] == UPLOAD_ERR_OK) OR ($_POST["project_data"])) {

        if (!$_POST["project_data"]) {
            // echo "!!!";
            $name = $_FILES['filename']['name'];
            $main_text1 = file_get_contents($_FILES['filename']['tmp_name']);
            //echo htmlspecialchars($main_text1);
            get_vals_index($main_text1);
            $vals = $_SESSION["vals"];
            // p_($vals); 
            $index = make_index($vals);
            // include("file_to_array.php");
            $vals=vals_out($vals);
            // $index = make_index($vals);
            ksort($vals);
            // include("array_to_file.php");
            // get_vals_index($main_text);
            // $vals = $_SESSION["vals"];
            // $vals=vals_out($vals);
            // $index = make_index($vals);
            // ksort($vals);
            $main_text=vals_index_to_project($vals);
            //print_r_($vals);
            $main_text1 = $main_text;
            //unset($vals);
            //echo htmlspecialchars($main_text1);
            //  p_($vals);exit;
            create_data($main_text1);
             
        } else {
            // $_SESSION["place"] = $_POST["user_place"];
            // $_SESSION["user_place"] = $_POST["user_place"];
            // $_SESSION["user_type_edge"] = $_SESSION["user_type_edge"];
            $main_text1 = $_POST["project_data"];
            create_data($main_text1);
           
        }
        $sheet = array();
        $band = array();
        create_data_step2($sheet, $band);
        $sheet_not_one = array();
        $band_not_one = array();
        if (count($sheet) > 0) {
            $sheet_one = db_get_arr_one_in($sheet, 'MATERIAL');
            $sheet_not_one = get_arr_invert($sheet, $sheet_one);
        }
        if (count($band) > 0) {
            $band_one = db_get_arr_one_in($band, 'BAND');
            $band_not_one = get_arr_invert($band, $band_one);
        }
        $_POST['form_step'] = 0;
        if (count($sheet_not_one) == 0 && count($band_not_one) == 0) $_POST['form_step'] = 1;
    }
    ?>
    <?php


    if ($_POST['form_step'] == 0 && (count($sheet_not_one) > 0 || count($band_not_one) > 0)) {
        
        
        $vals = $_SESSION['vals'];
        $index = $_SESSION['index'];
        // p_($vals);
        if (count($sheet_not_one) > 0) {
            ?>
            <div class="row">
                <div class="col-10">не найден листовой материал в БД, для продолжения работы необходимо выбрать
                    доступный материал из БД
                </div>

            </div>
            <?php

            foreach ($sheet_not_one as $key => $item) {
                ?>
                <div class="row" style="border: 2px dotted gray; margin: 20px;">
                    <div class="col-6">
                        <?php
                        echo $vals[$key]['attributes']['TYPENAME'] . ' (' . $vals[$key]['attributes']['UNIT'] . ')<br>' . $vals[$key]['attributes']['NAME'];
                        echo '<br>Детали:<br>';
                        echo get_parts($vals, $index, $vals[$key]['attributes']['ID']);
                        ?>
                    </div>
                    <div class="col-6">
                        <div id="slectedcode-<?= $key ?>"></div>
                        <input type="hidden" name="slectedid-<?= $key ?>" class="form-control"
                               id="slectedid-<?= $key ?>">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-typeop="1"
                                data-target="#selectPart"
                                data-targetid="<?= $key ?>">Выбрать замену листового материала
                        </button>
                    </div>

                </div>
                <?php
            }
        }
        if (count($band_not_one) > 0) {
            ?>
            <div class="row">
                <div class="col-10">не найдена кромка в БД, для продолжения работы необходимо выбрать кромку из БД</div>

            </div>
            <?php

            foreach ($band_not_one as $key => $item) {
                ?>
                <div class="row" style="border: 2px dotted gray; margin: 20px;">
                    <div class="col-6">
                        <?php
                        echo $vals[$key]['attributes']['TYPENAME'] . ' (' . $vals[$key]['attributes']['UNIT'] . ')<br>' . $vals[$key]['attributes']['NAME'];
                        echo '<br>Детали:<br>';
                        echo get_parts($vals, $index, $vals[$key]['attributes']['ID']);
                        ?>
                    </div>
                    <div class="col-6">
                        <div id="slectedcode-<?= $key ?>"></div>
                        <input type="hidden" name="slectedid-<?= $key ?>" class="form-control"
                               id="slectedid-<?= $key ?>">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-typeop="2"
                                data-target="#selectPart"
                                data-targetid="<?= $key ?>">Выбрать замену кромки
                        </button>
                    </div>

                </div>
                <?php
            }
        }
        echo '<div class="row"><input type="hidden" name="form_step" value="1">';
        echo '<div class="col-3 text-center mb-5">   <button  type="submit" class="btn btn-success" disabled name="op" value="1">Внести данные в проект</button></div>';
        echo '<div class="col-3 text-center mb-5">   <a class="btn btn-success" href="'.$main_dir.'/check_sheet_parts.php?data_id='.$_GET["data_id"].'">Счёт</a></div>';
//        echo '<div class="col-3 text-center mb-5">   <button class="btn btn-success" id="send_pdf_button">PDF</button></div>';
        echo '<div class="col-3 text-center mb-5"><button class="btn btn-success" id="full_project_pdf">Полный отчет PDF</button></div>';
        echo '<div class="col-3 text-center mb-5"><a href="'.$main_dir.'/index.php?go_back=1" class="btn btn-danger">Вернуться к редактированию деталей</a></div>';
    }
    elseif ((isset($_POST) && isset($_POST['form_step']) && (int)$_POST['form_step'] == 1)OR(isset($_SESSION['vals']))) {
        
        $vals = $_SESSION['vals'];
        $index = $_SESSION['index'];
        $vals=add_tag_print($vals);
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 10) == 'slectedid-') {
                $_key = str_replace('slectedid-', '', $key);
                $vals[$_key]['attributes']['CODE'] = $value;
            }
        }

        $_SESSION['vals'] = $vals;
//        echo '<pre>'; print_r($vals);                echo '</pre>';
        echo '<input type="hidden" name="form_step" value="2">';
        $_groupid = 0;
        $arr_parts = array();
        $materials = '';
        get_arr_parts($vals, $index, $arr_parts, $materials);
        // test('m',$materials);
        $band_for_select = $_SESSION['band_for_select'];
        $_SESSION['arr_parts'] = $arr_parts;
        $type_double = get_list_type_double();
        echo <<<rr
                <div class="row">
                <div class="col-12">
                    <h3>Торцовка материала:</h3>
                    <div style="color: red"><i>ВНИМАНИЕ! При изменении торцовки (норма 8мм/8мм - плитный материал, 0мм/16мм - столешницы) рекламации по сколам по краям не принимаются!</i></div>
                    <table border="2" cellpadding="40" cellspacing="20" class="table">
                        <thead>
                        <tr>
                            <th scope="col">Материал</th>
                            <th scope="col">Толщина</th>
                            <th scope="col">Ширина</th>
                            <th scope="col">Длина</th>
                            <th scope="col">Ширина</th>
                            <th scope="col">Длина</th>
                            <th scope="col">Кратность</th>
                            <th scope="col">Не менять раскрой</th>
                        </thead>
                        <tbody>
                rr;
                
           
//    p_($index);
        foreach ($index['OPERATION'] as $rt)
        { 
            if(($vals[$rt]['attributes']['TYPEID']=="CS")&&($vals[$rt]['type']=="open"))
            {
                $mat=part_get_material($vals,$vals[$rt+2]['attributes']['ID']);
                if (isset($mat['attributes']['MY_DOUBLE_ID'])) $readonly='readonly';else $readonly='';
                if (isset($mat['attributes']['MY_DOUBLE_ID'])) $text='Торцовки на сшивках не меняются. ';else $text='';
                // p_($mat);exit;
                if (!isset($mat['attributes']['CODE']))$mat['attributes']['CODE']=$mat['attributes']['MY_FIRST']."/".$mat['attributes']['MY_SECOND'];
                if ($mat['attributes']['ST']>0)
                {
                    $triml=16;
                    $trimw=16;
                }
                else
                {
                    $triml=8;
                    $trimw=8;
                }
                unset($sheet_sel,$half_sel,$sheet_sel_h);
                // echo $triml,$trimw;
                if (($mat['attributes']['CODE']>0)&&(is_numeric($mat['attributes']['CODE']))&&($_SESSION['user_place']>0))
                {
                    $sql='SELECT * FROM MATERIAL where CODE='.$mat['attributes']['CODE'].';';
                    // echo $sql_e;
                    $m_id=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['MATERIAL_ID'];
                    if ($m_id>0)
                    {
                        $sql='SELECT * FROM MATERIAL_HALF where place_id='.$_SESSION['user_place'].' AND material_id='.$m_id.';';
                        // echo $sql_e;
                        $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                        if (count($sql2)>0)
                        {
                            $mat['attributes']['NAME'] .=' <b><i>(материал продаётся кратно поллиста по короткой стороне) </b></i>';
                            // p_($vals[$rt]);
                            if ($vals[$rt]['attributes']['operation.my_krat_sheet']=="sheet")
                            {
                                $sheet_sel_h='selected';
                            }
                            else 
                            {
                                $half_sel='selected';
                            }
                        } 
                        else
                        {
                            $mat['attributes']['NAME'] .=' <b><i>(материал продаётся кратно листу) </b></i>';
                            $sheet_sel='selected';
                        }
                           
                    }
                }
                if ($vals[$rt]['attributes']['operation.my_fix_card']==1) $check3='checked';
                else $check3='';
                // p_($vals[$rt]);
                echo "<tr>
                <td>[/".$mat['attributes']['CODE']."/] ".$mat['attributes']['NAME']."</td>
                <td>".$mat['attributes']['T']."</td>
                <td>".round($mat['attributes']['W'],2)."</td>
                <td>".round($mat['attributes']['L'],2)."</td>
                <td>
                    <input type=\"number\" step=\"1\" min=\"0\" max=\"".$triml."\" ".$readonly." name=\"triml_".$vals[$rt]['attributes']['ID']."\" value=\"".$vals[$rt]['attributes']['CTRIML']."\"  title=\"".$text."Торцовка по длине (верх, низ) от 0 до ".$triml." мм\"  class=\"form-control\" size=\"2\" >
                </td>
                <td>
                    <input type=\"number\" step=\"1\" min=\"0\" max=\"".$trimw."\" ".$readonly." name=\"trimw_".$vals[$rt]['attributes']['ID']."\" value=\"".$vals[$rt]['attributes']['CTRIMW']."\"  title=\"".$text."Торцовка по ширине (право, лево) от 0 до ".$trimw." мм\"  class=\"form-control\" size=\"2\">
                </td>
                <td>
                    <select id=\"cs_krat_".$vals[$rt]['attributes']['ID']."\" name=\"cs_krat_".$vals[$rt]['attributes']['ID']."\" ";
                    if (isset($sheet_sel)) echo 'disabled="true"';
                    echo ">
                    <option value=\"sheet\" ".$sheet_sel." ".$sheet_sel_h.">Лист</option>
                    <option value=\"half\" ".$half_sel.">Поллиста</option>
                    
                    </select>
                </td>
                <td>
                    <input type=\"checkbox\" ".$check3." name=\"fix_card_".$vals[$rt]['attributes']['ID']."\" value=\"1\"  title=\"Раскрой идёт в производство без изменений (ручная группировка деталей).\"  class=\"form-control\" size=\"1\">
                </td>
            </tr>";
            }
        }
        echo " </table>
        </div>
    </div>";

        $basisProject = is_null($basisProject) ? null : '<p class="bg-red yellow tac pt5 pb5">При удалении сшивок из проекта, импортированного из Базиса, сшивки удаляются полностью, вместе со всеми связанными деталями и обработками!</p>';

        $vals=el_type_lines($vals,$_SESSION['user_type_edge'],$_SESSION);
        foreach ($index['OPERATION'] as $vb)
        {
            if ($vals[$vb]['attributes']['TYPEID']=="EL") $vals[$vb]['attributes']['ELCOLOR']="rgb(".mt_rand(0,155).",".mt_rand(0,150).",".mt_rand(0,150).")";
        }
        foreach ($arr_parts as $key => $row) 
        {
            // echo '<pre>'; print_r($row);                echo '</pre>';
            $_tst='---';
            if(($row['MY_DOUBLE_RES']>0))
            {
                $_tst='-'.$row['MY_DOUBLE_RES'];
                if (!isset($shapka_t))
                {
                    echo 
                    '<div class="row">
                    <div class="col-12">
                    <h3>Сшивки:</h3>
                    ' . $basisProject . '
                        <table border="2" cellpadding="40" cellspacing="20" class="table">
                            <thead>
                            <tr>
                                <th scope="col">Название</th>
                                <th scope="col">Удалить</th>
                            </thead>';

                    $shapka_t=1;
                    if ($_groupid != $row['group_id']) {
                        echo '<tr><td colspan="6" align="center" style="font-weight: bold;"  >' . $row['group_name'] . '</td></tr>';
                        $_groupid = $row['group_id'];
                    }
                }
            } 
           
            if(($row['MY_DOUBLE_RES']>0) && ($row['MY_DOUBLE_RES']==1)) {
                if ($row["NAME"] == "") {
                    $NAME = " <i><b>деталь без названия</b></i>";
                } else $NAME = "<i><b class='part_name_container' data-img='".$row['ID']."'>" . $row["NAME"] . "</b></i>";
                $r12345 = part_get_material($vals, $row['ID']);
               
                // if ($r12345[])
                $NAME .= "<br><small> Количество: <b><i>" . $row['COUNT'] . "</i></b> шт.<br>";
                $NAME .= "<br>номер детали в изделии: " . part_place_in_product($vals, $row['ID']) . "<br>";
                $NAME .= "номер детали в в раскрое: " . part_place_in_cs($vals, $row['ID']) . "<br>";
                // p_($r12345);
                if (isset($r12345["attributes"]["CODE"]))
                {
                    $NAME .= "материал: " . $r12345["attributes"]["NAME"] . "(" . $r12345["attributes"]["CODE"] . ")</small><br>";
                }
                else $NAME .= "материал: " . $r12345["attributes"]["NAME"] . "</small><br>";

                echo '<tr>
            <td >' . $NAME . '</td><td><input type="checkbox"  name="out_del[]" value="' . $row['ID'] . '" >';
                echo '</td></tr>';
            }
        }
        ?>
                </table>
            </div>
        </div>
        


          <?php
          $sql='SELECT * FROM DOUBLE_TYPE';
          $sql2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
          foreach ($sql2 as $sql3)
          {
              $double_edge[]=$sql3;
            //   p_($sql3);
          }
        //   p_($double_edge);
          $rrrr=<<<rr
                    <div class="row">
                    <div class="col-12">
                    <h3>Детали:</h3>
                    <table border="2" cellpadding="40" cellspacing="20" class="table">
                    <thead>
                    <tr>
                        <th scope="col">Название</th> 
                        <th scope="col">Резы</th>
                        <th scope="col">Сшивка</th>
                    </tr>
                    </thead>
                    rr;
                    foreach ($arr_parts as $keee=>$ker)
                    {
                        if ($ker['MY_DOUBLE']<1) $terrr=3;
                    }
                    if ($terrr>0) echo $rrrr;
                    
                                //    echo '<pre>'; print_r($arr_parts);                echo '</pre>';
                    foreach ($arr_parts as $key => $row) {
                        if ($_groupid != $row['group_id']) {
                            echo '<tr><td colspan="6" align="center" style="font-weight: bold;"  >' . $row['group_name'] . '</td></tr>';
                            $_groupid = $row['group_id'];
                        }
                        // p_($row);

                        if(!isset($row['MY_DOUBLE_ID'])||($row['MY_DOUBLE_ID']=='NULL')) {
                            if ($row["NAME"] == "") {
                                $NAME = " <div><i><b>деталь без названия</b></i>";
                            } else $NAME = "<div><i><b>" . $row["NAME"] . "</b></i>";
                            
                            $r12345 = part_get_material($vals, $row['ID']);
                            $NAME .= "<br><small> Количество: <b><i>" . $row['COUNT'] . "</i></b> шт.<br>";
                            $NAME .= "<br>номер детали в изделии: " . part_place_in_product($vals, $row['ID']) . "<br>";
                            $NAME .= "номер детали в в раскрое: " . part_place_in_cs($vals, $row['ID']) . "<br>";
                            $NAME .= "материал: " . $r12345["attributes"]["NAME"] . "(" . $r12345["attributes"]["CODE"] . ")</small><br>";

                            
                            
                            $get_edges=part_get_edges($vals,$row['ID']);
                            if ($get_edges['T']['attributes']['MY_CHECK_KL']==1) $kl_t='КР';else unset ($kl_t);
                            if ($get_edges['B']['attributes']['MY_CHECK_KL']==1) $kl_b='КР';else unset ($kl_b);
                            if ($get_edges['L']['attributes']['MY_CHECK_KL']==1) $kl_l='КР';else unset ($kl_l);
                            if ($get_edges['R']['attributes']['MY_CHECK_KL']==1) $kl_r='КР';else unset ($kl_r);
                            //////////////////////
                            $kromki = [
                                't' => [
                                    'letter' => $get_edges['T']['attributes']['ELSYMBOL'],
                                    'name' => $get_edges['T']['attributes']['NAME'],
                                    'color' => $get_edges['T']['attributes']['ELCOLOR'],
                                    'type' => $kl_t
                                ],
                                'b' => [
                                    'letter' => $get_edges['B']['attributes']['ELSYMBOL'],
                                    'name' => $get_edges['B']['attributes']['NAME'],
                                    'color' => $get_edges['B']['attributes']['ELCOLOR'],
                                    'type' => $kl_b
                                ],
                                'l' => [
                                    'letter' => $get_edges['L']['attributes']['ELSYMBOL'],
                                    'name' => $get_edges['L']['attributes']['NAME'],
                                    'color' => $get_edges['L']['attributes']['ELCOLOR'],
                                    'type' => $kl_l
                                ],
                                'r' => [
                                    'letter' => $get_edges['R']['attributes']['ELSYMBOL'],
                                    'name' => $get_edges['R']['attributes']['NAME'],
                                    'color' => $get_edges['R']['attributes']['ELCOLOR'],
                                    'type' => $kl_r
                                ],
                                'id'=>$row['ID']

                            ];
                            $sizes = [
                                'horizontal' => 'Г '.round($row['DW'],2).' / П '.round($row['CW'],2),
                                'vertical' => 'Г '.round($row['DL'],2).' / П '.round($row['CL'],2),
                                't'=> $r12345['attributes']['T']
                            ];
                            // p_($kromki);

                            // p_($sizes);
                            unset ($files);
                            $files = [];
                            $xnc=part_get_xnc($vals,$row['ID']);
                            // p_($xnc);
                            unset ($gr_in);
                            if (isset($xnc))
                            {
                                foreach ($xnc as $xx=>$x)
                                {
                                    if ($x['attributes']['SIDE']=='true') $side="(лицевая сторона)";
                                    else $side="(обратная сторона)";
                                    $files[]=array('side'=>$side,'files'=>$row['ID'].'_'.$x['attributes']['ID']);
                                    $gr_in=substr_count($xnc[$xx]['attributes']['PROGRAM'],'<gr ');
                                //     echo htmlspecialchars($xnc[0]['attributes']['PROGRAM']);
                                //    p_($gr_in);

                                }
                            }
                        //    p_($gr_in);
                            // $files = [
                            //     '6_3'
                            // ];
                            ////////////////////////
// p_($files);




                            echo '<tr><td class="detail_name_container"><div>' . $NAME ;
                            echo '<div>
                            <p><b>ID:</b> '.$row['ID'].'</p>
                            <p><b>Толщина:</b> '.round($row['T'], 1).'</p>
                            <p><b title="DL - габаритная длина детали с учётом кромки, JL - длина заготовки после прифуговки (т.е. длина перед кромкованием)">Длина (DL/JL):</b> '.round($row['DL'], 2) . '/'.round($row['JL'], 2).'.</p>
                            <p><b title="DW - габаритная ширина детали с учётом кромки, JW - ширина заготовки после прифуговки (т.е. ширина перед кромкованием)">Ширина (DW/JW:</b> '.round($row['DW'], 2) . '/'.round($row['JW'], 2).'</p>
                            </div></div>';
                            echo '<hr>'.get_detail_html_data($kromki, $sizes, $files, $row['ID'],round($row['JL'], 2),round($row['JW'], 2)).'</td>';

                            


                            echo '</td><td style="width: 221px;">';
                           
                            echo '<table><tr><td class="zarez_ugla_container">';
//                            echo '<pre>'; print_r($row);                echo '</pre>';
// p_($row);

                            if (((!isset($rty["L"]))&&(!isset($row['MY_XNC_SIDE_L'])))
                             OR ((!isset($rty["R"]))&&(!isset($row['MY_XNC_SIDE_R'])))
                             OR ((!isset($rty["T"])) &&(!isset($row['MY_XNC_SIDE_T'])))
                             OR ((!isset($rty["B"])) &&(!isset($row['MY_XNC_SIDE_L'])))
                             &&($r12345["attributes"]["T"]>8)) {
                                $_checked='';
                                $_displaystyle='display: none;';
                                if(isset($row['MY_CUT_ANGLE_T']) || isset($row['MY_CUT_ANGLE_L']) ||
                                    isset($row['MY_CUT_ANGLE_R']) || isset($row['MY_CUT_ANGLE_B']))
                                {
                                    $_displaystyle='';
                                    $_checked=' checked ';
                                }
                                echo '
                                <input ' . $_checked . ' type="checkbox" name="cut_angle_' . $row['ID'] . '" value="1">
                                <label title="Если в проекте есть прямое кромкование по стороне детали, по этой стороне детали срез торца недоступен. По одной стороне возмножно либо кромкование, либо срез торца. Срез приоритетнее кромкования.">Срез торца под угол.</label>
                                <i class="fas fa-info-circle"  data-toggle="modal" data-target="#extra_info_dialog" data-text="4"></i>
                                
        <div class="my-cut-angle-box angle-box-' . $row['ID'] . '"  style="' . $_displaystyle. '">
        <table cellpadding=0 border=0 cellspacing=0 align="center">
        ';

                                foreach (SIDE as $s)
                                {
                                    echo '<tr><td>';

                                    if ((!$rty[mb_strtoupper($s)])&&($row['MY_XNC_SIDE_'.mb_strtoupper($s)]<1)) 
                                    {
                                        echo SIDE_ST[$s].'(гр.) сдвиг(мм.)</td><td>';
                                        echo '    <input type="number" step="1" min="-45" max="45" name="my_cut_angle_' . $row['ID'] . '_'.$s.'" value="'.(isset($row['MY_CUT_ANGLE_'.mb_strtoupper($s)])?$row['MY_CUT_ANGLE_'.mb_strtoupper($s)]:0).'"  title="срез по краю ('.SIDE_T[$s].')"  class="my-cut-angle-'.$s.'  form-control" size="2" >';
                                        echo '    <input type="number" step="1" min="0" max="5" name="my_cut_angle_sdvig_' . $row['ID'] . '_'.$s.'" value="'.(isset($row['MY_CUT_ANGLE_SDVIG_'.mb_strtoupper($s)])?$row['MY_CUT_ANGLE_SDVIG_'.mb_strtoupper($s)]:0).'"  title="Сдвиг среза по краю ('.SIDE_T[$s].')"  class="my-cut-angle-'.$s.'  form-control" size="2" >';

                                    }
                                    elseif (isset($rty[mb_strtoupper($s)])) echo "<input type=\"number\"  value=0 title=\"Кромкование ".SIDE_T[$s].", срез недоступен.\"  class=\"my-cut-angle-".$s."  form-control\" size=\"2\" readonly=\"readonly\">";
                                    elseif ($row['MY_XNC_SIDE_'.mb_strtoupper($s)]==1) echo "<input type=\"number\" value=0  title=\"По ".SIDE_T[$s]." стороне обработка ЧПУ, срез недоступен.\"  class=\"my-cut-angle-".$s."  form-control\" size=\"2\" readonly=\"readonly\">";
                                    echo "</td></tr>";
                                    
                                }
                                // if ((!$rty["L"])&&($row['MY_XNC_SIDE_L']<1)) echo '    <input type="number" step="1" min="-45" max="45" name="my_cut_angle_' . $row['ID'] . '_l" value="'.(isset($row['MY_CUT_ANGLE_L'])?$row['MY_CUT_ANGLE_L']:0).'"  title="срез по краю (лево)"  class="my-cut-angle-l  form-control" size="2" >';
                                // elseif (isset($rty["L"])) echo "<input type=\"number\" value=0  title=\"Кромкование левое, срез недоступен.\"  class=\"my-cut-angle-t  form-control\" size=\"2\" readonly=\"readonly\">";
                                // elseif ($row['MY_XNC_SIDE_L']==1) echo "<input type=\"number\" value=0  title=\"По левой стороне обработка ЧПУ, срез недоступен.\"  class=\"my-cut-angle-t  form-control\" size=\"2\" readonly=\"readonly\">";
                                // echo "</td><td align=\"center\">";
                                // if ((!$rty["R"])&&($row['MY_XNC_SIDE_R']<1)) echo '    <input type="number" step="1" min="-45" max="45" name="my_cut_angle_' . $row['ID'] . '_r" value="'.(isset($row['MY_CUT_ANGLE_R'])?$row['MY_CUT_ANGLE_R']:0).'"  title="срез по краю (право)"  class="my-cut-angle-r  form-control" size="2">';
                                // elseif (isset($rty["R"])) echo "<input type=\"number\" value=0  title=\"Кромкование правое, срез недоступен.\"  class=\"my-cut-angle-t  form-control\" size=\"2\" readonly=\"readonly\">";
                                // elseif ($row['MY_XNC_SIDE_R']==1) echo "<input type=\"number\" value=0  title=\"По правой стороне обработка ЧПУ, срез недоступен.\"  class=\"my-cut-angle-t  form-control\" size=\"2\" readonly=\"readonly\">";
                                // echo "</td></tr><tr><td align=\"center\" colspan=\"2\">";
                                // if ((!$rty["B"])&&($row['MY_XNC_SIDE_B']<1)) echo '    <input type="number" step="1" min="-45" max="45" name="my_cut_angle_' . $row['ID'] . '_b" value="'.(isset($row['MY_CUT_ANGLE_B'])?$row['MY_CUT_ANGLE_B']:0).'"  title="срез по краю (низ)"  class="my-cut-angle-b  form-control" size="2">
                                // ';
                                // elseif (isset($rty["B"])) echo "<input type=\"number\" value=0  title=\"Кромкование нижнее, срез недоступен.\"  class=\"my-cut-angle-t  form-control\" size=\"2\" readonly=\"readonly\">";
                                // elseif ($row['MY_XNC_SIDE_B']==1) echo "<input type=\"number\" value=0  title=\"По нижней стороне обработка ЧПУ, срез недоступен.\"  class=\"my-cut-angle-t  form-control\" size=\"2\" readonly=\"readonly\">";
                                echo "</table></div>";
                                if ($gr_in>0)
                                {
                                    unset ($paz_t_o,$paz_t_z);
                                    echo '<hr>';
                                    if ($row['part.my_gr_type']=='false') $paz_t_o='checked';
                                    else $paz_t_z='checked';
                                    echo '
                                        <div>
                                        <label>Пазы на детали: <i class="fas fa-info-circle" data-toggle="modal" data-target="#extra_info_dialog" data-text="11"></i></label><br>
                                        <label>
                                            <input '.$paz_t_o.' type="radio" value="false" name="paz_'. $row['ID'].'">
                                            <span> Открытый </span>
                                        </label>
                                        <label>
                                            <input '.$paz_t_z.' type="radio" value="true" name="paz_'. $row['ID'].'">
                                            <span> Закрытый </span>
                                        </label>
                                        </div>
                                    ';
                                }
                                echo "<script>
                                    $('input[name=\"my_cut_angle_".$row['ID']."_t\"]').on('change', function(e) {
                                       if( $(e.target).val() != 0) {
                                       
                                           $('select[name=\"stitching_band_t_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_t_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                           $('select[name=\"stitching_band_t_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    });
                                    $('input[name=\"my_cut_angle_".$row['ID']."_l\"]').on('change', function(e) {
                                       if( $(e.target).val() != 0) {
                                           $('select[name=\"stitching_band_l_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_l_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                           $('select[name=\"stitching_band_l_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    });
                                    $('input[name=\"my_cut_angle_".$row['ID']."_r\"]').on('change', function(e) {
                                       if( $(e.target).val() != 0) {
                                           $('select[name=\"stitching_band_r_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_r_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                           $('select[name=\"stitching_band_r_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    });
                                    $('input[name=\"my_cut_angle_".$row['ID']."_b\"]').on('change', function(e) {
                                       if( $(e.target).val() != 0) {
                                           $('select[name=\"stitching_band_b_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_b_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                           $('select[name=\"stitching_band_b_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    });
                                    $(document).ready(function() {
                                        if( $('input[name=\"my_cut_angle_".$row['ID']."_t\"]').val() != 0) {
                                       
                                           $('select[name=\"stitching_band_t_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_t_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                            
                                           $('select[name=\"stitching_band_t_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    if( $('input[name=\"my_cut_angle_".$row['ID']."_l\"]').val() != 0) {
                                       
                                           $('select[name=\"stitching_band_l_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_l_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                            
                                           $('select[name=\"stitching_band_l_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    if( $('input[name=\"my_cut_angle_".$row['ID']."_r\"]').val() != 0) {
                                       
                                           $('select[name=\"stitching_band_r_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_r_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                            
                                           $('select[name=\"stitching_band_r_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    if( $('input[name=\"my_cut_angle_".$row['ID']."_b\"]').val() != 0) {
                                       
                                           $('select[name=\"stitching_band_b_".$row['ID']."\"]').val(0);
                                           $('select[name=\"stitching_band_b_".$row['ID']."\"]').attr('disabled', 'disabled');
     
                                       } else {
                                            
                                           $('select[name=\"stitching_band_b_".$row['ID']."\"]').removeAttr('disabled');
                                       }
                                    });
                                    

                                </script>";
                            }
                            echo '</td></tr>';
                            echo '<tr><td>';

                            if($row['part.my_side_to_cut_t']+$row['part.my_side_to_cut_l']+$row['part.my_side_to_cut_b']+$row['part.my_side_to_cut_r']>0)
                            {
                                $side_cut_style='';
                                $side_cut_checked = 'checked';
                            } 
                            else {
                                $side_cut_style = 'style="display:none;"';
                                $side_cut_checked = '';
                            }
                            // p_($row);
                            if(!isset($row['part.my_side_to_cut_t']))  $readonly['t'] ='readonly';else $readonly['t']='readonly';
                            if(!isset($row['part.my_side_to_cut_b']))  $readonly['b'] ='readonly';else $readonly['b']='readonly';
                            if(!isset($row['part.my_side_to_cut_r']))  $readonly['r'] ='readonly';else $readonly['r']='readonly';
                            if(!isset($row['part.my_side_to_cut_l']))  $readonly['l'] ='readonly';else $readonly['l']='readonly';
                            if($row['part.my_side_to_cut_t']+$row['part.my_side_to_cut_l']+$row['part.my_side_to_cut_b']+$row['part.my_side_to_cut_r']>0)
                            {
                            //    p_($row);
                                echo '

                                    <input type="checkbox" name="cut_side_' . $row['ID'] . '" value="1" '.$side_cut_checked.'>
                                    <label title="Деталь для обработки на сверлильном станке должна быть не менее размера 200*70 мм, для обработки на фрезере не менее 350*100мм. Если деталь меньше и есть возможность добавить по необрабатываемой ЧПУ стороне часть площади, деталь делается больше, обрабатывается ЧПУ, после чего урезается в размер, указанный Вами.">Урезка детали</label>
                                    <div class="my-cut-side-box side-box-' . $row['ID'] . '" '.$side_cut_style.'>
                                    <table cellpadding=0 border=0 cellspacing=0 align="center"><tr align="centr" width="100%">
                                    <td align="center" colspan="2">
                                ';
                                echo '    <input type="text" align ="center" readonly step="1"  name="my_cut_side_' . $row['id'] . '_t" value="'.$row['part.my_side_to_cut_d_t'].'/'.$row['part.my_side_to_cut_t'].'"  title="урезать до (верх)"  size="5" >';
                                echo "</td></tr><tr><td align=\"center\">";
                                echo '    <input type="text" readonly step="1"  align ="center" name="my_cut_side_' . $row['id'] . '_l" value="'.$row['part.my_side_to_cut_d_l'].'/'.$row['part.my_side_to_cut_l'].'"  title="урезать до (лево)"   size="5" >';
                                echo "</td><td align=\"center\">";
                                echo '    <input type="text" readonly step="1" align ="center" name="my_cut_side_' . $row['id'] . '_r" value="'.$row['part.my_side_to_cut_d_r'].'/'.$row['part.my_side_to_cut_r'].'"  title="урезать до (право)" size="5">';
                                echo "</td></tr><tr><td align=\"center\" colspan=\"2\">";
                                echo '    <input type="text" readonly step="1" align ="center" name="my_cut_side_' . $row['id'] . '_b" value="'.$row['part.my_side_to_cut_d_b'].'/'.$row['part.my_side_to_cut_b'].'"  title="урезать до (низ)"  size="5">';
                                echo "</td></tr></table></div>";
                            }

                        echo '</td></tr>';
                        echo '<tr><td>';




    //                         if ($row['XNC'] > 0 && $row['XNC_COUNTMILL'] > 0) {

    //                             if ((!$rty["L"]) OR (!$rty["R"]) OR (!$rty["T"]) OR (!$rty["B"])) {


    //                                 $_band_for_select = '<option value="0" data-w="0">Не установлено</option>' . $band_for_select . '<option value="DB">Из БД</option>';
    //                                 $_sides_band_for_select=array(
    //                                     'T'=>$_band_for_select,
    //                                     'L'=>$_band_for_select,
    //                                     'R'=>$_band_for_select,
    //                                     'B'=>$_band_for_select
    //                                 );
    //                                 $_xnc_band_style='display: none;';
    //                                 $_xnc_btnDB_style=array();
    //                                 $_xnc_selected=array();
    //                                 $_xnc_hidden_selected=array();
    //                                 $_xnc_countmill_checked='';

    //                                 $_arr_i1=array('T','L','R','B');
    //                                 foreach ($_arr_i1 as $v_i1)
    //                                 {
    //                                     $_xnc_btnDB_style[$v_i1]='display: none;';
    //                                     $_xnc_selected[$v_i1]='';
    //                                     $_xnc_hidden_selected[$v_i1]='';
    //                                     if (!$rty[$v_i1] && isset($row['MY_KL_EL'.$v_i1.'_CODE']))
    //                                     {
    //                                         if(substr_count($_band_for_select, 'value="'.$row['MY_KL_EL'.$v_i1.'_CODE'].'"')>0)
    //                                         {
    //                                             $_sides_band_for_select[$v_i1]=str_replace('value="'.$row['MY_KL_EL'.$v_i1.'_CODE'].'"', '  selected="selected" value="'.$row['MY_KL_EL'.$v_i1.'_CODE'].'"',$_band_for_select);
    //                                             $_xnc_countmill_checked='checked';
    //                                             $_xnc_band_style='';
    //                                         }
    //                                         else
    //                                         {
    //                                             $_sides_band_for_select[$v_i1]=str_replace('value="DB"', '  selected="selected" value="DB"',$_band_for_select);
    //                                             $_xnc_countmill_checked='checked';
    //                                             $_xnc_btnDB_style[$v_i1]='';
    //                                             $_xnc_selected[$v_i1]=$row['MY_KL_EL'.$v_i1.'_NAME'];
    //                                             $_xnc_hidden_selected[$v_i1]=' value="'.$row['MY_KL_EL'.$v_i1.'_CODE'].'" ';
    //                                             $_xnc_band_style='';

    //                                         }
    //                                     }
    //                                 }


    //                                 echo '<div style="float: left;"><input type="checkbox" '.$_xnc_countmill_checked.' name="xnc_countmill_' . $row['ID'] . '" value="1" >Кромка';
    //                                 echo '<span style="'.$_xnc_band_style.'" class="xnc_group_' . $row['ID'] . '"><br>
                                                               
    //                             ';
    //                             }
    //                             if (!$rty["T"]) echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_t" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_t>верх</label>';
    //                             if (!$rty["B"]) echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_b" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_b>низ</label>';
    //                             if (!$rty["R"]) echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_r" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_r>право</label>';
    //                             if (!$rty["L"]) echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_l" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_l>лево</label>';
                               
    //                             echo '</span>';
    //                             if ((!$rty["L"]) OR (!$rty["R"]) OR (!$rty["T"]) OR (!$rty["B"])) {
    //                                 // MY_KL_ELT_CODE

    //                                 echo '<br><input type="hidden" name="xnc_id_' . $row['ID'] . '" id="xnc_id_' . $row['ID'] . '" value="' . $row['XNC'] . '">';
    //                         //        echo '            <label style="" class="xnc_band ' . $row['ID'] . '">Кромка</label><br>';

    //                                 echo '       ';
    //                                 if (!$rty["T"]) echo '
    // <span style="'.$_xnc_band_style.'" class="xnc_bands ' . $row['ID'] . '">                 <label for="xnc_band_' . $row['ID'] . '_t">Верх</label>
    // <select class="form-control" name="xnc_band_' . $row['ID'] . '_t" data-add1="' . $row['ID'] . '" data-side="t" id="xnc_band_' . $row['ID'] . '_t">' . $_sides_band_for_select['T'] . '</select>
    // <input type="hidden" name="slectedxncid-t-' . $row['ID'] . '" class="form-control" id="slectedxncid-t-' . $row['ID'] . '" ' . $_xnc_hidden_selected['T'] . '>
    // <button style="' . $_xnc_btnDB_style['T'] . '" type="button" class="btn btn-primary" id="xnc_band_t_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
    //                  data-t="' . $row['T'] . '" data-target="#selectPart"
    //                         data-targetid="0" data-targetxncid="-t-' . $row['ID'] . '">Выбрать замену</button><div id="slectedxnccode-t-' . $row['ID'] . '">'.$_xnc_selected['T'].'</div>
    
    // </span><br>';
    //                                 if (!$rty["B"]) echo '
    // <span style="'.$_xnc_band_style.'" class="xnc_bands ' . $row['ID'] . '">                    <label for="xnc_band_' . $row['ID'] . '_b">Низ</label>
    // <select class="form-control" name="xnc_band_' . $row['ID'] . '_b"data-add1="' . $row['ID'] . '" data-side="b"  id="xnc_band_' . $row['ID'] . '_b">' . $_sides_band_for_select['B'] . '</select>
    // <input type="hidden" name="slectedxncid-b-' . $row['ID'] . '" class="form-control" id="slectedxncid-b-' . $row['ID'] . '" ' . $_xnc_hidden_selected['B'] . '>
    // <button style="' . $_xnc_btnDB_style['B'] . '" type="button" class="btn btn-primary" id="xnc_band_b_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
    //                  data-b="' . $row['T'] . '" data-target="#selectPart"
    //                         data-targetid="0" data-targetxncid="-b-' . $row['ID'] . '">Выбрать замену</button><div id="slectedxnccode-b-' . $row['ID'] . '">'.$_xnc_selected['B'].'</div>
    // </span>';
    //                                 if (!$rty["L"]) echo ' <span style="'.$_xnc_band_style.'" class="xnc_bands ' . $row['ID'] . '"><label for="xnc_band_' . $row['ID'] . '_l">Лево</label>
    // <select class="form-control" name="xnc_band_' . $row['ID'] . '_l" data-add1="' . $row['ID'] . '" data-side="l" id="xnc_band_' . $row['ID'] . '_l">' . $_sides_band_for_select['L'] . '</select>
    // <input type="hidden" name="slectedxncid-l-' . $row['ID'] . '" class="form-control" id="slectedxncid-l-' . $row['ID'] . '" ' . $_xnc_hidden_selected['L'] . '>
    // <button style="' . $_xnc_btnDB_style['L'] . '" type="button" class="btn btn-primary" id="xnc_band_l_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
    //                  data-l="' . $row['T'] . '" data-target="#selectPart"
    //                         data-targetid="0" data-targetxncid="-l-' . $row['ID'] . '">Выбрать замену</button><div id="slectedxnccode-l-' . $row['ID'] . '">'.$_xnc_selected['L'].'</div>
    // </span><br>';
    //                                 if (!$rty["R"]) echo '
    // <span style="'.$_xnc_band_style.'" class="xnc_bands ' . $row['ID'] . '">              <label for="xnc_band_' . $row['ID'] . '_r">Право</label>
    // <select class="form-control" name="xnc_band_' . $row['ID'] . '_r" data-add1="' . $row['ID'] . '" data-side="r" id="xnc_band_' . $row['ID'] . '_r">' . $_sides_band_for_select['R'] . '</select>
    // <input type="hidden" name="slectedxncid-r-' . $row['ID'] . '" class="form-control" id="slectedxncid-r-' . $row['ID'] . '" ' . $_xnc_hidden_selected['R'] . '>
    // <button style="' . $_xnc_btnDB_style['R'] . '" type="button" class="btn btn-primary" id="xnc_band_r_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
    //                  data-r="' . $row['T'] . '" data-target="#selectPart"
    //                         data-targetid="0" data-targetxncid="-r-' . $row['ID'] . '">Выбрать замену</button><div id="slectedxnccode-r-' . $row['ID'] . '">'.$_xnc_selected['R'].'</div>
    // </span><br>';
                                    
    //                                 echo '';
    //                             }
    //                         }
                            echo '</td></tr>';
                            $e5=get_cs_tool__sheet_part($vals,$row["ID"]);
                            if ($r12345['attributes']['ST']>0)
                            {
                                $p = xml_parser_create();
                                xml_parse_into_struct($p, $e5['cs']['attributes']['DRAW'], $v, $v_index);
                                xml_parser_free($p);
                                foreach ($v_index["D"] as $v1)
                                {
                                    if (($v[$v1]['attributes']["Y"]>0)&&($v[$v1]['attributes']["ID"]==$row["ID"]))
                                    {
                                        $postf_no=1;
                                    }
                                }
                                // p_($row);
                                if ($row['part.my_postforming_cut_top'] > 0) $checked_pf = 'checked';
                                else $checked_pf = '';

                                //.. 17.08.2021 Добавлен срез закругления столешниц с двух сторон
                                if ($row['part.my_postforming_cut_bottom'] > 0) $double_checked_pf = 'checked';
                                else $double_checked_pf = '';
                                
                                ?>
                                <?php if (!isset($postf_no)): ?>
                                    <tr>
                                        <td class="srez_okryglenia_containers"><hr>
                                            <div id="one-side-cut">
                                                <input type="checkbox" name="pf_no_<?= $key ?>_<?= $row['ID'] ?>" value="1" <?= $checked_pf ?>>
                                                <label title="Для использования шаблонов обработок ЧПУ для плитного материала на детали столешницы с закруглением, необходимо предварительно срезать закругление перед обработкой на ЧПУ. Это уменьшит ширину детали W на 20 мм. Если будут установлены обработки ЧПУ для плитного материала, а позднее удалена отметка среза закругления, обработки ЧПУ по этой детали будут удалены из проекта. Для детали с обрезанным закругением недоступны шаблоны обработок ЧПУ для столешниц.">Срез закругления столешницы <i class="fas fa-info-circle"  data-toggle="modal" data-target="#extra_info_dialog" data-text="5"></i></label>
                                            </div>
                                            <?php if ($checked_pf == 'checked'): ?>
                                                <div id="cut-on-both-sides">
                                                    <input type="checkbox" name="double_pf_no_<?= $key ?>_<?= $row['ID'] ?>" value="1" <?= $double_checked_pf ?>>
                                                    <label title="Установите галку если нужно закругление с двух противоположных сторон.">С двух сторон<i class="fas fa-info-circle"  data-toggle="modal" data-target="#extra_info_dialog" data-text="12"></i></label>
                                                </div>    
                                            <?php endif ?>
                                            
                                            <div class="my-cut-side-box side-box-<?= $key ?>_<?= $row['ID'] ?>" <?= $side_cut_style ?>></div>
                                        </td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <td>
                                            <i><small>Деталь без закругления</i></small>
                                        </td>
                                    </tr>
                                <?php endif ?>
                                <?php
                                unset($postf_no);
                            } 
                            
                            echo '</table></td><td class="add_sshivka_container">';
                            // p_($row);
                            if ((int)$row['T'] <= 20 && ((int)$row['T'] >= 10) && ((!isset($row['MY_DOUBLE_ID']))||($row['MY_DOUBLE_ID']=="NULL")) &&($row['CL']>70)&&($row['CW']>70) ) {
                                echo '<input type="checkbox" name="stitching_' . $row['ID'] . '" value="1">Создать<br>';
// test ('кр',edge_get($vals,$r12345['attributes']['CODE'],$link,'yes'));

                                echo '    <label style="display: none;" class="stitching_band ' . $row['ID'] . '">Кромка</label>';
                                echo '    <span style="display: none;" class="stitching_bands ' . $row['ID'] . '"><label for="stitching_band_t_' . $row['ID'] . '">Верх</label>
                <select  class="form-control" name="stitching_band_t_' . $row['ID'] . '" id="stitching_band_t_' . $row['ID'] . '">
                    <option value="0" data-t="0" >Не установлено</option>' . edge_get($vals,$r12345['attributes']['CODE'],$link,'no') . '<option value="DB">Из БД</option></select>';
                                echo '<input type="hidden" name="slectedid-t-' . $row['ID'] . '" class="form-control" id="slectedid-t-' . $row['ID'] . '">';
                                echo '<button style="display: none;" type="button" class="btn btn-primary" id="btn_band_t_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
                     data-t="' . $row['T'] . '" data-target="#selectPart"
                            data-targetid="' . $row['ID'] . '">Выбрать замену <i>(кромкование детали сшивки)</i></button><div id="slectedcode-t-' . $row['ID'] . '"></div></span><br>';
                                echo '    <span style="display: none;" class="stitching_bands ' . $row['ID'] . '"><label for="stitching_band_b_' . $row['ID'] . '">Низ</label>
                <select  class="form-control" name="stitching_band_b_' . $row['ID'] . '" id="stitching_band_b_' . $row['ID'] . '">
                  <option value="0" data-t="0" >Не установлено</option>  ' . edge_get($vals,$r12345['attributes']['CODE'],$link,'no') . '<option value="DB">Из БД</option></select>';
                                echo '<input type="hidden" name="slectedid-b-' . $row['ID'] . '" class="form-control" id="slectedid-b-' . $row['ID'] . '">';
                                echo '<button style="display: none;" type="button" class="btn btn-primary" id="btn_band_b_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
                     data-t="' . $row['T'] . '" data-target="#selectPart"
                            data-targetid="' . $row['ID'] . '">Выбрать замену <i>(кромкование детали сшивки)</i></button><div id="slectedcode-b-' . $row['ID'] . '"></div></span><br>';
                                echo '    <span style="display: none;" class="stitching_bands ' . $row['ID'] . '"><label for="stitching_band_l_' . $row['ID'] . '">Лево</label>
                <select  class="form-control" name="stitching_band_l_' . $row['ID'] . '" id="stitching_band_l_' . $row['ID'] . '">
                   <option value="0" data-t="0" >Не установлено</option> ' . edge_get($vals,$r12345['attributes']['CODE'],$link,'no'). '<option value="DB">Из БД</option></select>';
                                echo '<input type="hidden" name="slectedid-l-' . $row['ID'] . '" class="form-control" id="slectedid-l-' . $row['ID'] . '">';
                                echo '<button style="display: none;" type="button" class="btn btn-primary" id="btn_band_l_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
                     data-t="' . $row['T'] . '" data-target="#selectPart"
                            data-targetid="' . $row['ID'] . '">Выбрать замену <i>(кромкование детали сшивки)</i></button><div id="slectedcode-l-' . $row['ID'] . '"></div></span><br>';
                                echo '    <span style="display: none;" class="stitching_bands ' . $row['ID'] . '"><label for="stitching_band_r_' . $row['ID'] . '">Право</label>
                <select  class="form-control" name="stitching_band_r_' . $row['ID'] . '" id="stitching_band_r_' . $row['ID'] . '">
                   <option value="0" data-t="0" >Не установлено</option>' .edge_get($vals,$r12345['attributes']['CODE'],$link,'no') . '<option value="DB">Из БД</option></select>';
                                echo '<input type="hidden" name="slectedid-r-' . $row['ID'] . '" class="form-control" id="slectedid-r-' . $row['ID'] . '">';
                                echo '<button style="display: none;" type="button" class="btn btn-primary" id="btn_band_r_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
                     data-t="' . $row['T'] . '" data-target="#selectPart"
                            data-targetid="' . $row['ID'] . '">Выбрать замену <i>(кромкование детали сшивки)</i></button><div id="slectedcode-r-' . $row['ID'] . '"></div></span><br>';
                                

                                echo '<label style="display: none;"  for="type_double_' . $row['ID'] . '">вид основания сшивки <i class="fas fa-info-circle" data-toggle="modal" data-target="#extra_info_dialog" data-text="9" data-trigger="type_double_' . $row['ID'] . '"></i></label>

        <select style="display: none;"  class="form-control" name="type_double_' . $row['ID'] . '" id="type_double_' . $row['ID'] . '">' . get_type_double($row['W'],$row['L'],$link). '</select>
        <div style="display: none;"  class="alert alert-warning" role="alert" id="type_double_descripts_' . $row['ID'] . '">

</div>
        <select style="display: none;" class="form-control" name="back_double_' . $row['ID'] . '" id="back_double_' . $row['ID'] . '" data-code="' . $row['CODE'] . '">' . get_material_double($vals, $r12345['attributes']['CODE']) . '</select>
        <label style="display: none;"  for="band_inner_' . $row['ID'] . '">кромкование внутренней части сшивки</label>
        <select class="form-control" style="display: none;"  name="band_inner_' . $row['ID'] . '"  id="band_inner_' . $row['ID'] . '">
        <option value="0" data-t="0" >Не установлено</option>
        ' . edge_get($vals,$r12345['attributes']['CODE'],$link,'no') . '<option value="DB">Из БД</option></select>
        <input type="hidden" name="slectedid-inner-' . $row['ID'] . '" class="form-control" id="slectedid-inner-' . $row['ID'] . '">
        <button style="display: none;" type="button" class="btn btn-primary" id="btn_inner_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
                     data-t="' . $row['T'] . '" data-target="#selectPart"
                            data-targetid="' . $row['ID'] . '">Выбрать замену</button>
                            <div id="slectedcode-inner-' . $row['ID'] . '"></div>
        ';
                            }
                            $double_type_script = "
                                <script>
                                    $(document).on('change', '#type_double_".$row['ID']."', function (e) {
                                        let active_option = $(e.target).find('option[value='+ $(e.target).val() +']');
                                        let ell = $(active_option).attr('data-ell');
                                        let elr = $(active_option).attr('data-elr');
                                        let elt = $(active_option).attr('data-elt');
                                        let elb = $(active_option).attr('data-elb');
                                        if(ell == 2) {
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_l_".$row['ID']."').removeAttr('disabled');
                                        } else if(ell == 1) {
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_l_".$row['ID']."').removeAttr('disabled');
                                        } else if(ell == 0) {
                                            $('#stitching_band_l_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_l_".$row['ID']."').val(0);
                                        }

                                        if(elr == 2) {
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_r_".$row['ID']."').removeAttr('disabled');
                                        } else if(elr == 1) {
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_r_".$row['ID']."').removeAttr('disabled');
                                        } else if(elr == 0) {
                                            $('#stitching_band_r_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_r_".$row['ID']."').val(0);
                                        }

                                        if(elt == 2) {
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_t_".$row['ID']."').removeAttr('disabled');
                                        } else if(elt == 1) {
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_t_".$row['ID']."').removeAttr('disabled');
                                        } else if(elt == 0) {
                                            $('#stitching_band_t_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_t_".$row['ID']."').val(0);
                                        }

                                        if(elb == 2) {
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_b_".$row['ID']."').removeAttr('disabled');
                                        } else if(elb == 1) {
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_b_".$row['ID']."').removeAttr('disabled');
                                        } else if(elb == 0) {
                                            $('#stitching_band_b_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_b_".$row['ID']."').val(0);
                                        }
                                    }); 
                                    $(document).ready(function() {
                                        let active_option_value = $('#type_double_".$row['ID']."').val();
                                      let active_option = $('#type_double_".$row['ID']."').find('option[value=\"' + active_option_value + '\"]');
                                        let ell = $(active_option).attr('data-ell');
                                        let elr = $(active_option).attr('data-elr');
                                        let elt = $(active_option).attr('data-elt');
                                        let elb = $(active_option).attr('data-elb');
                                        if(ell == 2) {
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_l_".$row['ID']."').removeAttr('disabled');
                                        } else if(ell == 1) {
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_l_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_l_".$row['ID']."').removeAttr('disabled');
                                        } else if(ell == 0) {
                                            $('#stitching_band_l_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_l_".$row['ID']."').val(0);
                                        }

                                        if(elr == 2) {
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_r_".$row['ID']."').removeAttr('disabled');
                                        } else if(elr == 1) {
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_r_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_r_".$row['ID']."').removeAttr('disabled');
                                        } else if(elr == 0) {
                                            $('#stitching_band_r_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_r_".$row['ID']."').val(0);
                                        }

                                        if(elt == 2) {
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_t_".$row['ID']."').removeAttr('disabled');
                                        } else if(elt == 1) {
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_t_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_t_".$row['ID']."').removeAttr('disabled');
                                        } else if(elt == 0) {
                                            $('#stitching_band_t_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_t_".$row['ID']."').val(0);
                                        }

                                        if(elb == 2) {
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"small\"]').hide();
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"big\"]').show();
                                            $('#stitching_band_b_".$row['ID']."').removeAttr('disabled');
                                        } else if(elb == 1) {
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"small\"]').show();
                                            $('#stitching_band_b_".$row['ID']."').find('option[data-type=\"big\"]').hide();
                                            $('#stitching_band_b_".$row['ID']."').removeAttr('disabled');
                                        } else if(elb == 0) {
                                            $('#stitching_band_b_".$row['ID']."').attr('disabled', '');
                                            $('#stitching_band_b_".$row['ID']."').val(0);
                                        }
                                    });
                                </script> 
                            ";
                            echo $double_type_script;
                            echo '</td></tr>';
                            //  echo "!!!".get_material_double($vals, $r12345['attributes']['CODE']);
                        }
                    }
                    ?>
                </table>

            </div>
        </div>
        <?php
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'index_form_fin', $session);

        put_temp_file_data($_GET["data_id"], $_SESSION);
        echo '<div class="row"><div class="col-3 text-center mb-5">   <button  type="submit" class="btn btn-success"  name="op" value="1">Внести данные в проект</button></div>';
        echo '<div class="col-3 text-center mb-5">   <a class="btn btn-success" href="'.$main_dir.'/check_sheet_parts.php?data_id='.$_GET["data_id"].'">Счёт</a></div>';
        echo '<div class="col-3 text-center mb-5"><button class="btn btn-success" id="full_project_pdf">Полный отчет PDF</button>';
//        echo '<div class="col-3 text-center mb-5">   <button class="btn btn-success" style="margin-right: 15px;" id="send_pdf_button">PDF</button>';
//        if(isset($_SESSION['manager_id']) && $_SESSION['manager_id'] != 2 && $_SESSION['manager_id'] != null) {
//            echo '<button class="btn btn-success" id="full_project_pdf">Полный отчет PDF</button>';
//        }
        echo '</div>';
        echo '<div class="col-3 text-center mb-5"><a href="'.$main_dir.'/index.php?go_back=1" class="btn btn-danger">Вернуться к редактированию деталей</a></div>';
    }
    
    ?>
</form>


<div class="modal fade" id="selectPart" tabindex="-1" role="dialog" aria-labelledby="selectPartLabel"
     aria-hidden="true">
    <div class="modal-dialog " role="document" style="min-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="selectPartLabel">Найдите замену</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="targetid" name="targetid" value="">
                <input type="hidden" id="targetidadd" name="targetidadd" value="">
                <input type="hidden" id="typeop" name="typeop" value="">
                <input type="hidden" id="t" name="t" value="">
                <input type="hidden" id="cut" name="cut" value="">
                <div class="modal-body-in">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
        $('#send_pdf_button').on('click', async function(e) {
            e.preventDefault();

            let formData = new FormData();
            ///formData.append('project_data', $('#saveContainer').val());
            formData.append('project_data', `<?php echo $_SESSION['project_data']; ?>`);
            formData.append('action', 'get_all_pdf' );

            let request = await fetch('/pdf_send.php', {
                method: 'POST',
                headers: {
                    ///'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: formData
            });
            let body = await request.text();
            if(body.indexOf('<?php echo $main_dir; ?>/users_pdf/') > -1){
                window.open(body);
            };
        });
        $('#full_project_pdf').on('click', async function(e) {

            e.preventDefault();
            $('#checks_lodaer_containers').show();
            let formData = new FormData();
            formData.append('project_data', `<?php echo $_SESSION['project_data']; ?>`);
            formData.append('action', 'get_all_pdf_manager' );

            let request = await fetch('<?php echo $main_dir; ?>/pdf_send.php', {
                method: 'POST',
                headers: {
                    ///'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: formData
            });
            let body = await request.text();
            if(body.indexOf('.pdf') > -1){
                $('#checks_lodaer_containers').hide();
                window.open('<?php echo $main_dir; ?>/pdf_send.php?show=' + body);
            }
        });
    })
    // $(document).ready(function () {
    //     let torec = $('input[name^="my_cut_angle_"]');
    //     $.each(torec, function (index, val) {
    //     });
    // });
    $(document).ready(function () {
        $('body .table').on('click', '.fa-info-circle', async function (e) {
            var type = $(e.target).attr('data-text');
            if(type == 9) {
                let more_data = $('#' + $(e.target).attr('data-trigger')).val();
                switch (Number(more_data)) {
                    case 3:
                        type = 1;
                        break;
                    case 5:
                        type = 3;
                        break;
                    case 4:
                        type = 2;
                        break;
                    case 2:
                        type = 7;
                        break;
                    case 1:
                        type = 6;
                        break;
                }
            }
            if(type == 9) {
                $('#extra_info_dialog h5').html('срез стороны под углом');
                $('#extra_info_dialog .modal-body').html(`
                    <div class="extra_modal_description">
                        Раздел в разработке
                    </div>
                `);
                $('#modal_checks_lodaer_containers').hide();
                return false;
            }
            let requs = await fetch('<?= $laravel_dir ?>/get_index_extra_info?type=' + type);
            let data = await requs.json();
            if(type == 4) {
                $('#extra_info_dialog h5').html('Срез торца под углом');
            }
            if(type == 5) {
                $('#extra_info_dialog h5').html('Срез закругления на столешнице');
            }
            if(type == 12) {
                $('#extra_info_dialog h5').html('Срез закругления на столешнице с двух сторон');
            }
            if(type == 1) {
                $('#extra_info_dialog h5').html('Срез стороны под углом');
            }
            if(type == 2) {
                $('#extra_info_dialog h5').html('Срез стороны под углом');
            }
            if(type == 3) {
                $('#extra_info_dialog h5').html('Срез стороны под углом');
            }
            if(type == 11) {
                $('#extra_info_dialog h5').html('Пазы');
            }
            if(type == 9) {
                setTimeout(function () {
                    $('#extra_info_dialog').modal('hide');
                }, 1500);
            }
            $('#extra_info_dialog .modal-body').html(`
                    <div class="extra_modal_description">
                        ${data.DESCRIPTION}
                    </div>
                    <div class="extra_modal_image">
                        <img src="<?= $main_dir ?>/pic_draft/${data.pic}">
                    </div>
                `);
            $('#modal_checks_lodaer_containers').hide();
            // if(type == 3)
            if(type == 9) {
                $('#extra_info_dialog h5').html('срез стороны под углом');
                $('#extra_info_dialog .modal-body').html(`
                    <div class="extra_modal_description">
                        Раздел в разработке
                    </div>
                `);
            }
        })
    });
</script>
 <div id="checks_lodaer_containers">
      <div>
          <img src="<?php echo $main_dir; ?>/download.gif" alt="">
      </div>
  </div>
</body>
</html>
