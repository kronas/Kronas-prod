<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");
header("Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization");
header("Access-Control-Allow-Headers: *");
date_default_timezone_set('Europe/Kiev');

session_start();

include_once ("func.php");

$all_orders = [];

$request_order = 'SELECT * FROM `PROJECT` WHERE `DATE` > "2020-06-19 00:00:00" ORDER BY `DATE` DESC';

if ($result = mysqli_query($link, $request_order)) {

    /* извлечение ассоциативного массива */
    while ($row = mysqli_fetch_assoc($result)) {
        $all_orders[] = $row;
    }

    /* удаление выборки */
    mysqli_free_result($result);
}

/* закрытие соединения */
//mysqli_close($link);

foreach($all_orders as $order) {
    if($order['ORDER1C'] != $_SESSION['order_id']) {
        $request_project = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$order['ORDER1C'];
        $_SESSION['order_id'] = $order['ORDER1C'];
        $result_new = mysqli_query($link,$request_project);
//        var_dump($result_new);
//        var_dump($link);
//        var_dump($request_project);
//        exit;
        $data_project = $result_new->fetch_assoc();
        $_SESSION['project_data'] =  base64_decode($order['MY_PROJECT_OUT']);
        $_SESSION['user_place'] = $data_project['PLACE'];
        $project = $order['PROJECT_ID'];
        $new_order_id = $data_project['DB_AC_ID'];
        $_SESSION['project_data'] = xml_parser($_SESSION['project_data']);
        $p = xml_parser_create();
        xml_parse_into_struct($p, $_SESSION['project_data'], $vals, $index);
        xml_parser_free($p);
        put_programm($vals,$_SESSION['user_place'],$link, $project, $new_order_id, $serv_main_dir,$_SESSION,str2url($order['DB_AC_NUM']));

        echo 'Программы успешно заказа '.$order['ORDER1C'].' обновлены!
    
        ';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://v-webapp01.dn-kronas.local/gibservice/doReady.asp?order=".$order['DB_AC_ID'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
//    CURLOPT_HTTPHEADER => array(
//        "Content-Type: text/xml; charset=UTF8",
//        "Cookie: ASPSESSIONIDCQBCCCBQ=PCGLMLMCMDLBNJIHJNBCIDON"
//    ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
//print_r_($info);
        curl_close($curl);

    }


}















/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<?php
session_start();
error_reporting(E_ERROR);
ini_set("display_errors", 1);

include_once("func.php");

function get_data($sql, $count = 0) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        $result_array['count'] = $rows;
        if($count != 1) {
            $result_array['orders'] = [];
            for($i = 0; $i < $rows; ++$i) {
                $result_array['orders'][] = mysqli_fetch_assoc($result);
            }
        }
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $result_array;
}
function del_data($sql) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $status = 'good';
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $status;
}

function get_order_edit_variants($id) {
    $sql = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$id.' ORDER BY `DATE` DESC';
    $projects = get_data($sql);
    $html = '';
    if(count($projects['orders']) < 1) {
        return false;
    } else {
        foreach ($projects['orders'] as $project) {
            $author = '';
            if($project['manager_id'] > 0) {
                $author = get_manager_name($project['manager_id']);
            } else if ($project['client_id'] > 0) {
                $author = 'Клиент';
            }
            $html .= '<p><a href="'.$GLOBALS['laravel_dir'].'/projects_new/1_'.$id.'?order_project_id='.$project['PROJECT_ID'].'">'.$author.' от '.$project['DATE'].'</a> </p>';
        }
        return $html;
    }

}

function get_manager_name($id_m) {
    if(!$id_m) {
        return 'Неизвесто';
    }
    $sql = 'SELECT * FROM `manager` WHERE `id` = '.$id_m;
    $manager = get_data($sql);
    return $manager['orders'][0]['name'];
}
function get_client_name($id_c) {
    if(!$id_c) {
        return 'Неизвесто';
    }
    $sql = 'SELECT * FROM `client_code` WHERE `code_ac` = '.$id_c;
    $client = get_data($sql);
    return $client['orders'][0]['name'];
}
function get_place_name($id_p) {
    if(!$id_p) {
        return 'Не указан';
    }
    $sql = 'SELECT * FROM `PLACES` WHERE `PLACES_ID` = '.$id_p;
    $place = get_data($sql);
    return $place['orders'][0]['NAME'];
}
if(isset($_GET['filter_client'])) {
    $sql = 'SELECT `name`, `client_id`,`code`,`tel` FROM `client` WHERE `name` LIKE "%'.$_GET['filter_client'].'%" OR `tel` LIKE "%'.trim($_GET['filter_client']).'%" GROUP BY `name` LIMIT 10';
//    echo $sql;
    $clients_data = get_data($sql);
//    $contragents = [];
//    foreach($clients_data['orders'] as $client) {
//        $contr_sql = 'SELECT * FROM `client_code` WHERE `code_ac` = '.$client['code'];
//    }
    echo json_encode($clients_data['orders']);
    exit;
}
if($_GET['del_this_order'] > 0) {
    $sql = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$_GET['del_this_order'];
    $order_check = get_data($sql);
    if($order_check['orders'][0]['status'] == 'черновик') {
        $del_sql = 'DELETE FROM `ORDER1C` WHERE `ID` = '.$_GET['del_this_order'];
        del_data($del_sql);
        $del_sql_project = 'DELETE FROM `PROJECT` WHERE `ORDER1C` = '.$_GET['del_this_order'];
        del_data($del_sql_project);
    }
}
if($_GET['download_programm']) {
    $programs_url = base64_decode($_GET['download_programm']);
    chown($programs_url, 'i.kotok:apache');
    chmod($programs_url, 0777);


    function get_dir_files( $dir, $recursive = true, $include_folders = false ){
        if( ! is_dir($dir) )
            return array();

        $files = array();

        $dir = rtrim( $dir, '/\\' ); // удалим слэш на конце

        foreach( glob( "$dir/{,.}[!.,!..]*", GLOB_BRACE ) as $file ){

            if( is_dir( $file ) ){
                if( $include_folders )
                    $files[] = $file;
                if( $recursive )
                    $files = array_merge( $files, call_user_func( __FUNCTION__, $file, $recursive, $include_folders ) );
            }
            else
                $files[] = $file;
        }

        return $files;
    }

    $programs = get_dir_files($programs_url);

    $zip = new ZipArchive();

    $zip_url = $serv_main_dir.'/files/zips/'.mt_rand(11111111,99999999).'programm.zip';

    $zip->open($zip_url,ZIPARCHIVE::CREATE);

    chown($zip_url, 'i.kotok:apache');
    chmod($zip_url, 0777);


    foreach($programs as $file) {
        if (!$zip->addFile($file, str_replace($programs_url.'/', '', $file))) {
            exit ('Error adding');
        }
    }

    $zip->close();

    $download_url = str_replace($serv_main_dir,$main_dir.'/', $zip_url);
    header('Location: '.$download_url.'&nw='.$_GET['nw']);

    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <link rel="stylesheet" href="/css/fontawesome/all.min.css">
    <title>Каталог заказов KRONAS</title>
    <style >
        .erase-tr{
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }
        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }
        .loading-search{
            position: absolute;
            right: 22px;
            top: 3px;
            display: none;
        }
        #searchOnCode-box{padding: 10px;display: none;}
        #searchOnCode-box li{ cursor: pointer; }
        form{
            margin: 10px;
        }
        h3.success  {
            color: green;
        }
        body {
            padding: 45px;
        }
        .pagination {
            overflow-x: scroll;
        }
        #client_variants {
            position: absolute;
            z-index: 99999;
            width: 400px;
        }
        #client_variants .list-group-item {
            position: relative;
            display: block;
            padding: 0.4rem 1.25rem;
            border: 1px solid rgba(0,0,0,.125);
            background: lightgray;
        }
        .action_button_container {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
        }
        .action_button_container > a {
            margin: 0 5px;
        }
        .filter_actions_container {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-end;
            align-items: center;
        }
        .filter_actions_container > button {
            margin-left: 15px;
        }
        td {
            font-size: 13px;
            min-width: 134px;
            vertical-align: middle;
        }
        th {
            font-size: 13px;
        }
        .form-control {
            color: tomato;
        }
        #client_variants_auth {
            position: absolute;
            z-index: 99999;
            width: 400px;
        }
        #client_variants_auth .list-group-item {
            position: relative;
            display: block;
            padding: 0.4rem 1.25rem;
            border: 1px solid rgba(0,0,0,.125);
            background: lightgray;
        }
    </style>


</head>
<body>

<?php



$filter_manager = $_GET['manager'];
$filter_client = $_GET['client'];
$filter_date = $_GET['date'];
$filter_order_id = $_GET['order_id'];
$filter_order_akcent = $_GET['order_akcent'];
$filter_place = $_GET['place'];
$filter_status = $_GET['status'];
$filter_type = $_GET['type'];
$page = $_GET['page'];

if($filter_type == 1) {
    $sql .= 'SELECT `o`.`ID`,`o`.`DB_AC_ID`,`o`.`DB_AC_NUM`,`o`.`DB_AC_IN`,`o`.`manager`,`o`.`CLIENT`,`o`.`PLACE`,`o`.`status`,`p`.`ORDER1C`,`o`.`CLIENT_ID`, `o`.`readonly`
             FROM `ORDER1C` as `o` 
             JOIN `PROJECT` as `p` 
             ON `o`.`ID` = `p`.`ORDER1C` 
             WHERE `o`.`ID` = `p`.`ORDER1C`';
} else if($filter_type == 4) {
    $sql .= 'SELECT `o`.`ID`,`o`.`DB_AC_ID`,`o`.`DB_AC_NUM`,`o`.`DB_AC_IN`,`o`.`manager`,`o`.`CLIENT`,`o`.`PLACE`,`o`.`status`,`p`.`ORDER1C`,`o`.`CLIENT_ID` FROM `ORDER1C` as `o` LEFT OUTER JOIN `PROJECT` as `p` ON `o`.`ID` = `p`.`ORDER1C` WHERE `p`.`ORDER1C` IS NULL';
} else {
    $sql = "SELECT * FROM `ORDER1C` as `o` WHERE `o`.`ID` > 0 ";
}

if($_GET['manager_id']) {
    $_SESSION['manager_id'] = $_GET['manager_id'];
    if($_GET['is_admin'] == true) {
        $_SESSION['isAdmin'] = 1;
    }
}
if(!$_SESSION['manager_id']) {
    header('Location: '.$main_dir.'/login.php'.'?nw='.$_GET['nw']);
}
$manager_data_sql = 'SELECT * FROM `manager` WHERE `id` = '.$_SESSION['manager_id'];
$manager_data_info = get_data($manager_data_sql);
if($manager_data_info['orders'][0]['admin'] == 1) {
    $_SESSION['isAdmin'] = 1;
}
if($_SESSION['manager_id'] && !$_GET['manager']) {
    $filter_manager = $_SESSION['manager_id'];
}
if($filter_manager != 'all' && $filter_manager) {
    $sql .= ' AND `o`.`manager` = '.$filter_manager;
}
if($filter_client != 'all' && $filter_client) {
    $sql .= ' AND `o`.`CLIENT` = ' . $filter_client;
}
if($filter_date != 'all' && $filter_date) {
//    $post_date = new DateTime();
//    $new_date = date_sub($post_date, date_interval_create_from_date_string($filter_date.' days'));
//    $post_date = date_format($new_date,"Y-m-d H:i:s");
//    $sql .= ' AND `o`.`DB_AC_IN` between "'.$post_date.'" and "'.date("Y-m-d H:i:s").'"';
    $sql .= ' AND `o`.`DB_AC_IN` between "'.date('Y').'-'.$filter_date.'-01 00:00:00" and "'.date('Y').'-'.$filter_date.'-31 00:00:00"';
}
if($filter_order_id != 'all' && $filter_order_id) {
    $sql .= ' AND `o`.`DB_AC_ID` LIKE "%'.$filter_order_id.'%"';
}
if($filter_order_akcent != 'all' && $filter_order_akcent) {
    $sql .= ' AND `o`.`DB_AC_NUM` LIKE "%'.$filter_order_akcent.'%"';
}
if($filter_place != 'all' && $filter_place) {
    $sql .= ' AND `o`.`PLACE` = '.$filter_place;
}
if($filter_status != 'all' && $filter_status) {
    $sql .= ' AND `o`.`status` = "'.$filter_status.'"';
}


$orders_count = get_data($sql, 1);
if($filter_type == 1) {
    $sql .= ' GROUP BY `o`.`ID`';
}
if($page == 'all' || !$page) {
    $limits = ' LIMIT 100 OFFSET 0';
} else {
    $limits = ' LIMIT 100 OFFSET '.($page-1)*100;
}

$sql .= ' ORDER BY `o`.`DB_AC_IN` DESC';



$sql .= $limits;



$orders_db = get_data($sql);

$filtered_orders = $orders_db['orders'];

$pages = $orders_count['count']/100;
if(round($pages) >= $pages) {
    $pages = round($pages);
} else if(round($pages) < $pages) {
    $pages = round($pages) + 1;
}

////Filters
$sql_manager = 'SELECT * FROM `manager` ORDER BY `name`';
$managers = get_data($sql_manager);

$sql_places = 'SELECT * FROM `PLACES`';
$palces = get_data($sql_places);


?>

<div class="container-fluid">
    <form action="./manager_all_order.php<?='?nw='.$_GET['nw']?>" method="GET">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Менеджер:</label>
                    <select class="form-control" id="filter_manager" name="manager">
                        <option value="all">все</option>
                        <?php foreach($managers['orders'] as $manager): ?>
                        <option value="<?= $manager['id'] ?>" <?php if($filter_manager == $manager['id']) echo 'selected="selected"'; ?>><?= $manager['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Дата:</label>
                    <select class="form-control" id="filter_date" name="date">
                        <option value="all" <?php if($filter_date == 'all') echo 'selected'; ?> >все</option>
<!--                        <option value="1" --><?php //if($filter_date == 1) echo 'selected'; ?><!-- >За сегодняшний день</option>-->
<!--                        <option value="7" --><?php //if($filter_date == 7) echo 'selected'; ?><!-- >За прошлую неделю</option>-->
<!--                        <option value="30" --><?php //if($filter_date == 30) echo 'selected'; ?><!-- >За прошлых 30 дней</option>-->
<!--                        <option value="365" --><?php //if($filter_date == 365) echo 'selected'; ?><!-- >За весь год</option>-->


                        <option value="01" <?php if($filter_date == '01') echo 'selected'; ?>>Январь <?= date('Y') ?></option>
                        <option value="02" <?php if($filter_date == '02') echo 'selected'; ?>>Февраль <?= date('Y') ?></option>
                        <option value="03" <?php if($filter_date == '03') echo 'selected'; ?>>Март <?= date('Y') ?></option>
                        <option value="04" <?php if($filter_date == '04') echo 'selected'; ?>>Апрель <?= date('Y') ?></option>
                        <option value="05" <?php if($filter_date == '05') echo 'selected'; ?>>Май <?= date('Y') ?></option>
                        <option value="06" <?php if($filter_date == '06') echo 'selected'; ?>>Июнь <?= date('Y') ?></option>
                        <option value="07" <?php if($filter_date == '07') echo 'selected'; ?>>Июль <?= date('Y') ?></option>
                        <option value="08" <?php if($filter_date == '08') echo 'selected'; ?>>Август <?= date('Y') ?></option>
                        <option value="09" <?php if($filter_date == '09') echo 'selected'; ?>>Сентябрь <?= date('Y') ?></option>
                        <option value="10" <?php if($filter_date == '10') echo 'selected'; ?>>Октябрь <?= date('Y') ?></option>
                        <option value="11" <?php if($filter_date == '11') echo 'selected'; ?>>Ноябрь <?= date('Y') ?></option>
                        <option value="12" <?php if($filter_date == '12') echo 'selected'; ?>>Декабрь <?= date('Y') ?></option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Участок:</label>
                    <select class="form-control" id="filter_place" name="place">
                        <option value="all">все</option>
                        <?php foreach($palces['orders'] as $place): ?>
                            <option value="<?= $place['PLACES_ID'] ?>" <?php if($filter_place == $place['PLACES_ID']) echo 'selected="selected"'; ?>><?= $place['NAME'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Клиент:</label>
                    <input type="text" class="form-control" id="filter_client_name" placeholder="введите имя контрагента" name="filter_client_name" <?php if($_GET['filter_client_name']) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                    <div id="client_variants">
                        <ul class="list-group">

                        </ul>
                    </div>
                    <input type="hidden" name="client" id="filter_client" value="<?= $filter_client ?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Статус:</label>
                    <select class="form-control" id="filter_status" name="status">
                        <option value="all" <?php if($filter_status == 'all') echo 'selected'; ?> >все</option>
                        <option value="в работе" <?php if($filter_status == 'в работе') echo 'selected'; ?> >в работе</option>
                        <option value="выполнен" <?php if($filter_status == 'выполнен') echo 'selected'; ?> >выполнен</option>
                        <option value="выполнен" <?php if($filter_status == 'выполнен') echo 'selected'; ?> >черновик</option>
                        <option value="отменен" <?php if($filter_status == 'отменен') echo 'selected'; ?> >отменен</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Номер в Акценте:</label>
                    <input type="text" class="form-control" id="filter_order_akcent_name" placeholder="введите например к739/6" name="order_akcent"  <?php if($filter_order_akcent) echo 'value="'.$filter_order_akcent.'"' ?>>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlInput1">ID в Акценте:</label>
                    <input type="text" class="form-control" id="filter_order_id_name" placeholder="введите например 9466448" name="order_id" <?php if($filter_order_id) echo 'value="'.$filter_order_id.'"' ?>>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Тип:</label>
                    <select class="form-control" id="filter_type" name="type">
                        <option value="all" <?php if($filter_type == 'all') echo 'selected'; ?> >все</option>
                        <option value="1" <?php if($filter_type == 1) echo 'selected'; ?> >пильные</option>
                        <option value="4" <?php if($filter_type == 4) echo 'selected'; ?> >обычные</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                    <b>+</b> Новый заказ
                </button>
            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <div class="filter_actions_container">
                    <a href="<?= $main_dir ?>/manager_all_order.php">Очистить фильтры</a>
                    <button type="submit" class="btn btn-primary">Фильтровать</button>
                </div>

            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                <tr>
<!--                    <th scope="col">ID</th>-->
                    <th scope="col">ID в Акценте</th>
                    <th scope="col">Номер в Акценте</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Менеджер</th>
                    <th scope="col">Клиент</th>
                    <th scope="col">Участок</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($filtered_orders as $order): ?>
                <tr>
<!--                    <td>--><?//= $order['ID'] ?><!--</td>-->
                    <td><?= $order['DB_AC_ID'] ?></td>
                    <td><?= $order['DB_AC_NUM'] ?></td>
                    <td><?= $order['DB_AC_IN'] ?></td>
                    <td><?= get_manager_name($order['manager']) ?></td>
                    <td><?= get_client_name($order['CLIENT']) ?></td>
                    <td><?= get_place_name($order['PLACE']) ?></td>
                    <td><?= $order['status'] ?></td>
                    <td>
                        <div class="action_button_container">
                            <?php if(get_order_edit_variants($order['ID'])): ?>
                                <button type="button" class="btn btn-success" data-toggle="popover" title="Версии проектов заказа:" data-html="true" data-content='<?php echo get_order_edit_variants($order['ID']); ?>' title="Редактировать"><i class="fas fa-edit"></i></button>
                                <a href="<?= $main_dir ?>/refresh_project_programm.php?project=<?= $order['ID'] ?>" target="_blank" class="btn btn-info" title="Обновить программы"><i class="fas fa-sync-alt"></i></a>
                            <?php endif; ?>
                            <?php if($order['status'] == 'черновик'): ?>
                                <?php
                                $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                                if(strpos($url, '?') == false){
                                    $url .= '?del_this_order='.$order['ID'];
                                } else {
                                    $url .= '&del_this_order='.$order['ID'];
                                }
                                ?>
                                <a href="<?=$url?>" class="btn btn-danger">х</a>
                            <?php endif; ?>
                            <!--                        <a href="--><?//= $main_dir ?><!--/refresh_project_programm.php?project=--><?//= $order['ID'] ?><!--" target="_blank" class="btn btn-info">Обновить программы</a>-->
                            <?php if($_SESSION['isAdmin'] == 1 && $order['programm']): ?>
                            <a href="./manager_all_order.php?download_programm=<?= base64_encode($order['programm']) ?>" class="btn btn-info" target="_blank" title="Скачать программы"><i class="fas fa-cloud-download-alt"></i></a>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                        <?php for($i = 1; $i <= $pages; $i++): ?>
                            <li class="page-item <?php echo $page == $i ? 'active' : ''; ?>"><a class="page-link" href="<?= $main_dir ?>/manager_all_order.php?page=<?=$i?>&manager=<?= $_GET['manager'] ?>&client=<?= $_GET['client'] ?>&date=<?= $_GET['date'] ?>&status=<?= $_GET['status'] ?>&order_akcent=<?= $_GET['order_akcent'] ?>&place=<?= $_GET['place'] ?>&order_id=<?= $_GET['order_id'] ?>&type=<?= $_GET['type'] ?>$filter_client_name=<?= $_GET['filter_client_name']?>"><?=$i?></a></li>
                        <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>

<script>
    async function get_clients_filtered(client_id) {
        console.log('clear');
        $('#client_variants ul').html(' ');
        console.log('clearz');
        $('#client_variants ul').html(`
            <div id="checks_lodaer_containers_auth">
              <div style="background: #f5f3f3;padding: 45px;display: flex;flex-flow: row nowrap;justify-content: center;align-items: center;position: absolute;width: 100%;height: 100%;z-index: 999999;">
                  <img src="<?= $main_dir ?>/kronasapp/public/images/download.gif" alt="" width="45px">
              </div>
            </div>
        `);
        if(!client_id) {
            $('#filter_client').val('');
            $('#filter_client_name').val('');
            return;
        }
        let request = await fetch('<?= $main_dir ?>/manager_all_order.php?filter_client=' + client_id);
        let response = await request.json();
        let counter = 0;
        await response.forEach(function (element) {
            if(counter <= 10) {
                $('#client_variants ul').append(`
                    <li class="list-group-item" data-client="${element.code}" data-phone="${element.tel}">${element.name}</li>
                `);
            }
           counter++;
        });
        $('#client_variants ul li').on('click', function (e) {
            let current_variant = $(e.target).attr('data-client');
            let current_phone = $(e.target).attr('data-phone');
            let current_variant_name = $(e.target).html();
            $('#filter_client').val(current_variant);
            $('#filter_client_name').val(current_variant_name);
            $('#filter_client_phone').val(current_phone);
            $('#client_variants ul').html('   ');
        });
        setTimeout(function () {
            $('#checks_lodaer_containers_auth').hide();
        },1000);
    }
    $(document).ready(function () {
        $('#filter_client_name').on('input', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered(client_data);
        });
        // $('#filter_client_name').on('change', async function (e) {
        //     let client_data = $(e.target).val();
        //     get_clients_filtered(client_data);
        // });
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    });
    async function get_clients_filtered_auth(client_id) {
        $('#client_variants_auth ul').html(' ');
        $('#client_variants_auth ul').html(`
            <div id="checks_lodaer_containers">
              <div style="background: #f5f3f3;padding: 45px;display: flex;flex-flow: row nowrap;justify-content: center;align-items: center;position: absolute;width: 100%;height: 100%;z-index: 999999;">
                  <img src="<?= $main_dir ?>/kronasapp/public/images/download.gif" alt="" width="45px">
              </div>
            </div>
        `);
        if(!client_id) {
            $('#filter_client_auth').val('');
            $('#filter_client_name_auth').val('');
            return;
        }
        let request = await fetch('<?= $main_dir ?>/manager_all_order.php?filter_client=' + client_id);
        let response = await request.json();
        let counter = 0;
        await response.forEach(function (element) {
            if(counter <= 10) {
                $('#client_variants_auth ul').append(`
                    <li class="list-group-item" data-client="${element.client_id}" data-code="${element.code}" data-phone="${element.tel}">${element.name}</li>
                `);
            }
            counter++;
        });
        $('#client_variants_auth ul li').on('click', function (e) {
            let current_variant = $(e.target).attr('data-client');
            let current_code = $(e.target).attr('data-code');
            let current_phone = $(e.target).attr('data-phone');
            let current_variant_name = $(e.target).html();
            $('#filter_client_auth').val(current_variant);
            $('#filter_client_code_auth').val(current_code);
            $('#filter_client_name_auth').val(current_variant_name);
            $('#filter_client_phone_auth').val(current_phone);
            $('#client_variants_auth ul').html('   ');
        });
        setTimeout(function () {
            $('#checks_lodaer_containers').hide();
        },1000);
    }
    $(document).ready(function () {
        // $('#filter_client_name_auth').on('keypress', async function (e) {
        //     let client_data = $(e.target).val();
        //     await get_clients_filtered_auth(client_data);
        // });
        $('#filter_client_name_auth').on('input', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered_auth(client_data);
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
        $('#client_auth_form_button').on('click', function (e) {
            e.preventDefault();
            $('#client_auth_message').html(' ');
            if($('#filter_client_auth').val() == null || $('#filter_client_auth').val() == '' || $('#filter_client_code_auth').val() == null || $('#filter_client_code_auth').val() == '') {
                $('#client_auth_message').html('<span style="color:red">Необходимо выбрать клиента из выпадающего списка!</span>');
            } else {
                $('#sac_form').submit();
            }
        })
    });
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Выбирите клиента:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= $laravel_dir ?>/start_new_porject<?='?nw='.$_GET['nw']?>" method="GET" id="sac_form">
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Клиент:</label>
                    <input type="text" class="form-control" id="filter_client_name_auth" placeholder="введите имя контрагента" name="filter_client_name">
                    <div id="client_variants_auth">
                        <ul class="list-group">

                        </ul>
                    </div>
                    <input type="hidden" name="client_id" id="filter_client_auth" value="">
                    <input type="hidden" name="manager_id" id="filter_manager_auth" value="<?= $_SESSION['manager_id'] ?>">
                    <input type="hidden" name="client_code" id="filter_client_code_auth" value="">
                    <input type="hidden" name="set_new_order" value="1">
                </div>
                <div class="form-group" id="client_auth_message">

                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="client_auth_form_button">Создать новый заказ</button>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>


