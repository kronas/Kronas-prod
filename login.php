<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 19.06.2020
 * Time: 15:20
 */

require_once '_1/config.php';
require_once DIR_CORE . 'func.php';
require_once DIR_CORE . 'func_mini.php';
// exit('11');

session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

//..25.11.2020 session restore
if (!isset($_SESSION['manager_id']) && !isset($_SESSION['manager_name'])) {
    if (isset($_GET['session_id'])) {
        if (!empty($_GET['session_id'])) {
            $_SESSION = get_temp_file_data($_GET["session_id"]);

            //.. Восстанавливаем сессию
            if (isset($session_restore['project_manager'])) {
                $_SESSION['project_manager'] = $session_restore['project_manager'];
            }
            if (isset($session_restore['project_client'])) {
                $_SESSION['project_client'] = $session_restore['project_client'];
            }
            if (isset($session_restore['user'])) {
                $_SESSION['user'] = $session_restore['user'];
            }
            if (isset($session_restore['tec_info'])) {
                $_SESSION['tec_info'] = $session_restore['tec_info'];
            }

            $_SESSION['saveAutorization'] = $_GET["session_id"];
        }
    }else{
        header('Location: '.$laravel_dir.'/auth?checkautorized=2'.'&nw='.$_GET['nw']);
    }
}

if(isset($_GET['logout_manager'])) {
    unset($_SESSION['manager_id']);
    unset($_SESSION['manager_name']);
    unset($_SESSION['isAdmin']);
    unset($_SESSION['user']);
    //.. Удаляем сессию Гиблаба
    header('Location: '.$laravel_dir.'/set_auth_manager_ids?logout=1'.'&nw='.$_GET['nw']);
}
//.. Если вошли в систему в GibLab и пришли сюда
if (isset($_GET['giblabsession'])) {
    $sql = 'SELECT * FROM `manager` WHERE `id` = "'.$_GET['manager_id'].'"';
    $manager_data = get_data($sql);
    if(isset($manager_data[0])) {
        $manager_data = $manager_data[0];
        $_SESSION['manager_id'] = $manager_data['id'];
        $_SESSION['manager_name'] = $manager_data['name'];
        //.. Добавляем массив ['user'] в котором будет храниться вся информация по менеджеру
        // (необходимо для поддержки раздвижных сиситем)
        $_SESSION['isAdmin'] = $manager_data['admin'];
        $role = $manager_data['admin'] == 1 ? 'admin' : 'manager';
        $_SESSION['user'] = array (
            'ID' => $manager_data['id'],
            'name' => $manager_data['name'],
            'phone' => $manager_data['phone'],
            'mail' => $manager_data['e-mail'],
            'access' => $manager_data['access'],
            'role' => $role,
            'code' => $manager_data['code'],
        );
    }
    header('Location: '.$main_dir.'/manager_all_order.php'.'?nw='.$_GET['nw']);
}

if(isset($_SESSION['manager_id']) && isset($_SESSION['manager_name'])) {
    header('Location: '.$main_dir.'/manager_all_order.php'.'?nw='.$_GET['nw']);
}

function get_data($sql) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        $result_array = [];
        for($i = 0; $i < $rows; ++$i) {
            $result_array[] = mysqli_fetch_assoc($result);
        }
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $result_array;
}
if($_POST['login']) {
    if($_POST['user_type'] == 'client') {
        $sql = 'SELECT * FROM `client` WHERE `e-mail` = "'.$_POST['email'].'" AND `pass` = md5('.$_POST['password'].')';
        $client_data = get_data($sql);
        if(isset($client_data[0])) {
            $client_data = $client_data[0];
            $_SESSION['user'] = [
                'type' => 'client',
                'id' => $client_data['client_id'],
                'name' => $client_data['name'],
                'email' => $client_data['e-mail'],
                'code' => $client_data['code']

            ];
            header('Location: '.$main_dir.'/helpdesc/'.'&nw='.$_GET['nw']);
            exit;
        } else {
            $_SESSION['login_error'] = 'Данные авторизации не правильные!';
            header('Location: '.$main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
            exit;
        }
    }
    if($_POST['user_type'] == 'manager') {
        $sql = 'SELECT * FROM `manager` WHERE `e-mail` = "'.$_POST['email'].'" AND `password` = "'.$_POST['password'].'"';
        $manager_data = get_data($sql);

        //.. 07.11.2022 Тоценко Логирование авторизации
        // #################################################################################################################
        $userLogStr = "Entered email -> " . $_POST['email'] . "\r\nEntered password -> " . $_POST['password'] . "\r\n";

        if(isset($manager_data[0])) {
            $roleLog = $manager_data[0]['admin'] == 1 ? 'admin' : 'manager';
            $userLogStr .= "Manager ID -> " . $manager_data[0]['id'] . "\r\nManager name -> " . $manager_data[0]['name'] . "\r\nManager phone -> " . $manager_data[0]['phone'] . "\r\nManager mail -> " . $manager_data[0]['e-mail'] . "\r\nManager role -> " . $roleLog . "\r\n";
        }

        file_put_contents(DIR_LOGS . 'login/manager/loginPage/' . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . "\r\n" . $userLogStr . "\r\n\r\n", FILE_APPEND);

        // И удаление логов, старше трёх месяцев (клиентов, менеджеров авторизующихся в конструкторе заказов и менеджеров авторизующихся на странице логина)
        $loginPageAuth = GetFileNames(DIR_LOGS . 'login/manager/loginPage', true);
        $orderConstructorAuth = GetFileNames(DIR_LOGS . 'login/manager/laravel', true);
        $clientsAuth = GetFileNames(DIR_LOGS . 'login/client', true);

        if (!empty($loginPageAuth)) {
            foreach ($loginPageAuth as $value) {
                if (filemtime(DIR_LOGS . 'login/manager/loginPage/' . $value) < (mktime() - (2678400 * 3))) {
                    unlink(DIR_LOGS . 'login/manager/loginPage/' . $value);
                } else break;
            }
        }

        if (!empty($orderConstructorAuth)) {
            foreach ($orderConstructorAuth as $value) {
                if (filemtime(DIR_LOGS . 'login/manager/laravel/' . $value) < (mktime() - (2678400 * 3))) {
                    unlink(DIR_LOGS . 'login/manager/laravel/' . $value);
                } else break;
            }
        }

        if (!empty($clientsAuth)) {
            foreach ($clientsAuth as $value) {
                if (filemtime(DIR_LOGS . 'login/client/' . $value) < (mktime() - (2678400 * 3))) {
                    unlink(DIR_LOGS . 'login/client/' . $value);
                } else break;
            }
        }
        // #################################################################################################################

        if(isset($manager_data[0])) {
            $manager_data = $manager_data[0];
            $_SESSION['manager_id'] = $manager_data['id'];
            $_SESSION['manager_name'] = $manager_data['name'];
            //.. Добавляем массив ['user'] в котором будет храниться вся информация по менеджеру
            // (необходимо для поддержки раздвижных сиситем)
            $_SESSION['isAdmin'] = $manager_data['admin'];
            $role = $manager_data['admin'] == 1 ? 'admin' : 'manager';
            $_SESSION['user'] = array (
                'ID' => $manager_data['id'],
                'name' => $manager_data['name'],
                'phone' => $manager_data['phone'],
                'mail' => $manager_data['e-mail'],
                'access' => $manager_data['access'],
                'role' => $role,
                'code' => $manager_data['code'],
            );
            //..25.11.2020 session restore
            put_temp_file_data(session_id(), $_SESSION);

            header('Location: '.$laravel_dir.'/set_auth_manager_ids?manager_id='.$_SESSION['manager_id'].'&rssession=1'.'&nw='.$_GET['nw']);
            exit;
        } else {
            $_SESSION['login_error'] = 'Данные авторизации не правильные!';
            header('Location: '.$main_dir.'/login.php');
            exit;
        }
    }
}
// function getUserData() {
//     $sql = 'SELECT * FROM `manager` WHERE `e-mail` = "'.$_POST['email'].'" AND `password` = "'.$_POST['password'].'"';
//     $manager_data = get_data($sql);
//     if(isset($manager_data[0])) {
//         $manager_data = $manager_data[0];
//         $_SESSION['manager_id'] = $manager_data['id'];
//         $_SESSION['manager_name'] = $manager_data['name'];
//         //.. Добавляем массив ['user'] в котором будет храниться вся информация по менеджеру
//         // (необходимо для поддержки раздвижных сиситем)
//         $_SESSION['isAdmin'] = $manager_data['admin'];
//         $role = $manager_data['admin'] == 1 ? 'admin' : 'manager';
//         $_SESSION['user'] = array (
//             'ID' => $manager_data['client_id'],
//             'name' => $manager_data['name'],
//             'phone' => $manager_data['tel'],
//             'mail' => $manager_data['e-mail'],
//             'role' => $role,
//             'code' => $manager_data['code'],
//         );
// }

?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <script
                src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css">
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
        <link rel="stylesheet" href="/css/fontawesome/all.min.css">
        <title>Вход <?= $pipe ?></title>
        <style>
            html{height: 100%;}
            .main_login_container{display: flex;align-items: center;justify-content: center;height: 100%;}
            .card{max-width: 350px; margin: 70px auto 0;}
        </style>
    </head>
    <body>
        <div class="container main_login_container">
                    <div class="card">
                        <article class="card-body">
                            <h4 class="card-title text-center mb-4 mt-1">Авторизация менеджера</h4>
                            <hr>
                            <?php if(isset($_SESSION['login_error'])): ?>
                                <p class="text-danger text-center"><?= $_SESSION['login_error'] ?></p>
                            <?php endif; ?>
                            <form action="<?= $main_dir ?>/login.php<?='?nw='.$_GET['nw']?>" method="POST">
                                <div class="form-group">
<!--                                    <div class="custom-control custom-radio">-->
<!--                                        <input type="radio" id="user_client" name="user_type" class="custom-control-input" value="client" >-->
<!--                                        <label class="custom-control-label" for="user_client" >Клиент</label>-->
<!--                                    </div>-->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="user_manager" name="user_type" class="custom-control-input" value="manager" checked>
                                        <label class="custom-control-label" for="user_manager">Менеджер</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                        </div>
                                        <input name="email" class="form-control" placeholder="E-mail" type="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                        </div>
                                        <input class="form-control" placeholder="******" type="password" name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block" name="login" value="1"> Вход  </button>
                                </div>
                                <!--                        <p class="text-center"><a href="#" class="btn">Забыли пароль?</a></p>-->
                            </form>
                        </article>
                    </div>
        </div>
    </body>
</html>


<?php //include_once('templates/footer.php'); ?>

<?php unset($_SESSION['login_error']);  ?>
