<?php
session_start();

include_once("func.php");

function get_data($sql, $count = 0) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        $result_array['count'] = $rows;
        if($count != 1) {
            $result_array['orders'] = [];
            for($i = 0; $i < $rows; ++$i) {
                $result_array['orders'][] = mysqli_fetch_assoc($result);
            }
        }
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $result_array;
}

$actual_date = date('Y-m-d 00:00:00');
$before_14_days_date = date_modify(date_create(), '-7 day');
$before_14_days = date_format($before_14_days_date, 'Y-m-d 00:00:00');
$before_21_days_date = date_modify(date_create(), '-7 day');
$before_21_days = date_format($before_21_days_date, 'Y-m-d 00:00:00');

$sql_get = 'SELECT * FROM `ORDER1C` WHERE `production` = 1 AND `status` = "выполнен" AND `DB_AC_IN` < "'.$before_14_days.'"';
$complete_orders = get_data($sql_get);

foreach($complete_orders['orders'] as $order) {
    $sql_complete_projects = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$order['ID'].' ORDER BY `DATE` DESC';
    $complete_orders_projects = get_data($sql_complete_projects);
    foreach ($complete_orders_projects['orders'] as $k=>$v) {
        if($k == 0) {
            continue;
        } else {
            $sql_delete_project = 'DELETE FROM `PROJECT` WHERE `PROJECT_ID` = '.$v['PROJECT_ID'];
            mysqli_query($link, $sql_delete_project);
            echo 'удален проджект '.$v['PROJECT_ID'];
        }
    }
    $sql_change_status = 'UPDATE `ORDER1C` SET `status`= "архив" WHERE `ID` ='.$order['ID'];
}

foreach($complete_orders['orders'] as $order) {
    $programs = $order['programm'];
    echo 'Очистка папки '.$programs.'...<hr>';
    delDir($programs);
}


$sql_edits_orders = 'SELECT * FROM `ORDER1C` WHERE `status` = "черновик"';
$cedits_orders = get_data($sql_edits_orders);


foreach($cedits_orders['orders'] as $order) {
    $sql_get_edits_project = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$order['ID'].' ORDER BY `DATE` DESC';
    $sql_edits_projects = get_data($sql_get_edits_project);


    if($sql_edits_projects['orders'][0]['DATE'] < $before_21_days) {
        $sql_delete_order = 'DELETE FROM `ORDER1C` WHERE `ID` = '.$order['ID'];
        mysqli_query($link, $sql_delete_order);
        $sql_delete_project = 'DELETE FROM `PROJECT` WHERE `ORDER1C` = '.$order['ID'];
        mysqli_query($link, $sql_delete_project);
        $sql_delete_parts = 'DELETE FROM `PART` WHERE `ORDER1C` = '.$order['ID'];
        mysqli_query($link, $sql_delete_parts);
        delDir($order['programm']);
        echo 'Удален заказ-черновик '.$order['DB_AC_ID'].'<hr>';
    }else if($sql_edits_projects['orders'][0]['DATE'] < $before_14_days) {
        $sql_delete_project = 'DELETE FROM `PROJECT` WHERE `ORDER1C` = '.$order['ID'].' AND `PROJECT_ID` <> '.$sql_edits_projects['orders'][0]['PROJECT_ID'];
        mysqli_query($link, $sql_delete_project);
        delDir($order['programm']);
        echo 'Удалены проекты и программы заказа-черновика '.$order['DB_AC_ID'].'<hr>';
    }

}
