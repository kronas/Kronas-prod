(function ($) {


    $(document).ready(function () {
        $(document).on('shown.bs.modal', '#selectPart', function (event) {

            var button = $(event.relatedTarget);
            var recipient = button.data('targetid');

            $('#cut').val('cut');
                var btnID =button.attr('id');
                if (typeof btnID !== typeof undefined && btnID !== false)
                {
                    var cut=btnID[9];
            if(cut=='l' || cut=='r' || cut=='b' || cut=='t')
            {
                $('#cut').val(cut);
            }
            }

            $('#targetidadd').val('');
            $('#targetid').val(recipient);
            if(parseInt(recipient)==0)
            {
                $('#targetidadd').val(button.data('targetxncid'));
            }
            if(button.attr('id').substring(0,9) =='btn_inner')
            {
                $('#targetidadd').val('inner');
            }
            var typeop = button.data('typeop');
            $('#typeop').val(typeop);
//            var t = parseInt(button.data('t'));
            $(this).find(".modal-body-in").load("list2browse.php?typeop="+typeop);
        });
        $(document).on('hide.bs.modal', '#selectPart', function (event) {
            var recipient = $('#targetid').val();
            if(isNaN(parseInt($('#slectedid-' + recipient).val())))
            {
         /*       $('select#stitching_band_' + recipient).removeAttr('selected');


                $('select#stitching_band_' + recipient).val($("select#stitching_band_" + recipient + " option:first").val());

                $('input[name = stitching_' + recipient + ']').prop( "checked", false );
                $('select#stitching_band_' + recipient).change();
                $('input[name = stitching_' + recipient + ']').change();
           //     $("select#stitching_band_" + recipient).prop("selectedIndex", 0);
                */
            }
            form_ready_step2();
        });
        $(document).on('click', '#simple_list li span', function (event, sel) {

            var li = $(this).parent('li');
            var folderid = li.data('folderid');
            $('[class ^= list-data-tree-] li').hide();
            var t=parseInt($('#t').val());

            if ($('#typeop').val()==4 && t>0) {
                    $('[class ^= list-data-tree-] li.folid-' + folderid.toString()).each(function(i,elem) {
                        if(parseInt($(elem).data('w'))>=((t*2)+3)) $(elem).show();

                    });

            }
            else
            {
                $('[class ^= list-data-tree-] li.folid-' + folderid.toString()).show();
            }
        });
        $(document).on('click', '[class ^= list-data-tree-] li', function (event, sel) {

            var li = $(this);
            var code = li.data('code');

            var recipient = $('#targetid').val();
            if(parseInt(recipient)==0)
            {
                var recipientadd = $('#targetidadd').val();
                $('#targetidadd').val('');
                $('#slectedxnccode-' + recipientadd).text('Вы выбрали: ' + li.text());
                $('#slectedid-' + recipient).val(code);
            }
            else {
                $('#slectedcode-' + recipient).text('Вы выбрали: ' + li.text());
                $('#slectedid-' + recipient).val(code);
                if ($('#typeop').val() == 1 || $('#typeop').val() == 2) form_ready();
            }
            $('#selectPart').modal('hide')
        });
        $(document).on('click', 'div.data-list-line', function (event, sel) {
            var li = $(this);
            var code = li.data('code');

            var recipient = $('#targetid').val();
            var cut = $('#cut').val();

            if(recipient==0) {
                var targetidadd = $('#targetidadd').val();
                $('#slectedxnccode' + targetidadd).text('Вы выбрали: ' + li.text());
                $('#slectedxncid' + targetidadd).val(code);
            }else{
                if(cut=='cut') {
                    if($('#targetidadd').val()=='inner')
                    {
                        $('#slectedcode-inner-' + recipient).text('Вы выбрали: ' + li.text());
                        $('#slectedid-inner-' + recipient).val(code);
                    }
                    else {
                        $('#slectedcode-' + recipient).text('Вы выбрали: ' + li.text());
                        $('#slectedid-' + recipient).val(code);
                    }

                    if ($('#typeop').val() == 1 || $('#typeop').val() == 2) form_ready();
                }else{
                        $('#slectedcode-' + cut + '-' + recipient).text(li.text());
                        $('#slectedid-' + cut + '-' + recipient).val(code);
                }
            }
                $('#selectPart').modal('hide');
                $('.modal-backdrop.fade.show').remove();
                $('.popover.fade.show').remove();
        });


        $(document).on('change', 'select#user_place', function (event, sel) {
            var recipient = parseInt($(this).find(':selected').val());
            $('input[name="user_cutting"]').prop('checked', false);
            $('div.wrap-user-cutting').hide();
            if (arr_place_auto.includes(recipient) && arr_place_manual.includes(recipient)) {
                $('input[name="user_cutting"][value="auto"]').prop('checked', true);
                $('div.wrap-user-cutting').show()
            }
            else if (!arr_place_auto.includes(recipient) && !arr_place_manual.includes(recipient)) {
                $('input[name="user_cutting"][value="auto"]').prop('checked', true);
            }
            else {
                if (arr_place_auto.includes(recipient))  $('input[name="user_cutting"][value="auto"]').prop('checked', true);
                if (arr_place_manual.includes(recipient)) $('input[name="user_cutting"][value="manual"]').prop('checked', true);
            }
        });
        $(document).on('change', 'input[name ^= xnc_countmill_]', function (event, sel) {
            var nameID = $(this).attr('name').substring(14);
            if ($(this).prop('checked')) {
                $('label.xnc_band.'+nameID).show();
                $('span.xnc_bands.'+nameID).show();
                $('span.xnc_group_'+nameID).show();
                $('select[name^="xnc_band_'+nameID+'"').val(0); $('select[name^="xnc_band_'+nameID+'"').prop("disabled", false);
                $('input[name^="my_cut_angle_'+nameID+'"').val(0); $('input[name^="my_cut_angle_'+nameID+'"').prop("disabled", true);
                $('input[name^="xnc_el_'+nameID+'"').prop('checked', true);
//                if($('input[name="xnc_el_'+nameID+'_t"').prop("checked")==true) {$('input[name="my_cut_angle_'+nameID+'_t"').val(0); $('input[name="my_cut_angle_'+nameID+'_t"').prop("disabled", true);}
//                if($('input[name="xnc_el_'+nameID+'_l"').prop("checked")==true) {$('input[name="my_cut_angle_'+nameID+'_l"').val(0); $('input[name="my_cut_angle_'+nameID+'_l"').prop("disabled", true);}
//                if($('input[name="xnc_el_'+nameID+'_r"').prop("checked")==true) {$('input[name="my_cut_angle_'+nameID+'_r"').val(0); $('input[name="my_cut_angle_'+nameID+'_r"').prop("disabled", true);}
//                if($('input[name="xnc_el_'+nameID+'_b"').prop("checked")==true) {$('input[name="my_cut_angle_'+nameID+'_b"').val(0); $('input[name="my_cut_angle_'+nameID+'_b"').prop("disabled", true);}
            } else {
                $('input[name^="my_cut_angle_'+nameID+'"').prop("disabled", false);
                // xnc_el_6_l
                $('label.xnc_band.'+nameID).hide();
                $('span.xnc_bands.'+nameID).hide();
                $('span.xnc_group_'+nameID).hide();
//                $('input[name ^= my_cut_angle_'+nameID).prop("disabled", false);
            }
        });
        $(document).on('change', 'input[name ^= cut_angle_]', function (event, sel) {
            var nameID = $(this).attr('name').substring(10);
            if ($(this).prop('checked')) {
                $('.angle-box-'+nameID).show();
       } else {
                $('input[name ^=my_cut_angle_'+nameID).val(0);
                $('.angle-box-'+nameID).hide();
            }
        });
        ///////////////// Side /////////////////////
        $(document).on('change', 'input[name ^= cut_side_]', function (event, sel) {
            var nameID = $(this).attr('name').substring(9);
            if ($(this).prop('checked')) {
                $('.side-box-'+nameID).show();
            } else {
                $('input[name ^=my_cut_side_'+nameID).val(0);
                $('.side-box-'+nameID).hide();
            }
        });
        ////////////////////////////////////////////
        $(document).on('change', 'input[name ^= xnc_el_]', function (event, sel) {
            var nameID = $(this).attr('name').substring(7);
            if ($(this).prop('checked')) {
                $('input[name="my_cut_angle_'+nameID+'"').val(0); $('input[name="my_cut_angle_'+nameID+'"').prop("disabled", true);
                $("select#xnc_band_"+nameID).prop("disabled", false);
            } else {
                $('input[name="my_cut_angle_'+nameID+'"').val(0); $('input[name="my_cut_angle_'+nameID+'"').prop("disabled", false);

                $("select#xnc_band_"+nameID).val(0);
                $("select#xnc_band_"+nameID).prop("disabled", true);
            }
        });
        $(document).on('change', 'input[name ^= stitching_]', function (event, sel) {
                var nameID = $(this).attr('name').substring(10);
            var t = $('button#btn_band_l_' + nameID).data('t');
            if ($(this).prop('checked')) {
                $('label[for=type_double_' + nameID + ']').show();
                $('select#type_double_' + nameID).show();

                var tpdb =$('select#type_double_' + nameID).find(':selected');
                var description = tpdb.data('description');
                $('#type_double_descripts_' + nameID).text(description);
                $('#type_double_descripts_' + nameID).show();

                $('label.stitching_band.' + nameID).show();
                $('span.stitching_bands.' + nameID).show();
//                $('select#stitching_band_' + nameID).show();
//                $('select#stitching_band_' + nameID+ ' option').show();
                var opt_is =0;
                $('select#stitching_band_l_' + nameID+ ' option').each(function(i,elem) {

                    if(parseInt($(elem).data('w'))<((t*2)+3))
                    {
                        // var ccd=$(elem).val();
                        // console.log(ccd);
                        // $('span.stitching_bands.' + nameID +' select option[value="' + ccd +'"]').hide();
                    }
                    else if($(elem).val()!='DB') {
                        if(opt_is!=1) $(elem).attr("selected", "selected");
                        opt_is=1;
                    }

                });
                if(opt_is==0)
                {
                    $('label.stitching_band.' + nameID).hide();
                     $('span.stitching_bands.' + nameID+ ' select').hide();
                    $('span.stitching_bands.' + nameID+ " select  >option[value='DB']").attr("selected", "selected");
                    $('span.stitching_bands.' + nameID+ ' button').show();
                }
            } else {
                $('label[for=type_double_' + nameID + ']').hide();
                $('select#type_double_' + nameID).hide();
                $('label.stitching_band.' + nameID).hide();
                $('span.stitching_bands.' + nameID).hide();
                $('button#btn_band_' + nameID).hide();
                $('select#back_double_' + nameID).hide();
                $('select#band_inner_' + nameID).hide();
                $('label[for=band_inner_' + nameID+']').hide();
                $('#type_double_descripts_' + nameID).hide();
            }
            form_ready_step2();
        });
        //.. 18.08.2021 Блок управления вторым чекбоксом (с двух сторон) среза закругления столешницы
        $.each($('td.srez_okryglenia_containers'), function () {
            const cut_cell = $(this);
            cut_cell.on('change', 'input[name ^= pf_no_]', function () {
                const this_input = $(this);
                const nameID = this_input.attr('name').substring(6);
                // console.log(nameID);
                if (this_input.prop('checked')) {
                    cut_cell.append(`
                        <div id="cut-on-both-sides">
                            <input type="checkbox" name="double_pf_no_` + nameID + `" value="1">
                            <label title="Установите галку если нужно закругление с двух противоположных сторон.">С двух сторон<i class="fas fa-info-circle"  data-toggle="modal" data-target="#extra_info_dialog" data-text="12"></i></label>
                        </div>
                    `)
                } else {
                    cut_cell.children('#cut-on-both-sides').remove();
                }
            });
        });
        $(document).on('change', 'select[id ^= stitching_band_]', function (event, sel) {
            var recipient =$(this).find(':selected').val();
            var nameID = $(this).attr('id').substring(15);

            if ($(this).find(':selected').val()=='DB') {
                $('button#btn_band_' + nameID).show();
            } else {
                $('button#btn_band_' + nameID).hide();
                if(nameID[1]=='_' && (nameID[0]=='l' || nameID[0]=='r' || nameID[0]=='t' || nameID[0]=='b'))
                {
                    nameID=nameID[0]+'-'+nameID.substr(2);
                }
                $('#slectedcode-'+nameID).text('');
                $('#slectedid-'+nameID).val('');
            }
            form_ready_step2();
        });
        $(document).on('change', 'select[id ^= xnc_band_]', function (event, sel) {
            var add1 =$(this).data('add1').toString();
            var side =$(this).data('side');

            if ($(this).find(':selected').val()=='DB') {
                $('button#xnc_band_' + side +'_'+ add1).show();
            } else {
                $('button#xnc_band_' + side +'_'+ add1).hide();
                $('#slectedxncid-'+ side +'-'+ add1).text('');
                $('#slectedxnccode-'+ side +'-'+ add1).val('');
            }
            if ($(this).find(':selected').val()=='0') {
                console.log('123');
                $('select#stitching_band_' + side +'_'+ add1).prop("disabled", false);
                $('select[name^="xnc_band_'+nameID+'"').val(0); $('select[name^="xnc_band_'+nameID+'"').prop("disabled", false);
            }else{
                $('select#stitching_band_' + side +'_'+ add1).val(0); $('select#stitching_band_' + side +'_'+ add1).prop("disabled", true);
            }
        });
        $(document).on('change', 'select[id ^= band_inner_]', function (event, sel) {
            var btn_id = $(this).attr('id').substring(11);

            if ($(this).find(':selected').val()=='DB') {
                $('button#btn_inner_' + btn_id).show();
                $('div#slectedcode-inner-' + btn_id).text('');
                $('div#slectedcode-inner-' + btn_id).show();
            } else {
                $('button#btn_inner_' + btn_id).hide();
                $('input#slectedid-inner-' + btn_id).text('');
                $('div#slectedcode-inner-' + btn_id).text('');
                $('div#slectedcode-inner-' + btn_id).hide();
            }

        });
        $(document).on('change', 'select[id ^= type_double_]', function (event, sel) {
            var tpdb =$(this).find(':selected');
            var nameID = $(this).attr('id').substring(12);

            var same = tpdb.data('same');
            var edge = tpdb.data('edge');
            var description = tpdb.data('description');

            if (same!=1) {
                $('select#back_double_' + nameID).show();
                var bd_code = $('select#back_double_' + nameID).data('code');
                    var t = $('button#btn_band_' + nameID).data('t');
                /*
                rez_0=0;
                $('select#back_double_' + nameID+ ' option').each(function(i,elem) {
                    if($(elem).data('code')==bd_code || ((parseInt($(elem).data('t'))*2)+3)<t)
                    {
                        $(elem).hide();
                    }
                    else {
                   rez_0=1;
                        $('#type_double_descripts_' + nameID).text(description);
                        $('#type_double_descripts_' + nameID).show();
                    }

                });
                if(rez_0==0) $('select#back_double_' + nameID).hide();
                */
            } else {
                $('select#back_double_' + nameID).hide();
            }

            $('#type_double_descripts_' + nameID).text(description);
            $('#type_double_descripts_' + nameID).show();
            if (edge==1) {
                $('select#band_inner_' + nameID).show();
                $('label[for=band_inner_' + nameID+']').show();
                $('select#band_inner_' + nameID+ ' option').show();
                rez_0=0;
                $('select#band_inner_' + nameID+ ' option').each(function(i,elem) {
                     if(parseInt($(elem).data('w'))>30) $(elem).hide();
                    else {
                         if(rez_0==0)  $(elem).attr("selected", "selected");
                         rez_0=1;
                     }
                });
                if(rez_0==0) {
                    $('select#band_inner_' + nameID).hide();
                    $('label[for=band_inner_' + nameID+']').hide();
                }
            } else {
                $('select#band_inner_' + nameID).hide();
                $('label[for=band_inner_' + nameID+']').hide();
            }
        });
        //if ($("ul").is("#simple_list")) JSLists.applyToList('simple_list', 'ALL');

    });
    function form_ready() {

        ready = true;
        $('input[name ^= slectedid-]').each(function (index, value) {
            if (!$.isNumeric($(this).val()))ready = false;
        });
        if (ready) {
            $('button[name = op]').prop("disabled", false);
        }
    }
    function form_ready_step2() {
/*
        $('button[name = op]').prop("disabled", true);
        ready = true;
        $('input[name ^= stitching_]:checkbox:checked').each(function (index, value) {
            var nameID = $(this).attr('name').substring(10);
            var values = $('#stitching_band_'+nameID+' :selected').val();
            if(values=='DB')
            {
                if(isNaN(parseInt($('#slectedid-' + nameID).val())))
                {
                    ready = false;
                }
            }
        });
        if (ready) {
            $('button[name = op]').prop("disabled", false);
        }
*/
    }
})(jQuery);
