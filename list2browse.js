(function ($) {


    $(document).ready(function () {

        $('#submit').click(function(){

            var out={};

            $('input[id ^= check-]').each(function(i,elem) {
                if ($(this).prop('checked')) {
                    out[$(this).attr('id')] = $(this).val();
                }
            });
            var fadddata = $('param#filtr-add-data');

            out['filtr'] = 1;
            out['typeop'] = fadddata.data('typeop');
            out['vart'] = fadddata.data('vart');
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:out,
                success:function(resp){
                    $('.data-list').html(resp);
                }
            });

        });
        $('#reset').click(function(){

            var out={};

            $("#accordionExample input[id ^= check-]").prop("checked", false);
            $("#selected-prop").html('');
            var fadddata = $('param#filtr-add-data');

            out['filtr'] = 1;
            out['typeop'] = fadddata.data('typeop');
            out['vart'] = fadddata.data('vart');
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:out,
                success:function(resp){
                    $('.data-list').html(resp);
                }
            });

        });

        $(document).on('click', '.pagination span', function (event, sel) {
            var out={};
            var li = $(this);
            var pagego = li.data('pagego');
            var fadddata = $('param#filtr-add-data');

            var filtr = $('textarea#filtr-browse');
            out['pager'] = pagego-1;
            out['filter'] = filtr.val();
            out['typeop'] = fadddata.data('typeop');
            out['vart'] = fadddata.data('vart');
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:out,
                success:function(resp){
                    $('.data-list').html(resp);
                }
            });

        });

        $(document).on('click', '#accordionExample input[id ^= check-]', function (event, sel) {
            var nameID = $(this).attr('id').substring(6);
            if ($(this).prop('checked')) {
                if (!$("#selected-prop").is("#selected-"+nameID))
                {
                    $("#selected-prop #selected-"+nameID).remove();
                    $("#selected-prop").append("<span id='selected-"+nameID+"' class='selected-check'><i class='close-sel'>x</i> "+$(this).val()+"</span>");
                }
            } else {
                $("#selected-prop #selected-"+nameID).remove();
            }
        });


        $(document).on('click', '.modal-backdrop.fade.show', function (event, sel) {
            $(this).remove();
            $('.popover.fade.show').remove();
        });
        $(document).on('click', 'i,close-sel', function (event, sel) {
            var nameID = $(this).parent().attr('id').substring(9);
            $("#accordionExample input#check-"+nameID).prop("checked", false);
            $("#selected-prop #selected-"+nameID).remove();

        });
        $(document).on({
            mouseenter: function(){
               $(this).popover('show');
             //    var nameID = $(this).attr('id').substring(6);
                var obj = $(this);
                var code = obj.data('code');
                $('div.data-list-images').hide();
                $('div.images-'+code).show();
            },
            mouseleave: function(){
                $( this ).popover('hide')
            }
        }, 'div.data-list-line');
    });
})(jQuery);
