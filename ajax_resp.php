<?php

require_once '_1/config.php';
include_once('functions1.php');
if(isset($_POST)) {
    if(isset($_POST['filtr'])) {
        $fltr = array();
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 6) == 'check-') {
                $_in = explode('-', $key);
                $fltr[$_in[1]][] = $value;
                // echo $_in[0].' = '.$_in[1].' = '.$_in[2].' = '.$value."\n";
            }
            // echo $key.' = '.$value."\n";
        }
        $pics='';
        $data = get_list2browse($fltr, 0, 25, $_POST['typeop'], $_POST['vart'],$pics);
    }
    if(isset($_POST['pager'])) {
        $fltr=unserialize($_POST['filter']);
        // $data=var_dump($_POST['filter']);
        // echo '<pre>'; print_r($filter); echo '</pre>'; exit();
        $pics='';
        $data = get_list2browse($fltr, (int) $_POST['pager'], 25, $_POST['typeop'], $_POST['vart'],$pics);
    }
    if(isset($_POST['keyword'])) {
        $data=db_get_list_add_data($_POST['keyword']);
    }
    if(isset($_POST['addtolist'])) {
        $data=db_get_list_add_line($_POST['addtolist']);
    }
    if(isset($_POST['newOrderForm'])) {
        if ($_POST['newOrderForm'] == 'RS') {
            $form_link = $main_dir.'/clients/RS/clients/create.php';
            $apply = 'Создать заказ по раздвижной системе';
        } else {
            $form_link = $laravel_dir.'/start_new_porject'.'?nw='.$_GET['nw'];
            $apply = 'Создать пильный заказ';
        }
        ob_start();
        ?>
            <form action="<?= $form_link ?>" method="GET" id="sac_form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Клиент:</label>
                        <ul class="nav nav-tabs" id="myTab1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-selected="true">Имя</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="false">Телефон</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent1">
                            <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
                                <input type="text" class="form-control" id="filter_client_name_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if($_GET['filter_client_name']) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                            </div>
                            <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                                <input type="text" class="form-control" id="filter_client_phone_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if($_GET['filter_client_name']) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                            </div>
                        </div>
                        <div id="client_variants_auth">
                            <ul class="list-group">

                            </ul>
                        </div>


                        <input type="hidden" name="client_id" id="filter_client_auth" value="">
                        <input type="hidden" name="manager_id" id="filter_manager_auth" value="<?= $_SESSION['manager_id'] ?>">
                        <input type="hidden" name="client_code" id="filter_client_code_auth" value="">
                        <input type="hidden" name="set_new_order" value="1">
                    </div>
                    <div class="form-group" id="client_auth_message">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="client_auth_form_button"><?=$apply?></button>
                </div>
            </form>
        <?php
        ob_get_flush();
    }
    // ###########################################################################################
    // Следующее условие для удаления заказа из хранилища по ID
    if (isset($_POST["deleteOrderById"])) {
        $incomingData = $_POST;
        $data = false;
        require_once DIR_HANDLERS . 'delete_order.php';
    }
    // ###########################################################################################



    print $data;
}





?>