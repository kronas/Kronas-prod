<?php
session_start();

include_once("func.php");

if(isset($_GET['check_client_phone']) && strlen($_GET['check_client_phone']) == 17) {
    $cp = getClientByPhone($_GET['check_client_phone'], 1);
    if(empty($cp)) echo 0;
    else {

        //.. 30.10.2022 Тоценко
        // Добавлено создание клиента на лету (если клиент уже был создан ранее в Акценте)
        // До правок здесь, в else был только echo 1;

        $cp = $cp[0];
        $query = 'SELECT * FROM `client` WHERE `code` = ' . $cp['ClientId'];
        $getClient = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['res'];

        $query = 'SELECT * FROM `client_code` WHERE `code_ac` = ' . $cp['ClientId'];
        $getClientCode = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['res'];

        if (0 == $getClient) {

            $client_create_query = "INSERT INTO `client` (`name`, `tel`, `e-mail`, `code`) VALUES ('" . $cp['ClientName'] . "','" . $cp['ClientPhone'] . "','" . $cp['ClientEmail'] . "','" . $cp['ClientId'] . "')";
            sql_data(__LINE__,__FILE__,__FUNCTION__, $client_create_query);

            if (0 == $getClientCode) {
                $client_code_create_query = "INSERT INTO `client_code`(`code_ac`, `name`) VALUES ('" . $cp['ClientId'] . "','" . $cp['ClientName'] . "')";
                sql_data(__LINE__,__FILE__,__FUNCTION__, $client_code_create_query);
            }

            $getClient = 2; // 2 - клиент создан из записи Акцента
        } else {
            //.. 09.12.2022 Тоценко
            // Бывает, что в Акценте меняют данные клиента, а ID остаётся тот же,
            // следующее условие меняет (обновляет) данные клиента в базе

            $query = 'SELECT * FROM `client` WHERE `code` = ' . $cp['ClientId'];
            $getClientTel = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['data'];
            $clientTel = null;
            foreach ($getClientTel as $key => $value) {
                if (isset($value['tel'])) {
                    $clientTel = $value['tel'];
                    break;
                }
            }

            $query = "SELECT * FROM `client` WHERE `tel` = '" . $cp['ClientPhone'] . "'";
            $clientByPhone = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['res'];

            if (0 == $clientByPhone) {
                $query = "UPDATE `client` SET `name` = '" . $cp['ClientName'] . "', `tel` = '" . $cp['ClientPhone'] . "', `e-mail` = '" . $cp['ClientEmail'] . "' WHERE `tel` = '" . $clientTel . "'";
                $clientUpdate = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['res'];

                // Этот запрос для дублирующихся записей клиентов (бывает две записи клиента, одна с телефоном, другая с почтой)
                $query = "UPDATE `client` SET `name` = '" . $cp['ClientName'] . "', `e-mail` = '" . $cp['ClientEmail'] . "' WHERE `code` = " . $cp['ClientId'];
                $clientDoubleUpdate = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['res'];

                $query = "UPDATE `client_code` SET `name` = '" . $cp['ClientName'] . "' WHERE `code_ac`= " . $cp['ClientId'];
                $clientCodeUpdate = sql_data(__LINE__,__FILE__,__FUNCTION__, $query)['res'];

                if (1 == $clientUpdate && 1 == $clientDoubleUpdate && 1 == $clientCodeUpdate) {
                    $getClient = 3; // 3 - данные клиента (имя, телефон, почта) обновлены, но "code" остался прежним
                }
            }
        }

        echo $getClient;
    }
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <link rel="stylesheet" href="/css/fontawesome/all.min.css">
    <title>Регистрация клиента (контрагента)</title>
    <style >
        .erase-tr{
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }
        .client_registration_form {
            max-width: 700px;
            margin: 25px auto 45px;
        }

        .client_registration_form h5 {
            margin-top: 25px;
        }

        .client_registration_form [type="submit"] {
            width: 100%;
            margin-top: 50px;
        }

        .footer {
          position: absolute;
          left: 0;
          bottom: 0;
          width: 100%;
          height: 50px;
        }

        .footer .p-3 {
          padding: 0.5rem 1rem!important;
        }

        .footer .p-3 a{
          font-weight: bold;
        }
    </style>


</head>
<body>
<?php
include_once DIR_CORE . "/func.php";

/**
 * Функция возвращает массив клиентов, отфильтрованных по части номера телефона + помещает массив записей в сессию.
 * $v - строка (часть номера телефона)
 * $p - количество возвращаемых записей
 * **/
function getClientByPhone($v, $p) {
    global $kronas_api_link;
    // http://v-webapp01.dn-kronas.local/kronasapi/gibLabService/FindPhoneFromAgents/097/30
    // Первый параметр - строка с номером телефона
    // Второй параметр - кол-во записей выдаваемых в ответ
    $v = str_replace(' ', '', $v);
    $v = preg_replace('#^\+?38#', '', $v);
    $v = preg_replace('#[(|)|\-|]#', '', $v);
    $url = $kronas_api_link . '/gibLabService/FindPhoneFromAgents/'.$v.'/'.$p;
    $out = array();
    $tmp = (array)json_decode(getParse($url));
    if ($tmp) {
        foreach ($tmp['data'] as $value) {
            $out[] = (array)$value;
        }
    } else $out = false;
    // return $url;
    return $out;
}
/**
* Парсинг из подготовленного url-а
**/
function getParse($url, $data = false) {
    $res = null;
    $a = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    if ($data) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST",);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"),);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data,);
    }
    curl_setopt($ch, CURLOPT_USERAGENT, $a);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $res = curl_exec($ch);
    curl_close($ch);
    return $res;
}

if($_POST['set_new_client'] == 1) {
    $place_data = explode('-', $_POST['client_city']);
    $client_city = $place_data[0];
    $client_region = $place_data[1];
    $client_title = $_POST['client_title'];
    $client_surname = $_POST['client_surname'];
    $client_name = $_POST['client_name'];
    $client_email = $_POST['client_email'];
    $client_phone = $_POST['client_phone'];

   

    $client_full_name = $client_surname.' '.$client_name;

    $cp = getClientByPhone($client_phone, 10);
    // if (!empty($cp)) {
    //     // [ClientId] => 149645
    //     // [ClientName] => Тестов Тест Тестович 
    //     // [ClientPhone] => +38(098)765-43-21
    //     // [ClientEmail] => test@tst.com
    //     foreach ($cp as $value) {
    //         if($value['ClientEmail'] == $client_email) {
    //             $message = '<h3 class="text-center mt-5">Клиент с почтой <b><i>'.$client_email.'</i></b> существует.</h3><br>
    //                   <p class="text-center mb-0">Клиент: '.$value['ClientName'].'</p>
    //                   <p class="text-center mb-0">Телефон: '.$value['ClientPhone'].'</p>
    //                   <p class="text-center mb-0">Код: '.$value['ClientId'].'</p><hr>';
    //         }
    //     }
    // } else {
        $code_set_new_sql = 'INSERT INTO `client_code`(`name`,`code_ac`) VALUES ("'.$client_title.'", '.mt_rand(11111111,99999999).')';
        $code_set_new = sql_data(__LINE__,__FILE__,__FUNCTION__,$code_set_new_sql)['id'];
        
        $json_request = '
            {
                "ExternalId": '.$code_set_new.',
                "Type": "fface",
                "Surname": "'.$client_surname.'",
                "FirstName": "'.$client_name.'",
                "TypeOfOwnerId": 2,
                "Phones": [
                    {"TypeId": 1, "Number": "'.$client_phone.'"},
                ],
                "Emails": [
                    {"TypeId": 1, "Mail": "'.$client_email.'"}
                ],
                "Activities":[
                    {"Id": 1}
                ],
                "Addresses":[
                    {
                        "TypeId": 2,
                        "RegionId": '.$client_region.',
                        "CityId": '.$client_city.'
                    }
                ]
            }
        ';

        $response = getParse($kronas_api_link . '/gibLabService/agentCreate', $json_request);
        if($response && !preg_match('#[Error]#', $response)) {

            $client_set_new_sql = 'INSERT INTO `client`(`name`, `tel`, `e-mail`, `code`, `pass`) VALUES ("'.$client_full_name.'","'.$client_phone.'","'.$client_email.'", '.$response.',"'.mt_rand(11111111,99999999).'")';
            $client_set_new = sql_data(__LINE__,__FILE__,__FUNCTION__,$client_set_new_sql)['res'];

            $client_update_new_sql = 'UPDATE `client_code` SET `code_ac`= '.$response.' WHERE `idcl` = '.$code_set_new;
            $client_update_new = sql_data(__LINE__,__FILE__,__FUNCTION__,$client_update_new_sql)['res'];

            $_POST = array();
            $message = '<h3 class="text-center mt-5">Контрагент успешно добавлен!</h3>';
        } else {
            if ($response) {
                $error = (array)json_decode($response);
                if (isset($error['Error'])) $error = '<h5>' . $error['Error'] . '</h5>';
                else $error = null;
            }
            else $error = null;
            $message = '<h3 class="text-center mt-5">Ошибка записи в Акцент!</h3>';
        }
    // }
}


$sql_city = 'SELECT `c`.`id` as `cid`, `r`.`id` as `r_id`, `c`.`name` as `c_name`, `r`.`name` as `r_name`, `c`.`code_ac` as `c_code`, `r`.`code_ac` as `r_code` FROM `city` as `c` LEFT JOIN `region` as `r` ON `c`.`region_id` = `r`.`id` ORDER BY `c`.`name`';
$city_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_city);

$sql_region = 'SELECT * FROM `region`';
$region_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_region);

$sql_ownership = 'SELECT * FROM `ownership_type`';
$ownership_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_ownership);

?>
<?php if (isset($message) && !empty($message)): ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= $message ?>
        </div>
    </div>
</div>
<?php else: ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Регистрация клиента (контрагента)</h1>
        </div>
    </div>
    <div class="row" id="main_phone_container" style="display: flex;flex-flow: row nowrap; justify-content: center;">
        <div style="width: 400px;margin-top: 45px;">
            <label for="">Введите телефон:</label>
            <input type="hidden" data-path="<?= $page[$currentPath]['webPath'] ?>" id="dataPath">
            <input type="hidden" data-path="<?= $page[$currentPath]['pathInfo'] ?>" id="redirectPath">
            <input type="tel" name="client_phone" class="form-control" id="client_phone" required style="width: 300px;">
            <div class="text-success" id="client_exists_in_accent"></div>
            <div class="text-danger" id="phone_check_result"></div>
        </div>
    </div>
    <div class="row" style="display: none" id="main_register_container">
        <form class="client_registration_form" action="<?= HOST_NAME ?>/client_registration.php" method="POST">
            <h5>Общие:</h5>
            <hr>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="">Город</label>
                    <select class="form-control" name="client_city" required>
                        <option selected disabled>---</option>
                        <?php foreach($city_data['data'] as $city): ?>
                            <option value="<?= $city['c_code'] ?>-<?= $city['r_code'] ?>"><?= $city['c_name'] ?> (<?= $city['r_name'] ?>)</option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="inputAddress">Наименование</label>
                    <input type="text" class="form-control" name="client_title" placeholder="" required>
                </div>
            </div>
            <h5>Контактное лицо:</h5>
            <hr>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="">Фамилия</label>
                    <input type="text" name="client_surname" class="form-control" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="">Имя</label>
                    <input type="text" name="client_name" class="form-control" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="">Телефон</label>
                    <input type="tel" class="form-control" id="client_phone_check" name="contact_phone" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="">E-mail:</label>
                    <input type="email" class="form-control" name="client_email">
                </div>
            </div>
            <input type="hidden" name="client_phone" class="form-control" id="client_phone_input" required>
            <button type="submit" class="btn btn-success" name="set_new_client" value="1">Зарегистрироваться</button>
        </form>
    </div>
</div>
<div class="empty-space-50"></div>
<?php endif ?>

<script>
    $(document).ready(function () {
        let dataPath = $('#dataPath').attr('data-path');
        let to = $('#redirectPath').attr('data-path');
        $('input[type="tel"]').mask('+38(000)000-00-00', {
            placeholder: "+38(0__)___-__-__"
        });
        $('#client_phone').on('input', function (e) {
            let phone = $(e.target).val();
            if(phone.length == 17) {
                $.get( dataPath + "?to=" + to + "&check_client_phone=" + phone, function( data ) {
                    // Если нет такого клиента...
                    // console.log(data)
                    if(data == 0) {
                        $('#phone_check_result').html('');
                        $('#main_phone_container').hide('slow');
                        $('#main_register_container').show('slow');
                        $('#client_phone_check').val($('#client_phone').val());
                        $('#client_phone_input').val($('#client_phone').val());
                    } else if (data == 2) {
                        $('#client_exists_in_accent').html('Клиент создан автоматичестки, так как уже был зарегистрирован в Акценте!');
                    } else if (data == 3) {
                        $('#client_exists_in_accent').html('Данные клиента (имя, телефон, почта) успешно обновлены, так как они были изменены в Акценте!');
                    } else {
                        $('#phone_check_result').html('Клиент с таким номером уже существует!');
                    }
                });
            }
        });
    });
</script>

<footer class="footer bg-light text-center text-lg-start">
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    <a href="<?= HOST_NAME ?>"><?= (preg_replace('#^(http[s]?:\/\/|www.)#', '', HOST_NAME)) ?></a>
  </div>
</footer>

</body>
</html>