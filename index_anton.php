<?php
date_default_timezone_set('Europe/Kiev');

session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

unset($_SESSION);
require_once '_1/config.php';
require_once DIR_CORE . 'func.php';
$session_id = $_GET['data_id'];

$data = get_temp_file_data($session_id);

if(isset($_SESSION['error_ok'])) {
    $error_ok = $_SESSION['error_ok'];
}

if(isset($_SESSION['error'])) $error.=$_SESSION['error'];
$_SESSION = $data;

//.. Восстанавливаем сессию
if (isset($session_restore['project_manager'])) {
    $_SESSION['project_manager'] = $session_restore['project_manager'];
}
if (isset($session_restore['project_client'])) {
    $_SESSION['project_client'] = $session_restore['project_client'];
}
if (isset($session_restore['user'])) {
    $_SESSION['user'] = $session_restore['user'];
}
if (isset($session_restore['tec_info'])) {
    $_SESSION['tec_info'] = $session_restore['tec_info'];
}

if(isset($_SESSION['error_ok'])) $error_ok.=$_SESSION['error_ok'];
if($_POST['first_project_data']) {
    $_SESSION['project_data'] = $_POST['first_project_data'];
}
$message = [];
$_SESSION["project_data"]=str_replace("qquot","quot",$_SESSION["project_data"]);
$_SESSION["project_data"]=str_replace("ququot","quot",$_SESSION["project_data"]);
$_SESSION["project_data"]=str_replace("quoquot","quot",$_SESSION["project_data"]);
$_SESSION["project_data"]=str_replace("quotquot","quot",$_SESSION["project_data"]);



$new_vals = get_vals_index($_SESSION["project_data"]);
$vals = vals_out($new_vals['vals']);
$index = make_index($new_vals['vals']);

//.. 17.02.2023 Тоценко
// Добавлены дополнительные проверки
require_once DIR_FUNCTIONS . 'fs_production_restrictions.php';
$error .= checkRestrictions($vals);
if (!empty($_SESSION['additions_errors_ok'])) {
    $error_ok .= $_SESSION['additions_errors_ok'];
}
if (!empty($_SESSION['vals_after_change'])) {
    $vals = $_SESSION['vals_after_change'];
    $_SESSION['vals'] = $_SESSION['vals_after_change'];
    $_SESSION['project_data'] = vals_index_to_project($vals);
}
unset($_SESSION['additions_errors_ok'], $_SESSION['vals_after_change']);

unset($check_error_part);
$check_index=1;
$timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'index_anton_start', $_SESSION);
// xml($vals);

//.. Восстанавливаем важные данные сессии перед вызовом check_production_possibilities.php
if (isset($session_restore['project_manager'])) {
    $_SESSION['project_manager'] = $session_restore['project_manager'];
}
if (isset($session_restore['project_client'])) {
    $_SESSION['project_client'] = $session_restore['project_client'];
}
if (isset($session_restore['user'])) {
    $_SESSION['user'] = $session_restore['user'];
}
if (isset($session_restore['tec_info'])) {
    $_SESSION['tec_info'] = $session_restore['tec_info'];
}

require_once DIR_CORE . 'check_production_possibilities.php';
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'index_anton_check_pr_pos 1 time without xnc', $_SESSION);
$new_vals = get_vals_index($_SESSION["project_data"]);
$error_cpp = $error_ok;
$vals = vals_out($new_vals['vals']);
$index = make_index($new_vals['vals']);
unset($check_index);
foreach ($vals as $k=>$v)
{
    if (isset($vals[$k]['attributes']['NAME']))$vals[$k]['attributes']['NAME']=htmlspecialchars($vals[$k]['attributes']['NAME']);
    if (isset($vals[$k]['attributes']['DESCRIPTION']))$vals[$k]['attributes']['DESCRIPTION']=htmlspecialchars($vals[$k]['attributes']['DESCRIPTION']);
}
if(!$error)
{
    $update_project = vals_index_to_project($vals);
    $project_data=$update_project;

    $message['step_1'] = 'Проверка деталей с фрезерными шаблонами.';
    $new_vals = get_vals_index($project_data);
    $vals1 = vals_out($new_vals['vals']);
    $index1 = make_index($new_vals['vals']);
    require_once DIR_CORE . 'check_shablon_production_possibilities.php';
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'index_anton_check_pr_pos template', $_SESSION);
    if(isset($check_error_part))
    {
        foreach ($check_error_part as $v12344)
        {
            $error.=$v12344["ERROR"];
        }
        unset ($v12344);
    }
    elseif ((!$check_error_part)AND($_SESSION["vals"])) {
        $message['step_1'] .= 'Ошибок не обнаружено!';
    } else {
        $message['step_1'] .= 'Что-то пошло не так';
    }
    $update_project = vals_index_to_project($vals1);
    $project_data=gl_recalc(__LINE__,__FILE__,__FUNCTION__,$update_project);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'index_anton_recal_finish', $_SESSION);
    $message['step_3'] = '<br><br><br><b>Проверка производственных ограничений и возможных ошибок в проекте. </b><br>';
    $new_vals = get_vals_index_without_session($project_data);
    // p_($data);
    $vals[0]['attributes']['project.user_type_edge']=$data["user_type_edge"];
    $vals=el_type_lines($vals,$data["user_type_edge"],$_SESSION);
    
    $vals_to_step=$vals;
    $vals = vals_out($new_vals['vals']);
    $index = make_index($new_vals['vals']);
    unset($check_index);
    if (isset($_SESSION['check_index'])) $check_index=1;
    require_once DIR_CORE . 'check_production_possibilities.php';
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'index_anton_check_pr_pos 2 time all', $_SESSION);
    $_SESSION['project_data']=vals_index_to_project($vals_to_step);
    $message['project_data'] = $_SESSION['project_data'];
    $message['step_1_errors'] = $error;
    // $message['step_1_errors_ok'] = (!isset($error) || empty($error)) ? $error_ok : null;
    $message['step_1_errors_ok'] = $error_ok;
    $message['step_3_errors_ok'] = null;
    $message['ssss'] = $error_cpp;
    echo json_encode($message);
}
else
{
    $message['step_1_errors'] = $error;
    // $message['step_1_errors_ok'] = (!isset($error) || empty($error)) ? $error_ok : null;
    $message['step_1_errors_ok'] = $error_ok;
    $message['step_3_errors_ok'] = null;
    echo json_encode($message);
}

# Нет необходимости вызывать session_destroy() в обычном коде. Очищайте массив $_SESSION вместо удаления данных сессии.
session_destroy();
?>




