<?php

require_once 'include.php';

$tmp = [];
$res = [];
$outputFile = __DIR__ . '/citiesJson/citiesJsonList.json';

$q = 'SELECT `c`.`id` as `cid`, `r`.`id` as `r_id`, `c`.`name` as `c_name`, `r`.`name` as `r_name`, `c`.`code_ac` as `c_code`, `r`.`code_ac` as `r_code` FROM `city` as `c` LEFT JOIN `region` as `r` ON `c`.`region_id` = `r`.`id` ORDER BY `c`.`name`';
$cities = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);

foreach ($cities['data'] as $value) {
	$tmp['city'] = $value['c_name'];
	$tmp['region'] = $value['r_name'];
	$tmp['cityCode'] = $value['c_code'];
	$tmp['regionCode'] = $value['r_code'];
	$res[] = $tmp;
}

$jsonRes = json_encode($res);

file_put_contents($outputFile, $jsonRes);

// da($cities);
da($jsonRes);

?>