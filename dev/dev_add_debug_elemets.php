<?php

// Вспомогательный скрипт для разработки (на продакшене не используется)
// Добавляет к каждому вызову функции доп. параметры (__FILE__, __LINE__, __FUNCTION__)


session_start();
include_once '../_1/config.php';
require_once '../func_prints.php';
include_once 'dev_settings.php';
require_once '../func_mini.php';

// В settings два массива с названиями функций:
// $all_f - функции файла func.php
// $all_fm - функции файла func_math.php

$sFolder = 'test/src/';
$dFolder = 'test/dest/';
$files = [];

$tmp = GetFileNames($serv_main_dir . 'dev/' . substr($sFolder, 0, -1), true);

$functions = $all_f;
foreach ($all_fm as $value) {
	$functions[] = $value;
}

foreach ($tmp as $value) {
	if (preg_match('#\.php$#', $value)) {
		$files[] = $value;
	}
}

$tmp = [];
foreach ($files as $file) {
	$tmp = file_get_contents($sFolder . $file);
	foreach ($functions as $function) {

		if (preg_match_all('#(.*[^if][^function ][^A-Za-z_])(' . $function . ' ?\()(\))#', $tmp, $matches)) {
				
				$tmp2 = preg_replace('(.*[^if][^function ][^A-Za-z_])(' . $function . ' ?\()(\))#', '${1}${2}__FILE__, __LINE__, __FUNCTION__${3}', $tmp);

				if (!empty($tmp2)) {
					$tmp = $tmp2;
				}

		} else if (preg_match_all('#(.*[^if][^function ][^A-Za-z_])(' . $function . ' ?\(.*[^__FILE__, __LINE__, __FUNCTION__])(\))#', $tmp, $matches)) {
			
				$tmp2 = preg_replace('#(.*[^if][^function ][^A-Za-z_])(' . $function . ' ?\(.*[^__FILE__, __LINE__, __FUNCTION__])(\))#', '${1}${2}, __FILE__, __LINE__, __FUNCTION__${3}', $tmp);
				
				if (!empty($tmp2)) {
					$tmp = $tmp2;
				}

			// ds($file);
			// ds($function);
			// ds('TWO');
			// da($matches);

		}
		file_put_contents($dFolder . $file, $tmp);


		// if (preg_match_all('#^(.*?ksort)(\(.*, __FILE__, __LINE__, __FUNCTION__)(\).*)#', $tmp, $matches)) {
		// 	ds($file);
		// 	ds($function);
		// 	da($matches);
		// } else ds($file . ' - No matches');
	}
}


?>