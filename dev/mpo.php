<?php
/**
* Скрипт переноса данных проекта (MY_PROJECT_OUT) из базы в файл в хранилище "files/project_out"
* Использовался только один раз, оставлен для истории.
**/
ini_set('memory_limit', '3096M');
ini_set('max_execution_time', '1800');// 30мин
include_once '../func_mini.php';

$q = 'SELECT `PROJECT_ID` FROM `PROJECT`';
$res = getTableData($q);
// $c = 0;
foreach ($res as $key => $value) {
	// if ($c < 10) {
		// _print($value);
		deleteMPO($value['PROJECT_ID']);
	// }
	// $c++;
}
?>