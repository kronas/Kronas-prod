<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
//header('Content-Type: application/json');
//session_start();

include_once ('func.php');


$session_id = $_GET['data_id'];

$data = get_temp_file_data($session_id);

$all_data = json_decode($_POST['data'], true);

$project_data = $all_data['project_data'];

// 08.08.2023 Тоценко В массив материала добавлен акцентовский артикул материала
// $materials = $all_data['materials'];
$materials = getAccentArticles($all_data['materials']);

$new_vals = get_vals_index($project_data);
$error_cpp = $error_ok;
$vals = vals_out($new_vals['vals']);
$index = make_index($new_vals['vals']);
$vals=put_material_client ($materials,$vals,$link);
$project_data=vals_index_to_project($vals);
$_SESSION['project_data'] = $project_data;

//print_r_($_SESSION['project_data']);
//exit;

put_temp_file_data($session_id, $_SESSION);
header("Location: ".$laravel_dir."/step_1?file=" . $session_id.'&nw='.$_GET['nw']);


function getAccentArticles($materials) {
	foreach ($materials as $key => &$value) {
		$q = "SELECT `CODE` FROM `MATERIAL` WHERE `MATERIAL_ID` = " . $value['id'];
		$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);

		if ($res['res'] === 1) {
			$value['accentArticle'] = $res['data'][0]['CODE'];
		} else $value['accentArticle'] = null;
	}

	return $materials;
}


?>

