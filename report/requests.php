<?php
function getAllservice_type(){
    $db = Database::getInstance();
    $mysqli = $db->getConnection();

    $sql = "SELECT * FROM `SERVICE_TYPE` ";

    $result = $mysqli->query($sql);

    return $db->loadAssocList($result);
}
function getAllservice(){
    $db = Database::getInstance();
    $mysqli = $db->getConnection();

    $sql = "SELECT * FROM `SERVICE` ";

    $result = $mysqli->query($sql);
	
    return $db->loadAssocList($result);
}
function getPost_data($arr_post){

    if (isset ($_POST['start_date'])&&isset ($_POST['end_date'])&&($_POST['userPassword']!='aA123456'))
    {
        echo ("<pre>");
        print_r("                       Неправильный пароль. Повторите ввод пароля или обратитесь к администратору");
        die;
    }
    
    if(empty($arr_post['choice'])){
        $choice = $arr_post['choice'];
        if($choice =="0" ){
            echo ("<pre>");
            print_r("                       Для начала выберите тип операции, операцию и отрезок времени для отчета");
            die;
        }
    }
    $type_id = $arr_post['type_operation'];    
    $operation_id = $arr_post['operation'];
    $start =  $arr_post['start_date'];
    $end = $arr_post['end_date'];
    if($start == ""  || $end == "" ) {
        echo ("<pre>");
        print_r("                                             выберите правильные даты");
        die;
    }
	$result = array([$start, $end, $type_id, $operation_id]); 
    return $result;
}
function getData($arr_post){
    // echo ("<pre>");
    // print_r($_POST);
    // die;
    if (isset ($_POST['start_date'])&&isset ($_POST['end_date'])&&($_POST['userPassword']!='aA123456')){
        print_r("                       Неправильный пароль. Повторите ввод пароля или обратитесь к администратору");
        die;
    }
    if(empty($arr_post['choice'])){
        $choice = $arr_post['choice'];
        if($choice =="0" ){
            print_r("                       Для начала выберите тип операции, операцию и отрезок времени для отчета");
            die;
        }
    }
    $type_id = $arr_post['type_operation'];    
    $operation_id = $arr_post['operation'];
    if (isset($arr_post['start_date']))$start =  $arr_post['start_date'];
    else $start='';
    if (isset($arr_post['end_date'])) $end = $arr_post['end_date'];
    else $end='';
    if($start == ""  || $end == "" ) {
        print_r("                                             выберите правильные даты");
        die;
    }
    $time =  $arr_post['time'];
    $db = Database::getInstance();
    $mysqli = $db->getConnection();

    $sql = "    SELECT  `ORDER1C`.`ID`, `PART_COMPLETE_CUT`.`DATE` as `DATE` , `ORDER1C`.`readonly` as `READONLY` , `PLACES`.`NAME` as `FILIAL` , `TOOL_CUTTING_EQUIPMENT`.`NAME` as `EQUIPMENT`, `PART`.`MY_MAT` as `MAT_CODE` , `PART`.`CL`, `PART`.`CW`, `PART`.`COUNT`,`PART_SERV`.`COUNT` as `COUNT_SERV` , `SERVICE`.`NAME` as `SERVICE_NAME`,  `SERVICE_TYPE`.`NAME` as `SERVICE_TYPE` , `PART`.`PART_ID` as `PART_ID` , `MATERIAL`.`MY_1C_NOM` as `MY_1C_NOM`, `MATERIAL`.`NAME` as `MATERIAL`
   
    FROM `PART_COMPLETE_CUT` 
    
    LEFT JOIN `PART` ON `PART`.`PART_ID` = `PART_COMPLETE_CUT`.`PART_ID`
    LEFT JOIN `ORDER1C` ON `PART`.`ORDER1C` = `ORDER1C`.`ID`
    LEFT JOIN `PLACES` ON `PLACES`.`PLACES_ID` = `ORDER1C`.`PLACE`
    LEFT JOIN `TOOL_CUTTING_EQUIPMENT` ON `TOOL_CUTTING_EQUIPMENT`.`ID` = `PART_COMPLETE_CUT`.`EQUIPMENT_ID`
    LEFT JOIN `PART_SERV` ON `PART_SERV`.`PART` = `PART`.`PART_ID`
    LEFT JOIN `SERVICE` ON `SERVICE`.`CODE` = `PART_SERV`.`SERV`
    LEFT JOIN `SERVICE_TYPE` ON `SERVICE`.`SERVICE_TYPE` = `SERVICE_TYPE`.`SERVICE_TYPE_ID`
    LEFT JOIN `MATERIAL` ON `MATERIAL`.`MATERIAL_ID` = `PART`.`MY_MAT`

    WHERE `ORDER1C`.`readonly` = 1 AND   `PART_COMPLETE_CUT`.`DATE` BETWEEN ' ".$start." 00:00:01' AND ' ".$end." 23:59:59'  
    AND  TIME_FORMAT(readonly_date, '%H') = '".$time."'  ";
    if($type_id != "99999" ) $sql .= "AND   `SERVICE_TYPE`.`SERVICE_TYPE_ID` = ".$type_id ." ";
    else $sql .= "AND   `SERVICE_TYPE`.`SERVICE_TYPE_ID` != ".$type_id ." ";
    if($operation_id != "99999" ) $sql .= "AND   `SERVICE`.`SERVICE_ID` = ".$operation_id ." ";
    else $sql .= "AND   `SERVICE`.`SERVICE_ID` != ".$operation_id ." ";
    //$sql .= " AND `readonly_date` BETWEEN '".$start." 00:00:01' AND '".$end." 23:59:59' ";
    // echo $sql;
    $result = $mysqli->query($sql);	
    return $db->loadAssocList($result);
}
function getDataService($arr_post){
    // echo ("<pre>");
    // print_r($_POST);
    // die;
    if (isset ($_POST['start_date'])&&isset ($_POST['end_date'])&&($_POST['userPassword']!='aA123456')){
    
        echo ("<pre>");
        print_r("                       Неправильный пароль. Повторите ввод пароля или обратитесь к администратору");
        die;
    }
    if(empty($arr_post['choice'])){
        $choice = $arr_post['choice'];
        if($choice =="0" ){
            echo ("<pre>");
            print_r("                       Для начала выберите тип операции, операцию и отрезок времени для отчета");
            die;
        }
    }
    $type_id = $arr_post['type_operation'];    
    $operation_id = $arr_post['operation'];
    if (isset($arr_post['start_date']))$start =  $arr_post['start_date'];
    else $start='';
    if (isset($arr_post['end_date'])) $end = $arr_post['end_date'];
    else $end='';
    if($start == ""  || $end == "" ) {
        echo ("<pre>");
        print_r("                                             выберите правильные даты");
        die;
    }
    $time =  $arr_post['time'];
    $db = Database::getInstance();
    $mysqli = $db->getConnection();

    $sql = "    SELECT  `ORDER1C`.`ID`, `ORDER1C`.`readonly` as `READONLY`,  `ORDER1C`.`readonly_date` as `READONLY_DATA`, `PLACES`.`NAME` as `FILIAL` ,
      `PART`.`MY_MAT` as `MAT_CODE` , `PART_SERV`.`COUNT` as `COUNT_SERV` , `SERVICE`.`NAME` as `SERVICE_NAME`,  `SERVICE_TYPE`.`NAME` as `SERVICE_TYPE` ,
      `MATERIAL`.`MY_1C_NOM` as `MY_1C_NOM`
   
    FROM `PART_COMPLETE_CUT` 
    
    LEFT JOIN `PART` ON `PART`.`PART_ID` = `PART_COMPLETE_CUT`.`PART_ID`
    LEFT JOIN `ORDER1C` ON `PART`.`ORDER1C` = `ORDER1C`.`ID`
    LEFT JOIN `PLACES` ON `PLACES`.`PLACES_ID` = `ORDER1C`.`PLACE`
    LEFT JOIN `TOOL_CUTTING_EQUIPMENT` ON `TOOL_CUTTING_EQUIPMENT`.`ID` = `PART_COMPLETE_CUT`.`EQUIPMENT_ID`
    LEFT JOIN `PART_SERV` ON `PART_SERV`.`PART` = `PART`.`PART_ID`
    LEFT JOIN `SERVICE` ON `SERVICE`.`CODE` = `PART_SERV`.`SERV`
    LEFT JOIN `SERVICE_TYPE` ON `SERVICE`.`SERVICE_TYPE` = `SERVICE_TYPE`.`SERVICE_TYPE_ID`
    LEFT JOIN `MATERIAL` ON `MATERIAL`.`MATERIAL_ID` = `PART`.`MY_MAT`

    WHERE `ORDER1C`.`readonly` = 1 AND   `ORDER1C`.`readonly_date` BETWEEN ' ".$start." 00:00:01' AND ' ".$end." 23:59:59'  
    AND  TIME_FORMAT(readonly_date, '%H') = '".$time."'   ";    
    if($type_id != "99999" ) $sql .= "AND   `SERVICE_TYPE`.`SERVICE_TYPE_ID` = ".$type_id ." ";
    else $sql .= "AND   `SERVICE_TYPE`.`SERVICE_TYPE_ID` != ".$type_id ." ";
    if($operation_id != "99999" ) $sql .= "AND   `SERVICE`.`SERVICE_ID` = ".$operation_id ." ";
    else $sql .= "AND   `SERVICE`.`SERVICE_ID` != ".$operation_id ." ";
    //$sql .= " AND `readonly_date` BETWEEN '".$start." 00:00:01' AND '".$end." 23:59:59' ";
    // echo $sql;
    $result = $mysqli->query($sql);	
    return $db->loadAssocList($result);
}
function getData_old($arr_post){

    if (isset ($_POST['start_date'])&&isset ($_POST['end_date'])&&($_POST['userPassword']!='aA123456'))
    {
        echo ("<pre>");
        print_r("                       Неправильный пароль. Повторите ввод пароля или обратитесь к администратору");
        die;
    }

    $type_id = $arr_post['type_operation'];    
    $operation_id = $arr_post['operation'];
    $start =  $arr_post['start_date'];
    $end = $arr_post['end_date'];
    if($start == ""  || $end == "" ) {
        echo ("<pre>");
        print_r("                                             выберите правильные даты");
        die;
    }
    $time =  $arr_post['time'];
    $db = Database::getInstance();
    $mysqli = $db->getConnection();
    
    $sql = "    SELECT  `PART_COMPLETE_UNKNOWN_CUT`.`ORDER1C` as ID , `PART_COMPLETE_UNKNOWN_CUT`.`date` as `DATES`, `PART_COMPLETE_UNKNOWN_CUT`.`l` as CL , `PART_COMPLETE_UNKNOWN_CUT`.`w` as CW , `PART_COMPLETE_UNKNOWN_CUT`.`count` as `COUNT`, `PLACES`.`NAME` as FILIAL, `TOOL_CUTTING_EQUIPMENT`.`NAME` as `EQUIPMENT`, `MATERIAL`.`MY_1C_NOM` as `MY_1C_NOM`, `MATERIAL`.`CODE` as `MAT_CODE`, `MATERIAL`.`NAME` as `MATERIAL`
   
    FROM `PART_COMPLETE_UNKNOWN_CUT` 
    
    LEFT JOIN `ORDER1C` ON `PART_COMPLETE_UNKNOWN_CUT`.`ORDER1C` = `ORDER1C`.`ID`
    LEFT JOIN `PLACES` ON `PLACES`.`PLACES_ID` = `ORDER1C`.`PLACE`
    LEFT JOIN `TOOL_CUTTING_EQUIPMENT` ON `TOOL_CUTTING_EQUIPMENT`.`ID` = `PART_COMPLETE_UNKNOWN_CUT`.`EQUIPMENT`
    LEFT JOIN `MATERIAL` ON `MATERIAL`.`CODE` = `PART_COMPLETE_UNKNOWN_CUT`.`MATERIAL`
    WHERE TIME_FORMAT(`date`, '%H') = '".$time."' AND `PART_COMPLETE_UNKNOWN_CUT`.`date` BETWEEN ' ".$start." 00:00:01' AND ' ".$end." 23:59:59'  
    AND  TIME_FORMAT(readonly_date, '%H') = '".$time."'  ";
    $result = $mysqli->query($sql);	
    return $db->loadAssocList($result);
}

?>