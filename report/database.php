<?php

class Database {
    private $_connection;
    // Store the single instance.
    private static $_instance;

    /**
     * Get an instance of the Database.
     * @return Database
     */
    public static function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->_connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
        $this->_connection->set_charset("utf8");
        // Error handling.
        if (mysqli_connect_error()) {
            trigger_error('Failed to connect to MySQL: ' . mysqli_connect_error(), E_USER_ERROR);
        }
    }

    /**
     * Empty clone magic method to prevent duplication.
     */
    private function __clone() {}

    /**
     * Get the mysqli connection.
     */
    public function getConnection() {
        return $this->_connection;
    }

    /**
     * Getting a value as an array list
     */
    public function loadAssocList($result)
    {
        $this->queryErrors($result);

        $array = array();


        // Get all of the rows from the result set.
        while ($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row;
        }

        // Free up system resources and return.
        $this->freeResult($result);

        return $array;
    }

    /**
     * Getting a value as an array
     */
    public function loadAssoc($result)
    {
        $this->queryErrors($result);
        $array = mysqli_fetch_assoc($result);
        // Free up system resources and return.
        $this->freeResult($result);

        return $array;
    }

    /**
     * Getting a value
     */
    public function loadResult($result)
    {
        $this->queryErrors($result);
        $array = mysqli_fetch_row($result);
        $array = $array[0];
        // Free up system resources and return.
        $this->freeResult($result);

        return $array;
    }

    /**
     * Clearing the result
     */
    public function freeResult($result)
    {
        mysqli_free_result($result);
    }

    /**
     * Clearing a value
     */
    public function clearValue($post)
    {
        foreach ($post as $key => $value){
            $post[$key] = $this->_connection->real_escape_string($value);
        }
        return $post;
    }

    /**
     * Display request error
     */
    public function queryErrors($result)
    {
        if (!$result) {
            printf("Сообщение ошибки: %s\n", $this->_connection->error);
            die;
        }
    }
}

?>