<?php $start = microtime(true);

require_once("config.php");
require_once("database.php");
require_once("requests.php");

$service_type = getAllservice_type();
$service = getAllservice();

?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Отчетность</title>
	
	<style type="text/css">
		 td{
			border: solid 1px black;
			padding-left: 5px;
		}
		.time{
			height: 20px;
		}
		.header {height:60px; 
			background-color:#717dc9;
			padding:20px; 
			text-align:center
		 }
		.footer {padding:10px 0 10px 20px; background-color:#717dc9; font-size:13px} 
		
		td{
			border: solid 1px black;
		}
		.time{
			height: 20px;
		}
		.header {height:60px; 
			background-color:#717dc9;
			padding:20px; 
			text-align:center
		}
		.footer {padding:10px 0 10px 20px; background-color:#717dc9; font-size:13px}
		.colorRow1{
			background: yellowgreen;
		}
		.colorRow2{
			background: violet;
		}
		.center{
			margin: auto;
		}
		.none{
			display: none;
		}
		.modal-lg, .modal-xl {
			max-width: 80%;
		}
		.btn-light {
			color: #212529;
			background-color: transparent;
			border-color: #f8f9fa;
		}


		.btn-light.focus, .btn-light:focus {
			color: #212529;
			background-color: transparent;
			border-color: #dae0e5;
			box-shadow: 0 0 0 0.2rem rgba(216,217,219,.5);
		}
		#userPassword{
			height: 45px;
			padding-left: 10px;
			width: 220px;
		}
		.btn{
			height: 50px;
		}
		
	</style>
</head>



<body class="pt-5">
<script>
	function selectCategory(obj){
			console.log(obj.value);
			if(obj.value == "99999"){
				operation_select = document.getElementById("operation");
				
				for (let i = 0; i < operation_select.length; i++) {
					if(operation_select[i].classList.contains("none"))
				 		operation_select[i].classList.remove("none"); 
				}
			} 
			else {
				operation_select = document.getElementById("operation");
				
				for (let i = 0; i < operation_select.length; i++) {
					if(operation_select[i].classList.contains("none"))
				 		operation_select[i].classList.remove("none"); 
				}
				for (let i = 0; i < operation_select.length; i++) {
					if (operation_select[i].getAttribute("data-type") != obj.value)
				 		operation_select[i].classList.add("none"); 
				}
				

			}
					
		}
</script>
	<div class="container">
		<div class="row justify-content-center">
			<form method="POST" action="index.php<?='?nw='.$_GET['nw']?>">
				<div class="form-group row">
					<label class="col-4 input-group-text mr-3" for="type_operation">Тип операции</label>
					<select class="custom-select col-7" id="type_operation" name="type_operation" onchange="selectCategory(this)">
							<option value="99999">Все типы операций</option>
						<?php foreach ($service_type as $value) { ?>
							<option value="<?= $value['SERVICE_TYPE_ID'] ?>"><?= $value['NAME'] ?></option>
						<?php } ?>

					</select>
				</div>
				<div class="form-group row">
					<label class="col-4 input-group-text mr-3" for="operation">Операция</label>
					<select class="custom-select col-7" id="operation" name="operation">
							<option value="99999">Все  операции</option>
						<?php foreach ($service as $value) { ?>
							<option value="<?= $value['SERVICE_ID'] ?>" data-type="<?= $value['SERVICE_TYPE'] ?>"><?= $value['NAME'] ?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="form-group row">
					<label for="start_date" class="col-4 input-group-text">Дата начала:</label>
					<div class="col-5">
						<input class="form-control" type="date" value="" id="start_date" name="start_date">
					</div>
				</div>
				
				<div class="form-group row">
					<label for="start_date" class="col-4 input-group-text">Дата окончания:</label>
					<div class="col-5">
						<input class="form-control" type="date" value="" id="end_date" name="end_date">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-4 input-group-text" for="userPassword">Пароль:</label>
					<div class="col-5">
						<input id="userPassword" name="userPassword" type="password">
					</div>
					<button type="submit" class="btn btn-primary mb-2" > Создать отчёт</button>
				</div>
				
				
				
				
			</form>
		</div>
	</div>
	
	
	<div class="modal fade" id="exampleModal" style="width: 100%;" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Расшифровка данных по ячейке</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
      </div>
    </div>
  </div>
</div>


	<table cellpadding="0" cellspacing="0" width="100%" align="center" id="table2">
	<tbody id="insert"></tbody>
</table>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

		

<?php
if(!empty($_POST)) {


		// $_POST['choice']="1";
		// $data = getData($_POST);
		// $json = json_encode($data);	
		$arrData = array();

		for ($i=0; $i < 24; $i++) { 
			if ($i<10) $k="0"; 
			else $k=""	;
			$_POST['choice']="1";
			$_POST['time']=$k.$i;
			$data = getData($_POST);

			$json = json_encode($data);	
			array_push($arrData, $json);
		}
		$arrData_service = array();

		for ($i=0; $i < 24; $i++) { 
			if ($i<10) $k="0"; 
			else $k=""	;
			$_POST['choice']="1";
			$_POST['time']=$k.$i;
			$data = getDataService($_POST);
			
			$json = json_encode($data);	
			array_push($arrData_service, $json);
		}

		//Для старого реестра
		$arrData_old = array();		
		for ($i=0; $i < 24; $i++) { 
		if ($i<10) $k="0"; 
		else $k=""	;
		$_POST['choice']="1";
		$_POST['time']=$k.$i;
		$data_old = getData_old($_POST);
		$json_old = json_encode($data_old);	
		array_push($arrData_old, $json_old);
		}

		//Для передачи дат
		$post_data=getPost_data($_POST);
		$json_post = json_encode($post_data);
		// echo ("<pre>");
        // print_r($arrData);
        // die;




}		
else {
	//$data = getData(array("type_operation"=>"1","operation"=>"141","start_date"=>date("Y-m-d"),"end_date"=>date("Y-m-d")));
	$data = getData(array("choice"=>"0"));
	$data_old = getData_old(array("choice"=>"0"));
	$json = json_encode($data);	
	$json_old = json_encode($data_old);	
}		
?>

<script type="text/javascript">

	let post_data= JSON.parse('<?php echo $json_post; ?>');

	console.dir(post_data)

	let arrFull=[];
	let arrFull_old=[];
	let arrFull_service=[];

	arrFull.push(JSON.parse('<?php echo $arrData[0]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[0]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[0]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[1]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[1]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[1]; ?>'));	
	arrFull.push(JSON.parse('<?php echo $arrData[2]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[2]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[2]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[3]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[3]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[3]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[4]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[4]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[4]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[5]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[5]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[5]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[6]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[6]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[6]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[7]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[7]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[7]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[8]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[8]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[8]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[9]; ?>'));	     arrFull_old.push(JSON.parse('<?php echo $arrData_old[9]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[9]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[10]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[10]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[10]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[11]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[11]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[11]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[12]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[12]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[12]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[13]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[13]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[13]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[14]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[14]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[14]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[15]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[15]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[15]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[16]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[16]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[16]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[17]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[17]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[17]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[18]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[18]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[18]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[19]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[19]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[19]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[20]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[20]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[20]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[21]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[21]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[21]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[22]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[22]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[22]; ?>'));
	arrFull.push(JSON.parse('<?php echo $arrData[23]; ?>'));	 arrFull_old.push(JSON.parse('<?php echo $arrData_old[23]; ?>'));	arrFull_service.push(JSON.parse('<?php echo $arrData_service[23]; ?>'));
	console.log("arrFull");
	console.dir(arrFull);
	console.log("arrFull_old");
	console.dir(arrFull_old);
	console.log("arrFull_service");
	console.dir(arrFull_service);


	
	let arrFullCombined =[];
	for(let n = 0; n < 24; n++){
	arrFullCombined[n] = [...arrFull[n], ...arrFull_old[n]];
	}

	console.log("arrFullCombined");
	console.dir(arrFullCombined);
	

	let perimeter, perimeter_old, perimeter_old_unknown, all_perimeter,all_perimeter_sum = 0; // периметры
	let sum_perimeter, sum_perimeter_old, sum_perimeter_old_unknown; // суммирующая переменная для периметров
	let area, area_old, area_old_unknown, all_area, all_area_sum = 0; //площади
	let all_services_sum = 0;  //сервисы
	// массив для хранения данных для popup по реестрам
	let arr_for_modal_reestr = new Array(24);
	for (let i = 0; i < arr_for_modal_reestr.length; i++) {		
		arr_for_modal_reestr[i] = new Array(20);
		for (let j = 0; j < arr_for_modal_reestr[i].length; j++) {
			arr_for_modal_reestr[i][j] = new Array(20);
			for (let k = 0; k < arr_for_modal_reestr[i][j].length; k++) {
			arr_for_modal_reestr[i][j][k]= new Array(10);
			}
		}
	}
	// массив для хранения данных для popup по услугам
	let arr_for_modal_service = new Array(24);
	for (let i = 0; i < arr_for_modal_service.length; i++) {		
		arr_for_modal_service[i] = new Array(20);
		for (let j = 0; j < arr_for_modal_service[i].length; j++) {
			arr_for_modal_service[i][j] = new Array(2);			
		}		
	}
	// формирование таблицы
	let insert = document.getElementById("insert");
	function uniqueForTime(arr) {  // подсчет уникальных строк для временного промежутка
		let result = [];		
		for (let elem of arr) {	
			if (!result.includes ([elem.FILIAL, elem.EQUIPMENT].toString())) {
			result.push([elem.FILIAL, elem.EQUIPMENT].toString());
			}
		}			
		return result;
	}
	function uniqueForFilial(arr) {  // подсчет уникальных строк для филиалов (их количества)
		let result = [];
		for (let elem of arr) {
			if (!result.includes ( elem.FILIAL )) {
			result.push(elem.FILIAL);
			}
		}
		//console.log("уникальные филиалы");
		//console.log(result);
		return result;
	}
	function uniqueEquipment(arr) {  // подсчет уникальных строк для филиалов (их количества)
		let result = [];
		let filName = [];		
		for (let elem of arr) {	
			if (!filName.includes ( elem.EQUIPMENT )) {
			filName.push(elem.EQUIPMENT);
			result.push(elem);
			}
		}		
		return result;
	}

	for(let z = 0; z < 24; z++){
	let arr =  arrFullCombined[z];
	let arr_old = arrFull_old[z];

	// динамическое построение таблицы
	if(z==0){
		//построение шапки документа
		insert.innerHTML += "<tr><td colspan='15' class='header' id='head_table' >Отчет за период  </td></tr>";
		
		let headerString = '\
		<tr>\
			<td class="time" rowspan="2">Временной отрезок</td>\
			<td rowspan="2">Филиал</td>\
			<td colspan="2">Принято услуг</td>\
			<td rowspan="2">оборудование</td>\
			<td colspan="3">новый реестр</td>\
			<td colspan="2">старый реестр</td>\
			<td colspan="2">неизвестно</td>\
			<td colspan="3">итого</td>\
		</tr>\
		<tr>\
			<td >наш материал</td>\
			<td >материал клиента</td>\
			<td >периметр</td>\
			<td >площадь</td>\
			<td >услуг</td>\
			<td >периметр</td>\
			<td >площадь</td>\
			<td >периметр</td>\
			<td >площадь</td>\
			<td >периметр</td>\
			<td >площадь</td>\
			<td >услуг</td>\
		</tr>';

		insert.innerHTML += headerString;
	}

	let string1="";	
	let colNum=1;
	let doubleNumCount; //переменная для правильного отображения цифр
	let arrayForCounting;
	let services; // количество услуг общее
	let services_our; // количество услуг из своего материала
	let services_client; // количество услуг из материала клиента
	
	let data_for_modal; // пееменная которая вносится в массив данных для отображения модального окна
	
					
					
	
		if(z%2 == 0 ) colNum = 1 ;
		else colNum =2;
		if(z<10) doubleNumCount = "0"; 
		else doubleNumCount = "";
		string1 +="<tr class='colorRow" + colNum + "'><td rowspan=" + uniqueForTime(arr).length + ">" +doubleNumCount+ "" + z + ":00 - " +doubleNumCount+ "" +(z+1) + ":00</td>";
		
		for ( let j = 0; j < uniqueForFilial(arr).length; j++) { //формирую филиалы в отрезке времени
			
			let count_fil =  arr.filter(function(el) { 	 return el.FILIAL == uniqueForFilial(arr)[j];})			
			
			count_fil_2 = uniqueEquipment(count_fil);  // фильтр строк чтобы не было дублеров
			
				string1 += "<td  rowspan=" + count_fil_2.length + " class='colorRow" + colNum + "' >" +count_fil_2[0].FILIAL+ "</td>";
				string1 += "<td id="+z+","+j+",1"+"  rowspan=" + count_fil_2.length + " class='colorRow" + colNum + " press1' data-toggle='modal' data-target='#exampleModal' > 0 </td>";
				string1 += "<td id="+z+","+j+",2"+" rowspan=" + count_fil_2.length + " class='colorRow" + colNum + " press1' data-toggle='modal' data-target='#exampleModal'> 0 </td>";
				
				for (let k=0; k<count_fil_2.length; k++) {
					arrayForCounting = count_fil.filter(function(el) { return  el.EQUIPMENT == count_fil_2[k].EQUIPMENT;});  // один станок, один филиал для расчетов
					// тут начинаем работать со строкой заполнения у определенного станка

					//Услуги 
					let sum_services;					
					let sum_services_Client;
					services = arrayForCounting
										.filter(function(el) {return  el.hasOwnProperty('PART_ID')})
										.map(el => sum_services = +el.COUNT ).reduce((a, b) => a + b, 0);


					data_for_modal = arrayForCounting
							.filter(function(el) {return  el.hasOwnProperty('PART_ID')});
					arr_for_modal_reestr[z][j][k][1] = arr_for_modal_reestr[z][j][k][2] = arr_for_modal_reestr[z][j][k][3]  = data_for_modal;

					// периметр
					perimeter = data_for_modal
							.map(el => sum_perimeter = ( +el.CW/1000 + +el.CL/1000 ) *2 * +el.COUNT)
							.reduce((a, b) => a + b, 0);									
					//площадь
					area = data_for_modal
							.map(el => sum_area = (+el.CW/1000 * +el.CL/1000 ) * +el.COUNT)
							.reduce((a, b) => a + b, 0);


					data_for_modal = arrayForCounting
							.filter(function(el) {return  !el.hasOwnProperty('PART_ID')})
							.filter(function(el) { return  !((el.ID == null) || (el.MAT_CODE == null)) });
					arr_for_modal_reestr[z][j][k][4] = arr_for_modal_reestr[z][j][k][5] = data_for_modal;
					// периметр  cтарый реестр 			
					perimeter_old = data_for_modal
							.map(el => sum_perimeter_old = ( +el.CW + +el.CL ) *2 * +el.COUNT)
							.reduce((a, b) => a + b, 0);
					//площадь  старый реестр
					area_old = data_for_modal
							.map(el => sum_area_old = (+el.CW * +el.CL ) * +el.COUNT).reduce((a, b) => a + b, 0);	



					data_for_modal = arrayForCounting
							.filter(function(el) {return  !el.hasOwnProperty('PART_ID')})
							.filter(function(el) { return  (el.ID == null) || (el.MAT_CODE == null)});
					arr_for_modal_reestr[z][j][k][6] = arr_for_modal_reestr[z][j][k][7] = data_for_modal;

					// периметр  cтарый реестр  неизвестные данные
					perimeter_old_unknown = data_for_modal
							.map(el => sum_perimeter_old = ( +el.CW + +el.CL ) *2 * +el.COUNT)
							.reduce((a, b) => a + b, 0);	
					//площадь  старый реестр неизвестные данные
					area_old_unknown = data_for_modal
							.map(el => sum_area_old = (+el.CW * +el.CL ) * +el.COUNT).reduce((a, b) => a + b, 0);	

					string1 += "<td  class='colorRow" + colNum + "'>"+count_fil_2[k].EQUIPMENT+"</td>";
					string1 += "<td id="+z+","+j+","+k+",1"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>"+(parseInt(perimeter * 100) / 100).toLocaleString()+"</td>";
					string1 += "<td id="+z+","+j+","+k+",2"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>"+(parseInt(area * 100) / 100).toLocaleString()+"</td>";
					string1 += "<td id="+z+","+j+","+k+",3"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>" +(parseInt(services * 100) / 100).toLocaleString()+ "</td>";
					string1 += "<td id="+z+","+j+","+k+",4"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>" +(parseInt(perimeter_old * 100) / 100).toLocaleString()+ "</td>";
					string1 += "<td id="+z+","+j+","+k+",5"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>"+(parseInt(area_old * 100) / 100).toLocaleString()+"</td>";
					string1 += "<td id="+z+","+j+","+k+",6"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>" +(parseInt(perimeter_old_unknown * 100) / 100).toLocaleString()+ "</td> ";
					string1 += "<td id="+z+","+j+","+k+",7"+"  class='colorRow" + colNum + " press' data-toggle='modal' data-target='#exampleModal'>"+(parseInt(area_old_unknown * 100) / 100).toLocaleString()+"</td> ";
					
					
					all_perimeter = perimeter + perimeter_old + perimeter_old_unknown;
					all_perimeter_sum += all_perimeter ;
					string1 += "<td id="+z+","+j+","+k+",8"+"  class='colorRow" + colNum + "' >"+(parseInt(all_perimeter * 100) / 100).toLocaleString()+"</td>";
					all_area = area + area_old + area_old_unknown ;					
					all_area_sum += +(parseInt( (  isNaN(all_area)   ? +0 : +all_area   )* 100) / 100); //all_area;					
					string1 += "<td id="+z+","+j+","+k+",9"+"  class='colorRow" + colNum + " ' >" +(parseInt(all_area * 100) / 100).toLocaleString()+ "</td> ";
					all_services_sum += services ;
					
					string1 += "<td id="+z+","+j+","+k+",10"+"  class='colorRow" + colNum + " ' >"+(parseInt(services * 100) / 100).toLocaleString()+"</td> ";
										
					insert.innerHTML += string1;
					string1="";	
				}
		}		
	};

	// вставка строки ИТОГО

	


	// для сохранения выбоора на странице
	document.getElementById("start_date").value =post_data[0][0];
	document.getElementById("end_date").value =post_data[0][1];
	document.getElementById("type_operation").value =post_data[0][2];
	document.getElementById("operation").value =post_data[0][3];
	document.getElementById("head_table").innerHTML ="Отчет за период  с "+post_data[0][0]+" по "+post_data[0][1];
	
	 console.log("arr_for_modal_reestr", arr_for_modal_reestr);
	 
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Заполнение полей по услугам ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	let temp = [];
	for (let i = 0; i < arr_for_modal_service.length/*24*/; i++) {		
		
		for (let j = 0; j < arr_for_modal_service[i].length/*20*/; j++) {

			temp = [];
			try {
				temp = arrFull_service[i]
					.filter(function(el) { return  (el.FILIAL == arr_for_modal_reestr[i][j][0][1][1].FILIAL)});
			} catch (error) {
				console.log("catch");
			}
			

		
			arr_for_modal_service[i][j][1] = temp.filter(function(el) { return  !(el.MY_1C_NOM == "5931")}); 
			arr_for_modal_service[i][j][2] = temp.filter(function(el) { return  el.MY_1C_NOM == "5931"});
		}
	}

	let ins=0, sum_ins1=0, sum_ins2=0;	

	for (let i = 0; i < arr_for_modal_service.length; i++) {		
		
		for (let j = 0; j < arr_for_modal_service.length; j++) {
		
			if(document.getElementById(i+","+j+","+"1")){
				
			ins = arr_for_modal_service[i][j][1].map(el => sum_services = +el.COUNT_SERV ).reduce((a, b) => a + b, 0)            	

			sum_ins1 += +(parseInt(ins * 100) / 100);
			ins = (parseInt(ins * 100) / 100).toLocaleString();
			
			document.getElementById(i+","+j+","+"1").innerHTML = ins;
			}
			if(document.getElementById(i+","+j+","+"2")){
			ins = arr_for_modal_service[i][j][2].map(el => sum_services = +el.COUNT_SERV ).reduce((a, b) => a + b, 0)
			sum_ins2 += +(parseInt(ins * 100) / 100);
			ins = (parseInt(ins * 100) / 100).toLocaleString();			
			document.getElementById(i+","+j+","+"2").innerHTML = ins;
			}
		}
	}
	

	let allSummaryString = '<tr><td colspan="2"></td><td id="it1">'+(parseInt(sum_ins1 * 100) / 100).toLocaleString()+'</td><td >'+(parseInt(sum_ins2 * 100) / 100).toLocaleString()+'</td><td colspan="8"></td><td >'+(parseInt(all_perimeter_sum * 100) / 100).toLocaleString()+'</td><td >'+(parseInt(all_area_sum * 100) / 100).toLocaleString()+'</td><td >'+(parseInt(all_services_sum * 100) / 100).toLocaleString()+'</td></tr>';
	insert.innerHTML += allSummaryString;
	insert.innerHTML+='<tr><td colspan="15" class="footer">&copy; Все права защищены</td></tr>';	

		

	// Вывод данных в модальное окно по реестрам
	let press = document.getElementsByClassName("press"); 
	Array.from(press).forEach((el, index) => el.addEventListener("click", function() {		
		let arr_id = el.id.split(",");
		let elem = arr_for_modal_reestr[arr_id[0]][arr_id[1]][arr_id[2]][arr_id[3]];
		console.dir(arr_id);
		let insert_part="";		
		for (let i = 0; i < elem.length; i++) {
		 	insert_part += "<div>"+ elem[i].EQUIPMENT+", "+" \
			 ORDER1C = "+elem[i].ID+", "+"\
			 PART_ID = "+elem[i].PART_ID+", "+"\
			 material = "+elem[i].MATERIAL+", "+" \
			 CW = "+elem[i].CW/1000+", "+" \
			 CL = "+elem[i].CL/1000+", "+" \
			 COUNT = "+elem[i].COUNT+", "+" \
			 Data : "+elem[i].DATE+"  \
			 Услуг: "+elem[i].COUNT_SERV+"</div>";

		}
	  document.getElementsByClassName("modal-body")[00].innerHTML =insert_part? insert_part : "По выбранной ячейке данных нет";
	}, false))

	// Вывод данных в модальное окно по услугам
	let press1 = document.getElementsByClassName("press1"); 
	Array.from(press1).forEach((el, index) => el.addEventListener("click", function() {		
		let arr_id = el.id.split(",");
		let elem = arr_for_modal_service[arr_id[0]][arr_id[1]][arr_id[2]];
		console.dir(arr_id);
		let insert_part ="";		
		for (let i = 0; i < elem.length; i++) {
		 	insert_part += "<div> ORDER1C = "+elem[i].ID+", "+"\
			 FILIAL = "+elem[i].FILIAL+", "+" \
			 SERVICE_NAME = "+elem[i].SERVICE_NAME+", "+" \
			 SERVICE_TYPE = "+elem[i].SERVICE_TYPE+", "+" \
			 Data : "+elem[i].READONLY_DATA+"  \
			 Услуг: "+elem[i].COUNT_SERV+"</div>";
		}
	  document.getElementsByClassName("modal-body")[00].innerHTML = insert_part? insert_part : "По выбранной ячейке данных нет";
	}, false))


</script>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>
