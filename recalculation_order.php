<?php
session_start();
include_once '_1/config.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/fontawesome/all.min.css">
    <title>Пересчитать/сохранить заказ KRONAS</title>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <style>
        .main{
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            overflow: auto;
        }
        .message-block{
            display: none;
            background: #eee;
            box-shadow: 0px 0px 20px #bbb;
            padding: 20px;
            border: 1px solid #bbb;
            width: 480px;
            height: 350px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -220px 0 0 -250px;
        }
        .message-block-show{
            display: block;
            animation: showBlock 1s linear forwards;
        }
        @keyframes showBlock {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }
        .message-block h4{
            text-align: center;
            margin-bottom: 70px;
        }
        .message-block p{
            text-align: center;
            margin-bottom: 90px;
        }
        .button-block #user_save_project{
            float: right;
        }
    </style>
    </head>
    <body>

        <div class="main">
            <div class="message-block">
                <h4>Потеряна связь с сервером!</h4>
                <p>Попробуйте снова пересчитать заказ, либо сохраните проект и попробуйте позже.</p>
                <div class="button-block">
                    <a href="<?= $main_dir ?>/check_sheet_parts.php?data_id=<?= $_SESSION['data_id'] ?>" type="button" class="btn btn-primary">Пересчитать</a>
                    <button type="button" class="btn btn-primary" id="user_save_project">Сохранить проект (.kronas)</button>
                </div>
            </div>
        </div>




        <script>
            $('#user_save_project').on('click', async function (e) {
                e.preventDefault();
                setTimeout(async function() {
                    let request = await fetch('<?= $main_dir ?>/ajax_save_kronas.php', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        body: 'save_kronas'
                    });
                    let body = await request.text();

                    let link = document.createElement('a');
                    link.setAttribute('href', body);
                    link.setAttribute('download','');
                    
                    onload = link.click();

                    location.href = '<?= $laravel_dir ?>';
                }, 1500);
            });

            document.addEventListener('DOMContentLoaded', function () {
                const messageBlock = document.querySelector('.message-block');
                setTimeout(() => {
                    messageBlock.classList.add('message-block-show');
                }, 700);
            }, false);
        </script>

    </body>