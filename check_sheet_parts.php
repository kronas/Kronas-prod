<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <link href="/css/fontawesome/all.min.css" rel="stylesheet">
    <title>Подтверждение заказа</title>
    <style >
        .erase-tr{
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }
        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }
        .loading-search{
            position: absolute;
            right: 22px;
            top: 3px;
            display: none;
        }
        #searchOnCode-box{padding: 10px;display: none;}
        #searchOnCode-box li{ cursor: pointer; }
        form{
            margin: 10px;
        }
        h3.success  {
            color: green;
        }
        body {
            padding: 45px;
        }
        #checks_lodaer_containers {
            position: fixed;
            top: 0;
            left: 0;
            background: rgba(255,255,255,0.4);
            width: 100%;
            height: 100%;
            display: none;
            z-index: 999999999;
        }
        #checks_lodaer_containers > div {
            width: 100%;
            display: flex;
            flex-flow: column;
            justify-content: center;
            align-items: center;
            z-index: 9999;
            height: 100%;
        }
        #checks_lodaer_containers > div > h1 {
            text-align: center;
        }
    </style>


</head>
<body>
<!-- Social networks -->
<script>
    const script = document.createElement('script');
    script.src = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.umd.js';
    script.onload = function () {
        const body = document.querySelector('body');
        const widgetEl = document.createElement('div');
        widgetEl.setAttribute('id', 'wt-omnichannel-widget');
        body.appendChild(widgetEl);            

        const config = {
            "view": {
                "lang": "en",
                "btnOpacity": "1",
                "logoUrl": "https://kronas.com.ua/Media/pic/icons/KR_logo_25x25px.svg",
                "accentColor": "hsl(292, 56.99999999999999%, 21%)"
            },
            "chat": {
                "url": "wss://cloud.webitel.ua/chat/hddvjbgfoitieuhtgvaygashqutmuan"
            },
            "alternativeChannels": {
                "viber": "viber://pa?chatURI=kronasbot",
                "telegram": "https://t.me/Kronascomua_bot",
                "facebook": "https://www.facebook.com/kronas.com.ua/"
            }
        };

        const app = new WtOmniWidget('#wt-omnichannel-widget', config);
    };
    document.head.appendChild(script);

    const link = document.createElement('link');
    link.href = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.css';
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.media = 'screen,print';
    document.head.appendChild(link);
</script>
<!-- Social networks -->
<?php

require_once '_1/config.php';
require_once DIR_CORE . 'func.php';
session_start();

//.. 30.07.2022 Правки, касательно цен. Если цены не получены (по API из Акцента),
// то $_SESSION['failed_prices'] устанавливается в 1 и здесь его нужно сбрасывать
if (isset($_SESSION['failed_prices'])) {
    unset($_SESSION['failed_prices']);
}

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

if ((isset($_GET["data_id"]))&&(!isset($_POST["stock_to"]))) 
{
//    echo "11";
    $_SESSION = get_temp_file_data($_GET["data_id"]);

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }

    //..23.02.2021 Добавлены проверки из index_anton
    $_SESSION['project_data'] = check_index_anton($_GET["data_id"]);
    $_SESSION["data_id"] = $_GET["data_id"];
    if (isset($_SESSION['error']))
    {
        put_temp_file_data($_SESSION["data_id"], $_SESSION);
        header("Location: ".$laravel_dir."/step_1?file=".$_SESSION["data_id"].'&nw='.$_GET['nw']);
    }

    

}

if ($_GET["data_d"]) {
    unset($_SESSION, $_POST);
    session_destroy();

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}

if (!isset($_POST["stock_to"]))
{
    // if (isset($_POST['band_change'])) p_(htmlspecialchars($_SESSION["project_data"]));
    
    $vals = get_vals($_SESSION["project_data"]);
    $index = make_index($vals);
    ksort($vals);

    //.. 11.08.2022 Проверка на кромки фирмы MAAG
    // Если есть такие, то запрещаем PUR-кромкование на странице подтверждения заказа
    // А если в редакторе убрали кромки фирмы MAAG, то удаляем из сессии ключ 'pur_edge_denied'
    unset($_SESSION['pur_edge_denied']);

    foreach ($index["GOOD"] as $i) {
        if ($vals[$i]['attributes']['TYPEID']=='band') {
            if (isset($vals[$i]["attributes"]["CODE"]) && (is_numeric($vals[$i]["attributes"]["CODE"]))) {
                $sql = "SELECT b.*, (SELECT VALUESTR FROM `GOOD_PROPERTIES_BAND` WHERE `BAND_ID` = b.`BAND_ID` and `PROPERTY_ID` = 151 LIMIT 1) as proizv FROM `BAND` b WHERE CODE = " . $vals[$i]["attributes"]["CODE"] . ";";
                $sql2 = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                // 17.07.2023 Тоценко. Закомментировано ограничение для PUR-кромкования при кромковании MAAG-кромкой
                // if (isset($sql2)) {
                //     if ($sql2["MY_LASER"] <> 1 AND $sql2['proizv'] == "MAAG") {
                //         $_SESSION['pur_edge_denied'] = 1;
                //     }
                // }
            }
        }
    }

    if (isset($_SESSION['pur_edge_denied'])) {
        $_SESSION["user_type_edge"] = 'eva';

        //.. Устанавливаем по всему проекту единый вид кромкования.
        foreach ($index["PART"] as $i) {
            if (isset($vals[$i]['attributes']['part.my_el_type'])) {
                $vals[$i]['attributes']['part.my_el_type'] = $_SESSION["user_type_edge"];
            }
        }
        foreach ($index["OPERATION"] as $i) {
            if (isset($vals[$i]['attributes']['MY_EL_TYPE'])) {
                $vals[$i]['attributes']['MY_EL_TYPE'] = $_SESSION["user_type_edge"];
                $vals[$i]['attributes']['operation.my_el_type'] = $_SESSION["user_type_edge"];
            }
        }
    }

    foreach ($index["OPERATION"] as $i) {
        if (isset($vals[$i]['attributes']['MY_EL_TYPE'])) {
            if ($vals[$i]['attributes']['MY_EL_TYPE'] == 'laz') {
                $_SESSION['laz_key'] = 1;
                break;
            }
        }
    }

    $vals[0]['attributes']['project.user_type_edge'] = $_SESSION["user_type_edge"];

    $vals=el_type_lines($vals,$_SESSION["user_type_edge"],$_SESSION);
    // p_($_SESSION["user_type_edge"]);
    // p_($vals);
    // exit;
    unset ($_SESSION["vals"]);
    $index=make_index($vals);
    ksort($vals);
    // if (isset($_POST['band_change'])) p_(htmlspecialchars($_SESSION["project_data"]));
    $edge_mass=edge_mass($vals,$link);
    if ((count($edge_mass)>0)&&(!isset($_POST['band_change'])&&(!isset($_POST['stock_to']))))
    {
        echo "
            <form method=\"post\" enctype='multipart/form-data' action=\"check_sheet_parts.php?data_id=".$_GET['data_id'].'&nw='.$_GET['nw']."\">";

        echo "<input type='submit' class='btn btn-success' value='Далее' id='check_sheet_parts_submit'>";

        foreach ($index["GOOD"] as $i)
        {
            if ($vals[$i]['attributes']['TYPEID']=='band')
            {
                foreach ($index['OPERATION'] as  $j)
                {
                    if (($vals[$j]['attributes']['TYPEID']=='EL')&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                    {
                        $band[$vals[$i]['attributes']['ID']]['COUNT']=ceil($vals[$j+1]['attributes']['COUNT']);
                        // p_($vals[$i]);
            
                        $k=$j+2;
                        while ($vals[$k]['tag']=="PART")
                        {
                            foreach ($index["PART"] as $p)
                            {
                                if (($vals[$p]['attributes']['CL']>0)&&($vals[$p]['attributes']['ID']==$vals[$k]['attributes']['ID']))
                                {
                                    $band_req[$vals[$i]['attributes']['CODE']]=$vals[$i]['attributes']['CODE'];
                                    if ($vals[$p]['attributes']['T']+3>$band[$vals[$i]['attributes']['ID']]['T'])
                                    {
                                        $band[$vals[$i]['attributes']['ID']]['T']=$vals[$p]['attributes']['T']+3;
                                        
                                    }
                                    $band[$vals[$i]['attributes']['ID']]['OP']=$vals[$j]['attributes']['ID'];

                                    $band_all[$vals[$i]['attributes']['CODE']]['NAME']=$vals[$i]['attributes']['NAME'];
                                    $band_all[$vals[$i]['attributes']['CODE']]['BAND_ID']=$vals[$i]['attributes']['BAND_ID'];
                                    foreach (SIDE as $s)
                                    {
                                        if ($vals[$p]['attributes']['EL'.mb_strtoupper($s)]=='@operation#'.$vals[$j]['attributes']['ID'])
                                        {
                                            unset ($s2);
                                            if (isset($band[$vals[$i]['attributes']['ID']]['part'][$vals[$p]['attributes']['ID']]['side'])) $s2=$band[$vals[$i]['attributes']['ID']]['part'][$vals[$p]['attributes']['ID']]['side'];
                                            $band[$vals[$i]['attributes']['ID']]['part'][$vals[$p]['attributes']['ID']]=$vals[$p]['attributes'];
                                            if (isset($s2)) $band[$vals[$i]['attributes']['ID']]['part'][$vals[$p]['attributes']['ID']]['side']=$s2;
                                            $band[$vals[$i]['attributes']['ID']]['part'][$vals[$p]['attributes']['ID']]['side'][$s]=1;
                                            
                                        }
                                    }
                                    
                                }
                            }
                            $band[$vals[$i]['attributes']['ID']]['BAND_ID']=$vals[$i]['attributes']['BAND_ID'];

                            $k++;
                        }
                    }
                }
            }
        }
        // p_($band);

        foreach ($edge_mass as $edge)
        {
            foreach ($edge['decor'] as $edg)
            {
                // p_($edg);exit;
            
                    $band_req[$edg['CODE']]=$edg['CODE'];
                    $band_all[$edg['CODE']]['NAME']=$edg['NAME'];
                    $band_all[$edg['CODE']]['BAND_ID']=$edg['BAND_ID'];
            
            }
            
        }
        // p_($band_all);
        // p_($band_req);
        // exit;
        if (($_SESSION['client_code']>0)&&($_SESSION['user_place']>0))
        {
            $band_get=get_actual_price($band_req, $_SESSION['user_place'], $_SESSION['client_code']);
        }
        
        if (count($band_get)>0)
        {
            foreach ($band_get as $c_srvvv=>$srvvv)
            {
                $band_all[$c_srvvv]['PRICE']=$srvvv["PRICE"];
            }
        }
        else
        {
            foreach ($band_all as $c_srvvv=>$sql2)
            {
                if (($sql2["BAND_ID"]>0)&&(is_numeric($sql2["BAND_ID"])))
                {
                    $sql4="SELECT * FROM BAND_PRICE WHERE BAND_ID=".$sql2["BAND_ID"]." AND PLACE_ID=".$_SESSION['user_place'].";";
                    $sql4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql4)['data'][0];
                    $sql4=check_price($sql4,"BAND",$link,$sql2["BAND_ID"]);
                    $band_all[$c_srvvv]['PRICE']=$sql4["PRICE"];
                }
            }
        }
        foreach ($band_all as $code=>$band1)
        {
            if (($band1['BAND_ID']>0)&&(is_numeric($band1['BAND_ID'])))
            {
                $band_pr[$band1['BAND_ID']]=array('name'=>$band1['NAME'],'price'=>$band1['PRICE']);
                $sql='SELECT * FROM BAND_STOCK WHERE BAND_ID='.$band1['BAND_ID'].';';
                $sql5=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                foreach ($sql5 as $sql4)
                {
                    
                    foreach ($band as $k=>$b)
                    {
                        // p_($b);
                        if ($b['BAND_ID']==$band1['BAND_ID'])
                        {
                            if ($sql4['PLACE_ID']==$_SESSION['user_place'])
                            {
                            
                                $band[$k]['pl_stock']=$sql4['COUNT'];
                            }
                            else
                            {
                                $band[$k]['stock']+=$sql4['COUNT'];
                            }
                        }
                    }
                    
                }
            }
        }
        // добавить расчёт количества кромки в операциях




        // xml_html ($vals);
        echo '<div class="container"><div class="row"><div class="col-md-12">';
        echo '<div class="kromka_zamena_container">';
        foreach ($index["GOOD"] as $i)
        {
            if (($band[$vals[$i]['attributes']['ID']]['COUNT']>0)&&($vals[$i]['attributes']['TYPEID']=='band')&&(isset($band[$vals[$i]['attributes']['ID']]))&&(isset($edge_mass[$vals[$i]['attributes']['ID']]['decor'])))
            {
                $band[$vals[$i]['attributes']['ID']]['KL']=$vals[$i]['attributes']['MY_CHECK_KL'];
                
                foreach ($edge_mass[$vals[$i]['attributes']['ID']]['decor'] as $id=>$edge)
                {
                    if ($edge['W']<$band[$vals[$i]['attributes']['ID']]['T']) $id_no[$id]=$id;
                }
                // p_($vals[$i]['attributes']['ID']);
                if ($band[$vals[$i]['attributes']['ID']]['KL']==1) $type_band='(криволинейное)';
                else $type_band='(прямолинейное)';
                $details_id_random = 'zxc'.mt_rand(11111111, 99999999);
                echo '<div class="kromka_zamena_row card card-body"><div class="kromka_zamena_row_detail"><div><h4><i>Кромка '.$type_band.':</i></h4></div><div><b>Название:</b> [/'.$vals[$i]['attributes']['CODE'].'/] '.$vals[$i]['attributes']['NAME'].'</div> 
                <div><b>Нужное количество:</b>  '.$band[$vals[$i]['attributes']['ID']]['COUNT'].' м (сумма <b><i>'.round($band_pr[$vals[$i]['attributes']['BAND_ID']]['price']*$band[$vals[$i]['attributes']['ID']]['COUNT'],2).'</b></i> грн)</div>
                <div><b>Остаток на складе:</b> '.$band[$vals[$i]['attributes']['ID']]['pl_stock'].'  м,</div><div><b>На остальных складах Компании:</b> '.$band[$vals[$i]['attributes']['ID']]['stock'].'  м.</div>
               
                <a class="btn btn-info" data-toggle="collapse" href="#'.$details_id_random.'" style="width: 100%; text-align:left; display: flex; flex-flow: row nowrap; align-items: center; justify-content: space-between;" role="button" aria-expanded="false" aria-controls="'.$details_id_random.'"><span>Детали:</span><span><i class="far fa-arrow-alt-circle-down"></i></span></a>';
                echo '<div class="collapse" id="'.$details_id_random.'"><div class="card card-body"><table>';
                foreach($band[$vals[$i]['attributes']['ID']]['part'] as $part)
                {
                    $e4=part_get_material($vals,$part["ID"]);
                    if (!isset($part['NAME']))$part['NAME'] ="Деталь без названия";
                    echo '<tr>
                    <p><b>
                    '.$part['NAME'].'</b></p>
                    <p>ID: '.$part['ID'].'</p> 
                    <p>Размеры: '.$part['CL'].' * '.$part['CW'].' * '.$part['T'].' ';
                    foreach ($part['side'] as $s=>$ss)
                    {
                        echo '|'.SIDE_T[$s].'|';
                    }
                    echo '</p>
                    <p>Материал:<br>'.$e4['attributes']['NAME'].'</p>
                    <hr>
                    </tr>
                    
                     ';
                }
                echo '</table></div></div></div>';
                echo '<div class="kromka_zamena_variants_container"><h4><i>Варианты замены:</i></h4><div class="kromka_zamena_variants">';
                $rr++;
                echo '<div class="kromka_zamena_variant form-group">
                    <div class="custom-control custom-radio" style="background: lightblue;">
                      <input type="radio" id="a'.$rr.'" name="band_change['.$vals[$i]['attributes']['ID'].']" class="custom-control-input" value="" checked>
                      <label for="a'.$rr.'">Не менять</label>
                    </div>
                </div>';
                foreach ($edge_mass[$vals[$i]['attributes']['ID']]['decor'] as $id=>$edge)
                {
        // p_($edge);
                    
                    if (!isset($id_no[$id]))
                    {
                        echo '<div class="kromka_zamena_variant form-group">
<div class="custom-control custom-radio">
                    <input type="radio" id="a'.$id.'" name="band_change['.$vals[$i]['attributes']['ID'].']" value="'.$edge['BAND_ID'].'" class="custom-control-input">
                        <label for="a'.$id.'">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><b>[/'.$edge['CODE'].'/] '.$edge['NAME'].'</b></li>
                                <li class="list-group-item">Cумма: <b><i>'.round($band_pr[$edge['BAND_ID']]['price']*$band[$vals[$i]['attributes']['ID']]['COUNT'],2).'</b></i> грн<br></li>
                                <li class="list-group-item">остаток на складе: '.($edge['stock'][$_SESSION['user_place']]/1).'  м, </li>
                                <li class="list-group-item"> на остальных складах Компании:'.(array_sum($edge['stock'])-$edge['stock'][$_SESSION['user_place']]).'  м.</li>
                            </ul>
                        </label>
                    </div>
                        </div>';
                    }
            
                }
                unset($id_no);
                echo '</div></div></div>';
            }
        }
        echo '</div>';
        echo "<input type='submit' class='btn btn-success'  value='Далее' id='check_sheet_parts_submit'></form>";
        echo <<<fgh
        <div id="checks_lodaer_containers">
        <div>
        <img src="{$laravel_dir}/images/download.gif" alt="">
        <h1>Идет пересчет проекта...</h1>
    </div>
    </div>
fgh;
        echo "<script>
        let button = document.getElementById('check_sheet_parts_submit');
        let loader = document.getElementById('checks_lodaer_containers');
        
        button.onclick = function(e) {
            
            loader.style.display = 'flex';
      };
    
      
    </script>";
        echo '</div></div></div>';
        echo '<style>
                .kromka_zamena_row {
                    display: flex;
                    flex-flow: row nowrap;
                    justify-content: flex-start;
                    margin-top: 35px;
                }
                .kromka_zamena_row > div {
                    width: 50%;
                    padding: 15px;
                    text-align: left;
                }
                .kromka_zamena_row_detail .btn-info {
                    margin-top: 45px;
                }
                .kromka_zamena_row_detail > div {
                    margin-bottom: 9px;
                }
                .kromka_zamena_row_detail > div > b {
                    font-size: 84%;
                }
                .kromka_zamena_row h4 {
                    margin-bottom: 19px;
                    border-bottom: 1px solid lightgray;
                    text-align: left;
                    padding-bottom: 9px;
                }
                .custom-control-input {
                    position: absolute;
                    left: 7px;
                    z-index: 1;
                    width: 1rem;
                    height: 1.25rem;
                    opacity: 1;
                }
                .custom-control {
                  position: relative;
                  display: block;
                  min-height: 1.5rem;
                  display: flex;
                  flex-flow: row nowrap;
                  align-items: center;
                  padding: 9px;
                  padding-left: 2.5rem;
                  border: 1px solid lightgray;
                  border-radius: 15px;      
                }
                .custom-control label {
                    margin-bottom: 0;
                    cursor: pointer;
                    
                }
                .list-group-flush li {
                    background: transparent;
                }
              </style>';


        echo <<<dfg
            <script>
                $('.custom-control-input').on('change', function(e) {
                  let radio = $(e.target);
                  let main_container = radio.closest('.kromka_zamena_variants');
                  main_container.find('.custom-control').css('background', 'transparent');
                  radio.closest('.custom-control').css('background', 'lightblue');
                });
            </script>
        dfg;

        // p_($band);

        $_SESSION['band']=$band;
        $_SESSION['vals']=$vals;

        put_temp_file_data($_GET["data_id"], $_SESSION);
        // p_($_SESSION['vals']);
        // p_(htmlspecialchars($_SESSION["project_data"]));
        exit;
        // [band_change] => Array
        // (
        //     [3] => 11635
        //     [5] => 11807
        //     [16] => 11635
        // )
        // p_($edge_mass);

    }
}
// p_($_POST);
if (isset($_POST['band_change']))
{
    $band=$_SESSION['band'];
    // p_($vals);
    // echo '<hr>';
    foreach ($_POST['band_change'] as $k=>$b_id)
    {
        if (($b_id>0)&&(is_numeric($b_id)))
        {
            unset ($b_in);
            foreach ($band as $id=>$b)
            {
                if ($b['BAND_ID']==$b_id)
                {
                    $b_in=$id;
                }
            }
    
            if (isset($b_in))
            {
                foreach ($band[$k]['part'] as $p_id=>$pp)
                {
                    foreach ($index['PART'] as $i)
                    {
                        if (($vals[$i]['attributes']['CL']>0)&&($vals[$i]['attributes']['ID']==$p_id))
                        {
                            foreach ($pp['side'] as $s=>$sss)
                            {
                                // p_($vals[$i]);
                                if (isset($vals[$i]['attributes']['EL'.mb_strtoupper($s)]))
                                {
                                    // p_($vals[$i]);
                                    $vals[$i]['attributes']['EL'.mb_strtoupper($s)]="@operation#".$band[$b_in]['OP'];
                                    
                                    // p_($vals[$i]);
                                    // p_($band[$b_in]);
                                    
                                    // exit;

                                }
                                
                            }
                            foreach ($index['OPERATION'] as $j)
                            {
                                unset ($part_in);
                                if (($vals[$j]['attributes']['TYPEID']=='EL')&&($vals[$j]['attributes']['ID']==$band[$b_in]['OP']))
                                {
                                    $jj=$j+2;
                                    while ($vals[$jj]['tag']=="PART")
                                    {
                                        if ($vals[$i]['attributes']['ID']==$vals[$jj]['attributes']['ID']) $part_in=1;
                                        $jj++;
                                    }
                                    if (!isset($part_in))
                                    {
                                        $ar_part_in=Array
                                        (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes"=> Array ("ID"=>$vals[$i]['attributes']['ID'])
                                        );
                                        $vals=move_vals($vals,$j+2,1);
                                        $vals[$j+2]=$ar_part_in;
                                        unset ($ar_part_in);
                                        ksort($vals);
                                    }
                                }
                            }
                            // xml ($vals);

                        }
                    }
                }
            }
            else
            {
                $arr['band_in'][0]=$b_id;
                $vals=put_source_in($vals,$link,$arr,$_SESSION['user_place']);
                $vals=vals_out($vals);
                ksort ($vals);
                $index=make_index($vals);
                unset ($op);
                foreach ($index['GOOD'] as $i)
                {
                    if (($vals[$i]['attributes']['TYPEID']=="band")&&($vals[$i]['attributes']['BAND_ID']==$b_id))
                    {
                        foreach ($index['OPERATION'] as  $j)
                        {
                            if (($vals[$j]['attributes']['TYPEID']=='EL')&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                            {
                                $op=$vals[$j]['attributes']['ID'];
                            }
                        }
                    }
                }
                foreach ($band[$k]['part'] as $p_id=>$pp)
                {
                    foreach ($index['PART'] as $i)
                    {
                        if (($vals[$i]['attributes']['CL']>0)&&($vals[$i]['attributes']['ID']==$p_id))
                        {
                            foreach ($pp['side'] as $s=>$sss)
                            {
                                // p_($vals[$i]);
                                if (isset($vals[$i]['attributes']['EL'.mb_strtoupper($s)]))
                                {
                                    // p_($vals[$i]);
                                    $vals[$i]['attributes']['EL'.mb_strtoupper($s)]="@operation#".$op;
                                    // p_($vals[$i]);
                                    // p_($op);
                                    
                                    // exit;

                                }
                            }
                            foreach ($index['OPERATION'] as $j)
                            {
                                unset ($part_in);
                                if (($vals[$j]['attributes']['TYPEID']=='EL')&&($vals[$j]['attributes']['ID']==$op))
                                {
                                    $jj=$j+2;
                                    while ($vals[$jj]['tag']=="PART")
                                    {
                                        if ($vals[$i]['attributes']['ID']==$vals[$jj]['attributes']['ID']) $part_in=1;
                                        $jj++;
                                    }
                                    if (!isset($part_in))
                                    {
                                        $ar_part_in=Array
                                        (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes"=> Array ("ID"=>$vals[$i]['attributes']['ID'])
                                        );
                                        $vals=move_vals($vals,$j+2,1);
                                        $vals[$j+2]=$ar_part_in;
                                        unset ($ar_part_in);
                                        ksort($vals);

                                    }
                                }
                            }
                        }
                    }
                }
            }
    
        }
    }
    // p_($vals);
    // p_($_POST['band_change']);
    $_SESSION['project_data']=vals_index_to_project($vals);
    $_SESSION['vals']=$vals;
    // p_($vals);exit;
    put_temp_file_data($_SESSION["data_id"], $_SESSION);
    $_SESSION = get_temp_file_data($_GET["data_id"]);
    
    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}
// p_($_SESSION);
// exit;
$timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'check_sheet_part start after edge', $_SESSION);


// p_($_SESSION);
// p_($_POST);
// exit;
// test(11,htmlspecialchars($_SESSION['project_data']));
if ($_SESSION["project_data"] && !isset($_POST["stock_to"])&&(!isset($sheet_parts)) )
{
    // include ('step1.php');
    // if (!$_SESSION["user_place"]) $place=1;
    // else $place = $_SESSION["user_place"];
    $_SESSION["project_data"]=get_out_part_stock($_SESSION["project_data"]);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_sheet_part get_out_stock', $_SESSION);

// p_($_SESSION["project_data"]);
    $_POST["project_data"] = $_SESSION["project_data"];
    $_SESSION['check_shhet_part']=1;
    $_SESSION["project_data"]=gl_55(__LINE__,__FILE__,__FUNCTION__,$_SESSION["project_data"]);
    unset($_SESSION['check_shhet_part']);
// p_($_SESSION["project_data"]);exit;
// $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_sheet_part giblab calc optimize', $_SESSION);


    get_vals_index($_SESSION["project_data"]);
    $vals = $_SESSION["vals"];
    unset ($_SESSION["vals"]);
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    // xml_html($vals);

    $_SESSION['vals']=$vals;
}
// p_($vals);exit;
if ((!isset($sheet_parts)) && !isset($_POST["stock_to"]))
{
 
    check_opt_part ($vals);
    // p_($vals);
// Блок "остатки в детали" начало 1 части

//     foreach ($index["PART"] as $i)
//     {
       
//         if ($vals[$i]["attributes"]["SHEETID"]>0)
//         {
//             $j=$i-1;
//             $j1=$i-1;

//             while (($vals[$j]["tag"]=="PART")&&($vals[$i]["attributes"]["SHEETID"]<>$vals[$j]["attributes"]["ID"]))
//             {
//                 $j--;
//             }
//             while ($vals[$j1]["attributes"]["TYPEID"]<>"sheet")
//             {
//                 $j1--;
//             }
// //j основной лист
// //j1 sheet

//                 // p_($vals[$j1]);
//                 // p_($vals[$j]);
//                 // p_($vals[$i]);
//                 if ($vals[$j1]['attributes']['CODE']>0)
//                 {
//                     $sql23="select * from MATERIAL where CODE=".$vals[$j1]['attributes']['CODE'];
//                     $sql32=mysqli_query($link,$sql23);
//                     $sql332=mysqli_fetch_array($sql32);
//                     $sql_e='SELECT * FROM MATERIAL_HALF where place_id='.$_SESSION['user_place'].' AND material_id='.$sql332['MATERIAL_ID'].';';
//                     $sql_e_q=mysqli_query($link,$sql_e);
//                     $sql_e_nr=mysqli_num_rows($sql_e_q);
//                     if ($sql_e_nr>0) $sql332['MY_HALF'] = 1;
//                             else $sql332['MY_HALF'] = 0;
//                     if (isset($sql332))
//                     {
                        
//                         unset ($no_part);
//                         if (($vals[$j]["attributes"]['MY_1C_NOM']<>5931)&&($vals[$j]["attributes"]["L"]==$vals[$i]["attributes"]["L"])&&($vals[$i]["attributes"]["W"]>=$vals[$j]["attributes"]["W"]/2-5)
//                         &&($sql332['MY_HALF']==1)&&($sql332['ST']<>1)&&(($vals[$i]["attributes"]["W"]-$vals[$j]["attributes"]["W"]/2-5)>299))
//                         {
//                                 $vals[$i]["attributes"]["W"]=$vals[$i]["attributes"]["W"]-$vals[$j]["attributes"]["W"]/2-5;
//                         }
//                         elseif (($vals[$j]["attributes"]['MY_1C_NOM']<>5931)&&($vals[$j]["attributes"]["L"]==$vals[$i]["attributes"]["L"])&&($vals[$i]["attributes"]["W"]>=$vals[$j]["attributes"]["W"]/2-5)
//                         &&($sql332['MY_HALF']==1)&&($sql332['ST']<>1)&&(($vals[$i]["attributes"]["W"]-$vals[$j]["attributes"]["W"]/2-5)<=300))
//                         {
//                             $no_part=1;
//                         }
//                         if (!isset($no_part))
//                         {
//                             $sheet_parts[$vals[$i]["attributes"]["ID"]]=
//                             array("ID"=>$vals[$i]["attributes"]["ID"],
//                             "L"=>round($vals[$i]["attributes"]["L"]-30),
//                             "W"=>round($vals[$i]["attributes"]["W"]-30),
//                             "T"=>round($sql332["T"],2),
//                             "COUNT"=>$vals[$i]["attributes"]["COUNT"],
//                             "CODE"=>$sql332["CODE"],
//                             "MAT_NAME"=>$sql332["NAME"],
//                             "USEDCOUNT"=>$vals[$i]["attributes"]["COUNT"],
//                             "NAME"=>"Остаток клиента (".$sql332["CODE"].") (".round($vals[$i]["attributes"]["L"]-30)."|".round($vals[$i]["attributes"]["W"]-30)."|".round($sql332["T"],2).
//                             ") ".$vals[$j]["attributes"]["NAME"],
//                             "index"=>$i);
//                             if (($sql332['ST']==1)&&($vals[$i]["attributes"]["W"]==$vals[$j]["attributes"]["W"]))
//                             {
//                                 $sheet_parts[$vals[$i]["attributes"]["ID"]]['W']=$vals[$i]["attributes"]["W"];
//                                 $sheet_parts[$vals[$i]["attributes"]["ID"]]['NAME']="Остаток столешницы клиента (".$sql332["CODE"].") (".round($vals[$i]["attributes"]["L"]-30)."|".round($vals[$i]["attributes"]["W"])."|".round($sql332["T"],2).
//                                 ") ".$vals[$j]["attributes"]["NAME"];
//                             }
//                         }
//                     }
//                 }
//                 // else $_POST["stock_to"]='no';
//         }
//     }
    // $_SESSION["sheet_parts"]=$sheet_parts;


// Блок "остатки в детали" окончание 1 части

    $_SESSION["vals"]=$vals;
    
    
}

// p_($sheet_parts);exit;

// Блок "остатки в детали" начало 2 части

// if (isset($sheet_parts)&&(!isset($_POST["stock_to"])))
// {
//     echo "
//     <h3>В проекте есть остатки: </h3>
//     <hr>
//     <table cellspacing='0' ce>
//     <thead>
//         <tr>
//             <td>Название</td>
//             <td>Материал</td>
//             <td>Длина(L)</td>
//             <td>Ширина(W)</td>
//             <td>Количество</td>
//             <td>Код</td>
//         </tr>
//     </thead>
//     <tbody>
//     ";
//     foreach ($sheet_parts as $item) {
//         echo "
//             <tr>
//                 <td>".$item['NAME']."</td>
//                 <td>".$item['MAT_NAME']."</td>
//                 <td>".$item['L']."</td>
//                 <td>".$item['W']."</td>
                
//                 <td>".$item['COUNT']."</td>
//                 <td>".$item['CODE']."</td>
//             </tr>

//         ";
//     }
//     echo "</tbody></table><hr>";   
//     echo "
//     <style>
//         table td {
//             border: 1px solid gray;
//             padding: 9px;
//             text-align: left;
//         }
//         table thead td {
//             font-weight: bold;
//         }
//         table tbody td {
//             opacity: 1;
//         }
//         table {
//             margin: 15px 0;
//             border-collapse: collapse;
//             font-style: normal;
//         }
//     </style>
//     ";
//     echo "
//     <form method=\"post\" enctype='multipart/form-data' action=\"check_sheet_parts.php?data_id=".$_GET['data_id']."\">
//     <input type=\"radio\" name=\"stock_to\" value=\"yes\" checked> Остатки в детали<Br>
//     <input type=\"radio\" name=\"stock_to\" value=\"no\"> не нужны<Br><hr>";
//     echo "<input type='submit' value='Далее' id='check_sheet_parts_submit' > 
//        </form>
//        <div id=\"checks_lodaer_containers\">
//     <div>
//         <img src=\"".$laravel_dir."/images/download.gif\" alt=\"\">
//         <h1>Идет пересчет проекта...</h1>
//     </div>
//     </div>
//     <style>
//     </style>
//     <script>
//         let button = document.getElementById('check_sheet_parts_submit');
//         let loader = document.getElementById('checks_lodaer_containers');
//         button.onclick = function(e) {
//             loader.style.display = 'flex';
//       };
//     </script>
//        ";
// }
// if (isset($sheet_parts)&&(!isset($_POST["stock_to"])))
// {
//     put_temp_file_data($_GET["data_id"], $_SESSION);
//     exit;
// }
// if (isset($_POST["stock_to"])) $_SESSION['stock_to']=$_POST["stock_to"];
$_SESSION['stock_to']='no';
$_POST['stock_to']='no';

// окончание блока, 2 строки выше убрать, остальное включить

if ($_POST["stock_to"]=="yes") $vals_parts=put_sheet_parts_into_project($_SESSION['vals'],$_SESSION['sheet_parts']);
// xml_html($_SESSION['vals']);

$vals=$_SESSION['vals'];
$vals_shablon=$vals;
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_sheet_part giblab before calc', $_SESSION);


$data_xml = vals_index_to_project($vals);
//p_ ($vals);
$data_xml=gl_recalc (__LINE__,__FILE__,__FUNCTION__,$data_xml);
$data=get_vals_index_without_session($data_xml);
$vals_recalc = $data["vals"];
// p_($_SESSION['vals']);exit;
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_sheet_part giblab calc_finish', $_SESSION);





// если хотим записать после депараметризации, убрать ниже комент
// $_SESSION["project_data"]=$array['project'];


// header('Content-type: text/xml');
// echo vals_index_to_project($vals);
// p_($_SESSION);exit;

include ("calculate.php");
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_sheet_part calculate end', $_SESSION);



// перегнать Vals в строку, убрать параметризацию, пересчитать оптимизация. Отдать $vals, с которым я далее работаю.
function check_index_anton($session_id) {
    global $main_dir;
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $main_dir.'/index_anton.php?data_id='.$session_id,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        'Cookie: PHPSESSID=o2e4aotc1qnmmac4hglqqj6kbc2vnltrbdcm13gu4qgvjscbrvu0783ea9kj1e2espo4s351ietg2n5dd2vgcbq8df1cihrumkua1sqsqfb3dtp8pbgnsbs9ft9k4ivb'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return json_decode($response, true)['project_data'];
}
?>


</body>
</html>