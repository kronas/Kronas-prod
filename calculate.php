<?php
require_once '_1/config.php';
require_once DIR_CORE . "description.php";
require_once DIR_CORE . "func.php";

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

if(isset($_GET['change_user_tool']) && $_GET['change_user_tool'] == 1) {
    // unset ($_SESSION);
    $_SESSION = get_temp_file_data($_GET["data_id"]);
    // p_($_SESSION);exit;

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
    
    $_SESSION["data_id"] = $_GET["data_id"];
    $vals = $_SESSION["vals"];
    $vals=add_tag_print($vals);
    $vals[0]['attributes']['project.user_type_edge']=$_SESSION["user_type_edge"];
    $vals=el_type_lines($vals,$_SESSION["user_type_edge"],$_SESSION);
    if ($vals[0]['attributes']['project.my_rs']==1)$_SESSION['rsorder']=1;
    $vals_shablon=$vals;
    $data_xml = vals_index_to_project($vals);
    $data_xml=gl_recalc (__LINE__,__FILE__,__FUNCTION__,$data_xml);
    $data=get_vals_index_without_session($data_xml);
    $vals_recalc = $data["vals"];
}

if(isset($_POST['cr_res']) )
{
    if(isset($_SESSION) || !isset($_SESSION["data_id"]))
    {
        session_destroy();
        session_start();
        unset($_SESSION);

        //.. Восстанавливаем сессию
        if (isset($session_restore['project_manager'])) {
            $_SESSION['project_manager'] = $session_restore['project_manager'];
        }
        if (isset($session_restore['project_client'])) {
            $_SESSION['project_client'] = $session_restore['project_client'];
        }
        if (isset($session_restore['user'])) {
            $_SESSION['user'] = $session_restore['user'];
        }
        if (isset($session_restore['tec_info'])) {
            $_SESSION['tec_info'] = $session_restore['tec_info'];
        }
        
        $_SESSION["data_id"]=session_id();
        $_SESSION["vals"]=array();
        $_SESSION["project_data"]='';
        $_GET["data_id"]= $_SESSION["data_id"];
    }
    $res=json_decode($_POST['cr_res'],true);
    // p_($res);exit;
    $res=unserialize (base64_decode($res));

    // if (!isset($_SESSION['user_place']))$_SESSION['user_place']=$res['user_place'];
    if (!isset($_SESSION['user_place']))$_SESSION['user_place']=$res['array_form']['user_place'];
    // p_($res);exit;
    $vals=$res['vals'];
    // xml ($vals);
    // p_($vals);
    $_SESSION['vals']=$vals;
    $_SESSION['rsorder']=1;
    // p_($_SESSION);exit;
}
else
{
    if (isset($_SESSION["user_place"])) $place=$_SESSION["user_place"];
    else $place=1;
    unset($res,$_SESSION["res"]);
    $res=calculate ($vals_shablon,$vals_recalc,$link,$place);
    $_SESSION['vals']=$res['vals'];
    if ($vals[0]['attributes']['project.my_rs']==1) $_SESSION['rsorder']=1;
    else $_SESSION['rsorder']=0;
}
// p_($res['vals']);exit;
$_SESSION['project_data']=vals_index_to_project($res['vals']);
$_SESSION['part']=$res['part'];
$_SESSION['mat_kl']=$res['mat_kl'];
$_SESSION['service_price']=$res['service_price'];
$_SESSION['simple_price']=$res['simple_price'];
$_SESSION['part_price']=$res['part_price'];
$_SESSION['material_price']=$res['material_price'];
$_SESSION['band_price']=$res['band_price'];
// $_SESSION["data_id"]=session_id();
$_SESSION["res"]=$res;
$serv_t=$_SESSION["res"]['serv_time'];
$price_tot = null;
foreach($res['service_price'] as $k=>$v)
{
    $price_tot+=$v['service_price']*$v['service_count'];
}
foreach($res['material_price'] as $k=>$v)
{
    $price_tot+=$v['sheet_price']*$v['sheet_count'];
}
if (!empty($res['band_price'])) {
    foreach($res['band_price'] as $k=>$v)
    {
        $price_tot+=$v['band_price']*$v['band_count'];
    }
}
if ((isset($_SESSION['stock_to']) && $_SESSION['stock_to']=="yes")&&(isset($_SESSION['sheet_parts'])))
{
    // exit ('11');
    $res=calculate ($_SESSION['vals_shablon'],$vals_parts,$link,$place);
    unset($price_tot_part);
    foreach($res['part_price'] as $k=>$v)
    {
        $price_tot_part+=$v['price']*$v['count'];
    }
    foreach($res['part_price'] as $k=>$v)
    {
        $res['part_price'][$k]['price']= round($res['part_price'][$k]['price']/$price_tot_part*$price_tot,2);
    }
    $_SESSION['vals']=$res['vals'];
    $_SESSION['mat_kl']=$res['mat_kl'];

    $_SESSION['project_data']=vals_index_to_project($res['vals']);
    $_SESSION['part']=$res['part'];
    $_SESSION['part_price']=$res['part_price'];
    // $_SESSION["data_id"]=session_id();
    $_SESSION["res"]=$res;
}
$_SESSION["res"]['serv_time']=$serv_t;

put_temp_file_data($_SESSION["data_id"], $_SESSION);

if (!isset($_SESSION['failed_prices']) || $_SESSION['failed_prices'] !== 1) {
    
    //..17.02.2021 На локальном сервере код не выполняется "Заголовки отправлены", поэтому такой фикс
    // if (stripos($_SERVER['SERVER_NAME'], '.loc') || stripos($_SERVER['SERVER_NAME'], '.local')) {
    //     echo '<meta http-equiv="refresh" content="0; url=/zzz.php?data_id='.$_GET["data_id"].'&nw='.$_GET['nw'].'">';
    // } else {
    //     header("Location: zzz.php?data_id=".$_GET["data_id"].'&nw='.$_GET['nw']);
    // }
    echo '<meta http-equiv="refresh" content="0; url=/zzz.php?data_id='.$_GET["data_id"].'&nw='.$_GET['nw'].'">';

} else {
    // p_($_SESSION);
    
    //.. 30.07.2022 Правки, касательно цен. Если цены не получены (по API из Акцента)
    // выводим ошибку и страницу с кнопками пересчёта и сохранения (файл calculate.php)
    // if (stripos($_SERVER['SERVER_NAME'], '.loc') || stripos($_SERVER['SERVER_NAME'], '.local')) {
    //     echo '<meta http-equiv="refresh" content="0; url=/recalculation_order.php?data_id='.$_GET["data_id"].'&nw='.$_GET['nw'].'">';
    // } else {
    //     header("Location: recalculation_order.php?data_id=".$_GET["data_id"].'&nw='.$_GET['nw']);
    // }
    echo '<meta http-equiv="refresh" content="0; url=/recalculation_order.php?data_id='.$_GET["data_id"].'&nw='.$_GET['nw'].'">';
}

?>