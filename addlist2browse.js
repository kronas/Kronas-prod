(function ($) {


    $(document).ready(function () {
        $('#submit').click(function(){

            var out={};

            $('input[id ^= check-]').each(function(i,elem) {
                if ($(this).prop('checked')) {
                    out[$(this).attr('id')] = $(this).val();
                }
            });

            out['filtr'] = 1;
            out['typeop'] = $('param#filtr-add-data').data('type');
//            out['browse'] = $('textarea#filtr-browse').val();
            out['browseparent'] = $('textarea#filtr-browse-parent').val();

            $.ajax({
                type: 'POST',
                url:'addlist2browse_resp.php',
                data:out,
                success: function (data, textStatus) {
                    var resp = jQuery.parseJSON(data);
                    $('.disp-4').html(resp.disp4);
                    $('.disp-5').html(resp.disp5);

                },    error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

        });
        $('#reset').click(function(){

            var out={};

            $("#accordionExample input[id ^= check-]").prop("checked", false);
            $("#selected-prop").html('');



            out['filtr'] = 1;
            out['typeop'] = $('param#filtr-add-data').data('type');
//            out['browse'] = $('textarea#filtr-browse').val();
            out['browseparent'] = $('textarea#filtr-browse-parent').val();

            $.ajax({
                type: 'POST',
                url:'addlist2browse_resp.php',
                data:out,
                success: function (data, textStatus) {
                    var resp = jQuery.parseJSON(data);
                    $('.disp-4').html(resp.disp4);
                    $('.disp-5').html(resp.disp5);

                },    error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

        });

        $(document).on('click', '#accordionExample input[id ^= check-]', function (event, sel) {
            var nameID = $(this).attr('id').substring(6);
            console.log(nameID);
            if ($(this).prop('checked')) {
                if (!$("#selected-prop").is("#selected-"+nameID))
                {
                    $("#selected-prop #selected-"+nameID).remove();
                    $("#selected-prop").append("<span id='selected-"+nameID+"' class='selected-check'><i class='close-sel'>x</i> "+$(this).val()+"</span>");
                }
            } else {
                $("#selected-prop #selected-"+nameID).remove();
            }

        });

        $(document).on('click', 'i,close-sel', function (event, sel) {
            var nameID = $(this).parent().attr('id').substring(9);
            $("#accordionExample input#check-"+nameID).prop("checked", false);
            $("#selected-prop #selected-"+nameID).remove();

        });


        $(document).on('click', '.list-group.mode-0  li', function (event, sel) {

            var clicked = $(this);
            var children = clicked.data('children');
            $('.list-group.mode-0 li').removeClass('active');
            $(this).addClass('active');
            $('.list-group.mode-1 li').hide();
            $('.list-group.mode-1 li[data-parent='+children+']').show();
        });
        $(document).on('click', '.list-group.mode-1  li', function (event, sel) {

            var breadcrumbs=$('.list-group.mode-0 li[data-children="'+$(this).data('parent')+'"]').html();
            $('.breadcrumbs').html('<button type="button" class="btn btn-secondary btn-back"><<< Назад</button>   '+ breadcrumbs+' > '+$(this).html());

            var out={};

            out['section'] = 1;
            out['target'] = $(this).data('target');
            out['parent'] = $(this).data('parent');
            $.ajax({
                type: 'POST',
                url:'addlist2browse_resp.php',
                data:out,
                            success: function (data, textStatus) {
                                var resp = jQuery.parseJSON(data);
                                $('.disp-0').hide();
                                $('.disp-1').hide();
                                $('.disp-2').show();
                                $('.disp-3-1').show();
                                $('.disp-4').show();
                                $('.disp-3').html(resp.disp3);
                                $('.disp-4').html(resp.disp4);
                                $('.disp-5').html(resp.disp5);

                },    error: function( jqXhr, textStatus, errorThrown ){
                                console.log( errorThrown );
                            }
            });

        });
        $(document).on('click', '.breadcrumbs .btn-back', function (event, sel) {
            $('#selected-prop').html('');
            $('.disp-3').html('');
            $('.disp-4').html('');
            $('.disp-2').hide();
            $('.disp-3-1').hide();
            $('.disp-4').hide();
            $('.disp-0').show();
            $('.disp-1').show();


        });
        $(document).on('click', '.popover', function (event, sel) {
            $( this ).popover('hide');
        });
        $(document).on({
            mouseenter: function(){
                $(this).popover('show');
                var obj = $(this);
                var code = obj.data('code');
                $('div.data-list-images').hide();
                $('div.images-'+code).show();
            },
            mouseleave: function(){
                $( this ).popover('hide');
            }
        }, 'div.data-list-line');

        $(document).on('click', '.pagination span', function (event, sel) {
            var out={};
            var li = $(this);
            var pagego = li.data('pagego');
            out['pager'] = pagego-1;
//            out['filter'] = filtr.val();
//            out['typeop'] = fadddata.data('typeop');
//            out['vart'] = fadddata.data('vart');7
            out['typeop'] = $('param#filtr-add-data').data('type');
            out['browse'] = $('textarea#filtr-browse').val();
            out['browseparent'] = $('textarea#filtr-browse-parent').val();

                        $.ajax({
                            type: 'POST',
                            url:'addlist2browse_resp.php',
                            data:out,
                            success:function (data, textStatus) {
                                var resp = jQuery.parseJSON(data);
                                $('.disp-4').html(resp.disp4);
                                $('.disp-5').html(resp.disp5);
                            }
                        });
        });

        $(document).on('click', '.disp-4 div.data-list-line', function (event, sel) {
            if ($(".tablerow.group-2 input").is('[value="' + $(this).data('code') + '"]')) {
                $('#selectPart').modal('hide');
            } else {

            var cost = $(this).data('cost');
            var recipient=$('#targetid').val();
            console.log(recipient);
                var tr_add='<tr class="tablerow group-2 addtable-tr" data-calc="3" ' +
                    ' data-img = "' + $(this).data('img') + '" data-ins = "' + $(this).data('addesc') + '" data-original-title="' + $(this).data('original-title') + '">' +
                    '<td>' + $(this).data('code') + '</td>'+
                    '<td>' + $(this).data('original-title') +
                    '<input type="hidden" name="simpleprice[]" value="' + $(this).data('code') + '">' +
                    '</td>' +
                    '<td class="count text-center" data-count="1"><input type="text" name="simplepricecnt[]" value="1"></td>' +
                    '<td class="price text-center" data-price="' + cost + '">' + number_format(cost, 2, '.', ' ') + '</td>' +
                    '<td class="total text-center">' + number_format(cost, 2, '.', ' ') + '</td>' +
                    '<td class="erase text-center"><span class="erase-tr"><i class="close-sel">x</i> Удалить</span></td>' +
                    '</tr>';
                if(parseInt(recipient)>0)
                {
                    $(".table-add-list tbody tr.tr-id-"+recipient).replaceWith(tr_add);
                }
                else
                {
                    if ($(".lastBlock").hasClass('hide')) {
                        $(".lastBlock").removeClass('hide')
                        $(".lastBlock").addClass('visible')
                    }
                    $(".table-add-list tbody").append(tr_add);
                }
            $('#selectPart').modal('hide');
            $("input#discountPercent2").keyup();

        }
        });


        function number_format(number, decimals, dec_point, thousands_sep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + (Math.round(n * k) / k)
                            .toFixed(prec);
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
                .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
                    .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
                    .join('0');
            }
            return s.join(dec);
        }

    });


})(jQuery);
