<?php

/**
 * ВРЕМЕННЫЙ СКРИПТ!!!
 * Скрипт, обратный скрипту "sort_project_files.php" находит все файлы в подпапках и копирует их в общую папку,
 * а из подпапки удаляет.
 * **/		

ini_set('memory_limit', '3096M');
ini_set('max_execution_time', '300');// 300 - 5мин // 1800 - 30мин

// require_once 'startup.php';
require_once 'func.php';
$config['storage_dir'] = $serv_main_dir . 'files/project_out';
session_start();

if(!isset($_SESSION['user']) || $_SESSION['user']['ID'] != 218) {
    header('Location: '.$main_dir.'/login.php'.'?nw='.$_GET['nw']);
	exit();
}
$c = 0;
$a = GetFilesNames($config['storage_dir'], true);

echo count($a['files']).'<br>';
echo count($a['folders']).'<br><br><hr>';

foreach ($a['folders'] as $val) {
	$v = GetFilesNames($config['storage_dir'].'/'.$val, true);
	foreach ($v['files'] as $value) {
		if (strripos($value, '.txt')) {
			if (!file_exists($config['storage_dir'].'/'.$value)) {
				if (!copy($config['storage_dir'].'/'.$val.'/'.$value, $config['storage_dir'].'/'.$value)) {
					echo 'Error: '.$config['storage_dir'].'/'.$val.'/'.$value.'<br>';
					$c++;
				} else {
					unlink($config['storage_dir'].'/'.$val.'/'.$value);
				}
			} else {
				unlink($config['storage_dir'].'/'.$val.'/'.$value);
			}
		}
	}
}

if (!$c == 0) {
	echo "Не удалось скопировать ".$c." файлов!<br>";
} else {
	echo "Файлы успешно скопированы!<br>";
}
$a = GetFilesNames($config['storage_dir'], true);
echo 'Файлов: '.count($a['files']).'<br>';
echo 'Папок: '.count($a['folders']).'<br>';


/**
* Получение названий каталогов и файлов в каталоге
**/
function GetFilesNames($dir, $flag=false){
	$result = null;
	$result = scandir($dir);
    $data = array();
	foreach ($result as $file){
    		if($file != '.' && $file != '..'){
       		$data[] = $file;
    		}
	}
	if (!$flag){return $data;}
	else {return onlyFiles($data);}
}
/**
* Функция очищает массив от каталогов "scandir"
**/
function onlyFiles($array){
	$result = [];
	foreach ($array as $value) {
		if (preg_match('#([.][a-z]{2,4}$)#', $value)) {
			$result['files'][] = $value;
		} else $result['folders'][] = $value;
	}
	return $result;
}
?>