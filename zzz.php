<?php
session_start();
require_once '_1/config.php';
require_once DIR_CORE . 'functions1.php';
require_once DIR_CORE . 'func_mini.php';
require_once DIR_CORE . 'settings.php';

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

// здесь сессия, затирается, поэтому сохраняем донные в переменную
if (isset($_SESSION['manager_id']) || isset($_SESSION['client_id'])) {
    $session_backup = array();
    if (isset($_SESSION['manager_id'])) {
        $session_backup['manager_id'] = $_SESSION['manager_id'];
        $session_backup['manager_name'] = $_SESSION['manager_name'];
        $session_backup['isAdmin'] = $_SESSION['isAdmin'];
    }

}
//.. Строка, когда оформляется новый заказ в кабинете менеджера
if (isset($_SESSION['client_id'])) {
    $client_id = $_SESSION['client_id'];
}

if(isset($_GET['client_search'])) {

    $client_sql = 'SELECT * FROM `client` WHERE `tel` = "+'.trim($_GET['client_search']).'"';
    $client_db = mysqli_query($link, $client_sql)->fetch_all();
    echo json_encode($client_db);
    exit;
}

ini_set('max_input_time', '0');
ini_set('max_input_vars', '190000');
ini_set('max_input_nesting_level', '164');
ini_set('post_max_size', '16M');
ini_set('output_buffering', '0');

require_once DIR_CORE . 'functions1.php';

    // потом уберём
    //include("calculate.php");
    if(!isset($_POST['back_giblab_without'])) {
        $_SESSION = get_temp_file_data($_GET["data_id"]);
        //.. Восстанавливаем данные юзера
        if (isset($session_backup['manager_id']) || isset($client_id)) {
            if (isset($session_backup['manager_id'])) {
                $_SESSION['manager_id'] = $session_backup['manager_id'];
                $_SESSION['manager_name'] = $session_backup['manager_name'];
                $_SESSION['isAdmin'] = $session_backup['isAdmin'];
            }
        }

        //.. Восстанавливаем сессию
        if (isset($session_restore['project_manager'])) {
            $_SESSION['project_manager'] = $session_restore['project_manager'];
        }
        if (isset($session_restore['project_client'])) {
            $_SESSION['project_client'] = $session_restore['project_client'];
        }
        if (isset($session_restore['user'])) {
            $_SESSION['user'] = $session_restore['user'];
        }
        if (isset($session_restore['tec_info'])) {
            $_SESSION['tec_info'] = $session_restore['tec_info'];
        }
    }

    //.. Строка, когда оформляется новый заказ в кабинете менеджера
    // восстанавливаем client_id в перезаписанную сееию
    if (isset($client_id)) {
        $_SESSION['client_id'] = $client_id;
    }

    if ($_SESSION['vals'][0]['attributes']['project.my_rs']>0 ||$_SESSION['vals'][0]['attributes'][mb_strtoupper('project.my_rs')]>0 )
    {
        $_SESSION['rsorder'] = 1;
        $_SESSION['user_place'] = 1;
        $_SESSION['user_type_edge'] = 'eva';
    }

    $script_for_out  = '<script>
        var user_place = "'.$_SESSION['user_place'].'";
        var user_type = "'.$_SESSION['user_type_edge'].'";
    </script>';
    
    if($_SESSION['order_status'] == 1) {
        $sql = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$_SESSION['order_1c_project'];
        $res = mysqli_query($link, $sql)->fetch_assoc()['readonly'];
        if($res) {
            $_SESSION['order_status'] = 2;
        }
    }

    //.. 15.02.2022 Если заказ существует, то цвет клея уже установлен, получим его:
    $glue_color = null;
    if(isset($_SESSION['order_1c_project']) && !empty($_SESSION['order_1c_project'])) {
        $q = 'SELECT * FROM `ORDER1C` WHERE `ID` = ' . $_SESSION['order_1c_project'];
        if($res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q)) {
            if ($res['res'] == 1) {
                $glue_color = $res['data'][0]['glue_color'];
            }
        }
    }

    //.. 16.02.2022 Создаём массив, в котором будут храниться операции кромкования.
    // Отображаем выбор цвета клея только тогда, когда есть операции кромкования
    // и только если кромкование не лазерное (а ПУР, либо EVA)
    // Glue Color Selector Display
    $gcsd = [
        'pvc' => 0,
        'laz' => isset($_SESSION['laz_key']) ? 1 : null
    ];
    if (isset($_SESSION['laz_key'])) unset($_SESSION['laz_key']);

    $arr_part_double = array('laz' => 'Лазер', 'pur' => 'ПУР', 'eva' => 'ЭВА');

    if(!$_SESSION['is_client']) {
        if(isset($_SESSION['client_code']) && $_SESSION['client_code'] && $_SESSION['client_code'] != '' && $_SESSION['client_code'] != null && !isset($_SESSION['is_client'])) {
            $_SESSION['is_client'] = 1;
            if($_SESSION['client_phone'] == '' || !isset($_SESSION['client_phone'])) $_SESSION['client_phone'] = '+33(333)333-33-33';
            if($_SESSION['client_email'] == '' || !isset($_SESSION['client_email'])) $_SESSION['client_email'] = 'z1@z1.z1';
        } else {
            $_SESSION['is_client'] = 0;
        }
    }

    if(!isset($_SESSION['is_manager'])) {
        if(isset($_SESSION['manager_id']) && $_SESSION['manager_id'] && $_SESSION['manager_id'] != '' && $_SESSION['manager_id'] != null) {
            $_SESSION['is_manager'] = 1;
        } else {
            $_SESSION['is_manager'] = 0;
        }
    }


    $place = $_SESSION["user_place"];
    $part_double = array('user_type_edge' => $_SESSION["user_type_edge"]);
    require_once DIR_CORE . 'func.php';

    $simple_price=$_SESSION['simple_price'];

    $part_price=$_SESSION['part_price'];
    $service_price=$_SESSION['service_price'];
    $band_price=$_SESSION['band_price'];
    $material_price=$_SESSION['material_price'];

    //de($service_price);

    if(isset($_POST['back_giblab']))
    {
        $vals = $_SESSION['vals'];
        $index = $_SESSION['index'];
        //////////////////////////////////////////////////////////
        $_SESSION['client_fio'] = $_POST['name'];
        $_SESSION['client_phone'] = $_POST['telephone'];
        $_SESSION['client_email'] = $_POST['email'];
        $_SESSION['client_code'] = $_POST['client_code'];

        if($_POST['client_code']) {
            $client_sql = 'SELECT * FROM `client` WHERE `client_id` ='.$_POST['client_code'];
            $client_db = mysqli_query($link, $client_sql)->fetch_array(MYSQLI_ASSOC);
            $_SESSION['client_phone'] = $client_db['tel'];
            $_SESSION['client_email'] = $client_db['e-mail'];
            $_SESSION['client_id'] = $_POST['client_code'];
            $_SESSION['client_code'] = $client_db['code'];
            $_SESSION['client_fio'] = $client_db['name'];
        }

        $_SESSION['order_comment'] = $_POST['order_comment'];

        $_simple_price=$_SESSION['simple_price'];
        $_simple_price_new=array();
        $_get_simple=array();
        $_get_simple_cnt=array();

        if(isset($_POST['simpleprice']))
        {
            foreach ($_POST['simpleprice'] as $_k=>$_v)
            {
                if((int) $_v!=0)
                {
                    $found=0;
                    foreach ($_simple_price as $_k2=>$_v2)
                    {
                        ///echo (int) $_v.'=='.(int) $_v2['Code'].'<br>';
                        if((int) $_v==(int) $_v2['Code'] && (int) $_v!=0)
                        {
                            $found=1;
                            $_v2['count']=$_POST['simplepricecnt'][$_k];
                            $_simple_price_new[]=$_v2;
                        }
                    }
                    if($found==0)
                    {
                        $_get_simple[]=(int) $_v;
                        $_get_simple_cnt[(int) $_v]=$_POST['simplepricecnt'][$_k];
                    }
                }
            }
            if(count($_get_simple)>0)
            {
                db_get_simple_price_new($_get_simple, $_simple_price_new,$_get_simple_cnt);
            }
        }

        $_SESSION['simple_price_extra']=$_simple_price_new;
        ////////////////////////////////////////////////////////////


        put_temp_file_data($_SESSION["data_id"], $_SESSION);
        header("Location: ".$laravel_dir."/step_1?file=" . $_SESSION["data_id"].'&nw='.$_GET['nw']);
        exit;

    }

    if(isset($_POST['back_giblab_without']))
    {

        $vals = $_SESSION['vals'];
        $index = $_SESSION['index'];
        //////////////////////////////////////////////////////////
        $_SESSION['client_fio'] = $_POST['name'];
        $_SESSION['client_phone'] = $_POST['telephone'];
        $_SESSION['client_email'] = $_POST['email'];
        $_SESSION['client_code'] = $_POST['client_code'];
        $_SESSION['order_comment'] = $_POST['order_comment'];

        if($_POST['client_code']) {
            $client_sql = 'SELECT * FROM `client` WHERE `client_id` ='.$_POST['client_code'];
            $client_db = mysqli_query($link, $client_sql)->fetch_array(MYSQLI_ASSOC);
            $_SESSION['client_phone'] = $client_db['tel'];
            $_SESSION['client_email'] = $client_db['e-mail'];
            $_SESSION['client_id'] = $_POST['client_code'];
            $_SESSION['client_code'] = $client_db['code'];
            $_SESSION['client_fio'] = $client_db['name'];
        }

        $_simple_price=$_SESSION['simple_price'];
        $_simple_price_new=array();
        $_get_simple=array();
        $_get_simple_cnt=array();

        if(isset($_POST['simpleprice']))
        {
            foreach ($_POST['simpleprice'] as $_k=>$_v)
            {
                if((int) $_v!=0)
                {
                    $found=0;
                    foreach ($_simple_price as $_k2=>$_v2)
                    {
                        ///echo (int) $_v.'=='.(int) $_v2['Code'].'<br>';
                        if((int) $_v==(int) $_v2['Code'] && (int) $_v!=0)
                        {
                            $found=1;
                            $_v2['count']=$_POST['simplepricecnt'][$_k];
                            $_simple_price_new[]=$_v2;
                        }
                    }
                    if($found==0)
                    {
                        $_get_simple[]=(int) $_v;
                        $_get_simple_cnt[(int) $_v]=$_POST['simplepricecnt'][$_k];
                    }
                }
            }
            if(count($_get_simple)>0)
            {
                db_get_simple_price_new($_get_simple, $_simple_price_new,$_get_simple_cnt);
            }
        }

        $_SESSION['simple_price_extra']=$_simple_price_new;

        ////////////////////////////////////////////////////////////
        $_SESSION['project_data'] = vals_index_to_project($vals);
        $_SESSION['project_data']=get_out_part_stock($_SESSION['project_data']);

        put_temp_file_data($_SESSION["data_id"], $_SESSION);
        header("Location: ".$laravel_dir."/step_1?file=" . $_SESSION["data_id"].'&nw='.$_GET['nw']);
        exit;

    }
    elseif($_POST['order_confirm']>0) {

        $_SESSION['client_fio'] = $_POST['name'];
        $_SESSION['client_phone'] = $_POST['telephone'];
        $_SESSION['client_email'] = $_POST['email'];
        $_SESSION['client_code'] = $_POST['client_code'];
        $_SESSION['order_comment'] = $_POST['order_comment'];
        $_SESSION['manager_id'] =  $_POST['manager_id'];
        $_SESSION['glue_color'] =  $_POST['glue_color'];

        if($_POST['client_code']) {
            $client_sql = 'SELECT * FROM `client` WHERE `client_id` ='.$_POST['client_code'];
            $client_db = mysqli_query($link, $client_sql)->fetch_array(MYSQLI_ASSOC);
            $_SESSION['client_phone'] = $client_db['tel'];
            $_SESSION['client_email'] = $client_db['e-mail'];
            $_SESSION['client_id'] = $_POST['client_code'];
            $_SESSION['client_code'] = $client_db['code'];
            $_SESSION['client_fio'] = $client_db['name'];
        }

        $_SESSION['manager_addition_place'] = $_POST['manager_addition_place'];
        if($_POST['change_palce_id'] != $_SESSION['user_place'] || ($_POST['change_tool_id'] != null && $_POST['change_tool_id'] != $_SESSION['user_type_edge'])) {

            $_SESSION['user_place'] = $_POST['change_palce_id'];
            $_SESSION['user_type_edge'] =  $_POST['change_tool_id'];
            $_SESSION['change_project_tool_places'] = 1;

            $_simple_price=$_SESSION['simple_price'];
            $_simple_price_new=array();
            $_get_simple=array();
            $_get_simple_cnt=array();

            if(isset($_POST['simpleprice'])) {
                foreach ($_POST['simpleprice'] as $_k=>$_v) {
                    if((int) $_v!=0) {
                        $found=0;
                        foreach ($_simple_price as $_k2=>$_v2) {
                            if((int) $_v==(int) $_v2['Code'] && (int) $_v!=0) {
                                $found=1;
                                $_v2['count']=$_POST['simplepricecnt'][$_k];
                                $_simple_price_new[]=$_v2;
                            }
                        }
                        if($found==0)
                        {
                            $_get_simple[]=(int) $_v;
                            $_get_simple_cnt[(int) $_v]=$_POST['simplepricecnt'][$_k];
                        }
                    }
                }
                if(count($_get_simple)>0) {
                    db_get_simple_price_new($_get_simple, $_simple_price_new,$_get_simple_cnt);
                }
            }

            $_SESSION['simple_price_extra']=$_simple_price_new;
            unset($_SESSION['material_price'],$_SESSION['band_price'],$_SESSION['service_price'],
                $_SESSION['part'],$_SESSION['part_price'],$_SESSION["res"],$_SESSION["stock_to"],$_SESSION["sheet_parts"]);

            put_temp_file_data($_SESSION["data_id"], $_SESSION);
            header("Location: calculate.php?change_user_tool=1&data_id=".$_GET["data_id"].'&nw='.$_GET['nw']);
            exit("<meta http-equiv='refresh' content='0; url= /calculate.php?change_user_tool=1&data_id=".$_GET['data_id']."'>");
            exit;
        }

        $_simple_price=$_SESSION['simple_price'];
        $_simple_price_new=array();
        $_get_simple=array();
        $_get_simple_cnt=array();

        if($_POST['manager_id'] == 'store') {
            $_SESSION['manager_id'] = 39;
        } else {
            $_SESSION['manager_id'] = $_POST['manager_id'];
        }


        if(isset($_POST['simpleprice']))
        {

            foreach ($_POST['simpleprice'] as $_k=>$_v)
            {
                if((int) $_v!=0)
                {
                    $found=0;
                    foreach ($_simple_price as $_k2=>$_v2)
                    {
                        if((int) $_v==(int) $_v2['Code'] && (int) $_v!=0)
                        {
                            $found=1;
                            $_v2['count']=$_POST['simplepricecnt'][$_k];
                            $_simple_price_new[]=$_v2;
                        }
                    }
                    if($found==0)
                    {
                        $_get_simple[]=(int) $_v;
                        $_get_simple_cnt[(int) $_v]=$_POST['simplepricecnt'][$_k];
                    }
                }
            }
            if(count($_get_simple)>0)
            {
                db_get_simple_price_new($_get_simple, $_simple_price_new,$_get_simple_cnt);
            }
        }
        $_SESSION['simple_price_extra']=$_simple_price_new;
        put_temp_file_data($_SESSION["data_id"], $_SESSION);

        header("Location: confirm.php?data_id=".$_SESSION["data_id"].'&nw='.$_GET['nw']);
        echo 'ddddddd';
        exit;
    }

    //.. Если переход на "create.php" "rs_detail.php" или giblab
    // st_ - STorage - приставка для сохранения 'project_data' в формате для отображения в "rs_detail.php" и "create.php"
    // gl_ - GibLab - приставка для сохранения 'project_data' в формате для отображения в "giblab"
    // ci_ - ClientId - приставка для сохранения "client_id"
    $storage = $serv_main_dir.'/files/temp/';
    $project_id = null;
    $giblab_project_id = null;
    $project_data = null;
    $giblab_project_data = null;
    foreach ($_SESSION['vals'] as $value) {
        if (!$project_id && $value['tag'] && $value['tag'] = 'PROJECT') {
            $project_id = 'st_'.$value['attributes']['DATE'];
            $giblab_project_id = 'gl_'.$value['attributes']['DATE'];
            if (isset($_SESSION['client_id'])) {
                $client_id = 'ci_'.$value['attributes']['DATE'];
            }
            $client_id = 'ci_'.$value['attributes']['DATE'];
        }
        if (!$project_data && $value['tag'] && $value['tag'] = 'RS_DATA') {
            $project_data = $value['attributes']['ARRAY'];
            $giblab_project_data = $value['attributes']['ARRAY'];
        }
    }
    $giblab_project_data = vals_index_to_project($_SESSION['vals']);
    // Записываем project_data
    if ($file = fopen($storage.$project_id.'.tmp', 'w')) {
        fwrite($file, $project_data);
        fclose($file);
    } else echo 'Не удалось открыть файл проекта для записи.';
    // Записываем project_data для giblab
    if ($file = fopen($storage.$giblab_project_id.'.tmp', 'w')) {
        fwrite($file, $giblab_project_data);
        fclose($file);
    } else echo 'Не удалось открыть файл giblab проекта для записи.';
    // Получаем $client_id: если есть - пишем в файл, если нет берём из файла
    if(isset($_SESSION['client_id'])) {
        if ($file = fopen($storage.$client_id.'.tmp', 'w')) {
            fwrite($file, $_SESSION['client_id']);
            fclose($file);
            $client_id = $_SESSION['client_id'];
        } else echo 'Не удалось открыть файл идентификатора клиента для записи.';
    } else {
        if(file_exists($storage.$client_id.'.tmp')) {
            if ($file = fopen($storage.$client_id.'.tmp', 'r')) {
                $client_id = file_get_contents($file);
                fclose($file);
            }
        } else $client_id = false;
    }
    //.. Очищаем временные файлы, которые старше прошлых суток
    // Временные файлы используются скриптами "rs_detail.php", "rs_report.php" и "create.php"
    // GetFileNames() находится в "func_mini.php".
    $tmpFiles = GetFileNames($storage, true);
    if (!empty($tmpFiles)) {
        foreach ($tmpFiles as $value) {
            if (filemtime($storage.$value) < (mktime() - 86400)) {
                unlink($storage.$value);
            }
        }
    }

    //.. 11.08.2022 Проверка на кромки фирмы MAAG
    // Если есть такие, то запрещаем PUR-кромкование
    $usable_eddge_change = isset($_SESSION['pur_edge_denied']) ? 'disabled' : '';

    // Полушин: Данные из /clients/glass/*
    if (isset($_POST['glass_project'])) {
        $glass_project = unserialize(base64_decode($_POST['glass_project']));
        $glass_arr = $glass_project['glass_arr'];
        unset($glass_project['glass_arr']);
// todo: Удалет pdf и img файлы если они есть
//        foreach ($glass_arr['parts'] as $part) {
//            if (isset($part['path']) && !empty($part['path'])) {
//                foreach ($part['path'] as $path) {
//                    if (file_exists($serv_main_dir . $path)) unlink($serv_main_dir . $path);
//                }
//            }
//        }
        // тут уже готовый проджект-массив
        $result = $glass_project;

        //$result = glass_to_project($glass_arr,$_SESSION['client_code'],$_SESSION['user_place']);

    }
    echo $script_for_out;

    $up_padding = '';
    if($_SESSION['order_status'] == 1 || $_SESSION['order_status'] == 2) {
        $up_padding = 'style="padding-top: 35px"';

        if (isset($_SESSION['pur_edge_denied'])) {
            $up_padding = 'style="padding-top: 60px"';
        }
    } elseif(isset($_SESSION['pur_edge_denied']) && ($_SESSION['order_status'] != 1 && $_SESSION['order_status'] != 2 || !isset($_SESSION['order_status']))) {
        $up_padding = 'style="padding-top: 35px"';
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <script
                src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/fontawesome/all.min.css">
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
                integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
        <title>Подтверждение заказа</title>
        <style >
            .hide{display: none;}
            .visible{display: block;}
            .dibl{display: inline-block;}
            .warning-border{border: 3px solid #ff0000;}
            .bold{font-weight: bold;}
            .codeSearch{
                width: 100%;
                max-width: 600px;
                vertical-align: top;
                padding-top: 3px;
            }
            .glueColor .fa-info-circle{opacity: 0;}
            .glueColor:hover > .fa-info-circle{
                visibility: visible;
                opacity: 1;
                transition: opacity .5s linear;
            }
            .erase-tr{
                border: 1px solid red;
                border-radius: 10px;
                padding-right: 5px;
                margin: 2px 5px;
                display: inline-block;
                cursor: pointer;
            }
            .close-sel {
                position: relative;
                cursor: pointer;
                color: red;
                transform: rotate(-45deg);
                margin: 0 2px 0 10px;
            }
            .loading-search{
                position: absolute;
                right: 22px;
                top: 3px;
                display: none;
            }
            #searchOnCode-box{padding: 10px;display: none;}
            #searchOnCode-box li{ cursor: pointer; }
            form{
                margin: 10px;
            }
            #checks_lodaer_containers {
                position: fixed;
                left: 0;
                background: rgba(255,255,255,0.4);
                width: 100%;
                height: 100%;
                display: none;
                z-index: 99999;
                top: 0;
            }
            #checks_lodaer_containers > div {
                width: 100%;
                display: flex;
                flex-flow: column;
                justify-content: center;
                align-items: center;
                z-index: 9999;
                height: 100%;
            }
            #client_variant_container {
                position: absolute;
                background: #17a2b8;
                border: 1px solid lightgray;
                width: 300px;
                left: 15px;
                bottom: -35px;
                box-shadow: 0 0 5px lightgrey;
                color: white;
                padding: 5px 14px;
                cursor: pointer;
                display: none;
                top: 71px;
                height: 40px;
            }
            .form-group.w-25.client_form_container {
                position: relative;
            }
            body {
                overflow-x: hidden;
            }
            .edit_order_alerts {
                position: fixed;
                width: 100%;
                padding: 4px;
                color: white;
                background: green;
                text-align: center;
                top: 0;
                left: 1px;
            }
            .edit_order_alerts.red {
                background: red;
            }
            .modal-body{margin: auto;}
            .btn{margin: 3px auto;}
            button a{color: #fff;}
            button a:hover{color: #fff;text-decoration: none;}
            .table-add-list{border-bottom: 1px solid #bfbfbf;}
            form#zzz_main_form .text-right{margin-bottom: 1rem!important;}
        </style>


    </head>
    <body <?= $up_padding ?>>
    <!-- Social networks -->
    <script>
        const script = document.createElement('script');
        script.src = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.umd.js';
        script.onload = function () {
            const body = document.querySelector('body');
            const widgetEl = document.createElement('div');
            widgetEl.setAttribute('id', 'wt-omnichannel-widget');
            body.appendChild(widgetEl);            

            const config = {
                "view": {
                    "lang": "en",
                    "btnOpacity": "1",
                    "logoUrl": "https://kronas.com.ua/Media/pic/icons/KR_logo_25x25px.svg",
                    "accentColor": "hsl(292, 56.99999999999999%, 21%)"
                },
                "chat": {
                    "url": "wss://cloud.webitel.ua/chat/hddvjbgfoitieuhtgvaygashqutmuan"
                },
                "alternativeChannels": {
                    "viber": "viber://pa?chatURI=kronasbot",
                    "telegram": "https://t.me/Kronascomua_bot",
                    "facebook": "https://www.facebook.com/kronas.com.ua/"
                }
            };

            const app = new WtOmniWidget('#wt-omnichannel-widget', config);
        };
        document.head.appendChild(script);

        const link = document.createElement('link');
        link.href = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.css';
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.media = 'screen,print';
        document.head.appendChild(link);        
    </script>
    <!-- Social networks -->
    <?php




    if($_SESSION['change_project_tool_places'] == 1) {
        echo '<div class="alert alert-warning" role="alert">
              Проект был пересчитан в связи со сменой участка или типа кромкования!
            </div>';
        unset($_SESSION['change_project_tool_places']);
    }
    ?>
    <!-- Modal -->
    <div class="modal fade" id="exampleModa9" tabindex="-1" role="dialog" aria-labelledby="exampleModa9" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Выберите точку возврата:</p>

                    <?php
                        if ($client_id !== false) $returnLink = $laravel_dir.'/start_new_create_porject/?client_id='.$client_id.'&order_project_id='.$giblab_project_id;
                        else $returnLink = $laravel_dir.'/start_new_create_porject/?order_project_id='.$giblab_project_id;
                    ?>
                    <button class="btn btn-danger" id="return_in_project_without"><a href="<?= $returnLink ?>">Посмотреть деталировку</a></button>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalBackToGlass" tabindex="-1" role="dialog" aria-labelledby="modalBackToGlass" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Вы хотите вернутся в конструктор стекла?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?=$main_dir?>/clients/glass/details.php<?='?nw='.$_GET['nw']?>" method="POST">
                        <input type="hidden" name="glass_arr" value='<?php echo json_encode($glass_arr);?>'>
                        <button type="submit" class="btn btn-danger" id="back_to_glass">Вернуться</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <form method="post" enctype='multipart/form-data' id="zzz_main_form">
        <div class="row">
            <div class="col-12">
                <?php
                    echo $place>0?'Участок: '.db_get_user_place($place).'<br>':'';
                    //..29.12.2020..11.01.2021
                    if ($place > 0) $_SESSION["user_place_f_rs"] = db_get_user_place($place);

                    if($_SESSION['rsorder'] != 1) {
                ?>
                    Тип кромкования:
                <?php
                        echo $arr_part_double[$part_double['user_type_edge']].'<br />';
                    }
                    // Дата готовности (<i>если заказ будет подтверждён в производство сегодня</i>): <b><i><?php echo $_SESSION['res']['serv_order_time']; \?\></i></b><br><br>
                ?>
                <?php if($_SESSION['is_client'] != 1 && $_SESSION['rsorder'] != 1): ?>
                    <label class="sr-only" for="discountPercent">Скидка %</label>
                    <div class="input-group mb-2 mr-sm-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Скидка %</div>
                        </div>
                        <input type="text" class="form-control col-md-2 discountPercent" name="discountPercent1" id="discountPercent1">
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-12">
                <?php

                $full_total = 0;
                $resulttotal = 0;
                if (count($material_price) > 0 || count($band_price) > 0) {
                    ?>
                    <table class="table table-1 table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Материал</th>
                            <th scope="col">Наименование</th>
                            <th scope="col" class=" text-center">Количество (ед. имз.)</th>
                            <th scope="col" class=" text-center">Цена</th>
                            <th scope="col" class=" text-center">Сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (count($material_price) > 0) {
                            $_arr = array();
                            foreach ($material_price as $value) {
                                $_arr[] = $value['material_code'];
                            }
                            $materials_data = db_get_materials_data($_arr);

                            foreach ($material_price as $value) {
                                $_val = $materials_data[$value['material_code']]['L'] . ' X ' . $materials_data[$value['material_code']]['W'] . '<br>' .
                                    $materials_data[$value['material_code']]['TYPENAME'] . '<br>' . $materials_data[$value['material_code']]['NAME'];
                                echo '<tr class="tablerow group-1" data-calc="0" data-img="pic/' . $materials_data[$value['material_code']]['FILE_NAME_SMALL'] . '" data-ins="' . $_val . '" data-title="' . $materials_data[$value['material_code']]['NAME'] . '" >
      <td>' . $value['material_code'] . '</td>
      <td>' . $value['material_name'] . '</td>
      <td class="count text-center" data-count="' . $value['sheet_count'] . '">' . $value['sheet_count'] . '</td>
      <td class="price text-center" data-price="' . $value['sheet_price'] . '">' . $value['sheet_price'] . '</td>
      <td class="total text-center">' .number_format(  $value['sheet_count'] * $value['sheet_price'], 2, ',', ' ')  . '</td>
    </tr>';
                                $resulttotal = $resulttotal + ($value['sheet_count'] * $value['sheet_price']);
                            }
                        }

                        if (count($band_price) > 0) {
                            $_arr = array();
                            // p_($_SESSION['res']['serv_time']);
                            foreach ($band_price as $value) {
                                $_arr[] = $value['band_code'];
                            }
                            $bands_data = db_get_bands_data($_arr);

                            foreach ($band_price as $value) {
                                $_val = $bands_data[$value['band_code']]['L'] . ' X ' . $bands_data[$value['band_code']]['W'] . '<br>' .
                                    $bands_data[$value['band_code']]['TYPENAME'] . '<br>' . $bands_data[$value['band_code']]['NAME'];
                                echo '<tr class="tablerow group-1" data-calc="0" data-img="pic/' . $bands_data[$value['band_code']]['FILE_NAME_SMALL'] .
                                    '" data-ins="' . $_val . '" data-title="' . $bands_data[$value['band_code']]['NAME'] . '" >
      <td>' . $value['band_code'] . '</td>
      <td>' . $value['band_name'] . '</td>
      <td class="count text-center" data-count="' . $value['band_count'] . '">' . $value['band_count'] . '</td>
      <td class="price text-center" data-price="' . $value['band_price'] . '">' . $value['band_price'] . '</td>
      <td class="total text-center">' . number_format( $value['band_count'] * $value['band_price'], 2, ',', ' ') . '</td>
    </tr>';
                                $resulttotal = $resulttotal + ($value['band_count'] * $value['band_price']);
                            }
                        }
                        $full_total=$full_total+$resulttotal;
                        ?>
                        </tbody>
                    </table>
                    <div class="col-12 resulttotal-0 text-right mb-5" data-total="<?= $resulttotal ?>"
                         style="font-size: 22px;font-weight: bold;font-style: italic;"><?= number_format($resulttotal, 2, ',', ' ') ?> грн.</div>
                    <?php
                }
                if (count($service_price) > 0) {
                    $resulttotal = 0;
                    ?>

                    <table class="table table-2 table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Услуги</th>
                            <th scope="col">Наименование</th>
                            <th scope="col" class=" text-center">Количество (ед. имз.)</th>
                            <th scope="col" class=" text-center">Цена</th>
                            <th scope="col" class=" text-center">Сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach ($service_price as $value) {
                            //.. 16.02.2022 Определяем виды кромкования
                            if (isset($value['service_name'])) {
                                if (stripos($value['service_name'], 'Кромкование') !== false) {
                                    if (stripos($value['service_name'], 'лазер') !== false) $gcsd['laz'] = 1;
                                    else $gcsd['pvc'] = 1;
                                }
                            }
                            echo '<tr class="tablerow group-1" data-calc="1"  >
      <td>' . $value['service_code'] . '</td>
      <td>' . $value['service_name'] . '</td>
      <td class="count text-center" data-count="' . $value['service_count'] . '">' . $value['service_count'] . ' (' . $value['service_unit'] . ')</td>
      <td class="price text-center" data-price="' . $value['service_price'] . '">' . $value['service_price'] . '</td>
      <td class="total text-center">' .number_format( $value['service_count'] * $value['service_price'], 2, ',', ' ') . '</td>
    </tr>';
                            $resulttotal = $resulttotal +( $value['service_count'] * $value['service_price']);
                        }
                        $full_total=$full_total+$resulttotal;
                        ?>
                        </tbody>
                    </table>
                    <div class="col-12 resulttotal-1 text-right mb-5" data-total="<?= $resulttotal ?>"
                         style="font-size: 22px;font-weight: bold;font-style: italic;"><?= number_format($resulttotal, 2, ',', ' ') ?> грн.</div>
                    <?php
                }
                if($_SESSION['rsorder'] != 1) {
                if (count($part_price) > 0) {
                    ?>
                    <table class="table table-3 table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Деталь</th>
                            <th scope="col">Материал</th>
                            <th scope="col">Описание</th>
                            <th scope="col" class=" text-center">Количество</th>
                            <th scope="col" class=" text-center">Цена</th>
                            <th scope="col" class=" text-center">Сумма</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $resulttotal=0;
                        foreach ($part_price as $value) {

                            echo '<tr class="tablerow group-1" data-calc="2">
      <td>id=' . $value['part_name'] . '</td>
      <td>' . $value['material_code'] . '</td>
      <td>' . $value['description'] . '</td>
      <td class="count text-center" data-count="' . $value['count'] . '">' . $value['count'] .  '</td>
      <td class="price text-center" data-price="' . $value['price'] . '">' . $value['price'] . '</td>
      <td class="total text-center">' .number_format($value['count'] * $value['price'], 2, ',', ' ') . '</td>
    </tr>';
                            $resulttotal = $resulttotal + $value['count'] * $value['price'];
                        }

                        //    $full_total=$full_total+$resulttotal;

                        ?>
                        </tbody>
                    </table>
                    <div class="col-12 resulttotal-2 text-right mb-5"  data-total="<?= $resulttotal ?>"
                         style="font-size: 22px;font-weight: bold;font-style: italic;"><?= number_format($resulttotal, 2, ',', ' ') ?> грн.</div>
                    <?php
                    }
                }
                $visHide = (count($simple_price) > 0) ? 'visible' : 'hide';
                ?>
            </div>
            <div class="col-12 lastBlock <?= $visHide ?>">
                <h4>Дополнительно:</h4>
                <?php if($_SESSION['is_client'] != 1 && $_SESSION['rsorder'] != 1): ?>
                    <label class="sr-only" for="discountPercent">Скидка %</label>
                    <div class="input-group mb-2 mr-sm-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Скидка %</div>
                        </div>
                        <input type="text" class="form-control col-md-2 discountPercent" name="discountPercent2" id="discountPercent2">
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-12 lastBlock <?= $visHide ?>">
                <?php
                $resulttotal =0;

                ?>
                <table class="table table-4 table-striped table-add-list">
                    <thead>
                    <tr class="table-dark">
                        <th scope="col">Код</th>
                        <th scope="col">Наименование</th>
                        <th scope="col" class=" text-center">Количество</th>
                        <th scope="col" class=" text-center">Цена</th>
                        <th scope="col" class=" text-center">Сумма</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $resulttotal=0;
                    $_arr_type=array(
                        'MATERIAL'=>'<br>Плитные материалы<br>',
                        'BAND'=>'<br>Кромочные материалы<br>',
                        'SIMPLE'=>'<br>Мебельная фурнитура<br>',
                    );

                    // $rs_hidden_prices = '';
                    // if($_SESSION['rsorder']=1) {
                    //     $rs_hidden_prices = 'style="display: none!important;"';
                    // }
                    if (count($simple_price) > 0) {
                        $_where = array();
                        $_arr_empty = array();
                        $_counter=1;
                        foreach ($simple_price as $value) {
                            if((int)$value['Code']>0 ) $_where[] = $value['Code'];
                            else  $_arr_empty=$value;
                        }
                        if(count($_where)>0) $add_data = al2b_db_get_simpleslist($_where);
//                        p_($simple_price);
                        foreach ($simple_price as $value) {
                            $_d_class = '';
                            $_d_data = '';
                            if (isset($add_data[$value['Code']])) {
                                $_d_class = ' addtable-tr ';
                                $_d_data = ' data-img = "pic/' . $add_data[$value['Code']]['pic'] . '" 
                            data-ins = "' . $add_data[$value['Code']]['newdesc'] . $_arr_type[$add_data[$value['Code']]['typee']] . $value['Name'] . '" data-original-title="' . $value['Name'] . '" ';
                            }
                            if($_SESSION['rsorder'] != 1) {
                                echo '<tr class="tablerow group-2 tr-id-' . $_counter . ' ' . $_d_class . '" data-calc="3" ' . $_d_data . '>
      <td>' . ($value['Code']>0?$value['Code']:'<span style="color:red;">Не найдено</span>') . '</td>
      <td>' . $value['Name'] . '
      <input type="hidden" name="simpleprice[]" value="' . $value['Code'] . '">
      </td>
      
      <td class="count text-center" data-count="' . $value['count'] . '"><input type="text" name="simplepricecnt[]" value="' . $value['count'] . '" '.$rs_hidden_prices.'></td>
      <td class="price text-center" data-price="' . $value['price'] . '">' . $value['price'] . '</td>
      <td class="total text-center">' .  number_format($value['count'] * $value['price'], 2, ',', ' '). '</td>
      <td class="erase text-center">' . ($value['Code']==0?'<button type="button" class="btn btn-primary" data-toggle="modal" data-targetid="'.$_counter.'" 
                    data-target="#selectPart">Выбрать
            </button>':'') . '<span class="erase-tr" '.$rs_hidden_prices.'><i class="close-sel">x</i> Удалить</span></td>
    </tr>';
                            } else {
                                echo '<tr class="tablerow group-2 tr-id-' . $_counter . ' ' . $_d_class . '" data-calc="3" ' . $_d_data . '>
      <td>' . ($value['Code']>0?$value['Code']:'<span style="color:red;">Не найдено</span>') . '</td>
      <td>' . $value['Name'] . '
      <input type="hidden" name="simpleprice[]" value="' . $value['Code'] . '">
      </td>
      
      <td class="count text-center" data-count="' . $value['count'] . '"><p class="el-count">'.$value["count"].$rs_hidden_prices.'</p></td>
      <td class="price text-center" data-price="' . $value['price'] . '">' . $value['price'] . '</td>
      <td class="total text-center">' .  number_format($value['count'] * $value['price'], 2, ',', ' '). '</td>
      <td class="erase text-center"></td>
    </tr>';
                            }
                            $_counter++;
                            $resulttotal = $resulttotal + $value['count'] * $value['price'];
                        }

                    }
                    $full_total=$full_total+$resulttotal;
                    ?>
                    </tbody>
                </table>
                <div class="col-12 resulttotal-3 text-right mb-5"  data-total="<?= $resulttotal ?>"
                     style="font-size: 22px;font-weight: bold;font-style: italic;"><?= number_format($resulttotal, 2, ',', ' ') ?> грн.</div>
                <?php

                if (!isset($_SESSION['user_place'])) $_SESSION['user_place'] = 1;

                $manager_sql = 'SELECT * FROM `manager` WHERE `place` = ' . $_SESSION['user_place'] . ' AND (`admin` = 0 OR `admin` IS NULL) AND `active` = 1 AND `access` < 10 ORDER BY `name`';

                //.. 13.03.2023 Тоценко
                // Изменена выборка, если участок менеджера 8 (Житомир) или 10 (Ивано-Франковск), и текущий участок 1 или 2, то выбираем также менеджеров участка менеджера
                if ($_SESSION['is_manager'] == 1) {
                    $q = 'SELECT * FROM `manager` WHERE `id` = ' . $_SESSION['manager_id'] . ' LIMIT 1';
                    $res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
                    $managerPlace = $res['data'][0]['place'];

                    if (($managerPlace == 8 || $managerPlace == 9 || $managerPlace == 10) && ($_SESSION['user_place'] == 1 || $_SESSION['user_place'] == 2)) {
                        $manager_sql = 'SELECT * FROM `manager` WHERE (`place` = 1 OR `place` = 2 OR `place` = 4 OR `place` = 8 OR `place` = 9 OR `place` = 10) AND (`admin` = 0 OR `admin` IS NULL) AND `active` = 1 ORDER BY `name`';
                    } elseif (($managerPlace != 8 && $managerPlace != 9 && $managerPlace != 10) && ($_SESSION['user_place'] == 1 || $_SESSION['user_place'] == 2 || $_SESSION['user_place'] == 4)) {
                        $manager_sql = 'SELECT * FROM `manager` WHERE (`place` = 1 OR `place` = 2 OR `place` = 4) AND (`admin` = 0 OR `admin` IS NULL) AND `active` = 1 ORDER BY `name`';
                    } elseif ($_SESSION['user_place'] == 11) {
                        $manager_sql = 'SELECT * FROM `manager` WHERE (`place` = 1 OR `place` = 2 OR `place` = 4 OR `place` = 8) AND (`admin` = 0 OR `admin` IS NULL) AND `active` = 1 ORDER BY `name`';
                    }
                } else {
                    if ($_SESSION['user_place'] == 1 || $_SESSION['user_place'] == 2 || $_SESSION['user_place'] == 4) {
                        $manager_sql = 'SELECT * FROM `manager` WHERE (`place` = 1 OR `place` = 2 OR `place` = 4) AND (`admin` = 0 OR `admin` IS NULL) AND `active` = 1 AND `access` < 10 ORDER BY `name`';
                    }
                }
                
                $manager_db = mysqli_query($link, $manager_sql);

                ?>
            </div>
            <div class="col-12" <?php if($_SESSION['rsorder']==1) echo 'style="display:none!important"'; ?>>
                <div class="dibl">
                    <button type="button" class="btn btn-primary" data-toggle="modal"  data-targetid="0" data-target="#selectPart">Выбрать</button>
                </div>
                или
                <div class="codeSearch dibl">
                    <div class="spinner-border text-primary loading-search" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <input class="form-control" type="text" id="searchOnCode" placeholder="Найти по коду">
                    <div id="searchOnCode-box"></div>
                </div>
            </div>

        </div>

        <?php
            $sql_places = 'SELECT * FROM `PLACES` WHERE `active` = 1';
            $db_places = mysqli_query($link, $sql_places)->fetch_all(MYSQLI_ASSOC);

            $sql_edgeline = 'SELECT `PLACE` FROM `TOOL_EDGELINE_EQUIPMENT` WHERE `pur` = 1';
            $db_edgeline = mysqli_query($link, $sql_edgeline)->fetch_all(MYSQLI_ASSOC);
            $tool_per_place = [];
            foreach ($db_edgeline as $item) {
                $tool_per_place[] = $item['PLACE'];
            }

            function get_place_name($p_id) {
                $sql_places_name = 'SELECT * FROM `PLACES` WHERE PLACES_ID = '.$p_id;
                $db_places_name = mysqli_query($GLOBALS['link'], $sql_places_name)->fetch_assoc()['NAME'];
                return $db_places_name;
            }
        ?>


        <!-- <div class="row" <?php if($_SESSION['order_status_data'] != 'черновик' && $_SESSION['manager_id'] > 0 && ($_SESSION['order_status'] == 1 || $_SESSION['order_status'] == 2)) echo 'style="height: 1px!important; width: 1px!important; opacity: 0!important;"'; ?>> -->
        <div class="row mt-5">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Выбор менеджера</label>
                    <select class="form-control" name="manager_id">
                        <option value="store">Общий (интернет-магазин)</option>
                        <?php while ($manager = $manager_db->fetch_assoc()): ?>
                            <option value="<?php echo $manager['id'] ?>"
                                <?php
                                    if($_SESSION['order_manager_id'] && $_SESSION['order_manager_id'] == $manager['id']) {
                                        echo 'selected="selected"';
                                    }
                                    else if($_SESSION['manager_id'] == $manager['id']) {
                                        echo 'selected="selected"';
                                    }
                                ?>
                            ><?php echo $manager['name'].' ('.get_place_name($manager['place']).')' ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3" <?php if($_SESSION['order_status_data'] != 'черновик' && $_SESSION['manager_id'] > 0 && ($_SESSION['order_status'] == 1 || $_SESSION['order_status'] == 2) || $_SESSION['rsorder']==1) echo 'style="height: 1px!important; width: 1px!important; opacity: 0!important;"'; ?>>
                <div class="form-group">
                    <label>Выбор участка</label>
                    <select class="form-control" name="change_palce_id" id="select_places">
                        <?php foreach($db_places as $place): ?>
                            <option value="<?php echo $place['PLACES_ID'] ?>" <?php if(in_array($place['PLACES_ID'],$tool_per_place)) echo 'data-pur="1"' ?><?php if($_SESSION['user_place'] == $place['PLACES_ID']) echo 'selected="selected"'; ?>><?php echo $place['NAME'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <?php if (isset($_SESSION['manager_id']) && in_array($_SESSION['manager_id'], $addition_place_managers)): ?>
                <div class="col-md-3" <?php if($_SESSION['order_status_data'] != 'черновик' && $_SESSION['manager_id'] > 0 && ($_SESSION['order_status'] == 1 || $_SESSION['order_status'] == 2) || $_SESSION['rsorder']==1) echo 'style="height: 1px!important; width: 1px!important; opacity: 0!important;"'; ?>>
                    <div class="form-group">
                        <label>Доп.участок менеджера</label>
                        <select class="form-control" name="manager_addition_place" id="manager_addition_place">
                            <?php foreach($db_places as $place): ?>
                                <option value="<?php echo $place['PLACES_ID'] ?>" <?php if(isset($_SESSION['manager_addition_place'])) echo 'selected="selected"'; ?>><?php echo $place['NAME'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($gcsd['pvc'] === 1 && $gcsd['laz'] === null): ?>
            <div class="col-md-3">
                <div class="form-group glueColor" <?php if($_SESSION['rsorder']==1) echo 'style="height: 1px!important; width: 1px!important; opacity: 0!important;"'; ?>>
                    <label>Выбор цвета клея</label>
                    <i class="fa fa-info-circle" title="При выборе цвета клея, цвет применяется на весь заказ, поэтому если цвета материалов у вас кардинально отличаются, то такие заказы рекомендуется разделить и для каждого применить подходящий цвет клея" aria-hidden="true"></i>
                    <select class="form-control" id="glueColor" name="glue_color" required>
                        <?php if ($glue_color !== 'белый' && $glue_color !== 'прозрачный'): ?>
                        <option value="empty" name="empty"></option>
                        <?php endif ?>
                        <option value="белый" <?php echo  $glue_color == 'белый' ? 'selected' : ''?>>Белый</option>
                        <option value="прозрачный" <?php echo  $glue_color == 'прозрачный' ? 'selected' : ''?>>Прозрачный</option>
                    </select>
                </div>
            </div>    
            <?php endif ?>
            
            <div class="col-md-3">
                <div class="form-group" <?php if($_SESSION['rsorder']==1) echo 'style="height: 1px!important; width: 1px!important; opacity: 0!important;"'; ?>>
                    <label>Выбор типа кромкования</label>
                    <select class="form-control" name="change_tool_id" id="select_tools">

                    </select>
                </div>
            </div>
        </div>
        <hr>

        <?php if($_SESSION['is_manager'] == 1): ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group client_form_container">
                        <label>Клиент: <?php if($_SESSION['client_fio']) echo $_SESSION['client_fio'];?></label>
                        <input class="form-control" type="text" name="client_search" placeholder="" id="client_code_search" <?php if($_SESSION['client_phone']) echo 'value="'.$_SESSION['client_phone'].'"'; ?>  <?php if($_SESSION['is_manager'] == 1 && $_SESSION['client_id'] == 85159) echo 'style="display:none!important;"' ?>>
                        <input class="form-control" type="hidden" name="client_code" placeholder="" id="client_code_input" <?php if($_SESSION['client_id']) echo 'value="'.$_SESSION['client_id'].'"'; ?>>
                        <div id="client_variant_container">

                        </div>
                        <?php if($_SESSION['is_client'] == 0): ?>
                            <p>
                                <small>
                                    *- Введите номер телефона клиента и выбирите необходимого контрагента из предложенного списка.<br>
                                    *- Если по номеру телефона совпадений не найдено - заказ будет оформлен на Розничного покупателя.<br>
                                    *- Если поле номера телефона не заполнено - заказ также будет оформлен на Розничного покупателя.<br>
                                </small>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <hr>

        <div class="row" <?php if($_SESSION['is_manager'] == 1 && $_SESSION['client_id'] != 85159) {echo 'style="display: none!important;"';} ?>>
            <div class="col-md-3">
                <label>Введите Ваше имя</label>
                <input class="form-control" type="text" name="name" placeholder="Ф.И.О" required="required" value="<?php if($_SESSION['client_fio'] != 'Интернет-магазин') echo $_SESSION['client_fio']; ?>">
            </div>
            <div class="col-md-3">
                <label>Введите Ваш телефон</label>
                <input class="form-control" type="text" name="telephone" value="<?php if($_SESSION['client_phone'] != '+38(093)426-17-35') echo $_SESSION['client_phone']; ?>" placeholder="+38(0**)-***-****" id="phone_input" minlength="17" required="required">
            </div>
            <div class="col-md-3">
                <label>Введите Ваш e-mail</label>
                <input class="form-control" type="email" name="email" placeholder="example@mail.com" value="<?php if($_SESSION['client_email'] != 'testzzz@kronas.com.ua') echo $_SESSION['client_email']; ?>" novalidate>
            </div>
            <div class="col-md-3"></div>
            <input type="hidden" name="order_confirm" value="1">
        </div>

        <div class="row">
            <div class="col-md-12 d-flex flex-row flex-nowrap justify-content-between">
                <div class="form-group w-100">
                    <label>Комментарий</label>
                    <textarea class="form-control" rows="4" placeholder="Комментарий или пожелания к заказу" name="order_comment"><?php echo $_SESSION['order_comment']; ?></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 fulltotal text-right mb-5" style="font-size: 22px;font-weight: bold;font-style: italic;">
                Итого:  <?= number_format($full_total, 2, ',', ' ') ?> грн.</div>
            <div class="col-3 text-center mb-5">
                <?php if($_SESSION['order_status'] != 2): ?>

                <button type="submit" class="btn btn-success" id="submit_order">
                    <?php if($_SESSION['order_status'] == 1 && $_SESSION['order_status_data'] != 'черновик') {
                        echo 'Обновить данные по заказу';
                    } else {
                        echo 'Подтвердить';
                    }?>
                </button>
                <?php endif; ?>
            </div>
            <div class="col-6 text-center mb-5">
                <?php if ($_SESSION['rsorder'] && $_SESSION['rsorder']==1): ?>
                    <button class="btn btn-info"><a href="<?=$main_dir.'/rs_report.php?ip='.$project_id?>" target="_blank">РС-отчёт для производства</a></button>
                    <button class="btn btn-info"><a href="<?=$main_dir.'/rs_detail.php?ip='.$project_id?>" target="_blank">Спецификация РС</a></button>
                <?php endif ?>
                <button type="submit" name="pdf-crea"  value="1" class="btn btn-info">Спецификация (PDF)</button>
                <button class="btn btn-info" id="full_project_pdf">Полный отчет PDF</button>
            </div>
            <div class="col-3 text-center mb-5">

                <?php if ($_SESSION['rsorder'] && $_SESSION['rsorder']==1): ?>
                    <!-- Button trigger modal -->
                    <button type="button" id="back_edits_battun" class="btn btn-danger" data-toggle="modal" data-target="#exampleModa9">Вернуться к редактированию деталей</button>
                <?php elseif (isset($glass_arr)): // Полушин: for glass?>
                        <button type="button" class="btn btn-danger" id="back_edits_glass" data-toggle="modal" data-target="#modalBackToGlass">Вернуться к редактированию деталей</button>
                <?php else: ?>
                    <button type="submit" name="back_giblab"  value="1" class="btn btn-danger" id="return_in_project">Вернуться к редактированию деталей</button>
                <?php endif ?>
            </div>
        </div>
    </form>

    <script>

        <?php if($_SESSION['simple_price_extra']): ?>

        let simple_price_extra = <?php echo json_encode($_SESSION['simple_price_extra']); ?>;

        // simple_price_extra.forEach(function (item) {
        //     if(item.type == 'material') item.type = 'sheet';
        //     list_data_select_start(item.type + '_' + item.Code, item.count);
        // });

        <?php endif;?>




        <?php
        require_once DIR_CORE . 'zzz.js';

        ?>
        $(document).ready(function(){
            $('#phone_input').mask('+38(000)-000-0000', {
                placeholder: "+38(0__)-___-____"
            });
            $('#client_code_search').mask('+38(000)000-00-00', {
                placeholder: "+38(0__)-___-____"
            });

            $('#return_in_project').on('click', function (e) {
                $('input[name="name"]').removeAttr('required');
                $('input[name="telephone"]').removeAttr('required');
                $(e.target).click();
            });
            $('#return_in_project_without').on('click', function (e) {
                $('input[name="name"]').removeAttr('required');
                $('input[name="telephone"]').removeAttr('required');
                $(e.target).click();
            });
            <?php if(($_SESSION['manager_id'] > 0 && $_SESSION['manager_id'] != 39) || $_SESSION['is_client'] == 1): ?>
            $('input[name="name"]').removeAttr('required');
            $('input[name="telephone"]').removeAttr('required');
            <?php endif; ?>
        });
        function list_data_select(val) {
            $.ajax({
                type: "POST",
                url: "ajax_resp.php",
                data:'addtolist='+val,
                beforeSend: function(){
                    $(".loading-search").show();
                },
                success: function(data){
                    if ($(".lastBlock").hasClass('hide')) {
                        $(".lastBlock").removeClass('hide')
                        $(".lastBlock").addClass('visible')
                    }
                    $("#searchOnCode").val('');
                    $(".table-add-list tbody").append(data);
                    $("#searchOnCode-box").hide();
                    $(".loading-search").hide();
                    $("input#discountPercent2").keyup();
                }
            });
        }
        function list_data_select_start(val, count) {

            $.ajax({
                type: "POST",
                url: "ajax_resp.php",
                data:'addtolist='+val,
                beforeSend: function(){
                    $(".loading-search").show();
                },
                success: function(data){
                    if ($(".lastBlock").hasClass('hide')) {
                        $(".lastBlock").removeClass('hide')
                        $(".lastBlock").addClass('visible')
                    }
                    $(".table-add-list tbody").append(data);
                    $(".table-add-list tbody tr").last().find('input[name="simplepricecnt[]"]').val(count);
                    $(".table-add-list tbody tr").last().find('.el-count').val(count);
                    $("#searchOnCode-box").hide();
                    $(".loading-search").hide();
                    $("input#discountPercent2").keyup();
                }
            });
        }
        
        $('#glueColor').on('change', function (e) {
            if ($('#glueColor').val() !== 'empty') {
                $('#glueColor').removeClass('warning-border')
                $('.glueColor .reqMess').remove()
            }
        })

        $('#submit_order').on('click', function (e) {

            console.log($('#client_code_input').val());
            if(document.querySelector('input[name="name"]').validity.valid && document.querySelector('input[name="telephone"]').validity.valid) {
                e.preventDefault();
                if ($('#glueColor').length && $('#glueColor').val() === 'empty') {
                    if(!$('#glueColor').hasClass('warning-border')) {
                        $('#glueColor').addClass('warning-border')
                        $('.glueColor').append('<p style="color:#ff0000" class="reqMess bold">Выберите цвет клея</p>')
                    }
                } else {
                    if( !$('#client_code_input').val()) {
                        $('#client_code_input').val(85159);
                    };
                    if($('input[name="email"]').val() == 'Не указан') {
                        $('input[name="email"]').val(' ');
                    }
                    $('#checks_lodaer_containers').show();
                    $('#checks_lodaer_containers div').append('<h1>Идет запись в базу данных</h1>')
                    console.log($('#client_code_input').val());
                    $('#zzz_main_form').submit();
                }
            }

        });

        $('#select_places').on('change', function () {
            check_places_tool('');
        });
        function check_places_tool(place) {
            let option_id = $('#select_places').val();
            let type = '';
            let usable_eddge_change = `<?= $usable_eddge_change ?>`;
            let pur = $('#select_places option[value="' + option_id +'"]').attr('data-pur');
            console.log('pur', pur);
            console.log('usable_eddge_change', usable_eddge_change);
            if(pur == 1) {
                $('#select_tools').html('');

                if(place == 'pur') {
                    if (usable_eddge_change != 'disabled') {
                        type = 'selected="selected"';
                    }
                } else {
                    type = '';
                }
                $('#select_tools').append('<option value="pur" <?= $usable_eddge_change ?>'+type+'>ПУР</option>');

                if(place == 'eva') {
                    type = 'selected="selected"';
                } else {
                    type = '';
                }
                $('#select_tools').append('<option value="eva" '+type+'>ЭВА</option>');

                // if(place == 'laz') {
                //     type = 'selected="selected"';
                // } else {
                //     type = '';
                // }
                // $('#select_tools').append('<option value="laz" '+type+'>ЛАЗ</option>');

            } else {
                $('#select_tools').html('');
                $('#select_tools').append('<option value="eva" selected="selected">ЭВА</option>');
            }
        }
        check_places_tool('<?php echo $_SESSION['user_type_edge'] ?>');
    </script>
    <script>
        $(document).ready(function () {
            $('#send_pdf_button').on('click', async function(e) {
                e.preventDefault();

                let formData = new FormData();
                ///formData.append('project_data', $('#saveContainer').val());
                formData.append('project_data', `<?php echo $_SESSION['project_data']; ?>`);
                formData.append('action', 'get_all_pdf' );

                let request = await fetch('<?php echo $main_dir; ?>/pdf_send.php', {
                    method: 'POST',
                    headers: {
                        ///'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    body: formData
                });
                let body = await request.text();
                console.log(body);
                if(body.indexOf('/users_pdf/') > -1){
                    window.open(body);
                };
            });
            $('#full_project_pdf').on('click', async function(e) {

                e.preventDefault();
                $('#checks_lodaer_containers').show("slow");

                let formData = new FormData();
                formData.append('project_data', `<?php echo $_SESSION['project_data']; ?>`);
                formData.append('action', 'get_all_pdf_manager' );

                let request = await fetch('<?php echo $main_dir; ?>/pdf_send.php', {
                    method: 'POST',
                    headers: {
                        ///'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    body: formData
                });
                let body = await request.text();
                console.log(body);
                if(body.indexOf('.pdf') > -1){
                    $('#checks_lodaer_containers').hide();
                    window.open('<?php echo $main_dir; ?>/pdf_send.php?show=' + body);
                }
            });
            $('#select_places').on('change', function (e) {
                let place = $(e.target).val();
                if(Number(place) == user_place) {
                    e.preventDefault();
                } else {
                    $('input[name="name"]').removeAttr('required');
                    $('input[name="telephone"]').removeAttr('required');
                    setTimeout(function () {
                        $('#checks_lodaer_containers div h1').hide();
                    }, 500);

                    // $('#submit_order').click();
                }

            });
            $('#select_tools').on('change', function (e) {
                let place = $(e.target).val();
                if(Number(place) == user_type) {
                    e.preventDefault();
                } else {
                    $('input[name="name"]').removeAttr('required');
                    $('input[name="telephone"]').removeAttr('required');
                    setTimeout(function () {
                        $('#checks_lodaer_containers div h1').hide();
                    }, 500);

                    // $('#submit_order').click();
                }

            });
            $('#select_tools').on('change', function () {
                // $('#submit_order').click();
            });
            $('#client_code_search').on('change', async function (e) {
                let container = $('#client_variant_container');
                let val = e.target.value;
                let data = await fetch('<?php echo $main_dir; ?>/zzz.php?client_search=' + val);
                let body = await data.json();
                container.show();
                container.html(' ');
                if(body.length > 0) {
                    console.log('client');
                    body.forEach(element => {
                        let client = `
                        <div class="client_variant" onclick="{$('#client_code_search').val('${element[1]}');$('#client_code_input').val(${element[0]}); $('#client_variant_container').hide(); }">
                            <span>${element[1]}</span>
                        </div>
                    `;
                        container.append(client);
                    });
                } else {
                    console.log('empty');
                    container.html('Совпадений не найдено');
                }

            });

        });
        $(document).ready(function() {
            $('form').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>
    <div class="modal fade" id="selectPart" tabindex="-1" role="dialog" aria-labelledby="selectPartLabel" aria-hidden="true">
        <div class="modal-dialog " role="document" style="min-width: 80% !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="selectPartLabel">Найдите замену</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="targetid" name="targetid" value="">
                    <input type="hidden" id="typeop" name="typeop" value="">
                    <input type="hidden" id="t" name="t" value="">
                    <div class="modal-body-in">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="checks_lodaer_containers">
        <div>
            <img src="<?php echo $laravel_dir; ?>/images/download.gif" alt="">
        </div>
    </div>
    <?php if($_SESSION['order_status'] == 1):?>
        <div class="edit_order_alerts">
            <h4 style="font-size: 14px;">Уважаемый менеджер, Вы находитесь в режиме редактирования заказа №<?php echo $_SESSION['order_akcent_ids']; ?> , данные этого заказа будут обновлены</h4>
            <?php
            //.. 11.08.2022 Проверка на кромки фирмы MAAG, которые больше 1мм
            // Если есть такие, то запрещаем PUR-кромкование
            ?>
            <?php if(isset($_SESSION['pur_edge_denied'])):?>
                <h4 style="background: #fff; padding: 0.2rem 0; font-size: 16px; font-weight: bold; color: red;"> Из-за наличия в проекте MAAG-кромки PUR-кромкование недоступно</h4>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <?php if($_SESSION['order_status'] == 2):?>
        <div class="edit_order_alerts red">
            <h4 style="font-size: 14px;"> Уважаемый менеджер, заказ №<?php echo $_SESSION['order_akcent_ids']; ?>  открыт только для просмотра, так как уже размещён в производство</h4>
            <?php
            //.. 11.08.2022 Проверка на кромки фирмы MAAG, которые больше 1мм
            // Если есть такие, то запрещаем PUR-кромкование
            ?>
            <?php if(isset($_SESSION['pur_edge_denied'])):?>
                <h4 style="background: #fff; padding: 0.2rem 0; font-size: 16px; font-weight: bold; color: red;"> Из-за наличия в проекте MAAG-кромки PUR-кромкование недоступно</h4>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <?php
    //.. 11.08.2022 Проверка на кромки фирмы MAAG, которые больше 1мм
    // Если есть такие, то запрещаем PUR-кромкование
    ?>
    <?php if(isset($_SESSION['pur_edge_denied']) && $_SESSION['order_status'] != 1 && $_SESSION['order_status'] != 2):?>
        <div class="edit_order_alerts red">
             <h4 style="background: #fff; padding: 0.2rem 0; font-size: 16px; font-weight: bold; color: red;"> Из-за наличия в проекте MAAG-кромки PUR-кромкование недоступно</h4>
        </div>
    <?php endif; ?>
    </body>
    </html>