<?php

/**
 * Вывод переменной на экран
**/
function test($text, $value)
{
    echo "<pre>
        -----------------------
        переменная: " . $text . "
        значение: " . $value . "
        ----------------------
    </pre>";
}

/**
 * Форматированный вывод
 * da - dump all
**/
function da($val, $i = false, $allottedSpaceFlag = false){
    echo '<br>';
    if ($allottedSpaceFlag == true) {
        echo '----------------------------------------------------------------------------------------<br>';
    }
    if(is_array($val) || is_object($val)){
        echo "<pre>";
        if($i == false) print_r($val);
        else var_dump($val);
        echo "</pre>";
    }else{
        if($i == false) echo $val;
        else{
            echo "<pre>";
            var_dump($val);
            echo "</pre>";
        }
    }
    if ($allottedSpaceFlag == true) {
        echo '<br>----------------------------------------------------------------------------------------<br>';
    }
}

/**
 * Форматированный вывод
 * de - dump and exit
**/
function de($val = 'Stop!', $i = false, $allottedSpaceFlag = false){
    echo '<br>';
    if ($allottedSpaceFlag == true) {
        echo '----------------------------------------------------------------------------------------<br>';
    }
    if(is_array($val) || is_object($val)){
        echo "<pre>";
        if($i == false) print_r($val);
        else var_dump($val);
        echo "</pre>";
    }else{
        if($i == false) echo $val;
        else{
            echo "<pre>";
            var_dump($val);
            echo "</pre>";
        }
    }
    if ($allottedSpaceFlag == true) {
        echo '<br>----------------------------------------------------------------------------------------<br>';
    }
    exit();
}

/**
 * Форматированный вывод программ и данных проектов (xml-данных)
 * Если переменная $exit установлена, то функция останавливает выполнение кода.
 * dxml - dump xml
**/
function dxml($xml, $exit = false){
    if (!$exit) {
        echo '<textarea rows="10" cols="45">' . str_replace("<", "\r\n<", $xml) . '</textarea>';
    } else {
        echo '<textarea rows="10" cols="45">' . str_replace("<", "\r\n<", $xml) . '</textarea>';exit();
    }
}

/**
 * Вывод строки с переводом строки
 * ds - dump string
**/
function ds($val = null){
    if (!$val) { echo '<br />'; }
    else { echo '<p>'.$val.'</p>'; }    
}

/**
 * Вывод в файл
 * df - dump file
**/
function df($textValue, $path = false) {
    $tmpDir = sys_get_temp_dir();
    if ($path === false) $file = $tmpDir . '/__DEBUG.txt';
    else $file = $path;
    file_put_contents($file, date('Y-m-d h:i:s') . " —-— " . $textValue . "\r\n", FILE_APPEND);
}

function p_($array)
{
    echo '<pre>';
        echo '<br>Вывод массива:<hr>';
        print_r($array);
    echo  '</pre>' . PHP_EOL;
}

function br()
{
    echo "<br><hr>";
}

?>