<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");
header("Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization");
header("Access-Control-Allow-Headers: *");

session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

date_default_timezone_set('Europe/Kiev');
include_once ("func.php");

if(isset($_GET['show'])) {
    header('Content-type:application/pdf');
    $file_url = $_GET['show'];
    $file_url = str_replace('/kronasapp/storage/data/giblab/','/kronasapp/storage/app/data/giblab/', $file_url);
    echo file_get_contents($file_url);
    ///echo file_get_contents($GLOBALS['serv_main_dir'].'/kronasapp/storage/app/data/giblab/'.$_GET['show']);
    exit;
}
if (isset($_SESSION['$session_id']))$_GET['data_id']=$_SESSION['$session_id'];
if(isset($_GET['data_id'])) {
    $session_id = $_GET['data_id'];
    $_SESSION = get_temp_file_data($session_id);

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}
if(isset($_POST['action']) && isset($_POST['project_data'])) {
    $_SESSION['project_data']=$_POST['project_data'];
    $data = step_1_check( $_SESSION,$link,$_SESSION);
    // xml_echo ($data ['project_data']);exit;
    $_POST['project_data']=$data ['project_data'];

    $action = $_POST['action'];
    $project_data = $_POST['project_data'];

    $_SESSION['project_data'] = $project_data;
    $project_data=gl_55(__LINE__,__FILE__,__FUNCTION__,$project_data);

    $data = get_vals_index_without_session($project_data);
    $vals = $data["vals"];
    $vals=vals_out($vals);
    $vals=add_tag_print($vals);
    ksort ($vals);
    $index=make_index($vals);
    $vals=change_gr($vals);

    $project_data=vals_index_to_project($vals);

//папка в которой будет размещен архив
    $archive_dir = $GLOBALS['serv_main_dir']."/kronasapp/public/users_pdf/";
//папка с исходными файлами
    $src_dir = $GLOBALS['serv_main_dir']."/kronasapp/storage/app/data/giblab/";
//создание zip архива
    $zip = new ZipArchive();
//имя файла архива
    $archive_name = "pdf_".time().".zip";
    $fileName = $archive_dir.$archive_name;
    if ($zip->open($fileName, ZIPARCHIVE::CREATE) !== true) {
        fwrite(STDERR, "Error while creating archive file");
        exit(1);
    }
    if($action == 'get_all_pdf') {
        function pdf_action_sender($project_data, $action, $xnc) {

            if($action == '98123591_pdf') {
                $r=build_req_for_project($project_data, '98123591');
            } else  {
                $r=build_req_for_project($project_data, $action);
            }
            $response = gib_serv($r, $action,'Получение файла PDF',__LINE__,__FILE__,'pdf_action_sender',$_SESSION);  ///7A5D19E5
            $file = $response;
            return $file;
        }
        $send_1 = pdf_action_sender($project_data, 'D401090B', $xnc_op_12345);
        if($send_1 != 'stop') {
            $zip->addFile($send_1, 'Чертеж_деталей_и_XNC_операций.zip');
        }
        $send_next = pdf_action_sender($project_data, '7A5D19E5', $xnc_op_12345);
        $zip->addFile($send_next, 'Карты_кроя.zip');
        $new_file = $GLOBALS['serv_main_dir'].'/kronasapp/public/users_pdf/'.$archive_name;
        copy($fileName, $new_file);
    }
    if($action == 'get_all_pdf_manager') {
        function pdf_action_sender($project_data, $action, $xnc) {
            if($action == '98123591_pdf') {
                $r=build_req_for_project($project_data, '98123591');
            } else  {
                $r=build_req_for_project($project_data, $action);
            }

            $response = gib_serv($r, $action,'Получение файла PDF','75','pdf_send','pdf_action_sender',$_SESSION);  ///7A5D19E5
            $file = $response;
            return $file;
        }

        //.. 15.12.2022 Тоценко.
        // Добавлен массив пользователей, которым доступен просмотр наклеек
        if((substr_count($project_data,"typeId=\"LB\"") < 1) && ($_POST['is_admin'] == 'true' || in_array($_SESSION['user']['ID'], $PDF_access)))
        {
            $project_data=str_replace("</project>",file_get_contents('label.txt')."</project>",$project_data);
        }

        $send_3 = pdf_action_sender($project_data, '98123591_pdf', $xnc_op_12345);
        if($is_windows) {
            $send_3 = str_replace('\\', '/', $send_3); 
        }
        echo $send_3;
    }
//закрываем архив
    $zip->close();
    echo $pdf_name;
}

