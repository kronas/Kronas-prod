<?php
session_start();

include_once ('func.php');

//if (!extension_loaded('imagick')) echo 'imagick not installed';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <title>Подтверждение заказа</title>
    <style >
        .part_image_details_container {
            position: absolute;
            width: 781px;
            height: 537px;
            border: 1px solid lightgray;
            display: none;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            background: white;
            overflow: hidden;
            bottom: -74%;
            left: 15vw;
            z-index: 9999;
        }
        .part_name_container:hover .part_image_details_container {
            display: flex;
        }
        .part_name_container {
            position: relative;
        }
        .part_image_details_container img {
            max-width: 100%;
        }
        .detail_image_specification_inner {
            width: 100%;
            height: 300px;
            background: lightgray;
            border: 1px dashed red;
        }
        .left-kromka-container h3 {
            transform: rotate(-180deg);
            -webkit-writing-mode: vertical-rl;
            writing-mode:vertical-rl;
            margin: 0;
        }
        .left-kromka-container {
            text-align: center;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            max-width: 54px;
        }
        .right-kromka-container h3 {
            transform: rotate(0deg);
            -webkit-writing-mode: vertical-rl;
            writing-mode:vertical-rl;
            margin: 0;
        }
        .right-kromka-container {
            text-align: center;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            max-width: 54px;
        }
        .left_background_kromka > div {
            background: red;
            width: 100%;
            padding: 0;
            height: 94%;
        }
        .left_background_kromka {
            max-width: 15px;
            width: 100%;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            padding: 0;
        }
        .right_background_kromka > div {
            background: green;
            width: 100%;
            padding: 0;
            height: 94%;
        }
        .right_background_kromka {
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
            max-width: 15px;
            padding: 0;
        }
        .top_background_kromka {
            max-width: 100%!important;
            border-top: 15px solid blue;
            width: 100%;
            padding-top: 15px!important;
            margin: 0!important;
            margin-bottom: 15px!important;
            text-align: center;
        }
        .bottom_background_kromka {
            border-bottom: 15px solid yellow;
            max-width: 100%!important;
            width: 100%;
            padding-bottom: 15px!important;
            margin: 0!important;
            margin-top: 15px!important;
            text-align: center;
        }
        .main_container_row {
            justify-content: center;
        }
        .kromka_name_marker {
            max-width: 24px;
            padding: 0;
            text-align: center;
            margin-left: 15px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .left_kromka_name_marker {
            max-width: 34px;
            padding: 0;
            text-align: center;
            margin-left: 15px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .right_kromka_name_marker {
            max-width: 24px;
            padding: 0;
            text-align: center;
            margin-right: 15px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .kromka_marker_text {
            text-align: center;
            font-size: 24px;
            font-weight: bold;
        }
        .vertical_kromka_title {
            min-height: 54px;
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            align-items: center;
        }
        .detail_kromka_history {
            display: flex;
            flex-flow: column;
            justify-content: center;
            align-items: center;
            margin-top: 31px;
        }
        .detail_images_links {
            display: flex;
            flex-flow: column;
            justify-content: center;
            align-items: center;
            margin-top: 31px;
        }
    </style>


</head>
<body>

<div class="container">

</div>



</body>
</html>







<?php

$kromki = [
    't' => [
        'letter' => 'A',
        'name' => 'Название кромки',
        'color' => 'red',
        'type' => 'КР',
        'size' => '345',
        't' => '18'
    ],
    'r' => [
        'letter' => 'A',
        'name' => 'Название кромки',
        'color' => 'red',
        'type' => 'КР',
        'size' => '345',
        't' => '18'
    ],
    'b' => [
        'letter' => 'A',
        'name' => 'Название кромки',
        'color' => 'red',
        'type' => 'КР',
        'size' => '345',
        't' => '18'
    ],
    'l' => [
        'letter' => 'A',
        'name' => 'Название кромки',
        'color' => 'red',
        'type' => 'КР',
        'size' => '345',
        't' => '18'
    ]
];

function get_detail_html_data($kromka) {
    $html = <<<rr
        <div>
            <div class="detail_image_specification_container">
                <div class="detail_image_specification">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center vertical_kromka_title">{$kromka['t']['size']}</h3>
                        </div>
                    </div>
                    <div class="row main_container_row">
                        <div class="col-md-1 left-kromka-container">
                            <h3 class="text-center">{$kromka['l']['size']}</h3>
                        </div>
                        <div class="col-md-1 left_background_kromka">
                            <div style="background: {$kromka['l']['color']}"></div>
                        </div>
                        <div class="col-md-1 left_kromka_name_marker"><span class="kromka_marker_text" style="color: {$kromka['l']['color']}">{$kromka['l']['letter']} ({$kromka['l']['type']})</span></div>
                        <div class="col-md-6">
                            <div class="top_background_kromka kromka_name_marker" style="border-top: 15px solid {$kromka['t']['color']};">
                                <span class="kromka_marker_text" style="color: {$kromka['t']['color']}">{$kromka['t']['letter']} ({$kromka['t']['type']})</span>
                            </div>
                            <div>
                                <div class="detail_image_specification_inner">
        
                                </div>
                            </div>
                            <div class="bottom_background_kromka kromka_name_marker" style="border-bottom: 15px solid {$kromka['b']['color']};">
                                <span class="kromka_marker_text" style="color: {$kromka['b']['color']}">{$kromka['b']['letter']} ({$kromka['b']['type']})</span>
                            </div>
                        </div>
                        <div class="col-md-1 right_kromka_name_marker">
                            <span class="kromka_marker_text" style="color: {$kromka['r']['color']}">{$kromka['r']['letter']} ({$kromka['r']['type']})</span>
                        </div>
                        <div class="col-md-1 right_background_kromka">
                            <div style="background: {$kromka['r']['color']}"></div>
                        </div>
                        <div class="col-md-1 right-kromka-container">
                            <h3 class="text-center">{$kromka['r']['size']}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center vertical_kromka_title">{$kromka['b']['size']}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="detail_kromka_history">
                <div class="detail_kromka_history_item" style="color: {$kromka['t']['color']};">
                    <b>{$kromka['t']['letter']}</b><span> - {$kromka['t']['name']}</span>
                </div>
                <div class="detail_kromka_history_item" style="color: {$kromka['r']['color']};">
                    <b>{$kromka['r']['letter']}</b><span> - {$kromka['r']['name']}</span>
                </div>
                <div class="detail_kromka_history_item" style="color: {$kromka['b']['color']};">
                    <b>{$kromka['b']['letter']}</b><span> - {$kromka['b']['name']}и</span>
                </div>
                <div class="detail_kromka_history_item" style="color: {$kromka['l']['color']};">
                    <b>{$kromka['l']['letter']}</b><span> - {$kromka['l']['name']}</span>
                </div>
                </div>
                <div class="detail_images_links">
                    <div class="detail_images_links_item">
                        <a href="">
                            <span>Чертеж детали №1</span>
                            <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    rr;

    return $html;
}


echo get_detail_html_data($kromki);

?>










<!--<html>-->
<!--<head></head>-->
<!--<body>-->
<!-- <img src="--><?//= $main_dir ?><!--/output12074819.5590633.png">-->
<!--</body>-->
<!--</html>-->





