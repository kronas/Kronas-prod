<?php

require_once 'PHPMailer/Exception.php';
require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/SMTP.php';

global $mail_params;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (!defined('DIR_CORE')) {
    require_once '_1/config.php';
}

require_once DIR_FUNCTIONS . 'fs_universal_queries.php';
require_once DIR_FUNCTIONS . 'fs_mini_services.php';
require_once DIR_FUNCTIONS . 'fs_glass.php';
require_once DIR_FUNCTIONS . 'fs_rs.php';
require_once DIR_FUNCTIONS . 'fs_mail.php';
require_once DIR_FUNCTIONS . 'fs_import_vp.php';
require_once DIR_FUNCTIONS . 'fs_fsystem.php';
require_once DIR_FUNCTIONS . 'fs_xnc.php';
require_once DIR_FUNCTIONS . 'fs_calculate.php';
require_once DIR_FUNCTIONS . 'fs_step_1_check.php';
require_once DIR_FUNCTIONS . 'fs_put_program.php';

function gib_serv($POST_DATA, $action, $comment, $line, $file, $function, $session)
{
    if($action == '98123591_pdf') {
        $action = '98123591';
        $get_pdf_status = 1;
    }
    if ($action=='pr_get') 
    {
        $action='C4F6336B';
        $new_get=1;
    }
    // Настройки 
    $timer=timer(1,$file,$function,$line,$comment.'_start_action_'.$action, $session);
    if (!isset($session['manager_id'])) $session=$_SESSION;
    if ($POST_DATA=='')
    {
        $timer=timer(1,$file,$function,$line,$comment.' <p><b>Ошибка</b>: Пустая строка на входе.</p> ', $session);
        return $POST_DATA;
    }
    $url = 'https://service.giblab.com/';
	$headers = array(
		'action: '. $action .'','Accept-Language: ru',
	);
	$tmpDir = sys_get_temp_dir();

	// Отправляем $POST_DATA и принимаем ответ
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_TIMEOUT, 230);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $POST_DATA);
    $response = curl_exec($ch);
    curl_close ($ch);
	// Создаём временный архив (который прислал сервер)
    $temp_file = tempnam($tmpDir, 'tmp');
    // echo $temp_file;
	if ($file = fopen($temp_file,"w")) {
		fwrite($file, $response);
		fclose($file);
    }
    else
    {
		// Если по какой-то причине не удалось создать временный архив
        $filesArray['error'] = '<p><b>Ошибка</b>: не удалось создать временный файл.</p>';
        $timer=timer(1,$file,$function,$line,$comment.'_error_'.$filesArray['error'], $session);

		return $filesArray;
	}
    // p_($temp_file);exit;
	// Получаем файлы из архива
	$zip = new ZipArchive;
	$filesArray = [];
	if (($zip->open($temp_file) === TRUE)) {
		// Извлекаем по-одному файлу, если на каких-то итерациях не удалось создать файл,
		// то данному ключу присваивается сообщение об ошибке, а скрипт продолжает выполняться
            if ($new_get==1) return $temp_file;
		
            for ($i = 0; $i < $zip->numFiles; $i++) {
			$fileName = $zip->getNameIndex($i);
			$zip->extractTo($tmpDir, $fileName);
            // p_($fileName);
            if($get_pdf_status) {
                return $pdf_file_link = $tmpDir.'/'.$fileName;
            }
			if ($file = fopen($tmpDir.'/'.$fileName,"r")) {
				$filesArray[$fileName] = fread($file, filesize($tmpDir.'/'.$fileName));
				fclose($file);
				unlink($tmpDir.'/'.$fileName);
            } else
            {
                $filesArray[$fileName] = '<p><b>Ошибка</b>: не удалось создать файл <b>'.$fileName.'</b>.</p>';
                $timer=timer(1,$file,$function,$line,$comment.'_error_'.$filesArray[$fileName], $session);

            }
		}
	    $zip->close();
		unlink($temp_file);
	} else {
        $t=$comment.'<p><b>Ошибка</b>: ответ сервиса <b>'.file_get_contents($temp_file).'</b>.</p>';
        // p_($t);

        $t=str_replace('"','',$t);
        $t=str_replace("'","",$t);
        $filesArray['error'] = $t;
        $timer=timer(1,$file,$function,$line,$t, $session);

    }
    $timer=timer(1,$file,$function,$line,$comment.'_fin_action_'.$action, $session);
    if($get_pdf_status) {
        return $pdf_file_link;
    }
	return $filesArray;
}


function delete_order_programm($id)
{

    $serv_main_dir=$GLOBALS['serv_main_dir'];
    $sql_get_not_readonly_orders = 'SELECT * FROM `ORDER1C` WHERE `ID` = '. $id;
    $not_readonly_orders = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_get_not_readonly_orders)['data'];
    foreach ($not_readonly_orders as $o) {

        $slq1 = 'SELECT * FROM `PLACES`';
        $oo = sql_data(__LINE__, __FILE__, __FUNCTION__, $slq1)['data'];
        foreach ($oo as $o1)
        {
            if($o['DB_AC_ID'] && $o['DB_AC_NUM'] && $o1['PLACES_ID']) {

                $slq_get_tool_equip = 'SELECT * FROM `TOOL_CUTTING_EQUIPMENT` WHERE `place` = ' . $o1['PLACES_ID'];
                $tools_equip = sql_data(__LINE__, __FILE__, __FUNCTION__, $slq_get_tool_equip)['data'];

                $slq_get_tool_equip_xnc = 'SELECT * FROM `tool_xnc_equipment` WHERE `place` = ' . $o1['PLACES_ID'];
                $tools_equip_xnc = sql_data(__LINE__, __FILE__, __FUNCTION__, $slq_get_tool_equip_xnc)['data'];

                p_('<hr><span>Удаляем файлы заказа '.$o['DB_AC_ID'].' ('.$o['DB_AC_NUM'].') в '.$o1['NAME'].' ('.$o1['ADRESS'].') :</span><br><hr>');
                // p_($o);
                $r[]=$o['DB_AC_ID'];
                /// Delete files form Accent folder
                $accent_dir = $serv_main_dir . '/files/accent/' . $o['DB_AC_ID'];
                // test(1,$accent_dir);
                if (is_dir($accent_dir)) {
                    echo '<hr><br><span>Удаляем директорию Акцента:</span><br>' . $accent_dir . '<hr><br><br><br>';
                    if(!delDir($accent_dir)) $res=1;
                }


                /// Delete LC4 files
                foreach ($tools_equip as $t) {
                    if ($t['net_folder']) {
                        // unset ($ret_str);
                        // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['net_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                        $ret_str = del_filter_files($serv_main_dir.'/'.$t['net_folder'], $o['DB_AC_ID']);
                        // echo '--- '; var_dump($ret_str); echo '<br />';
                        // if ($ret_str) $res=1;
                        // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['net_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                        $ret_str = del_filter_files($serv_main_dir.'/'.$t['net_folder'], strrev($o['DB_AC_ID']));
                        // echo '--- '; var_dump($ret_str); echo '<br />';
                        
                    }
                    if ($t['local_folder']) {
                        // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['local_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                        $ret_str = del_filter_files($serv_main_dir.'/'.$t['local_folder'], $o['DB_AC_ID']);
                        // echo '--- '; var_dump($ret_str); echo '<br />';
                        // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['local_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                        $ret_str = del_filter_files($serv_main_dir.'/'.$t['local_folder'], strrev($o['DB_AC_ID']));
                        // echo '--- '; var_dump($ret_str); echo '<br />';
                    }

                }


                /// Delete MPR files
                $actual_year = date('Y');
                $actual_month_number = date('m');
                $actual_month1_string = get_month_name($actual_month_number);
                foreach ($tools_equip_xnc as $t) {
                    if($t['net_folder']) {
                        $equip_folder_bhx = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/BHX/';
                        $equip_folder_venture = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/Venture/';
                    }
                    if($t['local_folder']) {
                        $equip_folder_new_bhx = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                        $equip_folder_new_venture = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                    }

                    $folders = [];

                    if($equip_folder_bhx) {
                        $folders[] = $equip_folder_bhx;
                    }
                    if($equip_folder_venture) {
                        $folders[] = $equip_folder_venture;
                    }
                    if($equip_folder_new_bhx) {
                        $folders[] = $equip_folder_new_bhx;
                    }
                    if($equip_folder_new_venture) {
                        $folders[] = $equip_folder_new_venture;
                    }

                    foreach ($folders as $d) {
                        if (is_dir($d)) {
                        echo '<hr><br><span>Удаляем папку с МПР:</span><br>' . $d . '<hr><br><br><br>';
                        delDir($d);
                        }
                    }

                    $next_month_number = $actual_month_number - 1;
                    $next_month1_string = get_month_name($next_month_number);

                    if($t['net_folder']) {
                        $equip_folder_bhx = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/BHX/';
                        $equip_folder_venture = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/Venture/';
                    }
                    if($t['local_folder']) {
                        $equip_folder_new_bhx = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                        $equip_folder_new_venture = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                    }


                    $folders = [];

                    if($equip_folder_bhx) {
                        $folders[] = $equip_folder_bhx;
                    }
                    if($equip_folder_venture) {
                        $folders[] = $equip_folder_venture;
                    }
                    if($equip_folder_new_bhx) {
                        $folders[] = $equip_folder_new_bhx;
                    }
                    if($equip_folder_new_venture) {
                        $folders[] = $equip_folder_new_venture;
                    }

                    foreach ($folders as $d) {
                        if (is_dir($d)) {
                        echo '<hr><br><span>Удаляем папку с МПР:</span><br>' . $d . '<hr><br><br><br>';
                        delDir($d);
                        }
                    }
                }
            }
        }
        if (!isset($res2))
        {
            $sql = 'UPDATE ORDER1C SET del_programm=4, stanki=NULL, stanki_time=NULL WHERE ID='.$o['ID'];
            $not_readonly_orders = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
        }
    }
    return $r;
}

function check_db ()
{
    $sql='SELECT * FROM RS_decor_profile;';
    $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($res as $r)
    {
        $sql='SELECT * FROM RS_decor_profile WHERE `simple`='.$r['simple'].' 
        AND `profile_article`='.$r['profile_article'].' AND
        `type_profile_article`="'.$r['type_profile_article'].'" AND
        `decor`='.$r['decor'].' AND `profile`='.$r['profile'].' AND
        `rs_decor_profile_id`<> '.$r['rs_decor_profile_id'].'
        ;';
        $res1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
        if (count($res1)>0)
        foreach ($res1 as $r1)
        {
            $sql='DELETE FROM RS_decor_profile WHERE `rs_decor_profile_id`= '.$r1['rs_decor_profile_id'].';';
            // p_($r);
            // p_($r1);
            $res2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
        }
    }
}


function change_gr($vals)
{
    
    $vals=vals_out($vals);
    $index=make_index($vals);
    // p_($vals);
    ksort($vals);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'gr_change_start_start', $session);

    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=="GR")&&($vals[$i]['type']=="open"))
        {
            if ($vals[$i]['attributes']['GROFFSETINCL']=="true")
            {
                $delta=$vals[$i]['attributes']['GROFFSET']-$vals[$i]['attributes']['GRWIDTH']/2;
            }
            else
            {
                $delta=$vals[$i]['attributes']['GROFFSET']+$vals[$i]['attributes']['GRWIDTH']/2;
            }
            $ii=$i+1;
            // p_($vals[$i]);
            while ($vals[$ii]['tag']=="PART")
            {
                // p_($vals[$ii]);
                foreach ($index["PART"] as $p)
                {
                    // p_($vals[$p]);
                   
                    if (($vals[$p]['attributes']['CL']>0)&&($vals[$p]['attributes']['ID']==$vals[$ii]['attributes']['ID']))
                    {
                        // $part=get_all_part($vals,$vals[$p]['attributes']['ID']);
                        // $e4=get_cs_tool__sheet_part($vals,$vals[$ii]['attributes']['ID']);
                        // // p_($e4);exit;
                        // if ($e4['cs']['attributes']['CSIZEMODE']==1)
                        // {
                        //     $x=$vals[$p]['attributes']['DL'];
                        //     $y=$vals[$p]['attributes']['DW'];
                        // }
                        // elseif ($e4['cs']['attributes']['CSIZEMODE']==2) 
                        // {
                        //     $x=$vals[$p]['attributes']['CL'];
                        //     $y=$vals[$p]['attributes']['CW'];
                        // }
                        $t=array(
                            'L'=>array('x1'=>$delta,'y1'=>0,'x2'=>$delta,'y2'=>'dy'),
                            'R'=>array('x1'=>'dx-'.$delta,'y1'=>0,'x2'=>'dx-'.$delta,'y2'=>'dy'),
                            'B'=>array('x1'=>0,'y1'=>$delta,'x2'=>'dx','y2'=>$delta),
                            'T'=>array('x1'=>0,'y1'=>'dy-'.$delta,'x2'=>'dx','y2'=>'dy-'.$delta)
                        );
                        unset($xnc_id,$xn_id_side);
                        foreach (SIDE as $s)
                        {
                            
                            $s=mb_strtoupper($s);

                            if ($vals[$p]['attributes']['GR'.$s]=='@operation#'.$vals[$i]['attributes']['ID'])
                            {
                                // p_($vals[$i]['attributes']['ID']);
                                // p_($vals[$p]['attributes']);
                                if (!isset($xnc_id))
                                {  
                                    foreach ($index["OPERATION"] as $j)
                                    {
                                        if (($vals[$j]['attributes']['TYPEID']=="XNC")&&($vals[$j]['attributes']['SIDE']=="true")&&
                                        ((!isset($part['part']['attributes']['GR'.$s.'S'])OR($part['part']['attributes']['GR'.$s.'S']=="true"))))
                                        {
                                            $jj=$j+1;
                                            while ($vals[$jj]['tag']<>"PART")
                                            {
                                                $jj++;
                                            }
                                            if ($vals[$jj]['attributes']['ID']==$vals[$ii]['attributes']['ID'])
                                            {
                                                // p_($vals[$j]);
                                                $xnc_id=$j;
                                                $xn_id_side='true'; 
                                            } 
                                        }
                                        elseif (($vals[$j]['attributes']['TYPEID']=="XNC")&&($vals[$j]['attributes']['SIDE']=="false")&&
                                        ($part['part']['attributes']['GR'.$s.'S']=="false"))
                                        {
                                            $jj=$j+1;
                                            while ($vals[$jj]['tag']<>"PART")
                                            {
                                                $jj++;
                                            }
                                            if ($vals[$jj]['attributes']['ID']==$vals[$ii]['attributes']['ID'])
                                            {
                                                // p_($vals[$j]);
                                                $xnc_id=$j; 
                                                $xn_id_side='false'; 
                                            } 
                                        }
                                    }
                                }
                                // p_($xnc_id);
                                // p_($xn_id_side);
                                // exit;
                                // test('s',$s);
                                // test('det',$vals[$ii]['attributes']['ID']);
                                // p_($vals[$xnc_id+1]);
                                if (isset($xnc_id)&&($vals[$xnc_id]['attributes']['SIDE']==$xn_id_side))
                                {
                                    $p1 = xml_parser_create();
                                    xml_parse_into_struct($p1, $vals[$xnc_id]['attributes']['PROGRAM'], $v1, $i125);
                                    xml_parser_free($p1);
                                    unset ($gr_in);
                                    foreach ($i125['TOOL'] as $tool)
                                    {
                                        // p_($tool);
                                        //.. 05.07.2021 Пила заменена с 4мм на 3.2мм
                                        if (($v1[$tool]['attributes']['NAME']=="Cut3.2")&&($v1[$tool]['attributes']['D']=="3.2"))
                                        {
                                           
                                            $gr_in=1;
                                        }
                                    }
                                    if (!isset($gr_in))
                                    {
                                        // exit;
                                        $v1=move_vals($v1,1,1);
                                        //.. 05.07.2021 Пила заменена с 4мм на 3.2мм
                                        $v1[1]=array(
                                            'tag'=>'TOOL','type'=>'complete','level'=>2,'attributes'=>array(
                                                'comment'=>"Пазовальная пила толщиной 3.2мм","name"=>"Cut3.2","d"=>"3.2"
                                            )
                                        );
                                        ksort ($v1);
                                    }
                                    //.. 05.07.2021 Пила заменена с 4мм на 3.2мм
                                    $gggr=array(
                                        'tag'=>'GR','type'=>'complete','level'=>2,'attributes'=>array(
                                            "name"=>"Cut3.2","c"=>"0","t"=>$vals[$i]['attributes'][mb_strtoupper('grWidth')],
                                            "dp"=>$vals[$i]['attributes'][mb_strtoupper('grDepth')],
                                            "x1"=>$t[$s]['x1'],
                                            "x2"=>$t[$s]['x2'],
                                            "y1"=>$t[$s]['y1'],
                                            "y2"=>$t[$s]['y2']
                                        )
                                    );
                                    // p_($gggr);exit;
                                    $v1=vals_into_end($v1,$gggr);
                                    $vals[$xnc_id]['attributes']['PROGRAM']=vals_index_to_project($v1);
                                    
                                }
                                else
                                {
                                    $xnc_in=Array
                                    (
                                        "tag" => "OPERATION",
                                        "type" => "open",
                                        "level" => 2,
                                        "attributes"=> Array ("ID"=>new_id($vals)+mt_rand(450,34500))
                                    );
                                    $xnc_in['attributes'][mb_strtoupper('bySizeDetail')]='true';
                                    $xnc_in['attributes'][mb_strtoupper('mirHor')]='false';
                                    $xnc_in['attributes'][mb_strtoupper('mirVert')]='true';
                                    $xnc_in['attributes'][mb_strtoupper('typeId')]='XNC';
                                    if (!isset($xn_id_side)) $xn_id_side="true";
                                    $xnc_in['attributes'][mb_strtoupper('side')]=$xn_id_side;
                                    $xnc_in['attributes'][mb_strtoupper('typename')]='Деталь id='.$vals[$p]['attributes']['ID'].' (лицо)';
                                    $xnc_in['attributes'][mb_strtoupper('printable')]='true';
                                    $xnc_in['attributes'][mb_strtoupper('startNewPage')]='true';
                                    if($xnc_in['attributes']["SIDE"]=='true') $sides='f';
                                    else $sides='b';
                                    $xnc_in['attributes'][mb_strtoupper('code')]=$vals[$i]['attributes']['ID'].'_'.$hgf++.'_PAZ_'.$sides;
                                    $xnc_in['attributes'][mb_strtoupper('turn')]=0;
                                    $xnc_pr=array(
                                        array(
                                            'tag'=>'PROGRAM','type'=>'open','level'=>1,'attributes'=>array(
                                                'DX'=>$vals[$p]['attributes']['DL'],
                                                'DY'=>$vals[$p]['attributes']['DW'],
                                                'DZ'=>$vals[$p]['attributes']['T']
                                            )
                                        )
                                    );
                                    //.. 05.07.2021 Пила заменена с 4мм на 3.2мм
                                    $xnc_pr[]=
                                    array(
                                        'tag'=>'TOOL','type'=>'complete','level'=>2,'attributes'=>array(
                                            'comment'=>"Пазовальная пила толщиной 3.2мм","name"=>"Cut3.2","d"=>"3.2"
                                        )
                                    );
                                    //.. 05.07.2021 Пила заменена с 4мм на 3.2мм
                                    $xnc_pr[]=
                                    array(
                                        'tag'=>'GR','type'=>'complete','level'=>2,'attributes'=>array(
                                            "name"=>"Cut3.2","c"=>"0","t"=>$vals[$i]['attributes'][mb_strtoupper('grWidth')],
                                            "dp"=>$vals[$i]['attributes'][mb_strtoupper('grDepth')],
                                            "x1"=>$t[$s]['x1'],
                                            "x2"=>$t[$s]['x2'],
                                            "y1"=>$t[$s]['y1'],
                                            "y2"=>$t[$s]['y2']
                                        )
                                    );
                                    $xnc_pr[]=
                                        array(
                                            'tag'=>'PROGRAM','type'=>'close','level'=>1
                                        );
                                    $xnc_in['attributes'][mb_strtoupper('program')]=vals_index_to_project($xnc_pr);
                                    $xnc_out=
                                        array(
                                            'tag'=>'OPERATION','type'=>'close','level'=>1
                                        );
                                    
                                    $xnc_part=Array
                                            (
                                                "tag" => "PART",
                                                "type" => "complete",
                                                "level" => 2,
                                                "attributes"=> Array ("ID"=>$vals[$p]['attributes']['ID'])
                                            );
                                            // p_($xnc_in);exit;
                                    $vals=vals_into_end($vals,$xnc_in);
                                    $vals=vals_into_end($vals,$xnc_part);
                                    $vals=vals_into_end($vals,$xnc_out);
                                    // $index=make_index($vals);
                                    unset($xnc_pr);
                                    // p_($vals);exit;
                                    // p_($index);
                                    
                                }
                                unset ($xnc_id);

                            }
                            
                        }
                    }
                }
                // unset ($vals[$ii]);
                $ii++;
            }
            // p_($i);
            // unset ($vals[$i]);
            // $iii=$i+1;
            // while (($vals[$iii]['tag']<>"OPERATION")&&($vals[$iii]['type']<>"close"))
            // {
            //     unset ($vals[$iii]);
            //     $iii++;

            // }
            // unset ($vals[$iii]);
            
            $index=make_index($vals);

            //    xml($vals);exit;
        }
    }
    // ksort ($vals);

    
    $index=make_index($vals);
    // xml ($vals);

    foreach ($index["PART"] as $i)
    {
        foreach (SIDE as $s)
        {
            if (isset($vals[$i]['attributes']['GR'.mb_strtoupper($s)]))unset($vals[$i]['attributes']['GR'.mb_strtoupper($s)]);
        }
    }
    // 
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=="GR")&&($vals[$i]['type']=="complete"))
        {
            unset ($vals[$i]);
        }
        elseif (($vals[$i]['attributes']['TYPEID']=="GR")&&($vals[$i]['type']=="open"))
        {
            unset ($vals[$i]);
            $i++;
            while ($vals[$i]['tag']=="PART")
            {
                unset ($vals[$i]);
                $i++;
            }
            unset ($vals[$i]);
        }
    }
    $vals=vals_out ($vals);
    ksort($vals);
    // p_($vals);
    // exit;
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'gr_change_start_fin', $session);
// xml ($vals);
    return $vals;
}
function vp_put_source_in($vals,$link,$arr,$place)
{
    // p_($arr);exit;
    if((isset($vals))&&($place>0))
    {
        foreach ($arr['sheet_in'] as $sheet_k=>$ssheet)
        {
            // p_($ssheet);exit;
            //.. Выбираем станок, филиал, папку размещения программ и т.д.
            $sql="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
            inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
            where type = 'auto' and product = 'sh' and PLACE = ".$place." LIMIT 1";
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3))
                {
                    $tool["attributes"][$k3]=$v3;
                }
            }
            $tool["attributes"]["ID"]=new_id($vals);
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
            unset ($mat,$c);
            //.. Ищем в базе совпадающий по толщине (либо толще) материал
            while ((!isset($mat))&&($c<10))
            {
                $sql='SELECT * FROM MATERIAL WHERE MY_1C_NOM=5931 AND T='.$ssheet[2];
                // echo $sql;
                $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                if (!isset($mat))
                {
                    $ssheet[2]=ceil($ssheet[2])+1;
                    $c++;
                }
            }
            if (!isset($mat))
            {
                echo 'Материал ('.$ssheet[0].'/'.$ssheet[1].'/'.$ssheet[2].') не найдено вариантов в нашей БД.';
                exit;
            }
            // p_($mat);
            $sheet=Array
                (
                    "tag" => "GOOD",
                    "type" => "open",
                    "level" => 2
                );
            
            $sheet["attributes"]["W"]=$ssheet[1];
            $sheet["attributes"]["L"]=$ssheet[0];
            $sheet["attributes"]["T"]=$ssheet[2];
            $sheet["attributes"]["UNIT"]=$mat["UNIT"];
            $sheet["attributes"]["TYPENAME"]="Листовой";
            $sheet["attributes"]["COUNT"]=10000;
            //.. Название материала выводится начиная с "!!_"
            $sheet["attributes"]["NAME"]='!!_'.$ssheet[3];
            $sheet["attributes"]["ID"]=new_id($vals)+112;
            $sheet["attributes"]["DESCRIPTION"]=$mat['NAME'];
            $sheet["attributes"]["part.name1"]=$mat['NAME'];
            $sheet["attributes"]["TYPEID"]="sheet";
            $sheet["attributes"]["CODE"]=$mat["CODE"];
            $sheet_part=
                Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> 
                    Array (
                        "L"=>$ssheet[0],
                        "W"=>$ssheet[1],
                        "COUNT"=>10000,
                        "USEDCOUNT"=>0,
                        "ID"=>new_id($vals)+555
                    )
                );
            // p_($sheet);exit;
            $sheet2=Array
            (
                "tag" => "GOOD",
                "type" => "close",
                "level" => 2
            );
            $operation=Array
            (
                "tag" => "OPERATION",
                "type" => "open",
                "level" => 2
            );
            $operation["attributes"]["ID"]=new_id($vals)+2222;
            $operation["attributes"]["TYPEID"] = 'CS';
            $operation["attributes"]["TYPENAME"] = "Листовой раскрой";
            $operation["attributes"]["CTRIML"] = 8;
            $operation["attributes"]["CTRIMW"] = 8;
            $operation["attributes"]["CFILLREP"] = 0.9;
            $operation["attributes"]["CSTEXTURE"] = "true";
            $operation["attributes"]["PRINTABLE"] = "true";
            $operation["attributes"]["STARTNEWPAGE"] = "true";
            $operation["attributes"]["W"]=$ssheet[1];
            $operation["attributes"]["T"]=$ssheet[2];
            $operation["attributes"]["L"]=$ssheet[0];
            $operation["attributes"]["CSIZEMODE"] =1; 
            $operation["attributes"]["TOOL1"] =  $tool["attributes"]["ID"];
            $material=Array
            (
                "tag" => "MATERIAL",
                "type" => "complete",
                "level" => 2
            );
            $material["attributes"]["ID"]=$sheet["attributes"]["ID"];
            $operation2=Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
            $parts2=Array
            (
                "tag" => "PART",
                "type" => "complete",
                "level" => 2,
                "attributes"=> Array ("ID"=> $sheet_part["attributes"]["ID"])
            );
            // $vals=move_vals($vals,count($vals)-1,1);
            $vals=vals_into_end($vals,$tool);
            $vals=vals_into_end($vals,$sheet);
            $vals=vals_into_end($vals,$sheet_part);
            $vals=vals_into_end($vals,$sheet2);
            $vals=vals_into_end($vals,$operation);
            $vals=vals_into_end($vals,$material);
            $vals=vals_into_end($vals,$parts2);
            $vals=vals_into_end($vals,$operation2);
        }
        ksort($vals);
        $vals=vals_out($vals);
        $res['cs_id']=$operation["attributes"]["ID"];
        $index=make_index($vals);
        foreach ($arr['band_in'] as $band)
        {
            // p_($band);exit;
            $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
            inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
            where PLACE = ".$place." LIMIT 1";
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3))
                {
                    $tool["attributes"][$k3]=$v3;
                }
            }
            $res['el_elWidthPreJoint']=$tool["attributes"][mb_strtoupper('elWidthPreJoint')];
            
            $tool["attributes"]["ID"]=new_id($vals);
            unset ($sql3,$c);
            // p_($band);
            while ((!isset($sql3))&&($c<10))
            {
                $band[0]=strval($band[0]);
                $band[1]=strval($band[1]);
                $sql="SELECT * from BAND where MY_1C_NOM=6149 AND W=".$band[0]." AND T=".$band[1];
                // p_($link);
                $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                // echo($sql);echo'
                
                // ';  
                if (!isset($sql3))
                {
                    $band[0]=$band[0]+0.1;
                    $c++;
                }
            }
            if (!isset($sql3))
            {
                echo 'Кромка ('.$band[0].'/'.$band[1].') не найдено вариантов в нашей БД.';
                exit;
            }
            // exit;
            $band1=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3))
                {
                    $band1["attributes"][$k3]=$v3;
                }
            }
            $res['band_code']=$band1["attributes"]['CODE'];
            $res['band_t']=$band1["attributes"]['T'];
            $band1["attributes"]['NAME']='!!_'.$band[2];
            $band1["attributes"]["ID"]=new_id($vals)+1;
            $operation=Array
                (
                    "tag" => "OPERATION",
                    "type" => "open",
                    "level" => 2
                );
            $operation["attributes"]["ID"]=new_id($vals)+2;
            $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
            $operation["attributes"]["W"]=$band[0];
            $operation["attributes"]["T"]=$band[1];
            $operation["attributes"]["L"]=10000;
            $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
            $operation["attributes"]["ELCALCMAT"]="true";
            $operation["attributes"]["ELROUNDMAT"]="true";
            $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
            if ($sql3['T']<1)
            {
                $operation["attributes"]["ELCOLOR"]="rgb(255,".mt_rand(0,5).",".mt_rand(0,5).")";
                $operation["attributes"]["ELLINEMARK"]="1";
            }
            else
            {
                $operation["attributes"]["ELCOLOR"]="rgb(".mt_rand(0,5).",255,".mt_rand(0,5).")";
                $operation["attributes"]["ELLINEMARK"]="3";
            }
            $operation["attributes"]["TYPEID"]="EL";
            $material=Array
                (
                    "tag" => "MATERIAL",
                    "type" => "complete",
                    "level" => 2
                );
            $material["attributes"]["ID"]=new_id($vals)+1;
            $operation2=Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
        
            $vals=vals_into_end($vals,$tool);
            $vals=vals_into_end($vals,$band1);
            $vals=vals_into_end($vals,$operation);
            $vals=vals_into_end($vals,$material);
            $vals=vals_into_end($vals,$operation2);
            $vals=vals_out($vals);
            ksort($vals);
            $index=make_index($vals);
        }
        ksort($vals);
        $res['vals']=$vals;
        $res['el_id']=$operation["attributes"]["ID"];

        $vals=check_product_sheet_band ($vals);
        return $res;
    }
}
function check_opt_part ($vals, $exit = null)
{
    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    // p_($vals);
    foreach ($index["PART"] as $i)
    {
        if (($vals[$i]['attributes']['CL']>0)&&($vals[$i]['attributes']['COUNT']>$vals[$i]['attributes']['USEDCOUNT']))
        {
            $r=get_all_part($vals,$vals[$i]['attributes']['ID']);
            if (count($r['cl'])<1)
            {
                $part_problem[$vals[$i]['attributes']['ID']] = $vals[$i];
                $e4=part_get_material($vals, $vals[$i]["attributes"]["ID"]);
                $part_problem[$vals[$i]['attributes']['ID']]['sheet']=$e4;
            }
        }
    }
    if (count ($part_problem)>0 && $_POST['go_check_sheets_part'] != 1)
    {

        if(!$exit) 
        {
            echo ' Детали, которые не вошли в раскрой:';
            $html = '
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Название</th>
                  <th scope="col">Длина</th>
                  <th scope="col">Ширина</th>
                  <th scope="col">Не вошло в раскрой шт</th>
                  <th scope="col">Материал</th>
                </tr>
              </thead>
              <tbody>
            ';
            foreach ($part_problem as $id=>$part)
            {
                if (!isset($part['attributes']['NAME'])) $part['attributes']['NAME']='деталь без названия ';
                $under = $part['attributes']['COUNT']-$part['attributes']['USEDCOUNT'];
                $html .= <<<rrr
                    <tr>
                      <td>{$part['attributes']['NAME']}</td>
                      <td>{$part['attributes']['DL']}</td>
                      <td>{$part['attributes']['DW']}</td>
                      <td>{$under}</td>
                      <td>{$part_problem[$id]['sheet']['attributes']['NAME']}</td>
                    </tr>
                rrr;
            }
            $html .= '
             </tbody>
            </table>
            <div style="display: flex; flex-flow: row; align-items: center">
                <a href="'.$GLOBALS['laravel_dir'].'" class="btn btn-danger">Назад к редактированию</a>
                <!-- <form action="check_sheet_parts_test.php?data_id='.$_GET['data_id'].'" method="POST">
                        <input type="hidden" name="go_check_sheets_part" value="1">
                        <button type="submit" class="btn btn-success">OK</button>
                </form> -->
                
            </div>
            ';

            echo $html;
            $_SESSION['part_problem_show'] = 1;
            exit;
        } else {
            my_mail('Деталь не вошла в раскрой: '.$vals[0]['attributes']['NAME'].__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'Деталь не вошла в раскрой '.$vals[0]['attributes']['NAME'],'error','txt');
        }
    }
    
}

function put_material_client ($array,$vals,$link)
{
    // p_($array);exit;
    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    foreach ($array as $k=>$v)
    {
        unset ($stop);
        foreach ($index['GOOD'] as $i)
        {
            if ($vals[$i]['attributes'][mb_strtoupper('good.my_client_material_id')]==$array[$k]['id'])
            {
                $stop=1;
            }
            
        }
        if (!isset($stop))
        {
            $sql="SELECT * FROM MATERIAL WHERE MATERIAL_ID=".$v['id'];
            // $sql="SELECT * FROM MATERIAL WHERE MATERIAL_ID=4208";
            $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
            
            // p_($mat);exit;
            if ($mat['T']>0)
            {
                $sql="SELECT * FROM MATERIAL WHERE T=".$mat['T']." AND MY_1C_NOM=5931;";
                // echo $sql;
                $mat2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                while(!isset($mat2))
                {
                    if (!isset($mail)) my_mail('Проблема с материалом клиента'.__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'Просили материал '.$mat['T'],'error','txt');
                    $mail=1;
                    $_SESSION['error_ok'].='Материал клиента толщиною '.$mat['T'].'мм не введен в БД, используем код материала толщиной '.ceil($mat['T']).'мм. Это ошибка, которую быстро устраним, свяжитесь пожалуйста с менеджером и проблема будет решена.';
                    $mat['T']=ceil($mat['T']);
                    $mat['T']++;
                    $sql="SELECT * FROM MATERIAL WHERE T=".$mat['T']." AND MY_1C_NOM=5931;";
                // echo $sql;
                    $mat2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                }
            // p_($mat2);exit;

                $array[$k]['code']=$mat2['CODE'];
                // $array[$k]['material_client_id']=4208;
                // if (isset($v['client_code']))
                // {
                //     $v['client_code']=231026;
                //     $array[$k]['client_code']=231026;
                // }
                // if (isset($v['place']))
                // {
                //     $v['place']=1;
                // }
                $array[$k]['material_client_id']=$v['id'];
                $array[$k]['client_id']=$v['client_code'];
                $array[$k]['place']=$v['place'];
                // p_($mat);
                $ar_in=array('sheet_in'=>array($array[$k]['material_client_id']),
                            'sheet_in_ad'=>array($array[$k])
                            );
                            // p_($ar_in);exit;
                $vals=put_source_in($vals,$link,$ar_in,$array[$k]['place']);
            }
            
        }
        else  $_SESSION['error_ok'].='Материал клиента [/'.$array[$k]['id'].'/] уже внесён в проект. Увеличьте количество материала во вкладке "листы и остатки", если при внесении указали менее нужного. При вводе количества, больше фактического, система скорректирует при проверке остаток на правильный.';
    }
    // xml($vals);
    return $vals;
}
function put_source_in_cl($vals,$goods_pr,$goods_pr_type,$goods)
{
    $vals=vals_out ($vals);
    $ndex=make_index ($vals);
    // p_($goods_pr_type);
    // p_($goods);
    // exit;
    if(!isset($_SESSION['user_place']))$_SESSION['user_place']=1;
    $arr_st=RS_st_from_akcent($goods_pr,$_SESSION['user_place']);
    // p_($arr_st);exit;
    foreach ($goods_pr as $simple_id=>$count)
    {
        unset($no);
        foreach($count[0] as $k=>$v) if ($k==0) $no=1;
        if(!isset($no))
        {
            $sql='SELECT * FROM SIMPLE WHERE SIMPLE_ID='.$simple_id;
            // echo $sql;exit;
            $simple=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
            $vals=move_vals($vals,1,1);
            $vals[1]=Array
            (
                "tag" => "GOOD",
                "type" => "open",
                "level" => 2,
                "attributes"=>[
                "NAME"=>'Погонаж профиля '.$simple['NAME'],
                'TYPEID'=>'product',
                'COUNT'=>1,
                'ID'=>new_id($vals)+1130
                ]
            );

            $vals=move_vals($vals,2,1);
            $vals[2]=Array
            (
                "tag" => "GOOD",
                "type" => "close",
                "level" => 2,
                
            );
            ksort ($vals);

            foreach($count as $c=>$l)
            {
                foreach ($l as $l1=>$l2)
                {
            // p_($l1.$l2);exit;
                    if($l1>0)
                    {
                        $vals=move_vals($vals,2,1);
                        $vals[2]=Array
                        (
                            "tag" => "PART",
                            "type" => "complete",
                            "level" => 2,
                            "attributes"=>[
                            "NAME"=>'',
                            'CL'=>$l2*1000,
                            'L'=>$l2*1000,
                            'DL'=>$l2*1000,
                            'JL'=>$l2*1000,
                            'CW'=>50,
                            'W'=>50,
                            'DW'=>50,
                            'JW'=>50,
                            'COUNT'=>$l1,
                            'ID'=>new_id($vals)+11230
                            ]
                        );
                        $id[]=$vals[2]['attributes']['ID'];
                        ksort($vals);
                        $index=make_index($vals);
                    }
                    
                }
            }
            $index=make_index($vals);
            // xml($vals);
            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=>[
                    "SWSAWTHICK"=>rs_general('saw'),
                        "SWMAXLENGTHBAND"=>6000,
                        mb_strtoupper('swPackageHeight')=>18,
                        'TYPEID'=>'tool.cutting'
                    ]
                );
            
            
            $tool["attributes"]["ID"]=new_id($vals);
            
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
            
            

                // p_($mat);
            $band=Array
            (
                "tag" => "GOOD",
                "type" => "open",
                "level" => 2
            );
            if (!isset($simple['L'])) $simple['L']=5000;
        
            $band["attributes"]["CODE"]=$simple["CODE"];
            $band["attributes"]["SIMPLE_ID"]=$simple["SIMPLE_ID"];
            $band["attributes"]["T"]=17;
            $band["attributes"]["L"]=$simple['L'];
            $band["attributes"]["UNIT"]=$simple["UNIT"];
            $band["attributes"]["TYPENAME"]="Погонажный";
            $band["attributes"]["COUNT"]=10000;
            $band["attributes"]["NAME"]=$simple['NAME'];
            $band["attributes"]["ID"]=new_id($vals)+11230;
            $band["attributes"]["DESCRIPTION"]=$simple['NAME'];
            $band["attributes"]["TYPEID"]="band";
            $sheet["attributes"]["CODE"]=$simple["CODE"];
            unset ($band_part);
            // p_($arr_st);
            foreach ($arr_st as $k=>$v)
            {
                if (($k<>'code')&&($k==$simple["CODE"]))
                {
                    
                    foreach ($v as $k1=>$v1)
                    {
                        if (($k1==$simple["CODE"])&&($v1>0))
                        {
                            $band_part[]=
                            Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> 
                                Array (
                                    "L"=>$simple["L"],
                                    "W"=>50,
                                    "COUNT"=>$v1,
                                    "USEDCOUNT"=>0,
                                    "ID"=>new_id($vals)+mt_rand(0,1000000),
                                    'part.rs_real'=>1,
                                    'NAME'=>'М.ПОГ '.$k1.' / '.$simple['NAME'],
                                    'TYPENAME'=>'М.ПОГ '.$k1.' / '.$simple['NAME'],
                                    'DESCRIPTION'=>'М.ПОГ '.$k1.' / '.$simple['NAME']
                                )
                            );
                        }
                        elseif (($k1<>$simple["CODE"])&&($v1>0))
                        {
                            $sql='SELECT * FROM SIMPLE WHERE CODE='.$k1;
                            $simple_con=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                            $band_part[]=
                            Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> 
                                Array (
                                    "L"=>$simple_con["L"],
                                    "W"=>50,
                                    "COUNT"=>$v1,
                                    "USEDCOUNT"=>0,
                                    "ID"=>new_id($vals)+mt_rand(0,1000000),
                                    'part.rs_real'=>1,
                                    'NAME'=>'ШТ '.$k1.' / '.$simple_con['NAME'],
                                    'TYPENAME'=>'ШТ '.$k1.' / '.$simple_con['NAME'],
                                    'DESCRIPTION'=>'ШТ '.$k1.' / '.$simple_con['NAME']
                                )
                            );
                        }
                    }
                }
            }
            $band_part[]=
            Array
            (
                "tag" => "PART",
                "type" => "complete",
                "level" => 2,
                "attributes"=> 
                Array (
                    "L"=>$simple["L"],
                    "W"=>50,
                    "COUNT"=>10000,
                    "USEDCOUNT"=>0,
                    "ID"=>new_id($vals)+mt_rand(0,1000000),
                    'part.rs_real'=>0,
                    'NAME'=>'V '.$simple['CODE'].' / '.$simple['NAME'],
                    'TYPENAME'=>'V '.$simple['CODE'].' / '.$simple['NAME'],
                    'DESCRIPTION'=>'V '.$simple['CODE'].' / '.$simple['NAME']
                )
            );
            // if (count ($band_part)>1)
            // {
            //     p_($band_part);
            //     exit;
            // }
            

            $band2=Array
            (
                "tag" => "GOOD",
                "type" => "close",
                "level" => 2
            );
            $operation=Array
            (
                "tag" => "OPERATION",
                "type" => "open",
                "level" => 2
            );
            // p_($goods_pr_type[$simple['SIMPLE_ID']]);
            // p_($simple['NAME']);
            $operation["attributes"]["CMINDIMREGWASTE1"]=rs_general('min_rest_'.$goods_pr_type[$simple['SIMPLE_ID']]);
            $operation["attributes"]["CBUSINESSWASTE"]=rs_general('min_business_rest_'.$goods_pr_type[$simple['SIMPLE_ID']]);
            $operation["attributes"]["CTRIMW"]=rs_general('side_cut');
            $operation["attributes"][mb_strtoupper('cAllowanceWorkpiece')]=rs_general('part_cut_plus');
            if (rs_general('rest_cut')==1) $operation["attributes"][mb_strtoupper('cTrimWaste')]='true';
            else $operation["attributes"][mb_strtoupper('cTrimWaste')]='false';
            $operation["attributes"]["CMINDIMDETAIL"]=30;
            $operation["attributes"]["ID"]=new_id($vals)+2222;
            $operation["attributes"]["TYPEID"] = "CL";
            $operation["attributes"]["TYPENAME"] = "Погонажный раскрой";
            $operation["attributes"]["W"]=50;
            $operation["attributes"]["T"]=17;
            $operation["attributes"]["L"]=$simple["L"];
            $operation["attributes"]["TOOL1"] =  $tool["attributes"]["ID"];
            $operation["attributes"]["operation.my_cl"]=1;
            // p_($operation);
            
            $material=Array
            (
                "tag" => "MATERIAL",
                "type" => "complete",
                "level" => 2
            );
            $material["attributes"]["ID"]=$band["attributes"]["ID"];
            $operation2=Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
            
            
            $vals=vals_into_end($vals,$tool);
            $vals=vals_into_end($vals,$band);
            foreach ($band_part as $bp) $vals=vals_into_end($vals,$bp);
            $vals=vals_into_end($vals,$band2);
            $vals=vals_into_end($vals,$operation);
            $vals=vals_into_end($vals,$material);
            foreach ($id as $sp)
            {
                $parts2=Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> Array ("ID"=> $sp)
                );
                $vals=vals_into_end($vals,$parts2);
            }
            unset($id);
            $vals=vals_into_end($vals,$operation2);
            ksort($vals);
        }
        
        
    }
    
    ksort($vals);
    $vals=vals_out($vals);
    $cl=gl_cl(__LINE__,__FILE__,__FUNCTION__,vals_index_to_project($vals));
    $r = xml_parser_create();
    xml_parse_into_struct($r, $cl, $vals, $index);
    xml_parser_free($r);
    // p_($goods);
    // p_($goods_pr);
    if (rs_general('calc_right')==1)
    {
        foreach ($index['GOOD'] as $i)
        {
            if (($vals[$i]['attributes']['TYPEID']=='band')&&(isset($goods[$vals[$i]['attributes']['SIMPLE_ID']])))
            {
                $res['koef_price'][$vals[$i]['attributes']['CODE']]=$vals[$i]['attributes']['COUNT']/$goods[$vals[$i]['attributes']['SIMPLE_ID']];
                // $_SESSION['koef_price'][$vals[$i]['attributes']['CODE']]=
                $goods[$vals[$i]['attributes']['SIMPLE_ID']]=round($vals[$i]['attributes']['COUNT'],2);
            }
        }
        // exit ('11');
    }
    
    // p_($goods);
//     p_($res);
// exit;
//     xml_echo($cl); 
    // xml($vals);
    // my_mail('1','2',vals_index_to_project($vals),'xml');
    // exit;
    // 99999 Здесь сделать пересчёт goods после раскроя.
    // $_SESSION['koef_price'][код]
    // koef_price надо заполнить для погонажа.  Вносим [id=>коэффициент] для материала, [simple][code]=коэффициент для фурнитуры.
    $res['vals']=$vals;
    $res['goods']=$goods;
    
    // p_($goods);
    return $res;
}

function put_source_in($vals,$link,$arr,$place)
{
    // p_($arr);
    if((isset($vals))&&($place>0))
    {
        foreach ($arr['sheet_in'] as $sheet_k=>$ssheet)
        {
            $sql="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
            inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
            where type = 'auto' and product = 'sh' and PLACE = 1 LIMIT 1";
            // Здесь надо place=1 заменить на $place предварительно правильно заполнив все таблицы в БД
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
            // p_($sql);
            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3))
                {
                    $tool["attributes"][$k3]=$v3;
                }
            }
            
            $tool["attributes"]["ID"]=new_id($vals);
            if ($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)
            {
                $tool["attributes"]['good.my_client_material_id']=$arr['sheet_in_ad'][$sheet_k]['material_client_id'];
                $tool["attributes"]['good.my_client_material_code']=$arr['sheet_in_ad'][$sheet_k]['code'];
                $tool["attributes"]['good.my_client_material_client_id']=$arr['sheet_in_ad'][$sheet_k]['client_code'];
                $tool["attributes"]['good.my_client_material_place']=$arr['sheet_in_ad'][$sheet_k]['place'];
                $tool["attributes"]["good.my_client_material"]=1;
            }
           
            //станок есть, следующий материал сшивки
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
            if ((is_numeric($ssheet))&&($ssheet>0))
            {
                $sql='SELECT * FROM MATERIAL WHERE MATERIAL_ID='.$ssheet;
                // echo $sql;exit;
                $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                // p_($mat);
                $sheet=Array
                    (
                        "tag" => "GOOD",
                        "type" => "open",
                        "level" => 2
                    );
                
                $sheet["attributes"]["MATERIAL_ID"]=$mat["MATERIAL_ID"];
                if ($mat["W"]>0) $sheet["attributes"]["W"]=$mat["W"];
                else 
                {
                    if (($mat['RS_MIRROR']<>1)&&($mat['RS_GLASS']<>1))
                    {
                        $sheet["attributes"]["W"]=1830;
                        $mat["W"]=1830;
                    }
                    else
                    {
                        $sheet["attributes"]["W"]=3000;
                        $mat["W"]=3000;
                    }
                   
                }
                if ($mat["L"]>0) $sheet["attributes"]["L"]=$mat["L"];
                else 
                {
                    if (($mat['RS_MIRROR']<>1)&&($mat['RS_GLASS']<>1))
                    {
                        $sheet["attributes"]["L"]=2750;
                        $mat["L"]=2750;
                    }
                    else
                    {
                        $sheet["attributes"]["L"]=6000;
                        $mat["L"]=6000;
                    }
                }
                if ($mat["T"]>0) $sheet["attributes"]["T"]=$mat["T"];
                else $sheet["attributes"]["T"]=4;
                if (($mat["T"]==28)||($mat["T"]==38)||($mat["T"]==39)) $sheet["attributes"]["ST"]=1;
                $sheet["attributes"]["UNIT"]=$mat["UNIT"];
                $sheet["attributes"]["TYPENAME"]="Листовой";
                $sheet["attributes"]["COUNT"]=$mat["COUNT"];
                $sheet["attributes"]["NAME"]=$mat['NAME'];
                $sheet["attributes"]["ID"]=new_id($vals)+11230;
                $sheet["attributes"]["DESCRIPTION"]=$mat['NAME'];
                $sheet["attributes"]["part.name1"]=$mat['NAME'];
                $sheet["attributes"]["TYPEID"]="sheet";
                $sheet["attributes"]["CODE"]=$mat["CODE"];
// p_($arr['sheet_in_ad']);
                if ($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)
                {
                    if (is_numeric($arr['sheet_in_ad'][$sheet_k]['client_code'])) 
                    {
                        $sql='SELECT * FROM client WHERE client_id ='.$arr['sheet_in_ad'][$sheet_k]['client_code'];
                        $code=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]['code'];
                        // p_($code);
                    }
                    unset($cc);
                    if ((is_numeric($code))&&($code>0))
                    {
                        $sql='SELECT * FROM client WHERE code ='.$code;
                        // p_($sql);
                        $code=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                        // p_($code);
                        foreach ($code as $c)
                        {
                            $cc.=' OR Client ='.$c['client_id'].' ';
                        }
                        // p_($cc);
                    }
                    $sql='SELECT * FROM MATERIAL_CLIENT WHERE CODE_MAT ='.$arr['sheet_in_ad'][$sheet_k]['id'];
                    // echo $sql;
                    $code=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                    // p_($code);
                    $re=' AND (Material_client = '.$code[0]['mc_id'];
                    for ($r=1;$r<count($code);$r++)
                    {
                        $re.=' OR Material_client = '.$code[$r]['mc_id'];
                    }
                    $re.=') ';
                    // p_($re);
                    if (!isset($c)) $sql='SELECT * FROM MATERIAL_KL_STOCK_COUNT WHERE Count>0 AND Place='.$arr['sheet_in_ad'][$sheet_k]['place'].' AND Client ='.$arr['sheet_in_ad'][$sheet_k]['client_code'].$re;
                    else  $sql='SELECT * FROM MATERIAL_KL_STOCK_COUNT WHERE Count>0 AND Place='.$arr['sheet_in_ad'][$sheet_k]['place'].' AND (Client ='.$arr['sheet_in_ad'][$sheet_k]['client_code'].$cc.') '.$re;
                    // echo $sql;exit;
                    $mat_kl=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                    // p_($mat_kl);exit;
                    if (count ($mat_kl)>0)
                    {
                        
                        $sheet["attributes"]['good.my_client_material_id']=$arr['sheet_in_ad'][$sheet_k]['material_client_id'];

                        // 08.08.2023 Тоценко В массив материала добавлен акцентовский артикул материала
                        $sheet['attributes']['good.my_client_accent_article'] = $arr['sheet_in_ad'][$sheet_k]['accentArticle'];
                        $sheet["attributes"]['good.my_client_material_code']=$arr['sheet_in_ad'][$sheet_k]['code'];
                        $sheet["attributes"]["CODE"]=$arr['sheet_in_ad'][$sheet_k]['code'];

                        $sheet["attributes"]['good.my_client_material_client_id']=$arr['sheet_in_ad'][$sheet_k]['client_code'];
                        $sheet["attributes"]['good.my_client_material_place']=$arr['sheet_in_ad'][$sheet_k]['place'];
                        $sheet["attributes"]["good.my_client_material"]=1;
                        $sql='SELECT * FROM MATERIAL_HALF WHERE place_id='.$arr['sheet_in_ad'][$sheet_k]['place'].' AND material_id ='.$arr['sheet_in_ad'][$sheet_k]['material_client_id'];
                        // echo $sql;exit;
                        $half_kl=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['count'];
                        unset($ar_name);
                        foreach ($mat_kl as $km=>$mk)
                        {
                            $sql='SELECT * FROM MATERIAL_CLIENT WHERE mc_id ='.$mk['Material_client'];
                        // echo $sql;exit;
                            $m1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                            if ($m1['CODE_MAT']<>$arr['sheet_in_ad'][$sheet_k]['material_client_id']) unset ($mat_kl[$km]);
                            else
                            {
                                $mat_kl[$km]['L']=$m1['L'];
                                $mat_kl[$km]['W']=$m1['W'];
                                $mat_kl[$km]['T']=$m1['T'];
                                $mat_kl[$km]['Name']=$m1['Name'];
                                $ar_name[]=$m1['mc_id'];
                            }
                        }
                        // p_($ar_name);
                        // p_($mat_kl);exit;
                        $sheet["attributes"]["NAME"]= implode('||',$ar_name).' [*] '.$mat['NAME'];
                        $sheet["attributes"]["part.name1"]=$sheet["attributes"]["NAME"];
                        $sheet["attributes"]["good.my_client_material_name"]=$sheet["attributes"]["NAME"];
                        // p_($sheet);
                    }
                }
                $sheet_part=
                    Array
                    (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 2,
                        "attributes"=> 
                        Array (
                            "L"=>$mat["L"],
                            "W"=>$mat["W"],
                            "COUNT"=>10000,
                            "USEDCOUNT"=>0,
                            "ID"=>new_id($vals)+555
                        )
                    );

                if (($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)&&(count ($mat_kl)>0))
                {
                    unset ($sheet_part,$sheet_part2);
                    $id=new_id($vals);

                    foreach ($mat_kl as $kl=>$mmat)
                    {
                        $id++;
                        $sheet_part2[]=
                        Array
                        (
                            "tag" => "PART",
                            "type" => "complete",
                            "level" => 2,
                            "attributes"=> 
                            Array (
                                "L"=>$mmat["L"],
                                "W"=>$mmat["W"],
                                "COUNT"=>$mmat["Count"],
                                "USEDCOUNT"=>0,
                                "ID"=>$id+mt_rand(555,250000),
                                'part.my_client_material'=>1,
                                'part.my_client_material_id'=>$mmat["Material_client"],
                                'NAME'=>$mmat["Name"],
                                'DESCRIPTION'=>$mmat["Material_client"].' / '.$mmat["Name"]

                            )
                        );
                    }
                    if ($half_kl>0)
                    {
                        $sheet_part2[]=
                        Array
                        (
                            "tag" => "PART",
                            "type" => "complete",
                            "level" => 2,
                            "attributes"=> 
                            Array (
                                "L"=>$mat["L"],
                                "W"=>($mat["W"]/2-5),
                                "COUNT"=>1,
                                "USEDCOUNT"=>0,
                                "ID"=>$id+mt_rand(555,250000),
                                'NAME'=>'поллиста со склада',
                                'DESCRIPTION'=>'поллиста со склада'

                            )
                        );
                    }
                        
                    $sheet_part2[]=
                    Array
                    (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 2,
                        "attributes"=> 
                        Array (
                            "L"=>$mat["L"],
                            "W"=>($mat["W"]),
                            "COUNT"=>10000,
                            "USEDCOUNT"=>0,
                            "ID"=>$id+mt_rand(555,250000),
                            'NAME'=>'лист со склада',
                            'DESCRIPTION'=>'лист со склада'

                        )
                    );
                    
                }

                $sheet2=Array
                (
                    "tag" => "GOOD",
                    "type" => "close",
                    "level" => 2
                );
                $operation=Array
                (
                    "tag" => "OPERATION",
                    "type" => "open",
                    "level" => 2
                );
                if (($mat['RS_MIRROR']==1)||($mat['RS_GLASS']==1)) $operation["attributes"]["operation.glass"]=1;
                $operation["attributes"]["ID"]=new_id($vals)+2222;
                $operation["attributes"]["TYPEID"] = 'CS';
                $operation["attributes"]["TYPENAME"] = "Листовой раскрой";
                $operation["attributes"]["CTRIML"] = 8;
                $operation["attributes"]["CTRIMW"] = 8;
                $operation["attributes"]["CFILLREP"] = 0.9;
                $operation["attributes"]["CSTEXTURE"] = "true";
                $operation["attributes"]["PRINTABLE"] = "true";
                $operation["attributes"]["STARTNEWPAGE"] = "true";
                $operation["attributes"]["W"]=$mat["W"];
                $operation["attributes"]["T"]=$mat["T"];
                $operation["attributes"]["L"]=$mat["L"];
                $operation["attributes"]["CSIZEMODE"] =1; 
                $operation["attributes"]["TOOL1"] =  $tool["attributes"]["ID"];
                if (($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)&&(count ($mat_kl)>0))
                {
                    foreach ($arr['sheet_in_ad'][mb_strtolower($sheet_k)] as $rt=>$rt1)
                    {
                        $operation["attributes"]['operation.my_client_material_'.$rt]=$rt1;
                    }
                    $operation["attributes"]["operation.my_client_material"]=1;
                }
                $material=Array
                (
                    "tag" => "MATERIAL",
                    "type" => "complete",
                    "level" => 2
                );
                $material["attributes"]["ID"]=$sheet["attributes"]["ID"];
                $operation2=Array
                (
                    "tag" => "OPERATION",
                    "type" => "close",
                    "level" => 2
                );
                $parts2=Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> Array ("ID"=> $sheet_part["attributes"]["ID"])
                );
                if (($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)&&(count ($mat_kl)>0))
                {
                    unset ($parts2,$parts22);
                    foreach ($sheet_part2 as $kl=>$mmat)
                    {
                        $parts22[]=Array
                        (
                            "tag" => "PART",
                            "type" => "complete",
                            "level" => 2,
                            "attributes"=> Array ("ID"=> $mmat["attributes"]["ID"])
                        );
                    }
                }
                // p_($parts22);
                // p_($sheet_part2);
                // exit;
                // $vals=move_vals($vals,count($vals)-1,1);
                $vals=vals_into_end($vals,$tool);
                $vals=vals_into_end($vals,$sheet);
                // p_($rttr++);
                if (($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)&&(count ($mat_kl)>0))
                {
                    foreach ($sheet_part2 as $sp)
                    {
                        $vals=vals_into_end($vals,$sp);
                    }
                }
                else  $vals=vals_into_end($vals,$sheet_part);
                $vals=vals_into_end($vals,$sheet2);
                $vals=vals_into_end($vals,$operation);
                $vals=vals_into_end($vals,$material);
                if (($arr['sheet_in_ad'][$sheet_k]['material_client_id']>0)&&(count ($mat_kl)>0))
                {
                    foreach ($parts22 as $sp)
                    {
                        $vals=vals_into_end($vals,$sp);
                    }
                }
                else $vals=vals_into_end($vals,$parts2);
                $vals=vals_into_end($vals,$operation2);
            }
        }
        ksort($vals);
        // xml ($vals);

        $vals=vals_out($vals);
        $index=make_index($vals);
        foreach ($arr['band_in'] as $band)
        {
            $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
            inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
            where PLACE = ".$place." LIMIT 1";
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3))
                {
                    $tool["attributes"][$k3]=$v3;
                }
            }
            $tool["attributes"]["ID"]=new_id($vals);
            $sql="select * from BAND where BAND_ID=".$band;
            // echo $sql;
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            $band1=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3))
                {
                    $band1["attributes"][$k3]=$v3;
                }
            }
            $band1["attributes"]["ID"]=new_id($vals)+1;
            $operation=Array
                (
                    "tag" => "OPERATION",
                    "type" => "open",
                    "level" => 2
                );
            $operation["attributes"]["ID"]=new_id($vals)+2;
            $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
            $operation["attributes"]["W"]=$band1["attributes"]["W"];
            $operation["attributes"]["T"]=$band1["attributes"]["T"];
            $operation["attributes"]["L"]=10000;
            $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
            $operation["attributes"]["ELCALCMAT"]="true";
            $operation["attributes"]["ELROUNDMAT"]="true";
            $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
            if ($sql3['T']<1)
            {
                $operation["attributes"]["ELCOLOR"]="rgb(255,".mt_rand(0,5).",".mt_rand(0,5).")";
                $operation["attributes"]["ELLINEMARK"]="1";
            }
            else
            {
                $operation["attributes"]["ELCOLOR"]="rgb(".mt_rand(0,5).",255,".mt_rand(0,5).")";
                $operation["attributes"]["ELLINEMARK"]="3";
            }
            $operation["attributes"]["TYPEID"]="EL";
            $material=Array
                (
                    "tag" => "MATERIAL",
                    "type" => "complete",
                    "level" => 2
                );
            $material["attributes"]["ID"]=new_id($vals)+1;
            $operation2=Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
        
            $vals=vals_into_end($vals,$tool);
            $vals=vals_into_end($vals,$band1);
            $vals=vals_into_end($vals,$operation);
            $vals=vals_into_end($vals,$material);
            $vals=vals_into_end($vals,$operation2);
            $vals=vals_out($vals);
            ksort($vals);
            $index=make_index($vals);
        }
        ksort($vals);
        $vals=check_product_sheet_band ($vals);
        return $vals;
    }
}

function check_product_sheet_band ($vals)
{
    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    foreach ($index['GOOD'] as $i)
    {
        
        if ($vals[$i]['attributes']['TYPEID']=="product") $ok=1;
        elseif (($vals[$i]['attributes']['TYPEID']=="band"))
        {
            unset($op,$tool);
            foreach ($index['OPERATION'] as $j)
            {
                if (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                {
                    $k=$j+2;
                    if ($vals[$k]["tag"]=="PART")
                    {
                       $op=1;
                        $k++;
                    }
                    foreach ($index['GOOD'] as $k)
                    {
                        if (($vals[$k]['attributes']['TYPEID']=="tool.edgeline")&&($vals[$k]['attributes']['ID']==$vals[$j]['attributes']['TOOL1']))
                        {
                            $tool=1;
                        }
                    }
                    
                    
                }
                if (($vals[$j]['attributes']['TYPEID']=="CL")&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                {
                    $op=1;$tool=1;
                }

            }
            if((!isset($op)&&!isset($vals[$i]['attributes']['CODE']))OR(!isset($tool)))
            {
                foreach ($index['OPERATION'] as $j)
                {
                    if (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                    {
                        $k=$j+2;
                        while ($vals[$k]["tag"]=="PART")
                        {
                            unset ($vals[$k]);
                            $k++;
                        }
                        foreach ($index['GOOD'] as $k)
                        {
                            if (($vals[$k]['attributes']['TYPEID']=="tool.edgeline")&&($vals[$k]['attributes']['ID']==$vals[$j]['attributes']['TOOL1']))
                            {
                                unset ($vals[$k]);
                            }
                        }
                        
                        unset($vals[$k],$vals[$j],$vals[$j+1]);
                    }

                }

                unset ($vals[$i]);
                $_SESSION['error_ok'].="Кромка '".$vals[$i]['attributes']['NAME']."' удалена из проекта, так как у неё нет кода, соответственно добавлена каким-то образом по ошибке.<hr>";
            }
            
        }
        elseif (($vals[$i]['attributes']['TYPEID']=="sheet")&&(!isset($vals[$i]['attributes']['CODE']))
        &&(!isset($vals[$i]['attributes']['MY_FIRST']))&&(!isset($vals[$i]['attributes']['MY_SECOND'])))
        {
            
            unset($op,$tool,$part);
            foreach ($index['OPERATION'] as $j)
            {
                if (($vals[$j]['attributes']['TYPEID']=='CS')&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                {
                    $k=$j+2;
                    if ($vals[$k]["tag"]=="PART")
                    {
                       $op=1;
                        $k++;
                    }
                    foreach ($index['GOOD'] as $k)
                    {
                        if (($vals[$k]['attributes']['TYPEID']=="tool.cutting")&&($vals[$k]['attributes']['ID']==$vals[$j]['attributes']['TOOL1']))
                        {
                            $tool=1;
                        }
                    }
                    
                    
                }

            }
            if($vals[$i+1]["tag"]=="PART") $part=1;
            if((!isset($op)&&!isset($vals[$i]['attributes']['CODE']))OR(!isset($tool))OR(!isset($part)))
            {
                $_SESSION['error_ok'].="Материал '".$vals[$i]['attributes']['NAME']."' удален из проекта, так как у него нет кода, соответственно добавлен каким-то образом по ошибке.<hr>";
                foreach ($index['OPERATION'] as $j)
                {
                    if (($vals[$j]['attributes']['TYPEID']=='CS')&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                    {
                        foreach ($index['GOOD'] as $k)
                        {
                            if (($vals[$k]['attributes']['TYPEID']=="tool.cutting")&&($vals[$k]['attributes']['ID']==$vals[$j]['attributes']['TOOL1']))
                            {
                                unset ($vals[$k]);
                            }
                        }
                        $k=$j+2;
                        while ($vals[$k]["tag"]=="PART")
                        {
                            unset ($vals[$k]);
                            $k++;
                        }
                        unset($vals[$k],$vals[$j],$vals[$j+1]);
                    }
                }
                unset ($vals[$i]);
                $k=$i+1;
                while ($vals[$k]["tag"]=="PART")
                {
                    unset ($vals[$k]);
                    $k++;
                }
                unset ($vals[$k]);
            }
        }
        elseif ($vals[$i]['attributes']['TYPEID']=="tool.edgeline")
        {
            foreach ($index['OPERATION'] as $j)
            {
                if (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$i]['attributes']['ID']==$vals[$j]['attributes']['TOOL1'])) $ok_ed=1;
            }
            if (!isset($ok_ed)) unset($vals[$i]);
        }
        elseif ($vals[$i]['attributes']['TYPEID']=="tool.cutting")
        {
            foreach ($index['OPERATION'] as $j)
            {
                if ((($vals[$j]['attributes']['TYPEID']=='CS')&&($vals[$i]['attributes']['ID']==$vals[$j]['attributes']['TOOL1']))
                OR(($vals[$j]['attributes']['TYPEID']=="CL")&&($vals[$i]['attributes']['ID']==$vals[$j]['attributes']['TOOL1']))) $ok_ed=1;
            }
            if (!isset($ok_ed)) unset($vals[$i]);
        }
          
    }
    // xml_html($vals);
    // xml($vals);

    if (!isset($ok))
    {
        $array=Array
                (
                    "tag" => "GOOD",
                    "type" => "open",
                    "level" => 2,
                    "attributes"=>array('NAME'=>"new",'TYPEID'=>"product",'COUNT'=>1,'ID'=>(new_id($vals)+1200))
                );
        $array2=Array
                    (
                        "tag" => "GOOD",
                        "type" => "close",
                        "level" => 2
                    );
        $vals=move_vals($vals,1,2);
        $vals[1]=$array;
        $vals[2]=$array2;
    }
    ksort($vals);
    $vals=vals_out($vals);
    // p_($vals);
    // exit;
    return $vals;
}
function double_del ($vals,$out_del,$link)
{
    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    foreach ($out_del as $id_del)
    {
        foreach ($index['PART'] as $i)
        {
            if (($vals[$i]['attributes']['ID']==$id_del)&&($vals[$i]['attributes']['CL']>0)&&($vals[$i]['attributes']['MY_DOUBLE_RES']>0))
            {
                if (isset($vals[$i]['attributes']['_OLD_ELB'])) $el_op[]=substr($vals[$i]['attributes']['_OLD_ELB'],11);
                if (isset($vals[$i]['attributes']['_OLD_ELT'])) $el_op[]=substr($vals[$i]['attributes']['_OLD_ELT'],11);
                if (isset($vals[$i]['attributes']['_OLD_ELL'])) $el_op[]=substr($vals[$i]['attributes']['_OLD_ELL'],11);
                if (isset($vals[$i]['attributes']['_OLD_ELR'])) $el_op[]=substr($vals[$i]['attributes']['_OLD_ELR'],11);
                if (isset($vals[$i]['attributes']['_OLD_GRB'])) $gr_op[]=substr($vals[$i]['attributes']['_OLD_GRB'],11);
                if (isset($vals[$i]['attributes']['_OLD_GRT'])) $gr_op[]=substr($vals[$i]['attributes']['_OLD_GRT'],11);
                if (isset($vals[$i]['attributes']['_OLD_GRL'])) $gr_op[]=substr($vals[$i]['attributes']['_OLD_GRL'],11);
                if (isset($vals[$i]['attributes']['_OLD_GRR'])) $gr_op[]=substr($vals[$i]['attributes']['_OLD_GRR'],11);
                $double_id=$vals[$i]['attributes']['MY_DOUBLE_ID'];
                $t1=part_get_material($vals,$id_del);
                $t=$t1['attributes']['T'];
                unset($part_del1);
                unset($part_del);
                foreach ($index['PART'] as $pp)
                {
                    if ($vals[$pp]['attributes']['MY_DOUBLE_ID']==$double_id)
                    {
                       
                        $part_del[]=$vals[$pp]['attributes']['ID'];
                        $part_del1[]=$vals[$pp]['attributes']['NAME'];
                    }
                    if (($vals[$pp]['attributes']['MY_DOUBLE_ID']==$double_id)&&($vals[$pp]['attributes']['MY_DOUBLE_FACE']==1))
                    {
                        $t2=part_get_material($vals,$vals[$pp]['attributes']['ID']);
                    }
                }
// проверка старого ID
                if (isset($vals[$i]['attributes']['_OLD_ID']))
                {
                    $old_id=$vals[$i]['attributes']['_OLD_ID'];
                    $new_for_old_id=new_id($vals);
                    foreach ($index['PART'] as $pp)
                    {
                        if ($vals[$pp]['attributes']['ID']==$old_id)
                        {
                            $vals[$pp]['attributes']['ID']=$new_for_old_id;
                        }
                    }
                    $part_del[]=$new_for_old_id;
                    $side=array('L','R','B','T');
                    foreach ($side as $s)
                    {
                        if (isset($vals[$i]["attributes"]["GR".$s]))
                        {
                            foreach ($index["OPERATION"] as $iop)
                            {
                                if (($vals[$iop]["attributes"]["TYPEID"]=="GR")AND
                                ($vals[$iop]["type"]=="open")AND($vals[$iop+1]["tag"]=="PART"))
                                {
                                    $iop1=$iop+1;
                                    while ($vals[$iop1]["tag"]=="PART")
                                    {
                                        if(($vals[$iop1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                                        {
                                            $vals[$iop1]["attributes"]["ID"]=$old_id;
                                        }
                                        $iop1++;
                                    }
                                }
                            }
                        }
                    }
                    foreach ($side as $s)
                    {
                        if (isset ($vals[$i]['attributes']['EL'.$s]))
                        {
                            $opp=substr($vals[$i]['attributes']['EL'.$s],11);
                            foreach ($index['OPERATION'] as $io_el)
                            {
                                if (($vals[$io_el]['attributes']['TYPEID']=='EL')&&($vals[$io_el]['attributes']['ID']==$opp))
                                {
                                    unset ($vals[$io_el]['attributes']['NAME']);
                                    unset ($vals[$io_el]['attributes']['DESCRIPTION']);
                                    unset ($vals[$io_el]['attributes']['MY_DOUBLE_ID']);
                                    unset ($vals[$io_el]['attributes']['MY_DOUBLE']);
                                    unset ($vals[$io_el]['attributes']['part.my_double_id']);
                                    unset ($vals[$io_el]['attributes']['part.my_double']);
                                    $rr=$io_el+2;
                                    while ($vals[$rr]['tag']=='PART')
                                    {
                                        $rdp++;
                                        if ($vals[$rr]['attributes']['ID']==$vals[$i]['attributes']['ID']) $rrr=$rr;
                                        $rr++;

                                    }
                                    unset($vals[$rrr]);
                                    if ($rdp==1)
                                    {
                                        foreach ($index['GOOD'] as $fr)
                                        {
                                            if (($vals[$fr]['attributes']['TYPEID']=='tool.edgeline')&&($vals[$fr]['attributes']['ID']==$vals[$io_el]['attributes']['TOOL1']))
                                            {
                                                unset ($vals[$fr]['attributes']['NAME']);
                                                unset ($vals[$fr]['attributes']['DESCRIPTION']);
                                                unset ($vals[$fr]['attributes']['MY_DOUBLE_ID']);
                                                unset ($vals[$fr]['attributes']['MY_DOUBLE']);
                                                unset ($vals[$fr]['attributes']['part.my_double_id']);
                                                unset ($vals[$fr]['attributes']['part.my_double']);
                                            }
                                            if (($vals[$fr]['attributes']['TYPEID']=='band')&&($vals[$fr]['attributes']['ID']==$vals[$io_el+1]['attributes']['ID']))
                                            {
                                                $sql='SELECT * FROM BAND WHERE CODE='.$vals[$fr]['attributes']['CODE'];
                                                $array_db=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                                                $vals[$fr]['attributes']['NAME']=$array_db['KR_NAME'];
                                                unset ($vals[$fr]['attributes']['DESCRIPTION']);
                                                unset ($vals[$fr]['attributes']['MY_DOUBLE_ID']);
                                                unset ($vals[$fr]['attributes']['MY_DOUBLE']);
                                                unset ($vals[$fr]['attributes']['part.my_double_id']);
                                                unset ($vals[$fr]['attributes']['part.my_double']);

                                                // unset ($vals[$fr]);
                                            }
                                        }
                                        // unset($vals[$io_el]);
                                        // unset($vals[$io_el+1]);
                                        // unset($vals[$io_el+3]);
                                    }
                                    unset($rdp);
                                }
                            }
                        }
                    }
                    foreach ($index['OPERATION'] as $io_el)
                    {
                        if ($vals[$io_el]['attributes']['TYPEID']=='CS')
                        {
                            $rr=$io_el+2;
                            unset($rrr);
                            while ($vals[$rr]['tag']=='PART')
                            {
                                if ($vals[$rr]['attributes']['ID']==$vals[$i]['attributes']['ID']) $rrr=$rr;
                                $rr++;

                            }
                            
                            if ($rrr>0)
                            {
                                unset($vals[$rrr]);
                                foreach ($index['GOOD'] as $fr)
                                {
                                    if (($vals[$fr]['attributes']['TYPEID']=='tool.cutting')&&($vals[$fr]['attributes']['ID']==$vals[$io_el]['attributes']['TOOL1']))
                                    {
                                        unset ($vals[$fr]);
                                    }
                                    if (($vals[$fr]['attributes']['TYPEID']=='sheet')&&($vals[$fr]['attributes']['ID']==$vals[$io_el+1]['attributes']['ID']))
                                    {
                                        unset ($vals[$fr]);
                                        unset ($vals[$fr+1]);
                                        unset ($vals[$fr+2]);
                                    }
                                }
                                unset($vals[$io_el]);
                                unset($vals[$io_el+1]);
                                unset($vals[$io_el+3]);
                                unset($vals[$io_el+4]);
                            }
                        }
                    }
                   

                    foreach($vals[$i]['attributes'] as $key=>$atr)
                    {
                        if (substr_count($key,'_OLD')<1)
                        {
                            
                            unset ($vals[$i]['attributes'][$key]);
                        }
                        
                    }
                    foreach($vals[$i]['attributes'] as $key=>$atr)
                    {
                        
                        $new_key=substr($key,5);
                        $vals[$i]['attributes'][$new_key]=$atr;
                        unset($vals[$i]['attributes'][$key]);
                        
                    }
                    $index=make_index($vals);
                    
                    foreach ($index['PART'] as $ipl)
                    {
                        if ((in_array($vals[$ipl]['attributes']['ID'],$part_del))&&($i<>$ipl))
                        {
                            foreach ($side as $s)
                            {
                                if (isset ($vals[$ipl]['attributes']['EL'.$s]))
                                {
                                    $opp2=substr($vals[$ipl]['attributes']['EL'.$s],11);
                                    foreach ($index['OPERATION'] as $io_el2)
                                    {
                                        if (($vals[$io_el2]['attributes']['TYPEID']=='EL')&&($vals[$io_el2]['attributes']['ID']==$opp2))
                                        {
                                            foreach ($index['GOOD'] as $fr2)
                                            {
                                                if (($vals[$fr2]['attributes']['TYPEID']=='tool.edgeline')&&($vals[$fr2]['attributes']['ID']==$vals[$io_el2]['attributes']['TOOL1']))
                                                {
                                                    unset ($vals[$fr2]['attributes']['NAME']);
                                                    unset ($vals[$fr2]['attributes']['DESCRIPTION']);
                                                    unset ($vals[$fr2]['attributes']['MY_DOUBLE_ID']);
                                                    unset ($vals[$fr2]['attributes']['MY_DOUBLE']);
                                                    unset ($vals[$fr2]['attributes']['part.my_double_id']);
                                                    unset ($vals[$fr2]['attributes']['part.my_double']);

                                                }
                                                if (($vals[$fr2]['attributes']['TYPEID']=='band')&&($vals[$fr2]['attributes']['ID']==$vals[$io_el2+1]['attributes']['ID']))
                                                {
                                                    $sql='SELECT * FROM BAND WHERE CODE='.$vals[$fr2]['attributes']['CODE'];
                                                    $array_db=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                                                    $vals[$fr2]['attributes']['NAME']=$array_db['KR_NAME'];
                                                    unset ($vals[$fr2]['attributes']['DESCRIPTION']);
                                                    unset ($vals[$fr2]['attributes']['MY_DOUBLE_ID']);
                                                    unset ($vals[$fr2]['attributes']['MY_DOUBLE']);
                                                    unset ($vals[$fr2]['attributes']['part.my_double_id']);
                                                    unset ($vals[$fr2]['attributes']['part.my_double']);

                                                    // unset ($vals[$fr]);
                                                }
                                            }
                                            unset ($vals[$io_el2]['attributes']['NAME']);
                                            unset ($vals[$io_el2]['attributes']['DESCRIPTION']);
                                            unset ($vals[$io_el2]['attributes']['MY_DOUBLE_ID']);
                                            unset ($vals[$io_el2]['attributes']['MY_DOUBLE']);
                                            unset ($vals[$io_el2]['attributes']['part.my_double_id']);
                                            unset ($vals[$io_el2]['attributes']['part.my_double']);
                                            $rr2=$io_el2+2;
                                            while ($vals[$rr2]['tag']=='PART')
                                            {
                                                if ($vals[$rr2]['attributes']['ID']==$vals[$ipl]['attributes']['ID']) unset($vals[$rr2]);
                                                $rr2++;
        
                                            }
                                        }
                                    }
                                }
                            }
                            unset($vals[$ipl]);
                        }
                    }
                    
                    $index=make_index($vals);
                    foreach ($index['OPERATION'] as $ioel)
                    {
                     
                        if (($vals[$ioel]['attributes']['TYPEID']=='CS')&&($vals[$ioel+1]['attributes']['ID']==$t2['attributes']['ID']))
                        {
                            $vals=move_vals($vals,$ioel+2,1);
                            $vals[$ioel+2]=Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> Array ("ID"=> $old_id)
                            );
                        }
                        $index=make_index($vals);
                    }
                    foreach ($index['OPERATION'] as $ioel)
                    {
                        if (($vals[$ioel]['attributes']['TYPEID']=='EL')&&(in_array($vals[$ioel]['attributes']['ID'],$el_op)))
                        {
                            $vals=move_vals($vals,$ioel+2,1);
                            $vals[$ioel+2]=Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> Array ("ID"=> $old_id)
                            );
                        }
                        $index=make_index($vals);
                    }
                    foreach ($index['OPERATION'] as $ioel)
                    {
                        if (($vals[$ioel]['attributes']['TYPEID']=='GR')&&(in_array($vals[$ioel]['attributes']['ID'],$gr_op)))
                        {
                            $vals=move_vals($vals,$ioel+1,1);
                            $vals[$ioel+1]=Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> Array ("ID"=> $old_id)
                            );
                        }
                        $index=make_index($vals);
                    }
                    foreach ($index['OPERATION'] as $ioel)
                    {
                        if (($vals[$ioel]['attributes']['TYPEID']=='XNC')&&($vals[$ioel]['attributes']['MY_DOUBLE_ID']==$double_id))
                        {
                            $vals[$ioel]['attributes']['NAME']='';
                            $vals[$ioel]['attributes']['DESCRIPTION']='';
                            unset($vals[$ioel]['attributes']['MY_DOUBLE']);
                            unset($vals[$ioel]['attributes']['part.my_double']);
                            unset($vals[$ioel]['attributes']['MY_DOUBLE_ID']);
                            unset($vals[$ioel]['attributes']['part.my_double_id']);
                            unset($vals[$ioel]['attributes']['part.name1']);
                            unset($vals[$ioel]['attributes']['_OLD_ID']);
                            $vals[$ioel]['attributes']['TYPENAME']='';
                            $dt=$t-$vals[$ioel]['attributes']['_OLD_T'];
                            $vals[$ioel]["attributes"]["PROGRAM"]=str_replace("dz=\"".$t."\"","dz=\"".$vals[$ioel]['attributes']['_OLD_T']."\"",$vals[$ioel]["attributes"]["PROGRAM"]);
                            $vals[$ioel]["attributes"]["PROGRAM"] = xml_parser($vals[$ioel]["attributes"]["PROGRAM"]);
                            $r = xml_parser_create();
                            xml_parse_into_struct($r, $vals[$ioel]["attributes"]["PROGRAM"], $v1, $i1);
                            xml_parser_free($r);
                            foreach($v1 as $k11=>$v11)
                            {
                                foreach($v11['attributes'] as $trt=>$tt)
                                {
                                    $v1[$k11]['attributes'][mb_strtolower($trt)]=$tt;
                                    unset($v1[$k11]['attributes'][$trt]);
                                }
                                if (isset($v1[$k11]['attributes']['dp']))$v1[$k11]['attributes']['dp']=$v1[$k11]['attributes']['dp']-$dt;
                            }
                            // p_($v1);exit;
                            $vals[$ioel]["attributes"]["PROGRAM"]=vals_index_to_project($v1);
                            unset($vals[$ioel]['attributes']['_OLD_T']);
                        
                            $vals=move_vals($vals,$ioel+1,1);
                            $vals[$ioel+1]=Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> Array ("ID"=> $old_id)
                            );
                            $vals=move_vals($vals,$ioel+2,1);
                            $sheet2=Array
                            (
                                "tag" => "OPERATION",
                                "type" => "close",
                                "level" => 2
                            );
                            $index=make_index($vals);
                        }
                    }
                    ksort($vals);
                    unset($el_op);
                }

            }

        }
    }
    ksort ($vals);
    // header('Content-type: text/xml');
    // echo vals_index_to_project($vals);exit;
    return($vals);
}
function check_shabllon_wl($vals1,$check_error_part)
{
    $vals1=vals_out($vals1);
    ksort($vals1);
    $index1=make_index($vals1);
    foreach($index1["PART"] as $i12)
    {
        if((!isset($vals1[$i12]["attributes"]["SHEETID"]))&&($vals1[$i12]["attributes"]["CW"]>0)&&($vals1[$i12]["attributes"]["CL"]>0))
        {
            $tr=$vals1[$i12]["attributes"]["ID"];
            $iddd=0;  
            foreach ($index1["OPERATION"]as $i33)
            {
                
                if (($vals1[$i33]["attributes"]["TYPEID"]==="XNC")AND($vals1[$i33]["type"]=="open"))
                {
                    
                    
                    if ($vals1[$i33+1]["attributes"]["ID"]==$tr)
                    {
                        $pr12=$vals1[$i33]["attributes"]["PROGRAM"];
                        $v412[$tr][$iddd] = xml_parser($v412[$tr][$iddd]);
                        $r = xml_parser_create();
                        xml_parse_into_struct($r, $pr12, $v412[$tr][$iddd], $i412[$tr][$iddd]);
                        xml_parser_free($r);
                        if(($vals[$i33]["attributes"]["TURN"]==1)OR($vals[$i33]["attributes"]["TURN"]==3))
                        {
                            $t121_dl=$v412[$tr][$iddd][0]["attributes"]["DX"];
                            $v412[$tr][$iddd][0]["attributes"]["DX"]= $v412[$tr][$iddd][0]["attributes"]["DY"];
                            $v412[$tr][$iddd][0]["attributes"]["DY"]=$t121_dl;
                            $turn=$vals[$i33]["attributes"]["TURN"];
                            $turn_text="(ПОВОРОТ детали = ".($turn*90)." градусов)";
                        }
                        $iddd++;
                    }
                    
                }
            }
            unset($iddd,$i121,$i1211,$i33);
            
            foreach ($i412[$tr] as $k=>$v)
            {
                foreach ($v["CALL"] as $k1=>$v1)
                {
                if($v412[$tr][$k][$v1]["attributes"]["NAME"]<>"")
                {
                    
                        $r1=$v412[$tr][$k][$v1]["attributes"]["NAME"];
                        $id_s=substr($r1,strpos($r1,"[/")+2,4)/1;
                        
                        $sql="select * from xnc_template where id=".$id_s;
                        $sql1=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                        
                        if ($v412[$tr][$iddd][0]["attributes"]["DX"]<$sql1["L"])
                        {
                            $error.="<br>[/0129] Деталь '".$vals1[$i12]["attributes"]["NAME"]."' (id='".$vals1[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals1[$i12]["attributes"]["W"]." * L ".$vals1[$i12]["attributes"]["L"].")
                            - длина программы DX детали ".$v412[$tr][$iddd][0]["attributes"]["DX"]." мм менее допустимой для шаблона (".$sql1["name"].") ".$sql1["L"]." mm. 
                            <br>";
                        }
                        if ($v412[$tr][$iddd][0]["attributes"]["DY"]<$sql1["W"])
                        {
                            $error.="<br>[/0130] Деталь '".$vals1[$i12]["attributes"]["NAME"]."' (id='".$vals1[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals1[$i12]["attributes"]["W"]." * L ".$vals1[$i12]["attributes"]["L"].")
                            - ширина программы DY детали ".$v412[$tr][$iddd][0]["attributes"]["DY"]." мм менее допустимой для шаблона (".$sql1["name"].") ".$sql1["W"]." mm. 
                            <br>";
                        }
                
                }
                
                }
                unset($r1);
            }
            
                        
            
            if($error)
            {
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["ID"].=$vals1[$i12]["attributes"]["ID"];
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["NAME"].=$vals1[$i12]["attributes"]["NAME"];
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["DL"].=$vals1[$i12]["attributes"]["DL"];
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["L"].=$vals1[$i12]["attributes"]["L"];
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["DW"].=$vals1[$i12]["attributes"]["DW"];
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["W"].=$vals1[$i12]["attributes"]["W"];
                $check_error_part[$vals1[$i12]["attributes"]["ID"]]["ERROR"].=$error;
            }
        }
    }      
    unset($name1,$turn_text,$i,$ban121,$turn, $error,$v412_diam,$v412_pr,$t121_dw,$t121_dl,$v4122,$k2121211,$k12333,$o21219,$o212199,$ac,$gr12,$gr112,$mat12,$w12,$t12,$l12,$n12,$ban12["wb12"],$ban12["tb12"],$mat12,$ban12,$ban12["nb12"],$r, $pr12, $v412, $i412,$depth_tools_from_DB,$v412,$i412,$rrrr,$rrr,$rr,$o21212,
    $name_tools_from_DB, $diameter_tools_from_DB, $depth_tools_from_DB,$ik121);
}

function check_stock($vals,$link,$place)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    $arr_st=st_from_akcent($vals,$place);
    
    // p_($arr_st);exit;
    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="band")
        {
            foreach ($index["OPERATION"] as $d)
            {
                if (($vals[$d+1]["tag"]=="MATERIAL")&&($vals[$d]["attributes"]["TYPEID"]="EL")&&($vals[$d+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"])
                &&($vals[$d+1]["attributes"]["COUNT"]>0)&&($vals[$i]["attributes"]["CODE"]>0))
                {
                    $sql="select * from BAND where CODE=".$vals[$i]["attributes"]["CODE"].";";
                    $res2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                    if (($res2["BAND_ID"]>0)&&($res2['MY_1C_NOM']<>6149))
                    {
                        $sql="select * from BAND_STOCK where BAND_ID=".$res2["BAND_ID"].";";
                        
                        $res33=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                        foreach ($res33 as $res4)
                        {
                            $sql2="select NAME from PLACES where PLACES_ID=".$res4["PLACE_ID"].";";
                            $res7=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql2)['data'][0];

                            if ($res4["COUNT"]>0) $res_st[$res4["PLACE_ID"]]=array("count"=>$res4["COUNT"],"name"=>$res7["NAME"]);
                        }
                        if (count($res_st)>0)
                        {
                            if($vals[$d+1]["attributes"]["COUNT"]>$arr_st[$vals[$i]["attributes"]["CODE"]])
                            {
                                $error_ok.="<br>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                                " на складе <b><i>".$res_st[$place]["name"]."</i></b> нужно ".strval($vals[$d+1]["attributes"]["COUNT"]). ", в наличии ".strval($arr_st[$vals[$i]["attributes"]["CODE"]]).".<br><br> Оcтатки в метрах на других складах:<br><br>
                                ";
                                foreach ($res_st as $p=>$s)
                                {
                                    if (($p<>$place)&&($s["count"]>0))
                                    {
                                        $error_ok.="- ".$s["name"]." в наличии ".$s["count"]."<br>";
                                    }
                                }
                                $error_ok.="<hr>";
                            }
                        }
                        else  $error_ok.="<br>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                        " отсутствует на всех складах Компании. Попробуйте выбрать иную кромку на нашем сайте.<hr>";
                    }
                }
            }  
        }
        elseif (($vals[$i]["attributes"]["TYPEID"]=="sheet")&&($vals[$i]["attributes"]["CODE"]>0))
        {
            
            
            if (($vals[$i+1]["tag"]=="PART")&&(!$vals[$i+1]["attributes"]["SHEETID"])&&($vals[$i+1]["attributes"]["USEDCOUNT"]>0))
            {
                // exit('11');
                
                $sql="select * from MATERIAL where CODE=".$vals[$i]["attributes"]["CODE"].";";
                $res2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                $codes=[$vals[$i]["attributes"]["CODE"]];
               
                if (($res2["MATERIAL_ID"]>0)&&($res2['MY_1C_NOM']<>5931))
                {
                    $sql="select * from MATERIAL_STOCK where MATERIAL_ID=".$res2["MATERIAL_ID"].";";
                    $res33=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                    foreach ($res33 as $res4)
                    {
                        $sql2="select NAME from PLACES where PLACES_ID=".$res4["PLACE_ID"].";";
                        $res7=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql2)['data'][0];

                       if ($res4["COUNT"]>0) $res_st[$res4["PLACE_ID"]]=array("count"=>$res4["COUNT"],"name"=>$res7["NAME"]);
                    }
                    if (count($res_st)>0)
                    {
                        // p_$arr_st($vals[$i]["attributes"]["CODE"]);
                        if($vals[$i+1]["attributes"]["USEDCOUNT"]>$arr_st[$vals[$i]["attributes"]["CODE"]])
                        {
                             $error_ok.="<br>выбранный плитный материал ".$vals[$i]["attributes"]["NAME"].
                            " на складе <b><i>".$res_st[$place]["name"]."</i></b> нужно ".$vals[$i+1]["attributes"]["USEDCOUNT"]. ", в наличии ".strval($arr_st[$vals[$i]["attributes"]["CODE"]]).".<br><br> Оcтатки в листах на других складах:<br><br>
                            ";
                            foreach ($res_st as $p=>$s)
                            {
                                if (($p<>$place)&&($s["count"]>0))
                                {
                                    $error_ok.="- ".$s["name"]." в наличии ".$s["count"]."<br>";
                                }
                            }
                            $error_ok.="<hr>";
                        }
    
                    }
                    else  $error_ok.="<br>выбранный плитный материал ".$vals[$i]["attributes"]["NAME"].
                    " отсутствует на всех складах Компании. Попробуйте выбрать иной материал на нашем сайте.<hr>";
                }
            }            
        }
        unset ($res,$res2,$res3,$res4,$res_st);
    }
    return $error_ok;
    
}
function check_art($vals,$link,$place)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    // xml ($vals);
    foreach ($index["GOOD"] as $i)
    {
        unset ($dfd);
        foreach ($index['OPERATION'] as $rt)
        {
            // p_($vals[$rt]);
            
            if (($vals[$rt]["attributes"]["TYPEID"]=="CL")&&($vals[$rt+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"])) 
            {
                $dfd=1;
            }
        }
       
        if ((!isset($dfd))&&(($vals[$i]["attributes"]["TYPEID"]=="sheet")||($vals[$i]["attributes"]["TYPEID"]=="band")))
        {
            
        //     p_($vals[$i]);
        // p_($dfd);
            if (($vals[$i]["attributes"]["TYPEID"]=="band")&&(($vals[$i]["attributes"]["CODE"]>0)&&(is_numeric($vals[$i]["attributes"]["CODE"]))))
            {
                $sql="select BAND_ID from BAND where CODE=".$vals[$i]["attributes"]["CODE"].";";
                // echo $sql;
                $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['count'];
                if (($res==0)&&(!isset($er[$vals[$i]["attributes"]["CODE"]])))
                {
                    
                    $er[$vals[$i]["attributes"]["CODE"]]=1;
                    $error.="<br><B>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                    " отсутствует в БД, просьба выбрать кромку из БД.</b><hr>";
                }
            }
            elseif ($vals[$i]["attributes"]["TYPEID"]=="band")
            {
                $error.="<br><B>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                " отсутствует в БД, просьба выбрать кромку из БД.</b><hr>";
            }
            if ((($vals[$i]["attributes"]["TYPEID"]=="sheet")&&($vals[$i]["attributes"]["MY_DOUBLE"]<>1))&&(($vals[$i]["attributes"]["CODE"]>0)&&(is_numeric($vals[$i]["attributes"]["CODE"]))))
            {
                $sql="select MATERIAL_ID from MATERIAL where CODE=".$vals[$i]["attributes"]["CODE"].";";
                $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                // p_($sql);
                if (($res['count']==0)&&(!isset($er[$vals[$i]["attributes"]["CODE"]])))
                {
                    $er[$vals[$i]["attributes"]["CODE"]]=1;
                    $error.="<br><b>выбранный материал ".$vals[$i]["attributes"]["NAME"].
                    " отсутствует в БД, просьба выбрать материал из БД.</b><hr>";
                }
            }
            elseif (($vals[$i]["attributes"]["TYPEID"]=="sheet")&&($vals[$i]["attributes"]["MY_DOUBLE"]<>1))
            {
                $error.="<br><b>выбранный материал ".$vals[$i]["attributes"]["NAME"].
                " отсутствует в БД, просьба выбрать материал из БД.</b><hr>";
            }
                
            unset ($res,$res2,$res3,$res4,$res_st);
        }
    }
    return $error;
    
}







function do_double($vals,$place,$arr_out,$link)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    // xml ($vals);
    // перебираем сшивки
    // p_($arr_out);exit;
    foreach ($arr_out as $k=>$v)
    {
                    
        if (($v["type_double"]>0)&&($v["back_double"]>0))
        {
            // дублируем лицевую деталь исходник
            foreach ($index["PART"] as $i)
            {
                $vals[$i]=my_uncet($vals[$i],'my_db_id');
                if(($vals[$i]["attributes"]["ID"]==$k)&&(!isset($vals[$i]["attributes"]["SHEETID"]))&&($vals[$i]["attributes"]["DL"]>0))
                {
                    $i_part=$i;
                    $part_temp=$vals[$i];
        // p_($v);exit;
              
                    $dl_start=get_thikness_edge($vals,$vals[$i_part]["attributes"]['ID'],'dl');
                    $dw_start=get_thikness_edge($vals,$vals[$i_part]["attributes"]['ID'],'dw');
                    foreach ($index["OPERATION"] as $iooo)
                    {
                        if (($vals[$iooo]['attributes']['TYPEID']=='CS')&&($vals[$iooo]["type"]=="open"))
                        {
                            $iooo1=$iooo+2;
                            while ($vals[$iooo1]["tag"]=="PART")
                            {
                                if ($vals[$iooo1]['attributes']['ID']==$k) $cs_doub_size_mode=$vals[$iooo]['attributes']['CSIZEMODE'];
                                $iooo1++;
                            }
                        }
                    }
                }
            }
            //узнаём сколько деталей на задней части
            $sql="select * from DOUBLE_CALC where DOUBLE_ID=".$v["type_double"].";";
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

            foreach($sql3 as $sql2)
            {
                $part_back[]=$sql2;
            } 
            
            $count_part_back=count($part_back);
            $vals=move_vals($vals,$i_part+1,$count_part_back);
            $vals[$i_part]["attributes"]["W"]= $vals[$i_part]["attributes"]["W"]+30;
            $vals[$i_part]["attributes"]["L"]= $vals[$i_part]["attributes"]["L"]+30;
            $vals[$i_part]["attributes"]["DW"]= $vals[$i_part]["attributes"]["W"];
            $vals[$i_part]["attributes"]["JW"]= $vals[$i_part]["attributes"]["W"];
            $vals[$i_part]["attributes"]["CW"]= $vals[$i_part]["attributes"]["W"];
            $vals[$i_part]["attributes"]["DL"]= $vals[$i_part]["attributes"]["L"];
            $vals[$i_part]["attributes"]["JL"]= $vals[$i_part]["attributes"]["L"];
            $vals[$i_part]["attributes"]["CL"]= $vals[$i_part]["attributes"]["L"];
            $vals[$i_part]["attributes"]["ELL"]="";
            $vals[$i_part]["attributes"]["ELR"]="";
            $vals[$i_part]["attributes"]["ELT"]="";
            $vals[$i_part]["attributes"]["ELB"]="";
            $vals[$i_part]["attributes"]["ELLMAT"]="";
            $vals[$i_part]["attributes"]["ELRMAT"]="";
            $vals[$i_part]["attributes"]["ELTMAT"]="";
            $vals[$i_part]["attributes"]["ELBMAT"]="";
            unset (
                $vals[$i_part]['attributes']['part.my_kl_elb'],
                $vals[$i_part]['attributes']['part.my_kl_elt'],
                $vals[$i_part]['attributes']['part.my_kl_ell'],
                $vals[$i_part]['attributes']['part.my_kl_elr'],
                $vals[$i_part]['attributes']['part.my_kl_elr_code'],
                $vals[$i_part]['attributes']['part.my_kl_ell_code'],
                $vals[$i_part]['attributes']['part.my_kl_elb_code'],
                $vals[$i_part]['attributes']['part.my_kl_elt_code'],
                $vals[$i_part]['attributes']['part.my_xnc_cut_deltay'],
                $vals[$i_part]['attributes']['part.my_xnc_cut_deltax'],
                $vals[$i_part]['attributes']['part.my_side_to_cut_b'],
                $vals[$i_part]['attributes']['part.my_side_to_cut_l'],
                $vals[$i_part]['attributes']['part.my_side_to_cut_r'],
                $vals[$i_part]['attributes']['part.my_side_to_cut_t'],
                $vals[$i_part]['attributes']['MY_SIDE_TO_CUT_B'],
                $vals[$i_part]['attributes']['MY_SIDE_TO_CUT_T'],
                $vals[$i_part]['attributes']['MY_SIDE_TO_CUT_L'],
                $vals[$i_part]['attributes']['MY_SIDE_TO_CUT_R'],
            );
            $vals[$i_part]["attributes"]["NAME"]="Ст.Л id=[/".$vals[$i_part]["attributes"]["ID"]."/] * ".$v["type_double"];
            $vals[$i_part]["attributes"]["part.name1"]=$vals[$i_part]["attributes"]["NAME"];
            $vals[$i_part]["attributes"]["NAME"];
            $vals[$i_part]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $vals[$i_part]["attributes"]["MY_DOUBLE"]=1;
            $vals[$i_part]["attributes"]["MY_DOUBLE"]=1;
            $vals[$i_part]["attributes"]["MY_DOUBLE_FACE"]=1;
            $vals[$i_part]["attributes"]["part.my_double_type"]=$v["type_double"];
            $vals[$i_part]["attributes"]["MY_DOUBLE_TYPE"]=$v["type_double"];
            $ii=new_id($vals)+1;
            $i_part1=$i_part+1;
            
            foreach ($part_back as $k1=>$v1)
            {
                $count_part++;
                $ii++;
                
               
                if (count($part_back)>1) $txt="false"; else $txt="true";
                $vals[$i_part1++]=Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> Array ("ID"=>$ii,"MY_DOUBLE"=>"1","MY_DOUBLE_PARENT"=>$vals[$i_part]["attributes"]["ID"],
                    "MY_DOUBLE_FACE"=>"0",
                    "L"=>calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["L"]),
                    "W"=>calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["W"]),
                    "DL"=>calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["L"]),
                    "DW"=>calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["W"]),
                    "СL"=>calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["L"]),
                    "СW"=>calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["W"]),
                    "JL"=>calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["L"]),
                    "JW"=>calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["W"]),
                    "part.my_double_type"=>$v["type_double"],
                    "MY_DOUBLE_TYPE"=>$v["type_double"],
                    "MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"],
                    "COUNT"=>$vals[$i_part]["attributes"]["COUNT"],"USEDCOUNT"=>0,
                    "TXT"=>$txt,"NAME"=> "Ст.Н id=[/".$vals[$i_part]["attributes"]["ID"]."/]. ".$count_part."/".$count_part_back." "
                    .$part_temp["attributes"]["NAME"],
                    "part.name1"=> "СН id=[/".$vals[$i_part]["attributes"]["ID"]."/]. ".$count_part."/".$count_part_back." "
                    .$part_temp["attributes"]["NAME"]
                    )
                );
                $parts_id[]=array($ii,"ELL"=>$v1["ELL"],"ELR"=>$v1["ELR"],"ELT"=>$v1["ELT"],"ELB"=>$v1["ELB"]);
            }
            ksort($vals);
            if($v["band_edge"]>0)
            {
                ksort($vals);
    // Если задняя часть кромкуется, добавляем здесь кромку, станок и указатели в детали
                $vals=put_el_into_vals($part_temp["attributes"]["ID"],$parts_id,$vals,$vals[$i_part]["attributes"]["ID"],$place,$v["band_edge"],$link);
                
            }
    // Добавляем порезку задних частей
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);

            foreach ($index["OPERATION"] as $ics)
            {
                if (($vals[$ics]["attributes"]["TYPEID"]=='CS')&&($vals[$ics+1]["attributes"]["ID"]==$v["back_double"]))
                {
                    $ics_temp=$ics+2;
                    while($vals[$ics_temp]['tag']=="PART")
                    {
                        if($vals[$ics_temp]["attributes"]["ID"]==$vals[$i_part]["attributes"]["ID"]) $signal=$ics_temp;
                        $ics_temp++;
                    }
                    if ($signal>0) $ics=$signal-2;


                    $vals=move_vals($vals,$ics+2,count($parts_id)+2);
                    unset ($signal);
                    foreach ($parts_id as $pics)
                    {
                            $vals[$ics+3+$iiii]=Array
                            (
                                "tag" => "PART",
                                "type" => "complete",
                                "level" => 2,
                                "attributes"=> Array ("ID"=>$pics[0],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
                            );
                            $iiii++;
                    }
                    // p_($vals[$ics]);
                }
            }
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
           
    // добавляем деталь сшивки


            $vals=move_vals($vals,$i_part,2);
            $vals[$i_part]=$part_temp;
            unset (
                $vals[$i_part]['attributes']['part.my_kl_elb'],
                $vals[$i_part]['attributes']['part.my_kl_elt'],
                $vals[$i_part]['attributes']['part.my_kl_ell'],
                $vals[$i_part]['attributes']['part.my_kl_elr'],
                $vals[$i_part]['attributes']['part.my_kl_elr_code'],
                $vals[$i_part]['attributes']['part.my_kl_ell_code'],
                $vals[$i_part]['attributes']['part.my_kl_elb_code'],
                $vals[$i_part]['attributes']['part.my_kl_elt_code'],
            );
            foreach ($v as $hg=>$hg1)$vals[$i_part]["attributes"]['part.my_double_par_'.$hg]=$hg1;

            $vals[$i_part]["attributes"]["MY_DOUBLE_RES"]=1;
            $vals[$i_part]["attributes"]["part.my_double_type"]=$v["type_double"];
            $vals[$i_part]["attributes"]["MY_DOUBLE_TYPE"]=$v["type_double"];
            $vals[$i_part]["attributes"]["ELL"]="";
            $vals[$i_part]["attributes"]["ELLMAT"]="";
            $vals[$i_part]["attributes"]["ELT"]="";
            $vals[$i_part]["attributes"]["ELTMAT"]="";
            $vals[$i_part]["attributes"]["ELB"]="";
            $vals[$i_part]["attributes"]["ELBMAT"]="";
            $vals[$i_part]["attributes"]["ELR"]="";
            $vals[$i_part]["attributes"]["ELRMAT"]="";
            $vals[$i_part]["attributes"]["ID"]=new_id($vals);
            $vals[$i_part+2]["attributes"]["MY_DOUBLE_PARENT"]=$vals[$i_part]["attributes"]["ID"];
            $vals[$i_part]["attributes"]["MY_DOUBLE_PARENT"]=0;
            $vals[$i_part]["attributes"]["MY_DOUBLE"]=1;
            $vals[$i_part]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $vals[$i_part]["attributes"]["NAME"]="СШИВКА [/".$part_temp["attributes"]["ID"]."/] ".$vals[$i_part]["attributes"]["NAME"];
            $vals[$i_part]["attributes"]["part.name1"]=$vals[$i_part]["attributes"]["NAME"];
            foreach ($part_temp["attributes"] as $atr=>$meb)
            {
               $vals[$i_part]["attributes"]["_old_".$atr]=$meb;
            }
            ksort($vals);
            
    // добавляем станок порезки, материал сшивки, операцию порезки детали
            $sql="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
            inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
            where type = 'auto' and product = 'sh' and PLACE = ".$place." LIMIT 1";
            $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            if (count($sql3[0])==0){
                $sql="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
                inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
                where type = 'auto' and product = 'sh' and PLACE = 1 LIMIT 1";
                $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
            }
            foreach ($sql3[0] as $k3=>$v3)
            {
                if (!is_int($k3)&&($k3<>"TOOL_CUTTING_ID")&&($k<>"TOOL_CUTTING_EQUIPMENT_ID"))
                {
                    $tool["attributes"][$k3]=$v3;
                }
            }
            $tool["attributes"]["ID"]=new_id($vals);
            $tool["attributes"]["DESCRIPTION"]="Порезка основной части сшивки [/".$part_temp["attributes"]["ID"]."/].";
            $tool["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
    //станок есть, следующий материал сшивки
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
            $first=$vals[get_sheet_num($vals,$part_temp["attributes"]["ID"])];
            $second=$vals[get_sheet_num($vals,$parts_id[0][0])];
            $sheet=Array
                (
                    "tag" => "GOOD",
                    "type" => "open",
                    "level" => 2
                );
            
            $sheet["attributes"]["W"]=$vals[$i_part+1]["attributes"]["W"];
            $sheet["attributes"]["L"]=$vals[$i_part+1]["attributes"]["L"];
            $sheet["attributes"]["T"]=$first["attributes"]["T"]+$second["attributes"]["T"];
            $sheet["attributes"]["T1"]=$first["attributes"]["T"];
            $sheet["attributes"]["T2"]=$second["attributes"]["T"];
            $sheet["attributes"]["UNIT"]=$first["attributes"]["UNIT"];
            $sheet["attributes"]["TYPENAME"]="Листовой";
            $sheet["attributes"]["COUNT"]=$vals[$i_part]["attributes"]["COUNT"];
            $sheet["attributes"]["NAME"]="СШ № [/".$part_temp["attributes"]["ID"]."/] (".$first["attributes"]["CODE"].")  / (".$second["attributes"]["CODE"].")";
            $sheet["attributes"]["ID"]=new_id($vals)+1;
            $sheet["attributes"]["DESCRIPTION"]=$sheet["attributes"]["NAME"];
            $sheet["attributes"]["part.name1"]=$sheet["attributes"]["NAME"];
            $sheet["attributes"]["MY_DOUBLE"]=1;
            $sheet["attributes"]["TYPEID"]="sheet";
            $sheet["attributes"]["MY_FIRST"]=$first["attributes"]["CODE"];
            $sheet["attributes"]["MY_SECOND"]=$second["attributes"]["CODE"];
            $sheet["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $vals[$i_part]["attributes"]['part.my_double_par_first']=$first["attributes"]["CODE"];
            $vals[$i_part]["attributes"]['part.my_double_par_second']=$second["attributes"]["CODE"];
    //добавить листы для основной части сшивки
            $sheet_part_id=new_id($vals)+mt_rand(45000,50005);
            $sheet_part=
                Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> 
                    Array (
                        "L"=>$vals[$i_part+1]["attributes"]["L"],
                        "W"=>$vals[$i_part+1]["attributes"]["W"],
                        "COUNT"=>$vals[$i_part]["attributes"]["COUNT"],
                        "part.my_double_type"=>$v["type_double"],
                        "MY_DOUBLE_TYPE"=>$v["type_double"],
                        "USEDCOUNT"=>0,
                        "ID"=>$sheet_part_id,
                        "MY_DOUBLE"=>"1",
                        "MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"],
                    )
                );
                
                $sheet2=Array
                (
                    "tag" => "GOOD",
                    "type" => "close",
                    "level" => 2
                );
               
    //Операция порезки сшивки
            $operation=Array
            (
                "tag" => "OPERATION",
                "type" => "open",
                "level" => 2
            );
            $operation["attributes"]["ID"]=new_id($vals)+2;
            $operation["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $operation["attributes"]["TYPEID"] = 'CS';
            $operation["attributes"]["TYPENAME"] = "Листовой раскрой";
            $operation["attributes"]["CTRIML"] = 8;
            $operation["attributes"]["CTRIMW"] = 8;
            $operation["attributes"]["CFILLREP"] = 0.9;
            $operation["attributes"]["CSTEXTURE"] = "true";
            $operation["attributes"]["PRINTABLE"] = "true";
            $operation["attributes"]["STARTNEWPAGE"] = "true";
            $operation["attributes"]["W"] = $sheet["attributes"]["W"];
            $operation["attributes"]["T"] = $sheet["attributes"]["T"];
            $operation["attributes"]["L"] = $sheet["attributes"]["L"];
            $operation["attributes"]["CSIZEMODE"] =$cs_doub_size_mode; 
            $operation["attributes"]["TOOL1"] =  $tool["attributes"]["ID"];
    // материал и детали в операцию порезки       
            $material=Array
            (
                "tag" => "MATERIAL",
                "type" => "complete",
                "level" => 2
            );
            $material["attributes"]["ID"]=$sheet["attributes"]["ID"];
            $material["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $operation2=Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
            $operation2["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $parts=Array
            (
                "tag" => "PART",
                "type" => "complete",
                "level" => 2,
                "attributes"=> Array ("ID"=> $vals[$i_part]["attributes"]["ID"],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
            );
            $parts2=Array
            (
                "tag" => "PART",
                "type" => "complete",
                "level" => 2,
                "attributes"=> Array ("ID"=> $sheet_part["attributes"]["ID"],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
            );
            $sheet2["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $tool["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $vals=vals_into_end($vals,$tool);
            $vals=vals_into_end($vals,$sheet);
            $vals=vals_into_end($vals,$sheet_part);
            $vals=vals_into_end($vals,$sheet2);
            $vals=vals_into_end($vals,$operation);
            $vals=vals_into_end($vals,$material);
            $vals=vals_into_end($vals,$parts);
            $vals=vals_into_end($vals,$parts2);
            $vals=vals_into_end($vals,$operation2);

    //Кромкование самой сшивки
            $side=array ("l","r","b","t");
            foreach ($side as $s)
            { 
                
                
                
                if ($v["band_id_".$s]>0)
                {
                    
                    ksort($vals);
                    $vals=vals_out($vals);
                    $index=make_index($vals);
                    $res=check_band($vals,$v["band_id_".$s]);
                    if(!$res["id"])
                    {
                        $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
                        inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
                        where PLACE = ".$place." LIMIT 1";
                        $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                        $tool=Array
                            (
                                "tag" => "GOOD",
                                "type" => "complete",
                                "level" => 2
                            );
                        foreach ($sql3 as $k3=>$v3)
                        {
                            if (!is_int($k3)&&($k3<>"TOOL_EDGELINE_ID")&&($k3<>"TOOL_EDGELINE_EQUIPMENT_ID"))
                            {
                                $tool["attributes"][$k3]=$v3;
                            }
                        }
                        $tool["attributes"]["ID"]=new_id($vals);
                        $tool["attributes"]["MY_DOUBLE"]=1;
                        $tool["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $tool["attributes"]["DESCRIPTION"]="Кромкование cшивки [/".$part_temp["attributes"]["ID"]."/].";
                    
                        $sql="select * from BAND where CODE=".$v["band_id_".$s];
                        $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                        $band1=Array
                            (
                                "tag" => "GOOD",
                                "type" => "complete",
                                "level" => 2
                            );
                        foreach ($sql3 as $k3=>$v3)
                        {
                            if (!is_int($k3)&&($k3<>"BAND_ID"))
                            {
                                $band1["attributes"][$k3]=$v3;
                            }
                        }
                        $bt=$band1["attributes"]["NAME"];
                        $band1["attributes"]["NAME"]="СШ [/".$part_temp["attributes"]["ID"]."/] ".$band1["attributes"]["KR_NAME"];
                        $band1["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $band1["attributes"]["MY_DOUBLE"]=1;
                        unset($band1["attributes"]["KR_NAME"]);
                        $band1["attributes"]["ID"]=new_id($vals)+1;
                        $band1["attributes"]["DESCRIPTION"]="Кромка для кромкования сшивки [/".$part_temp["attributes"]["ID"]."/].";
                        $operation=Array
                            (
                                "tag" => "OPERATION",
                                "type" => "open",
                                "level" => 2
                            );
                        $operation["attributes"]["ID"]=new_id($vals)+2;
                        $operation["attributes"]["MY_DOUBLE"]=1;
                        $operation["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
                        $operation["attributes"]["PRINTABLE"]="false";
                        $operation["attributes"]["STARTNEWPAGE"]="true";
                        $operation["attributes"]["W"]=$band1["attributes"]["W"];
                        $operation["attributes"]["T"]=$band1["attributes"]["T"];
                        $operation["attributes"]["L"]=10000;
                        $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
                        $operation["attributes"]["ELCALCMAT"]="true";
                        $operation["attributes"]["ELROUNDMAT"]="true";
                        $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
                        
                        
                        $operation["attributes"]["TYPEID"]="EL";
                        $operation["attributes"]["DESCRIPTION"]="Кромкооблицовка прямолинейная ".$bt." для кромкования сшивки.";
                        $material=Array
                            (
                                "tag" => "MATERIAL",
                                "type" => "complete",
                                "level" => 2
                            );
                        $material["attributes"]["ID"]=new_id($vals)+1;
                        $material["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $operation2=Array
                            (
                                "tag" => "OPERATION",
                                "type" => "close",
                                "level" => 2
                            );
                        $operation2["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                    }
                    $parts=Array
                    (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 2,
                        "attributes"=> Array ("ID"=>$vals[$i_part]["attributes"]["ID"],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
                    );
                    if (!$res["id"])
                    {
                        $vals=vals_into_end($vals,$tool);
                        $vals=vals_into_end($vals,$band1);
                        $vals=vals_into_end($vals,$operation);
                        $vals=vals_into_end($vals,$material);
                        $vals=vals_into_end($vals,$parts);
                        $vals=vals_into_end($vals,$operation2);
                        ksort($vals);
                        $vals=vals_out($vals);
                        $index=make_index($vals);
                    }
                    else
                    {
                        $operation["attributes"]["ID"]=$res["id"];
                        $band1["attributes"]["NAME"]=$res["name"];
                        if ($res["index"]>0) $rrr=$res["index"]+2;
                        unset ($weq);
                        while ($vals[$rrr]["tag"]=="PART")
                        {
                           
                            if($vals[$rrr]["attributes"]["ID"]== $vals[$i_part]["attributes"]["ID"])
                            {
                                $weq=1;
                            }
                            $rrr++; 
                        }
                        if (!$weq)
                        {
                            $vals=move_vals($vals,$res["index"]+2,1);
                            $vals[$res["index"]+2]=$parts;
                        }
                    }
                    unset($ii_p,$weq);
                    foreach ($index["PART"] as $int)
                    {
                        if (($vals[$int]["attributes"]["ID"]== $vals[$i_part]["attributes"]["ID"])AND($vals[$int]["attributes"]["W"]<>0)) $ii_p=$int;
                    }
                  
                    if ($v["band_id_".$s]>0)
                    {
                       
                        $vals[$ii_p]["attributes"]["EL".mb_strtoupper($s)]="@operation#".$operation["attributes"]["ID"];
                        $vals[$ii_p]["attributes"]["EL".mb_strtoupper($s)."MAT"]=$band1["attributes"]["NAME"];
                        unset($vals[$ii_p]["attributes"]["part.my_kl_el".$s]);
        
                    }
                    unset($ii_n,$operation,$band1);
                    ksort($vals);
            
                }
                //1 - с кромкой,2 - пильные
                if ($cs_doub_size_mode==1)
                {
                    $dl=get_thikness_edge($vals,$vals[$i_part]["attributes"]['ID'],'dl');
                    $dw=get_thikness_edge($vals,$vals[$i_part]["attributes"]['ID'],'dw');
                    $vals[$i_part]["attributes"]['CL']=$vals[$i_part]["attributes"]['DL']-$dl;
                    $vals[$i_part]["attributes"]['L']=$vals[$i_part]["attributes"]['DL']-$dl;
                    $vals[$i_part]["attributes"]['JL']=$vals[$i_part]["attributes"]['DL']-$dl;
                    $vals[$i_part]["attributes"]['CW']=$vals[$i_part]["attributes"]['DW']-$dw;
                    $vals[$i_part]["attributes"]['W']=$vals[$i_part]["attributes"]['DW']-$dw;
                    $vals[$i_part]["attributes"]['JW']=$vals[$i_part]["attributes"]['DW']-$dw;
                }
    // Вносим изменения в XNC и GR операции

                ksort($vals);

                $vals=vals_out($vals);
                $index=make_index($vals);
                foreach ($side as $s)
                {
                    if (isset($part_temp["attributes"]["GR".mb_strtoupper($s)]))
                    {
                        foreach ($index["OPERATION"] as $iop)
                        {
                            if (($vals[$iop]["attributes"]["TYPEID"]=="GR")AND
                            ($vals[$iop]["type"]=="open")AND($vals[$iop+1]["tag"]=="PART"))
                            {
                                $iop1=$iop+1;
                                while ($vals[$iop1]["tag"]=="PART")
                                {
                                    if(($vals[$iop1]["attributes"]["ID"]==$part_temp["attributes"]["ID"]))
                                    {
                                        $vals[$iop1]["attributes"]["ID"]=$vals[$i_part]["attributes"]["ID"];
                                    }
                                    $iop1++;
                                }
                            }
                        }
                    }
                }
                foreach ($index["OPERATION"] as $iop)
                {
                    if (($vals[$iop]["attributes"]["TYPEID"]=="XNC")AND
                    ($vals[$iop]["type"]=="open")AND($vals[$iop+1]["attributes"]["ID"]==$part_temp["attributes"]["ID"]))
                    {
                        if(($vals[$iop]["attributes"]["TYPEID"]=="XNC"))
                        {
                            $vals[$iop+1]["attributes"]["ID"]=$vals[$i_part]["attributes"]["ID"];
                            // $vals[$iop]["attributes"]['MIRHOR']='false';
                            // $vals[$iop]["attributes"]['MIRVERT']='true';
                            $vals[$iop]["attributes"]["NAME"]=$vals[$iop]["attributes"]["NAME"]."СШ [/".$part_temp["attributes"]["ID"]."/] ";
                            $vals[$iop]["attributes"]["part.name1"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop]["attributes"]["TYPENAME"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop]["attributes"]["DESCRIPTION"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop]["attributes"]["MY_DOUBLE"]=1;
                            $vals[$iop]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop]["attributes"]["_old_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop+1]["attributes"]["MY_DOUBLE"]=1;
                            $vals[$iop+1]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop+1]["attributes"]["_old_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop]["attributes"]["_old_T"]=$sheet["attributes"]["T1"];
                            $vals[$iop]["attributes"]["PROGRAM"]=str_replace("dz=\"".$sheet["attributes"]["T1"]."\"","dz=\"".$sheet["attributes"]["T"]."\"",$vals[$iop]["attributes"]["PROGRAM"]);
                        }
                    }
                }
                ksort($vals);
            }
        }
        unset($count_part,$part_back,$count_part_back,$k1,$v1,$ii,$i_part,$i_part1,$parts_id,$part_temp,$tool,$operation);
        
    }
    ksort($vals);

    // p_($vals[0]);exit;
    return($vals);
}
function sort_vals($vals)
{
    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    foreach ($index['GOOD'] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=='product')&&($vals[$i]['type']=='open')&&($vals[$i+1]['tag']=='PART'))
        {
            $j=$i+1;
            while ($vals[$j]['tag']=='PART')
            {
                if ($vals[$j]['attributes']['MY_DOUBLE_ID']>0)
                {
                    $v[$vals[$j]['attributes']['MY_DOUBLE_ID']][$vals[$j]['attributes']['MY_DOUBLE_RES']][$j]=$vals[$j];
                }
                else
               {
                   $v1[$j]=$vals[$j];
               }
               
                $j++;
            }
            ksort($v);
            
            if (count($v)>0)
            {
                // p_($v);exit;
                $j=$i+1;
                while ($vals[$j]['tag']=='PART')
                {
                    unset($vals[$j]);
                    $j++;
                }
                
                $j=$i+1;
                foreach ($v as $k=>$v2)
                {
                    foreach ($v2 as $v3)
                    {
                        foreach ($v3 as $v4)
                        {   
                            $vals[$j]=$v4;
                            $ar_p[]=$j;
                            $j++;
                        }
                    }
                }
                foreach ($v1 as $v2)
                {
                    $vals[$j]=$v2;
                    $ar_p[]=$j;
                    $j++;
                }   
            }         
        }
    }
    // xml($vals);

    // unset($v);
    // unset($v1);
    // unset($v2);
    // unset($v3);
    // unset($v4);
    // unset($v5);
    // ksort($vals);
    // p_($ar_p);
    // foreach ($index['OPERATION'] as $i)
    // {
    //     if (($vals[$i]['attributes']['TYPEID']=='CS')&&($vals[$i]['type']=='open')&&($vals[$i+2]['tag']=='PART'))
    //     {
    //         $j=$i+2;
    //         while ($vals[$j]['tag']=='PART')
    //         {
    //             foreach ($index['PART'] as $i1)
    //             {
                   
    //                 if ((in_array($i1,$ar_p))&&($vals[$i1]['attributes']['ID']==$vals[$j]['attributes']['ID'])&&(isset($vals[$i1]['attributes']['MY_DOUBLE']))&&(!isset($vals[$i1]['attributes']['MY_DOUBLE_RES'])))
    //                 {
                          
    //                     $v[$vals[$i1]['attributes']['MY_DOUBLE_ID']][$vals[$i1]['attributes']['MY_DOUBLE_RES']][$j]=$vals[$j];
    //                     $v[$vals[$i1]['attributes']['MY_DOUBLE_ID']][$vals[$i1]['attributes']['MY_DOUBLE_RES']][$j]['attributes']['MY_DOUBLE_ID']=$vals[$i1]['attributes']['MY_DOUBLE_ID'];
    //                     unset($vals[$j]);
                        
    //                 }    
    //                 elseif((in_array($i1,$ar_p))&&($vals[$i1]['attributes']['ID']==$vals[$j]['attributes']['ID'])&&(!isset($vals[$i1]['attributes']['MY_DOUBLE'])))
    //                 {
    //                     // p_($vals[$j]);
    //                     $v1[$j]=$vals[$j];
    //                     unset($vals[$j]);
    //                 }
                    
    //                 // echo "<hr>";   
                    
    //             }
                
    //             $j++;
    //         }
            
    //         ksort($v);
            
    //         $j=$i+2;
    //         foreach ($v as $k=>$v2)
    //         {
    //             foreach ($v2 as $v3)
    //             {
    //                 foreach ($v3 as $v4)
    //                 {   
    //                     $vals[$j]=$v4;
    //                     $j++;
    //                 }
    //             }
    //         }
    //         foreach ($v1 as $v2)
    //         {
    //             $vals[$j]=$v2;
    //             $j++;
    //         }            
    //     }
    //     ksort($vals);
    //     // p_($vals);exit;
    //     unset($v,$v1);
    // }
   
    ksort($vals);
    return $vals;

}
function put_el_into_vals ($my_double_id,$part_id,$vals,$id_part,$place,$band,$link)
{
    $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
    inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
    where PLACE = ".$place." LIMIT 1";
    $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

    $tool=Array
        (
            "tag" => "GOOD",
            "type" => "complete",
            "level" => 2
        );
    foreach ($sql3 as $k=>$v)
    {
        if (!is_int($k)&&($k<>"TOOL_EDGELINE_ID")&&($k<>"TOOL_EDGELINE_EQUIPMENT_ID"))
        {
            $tool["attributes"][$k]=$v;
        }
    }
    $tool["attributes"]["ID"]=new_id($vals);
    $tool["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    $tool["attributes"]["DESCRIPTION"]="Кромкование нижней части сшивки  [/".$my_double_id."/].";
    $sql="select * from BAND where CODE=".$band;
    $sql3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

    $band1=Array
        (
            "tag" => "GOOD",
            "type" => "complete",
            "level" => 2
        );
    foreach ($sql3 as $k=>$v)
    {
        if (!is_int($k)&&($k<>"BAND_ID"))
        {
            $band1["attributes"][$k]=$v;
        }
    }
    $bt=$band1["attributes"]["NAME"];
    $band1["attributes"]["NAME"]="СШИВКА [/".$my_double_id."/] ".$band1["attributes"]["KR_NAME"];
    $band1["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    unset($band1["attributes"]["KR_NAME"]);
    $band1["attributes"]["ID"]=new_id($vals)+1;
    $band1["attributes"]["DESCRIPTION"]="Кромка для кромкования нижней части сшивки [/".$my_double_id."/].";
    $operation=Array
        (
            "tag" => "OPERATION",
            "type" => "open",
            "level" => 2
        );
    $operation["attributes"]["ID"]=new_id($vals)+2;
    $operation["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
    $operation["attributes"]["PRINTABLE"]="false";
    $operation["attributes"]["STARTNEWPAGE"]="true";
    $operation["attributes"]["W"]=$band1["attributes"]["W"];
    $operation["attributes"]["T"]=$band1["attributes"]["T"];
    $operation["attributes"]["L"]=10000;
    $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
    $operation["attributes"]["ELCALCMAT"]="true";
    $operation["attributes"]["ELROUNDMAT"]="true";
    $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
    if ($sql3['T']<1)
    {
        $operation["attributes"]["ELCOLOR"]="rgb(255,".mt_rand(0,5).",".mt_rand(0,5).")";
        $operation["attributes"]["ELLINEMARK"]="1";
    }
    else
    {
        $operation["attributes"]["ELCOLOR"]="rgb(".mt_rand(0,5).",255,".mt_rand(0,5).")";
        $operation["attributes"]["ELLINEMARK"]="3";
    }
    
    $operation["attributes"]["TYPEID"]="EL";
    $operation["attributes"]["DESCRIPTION"]="Кромкооблицовка прямолинейная ".$bt." для кромкования нижней части сшивки [/".$my_double_id."/].";
    $material=Array
        (
            "tag" => "MATERIAL",
            "type" => "complete",
            "level" => 2
        );
    $material["attributes"]["ID"]=new_id($vals)+1;
    $material["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    $operation2=Array
        (
            "tag" => "OPERATION",
            "type" => "close",
            "level" => 2
        );
    $operation2["attributes"]["MY_DOUBLE_ID"]=$my_double_id;

    foreach ($part_id as $idp)
    {
        $parts[]=Array
        (
            "tag" => "PART",
            "type" => "complete",
            "level" => 2,
            "attributes"=> Array ("ID"=>$idp[0],"MY_DOUBLE_ID"=>$my_double_id)
        );
       
        
        
    }
  
    if(count($parts)>0)
    {
        $vals=vals_into_end($vals,$tool);
        $vals=vals_into_end($vals,$band1);
        $vals=vals_into_end($vals,$operation);
        $vals=vals_into_end($vals,$material);
        foreach($parts as $q) $vals=vals_into_end($vals,$q);
        $vals=vals_into_end($vals,$operation2);
    }
    ksort($vals);
    $index=make_index($vals);
    reset($part_id);
    foreach ($part_id as $idp)
    {
        foreach ($index["PART"] as $in)
        {
            if (($vals[$in]["attributes"]["ID"]==$idp[0])&&($vals[$in]["attributes"]["L"]>0)) $ii_p=$in;
        }
        $e=array("L","R","T","B");
        foreach ($e as $l)
        {
            if ($idp["EL".$l]<>"")
            {
                $vals[$ii_p]["attributes"]["EL".$l]="@operation#".$operation["attributes"]["ID"];
                $vals[$ii_p]["attributes"]["EL".$l."MAT"]=$band1["attributes"]["NAME"];

            }
        }
        unset($ii_n);
    }


    unset($tool,$band1,$operation,$operation2,$parts);

    return $vals;
}

function el_type_lines($vals,$type,$s)
{
    // p_($s);exit;
    if ((isset($s['manager_id']))&&(is_numeric($s['manager_id'])))
    {
        $sql="SELECT * FROM manager WHERE id=".$s['manager_id'];

        $s2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
        
        if($s2['admin']>0)$s['isAdmin']=1;
    }
    $vals=vals_out($vals);
    $index=make_index($vals);
    $letter=array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    $num_l=-1;
    foreach ($index["OPERATION"] as $i)
    {
        
        if (($vals[$i]["attributes"]["TYPEID"]=="EL")&&($vals[$i]["type"]=="open"))
        {
            // p_($vals[$i]);
            $num_l++;
            unset($code);
            foreach ($index["GOOD"] as $i1)
            {
                if (($vals[$i1]["attributes"]["TYPEID"]=="band")&&($vals[$i1]["attributes"]["ID"]==$vals[$i+1]["attributes"]["ID"]))
                {
                    $code=$vals[$i1]["attributes"]["CODE"];
                    $vals[$i]["attributes"]["ELSYMBOL"]=$letter[$num_l];
                
            
                    if (($code>0)&&(is_numeric($code)))
                    {
                        $sql = "SELECT b.*, (SELECT VALUESTR FROM `GOOD_PROPERTIES_BAND` WHERE `BAND_ID` = b.`BAND_ID` and `PROPERTY_ID` = 151 LIMIT 1) as proizv FROM `BAND` b WHERE CODE=".$code.";";
                        $sql2 = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                        if (isset($sql2))
                        {
                            if ($sql2["MY_LASER"]==1) {

                                $vals[$i]["attributes"]["MY_EL_TYPE"] = "laz";

                            } elseif ((($sql2["MY_LASER"] <> 1)) AND ((($sql2['proizv'] == "MAAG") && ($sql2['T'] > 1))) && (!isset($s['isAdmin']))) {

                                // $vals[$i]["attributes"]["MY_EL_TYPE"] = "eva";
                                $vals[$i]["attributes"]["MY_EL_TYPE"] = $type;

                            } elseif ((($sql2["MY_LASER"] <> 1)) AND ((($sql2['proizv'] == "MAAG") && ($sql2['T'] > 1))) && ($s['isAdmin'] == 1)) {

                                $vals[$i]["attributes"]["MY_EL_TYPE"] = $type;

                            } else {

                                $vals[$i]["attributes"]["MY_EL_TYPE"] = $type;

                            }

                            $vals[$i]["attributes"]["operation.my_el_type"]=$vals[$i]["attributes"]["MY_EL_TYPE"];
                            $iy=$i+2;
                            
                            while ($vals[$iy]["tag"]=="PART")
                            {
                                foreach ($index["PART"] as $im)
                                {
                                    if (($vals[$im]["attributes"]["ID"]==$vals[$iy]["attributes"]["ID"])&&($vals[$im]["attributes"]["L"]>0))
                                    {
                                        $vals[$im]["attributes"]["part.my_el_type"]=$vals[$i]["attributes"]["MY_EL_TYPE"];
                                    }
                                }
                                $iy++;
                            }
                            // для ПВХ толщиной 2мм – зеленый цвет RGB 0, 102, 0
                            // Для ПВХ 0,5 мм красный цвет RGB 204,0,0
                            // ПВХ 0,4 мм красный цвет RGB 153,0,0
                            // Остальные ПВХ толщиной от 0,7 мм до 1,9 мм RGB 153,0,153
                            // Для ПВХ 0,6 мм красный цвет RGB 204,0,51

                            if ($sql2["T"]==2)
                            {
                                $vals[$i]["attributes"]["ELLINEMARK"]="3";
                                $vals[$i]["attributes"]["ELCOLOR"]="rgb(0,102,0)";
                            } 
                            elseif ($sql2["T"]==0.5)
                            {
                                $vals[$i]["attributes"]["ELLINEMARK"]="1";
                                $vals[$i]["attributes"]["ELCOLOR"]="rgb(204,0,0)";
                            } 
                            elseif ($sql2["T"]==0.4)
                            {
                                $vals[$i]["attributes"]["ELLINEMARK"]="1";
                                $vals[$i]["attributes"]["ELCOLOR"]="rgb(153,0,0)";
                            } 
                            elseif ($sql2["T"]==0.6)
                            {
                                $vals[$i]["attributes"]["ELLINEMARK"]="1";
                                $vals[$i]["attributes"]["ELCOLOR"]="rgb(204,0,51)";
                            } 
                            elseif (($sql2["T"]>0.6)&&($sql2["T"]<2))
                            {
                                $vals[$i]["attributes"]["ELLINEMARK"]="2";
                                $vals[$i]["attributes"]["ELCOLOR"]="rgb(153,0,153)";

                            }
                            else
                            {
                                $vals[$i]["attributes"]["ELLINEMARK"]="1";
                                $vals[$i]["attributes"]["ELCOLOR"]="rgb(255,0,0)"; 

                            }
                        }
                        // elseif (isset($code))
                        // {
                        //     $_SESSION['error'].="<br>Кромка ".$vals[$i1]["attributes"]["NAME"]." с кодом ".$code." не найдена в БД.<hr>";
                        //     // p_($code);
                        //     // exit('11');

                        // }
                    
                        if (($sql2['proizv']=="MAAG")&&($sql2['T']>1)&&($type=="pur"))
                        {
                            // p_($sql2);
                            $_SESSION['error_ok'].="<br>Кромка ".$sql2["NAME"]." с кодом ".$code." не может быть закромкована ПУР поклейкой в силу особенностей кромки. Для этой кромки будет использоваться ЭВА.<hr>";
                        }
                    }
                    unset($code);
                }
            }
        }
    }
    return $vals;
}
function vals_index_to_project($vals)
{
   //p_($vals);
    include ("description.php");
    foreach ($vals as $tr=>$v)
    {
        foreach ($v["attributes"] as $ka=>$va)
        {
            if (isset($v["attributes"][mb_strtolower($ka)])&&isset(($v["attributes"][mb_strtoupper($ka)])))
            {
                unset ($v["attributes"][mb_strtoupper($ka)]);
            }   
        }
        if ($v["type"]=="open")
        {
            $text.="<".mb_strtolower($v["tag"])." ";
            foreach ($v["attributes"] as $ka=>$va)
            {
                
                unset($vals[$tr]['attributes'][$ka]);
                unset($vals[$tr]['attributes'][mb_strtoupper($ka)]);
                unset($vals[$tr]['attributes'][mb_strtolower($ka)]);
                
                if($v["tag"]<>"PROGRAM")
                {
                    $va=htmlspecialchars($va);
                   
                }
                if (!$gib_per[$ka])
                {
                    if ((substr_count($ka,"MY_")>0)&&(substr_count($ka,"PART")==0)&&(substr_count($ka,"PROJECT")==0)&&(substr_count($ka,"GOOD")==0)&&(substr_count($ka,"OPERATION")==0)) $gib_per[$ka]=$ka;
                    else $gib_per[$ka]=mb_strtolower($ka);
                    // echo $ka."<br>";
                }
                if ($ka==array_key_last($v["attributes"])) $text.=$gib_per[$ka]."=\"".$va."\"";
                else $text.=$gib_per[$ka]."=\"".$va."\" ";
            }
            $text.=">";
        }
        elseif ($v["type"]=="complete")
        {
            $text.="<".mb_strtolower($v["tag"])." ";
            foreach ($v["attributes"] as $ka=>$va)
            {
                if($v["tag"]<>"PROGRAM")$va=htmlspecialchars($va);
                
                if (!$gib_per[$ka])
                {
                    if ((substr_count($ka,"MY_")>0)&&(substr_count($ka,"PART")==0)&&(substr_count($ka,"PROJECT")==0)&&(substr_count($ka,"GOOD")==0)&&(substr_count($ka,"OPERATION")==0)) $gib_per[$ka]=$ka;
                    else $gib_per[$ka]=mb_strtolower($ka);
                }
                if ($ka==array_key_last($v["attributes"])) $text.=$gib_per[$ka]."=\"".$va."\"";
                else $text.=$gib_per[$ka]."=\"".$va."\" ";
                // if($v["tag"]=="BR")
                // {
                //     p_($va);
                //     echo "<hr>";
                // }
            }
            $text.="/>";
        }
        elseif ($v["type"]=="close")
        {
            $text.="</".mb_strtolower($v["tag"]).">";
        }
        
    }
   
    $text=quot($text);
                
    // p_($text);
    // exit;
    return $text;
}

/**
 * Функция меняет прямолинейное кромкование на криволинейное на деталях, одна сторона которых меньше или равна 60мм
 * $vals - массив проекта
 * $arr_out - массив отобранных деталей ([part_id] => id детали в массиве проекта [xnc_band_l (_l - кромкуемая сторона)] => код кромки)
 * $link - ссылка на подключение к БД
**/
function change_kl_edge ($vals, $arr_out, $link) {

    // Массив $arr_out содержит массив(ы) с деталями, одна сторона которых меньше или равна 60мм,
    // чтобы заменить прямолинейное кромкование на криволинейное

    $vals = vals_out($vals);
    $index = make_index($vals);
    ksort($vals);

    // В блоке удаляются кромки, операции кромкования и детали с данным кромкованием
    foreach ($index["OPERATION"] as $k) {

       if (($vals[$k]["attributes"]["TYPEID"] == "EL") && ($vals[$k]["attributes"]["MY_CHECK_KL"] == 1)) {

            foreach ($index["GOOD"] as $e) {

                if (($vals[$e]["attributes"]["TYPEID"] == "tool.edgeline") && ($vals[$e]["attributes"]["ID"] == $vals[$k]["attributes"]["TOOL1"])) {
                    unset ($vals[$e]);
                } elseif (($vals[$e]["attributes"]["TYPEID"] == "band") && ($vals[$e]["attributes"]["ID"] == $vals[$k + 1]["attributes"]["ID"])) {
                    unset ($vals[$e]);
                }
            }
            
            $r = $k + 2;
            while ($vals[$r]["tag"] == "PART") {
                unset ($vals[$r]);
                $r++;
            }

            unset ($vals[$r]);
            unset ($vals[$k]);
            unset ($vals[$k + 1]);
       }
    }

    $index = make_index($vals);
    $vals = vals_out($vals);
    ksort($vals);
    $side = array("l", "r", "t", "b");

    // Обработка криволинейных кромкований
    foreach ($index["PART"] as $i) {
        foreach ($side as $s) {
            if ((isset($vals[$i]["attributes"]["part.my_kl_el".$s])) OR ($vals[$i]["attributes"]["part.my_kl_el" . $s] <> "")
            OR (isset($vals[$i]["attributes"]["PART.MY_KL_EL" . mb_strtoupper($s)]))
            OR ($vals[$i]["attributes"]["PART.MY_KL_EL" . mb_strtoupper($s)] <> "")) {
                $vals[$i] = my_uncet($vals[$i],'my_kl_el' . $s);
                $vals[$i] = my_uncet($vals[$i],'my_kl_el' . $s . '_code');
                unset ($vals[$i]["attributes"]["EL" . mb_strtoupper($s)]);
                unset ($vals[$i]["attributes"]["EL" . mb_strtoupper($s) . "MAT"]);
            }
        }
    }

    $vals = vals_out($vals);
    $index = make_index($vals);

    foreach ($arr_out as $v) {
        foreach ($side as $s) {
            if (!in_array($v["xnc_band_" . $s], $xnc_band)) $xnc_band[] = $v["xnc_band_" . $s];
        }
    }
    
    // Массив использующихся кромок
    $xnc_band = array_unique($xnc_band);
    $xnc_band = array_diff($xnc_band, array(0));
    $xnc_band = array_diff($xnc_band, array(''));

    // Тут собираются массивы кромок и операций кромкования (криволинейного)
    foreach ($xnc_band as $i=>$band) {

        // Блок проверяет, есть ли сшивка
        foreach ($arr_out as $a => $b) {
           foreach($side as $s) {
                if ($band == $b["xnc_band_" . $s]) {
                    foreach ($index["PART"] as $n) {
                        if (($vals[$n]["attributes"]["ID"] == $b["part_id"])
                                && ($vals[$n]["attributes"]["L"] > 0)
                                && ($vals[$n]["attributes"]["MY_DOUBLE_PARENT"] > 0))
                        {
                            $arr_out[$vals[$n]["attributes"]["MY_DOUBLE_PARENT"]] = $arr_out[$b["part_id"]];
                            $b["part_id"] = $vals[$n]["attributes"]["MY_DOUBLE_PARENT"];

                        }
                    }
                    $part_b[$band][$s][] = $b["part_id"];
                }
            }
        }

        // Проверяем есть ли криволинейка по этому коду кромки
        $res_band = check_band_kl($vals,$band);

        $id_pro = $id_pro + 4;
        if (isset($res_band["id"]) && $res_band["id"] > 0) {
            $operation["attributes"]["ID"] = $res_band["id"];
            $band1["attributes"]["NAME"] = $res_band["name"];
            $band_in = $res_band["index"];
            $band_code = $res_band["code"];
            $band_array[$band] = array (
                "op" => $operation,
                "band1" => $band1,
                "band_in" => $band_in,
                "band_code" => $band_code
            );
        } else {
            $sql = "select * from TOOL_EDGELINE where TOOL_EDGELINE_ID = 4";
            $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];

            $tool = Array (
                "tag" => "GOOD",
                "type" => "complete",
                "level" => 2
            );

            foreach ($sql3 as $k => $v) {
                if (!is_int($k) && ($k <> "TOOL_EDGELINE_ID")) {
                    $tool["attributes"][$k] = $v;
                }
            }

            $tool["attributes"]["ID"] = new_id($vals) + $id_pro;
            $tool["attributes"]["MY_CHECK_KL"] = "1";
            $sql = "select * from BAND where CODE=" . $band;

            $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];

            if (!isset($sql3)) {
                $index = make_index($vals); 
                foreach ($index['GOOD'] as $bb) {
                    if (($vals[$bb]['attributes']["TYPEID"] == "band") && ($vals[$bb]['attributes']['CODE'] == $band)) {
                        if(!isset($vals[$bb]['attributes']['W'])) $vals[$bb]['attributes']['W'] = 22;
                        if(!isset($vals[$bb]['attributes']['T'])) $vals[$bb]['attributes']['T'] = 2;
                        $sql = "select * from BAND where MY_1C_NOM=6149 AND W=" . $vals[$bb]['attributes']['W'] . " AND T=" . $vals[$bb]['attributes']['T'];
                        $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                        $rr = 0;

                        while (!isset($sql3)) {
                            $sql = "select * from BAND where MY_1C_NOM=6149 AND W=".$vals[$bb]['attributes']['W']." AND T=".($vals[$bb]['attributes']['T']+$rr);
                            $sql3 = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql)['data'][0];
                            $rr = $rr + 0.1;
                        }

                        $b123[] = $bb;
                        foreach($sql3 as $rrr3 => $rrr4) {
                            if (!is_integer($rrr3)) $vals[$bb]['attributes'][$rrr3] = $rrr4;
                        }
                        $vals[$bb]['attributes']['COUNT'] = 10000;
                    }
                }
            }
            $xnc_band[$i] = $sql3["CODE"];

            foreach ($b123 as $bb) $vals[$bb]['attributes']["CODE"] = $sql3["CODE"];
            unset ($b123);
            $band1 = Array (
                "tag" => "GOOD",
                "type" => "complete",
                "level" => 2
            );

            foreach ($sql3 as $k => $v) {
                if (!is_int($k)) {
                    $band1["attributes"][$k] = $v;
                }
            }
            
            $bt = $band1["attributes"]["NAME"];
            $band1["attributes"]["NAME"] = "КЛ " . $band1["attributes"]["KR_NAME"];
            unset($band1["attributes"]["KR_NAME"]);
            $band_code = $band1["attributes"]["CODE"];
            $band1["attributes"]["ID"] = new_id($vals) + 1 + $id_pro;
            $band1["attributes"]["MY_CHECK_KL"] = "1";

            $operation = Array (
                "tag" => "OPERATION",
                "type" => "open",
                "level" => 2
            );

            $operation["attributes"]["ID"] = new_id($vals) + 2 + $id_pro;
            $operation["attributes"]["MY_CHECK_KL"] = "1";
            $operation["attributes"]["TYPENAME"] = "Кромкооблицовка криволинейная";
            $operation["attributes"]["PRINTABLE"] = "false";
            $operation["attributes"]["STARTNEWPAGE"] = "true";
            $operation["attributes"]["W"] = $band1["attributes"]["W"];
            $operation["attributes"]["T"] = $band1["attributes"]["T"];
            $operation["attributes"]["L"] = 10000;
            $operation["attributes"]["ELWASTEPRC"] = $tool["attributes"]["ELWASTEPRC"];
            $operation["attributes"]["ELCALCMAT"] = "true";
            $operation["attributes"]["ELROUNDMAT"] = "true";
            $operation["attributes"]["TOOL1"] = $tool["attributes"]["ID"];
            
            if ($band1["attributes"]["T"] < 1) {
                 $operation["attributes"]["ELLINEMARK"] = "1";
                 $operation["attributes"]["ELCOLOR"] = "rgb(255," . mt_rand(0, 5) . "," . mt_rand(0,5) . ")";
            } else {
                $operation["attributes"]["ELLINEMARK"] = "3";
                $operation["attributes"]["ELCOLOR"] = "rgb(" . mt_rand(0, 5) . ", 255, " . mt_rand(0,5) . ")";
            }
            $operation["attributes"]["TYPEID"] = "EL";
            $operation["attributes"]["DESCRIPTION"] = "Кромкооблицовка криволинейная для кромки " . $bt;
            $material = Array (
                "tag" => "MATERIAL",
                "type" => "complete",
                "level" => 2
            );
            $material["attributes"]["ID"] = new_id($vals) + 1 + $id_pro;
            $material["attributes"]["MY_CHECK_KL"] = 1;
            $operation2 = Array (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
            $band_array[$band] = array (
                "operation" => $operation,
                "operation2" => $operation2,
                "band1" => $band1,
                "band_in" => $band_in,
                "band_code" => $band_code,
                'tool' => $tool,
                "material" => $material
            );
        }
    }

    foreach ($band_array as $code_el_kl => $band) {
        foreach(SIDE as $s) {
            foreach ($part_b[$code_el_kl][$s] as $b) {
                foreach ($index["PART"] as $i4) {
                    if (($vals[$i4]["attributes"]["ID"] == $b) && ($vals[$i4]["attributes"]["CL"] > 0)) $part_val = $i4;
                }
                
                // Тут детали присваивается криволинейное кромкование
                if ($arr_out[$b]["xnc_band_" . $s] > 0) {
                    $vals[$part_val] = my_uncet($vals[$part_val], "my_cut_angle_" . $s);
                    $vals[$part_val]["attributes"]["EL" . mb_strtoupper($s)] = "@operation#" . $band['operation']["attributes"]["ID"];
                    $vals[$part_val]["attributes"]["EL" . mb_strtoupper($s) . "MAT"] = $band['band1']["attributes"]["NAME"];
                    $vals[$part_val]["attributes"]["part.my_kl_el" . $s] = 1;
                    $vals[$part_val]["attributes"]["part.my_kl_el" . $s . "_code"] = $code_el_kl;
                    
                    $parts = Array (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 2,
                        "attributes" => Array ("ID" => $b)
                    );

                    if((!isset($band['band_in'])) && (!isset($band_array[$code_el_kl]['op_in']))) {
                        
                        $band_array[$code_el_kl]['op_in'] = 1;
                        $vals = vals_into_end($vals, $band['tool']);
                        $vals = vals_into_end($vals,$band['band1']);
                        $vals = vals_into_end($vals,$band['operation']);
                        $band_array[$code_el_kl]['band_in'] = count($vals) - 2;
                        $vals = vals_into_end($vals, $band['material']);
                        $vals = vals_into_end($vals, $parts);
                        $vals = vals_into_end($vals, $band['operation2']);
                        ksort($vals);
                        $vals = vals_out($vals);
                        $index = make_index($vals);
                    } elseif($band_array[$code_el_kl]['band_in'] > 0) {
                        $c_v = $band_array[$code_el_kl]['band_in'] + 2;
                        while ($vals[$c_v]["tag"] == "PART") {
                            if ($vals[$c_v]['attributes']['ID'] == $b) {
                                $no = 1;
                            }
                            $c_v++;
                        }
                        if (!isset($no)) {
                            $c_v = $band_array[$code_el_kl]['band_in'] + 2;
                            $vals = move_vals($vals, $c_v, 1);
                            $vals[$c_v] = $parts;
                            ksort($vals);
                            $vals = vals_out($vals);
                            $index = make_index($vals);
                        } else unset ($no);
                    }
                    ksort($vals);
                    $vals = vals_out($vals);
                    $index = make_index($vals);
                }
            }
        }
        unset($tool, $band1, $operation, $operation2, $parts, $pt, $band_in, $renm);
    }

    return $vals;
}

function change_st_edge ($vals,$arr_out,$link)
{
    $vals=vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    $side=array("l","r","t","b");
    foreach ($arr_out as $v)
    {
        foreach ($side as $s)
        {
            // p_($v["xnc_band_".$s]);
            if (!in_array($v["band_".$s],$xnc_band)) $xnc_band[]=$v["band_".$s];
        }
    }
    $xnc_band=array_unique($xnc_band);
    $xnc_band=array_diff($xnc_band, array(0));
    $xnc_band=array_diff($xnc_band, array(''));
    foreach ($xnc_band as $i=>$band)
    {
        foreach ($arr_out as $a=>$b)
        {
           foreach($side as $s)
           {
                if ($band==$b["band_".$s])
                {
                    $part_b[$band][$s][]=$b["part_id"];
                }
            }
        }
        $res_band=check_band($vals,$band);
        $operation["attributes"]["ID"]=$res_band["id"];
        $band1["attributes"]["NAME"]=$res_band["name"];
        $band_in=$res_band["index"];
        $band_code=$res_band["code"];
       
        reset ($side);
        foreach($side as $s)
        {
            foreach ($part_b[$band][$s] as $b)
            {
                foreach ($index["PART"] as $i4)
                {
                    if (($vals[$i4]["attributes"]["ID"]==$b)&&($vals[$i4]["attributes"]["L"]>0)) $part_val=$i4;
                }
                    
                if ($arr_out[$b]["band_".$s]>0)
                {
                    $vals[$part_val]=my_uncet($vals[$part_val],"my_cut_angle_".$s);
                    $vals[$part_val]=my_uncet($vals[$part_val],"my_kl_el".$s);
                    $vals[$part_val]=my_uncet($vals[$part_val],"my_kl_el".$s.'_code');
                    $parts=Array
                    (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 2,
                        "attributes"=> Array ("ID"=>$b)
                    );
                    if($band_in>0)
                    {
                        $vals[$part_val]["attributes"]["EL".mb_strtoupper($s)]="@operation#". $operation["attributes"]["ID"];
                        $vals[$part_val]["attributes"]["EL".mb_strtoupper($s)."MAT"]=$band1["attributes"]["NAME"];
                        $t1=$band_in+2;
                        unset ($yes);
                        while ($vals[$t1]["tag"]=="PART")
                        {
                            if ($vals[$t1]['attributes']['ID']==$b) $yes=1;
                            $t1++;
                        }
                        if (!isset($yes))
                        {
                            $t1=$band_in+2;
                            $vals=move_vals($vals,$t1,1);
                            $vals[$t1]=$parts;
                            ksort($vals);
                            $vals=vals_out($vals);
                            $index=make_index($vals);
                        }
                    }
                }
            }
        }
       
        unset($tool,$band1,$operation,$operation2,$parts,$pt,$band_in,$renm);
    }
    // p_($vals);
    return $vals;
}

function check_engle_edge ($vals,$array)
{
    $side=array("l","r","t","b");
    foreach ($index["PART"] as $v)
    {
        if (($vals[$v]["attributes"]["CL"]>0))
        {
            
            foreach ($side as $s)
            {
                $vals[$v]=my_uncet($vals[$v],"my_cut_angle_".$s);
            }
            
        }
    }
    $vals=vals_out($vals);
    $index=make_index($vals);
    // p_($array);exit;
    foreach ($array as $part_id=>$cut)
    {
        foreach ($index["PART"] as $v)
        {
            if (($vals[$v]["attributes"]["ID"]==$part_id)AND($vals[$v]["attributes"]["CL"]>0)AND
           ($vals[$v]["attributes"]["MY_DOUBLE_PARENT"]>0))
            {
                
                $array[$vals[$v]["attributes"]["MY_DOUBLE_PARENT"]]=$array[$part_id];
                $part_id=$vals[$v]["attributes"]["MY_DOUBLE_PARENT"];
            }
        }
        foreach ($index["PART"] as $v)
        {
            
            if (($vals[$v]["attributes"]["ID"]==$part_id)AND($vals[$v]["attributes"]["CL"]>0))
            {
                // p_($cut);exit;
                foreach ($cut as $k1=>$v1)
                {
                    // p_($k1);
                    foreach (SIDE as $s) if (substr_count($k1,'_'.mb_strtoupper($s))>0) $s_side=$s;
                    // p_($v1);
                    // p_($cut);
                    // p_($s_side);
                    // exit;
                    // $side=substr($k1,strlen($k1)-1,1);
                    // p_($v1);
                    if (($v1<>0)&&(substr_count($k1,'SDVIG')==0))
                    {
                        
                        // $vals[$v]["attributes"][$k1]=$v1;

                        if (isset($vals[$v]["attributes"]["EL".$s_side]))
                        {
                            $id_op=substr($vals[$v]["attributes"]["EL".$s_side],strpos($vals[$v]["attributes"]["EL".$s_side],"#")+1);
                            foreach ($index["OPERATION"] as $k)
                            {
                                
                                if (($vals[$k]["attributes"]["TYPEID"]=="EL")&&($vals[$k]["attributes"]["ID"]==$id_op))
                                {
                                    
                                        $r=$k+2;
                                        while(($vals[$r]["tag"]=="PART"))
                                        {
                                            if ($vals[$r]["attributes"]["ID"]==$vals[$v]["attributes"]["ID"]) unset ($vals[$r]);
                                            $r++;
                                        }
                                }
                            }
                        }
                        unset($vals[$v]["attributes"]["EL".$s_side]);
                        unset($vals[$v]["attributes"]["EL".$s_side."MAT"]);
                        // p_($s_side);
                        $vals[$v]["attributes"]["part.my_cut_angle_".$s_side]=$cut['MY_CUT_ANGLE_'.mb_strtoupper($s_side)];
                        $vals[$v]["attributes"]["part.my_cut_angle_sdvig_".$s_side]=$cut['MY_CUT_SDVIG_ANGLE_'.mb_strtoupper($s_side)];
                        $vals[$v]["attributes"][mb_strtoupper('my_cut_angle_'.$s_side)]=$cut['MY_CUT_ANGLE_'.mb_strtoupper($s_side)];
                        $vals[$v]["attributes"][mb_strtoupper('my_cut_angle_sdvig_'.$s_side)]=$cut['MY_CUT_SDVIG_ANGLE_'.mb_strtoupper($s_side)];
                        // p_($vals[$v]);exit;
                    }
                    elseif (($v1==0)&&(substr_count($k1,'SDVIG')==0))
                    {
                        // exit('11');
                        // p_($v1);
                        // p_($s_side);
                        // p_($vals[$v]);
                        unset($vals[$v]["attributes"]["part.my_cut_angle_".$s_side]);
                        unset($vals[$v]["attributes"][mb_strtoupper('my_cut_angle_'.$s_side)]);
                        // p_($vals[$v]);

                    }
                    unset ($side);
                }
            }
        }
    }
    // p_ ($vals);exit;
    return $vals;
}

function edge_get($vals,$sheet,$link,$filter)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    $sql='SELECT * FROM MATERIAL WHERE CODE='.$sheet;
    $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

    // p_($mat);
    foreach ($index['GOOD'] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=='band')&&(!isset($code[$vals[$i]['attributes']['CODE']])))
        {
            if (($vals[$i]['attributes']['CODE']>0)&&(is_numeric($vals[$i]['attributes']['CODE'])))
            {
                $code[$vals[$i]['attributes']['CODE']]=1;
                if ($vals[$i]['attributes']['W']>$mat['T']+10+3)
                {
                    $sql='SELECT * FROM BAND WHERE CODE='.$vals[$i]['attributes']['CODE'];
                    $band=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                    $band_pr[$band['BAND_ID']]=array('id'=>$band['BAND_ID'],'w'=>$band['W'],'name'=>$band['NAME'],'code'=>$band['CODE'],'type'=>'big');
                    if (!in_array($band['BAND_ID'],$band_pr__)) $band_pr__[]=$band['BAND_ID'];
                } else if($filter=="no") {
                    $sql='SELECT * FROM BAND WHERE CODE='.$vals[$i]['attributes']['CODE'];
                    $band=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                    $band_pr[$band['BAND_ID']]=array('id'=>$band['BAND_ID'],'w'=>$band['W'],'name'=>$band['NAME'],'code'=>$band['CODE'],'type'=>'small');
                    if (!in_array($band['BAND_ID'],$band_pr__)) $band_pr__[]=$band['BAND_ID'];
                }
            }
            
        } 
    }
    // p_($band_pr);
    $sql="SELECT * FROM MATERIAL_BAND_CONNECTION WHERE MATERIAL_ID=".$mat['MATERIAL_ID'];
    $connn=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

    foreach ($connn as $conn)
    {
        $conn2[]=$conn;
    }
    // p_($conn2);
    foreach ($conn2 as $v)
    {
        if (in_array($v['BAND_ID'],$band_pr__))
        {
            $band_rec[$v['BAND_ID']]=$band_pr[$v['BAND_ID']];
            unset ($band_pr[$v['BAND_ID']]);
        }
        else
        {
            $sql='SELECT * FROM BAND WHERE BAND_ID='.$v['BAND_ID'];
            $band=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

            if ($band['W']>$mat['T']+10+3)
            {
                $band_rec[$band['BAND_ID']]=array('id'=>$band['BAND_ID'],'w'=>$band['W'],'name'=>$band['NAME'],'code'=>$band['CODE'],'type'=>'big');
            } else if($filter=="no") {
                $band_rec[$band['BAND_ID']]=array('id'=>$band['BAND_ID'],'w'=>$band['W'],'name'=>$band['NAME'],'code'=>$band['CODE'],'type'=>'small');
            }
        }
    }
    unset($band_pr__);
    // $band_rec=array_unique($band_rec);
    // $band_pr=array_unique($band_pr);
    foreach ($band_rec as $v)
    {
        $band_for_select.='<option value="'.$v['code'].'" data-w="'.$v['w'].'" data-type="'.$v['type'].'">[рек]'.$v['name'].'</option>';
    }
    foreach ($band_pr as $v)
    {
        $band_for_select.='<option value="'.$v['code'].'" data-w="'.$v['w'].'" data-type="'.$v['type'].'">[пр]'.$v['name'].'</option>';
    }
    // p_($band_for_select);

    return $band_for_select;
    // p_($band_rec);
    // p_($conn2);
    // $sql
}

function edge_get_vals($vals,$link,$filter,$place)
{
    
    if ((isset($vals))&&($place>0))
    {
    // echo "11";
        
        $vals=vals_out($vals);
        $index=make_index($vals);
        foreach ($index['GOOD'] as $i)
        {
            if (($vals[$i]['attributes']['TYPEID']=='band')&&(!isset($code[$vals[$i]['attributes']['CODE']]))&&($vals[$i]['attributes']['CODE']>0))
            {
                $code[$vals[$i]['attributes']['CODE']]=1;
                $sql='SELECT * FROM BAND WHERE CODE='.$vals[$i]['attributes']['CODE'];
                $band=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                $band_pr[$band['BAND_ID']]=array('id'=>$band['BAND_ID'],'w'=>$band['W'],'name'=>$band['NAME'],'code'=>$band['CODE']);
                
            } 
        }
        // p_($band_pr);exit;
        foreach ($index['GOOD'] as $q)
        {
            if (($vals[$q]['attributes']['TYPEID']=='sheet')&&(!isset($code[$vals[$q]['attributes']['CODE']]))&&($vals[$q]['attributes']['CODE']>0)&&(is_numeric($vals[$q]['attributes']['CODE'])))
            {
                $sql='SELECT * FROM MATERIAL WHERE CODE='.$vals[$q]['attributes']['CODE'];
                // echo $sql;
                $mat=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                if (($mat['MATERIAL_ID']>0)&&(is_numeric($mat['MATERIAL_ID'])))
                {
                    $sql="SELECT * FROM MATERIAL_BAND_CONNECTION WHERE MATERIAL_ID=".$mat['MATERIAL_ID'];
                    $connn=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                    if (count($connn)>0)
                    {
                        foreach ($connn as $conn)
                        {
                            $conn2[]=$conn;
                        }
                        // p_($conn2);exit; 
                        foreach ($conn2 as $v)
                        {
                            $sql='SELECT * FROM BAND_PRICE WHERE BAND_ID='.$v['BAND_ID']." AND PLACE_ID=".$place;
                            // echo $sql;
                            $band2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                            $sql='SELECT * FROM BAND_PIC WHERE BAND_ID='.$v['BAND_ID'];
                            $connn=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                            unset($pic);
                            foreach ($connn as $conn3)
                            {
                                $pic[]=$conn3;
                            }
                            $pic=array_unique($pic);
                            // !!!!!!!

                            if (isset($band_pr[$v['BAND_ID']]))
                            {
                                $band_rec[$v['BAND_ID']]=$band_pr[$v['BAND_ID']];
                                $band_rec[$v['BAND_ID']]['in']=1;
                                $band_rec[$v['BAND_ID']]['price']=$band2['PRICE'];
                                if (isset($pic)) $band_rec[$v['BAND_ID']]['pic']=$pic;
                                // unset ($band_pr[$v['BAND_ID']]);
                            }
                            else
                            {
                                $sql='SELECT * FROM BAND WHERE BAND_ID='.$v['BAND_ID'];
                                $band=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                                if ($band['W']<($mat['T']+$filter))
                                {
                                    $band_rec[$band['BAND_ID']]=array('id'=>$band['BAND_ID'],'w'=>$band['W'],'name'=>$band['NAME'],'code'=>$band['CODE'],'in'=>0);
                                    $band_rec[$v['BAND_ID']]['price']=$band2['PRICE'];
                                    if (isset($pic)) $band_rec[$v['BAND_ID']]['pic']=$pic;
                                }
                            }
                            
                        }
                        unset ($conn2);
                        if (isset($band_rec))
                        {
                            unset($pic);
                            $sheet_band[$vals[$q]['attributes']['CODE']]=$band_rec;
                            $sql='SELECT * FROM MATERIAL_PRICE WHERE MATERIAL_ID='.$mat['MATERIAL_ID']." AND PLACE_ID=".$place;
                            $mat2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                            $sheet_band[$vals[$q]['attributes']['CODE']]['name']=$vals[$q]['attributes']['NAME'];
                            $sheet_band[$vals[$q]['attributes']['CODE']]['price']=$mat2['PRICE'];
                            $sheet_band[$vals[$q]['attributes']['CODE']]['l']=$mat['L'];
                            $sheet_band[$vals[$q]['attributes']['CODE']]['w']=$mat['W'];
                            $sql='SELECT * FROM MATERIAL_PIC WHERE MATERIAL_ID='.$mat['MATERIAL_ID'];
                            $mat3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
                            
                            foreach ($mat3 as $conn)
                            {
                                $pic[]=$conn;
                            }
                            $pic=array_unique($pic);
                            if (isset($pic)) $sheet_band[$vals[$q]['attributes']['CODE']]['pic']=$pic;
                            
                        }
                        unset($band_rec);
                        $code[$vals[$q]['attributes']['CODE']]=1;
                    }
                }
            }
        }
        
        foreach ($sheet_band as $k=>$v)
        {
            foreach ($v as $k1=>$v1)
            {
                if (isset($band_pr[$k1]))
                {
                    unset ($band_pr[$k1]);
                }
            }
        }
        
        ksort($sheet_band);
        ksort($band_pr);
        unset($code);

        foreach ($index['GOOD'] as $i)
        {
            if (($vals[$i]['attributes']['TYPEID']=="sheet")&&(!isset($code[$vals[$i]['attributes']['CODE']]))&&($vals[$i]['attributes']['CODE']>0)
            &&(is_numeric($vals[$i]['attributes']['CODE'])))
            {
                $code[$vals[$i]['attributes']['CODE']]=1;
                $sql='SELECT * FROM MATERIAL WHERE CODE='.$vals[$i]['attributes']['CODE'];
                $material=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                $material_pr[$material['MATERIAL_ID']]=array('id'=>$material['MATERIAL_ID'],'w'=>$material['W'],'l'=>$material['L'],'name'=>$material['NAME'],'code'=>$material['CODE']);
            } 
        }
        
        unset ($conn2,$conn);
        foreach ($band_pr as $k=>$v)
        {
            if (($k>0)&&(is_numeric($k)))
            {
                $sql="SELECT * FROM MATERIAL_BAND_CONNECTION WHERE BAND_ID=".$k;
                // echo $sql;
                $mat_con=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

                foreach ($mat_con as $conn)
                {
                    $conn2[$conn['BAND_ID']]=$conn;
                }
                // p_($conn2);
                
                foreach ($conn2 as $v1)
                {
                    unset($pic);
                    $sql='SELECT * FROM MATERIAL_PRICE WHERE MATERIAL_ID='.$v1['MATERIAL_ID']." AND PLACE_ID=".$place;
                    $mat2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                    $sql='SELECT * FROM MATERIAL_PIC WHERE MATERIAL_ID='.$v1['MATERIAL_ID'];
                    $mat_pic=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

                    foreach ($mat_pic as $conn4)
                    {
                        $pic[]=$conn4;
                    }
                    $pic=array_unique($pic);
                    
                    

                    if (isset($material_pr[$v['id']]))
                    {
                        $material_rec[$v1['MATERIAL_ID']]=$material_pr[$v1['MATERIAL_ID']];
                        $material_rec[$v1['MATERIAL_ID']]['in']=1;
                        $material_rec[$v1['MATERIAL_ID']]['price']=$mat2['PRICE'];
                        if (isset($pic)) $material_rec[$v1['MATERIAL_ID']]['pic']=$pic;
                    }
                    else
                    {
                        $sql='SELECT * FROM MATERIAL WHERE MATERIAL_ID='.$v1['MATERIAL_ID'];
                        $material=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                        if ($v['w']>=($material['T']+3))
                        {
                            $material_rec[$material['MATERIAL_ID']]=array('id'=>$material['MATERIAL_ID'],'w'=>$material['W'],'l'=>$material['L'],'name'=>$material['NAME'],'code'=>$material['CODE'],'in'=>0);
                            $material_rec[$v1['MATERIAL_ID']]['price']=$mat2['PRICE'];
                            if (isset($pic)) $material_rec[$v1['MATERIAL_ID']]['pic']=$pic;
                        }
                    }
                    
                }
                unset ($conn2);
                if (isset($material_rec))
                {
                    $band_sheet[$k]=$material_rec;
                    $sql='SELECT * FROM BAND_PRICE WHERE BAND_ID='.$k." AND PLACE_ID=".$place;
                    
                    $band2=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];

                    $band_sheet[$v['id']]['name']=$v['name'];
                    $band_sheet[$v['id']]['price']=$band2['PRICE'];
                    $sql='SELECT * FROM BAND_PIC WHERE BAND_ID='.$k;
                    $band3=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

                    unset($pic);
                    foreach ($band3 as $conn)
                    {
                        $pic[]=$conn;
                    }
                    $pic=array_unique($pic);
                    if (isset($pic)) $band_sheet[$v['id']]['pic']=$pic;
                    unset($pic);
                }
                unset($material_rec);
            }
        }
        ksort($band_sheet);
        $res['sheet']=$sheet_band;
        $res['band']=$band_sheet;
        return $res;
    }
}

function change_edge_from_mass ($vals,$arr,$session,$link)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    // p_($arr);
    foreach ($index["PART"] as $i)
    {
        if ($vals[$i]['attributes']['CL']>0)
        {
            if (isset($vals[$i]['attributes']['ELL'])OR isset($vals[$i]['attributes']['ELR'])OR isset($vals[$i]['attributes']['ELB'])OR isset($vals[$i]['attributes']['ELT']))
            {
                $e5=part_get_edges($vals,$vals[$i]['attributes']['ID']);
                foreach (SIDE as $s)
                {
                    $s=mb_strtoupper($s);
                    if (isset ($vals[$i]['attributes']['EL'.$s]))
                    {
                        if (isset($arr[$e5[$s]['attributes']['ID']]['complete']))
                        {
                            $d=$e5[$s]['attributes']['W']-$vals[$i]['attributes']['T'];
                                // test ('d',$d);

                            unset($nes_change);
                            if ($d<3)
                            {
                                $nes_change=1;
                                $d=300;
                                // exit;
                            } 
                            $band_id_st=$e5[$s]['attributes']['BAND_ID'];
                            $band_id=$e5[$s]['attributes']['BAND_ID'];
                                // test ('bid',$band_id);
                            // echo ($band_id);

                            foreach ($arr[$e5[$s]['attributes']['ID']]['complete'] as $k_b_id=>$b_id)
                            {
                                $d1=$arr[$e5[$s]['attributes']['ID']]['decor'][$k_b_id]['W']-$vals[$i]['attributes']['T'];
                                // test ('name',$arr[$e5[$s]['attributes']['ID']]['decor'][$k_b_id]['NAME']);
                                // test ('d1',$d1);

                                if ((($d1<$d)&&($d1>=3)))
                                {
                                    // exit;
                                    $d=$d1;
                                    $band_id=$arr[$e5[$s]['attributes']['ID']]['decor'][$k_b_id]['BAND_ID'];
                                    // test ('d',$d);

                                }
                                // test ('d after',$d);

                                // echo $d1."<hr>";
                            }
                            // test ('place',$place);
                            // test ('ch1',$nes_change);

                            if ($band_id_st<>$band_id)
                            {
                            // test ('ch',$nes_change);
                                $e4=part_get_material($vals, $vals[$i]["attributes"]["ID"]);
                                // p_($e4);
                                if ($nes_change>0) 
                                {
                                    $change_part[$band_id][$vals[$i]['attributes']['ID']][$s]=1;
                                    $_SESSION['error_ok'].="<br>Деталь '".$vals[$i]["attributes"]["NAME"]."' (id='".$vals[$i]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (CL ".$vals[$i]["attributes"]["CL"]." * CW ".$vals[$i]["attributes"]["CW"].")
                                    - кромкование ".SIDE_T[mb_strtolower($s)]." заменено с кромки (".$e5[$s]['attributes']['NAME'].") на кромку (".$arr[$e5[$s]['attributes']['ID']]['decor'][$band_id]['NAME']."), так как выбранная кромка не подходит для данного материала. 
                                    <hr>";
                                }
                                else
                                {
                                    foreach ($arr[$e5[$s]['attributes']['ID']]['decor'][$k_b_id]['stock'] as $pl=>$stock)
                                    {
                                        $all_stock+=$stock;
                                        // p_($stock);
                                    }
                                    if ($all_stock>0)
                                    {
                                        $_SESSION['error_ok'].="<br>Деталь '".$vals[$i]["attributes"]["NAME"]."' (id='".$vals[$i]["attributes"]["ID"]."') (материал '".$e4['attributes']['NAME']."') (CL ".$vals[$i]["attributes"]["CL"]." * CW ".$vals[$i]["attributes"]["CW"].")
                                        - кромкование ".SIDE_T[mb_strtolower($s)]." может быть заменено с кромки (".$e5[$s]['attributes']['NAME'].") на кромку (".$arr[$e5[$s]['attributes']['ID']]['decor'][$band_id]['NAME'].") для оптимального использования материала.
                                        Остаток материала ".$stock." м по Компании.
                                        <hr>";
                                    }
                                }
                            }
                            
                            // p_($s);
                        }
                        
                    }
                }
            }
        }
    }
    foreach ($index['GOOD'] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=="band")&&(isset($change_part[$vals[$i]['attributes']['BAND_ID']])))
        {
            foreach($index['OPERATION'] as $j)
            {
                
                if (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                {
                    // exit;
                    $band_in[$vals[$i]['attributes']['BAND_ID']]=$vals[$j]['attributes']['ID'];
                }
            }
            
        }
    }
    // p_($band_in);
    foreach ($change_part as $key=>$v)
    {
        if (!isset($band_in[$key])) $band_insert['band_in'][]=$key;
    }
    if (count($band_insert)>0) $vals=put_source_in($vals,$link,$band_insert,$session['user_place']);
    $index=make_index($vals);
    foreach ($index['GOOD'] as $i)
    {
        if (($vals[$i]['attributes']['TYPEID']=="band")&&(isset($change_part[$vals[$i]['attributes']['BAND_ID']])))
        {
            // exit;
            foreach($index['OPERATION'] as $j)
            {
                
                if (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$j+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                {
                    // exit;
                    $band_in[$vals[$i]['attributes']['BAND_ID']]=$vals[$j]['attributes']['ID'];
                }
            }
            
        }
    }
    // p_($change_part);

    foreach ($change_part as $band_id => $part)
    {
        foreach ($part as $part_id=>$part_band)
        {
            foreach ($index["PART"] as $i)
            {
                if (($vals[$i]['attributes']["CL"]>0)&&($vals[$i]['attributes']["ID"]==$part_id))
                {
                    foreach (SIDE as $s)
                    {
                        $s=mb_strtoupper($s);
                        if (isset($part_band[$s]))
                        {
                            // exit;
                            $op_el=substr($vals[$i]['attributes']['EL'.$s],strpos($vals[$i]['attributes']['EL'.$s],"#")+1);
                            foreach($index['OPERATION'] as $j)
                            {
                                
                                if (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$j]['attributes']['ID']==$op_el))
                                {
                                    // exit;
                                    $k=$j+2;
                                    while ($vals[$k]['tag']=="PART")
                                    {
                                        if ($vals[$k]['attributes']['ID']==$vals[$i]['attributes']['ID']) unset ($vals[$k]);
                                        $k++;
                                    }
                                }
                                elseif (($vals[$j]['attributes']['TYPEID']=="EL")&&($vals[$j]['attributes']['ID']==$band_in[$band_id]))
                                {
                                    unset($in);
                                    $k=$j+2;
                                    while ($vals[$k]['tag']=="PART")
                                    {
                                        if ($vals[$k]['attributes']['ID']==$vals[$i]['attributes']['ID']) $in=1;
                                        $k++;
                                    }
                                    if (!isset($in))
                                    {
                                        $vals=move_vals($vals,$j+2,1);
                                        $vals[$j+2]=Array
                                        (
                                            "tag" => "PART",
                                            "type" => "complete",
                                            "level" => 2,
                                            "attributes"=> Array ("ID"=>$vals[$i]['attributes']['ID'])
                                        );
                                    }
                                }
                            }
                            // test('op',$op_el);
                            $vals[$i]['attributes']['EL'.$s]="@operation#".$band_in[$band_id];

                        }
                    }
                }
            }
        }
    }
    ksort($vals);
    $vals = vals_out($vals);

    return $vals;
}

function change_bores($link, $v1, $diameter_tools_from_DB, $part, $e4)
{
    
    $i1=make_index($v1);
    // p_($diameter_tools_from_DB);
    $bores=array('f',"l","r","t","b");
    $bores_text=array('фронтальное',"левое","правое","верхнее","нижнее");

    // Здесь собирается массив сверлений по сторонам (стороны - ключи массива, названия инструмента - ключи подмассивов)
    foreach ($bores as $b)
    {
        foreach ($i1['B'.mb_strtoupper($b)] as $t)
        {
            $bore[$b][$v1[$t]['attributes']['NAME']][]=$t; 
        }
    }

    foreach ($i1['TOOL'] as $t)
    {
        if (substr_count($v1[$t]['attributes']['NAME'],"Bore")>0)
        {
            $tool_name=$v1[$t]['attributes']['NAME'];
            $tool_d=$v1[$t]['attributes']['D'];
            foreach ($bores as $key_bore=>$b)
            {
                if ((isset($bore[$b][$tool_name]))&&(!isset($diameter_tools_from_DB[$b][$tool_d])))
                {
                    unset($true);
                    $ar=array_values($diameter_tools_from_DB[$b]);
                    sort ($ar);
                    foreach ($ar as $b_temp_key=>$b_temp)
                    {
                        if (($tool_d<=$b_temp)&&(!isset($b_exit)))
                        {
                            $b_per_b=$ar[$b_temp_key]-$tool_d; // На сколько текущий диаметр из БД больше текущего из проекта
                            $b_per_m=$tool_d-$ar[$b_temp_key-1]; // На сколько текущий диаметр из проекта больше предыдущего  из БД
                            if (($b_per_b>$b_per_m)OR($b_per_b==$b_per_m)) $b_temp=$ar[$b_temp_key-1];
                            if (abs($b_temp-$tool_d)<=1 )
                            {    
                                if ($tool_d<>$b_temp)
                                {
                                    $error_ok.="<br>Деталь '".$part["NAME"]."' (id='".$part["ID"]."') (материал '".$e4['attributes']['NAME']."') (L ".$part["L"]." * W ".$part["W"].")
                                    - инструмент сверления (".$bores_text[$key_bore].") '".$tool_name."' диаметром ".$tool_d." мм заменён на '"."Bore".$b_temp."' диаметром ".$b_temp." мм.
                                    <hr>";
                                }
                                $tool_d_new=$b_temp;
                                $b_exit=1;
                            }
                        }
                    }
                    unset($b_exit);
                    if (isset($tool_d_new))
                    {
                        $bore_tool[]=$tool_d_new;
                        foreach ($bore[$b][$tool_name] as $v_num)
                        {
                            $v1[$v_num]['attributes']['NAME']='Bore'.$tool_d_new;
                        
                        }
                        if ($tool_name<>'Bore'.$tool_d_new)
                        {
                            $bore[$b]['Bore'.$tool_d_new]=$bore[$b][$tool_name];
                            unset($bore[$b][$tool_name]);
                        }
                    }
                    unset($tool_d_new);
                }
                
            }
        }
    }

    
    if (isset($bore_tool))
    {
        foreach ($bore_tool as $b)
        {
            $v1=move_vals($v1,1,1);
            $v1[1]=array('tag'=>'TOOL','type'=>'complete','level'=>2,'attributes'=>array('D'=>$b,'NAME'=>'Bore'.$b));
            ksort($v1);
            $i1=make_index($v1);
        }
        unset($name_bore,$d_bore);
        foreach($i1['TOOL'] as $i)
        {
            if ((!in_array($v1[$i]['attributes']['NAME'],$name_bore))
                &&(!in_array($v1[$i]['attributes']['D'],$d_bore)))
                
            {
                $name_bore[]=$v1[$i]['attributes']['NAME'];
                $d_bore[]=$v1[$i]['attributes']['D'];
            }
            else
            {
                unset($v1[$i]);
            }
        }
        ksort($v1);
        unset($bore);
        foreach ($bores as $b)
        {
            foreach ($i1['B'.mb_strtoupper($b)] as $t)
            {
                $bore[$v1[$t]['attributes']['NAME']][]=$t; 
            }
        }
        foreach ($i1['TOOL'] as $t)
        {
            if ((!isset($bore[$v1[$t]['attributes']['NAME']]))&&(substr_count($v1[$t]['attributes']['NAME'],'Bore')>0)) unset ($v1[$t]);
        }
        ksort($v1);
        
    }
    $res=array('v1'=>$v1,'error_ok'=>$error_ok);
    return $res;
}
function cut_par($t,$ar)
{
    // echo $t;
    if (substr_count($t,'-')>0)
    {
        $t=explode('-',$t);
        $t[0]=str_replace('(','',$t[0]);
        $t[0]=str_replace(')','',$t[0]);

        if ($t[0]=='dx') $t[0]=$ar['DX'];
        if ($t[0]=='dy') $t[0]=$ar['DY'];
        foreach ($t as $t2=>$t1)
        {
            if (substr_count($t1,'+')>0) 
            {
                $t[$t2]=explode('+',$t1);
                foreach ($t[$t2] as $t3=>$t4)
                {
                    $t[$t2][$t3]=str_replace('(','',$t4);
                    $t[$t2][$t3]=str_replace(')','',$t4);
                }
            }
            else
            {
                $t[$t2]=str_replace('(','',$t[$t2]);
                $t[$t2]=str_replace(')','',$t[$t2]);
            }
        }
        // p_($t);
        unset ($r);
        foreach ($t as $k=>$v)
        {
            if ($k>0)
            {
                if (!is_array($v))$r-=$v;
                else
                {
                    foreach ($v as $f=>$e)
                    {
                        if ($f==0) $r-=$e;
                        else $r+=$e;
                    }
                }
            }
        }
        $t=$t[0]+$r;
    }
    elseif  (substr_count($t,'+')>0)
    {
        $t=explode('+',$t);
        $t[0]=str_replace('(','',$t[0]);
        $t[0]=str_replace(')','',$t[0]);

        if ($t[0]=='dx') $t[0]=$ar['DX'];
        if ($t[0]=='dy') $t[0]=$ar['DY'];
        foreach ($t as $t2=>$t1)
        {
            $t[$t2]=str_replace('(','',$t[$t2]);
            $t[$t2]=str_replace(')','',$t[$t2]);
            if ($t2>0) $rt+=$t1;
            
        }
        $t=$rt;

    }
    elseif  (substr_count($t,'/')>0)
    {
        $t=explode('/',$t);
        $t[0]=str_replace('(','',$t[0]);
        $t[0]=str_replace(')','',$t[0]);

        if ($t[0]=='dx') $t[0]=$ar['DX'];
        if ($t[0]=='dy') $t[0]=$ar['DY'];
        $rt=$t[0];
        foreach ($t as $t2=>$t1)
        {
            $t[$t2]=str_replace('(','',$t[$t2]);
            $t[$t2]=str_replace(')','',$t[$t2]);
            if ($t2>0) $rt=$rt/$t[$t2];
            
        }
        $t=$rt;

    }
    return $t;

}

function mr_change ($v1,$ar)
{

    $i1=make_index($v1);
    if ((isset($i1['MR']))&&(count($i1['MR'])==1)&&($v1[$i1['MR'][0]]['attributes']['A']==0)
    &&((substr_count($ar['attributes']['operation.my_template'],'37')>0)||
    (substr_count($ar['attributes'][mb_strtoupper('operation.my_template')],'37')>0)))
    {
        // p_($ar);
        $vals[$i]['attributes']['MIRHOR']='false';
        $vals[$i]['attributes']['MIRVERT']='true';
        unset ($ii);
        foreach ($i1['CALL'] as $ca)
        {
            if ($v1[$ca]['type']=='open')
            {
                $ii++;
                do
                {
                    $ca++;
                    if ((isset($v1[$ca]['attributes']['NAME']))&&(($v1[$ca]['tag']=='PAR')))
                    {
                        if (isset($v1[$ca]['attributes']['VALUE'])) $n[$ii][strtolower($v1[$ca]['attributes']['NAME'])]=$v1[$ca]['attributes']['VALUE'];
                        // elseif ((isset($v1[$e]['attributes']['EXPR']))&&(!isset($name[strtolower($v1[$e]['attributes']['NAME'])]))) $name[strtolower($v1[$e]['attributes']['NAME'])]=$v1[$e]['attributes']['EXPR'];
                    }

                } while ($v1[$ca]['tag']<>'CALL');    
            }
        }
        // p_($n);exit;
        $ty=$v1[$i1['MR'][0]];
        foreach ($n as $name)
        {
            foreach ($name as $k=>$v)
            {
                if (!is_numeric($v))
                {
                    $name[$k]=cut_par($v,$v1[0]['attributes']);
                }
            }
            $name['frez']=$ty['attributes']['NAME'];
            $name['dp']=$name['hfr'];
            foreach ($i1['TOOL'] as $r)
            {
                if ($v1[$r]['attributes']['NAME']==$name['frez']) $d=$v1[$r]['attributes']['D'];
            }
            unset($v1[$i1['MR'][0]]);
            $dx=$v1[0]['attributes']['DX'];
            $dy=$v1[0]['attributes']['DY'];


            $x0='(dx-'.($dx-$name['px']).')';
            $y0='(dy-'.($dy-$name['py']).')';
            $x1='('.$x0.'-'.($name['l']/2).')';
            $y1='('.$y0.'-'.($name['n']/2).')';
            $y2='('.$y0.'+'.($name['n']/2).')';
            $x2='('.$x0.'+'.($name['l']/2).')';
            $ii=$i1['MR'][0]+2;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'MS','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x0,'Y'=>$y0,'DP'=>$name['dp'],'IN'=>1,'OUT'=>1,'SXY'=>'tool.dia/2','C'=>2,'NAME'=>$name['frez']));
            $ii++;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x0,'Y'=>$y1,'DP'=>$name['dp']));
            $ii++;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x1,'Y'=>$y1,'DP'=>$name['dp']));
            $ii++;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x1,'Y'=>$y2,'DP'=>$name['dp']));
            $ii++;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x2,'Y'=>$y2,'DP'=>$name['dp']));
            $ii++;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x2,'Y'=>$y1,'DP'=>$name['dp']));
            $ii++;
            $v1=move_vals($v1,$ii,1);
            $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x1,'Y'=>$y1,'DP'=>$name['dp']));
            unset ($r);
            $r['y2']=cut_par($y2,$v1[0]['attributes']);
            $r['y1']=cut_par($y1,$v1[0]['attributes']);
            $r['x1']=cut_par($x1,$v1[0]['attributes']);
            $r['x2']=cut_par($x2,$v1[0]['attributes']);
            $r['x0']=cut_par($x0,$v1[0]['attributes']);
            $r['y0']=cut_par($y0,$v1[0]['attributes']);
            $r2['y2']=$r['y2'];
            $r2['y1']=$r['y1'];
            $r2['x1']= $r['x1'];
            $r2['x2']=$r['x2'];
            unset($uu);
            $check_while = array();
            do
            {
                $y2.='-'.$d/2;
                $r['y2']=cut_par($y2,$v1[0]['attributes']);
                $r['y1']=cut_par($y1,$v1[0]['attributes']);
                $r['x1']=cut_par($x1,$v1[0]['attributes']);
                $r['x2']=cut_par($x2,$v1[0]['attributes']);
                $r['x0']=cut_par($x0,$v1[0]['attributes']);
                $r['y0']=cut_par($y0,$v1[0]['attributes']);
                
                if ((($r['x1']<$r['x0']-$name['dp']/4)&&($r['y2']>$r['y0']+$name['dp']/4))&&(!isset($uu)))
                {
                    $r2['y2']=$r['y2'];
                    if ($r['y2']-$r2['y1']>$d)
                    {
                        $ii++;
                        $v1=move_vals($v1,$ii,1);
                        $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x1,'Y'=>$y2,'DP'=>$name['dp']));
                    }
                    else
                    {
                        $uu=1;
                    }
                    
                }
                $x2.='-'.$d/2;
                $r['x2']=cut_par($x2,$v1[0]['attributes']);
                if ((($r['x2']>$r['x0']+$name['dp']/4)&&($r['y2']>$r['y0']+$name['dp']/4))&&(!isset($uu)))
                {
                    $r2['x2']=$r['x2'];
                    if ($r['x2']-$r2['x1']>$d)
                    {
                        $ii++;
                        $v1=move_vals($v1,$ii,1);
                        $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x2,'Y'=>$y2,'DP'=>$name['dp']));
                    }
                    else
                    {
                        $uu=1;
                    }
                }
                $y1.='+'.$d/2;
                $r['y1']=cut_par($y1,$v1[0]['attributes']);
                if ((($r['x2']>$r['x0']+$name['dp']/4)&&($r['y1']<$r['y0']-$name['dp']/4))&&(!isset($uu)))
                {
                    $r2['y1']=$r['y1'];
                    if ($r2['y2']-$r['y1']>$d)
                    {
                        $ii++;
                        $v1=move_vals($v1,$ii,1);
                        $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x2,'Y'=>$y1,'DP'=>$name['dp']));
                    }
                    else
                    {
                        $uu=1;
                    }
                }
                $x1.='+'.$d/2;
                $r['x1']=cut_par($x1,$v1[0]['attributes']);
                if ((($r['x1']<$r['x0']-$name['dp']/4)&&($r['y1']<$r['y0']-$name['dp']/4))&&(!isset($uu)))
                {
                    $r2['x1']= $r['x1'];
                    if ($r2['x2']-$r['x1']>$d)
                    {
                        $ii++;
                        $v1=move_vals($v1,$ii,1);
                        $v1[$ii]=array('tag'=>'ML','type'=>'complete','level'=>2,'attributes'=>array('X'=>$x1,'Y'=>$y1,'DP'=>$name['dp']));
                    }
                    else
                    {
                        $uu=1;
                    }
                }
                
                // В единичном случае при счёте проекта зависала система, потому что не выполнялось условие while
                // Сделал хук, если проверяемые данные одинаковые три раза - выходим из цикла.
                // Но если система и в этом случае не выходит из цикла (а такое было), то по достижении пяти повторений
                // принудительно обрываем цикл.
                if (!empty($check_while) && $check_while['x0'] == $r['x0'] &&
                        $check_while['x1'] == $r['x1'] &&
                        $check_while['x2'] == $r['x2'] &&
                        $check_while['y0'] == $r['y0'] &&
                        $check_while['y1'] == $r['y1'] &&
                        $check_while['y2'] == $r['y2']) {
                    //.. 08.07.2021 Если 5 раз подряд ничего не меняется,
                    // то ниже принудительно выходим из цикла
                    $check_while['count']++;
                }
                elseif (!empty($check_while) && $check_while['x0'] != $r['x0'] &&
                        $check_while['x1'] != $r['x1'] &&
                        $check_while['x2'] != $r['x2'] &&
                        $check_while['y0'] != $r['y0'] &&
                        $check_while['y1'] != $r['y1'] &&
                        $check_while['y2'] != $r['y2']) {
                    $check_while['x0'] = $r['x0'];
                    $check_while['x1'] = $r['x1'];
                    $check_while['x2'] = $r['x2'];
                    $check_while['y0'] = $r['y0'];
                    $check_while['y1'] = $r['y1'];
                    $check_while['y2'] = $r['y2'];
                    //.. 08.07.2021 Здесь сбрасываем счёт
                    if (!$check_while['count'] == 0) {
                        $check_while['count'] = 0;
                    }
                }
                elseif (empty($check_while)) {
                    $check_while['x0'] = $r['x0'];
                    $check_while['x1'] = $r['x1'];
                    $check_while['x2'] = $r['x2'];
                    $check_while['y0'] = $r['y0'];
                    $check_while['y1'] = $r['y1'];
                    $check_while['y2'] = $r['y2'];
                    $check_while['count'] = 0;
                }

                if ($check_while['count'] >= 5) {
                    break;
                }
                
            } while ((($r['y2']>$r['y0']+$name['dp']/4)AND($r['y1']<$r['y0']-$name['dp']/4)AND($r['x1']<$r['x0']-$name['dp']/4)AND($r['x2']>$r['x0']+$name['dp']/4)));
    
            // p_('<br>Убрана выемка в операции: '.$ar['attributes']['operation.my_template'].'<br>');
        }
    }
    else return false;
    ksort($v1);
    // p_($v1);
    return $v1;
}

function check_double ($vals)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    
    ksort($vals);
    $sql="SELECT * FROM DOUBLE_TYPE";
    $type=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($type as $t)
    {
        $sql="SELECT * FROM DOUBLE_CALC WHERE DOUBLE_ID=".$t['DOUBLE_TYPE_ID'];
        $calc[$t['DOUBLE_TYPE_ID']]=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['count'];
    }
    foreach ($index["PART"] as $i)
    {
       
        if (($vals[$i]["attributes"]["CL"]>0)&&($vals[$i]["attributes"]["MY_DOUBLE"]==1))
        {
            if ($vals[$i]["attributes"]["MY_DOUBLE_RES"]==1)
            {
                
                unset ($cc,$ccc,$c5);
                foreach ($index["PART"] as $j)
                {
                   
                    if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                    ($vals[$j]["attributes"]["MY_DOUBLE_FACE"]==0)) $cc++;
                }
                if ($cc<>$calc[$vals[$i]["attributes"]["MY_DOUBLE_TYPE"]]) $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                else
                {
                    foreach ($index["PART"] as $j)
                    {
                    
                        if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                        ($vals[$j]["attributes"]["MY_DOUBLE_FACE"]=='1')) $ccc++;
                        // p_($ccc);
                    }
                }
                if (!isset($ccc)) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '1!!';
                        p_($del_double);exit;
                    }
                }
                else
                {
                    

                    foreach ($index["GOOD"] as $j)
                    {
                    
                        if (($vals[$j]["attributes"]["TYPEID"]=='sheet')&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])) $c5++;
                    }
                }
                if (!isset($c5)) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '2';
                        p_($del_double);exit;
                    }
                }
            }
            elseif ($vals[$i]["attributes"]["MY_DOUBLE_FACE"]==1)
            {
                
                unset ($cc,$ccc);
                foreach ($index["PART"] as $j)
                {
                   
                    if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                    ($vals[$j]["attributes"]["MY_DOUBLE_FACE"]==0)) $cc++;
                }
                if ($cc<>$calc[$vals[$i]["attributes"]["MY_DOUBLE_TYPE"]]) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '3';
                        p_($del_double);exit;
                    }
                }
                else
                {
                    foreach ($index["PART"] as $j)
                    {
                    
                        if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                        ($vals[$j]["attributes"]["MY_DOUBLE_RES"]=='1')) $ccc++;
                    }
                }
                
                if (!isset($ccc)) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '4';
                        p_($del_double);exit;
                    }
                }
            }
            elseif (($vals[$i]["attributes"]["MY_DOUBLE_FACE"]==0)&&(!isset($vals[$i]["attributes"]["MY_DOUBLE_RES"])))
            {
                
                unset ($cc,$ccc,$ccc);
                foreach ($index["PART"] as $j)
                {
                   
                    if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                    ($vals[$j]["attributes"]["MY_DOUBLE_FACE"]==0)) $cc++;
                }
                // p_($calc[$vals[$i]["attributes"]["MY_DOUBLE_TYPE"]]);
                // p_($cc);
                if ($cc<>$calc[$vals[$i]["attributes"]["MY_DOUBLE_TYPE"]]) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '5';
                        p_($del_double);exit;
                    }
                }
                else
                {
                    // p_($vals[$i]);
                    // exit;
                    foreach ($index["PART"] as $j)
                    {
                    
                        if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                        ($vals[$j]["attributes"]["MY_DOUBLE_RES"]=='1')) $ccc++;
                        // p_($ccc);
                    }
                }
                
                if (!isset($ccc)) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '6';
                        p_($del_double);exit;
                    }
                }
                else
                {
                    foreach ($index["PART"] as $j)
                    {
                    
                        if (($vals[$j]["attributes"]["CL"]>0)&&($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$vals[$i]["attributes"]["MY_DOUBLE_ID"])&&
                        ($vals[$j]["attributes"]["MY_DOUBLE_FACE"]=='1')) $cccc++;
                        // p_($ccc);
                    }
                }
                if (!isset($cccc)) 
                {
                    $del_double[$vals[$i]["attributes"]["MY_DOUBLE_ID"]]=1;
                    if ($_SESSION["d_double"]==1)
                    {
                        echo '7';
                        p_($del_double);exit;
                    }
                }
            }

        }
    }
    // xml ($vals);
    
    if (count($del_double)>0)
    {
    if ($_SESSION["d_double"]==1) echo "1";
        
        foreach ($del_double as $k=>$v)
        {
            foreach ($index["PART"] as $j)
            {
            
                if (($vals[$j]["attributes"]["CL"]>0)&&(($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$k)||($vals[$j]["attributes"]["part.my_double_id"]==$k)))
                {
                    // p_($vals[$j]);
                    
                    $vals=del_part($vals,$vals[$j]["attributes"]["ID"]);
                }
            }
            $index=make_index($vals);
            unset($f_del);
            foreach ($index["GOOD"] as $j)
            {
            
                if ($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$k)
                {
                    
                    if ($vals[$j]['type']=='complete') unset ($vals[$j]);
                    elseif ($vals[$j]['type']=='open')
                    {
                        unset ($vals[$j]);
                        $d=$j+1;
                        while ($vals[$d]['tag']=='PART')
                        {
                            // $vals=del_part($vals,$vals[$d]["attributes"]["ID"]);
                            $f_del[]=$vals[$d]["attributes"]["ID"];
                            unset($vals[$d]);
                            $d++;
                        }
                        unset($vals[$d]);

                    }
                }
            }
            $index=make_index($vals);
            // p_($vals);

            foreach ($index["OPERATION"] as $j)
            {
            
                if ($vals[$j]["attributes"]["MY_DOUBLE_ID"]==$k)
                {
                    
                    if ($vals[$j]['type']=='complete') unset ($vals[$j]);
                    elseif ($vals[$j]['type']=='open')
                    {
                        unset ($vals[$j],$vals[$j+1]);
                        $d=$j+2;
                        while ($vals[$d]['tag']=='PART')
                        {
                            // $vals=del_part($vals,$vals[$d]["attributes"]["ID"]);
                            $f_del[]=$vals[$d]["attributes"]["ID"];

                            unset($vals[$d]);
                            $d++;
                        }
                        unset($vals[$d]);

                    }
                }
            }
        }
    }
    if ($_SESSION["d_double"]==1)exit;

    if (isset($del_double))
    {
        if (count($del_double)==1) 
        {
            foreach($del_double as $k=>$d) $_SESSION['error_ok'].='<br> Сшивка '.$k.' была удалена в режиме редактирования деталей частично. Впредь удаляйте сшивки в дополнительных операциях. Ваши действия могли привести к нарушению
            целостности проекта. Из проекта удалены все части сшивки. К сожалению могли быть затронуты другие детали и операции. Проверьте деталировку и впредь удаляйте сшивки в дополнительных операциях.<br><hr>';
    
        }
        elseif (count($del_double)>1)
        {
            unset ($k1);
            foreach($del_double as $k=>$d) $k1[]=$k;
            $_SESSION['error_ok'].='<br> Сшивки '.(implode(',',$k1)).' была удалена в режиме редактирования деталей частично. Впредь удаляйте сшивки в дополнительных операциях. Ваши действия могли привести к нарушению
            целостности проекта. Из проекта удалены все части сшивки. К сожалению могли быть затронуты другие детали и операции. Проверьте деталировку и впредь удаляйте сшивки в дополнительных операциях.<br><hr>';
    
        }
        // xml ($vals);99999999
    }
    foreach ($f_del as $id)
    {
        $vals= del_part($vals,$id);
    }
    // p_($vals);
    
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    return ($vals);
}

function edge_calc($vals) 
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    // p_($vals);


    foreach ($index['OPERATION'] as $i)
    {
        if ($vals[$i]['attributes']['TYPEID']=="EL")
        {
            unset ($count);
            unset ($count_el);
            foreach ($index['GOOD'] as $t)
            {
                if (($vals[$t]['attributes']['TYPEID']=='tool.edgeline')&&($vals[$t]['attributes']['ID']==$vals[$i]['attributes']['TOOL1']))
                {
                    $tool=$vals[$t]['attributes'];
                }
            }
            $j=$i+2;
            while ($vals[$j]['tag']=="PART")
            {
                foreach ($index['PART'] as $p)
                {
                    if (($vals[$p]['attributes']['CL']>0)&&($vals[$p]['attributes']['ID']==$vals[$j]['attributes']['ID']))
                    {
                        
                        
                        if ($vals[$p]['attributes']['ELL']=="@operation#".$vals[$i]['attributes']['ID'])
                        {
                            $count+=$vals[$p]['attributes']['CW']/1000*$vals[$p]['attributes']['COUNT']*(1+$tool['ELWASTEPRC'])+($tool['ELRESTSIDE']/1000*2);
                            $count_el+=$vals[$p]['attributes']['CW']/1000*$vals[$p]['attributes']['COUNT'];
                        }
                        if ($vals[$p]['attributes']['ELR']=="@operation#".$vals[$i]['attributes']['ID'])
                        {
                            $count_el+=$vals[$p]['attributes']['CW']/1000*$vals[$p]['attributes']['COUNT'];
                            $count+=$vals[$p]['attributes']['CW']/1000*$vals[$p]['attributes']['COUNT']*(1+$tool['ELWASTEPRC'])+($tool['ELRESTSIDE']/1000*2);
                        }
                        if ($vals[$p]['attributes']['ELT']=="@operation#".$vals[$i]['attributes']['ID'])
                        {
                            $count_el+=$vals[$p]['attributes']['CL']/1000*$vals[$p]['attributes']['COUNT'];
                            $count+=$vals[$p]['attributes']['CL']/1000*$vals[$p]['attributes']['COUNT']*(1+$tool['ELWASTEPRC'])+($tool['ELRESTSIDE']/1000*2);
                        }
                        if ($vals[$p]['attributes']['ELB']=="@operation#".$vals[$i]['attributes']['ID'])
                        {
                            $count_el+=$vals[$p]['attributes']['CL']/1000*$vals[$p]['attributes']['COUNT'];
                            $count+=$vals[$p]['attributes']['CL']/1000*$vals[$p]['attributes']['COUNT']*(1+$tool['ELWASTEPRC'])+($tool['ELRESTSIDE']/1000*2);
                        }
                    }
                }
                $j++;
            }
            $vals[$i+1]['attributes']['COUNT']=ceil($count);
            $vals[$i]['attributes']['ELLENGTH']=ceil($count_el);
        }
    }

    foreach ($index['GOOD'] as $i)
    {
        if ($vals[$i]['attributes']['TYPEID']=="band")
        {
            unset ($dfd);
            foreach ($index['OPERATION'] as $tr)
            {
                if (($vals[$tr]['attributes']['TYPEID']=='CL')&&($vals[$tr+1]['attributes']['ID']==$vals[$i]['attributes']['ID'])) $dfd=1;
            }
            if (!isset($dfd))
            {
                $band[$vals[$i]['attributes']['CODE']][]=$vals[$i];
                unset ($vals[$i]);
            }
            
        }
    }
    ksort ($vals);

    ksort ($band);
    $vals=vals_out ($vals);
    // p_ ($vals);exit;
    $index=make_index($vals);
    foreach ($index['GOOD'] as $g)
    {
        if ($vals[$g]['attributes']['TYPEID']=='sheet')
        {
            if (!isset($g1)) $g1=$g;
        }
    }

    // p_($band);exit;
    foreach ($band as $c)
    {
        foreach ($c as $e)
        {
            // p_($e);
            $vals=vals_into_end($vals,$e);
            ksort ($vals);
            // p_($vals);exit;
            // $vals[$g1]=$e;
        }
    }

    // xml ($vals);
// 789456
    // p_($vals);exit;
    return $vals;
}
function edge_mass($vals,$link) 
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    // Array
    // (
    // [band_in] => Array
    //     (
    //         [0] => 11092
    //     )
    // )
    // xml ($vals);
    foreach ($index["GOOD"] as $i)
    {
        if (($vals[$i]['attributes']["TYPEID"]=='band')&&($vals[$i]['attributes']["MY_1C_NOM"]<>6741)&&($vals[$i]['attributes']["MY_1C_NOM"]<>6149))
        {
            unset ($ok);  
            // p_($vals[$i]);

            foreach ($index['OPERATION'] as $r)
            {
                if (($vals[$r]['attributes']['TYPEID']=="EL")&&($vals[$r+1]['attributes']['ID']==$vals[$i]['attributes']['ID']))
                {
                    if ($vals[$r+2]['tag']=="PART")
                    {
                        $ok=1;
                    }
                }
            }
            if ($ok==1)
            {
                unset ($text);
                // p_($vals[$i]);    
                if ((!isset($vals[$i]['attributes']["BAND_ID"]))&&($vals[$i]['attributes']["CODE"]>0)&&(is_numeric($vals[$i]['attributes']["CODE"])))
                {
                    $sql='SELECT * FROM BAND WHERE CODE='.$vals[$i]['attributes']["CODE"].';';
                    $vals[$i]['attributes']["BAND_ID"]=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0]["BAND_ID"];
                }
                if ($vals[$i]['attributes']["BAND_ID"]>0)
                {
                    $sql='SELECT * FROM GOOD_PROPERTIES_BAND WHERE BAND_ID='.$vals[$i]['attributes']["BAND_ID"].' AND PROPERTY_ID=156;';
                    $ssq=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
                    if (isset($ssq['VALUESTR']))
                    {
                        $sql2='SELECT BAND_ID FROM GOOD_PROPERTIES_BAND WHERE BAND_ID<>'.$vals[$i]['attributes']["BAND_ID"].' AND PROPERTY_ID=156 AND VALUESTR="'.$ssq['VALUESTR'].'";';
                        $ssq222=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql2)['data'];
                        if (count($ssq222)>0) 
                        {                   
                            foreach ($ssq222 as $ssq2)
                            {  
                                $band_id[]=$ssq2['BAND_ID'];
                            }
                            $text=" AND (";
                            foreach ($band_id as $b)
                            {
                                $text.= ' BAND_ID='.$b.' OR';
                            }
                            $text=substr($text,0,strlen($text)-3);
                            $text.=') ';
                            // p_($text);exit;

                            unset($band_id);
                            $sql5='SELECT * FROM GOOD_PROPERTIES_BAND WHERE BAND_ID='.$vals[$i]['attributes']["BAND_ID"].' AND PROPERTY_ID=154;';
                            $ssq5=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql5)['data'][0];

                            $sql53='SELECT * FROM GOOD_PROPERTIES_BAND WHERE BAND_ID<>'.$vals[$i]['attributes']["BAND_ID"].' AND PROPERTY_ID=154 AND VALUESTR="'.$ssq5['VALUESTR'].'" '.$text;
                            // echo $sql53;
                            $ssq5535=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql53)['data'];
                            
                            foreach ($ssq5535 as $ssq53)
                            {  
                                $st='SELECT * FROM BAND WHERE BAND_ID='. $ssq53['BAND_ID'];
                                $ssst=sql_data(__LINE__,__FILE__,__FUNCTION__,$st)['data'][0];
                                $st='SELECT * FROM BAND_STOCK WHERE BAND_ID='. $ssq53['BAND_ID'];
                            
                                $ssst21=sql_data(__LINE__,__FILE__,__FUNCTION__,$st)['data'];
                                
                                foreach ($ssst21 as $ssst1)
                                {
                                    if ($ssst1['COUNT']>0)$stock[$ssst1['PLACE_ID']]=$ssst1['COUNT'];
                                }
                                $band_id[$ssq53['BAND_ID']]=$ssst;
                                $band_id[$ssq53['BAND_ID']]['stock']=$stock;
                                unset ($stock);
                            }
                            // p_($band_id);
                            if (count($band_id)>0) $res[$vals[$i]['attributes']["ID"]]['decor']=$band_id;
                            unset($band_id);
                            if (count($res[$vals[$i]['attributes']["ID"]]['decor'])>0)
                            {
                                $sql4='SELECT * FROM GOOD_PROPERTIES_BAND WHERE BAND_ID='.$vals[$i]['attributes']["BAND_ID"].' AND PROPERTY_ID=33;';
                                $ssq4=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql4)['data'][0];

                                $sql33='SELECT * FROM GOOD_PROPERTIES_BAND WHERE BAND_ID<>'.$vals[$i]['attributes']["BAND_ID"].' AND PROPERTY_ID=33 AND VALUEDIG='.$ssq4['VALUEDIG'].' '.$text;
                                $ssq332=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql33)['data'];
                                
                                foreach ($ssq332 as $ssq33)
                                {  
                                    
                                    $band_id[$ssq33['BAND_ID']]=$ssq33['BAND_ID'];
                                }
                                if (count($band_id)>0) $res[$vals[$i]['attributes']["ID"]]['complete']=$band_id;

                                unset($band_id);
                            }
                            // exit;
                        }   
                      
                    }
                }
            }
        }            
      
    }
    // p_($res);exit;
    return($res);
}


// function frez_diametr_correct ($vals)
// {
//     $index=make_index($vals);
//     foreach ($index['OPERATION'] as $i)
//     {
//         if (($vals[$i]['attributes']['TYPEID']=="XNC")&&($vals[$i]['attributes']['PROGRAM']<>''))
//         {
//             $v1=get_vals($vals[$i]['attributes']['PROGRAM']);
//             $i1=make_index($v1);
//             // p_($v1);
//             $per['dx']=$v1[0]['attributes']['DX'];
//             $per['dy']=$v1[0]['attributes']['DY'];
//             $per['dz']=$v1[0]['attributes']['DZ'];
//             foreach ($i1['PAR'] as $par)
//             {
//                 if(isset($v1[$par]['attributes']['NAME'])&&(isset($v1[$par]['attributes']['VALUE'])))
//                 $per[$v1[$par]['attributes']['NAME']]=$v1[$par]['attributes']['VALUE'];
//             }
//             foreach ($i1['VAR'] as $par)
//             {
//                 foreach ($per as $k=>$p) $v1[$par]['attributes']['EXPR']=str_replace($k,$p,$v1[$par]['attributes']['EXPR']);
//                 if (!isset($per[$v1[$par]['attributes']['NAME']]))
//                 if(isset($v1[$par]['attributes']['NAME'])&&(isset($v1[$par]['attributes']['EXPR'])))
//                 $per[$v1[$par]['attributes']['NAME']]=$v1[$par]['attributes']['EXPR'];
//             }
//             $arr=['MAC','MS','ML','GR','BF','BL','BR','BB','BT'];
//             $arr_2=['X','Y','DP','T','TN','CX','CY','X1','X2','Y1','Y2'];
//             // p_($arr_2);exit;

//             foreach ($arr as $a)
//             {
//                 // p_($per);
//                 foreach ($i1[$a] as $i_ms)
//                 {
//                     foreach ($v1[$i_ms]['attributes'] as $k1=>$v)
//                     {
//                         // p_($v1[$i_ms]);
//                         if (in_array($k1,$arr_2))
//                         {
//                             // echo $k1.' ';
//                             foreach ($per as $k=>$p) $v=str_replace($k,$p,$v);
//                             try {$v = eval('return '.$v.' ;');}
//                             catch (Exception $e) {$v = $v;}
//                             $v1[$i_ms]['attributes'][$k1]=$v;
//                         }
                        
//                         // p_($v1[$i_ms]);

//                     }
//                 }
//             }
            
//             // p_($v1);exit;
//             foreach ($i1['MS'] as $i_ms)
//             {
                
//                 // p_($v1[$i_ms]);
//                 foreach ($i1['TOOL'] as $t)
//                 {
//                     if ($v1[$t]['attributes']['NAME']==$v1[$i_ms]['attributes']['NAME']) $diam=$v1[$t]['attributes']['D'];
//                 }
//                 // p_($diam);
//                 $i_ms_after=$i_ms+1;
                
//                 while (($v1[$i_ms_after]['tag']=='MAC')||($v1[$i_ms_after]['tag']=='ML'))
//                 {
//                     if ($v1[$i_ms_after]['tag']=='MAC')
//                     {
                        
                        
//                         $now=$v1[$i_ms_after]['attributes'];
//                         $before=$v1[$i_ms_after-1]['attributes'];
//                         // $now['CX']=abs($now['CX']);
//                         // $now['CX']=abs($now['CY']);
//                         // $now['X']=abs($now['X']);
//                         // $now['Y']=abs($now['Y']);
//                         // $before['X']=abs($before['X']);
//                         // $before['Y']=abs($before['Y']);
//                         // if ($now['CX']>$now['X']) $a=$now['CX']-$now['X'];
//                         // else $a=$now['X']-$now['CX'];
//                         // if ($now['CY']>$now['Y']) $b=$now['CY']-$now['Y'];
//                         // else $b=$now['Y']-$now['CY'];
//                         // if ($now['CX']>$before['X']) $a1=$now['CX']-$before['X'];
//                         // else $a1=$before['X']-$now['CX'];
//                         // if ($now['CY']>$now['Y']) $b1=$now['CY']-$now['Y'];
//                         // else $b1=$now['Y']-$now['CY'];
//                         $a=$now['CX']-$now['X'];
//                         $b=$now['CY']-$now['Y'];
//                         $r1=sqrt($a*$a+$b*$b);
//                         $a1=$now['CX']-$before['X'];
//                         $b1=$now['CY']-$before['Y'];
//                         $r2=sqrt($a1*$a1+$b1*$b1);
//                         if ($r1<>$r2)
//                         {
//                             $x1=$before['X'];
//                             $y1=$before['Y'];
//                             $x2=$now['X'];
//                             $y2=$now['Y'];
//                             $cx=$now['CX'];
//                             $cy=$now['CY'];
//                             // echo "x1 $x1 x2 $x2 y1 $y1 y2 $y2 cx $cx cy $cy <hr>";

//                             $r=sqrt(($cx-$x1)*($cx-$x1)+($cy-$y1)*($cy-$y1));
//                             // test ('r',$r);
//                             $d=sqrt (($x1-$x2)*($x1-$x2) + ($y1-$y2)*($y1-$y2));
//                             // test ('d',$d/2);
//                             // $r+=5;
//                             // test ('r',$r);

//                             $h = sqrt($r * $r - ($d/2) * ($d/2));
//                             if (is_nan($h))
//                             {
//                                 $d=$d-1;
//                                 $h = sqrt($r * $r - ($d/2) * ($d/2));
//                             }
//                             // test ('h',$h);

//                             $x01 = $x1 + ($x2 - $x1)/2 + $h * ($y2 - $y1) / $d;
//                             $y01 = $y1 + ($y2 - $y1)/2 - $h * ($x2 - $x1) / $d;
//                             $x02 = $x1 + ($x2 - $x1)/2 - $h * ($y2 - $y1) / $d;
//                             $y02 = $y1 + ($y2 - $y1)/2 + $h * ($x2 - $x1) / $d;
//                             // p_($v1[$i_ms_after]['attributes']);
//                             if ($now['DIR']=='false')
//                             {
//                                 $v1[$i_ms_after]['attributes']['CX']=$x01;
//                                 $v1[$i_ms_after]['attributes']['CY']=$y01;
//                                 $now['CX']=$x01;
//                                 $now['CY']=$y01;
//                             }
//                             else
//                             {
//                                 $v1[$i_ms_after]['attributes']['CX']=$x02;
//                                 $v1[$i_ms_after]['attributes']['CY']=$y02;
//                                 $now['CX']=$x02;
//                                 $now['CY']=$y02;
//                             }
//                             // p_($v1[$i_ms_after]['attributes']);
//                             $a=$now['CX']-$now['X'];
//                             $b=$now['CY']-$now['Y'];
//                             $r1=sqrt($a*$a+$b*$b);
//                             $a1=$now['CX']-$before['X'];
//                             $b1=$now['CY']-$before['Y'];
//                             $r2=sqrt($a1*$a1+$b1*$b1);
                            
//                             // p_($now);
//                             // p_($before);
//                             // p_('a - '.$a.'   a1 - '.$a1.'  b - '.$b.'   b1 - '.$b1);
//                         }
//                         if ($r1<$diam/2)
//                         {
//                             foreach ($i1['TOOL'] as $t)
//                             {
//                                 if ($v1[$t]['attributes']['D']==$r1*2)
//                                 {
//                                     $v1[$i_ms_after]['attributes']['NAME']=$v1[$t]['attributes']['NAME'];
//                                 }
//                                 else
//                                 {
//                                     $v1[$i_ms]['attributes']['NAME']='Mill'.($r1*2-1);
//                                     $new_t[]=($r1*2-1);
                                    
//                                 }
//                             }
//                         }
//                         // p_('r1 - '.$r1.'   r2 - '.$r2);
                        

//                     }
//                     $i_ms_after++;
//                 }
//             }
//             $new_t=array_unique($new_t);
//             // p_($new_t);
//             foreach ($new_t as $t)
//             {
//                 $ar=['tag'=>'TOOL','level'=>2,'type'=>'complete','attributes'=>['NAME'=>'Mill'.$t,'D'=>$t]];
//                 $v1=move_vals($v1,1,1);
//                 $v1[1]=$ar;
//                 ksort($v1);
//             }
//             // p_($v1);
//             $vals[$i]['attributes']['PROGRAM']=vals_index_to_project($v1);
//         }
        
        

//     }
//     // xml($vals);
//     return $vals;
// }

function put_sheet_parts_into_project($vals,$sheet_parts)
{
    //xml($vals);exit;
    
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    $vals=move_vals($vals,1,count($sheet_parts)+2);

    $vals[1]=
        Array
        (
            "tag" => "GOOD",
            "type" => "open",
            "level" => 2,
            "attributes" => Array
                (
                    "ID" => new_id($vals)+1,
                    "NAME" => "Остаток клиента",
                    "TYPEID" => "product",
                    "COUNT"=>1,
                    "MY_CLIENT_STOCK"=>1
                )
        );
    

    $i=1;
    // p_($sheet_parts);exit;
    foreach ($sheet_parts as $value)
    {
       
        $i++;
        $vals[$i]=
        Array
        (
            "tag" => "PART",
            "type" => "complete",
            "level" => 3,
            "attributes" => Array
                (
                    "CL" => $value["L"],
                    "COUNT" => $value["COUNT"],
                    "CW" => $value["W"],
                    "DL" => $value["L"],
                    "DW" => $value["W"],
                    "ID" => $value["ID"],
                    "MY_CLIENT_STOCK"=>1,
                    "MY_MATERIAL_CODE" => $value["CODE"],
                    "JL" => $value["L"],
                    "JW" => $value["W"],
                    "L" => $value["L"],
                    "NAME" => $value["NAME"],
                    "USEDCOUNT" => 0,
                    "W" => $value["W"]
                )

        );
        // p_($vals[2]);exit;
        unset($vals[count($sheet_parts)+2+$value["index"]]);
        
    }
    $vals[2+count($sheet_parts)]=
    Array
        (
            "tag" => "GOOD",
            "type" => "close",
            "level" => 2
        );
     
    ksort($vals);
    
    $vals=vals_out($vals);
    $project_data=vals_index_to_project($vals);
    $_SESSION['op_ch_sheet_part']=1;
    $response_optimization = send_post_data($project_data, '55C00560');

    $array = json_decode($response_optimization, true);
    if (!isset($array['project'])) {
        echo "Данные не прошли оптимизацию";
        exit;
    } else {
        $data_xml = $array['project'];
        ///echo 'Данные прошли оптимизацию';
    }
    unset($_SESSION['op_ch_sheet_part']);
    $data = get_vals_index_without_session($array['project']);
    $vals = $data["vals"];
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    
    return ($vals);
}

function put_to_db($vals,$link,$status,$order1c,$project,$part)
{
    
    $vals = vals_out($vals);
    ksort($vals);
    $index=make_index($vals);
    $vals2=$vals;
    // p_($vals);exit;

    // $v3[]=(vals_index_to_project($vals));

    // xml_html ($vals);
    if (($status==1)&&(isset($vals)))
    {
        include("check_production_possibilities.php");
        // echo $error;
    }
    elseif (($status==2)&&(isset($vals)))
    {

    }
    else exit('out of data!');
    if (!isset($error))
    {
        $vals=$vals2;

        foreach ($vals as $i=>$v)
        {
            $vals[$i]=my_uncet($vals[$i],'my_db_id');
        }
        $sql = "DELETE FROM PART_BAND WHERE order1c=".$order1c.";";
        $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['res'];
        $sql = "DELETE FROM PART_SERV WHERE order1c=".$order1c.";";
        $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['res'];
        $sql = "DELETE FROM PRODUCT  WHERE ORDER1C_ID=".$order1c.";";
        $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['res'];
        $sql = "DELETE FROM PART  WHERE ORDER1C=".$order1c.";";
        $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['res'];
        $vals=add_tag_print($vals);
        foreach ($index['OPERATION'] as $i)
        {
            if ((isset($vals[$i]['attributes']['DATA']))&&($vals[$i]['attributes']['operation.my_fix_card']<>1))
            {
                unset ($vals[$i]['attributes']['DATA']);
                unset ($vals[$i]['attributes']['DRAW']);
            }
            elseif ((isset($vals[$i]['attributes']['DATA']))&&($vals[$i]['attributes']['operation.my_fix_card']==1))
            {
                $y=$i+2;
                while ($vals[$y]['tag']=='PART')
                {
                    $dont_tuch_part[]=$vals[$y]['attributes']['ID'];
                    $y++;
                }
            }
        }
        // p_($vals);exit;

        foreach ($index['PART'] as $i)
        {
            if (!in_array($vals[$i]['attributes']['ID'],$dont_tuch_part))
            {
                if ($vals[$i]['attributes']['SHEETID']>0)
                {
                    $r=$vals[$i]['attributes']['ID'];
                    foreach ($index['PART'] as $j)
                    {
                        if ($vals[$j]['attributes']['ID']==$r) unset ($vals[$j]);
                    }
                    unset ($vals[$i]);
                }
                elseif ($vals[$i]['attributes']['USEDCOUNT']>0) $vals[$i]['attributes']['USEDCOUNT']=0;
            }
        }
        ksort ($vals);
        $index=make_index($vals);
        
        foreach ($index['GOOD'] as $i)
        {
            if (($vals[$i]['attributes']['TYPEID']=='product')&&($vals[$i]['type']=='open')&&($vals[$i+1]['tag']=='PART'))
            {
                
                $sql = "INSERT INTO PRODUCT (`ORDER1C_ID`, `PROJECT_ID`, `NAME`, `COUNT`) 
                VALUES ('".$order1c."','".$project."','".$vals[$i]['attributes']['NAME']."','".$vals[$i]['attributes']['COUNT']."')";
                // echo $sql;
                $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                $product=$query['id'];


                $vals[$i]['attributes']['MY_DB_ID']=$product;
                $j=$i+1;

                while ($vals[$j]['tag']=="PART")
                {
                    // p_($vals[$j]['attributes']);
                    
                    $array=array('L','W', 'T', 'DL','DW','JL','JW','CL','CW','ALLOWANCET','ALLOWANCEB','ALLOWANCEL','ALLOWANCER', 'COUNT', 'USEDCOUNT','TXT','GROUPINBLOCK', 'GROUPINBLOCKMC',
                    'GROUPINBLOCKMAXC','MY_DOUBLE','MY_DOUBLE_FACE','MY_SIDE_TO_CUT_L','MY_SIDE_TO_CUT_R','MY_SIDE_TO_CUT_T','MY_SIDE_TO_CUT_B','MY_XNC_CUT_DELTAX','MY_XNC_CUT_DELTAY',
                    'MY_DOUBLE_TYPE','MY_DOUBLE_RES','MY_MAT', 'MY_MAT_AREA_ST', 'MY_MAT_AREA_LOSE','CUT_L','CUT_R','CUT_T','CUT_B');
                    
                    $sql=arrayTOsql($array,'PART',$order1c,$product,$vals[$j]['attributes']);
                    // echo $sql;exit;
                    $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                    $part_connection_db[$vals[$j]['attributes']['ID']]=$query['id'];
                    
                    if ($query['id']>0) $vals[$j]['attributes']['MY_DB_ID']=$part_connection_db[$vals[$j]['attributes']['ID']];
                        
                    
                   
                    $sql="UPDATE PART SET ";
                    $side=array("L","R","B","T");
                    foreach($side as $s)
                    {
                        if (isset($vals[$j]['attributes']['MY_DB_EL'.$s])) $sql.=" EL".$s."=".$vals[$j]['attributes']['MY_DB_EL'.$s].",";
                        if (isset($vals[$j]['attributes']['MY_DB_KL_EL'.$s])) $sql.=" KL_EL".$s."=".$vals[$j]['attributes']['MY_DB_KL_EL'.$s].",";
                        if (isset($vals[$j]['attributes']['MY_CUT_ANGLE_'.$s])) $sql.=" CUT_".$s."=".$vals[$j]['attributes']['MY_CUT_ANGLE_'.$s].",";
                        if (isset($vals[$j]['attributes']['GR'.$s]))
                        {
                        
                            foreach ($index['OPERATION'] as $i1)
                            {
                                // echo "!";
                                if (($vals[$i1]['attributes']["TYPEID"]=="GR")&&
                                ($vals[$j]['attributes']['GR'.$s]=="@operation#".$vals[$i1]['attributes']["ID"]))
                                
                                {
                                $gr=base64_encode(serialize($vals[$i1]));
                                    $sql.=" GR".$s."='".$gr."',";
                                    // $gr=base64_decode($gr);
                                    // $gr=unserialize($gr);
                                    // p_($gr);
                                }
                            }
                        }
                    }
                    if ($vals[$j]['attributes']['MY_MAT']>0) $sql.=" MY_MAT=".$vals[$j]['attributes']['MY_MAT'].",";
                    elseif (($vals[$j]['attributes']['MY_DB_FIRST']>0)&&($vals[$j]['attributes']['MY_DB_SECOND']>0))
                    {
                        $sql.=" MY_FIRST=".$vals[$j]['attributes']['MY_DB_FIRST'].",";
                        $sql.=" MY_SECOND=".$vals[$j]['attributes']['MY_DB_SECOND'].",";
                    }
                    if ($vals[$j]['attributes']['part.my_double_type']>0)
                    {
                        $sql.=" MY_DOUBLE_TYPE=".$vals[$j]['attributes']['part.my_double_type'].",";
                    }
                    foreach ($index['OPERATION'] as $i1)
                    {
                        if (($vals[$i1]['attributes']["TYPEID"]=="XNC")&&($vals[$i1]['type']=='open'))
                        {
                            $i2=$i1+1;
                            while ($vals[$i2]['tag']<>"PART")
                            {
                                $i2++;
                            }
                            
                            while ($vals[$i2]['tag']=="PART")
                            {
                                if ($vals[$i2]['attributes']['ID']==$vals[$j]['attributes']['ID'])
                                {
                                    $xnc[$part_connection_db[$vals[$j]['attributes']['ID']]][]=base64_encode(serialize($vals[$i1]));
                                }
                                $i2++;
                            }
                        }
                        
                    }
                   
                    if (isset($xnc))
                    {
                        $xnc=base64_encode(serialize($xnc));
                        $sql.=" MY_XNC='".$xnc."',";
                        // $xnc=base64_decode($xnc);
                        // $xnc=unserialize($xnc);
                        // p_($xnc);
                    }
                    unset($xnc);
                    if (substr($sql,strlen($sql)-1,1)==",")
                    {
                        $sql=substr($sql,0,strlen($sql)-1);
                    }
                    if ((isset($part_connection_db[$vals[$j]['attributes']['ID']]))&&($part_connection_db[$vals[$j]['attributes']['ID']]>0))
                    {
                        if ($sql<>"UPDATE PART SET ") $sql.=", MY_DATE='".date('Y-m-d H:i:s')."' WHERE PART_ID=".$part_connection_db[$vals[$j]['attributes']['ID']].";";
                        else $sql.=" MY_DATE='".date('Y-m-d H:i:s')."' WHERE PART_ID=".$part_connection_db[$vals[$j]['attributes']['ID']].";";
                    // echo $sql;
                        $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['res'];
                    }
                    $j++;
                }
            }
        }
        foreach ($index['PART'] as $i)
        {
            if ((isset($vals[$i]['attributes']['ID']))&&(isset($part_connection_db[$vals[$i]['attributes']['ID']])&&($part_connection_db[$vals[$i]['attributes']['ID']]>0)))
            {
                    $vals[$i]['attributes']['ID']=$part_connection_db[$vals[$i]['attributes']['ID']];
            }
        
          
        }
        unset ($double_id);
        // p_($part_connection_db);exit;
        

        // p_part($vals);
        foreach ($index['PART'] as $i)
        {
            if (!in_array($vals[$i]['attributes']['MY_DOUBLE_ID'],$double_id))$double_id[]=$vals[$i]['attributes']['MY_DOUBLE_ID'];
        }
        foreach ($double_id as $did)
        {
            foreach ($index['PART'] as $i)
            {
                if (($vals[$i]['attributes']['MY_DOUBLE_ID']==$did)&&($vals[$i]['attributes']['MY_DOUBLE_RES']==1))
                {
                    $new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']]=$vals[$i]['attributes']['MY_DB_ID'];
                    $new_parent_main[$vals[$i]['attributes']['MY_DOUBLE_ID']]=$vals[$i]['attributes']['MY_DB_ID'];
                    $vals[$i]['attributes']['MY_DOUBLE_ID']=$vals[$i]['attributes']['MY_DB_ID'];
                    unset($vals[$i]['attributes'][mb_strtolower('MY_DOUBLE_ID')],$vals[$i]['attributes'][mb_strtolower('PART.MY_DOUBLE_ID')]);
                    unset($vals[$i]['attributes']['PART.MY_DOUBLE_ID']);
                }
            }
            foreach ($index['PART'] as $i)
            {
                if (($vals[$i]['attributes']['MY_DOUBLE_ID']==$did)&&($vals[$i]['attributes']['MY_DOUBLE_FACE']==1))
                {
                    $new_parent[$vals[$i]['attributes']['MY_DOUBLE_ID']]=$vals[$i]['attributes']['MY_DB_ID'];
                    $vals[$i]['attributes']['MY_DOUBLE_PARENT']=$new_parent_main[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                    $vals[$i]['attributes']['MY_DOUBLE_ID']=$new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                    unset($vals[$i]['attributes'][mb_strtolower('MY_DOUBLE_ID')],$vals[$i]['attributes'][mb_strtolower('PART.MY_DOUBLE_ID')]);
                    unset($vals[$i]['attributes']['PART.MY_DOUBLE_ID']);


                }
            }
            foreach ($index['PART'] as $i)
            {
                if (($vals[$i]['attributes']['MY_DOUBLE_ID']==$did)&&($vals[$i]['attributes']['MY_DOUBLE_FACE']==0))
                {
                    if (isset($new_parent[$vals[$i]['attributes']['MY_DOUBLE_ID']])) $vals[$i]['attributes']['MY_DOUBLE_PARENT']=$new_parent[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                    if (isset($new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']])) $vals[$i]['attributes']['MY_DOUBLE_ID']=$new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                    unset($vals[$i]['attributes'][mb_strtolower('MY_DOUBLE_ID')],$vals[$i]['attributes'][mb_strtolower('PART.MY_DOUBLE_ID')]);
                    unset($vals[$i]['attributes']['PART.MY_DOUBLE_ID']);


                }
            }
        }
        foreach ($index['GOOD'] as $i)
        {
            if (($vals[$i]['attributes']['MY_DOUBLE_ID']>0)&&(isset($new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']])))
            {
                $vals[$i]['attributes']['MY_DOUBLE_ID']=$new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                unset($vals[$i]['attributes'][mb_strtolower('MY_DOUBLE_ID')],$vals[$i]['attributes'][mb_strtolower('GOOD.MY_DOUBLE_ID')]);
                unset($vals[$i]['attributes']['GOOD.MY_DOUBLE_ID']);
            }
        }
        foreach ($index['OPERATION'] as $i)
        {
            if (($vals[$i]['attributes']['MY_DOUBLE_ID']>0)&&(isset($new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']])))
            {
                $vals[$i]['attributes']['MY_DOUBLE_ID']=$new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                unset($vals[$i]['attributes'][mb_strtolower('MY_DOUBLE_ID')],$vals[$i]['attributes'][mb_strtolower('OPERATION.MY_DOUBLE_ID')]);
                unset($vals[$i]['attributes']['OPERATION.MY_DOUBLE_ID']);
            }
        }
        // p_part($vals);
        // p_($vals);
        // exit;
        foreach ($vals as $i=>$key)
        {
            
            if (((isset($vals[$i]['attributes']['NAME']))&&(substr_count($vals[$i]['attributes']['NAME'],"[/")>0))&&($vals[$i]['tag']=='PART'))
            {
                $num=substr($vals[$i]['attributes']['NAME'],strpos($vals[$i]['attributes']['NAME'],"[/")+2,strpos($vals[$i]['attributes']['NAME'],"/]")-strpos($vals[$i]['attributes']['NAME'],"[/")-2);
                $vals[$i]['attributes']['NAME']=str_replace("[/".($num/1)."/]","[/".$vals[$i]['attributes']['MY_DOUBLE_ID']."/]",$vals[$i]['attributes']['NAME']);
                unset($vals[$i]['attributes']['part.name1']);
                unset($vals[$i]['attributes']['PART.NAME1']);
                $vals[$i]['attributes']['part.name1']=$vals[$i]['attributes']['NAME'];
                
            }
            elseif (((isset($vals[$i]['attributes']['NAME']))&&(substr_count($vals[$i]['attributes']['NAME'],"[/")>0))&&($vals[$i]['tag']=='GOOD'))
            {
                $num=substr($vals[$i]['attributes']['NAME'],strpos($vals[$i]['attributes']['NAME'],"[/")+2,strpos($vals[$i]['attributes']['NAME'],"/]")-strpos($vals[$i]['attributes']['NAME'],"[/")-2);
                $vals[$i]['attributes']['NAME']=str_replace("[/".($num/1)."/]","[/".$vals[$i]['attributes']['MY_DOUBLE_ID']."/]",$vals[$i]['attributes']['NAME']);
                unset($vals[$i]['attributes']['good.name1']);
                unset($vals[$i]['attributes']['GOOD.NAME1']);
                $vals[$i]['attributes']['good.name1']=$vals[$i]['attributes']['NAME'];
            }
            elseif (((isset($vals[$i]['attributes']['NAME']))&&(substr_count($vals[$i]['attributes']['NAME'],"[/")>0))&&($vals[$i]['tag']=='OPERATION'))
            {
                $num=substr($vals[$i]['attributes']['NAME'],strpos($vals[$i]['attributes']['NAME'],"[/")+2,strpos($vals[$i]['attributes']['NAME'],"/]")-strpos($vals[$i]['attributes']['NAME'],"[/")-2);
                unset($vals[$i]['attributes']['operation.name1']);
                unset($vals[$i]['attributes']['OPERATION.NAME1']);
                if (($num<>'')||(isset($num)))
                {
                    $num=$num/1;
                    $vals[$i]['attributes']['NAME']=str_replace("[/".$num."/]","[/".$new_did[$num]."/]",$vals[$i]['attributes']['NAME']);
                    $vals[$i]['attributes']['NAME']=str_replace($num,$new_did[$num],$vals[$i]['attributes']['NAME']);
                    $vals[$i]['attributes']['operation.name1']=$vals[$i]['attributes']['NAME'];
                    $vals[$i]['attributes']['TYPENAME']=$vals[$i]['attributes']['NAME'];
                    $vals[$i]['attributes']['CODE']=str_replace($num,$new_did[$num],$vals[$i]['attributes']['CODE']);

                }
                else
                {
                    $vals[$i]['attributes']['CODE']=$new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']];
                    $vals[$i]['attributes']['NAME']="[/".$new_did[$vals[$i]['attributes']['MY_DOUBLE_ID']]."/]";
                    $vals[$i]['attributes']['TYPENAME']=$vals[$i]['attributes']['NAME'];
                }
            }
            if (((isset($vals[$i]['attributes']['DESCRIPTION']))&&(substr_count($vals[$i]['attributes']['DESCRIPTION'],"[/")>0)))
            {
                $num=substr($vals[$i]['attributes']['DESCRIPTION'],strpos($vals[$i]['attributes']['DESCRIPTION'],"[/")+2,strpos($vals[$i]['attributes']['DESCRIPTION'],"/]")-strpos($vals[$i]['attributes']['DESCRIPTION'],"[/")-2);
                $vals[$i]['attributes']['DESCRIPTION']=str_replace("[/".($num/1)."/]","[/".$vals[$i]['attributes']['MY_DOUBLE_ID']."/]",$vals[$i]['attributes']['DESCRIPTION']);
            }
            
            if (($vals[$i]['attributes']['MY_DB_ID']>0)&&($vals[$i]['attributes']['TYPEID']=="product"))
            {
                $sql="UPDATE PRODUCT SET NAME='".$vals[$i]['attributes']['NAME']."'
                WHERE PRODUCT_ID=".$vals[$i]['attributes']['MY_DB_ID'].";";
                $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
            }
        }
        $vals=add_tag_print($vals);
        // 222222
        // p_($part);
        $ar=array('kromkovanie','service_porezka','service_zar','GR','XNC','service_styagka','por_kromki');
        unset ($p_serv);
        unset ($pb);
        foreach ($part as $id=>$p)
        {
            
            foreach ($ar as $a)
            {
                if (count($p[$a])>0)
                {
                    foreach ($p[$a] as $serv=>$c)
                    {
                        $p_serv[$id][$serv]+=$c;
                    }
                }
            }
            
            if (count($p['band'])>0)
            {
                $ar2=array('left'=>'CW','right'=>'CW','top'=>'CL','botton'=>'CL');
                
                foreach ($ar2 as $s=>$k)
                {
                    if (($p[$s.'_band']>0)&&(is_numeric($p[$s.'_band'])))
                    {
                        $l=$p[$k]/1000*1.02+0.07;
                        if ($l<0.11) $l=0.11;
                        $pb[$id][$p[$s.'_band']]+=$l;
                    }
                }
            }
            
        }
        if (count($p_serv)>0) 
        {
            foreach ($part as $id=>$p)
            {
                if (isset ($p_serv[$id]))
                {
                    foreach ($p_serv[$id] as $serv=>$c)
                    {
                        if (($part_connection_db[$id]>0)&&($serv>0)&&($order1c>0))
                        {
                            $sql='INSERT INTO PART_SERV (`part`,`serv`,`count`,`order1c`) VALUES ('.$part_connection_db[$id].','.$serv.','.$c.','.$order1c.');';
                            // echo $sql;
                            $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                        }
                    }
                }
            }
        }
        if (count($pb)>0) 
        {
            foreach ($part as $id=>$p)
            {
                if (isset ($pb[$id]))
                {
                    foreach ($pb[$id] as $b=>$c)
                    {
                        if (($part_connection_db[$id]>0)&&($b>0)&&($c>0)&&($order1c>0))
                        {
                            $sql='INSERT INTO PART_BAND (`part`,`band`,`count`,`order1c`) VALUES ('.$part_connection_db[$id].','.$b.','.$c.','.$order1c.');';
                            $res=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                        }
                    }
                }
            }
        }
        $index=make_index($vals);
        

        // my_mail('!!! '.__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'Деталь не вошла в раскрой '.$vals[0]['attributes']['NAME'],(vals_index_to_project($vals)),'xml');
        // $v333=print_r($part_connection_db,true);
        // my_mail('11!!! '.__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'4',$v333,'txt');

        // $v3[]=(vals_index_to_project($vals));

        foreach ($index['OPERATION'] as $i)
        {
            // p_($vals[$i]);
            if ((isset($vals[$i]['attributes']['DRAW']))&&($vals[$i]['attributes']['operation.my_fix_card']==1))
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]['attributes']['DRAW'], $v1, $i1);
                xml_parser_free($p);
                // $v3[]=(vals_index_to_project($v1));

                // p_($part_connection_db);
                // p_(htmlspecialchars(vals_index_to_project($v1)));
                foreach ($i1['D'] as $r)
                {
                    
                    if($part_connection_db[$v1[$r]['attributes']['ID']]>0) $v1[$r]['attributes']['ID']=$part_connection_db[$v1[$r]['attributes']['ID']];

                }
                // $v3[]=(vals_index_to_project($v1));
                
                // p_(htmlspecialchars(vals_index_to_project($v1)));
               
                // exit;
                $vals[$i]['attributes']['DRAW']=vals_index_to_project($v1);
            }

            if ((isset($vals[$i]['attributes']['DATA']))&&($vals[$i]['attributes']['operation.my_fix_card']==1))
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]['attributes']['DATA'], $v1, $i1);
                xml_parser_free($p);

                foreach ($i1['COMMAND'] as $r)
                {  
                    if ($part_connection_db[$v1[$r]['attributes']['REF']]>0) $v1[$r]['attributes']['REF']=$part_connection_db[$v1[$r]['attributes']['REF']];
                }
                $vals[$i]['attributes']['DATA']=vals_index_to_project($v1);
            }

            //.. 15.09.2022 Правки, чтобы совпвдали раскрои при редактировании и оформлении проекта
            if ((isset($vals[$i]['attributes']['DATA']))&&($vals[$i]['attributes']['operation.my_fix_card']<>1))
            {
                $p = xml_parser_create();
                xml_parse_into_struct($p, $vals[$i]['attributes']['DATA'], $v1, $i1);
                xml_parser_free($p);

                foreach ($i1['COMMAND'] as $r)
                {  
                    if ($part_connection_db[$v1[$r]['attributes']['REF']]>0) $v1[$r]['attributes']['REF']=$part_connection_db[$v1[$r]['attributes']['REF']];
                }
                $vals[$i]['attributes']['DATA']=vals_index_to_project($v1);
            }
        }

        return ($vals);
    }
    else return ($error);
    
}

?>