<?php
$main_text="";
$main_text="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<project ";
foreach ($project as $k=>$v)
{
    $main_text.=k($k,$gib_per)."=\"".$v."\" ";
}
$main_text=substr($main_text,0,strlen($main_text)-1);
$main_text.=">\n";
foreach ($good_product_ as $k=>$v)
{
    $main_text.="<good ";
    foreach ($v as $k1=>$v1)
    {
        $main_text.=k($k1,$gib_per)."=\"".$v1."\" ";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.=">\n";
    
    foreach($id_part as $kp=>$vp)
    {
        $i=0;    
        if ($vp==$k)
        {
            $i++;
            if ($i==1) $main_text.="<part ";
            foreach ($part_[$kp] as $kpa=>$vpa)
            {
                $main_text.=k($kpa,$gib_per)."=\"".$vpa."\" ";
            }
            $main_text=substr($main_text,0,strlen($main_text)-1);
            $main_text.="/>\n";
        }
    }
    $main_text.="</good>\n";
}
foreach($cut_mat_part_ as $cmk=>$cmv)
{
    $main_text.="<good ";
    foreach($good_tool_cutting_[$cmk] as $k=>$v)
    {
        $main_text.=k($k,$gib_per)."=\"".$v."\" ";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.="/>\n";
    $main_text.="<good ";
    foreach ($good_sheet_[key($cmv)] as $k1=>$v1)
    {
        $main_text.=k($k1,$gib_per)."=\"".$v1."\" ";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.=">\n";
    foreach ($cmv[key($cmv)] as $v)
    {
        $main_text.="<part ";
        foreach($good_sheet_part_[$v] as $k=>$v)
        {
            $main_text.=k($k,$gib_per)."=\"".$v."\" ";
        }
        $main_text=substr($main_text,0,strlen($main_text)-1);
        $main_text.="/>\n";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.="\n</good>\n";    
}
foreach($band_array_con as $k=>$v)
{
    $main_text.="<good ";
    foreach($good_tool_edgeline_[$k] as $k1=>$v1)
    {
        $main_text.=k($k1,$gib_per)."=\"".$v1."\" ";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.="/>\n";
    $main_text.="<good ";
    foreach($good_band_[$v] as $k2=>$v2)
    {
        $main_text.=k($k2,$gib_per)."=\"".$v2."\" ";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.="/>\n";
}
foreach($operation_mat_ as $k1=>$v1)
{
    foreach($v1 as $k=>$v)
    {
        $main_text.="<operation ";
        foreach($operation_[$v["type"]][$k] as $k1=>$v1)
        {
            $main_text.=k($k1,$gib_per)."=\"".$v1."\" ";
        }
        $main_text=substr($main_text,0,strlen($main_text)-1);
        $main_text.=">\n";
        if ($v["MATERIAL_ID"])
        {
            $main_text.="<material id=\"".$v["MATERIAL_ID"]."\" count=\"".$v["MATERIAL_COUNT"]."\"/>\n";
        }
        if($v["PARTS"])
        {
            $pt=explode("__",$v["PARTS"]);
            foreach ($pt as $kp=>$vp)
            {
                $main_text.="<part id=\"".$vp."\"/>\n";
                
            }
        }
        if ($v["MATERIAL_ID"]) $main_text.="</operation>\n"; 
        else $main_text.="</operation>\n";
    }
}
foreach ($good_simple_ as $k=>$v)
{
    $main_text.="<good ";
    foreach ($v as $k1=>$v1)
    {
        $main_text.=k($k1,$gib_per)."=\"".$v1."\" ";
    }
    $main_text=substr($main_text,0,strlen($main_text)-1);
    $main_text.="/>\n";
}
$main_text.="</project>";
//..27.11.2020 Тут не стал ничего менять т.к. нет записи в БД, только в переменную
$project["MY_PROJECT_OUT"]=base64_encode($main_text);
?>