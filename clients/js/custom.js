jQuery(document).ready(function(){

$('.phone').mask('+00(000)000-00-00', {placeholder: "+__ (___) ___-__-__"});

    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        var idT=e.target.attributes.getNamedItem("aria-controls").value;
        var idR=e.relatedTarget.attributes.getNamedItem("aria-controls").value;
        if(idT=='pills-user') $('#typein').val('client'); else $('#typein').val('manager');
        $('div#'+idT+'  .the-required').attr('required',"required");
        $('div#'+idR+'  .the-required').removeAttr('required');
    });

    $('#writeOff a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var idT=e.target.attributes.getNamedItem("aria-controls").value;
        var idR=e.relatedTarget.attributes.getNamedItem("aria-controls").value;
        $('div#'+idT+'  .the-required').attr('required',"required");
        $('div#'+idR+'  .the-required').removeAttr('required');
    });
    /*
    $(document).on('keyup blur', '#pills-user input#mail1', function (event, sel) {
        $('div#pills-user input#mail1').attr('required',"required");
        $('div#pills-user input#login_tel').removeAttr('required');
        $('div#pills-user input#login_tel').val('');
    });

    $(document).on('keyup blur', '#pills-user input#login_tel', function (event, sel) {
        $('div#pills-user input#login_tel').attr('required',"required");
        $('div#pills-user input#mail1').removeAttr('required');
        $('div#pills-user input#mail1').val('');
    });
    */
    $(document).on('keyup', '.add-user input#new_phone', function (event, sel) {
       if($(this).val().length==17)
       {
           $('#new_phone').attr('data-valid','0');
           var out={};
           out['new_phone'] = $(this).val();
           $.ajax({
               type: 'POST',
               url:'ajax_resp.php',
               data:out,
               success:function(resp){
                   if(resp==1) {
                       $('#phoneHelp').html('<span style="color: red;">Такой телефон уже существует! </span>');
                       $('label[for="new_mail"]').addClass('label-required');
                       $('input#new_mail').attr('required',"required");
                       $('#new_phone').attr('data-valid','0');
                       add_user_showhide();
                   }
                   else
                      {
                          $('#phoneHelp').html('<span style="color: green;">Такой телефон не существует, продолжайте... </span>');
                          $('#mailHelp').html('');
                          $('label[for="new_mail"]').removeClass('label-required');
                          $('input#new_mail').removeAttr('required');
                          $('#new_phone').attr('data-valid','1');
                          add_user_showhide();
                      }

               }
           });
       }
       else
       {
           $('#phoneHelp').html('<span style="color: red;">Введите телефон! </span>');
           $('label[for="new_mail"]').addClass('label-required');
           $('input#new_mail').attr('required',"required");
           $('#new_phone').attr('data-valid','0');
           add_user_showhide();
       }
    });
    $(document).on('keyup', '.add-user input#new_mail', function (event, sel) {
        if ($(this).is(':valid')) {
            $('#new_mail').attr('data-valid', '0');
            var out={};
            out['new_mail'] = $(this).val();
            $.ajax({
                type: 'POST',
                url: 'ajax_resp.php',
                data: out,
                success: function (resp) {
                    if (resp == 1) {
                        $('#mailHelp').html('<span style="color: red;">Такой E-mail уже существует! </span>');
                        $('label[for="new_phone"]').addClass('label-required');
                        $('input#new_phone').attr('required',"required");
                        $('#new_mail').attr('data-valid', '0');
                        add_user_showhide();
                    }
                    else {
                        $('#mailHelp').html('<span style="color: green;">Такой E-mail не существует, продолжайте... </span>');
                        $('#phoneHelp').html('');
                        $('label[for="new_phone"]').removeClass('label-required');
                        $('input#new_phone').removeAttr('required');
                        $('#new_mail').attr('data-valid','1');
                        add_user_showhide();
                    }

                }
            });
        }
        else
        {
            $('#mailHelp').html('<span style="color: red;">Введите E-mail</span>');
            $('label[for="new_phone"]').addClass('label-required');
            $('input#new_phone').attr('required',"required");
            $('#new_mail').attr('data-valid','0');
            add_user_showhide();
        }
    });
    $(document).on('keyup', '.add-user .input-password input', function (event, sel) {
        var theID=$(this).attr('id');
        if(theID=='new_password')  var theID2='new_password2'; else var  theID2='new_password';
        if($(this).val()==$('#'+theID2).val())
        {
            $('#passwordHelp').html('<span style="color: green;">Пароли совпадают </span>');
            $(".add-user button").prop( "disabled", false );
        }
        else
        {
            $('#passwordHelp').html('<span style="color: red;">Пароли не совпадают </span>');
            $(".add-user button").prop( "disabled", true );
        }

    });
    $(document).on('keyup', 'input#client_search_tel', function (event, sel) {

        if($(this).val().length==17)
        {
            var out={};
            out['client_search_tel'] = $(this).val();
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:out,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#client_searchStatus').html('<span style="color: green;">Клиент найден! </span>');
                        var resp_mail='';
                        var resp_name='';
                        if(resp_in.mail !== null)  resp_mail=resp_in.mail;
                        if(resp_in.name !== null)  resp_name=resp_in.name;

                        $('.client-data').html('<span>Добавить остатки клиенту <h3>'+resp_name+' '+resp_mail+'</h3></span>');
                        $('input[name="clientID"]').val(resp_in.client_id);
                        $('.client-data').show();

                        $('#client_search_tel').prop( "disabled", true );
                        $('form.coming').show();
                    }
                    else
                    {
                        $('#client_searchStatus').html('<span style="color: red;">Такой телефон не существует, повторите поиск... </span>');
                        $('.client-data').html();
                        $('.client-data').hide();
                    }
                }
            });
        }
        else
        {
            $('#client_searchStatus').html();
        }
    });
    $(document).on('click', '.coming #add', function (event, sel) {
        var cnt=parseInt( $('.coming .tmpl').data("count"))+1;
        $('.coming .tmpl').data("count",cnt);
        var Cline=$('.coming .tmpl').html();
        $('.coming .coming-in').append('<div class="row line-'+cnt+' add-lines" data-index="'+cnt+'">'+Cline+'</div>');
        $('.coming  .line-'+cnt+' input').attr("required", true);
        // $('.coming  .line-'+cnt+' input[name="crash_checkbox[]"]').attr("required", false);
        $('.coming  .line-'+cnt+' select').attr("required", true);
        $(".coming   button.btn-success").prop( "disabled", false );
    });
    $(document).on('click', '.list-cells #addLine', function (event, sel) {
        var teLine=$('tr#template');
        var teLine2='<tr>'+teLine[0]['innerHTML']+'</tr>';
       $('.list-cells table tr:last').after(teLine2);
        $('.list-cells table tr:last input').attr("required", true);
        var count = $(".list-cells tbody tr").length;
        $('.list-cells #countCells').val(count);

    });
    /*
    $(document).on('change', '.coming .add-lines input[name="L[]"]', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        double_verify(container);
//        var cnt=parseInt( $('.coming .tmpl').data("count"))+1;
    });
    */
    $(document).on('change', 'form.return input[name^="return_"]', function (event, sel) {
        var retmax=$(this).data("retmax");
        var ret=$(this).val();
        if(ret>retmax) $(this).val(retmax);
        if(ret<0) $(this).val(0);
    });
    $(document).on('keypress keyup blur', 'form.into-production input[name^="return_"]', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
        var retmax=$(this).data("retmax");
        var numline=$(this).data("numline");
        var ret=$(this).val();
        if(ret>retmax) $(this).val(retmax);
        if(ret<0) $(this).val(0);
        var newval=$(this).val();
        if(newval>0)
        {
            $("form.into-production #orders_"+numline).attr('required',"required");
            $("form.into-production #comment_"+numline).attr('required',"required");
        }
        else
        {
            $("form.into-production #orders_"+numline).val('');
            $("form.into-production #comment_"+numline).val('');
            $("form.into-production #orders_"+numline).removeAttr('required');
            $("form.into-production #comment_"+numline).removeAttr('required');
        }
    });
    $(document).on('change', 'form.return input[name^="corrects_"]', function (event, sel) {
        var retmax=parseInt($(this).data("retmax"));
        var ret=parseInt($(this).val());
        if((ret+retmax)<0) $(this).val(0-retmax);
    });
     $(document).on('keyup', 'form.write-off input[name^="return_"]', function (event, sel) {
        var inprod=parseInt($("input[name='check_inprod']").val());
        var user_in_production=parseInt($("input[name='user_in_production']").val());
         var user_actives=parseInt($("input[name='user_actives']").val());
         var index=$(this).data("index");
         var client_id=parseInt($("input[name='client_"+index+"']").val());
         if((inprod===0 && client_id==user_in_production) || (user_actives==36))
         {
            var ret=$(this).val();
            if(ret>0)
            {
                $(".return-opt-"+index).show();
            }
            else
            {
                $(".returncomment-"+index+" textarea").val('');
                $(".returncomment-"+index+" textarea").removeAttr('required');
                $(".returncomment-"+index).hide();
                $('input:radio[name="cause_ret_'+index+'"][value="time"]').prop('checked', true);
                $(".return-opt-"+index).hide();
            }
        }
    });
    $(document).on('change', 'form.write-off input[name^="cause_ret_"]', function (event, sel) {
        var inprod=parseInt($("input[name='check_inprod']").val());
        var user_in_production=parseInt($("input[name='user_in_production']").val());
        var user_actives=parseInt($("input[name='user_actives']").val());
        var index=$(this).parent().parent().data("index");
        var client_id=parseInt($("input[name='client_"+index+"']").val());
        if((inprod===0 && client_id==user_in_production) || (user_actives==36))
        {
            var ret=$(this).val();
            if(ret=='refuse')
            {
                $(".returncomment-"+index).show();
                $(".returncomment-"+index+" textarea").attr('required',"required");
            }
            else
            {
                $(".returncomment-"+index+" textarea").val('');
                $(".returncomment-"+index+" textarea").removeAttr('required');
                $(".returncomment-"+index).hide();
            }
        }
    });
    $(document).on('click', '.erase-tr', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        $("div.line-"+container).remove();
        if($(".coming .row.add-lines").length==1) $(".coming   button.btn-success").prop( "disabled", true );
    });
    $(document).on('click', '.cell-erase-tr', function (event, sel) {
        $(this).parent().parent().remove();

    });

    $(document).on('keyup', '.searchOnCode', function (event, sel) {
        var container=$(this).parent().parent().parent().parent().parent().data("index");
        $.ajax({
            type: 'POST',
            url:'ajax_resp.php',
            data:'keyword='+$(this).val()+'&contID='+container,
            beforeSend: function(){
                $(".line-"+container+" .loading-search.ls-onCODE").show();
            },
            success: function(data){
                $(".line-"+container+" .searchOnCode-box").show();
                $(".line-"+container+" .searchOnCode-box").html(data);
                $(".line-"+container+" .loading-search.ls-onCODE").hide();
            }
        });
    });
    $(document).on('blur', '.searchOnCode', function (event, sel) {
        var container=$(this).parent().parent().parent().parent().parent().data("index");
        setTimeout(function(){  $(".line-"+container+" .searchOnCode-box").hide();}, 1500);

    });
    $(document).on('keyup', '.searchOnT', function (event, sel) {
        var container=$(this).parent().parent().parent().parent().parent().data("index");
        $.ajax({
            type: 'POST',
            url:'ajax_resp.php',
            data:'keyT='+$(this).val()+'&contID='+container,
            beforeSend: function(){
                $(".line-"+container+" .loading-search.ls-onT").show();
            },
            success: function(data){
                $(".line-"+container+" .searchOnT-box").show();
                $(".line-"+container+" .searchOnT-box").html(data);
                $(".line-"+container+" .loading-search.ls-onT").hide();
            }
        });
    });
    $(document).on('keypress keyup blur', '.onlyNumber', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
    });
    $(document).on('blur', '.searchOnT', function (event, sel) {
        var container=$(this).parent().parent().parent().parent().parent().data("index");
        setTimeout(function(){  $(".line-"+container+" .searchOnT-box").hide(); }, 500);

    });
    $(document).on('keypress keyup blur', '.searchOnT', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
    });

    $(document).on('keypress keyup blur', '.write-off #manual input#numMat', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
        if(parseInt($(this).val())>0)
        {
            $(".write-off #manual input#numMat").attr('required',"required");
            $(".write-off #manual select#select_places").removeAttr('required');
        }
    });
    $(document).on('change', '.write-off #manual select#select_places', function (event, sel) {
        var recipient = parseInt($(this).find(':selected').val());
        if(!isNaN(recipient))
        {
            $(".write-off #manual input#numMat").removeAttr('required');
            $(".write-off #manual select#select_places").attr('required',"required");
        }
    });
    $(document).on('change', '.balance select[name="cells[]"]', function (event, sel) {
        var newCell = parseInt($(this).find(':selected').val());
        if(!isNaN(newCell))
        {
            var uid=$(this).data('uid');
            var place=$(this).data('place');
            var mid=$(this).data('mid');
            var trID=mid+'-'+place+'-'+uid;
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'newCell='+newCell+'&uid='+uid+'&place='+place+'&mid='+mid,
                beforeSend: function(){
                    $('<span style="color: red; font-size: 12px;" class="'+trID+'-write">Пишем...</span>').insertAfter('#'+trID+' select');
                },
                success: function(data){
                    $('.'+trID+'-write').remove();
                    $('<span style="color: green; font-size: 12px;" class="'+trID+'-apple">Записали...</span>').insertAfter('#'+trID+' select');
                    setTimeout(function(){$('.'+trID+'-apple').remove();}, 3000);
                }
            });
        }
    });
    $(document).on('change', '.coming select#select_places', function (event, sel) {
        var recipient = parseInt($(this).find(':selected').val());
        if(!isNaN(recipient))
        {
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'selectCells='+recipient,
                success: function(data){
                    $(".coming select[name='cells[]']").html(data);
                }
            });
        }
    });
    $(document).on('change', '.balance select#select_places', function (event, sel) {
        var recipient = parseInt($(this).find(':selected').val());
        if(!isNaN(recipient))
        {
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'selectCells='+recipient,
                success: function(data){
                    $(".balance select[name='cells']").html(data);
                }
            });
        }else{
            $(".balance select[name='cells']").html('<option value="" selected="">Выберите склад</option>');
        }
    });
/* ======================================================================================================================= */
    $(document).on('change', '#profiles select#profile_list', function (event, sel) {
        $('#glue_set').addClass('d-none');
        $('#glue_del').addClass('d-none');
        var select_profile = parseInt($(this).find(':selected').val());
        $(".forprofileDecor").html('');
        $(".forprofileSchetka").html('');
        $(".forprofileDovodchik").html('');
        $(".forprofileRolick").html('');
        $(".forprofileStopor").html('');
        $('#profiles #erase_decor').prop( "disabled", true );
        $('#profiles #edit_decor').prop( "disabled", true );
        if(!isNaN(select_profile))
        {
            $('#profiles #profile_edit').prop( "disabled", false );
            $('#profiles #erase_proflie').prop( "disabled", false );
            $('button#profile_edit').attr("data-targetid",select_profile);
            $('button#add_decor').attr("data-targetid",select_profile);
            $('button#add_decor').prop( "disabled", false );
            $('button#edit_decor').attr("data-targetid",select_profile);
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'profileDetails='+select_profile,
                success: function(data){
                    var resp_in =JSON.parse(data);
                    $(".forprofileDecor").html(resp_in.decors);
                    $(".forprofileDovodchik").html(resp_in.dovodchiks);
                    $(".forprofileRolick").html(resp_in.rolick);
                    $(".forprofileStopor").html(resp_in.stopor);
                }
            });
        }
        else
        {
            $('button#add_decor').attr("data-targetid",0);
            $('button#add_decor').prop( "disabled", true );
            $('button#edit_decor').attr("data-targetid",0);
            $('#profiles #profile_edit').prop( "disabled", true );
            $('#profiles #erase_proflie').prop( "disabled", true );
            $('button#profile_edit').attr("data-targetid",0);
        }
    });

    $(document).on('change', '#profiles select#profileDecors', function (event, sel) {
        $('#glue_set').addClass('d-none');
        $('#glue_del').addClass('d-none');
        $(".forprofileSchetka").html('');
        $('#profiles #erase_decor').prop( "disabled", true );
        $('#profiles #edit_decor').prop( "disabled", true );
        $('#profiles #edit_decor').attr("data-decortid",0);
        var tmp=0;
        var selectedDecor=0;
        $( "#profiles select#profileDecors option:selected" ).each(function() {
            selectedDecor=$( this ).val();
            tmp++;
        });
        if(tmp===1){
            var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
            // forprofileSchetka
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'profileIDforSchetka='+select_profile+'&selectedDecor='+selectedDecor,
                success: function(data){
                    var resp_in =JSON.parse(data);
                    if(resp_in.schetka.length===0)
                    {
                        $('#profiles #erase_decor').prop( "disabled", false );
                    }
                    $('#profiles #edit_decor').prop( "disabled", false );
                    $('#profiles #edit_decor').attr("data-decortid",selectedDecor);
                    $(".forprofileSchetka").html(resp_in.schetka);
                }
            });
        }else{

            $('.profileDecorsMess').html('<div class="alert alert-danger" role="alert">Выберите один декор!</div>');
            setTimeout(function(){  $('.profileDecorsMess').html('');}, 5000);
        }
        //     var select_profile = parseInt($('#profiles select#profileDecors').find(':selected').val());
        //     console.log(select_profile);
    });
    $(document).on('change', '#profiles select#profileSchetka', function (event, sel) {
        var tmp=0;
        var selectedSchetka=0;
        var selectedSchetkatext='';
        $('#glue_set').addClass('d-none');
        $('#glue_del').addClass('d-none');
        $( "#profiles select#profileSchetka option:selected" ).each(function() {
            selectedSchetka=$( this ).val();
            selectedSchetkatext= $( this ).text();
            tmp++;
        });
        if(tmp>1){

            $('.profileSchetkaMess').html('<div class="alert alert-danger" role="alert">Выберите одну щетку!</div>');
            setTimeout(function(){  $('.profileSchetkaMess').html('');}, 5000);
        }
        else
        {
            if(selectedSchetkatext.includes("[ Клеевая ]"))
            {
                $('#glue_del').removeClass('d-none');
            }else
            {
                $('#glue_set').removeClass('d-none');
            }
        }
    });
    $(document).on('click', '.glue-setting-schetka', function (event, sel) {
        var tmp=0;
        var selectedSchetka=0;
        var selectedSchetkatext='';
        var typebtn=$( this ).val();
        $( "#profiles select#profileSchetka option:selected" ).each(function() {
            selectedSchetka=$( this ).val();
            selectedSchetkatext= $( this ).text();
            tmp++;
        });
        if(tmp>1){

            $('.profileSchetkaMess').html('<div class="alert alert-danger" role="alert">Выберите одну щетку!</div>');
            setTimeout(function(){  $('.profileSchetkaMess').html('');}, 5000);
        }
        else
        {
            var selectedDecor=0;
            var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
            $( "#profiles select#profileDecors option:selected" ).each(function() {
                selectedDecor=$( this ).val();
            });
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'glueProfileID='+select_profile+'&glueDecor='+selectedDecor+'&glueSchetka='+selectedSchetka+'&op='+typebtn,
                success: function(data){
                    var resp_in =JSON.parse(data);
                    $('#glue_set').addClass('d-none');
                    $('#glue_del').addClass('d-none');
                    $(".forprofileSchetka").html(resp_in.schetka);
                }
            });
        }
        return false;
    });
    $(document).on('change', '#profiles select#profileDovodchik', function (event, sel) {
        var tmp=0;
        var selectedDovodchik=0;
        $( "#profiles select#profileDovodchik option:selected" ).each(function() {
            selectedDovodchik=$( this ).val();
            tmp++;
        });
        if(tmp>1){

            $('.profileDovodchikMess').html('<div class="alert alert-danger" role="alert">Выберите один доводчик!</div>');
            setTimeout(function(){  $('.profileDovodchikMess').html('');}, 5000);
        }
    });
    $(document).on('change', '#profiles select#profileRolick', function (event, sel) {
        var tmp=0;
        var selectedRolick=0;
        $( "#profiles select#profileRolick option:selected" ).each(function() {
            selectedRolick=$( this ).val();
            tmp++;
        });
        if(tmp>1){

            $('.profileRolickMess').html('<div class="alert alert-danger" role="alert">Выберите один ролик!</div>');
            setTimeout(function(){  $('.profileRolickMess').html('');}, 5000);
        }
    });
    $(document).on('change', '#profiles select#profileStopor', function (event, sel) {
        var tmp=0;
        var selectedStopor=0;
        $( "#profiles select#profileStopor option:selected" ).each(function() {
            selectedStopor=$( this ).val();
            tmp++;
        });
        if(tmp>1){

            $('.profileStoporkMess').html('<div class="alert alert-danger" role="alert">Выберите один стопор!</div>');
            setTimeout(function(){  $('.profileStoporkMess').html('');}, 5000);
        }
    });
    $(document).on('click', '#profiles #erase_proflie', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        if(!isNaN(select_profile) && select_profile>0)
        {

            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'erase_proflie='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles #profile_list').html(resp_in.opt_list);
                        $('#profiles .messages-profile').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');

                    }
                    else
                    {
                        $('#profiles .messages-profile').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                    setTimeout(function(){  $('#profiles .messages-profile').html('');}, 5000);
                }
            });
        }

        return false;
    });
    $(document).on('click', '#profiles #erase_decor', function (event, sel) {
        var select_decor = parseInt($('#profiles select#profileDecors').find(':selected').val());
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        if(select_decor>0)
        {
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'eraseDecor='+select_decor+'&onProfile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    console.log(resp_in);
                    if(resp_in.result) {
                        $('.forprofileDecor').html(resp_in.opt_list);
                        //    $(".forprofileDecor").html(resp_in.decors);
                        $('#profiles .messages-decor').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');

                    }
                    else
                    {
                        $('#profiles .messages-decor').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                    setTimeout(function(){  $('#profiles .messages-decor').html('');}, 5000);
                }
            });
        }
        return false;
    });

    $(document).on('click', '#profiles #add_schetka', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var select_schetka = parseInt($('input[name="schetka_val"]').val());
        var select_schetka_glue = 0;
        if($('#schetka_glue').prop('checked')) {
            select_schetka_glue = 1;
        }
        var tmp=0;
        var selectedDecor=0;
        $( "#profiles select#profileDecors option:selected" ).each(function() {
            selectedDecor=$( this ).val();
            tmp++;
        });
        if(tmp==1 && selectedDecor>0 && select_profile>0 && select_schetka>0){

            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'link_schetka='+select_schetka+'&decor='+selectedDecor+'&profile='+select_profile+'&glue='+select_schetka_glue,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-schetka').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileSchetka").html(resp_in.schetka);
                        $('#profiles #erase_decor').prop( "disabled", true );
                    }
                    else
                    {
                        $('#profiles .messages-schetka').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="schetka"]').val('');
            $('input[name="schetka_val"]').val('');
            $('#schetka_glue').prop('checked',0);
        }else{

            $('#profiles .messages-schetka').html('<div class="alert alert-danger" role="alert">Выберите профиль, один декор и щетку!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-schetka').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #remove_schetka', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var tmp=0;
        var selectedDecor=0;
        $( "#profiles select#profileDecors option:selected" ).each(function() {
            selectedDecor=$( this ).val();
            tmp++;
        });
        var tmp2=0;
        var selectedSchetka=0;
        $( "#profiles select#profileSchetka option:selected" ).each(function() {
            selectedSchetka=$( this ).val();
            tmp2++;
        });

        if(tmp==1 && tmp2==1 && selectedDecor>0 && select_profile>0 && selectedSchetka>0){

            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'unlink_schetka='+selectedSchetka+'&decor='+selectedDecor+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-schetka').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileSchetka").html(resp_in.schetka);
                        if(resp_in.schetka.length===0)
                        {
                            console.log('0');
                            $('#profiles #erase_decor').prop( "disabled", false );
                        }else
                        {
                            console.log('1');
                            $('#profiles #erase_decor').prop( "disabled", true );
                        }
                    }
                    else
                    {
                        $('#profiles .messages-schetka').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="schetka"]').val('');
            $('input[name="schetka_val"]').val('');

        }else{

            $('#profiles .messages-schetka').html('<div class="alert alert-danger" role="alert">Выберите профиль, один декор и одну щетку!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-schetka').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #add_dovodchik', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var select_dovodchik = parseInt($('input[name="dovodchik_val"]').val());

        if(select_profile>0 && select_dovodchik>0){

            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'link_dovodchik='+select_dovodchik+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-dovodchik').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileDovodchik").html(resp_in.dovodchik);
                    }
                    else
                    {
                        $('#profiles .messages-dovodchik').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="dovodchik"]').val('');
            $('input[name="dovodchik_val"]').val('');
        }else{

            $('#profiles .messages-dovodchik').html('<div class="alert alert-danger" role="alert">Выберите профиль, и доводчик!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-dovodchik').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #remove_dovodchik', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var tmp=0;
        var select_dovodchik=0;
        $( "#profiles select#profileDovodchik option:selected" ).each(function() {
            select_dovodchik=$( this ).val();
            tmp++;
        });
        if(tmp==1 && select_profile>0 && select_dovodchik>0){

            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'unlink_dovodchik='+select_dovodchik+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-dovodchik').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileDovodchik").html(resp_in.dovodchik);
                    }
                    else
                    {
                        $('#profiles .messages-dovodchik').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="dovodchik"]').val('');
            $('input[name="dovodchik_val"]').val('');
        }else{

            $('#profiles .messages-dovodchik').html('<div class="alert alert-danger" role="alert">Выберите профиль, и доводчик!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-dovodchik').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #add_rolick', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var select_rolick = parseInt($('input[name="rolick_val"]').val());

        if(select_profile>0 && select_rolick>0){
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'link_rolick='+select_rolick+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-rolick').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileRolick").html(resp_in.rolick);
                    }
                    else
                    {
                        $('#profiles .messages-rolick').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="rolick"]').val('');
            $('input[name="rolick_val"]').val('');
        }else{

            $('#profiles .messages-rolick').html('<div class="alert alert-danger" role="alert">Выберите профиль, и ролик!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-rolick').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #remove_rolick', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var tmp=0;
        var select_rolick=0;
        $( "#profiles select#profileRolick option:selected" ).each(function() {
            select_rolick=$( this ).val();
            tmp++;
        });
        if(tmp==1 && select_profile>0 && select_rolick>0){

            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'unlink_rolick='+select_rolick+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-rolick').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileRolick").html(resp_in.rolick);
                    }
                    else
                    {
                        $('#profiles .messages-rolick').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="rolick"]').val('');
            $('input[name="rolick_val"]').val('');
        }else{

            $('#profiles .messages-rolick').html('<div class="alert alert-danger" role="alert">Выберите профиль, и ролик!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-rolick').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #add_stopor', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var select_stopor = parseInt($('input[name="stopor_val"]').val());

        if(select_profile>0 && select_stopor>0){
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'link_stopor='+select_stopor+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-stopor').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileStopor").html(resp_in.stopor);
                    }
                    else
                    {
                        $('#profiles .messages-stopor').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="stopor"]').val('');
            $('input[name="stopor_val"]').val('');
        }else{

            $('#profiles .messages-stopor').html('<div class="alert alert-danger" role="alert">Выберите профиль, и стопор!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-stopor').html('');}, 5000);
        return false;
    });
    $(document).on('click', '#profiles #remove_stopor', function (event, sel) {
        var select_profile = parseInt($('#profiles select#profile_list').find(':selected').val());
        var tmp=0;
        var select_stopor=0;
        $( "#profiles select#profileStopor option:selected" ).each(function() {
            select_stopor=$( this ).val();
            tmp++;
        });

        if(tmp==1 && select_profile>0 && select_stopor>0){
            $.ajax({
                type: 'POST',
                url:'RS/ajax_resp.php',
                data:'unlink_stopor='+select_stopor+'&profile='+select_profile,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#profiles .messages-stopor').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                        $(".forprofileStopor").html(resp_in.stopor);
                    }
                    else
                    {
                        $('#profiles .messages-stopor').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                    }
                }
            });
            $('input[name="stopor"]').val('');
            $('input[name="stopor_val"]').val('');
        }else{

            $('#profiles .messages-stopor').html('<div class="alert alert-danger" role="alert">Выберите профиль, и стопор!</div>');
        }
        setTimeout(function(){  $('#profiles .messages-stopor').html('');}, 5000);
        return false;
    });

    $(document).on('click', '#profiles #save_limits', function (event, sel) {
        var out={};
        out['limits']= {};
        $( "#profiles .container-setting-profile input" ).each(function() {
            out['limits'][$( this ).attr('name')] = $( this ).val();
        });
        var outJS=JSON.stringify(out);
        $.ajax({
            type: 'POST',
            url: 'ajax_resp.php',
            data: {'data': outJS},
            success: function (resp) {
                var resp_in = JSON.parse(resp);
                if (resp_in.result) {
                    $('#profiles .messages-limits').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                } else {
                    $('#profiles .messages-limits').html('<div class="alert alert-danger" role="alert">Ошибка сохранения! Обратитесь к администратору!</div>');
                }
            }
        });
        setTimeout(function(){  $('#profiles .messages-limits').html('');}, 5000);
        return false;
    });

        /*
            $(document).on('click', '#profiles #save_limits', function (event, sel) {
                var cstrMaxDH = $('input#constructMaxDH').val();
                var cstrMaxDL = $('input#constructMaxDL').val();
                var cstrMinDH = $('input#constructMinDH').val();
                var cstrMinDL = $('input#constructMinDL').val();
                var cstrMaxWI = $('input#constructMaxWI').val();
                var cstrConSG = $('input#constructConSG').val();
                var cstrConSS = $('input#constructConSS').val();
                var cstrConGG = $('input#constructConGG').val();
                var cstrMaxSanH = $('input#constructMaxSanH').val();
                $.ajax({
                    type: 'POST',
                    url:'RS/ajax_resp.php',
                    data:'MaxDH='+cstrMaxDH+'&MaxDL='+cstrMaxDL+'&MinDL='+cstrMinDL+'&MinDH='+cstrMinDH+'&MaxWI='+cstrMaxWI+'&ConSG='+cstrConSG+'&ConSS='+cstrConSS+'&ConGG='+cstrConGG+'&MaxSanH='+cstrMaxSanH,
                    success:function(resp){
                        var resp_in =JSON.parse(resp);
                        if(resp_in.result) {
                            $('#profiles .messages-limits').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                            $(".forprofileStopor").html(resp_in.stopor);
                        }
                        else
                        {
                            $('#profiles .messages-limits').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                        }
                    }
                });
                setTimeout(function(){  $('#profiles .messages-limits').html('');}, 5000);
                return false;
            });


            */

    $(document).on('click', '#profiles #save_fillFactor', function (event, sel) {
        var fillFactorHeightPlate = $('input#fillFactorHeightPlate').val();
        var fillFactorWidthtPlate = $('input#fillFactorWidthtPlate').val();
        var fillFactorHeightGlass =  $('input#ffillFactorHeightGlass').val();
        var fillFactorWidthtGlass =  $('input#fillFactorWidthtGlass').val();
        $.ajax({
            type: 'POST',
            url:'ajax_resp.php',
            data:'ffHeightP='+fillFactorHeightPlate+'&ffWidthtP='+fillFactorWidthtPlate+'&ffHeightG='+fillFactorHeightGlass+'&ffWidthtG='+fillFactorWidthtGlass,
            success:function(resp){
                var resp_in =JSON.parse(resp);
                if(resp_in.result) {
                    $('#profiles .messages-limits').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');
                    $(".forprofileStopor").html(resp_in.stopor);
                }
                else
                {
                    $('#profiles .messages-limits').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');

                }
            }
        });
        setTimeout(function(){  $('#profiles .messages-limits').html('');}, 5000);
        return false;
    });

    $(document).on('click', '.managers .manager-admin', function (event, sel) {
        event.stopImmediatePropagation(); // off dubleclick
        var action=$(this).data('action');
        var id=$(this).data('id');
        console.log(action);
        console.log(id);
        console.log($(this));
        var out={};
        out['changeManagerStatus'] = action;
        out['changeManagerID'] = id;
         $.ajax({
            type: 'POST',
            url: 'ajax_resp.php',
            data: out,
            success: function (resp) {
                if(action=='off'){
                    $('.managers tr[data-lineid="'+id+'"] a[data-action="off"]').addClass('d-none');
                    $('.managers tr[data-lineid="'+id+'"] a[data-action="on"]').removeClass('d-none');
                }else
                {
                    $('.managers tr[data-lineid="'+id+'"] a[data-action="on"]').addClass('d-none');
                    $('.managers tr[data-lineid="'+id+'"] a[data-action="off"]').removeClass('d-none');
                }
            }
        });

        return false;
    });
    $(document).on('click', '#write-off-plan #saveWOP', function (event, sel) {
        event.stopImmediatePropagation(); // off dubleclick
        var errors_stack='';
        var rowID=$('#rowID').val();
        var out={};
        out['wof']= {};
        $( "#write-off-plan input[name ^='docID_']" ).each(function() {
            var key=parseInt($( this ).attr('name').substring(6));
            var wopIN=parseInt($('#wopIN_'+key).val());
            var wop=parseInt($('#wop_'+key).val());
            var docID=$( this ).val();
            if(wop>0 && wopIN<wop)
            {
                if(errors_stack.length>0) errors_stack=errors_stack+'<br>';
                errors_stack=errors_stack+'Списать больше чем есть - нельзя! Документ №'+docID;
            }
            else
            {
                if(wop>0)
                {
                out['wof'][key]={
                    'id':docID,
                    'old':wopIN,
                    'wop':wop
                };
                }
            }
        });
        if(errors_stack.length>0){
            $('#write-off-plan .messages-write-off-plan').html('<div class="alert alert-danger" role="alert">'+errors_stack+'</div>');
        }
        else {

            if (out['wof'][0]!= undefined)
            {
                out['matID'] = parseInt($('#matID').val());
                out['clCODE'] = parseInt($('#clCODE').val());
                out['place'] = parseInt($('#place').val());
                out['rowID'] = rowID;
                console.log(out);
                var outJS = JSON.stringify(out);
                $.ajax({
                    type: 'POST',
                    url: 'ajax_resp.php',
                    data: {'data': outJS},
                    success: function (resp) {
                        var resp_in = JSON.parse(resp);
                        console.log(resp_in);
                        if (resp_in.result) {
                            // $('#wopWindow').removeClass('show');
                            // $('.modal-backdrop.fade.show').removeClass('show');
                            $('#wopWindow button.close').click();
                            var newdata='';
                            if(resp_in.resVal>0) {
                                var newdata = '<button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#wopWindow" ' +
                                    ' data-clcode="' + out['clCODE'] + '" data-mcid="' + out['matID'] + '" data-place="' + out['place'] +
                                    '" data-rowID="' + out['rowID'] + '">(-' + resp_in.resVal + ')</button>';
                            }
                            $('tr#'+out['rowID']+' .wop-td button').replaceWith(newdata);
                            $('tr#'+out['rowID']).after('<tr class="tr-messages"><td colspan="10" align="right"><div class="alert alert-success" role="alert">' + resp_in.message + '</div></td></tr>');
                            setTimeout(function(){  $('tr.tr-messages').remove();}, 5000);
                        } else {
                            $('#profiles .messages-limits').html('<div class="alert alert-danger" role="alert">Ошибка сохранения! Обратитесь к администратору!</div>');
                        }
                    }
                });

            }
            // $('table.table.balance #'+rowID+' .wop-td').html('123')
        }
        setTimeout(function(){  $('#write-off-plan .messages-write-off-plan').html('');}, 5000);
    });

    $(document).on('click', '.forwop', function (event, sel) {
        $('#wopWindow').modal('show')
    });
    $(document).on('shown.bs.modal', '#wopWindow', function (event) {

        var button = $(event.relatedTarget);
        var clcode = button.data('clcode');
        var mcid = button.data('mcid');
        var place = button.data('place');
        var rowid = button.data('rowid');
        $(this).find(".modal-body-in").load("write_off_plan.php?matID="+mcid+"&clCODE="+clcode+"&place="+place+"&rowID="+rowid);

    });

    $(document).on('keyup', '#new_profile', function (event, sel) {profile_form_ready();});
    $(document).on('keyup', '#new_profile_wcr', function (event, sel) {profile_form_ready();});
    $(document).on('keyup', 'input#new_profile', function (event, sel) {
        profile_form_ready_copy();
    });
    $(document).on('keyup', 'input#new_profile_wcr', function (event, sel) {
        profile_form_ready_copy();
    });
    $(document).on('keyup', '.new-profile input', function (event, sel) {
        profile_form_ready_copy();
    });
    $(document).on('keyup', '.searchSimple', function (event, sel) {
        var pname=$(this).prop('name');
        $.ajax({
            type: 'POST',
            url:'RS/ajax_resp.php',
            data:'searchSimple='+$(this).val()+'&objectName='+pname,
            beforeSend: function(){
                $("."+pname+" input[name='"+pname+"_val']").val('');
                $("."+pname+" .loading-search.ls-onCODE").show();
                $("."+pname+" .loading-search").show();
            },
            success: function(data){
                $("."+pname+" .search-box").show();
                $("."+pname+" .search-box").html(data);
                $("."+pname+" .loading-search").hide();
                // profile_form_ready();
            }
        });
    });
    $(document).on('keyup', '.decors input', function (event, sel) {
        decors_form_ready_copy();
    });
    $(document).on('change', '.decors select', function (event, sel) {
        decors_form_ready_copy();
    });

    function decors_form_ready_copy() {
        if( $('input#decor_name').val().length>2 &&
            $('input#decor_color').val().length>4 &&
            parseInt($('select#vert').val())>0  &&
            parseInt($('select#hor_up').val())>0 &&
            parseInt($('select#hor_down').val())>0 &&
            parseInt($('select#napr_up').val())>0 &&
            parseInt($('select#napr_down').val())>0 &&
            parseInt($('select#connection').val())>0 )
        {
            $("button#saveDecors").prop( "disabled", false );
        }else
        {
            $("button#saveDecors").prop( "disabled", true );
        }
    }



    function profile_form_ready() {
        if( parseFloat($('input#width_l_sheet').val())>0 &&
            parseFloat($('input#height_w_sheet').val())>0 &&
            parseFloat($('input#wdth_l_glass').val())>0 &&
            parseFloat($('input#height_w_glass').val())>0 &&
            parseFloat($('input#koef_h_door').val())>0 &&
            parseFloat($('input#koef_l_hor_pofile').val())>0 &&
            $('input#vert').val().length>0 &&
            $('input#hor_up').val().length>0 &&
            $('input#hor_down').val().length>0 &&
            $('input#napr_up').val().length>0 &&
            $('input#napr_down').val().length>0 &&
            $('input#connection').val().length>0 &&
            $('input[name="newProfile"]').val().length>3 &&
            parseFloat($('input[name="newProfileWCR"]').val())>0 &&
            parseFloat($('input#max_door_height').val())>0 &&
            parseFloat($('input#max_door_length').val())>0 &&
            parseFloat($('input#max_wdth_in').val())>0 &&
            parseFloat($('input#con_sheet_glass').val())>0 &&
            parseFloat($('input#con_sheet_sheet').val())>0 &&
            parseFloat($('input#con_glass_glass').val())>0 &&
            parseFloat($('input#min_door_height').val())>0 &&
            parseFloat($('input#min_door_length').val())>0)
        {
            $("button#saveProfile").prop( "disabled", false );
        }else
        {
            $("button#saveProfile").prop( "disabled", true );
        }
    }
    /* ======================================================================================================================= */


function add_user_showhide() {
    var m=parseInt($('#new_mail').attr('data-valid'));
    var p=parseInt($('#new_phone').attr('data-valid'));
    if (m+p>0) {
        $(".input-name input").val('');
        $(".input-password input").val('');
        $(".input-name").show();
        $(".input-password").show();
        $(".add-user button").show();
    } else {
        $(".input-name input").val('');
        $(".input-password input").val('');
        $(".input-name").hide();
        $(".input-password").hide();
        $(".add-user button").hide();
    }
}

    $('#send_pin_code').on('click', async function (e) {
        e.preventDefault();
        var tel = $('#phone1').val();
        console.log(tel);
        if(tel)
        {
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'client_tel='+tel,
                success: function(data){
                    if(data == 1) {
                        $('#send_pin_group').hide();
                        $('#send_pin_code').hide();
                        $('#set_pin_code').show();
                    } else {
                        $('#client_auth_message').html(data);
                    }
                }
            });
        }
    });
    //.. Когда юзер ввёл пин-код и нажал Enter
    $(document).on('keypress', function(e) {
        // Не выполняем никаких действий (заглушка, нет времени реализовать нормальную обработку)
        if(e.which == 13 && $('#pin1').is(":focus")) {
            e.preventDefault();
        }
    });
    /*==========================================================
* // for a materials tab on the page of settings
* ==========================================================*/
// edits default values for material_general table
    $('#submitMatGeneral').on('click', function (event) {
        event.preventDefault();
        const inputs = $('#formMatGeneral').find('input');
        const divMessage = $('#messageMatGeneral');
        let request = '';
        for(let input of inputs) {
            request += $(input).attr('name') + "=";
            if ($(input).attr('type') === 'checkbox') {
                request += ($(input).prop('checked')) ? '1' : '0';
            } else {
                request += $(input).val();
            }
            if ($(input).attr('name') !== 'price_from_size') request += "&";
        }
        const preloader = $('#start_checks_lodaer_containers').removeClass('d-none');
        $.post(
            'ajax_materials.php',
            request,
            function (data) {
                const response = JSON.parse(data);
                if(response.error) {
                    divMessage.addClass('alert alert-danger').text(response.message);
                } else {
                    divMessage.addClass('alert alert-success').text(response.message);
                }
                setTimeout(function () {
                    divMessage.removeClass('alert alert-danger alert-success').text('');
                }, 2000);
                preloader.addClass('d-none');
            }
        );
    });

    /**
     *  Marks selected materials from materials table
     */
    function chooseMaterials() {
        if (!$(this).hasClass('clicked')) {
            $(this).addClass('clicked');
            $(this).css({'background': '#00000013'});
        } else {
            $(this).removeClass('clicked');
            $(this).css({'background': 'transparent'});
        }
        if ($('#tableMat table>tbody>tr').hasClass('clicked')) {
            $('#btnAddMat').removeClass('d-none');
            $('#chooseAll').removeClass('d-none');
        } else {
            $('#btnAddMat').addClass('d-none');
            $('#chooseAll').addClass('d-none');
        }

    }

// chooses all materials
    $('#chooseAll').on('click', (e) => {
        e.preventDefault();
        const $this = $(e.currentTarget);
        const materials = $('#tableMat table>tbody>tr');
        if (!$this.hasClass('click')) {
            $this.addClass('click');
            materials.css({'background': '#00000013'});
            materials.addClass('clicked');

        } else {
            $this.removeClass('click');
            materials.css({'background': 'transparent'});
            materials.removeClass('clicked');
            $this.addClass('d-none');
            $('#btnAddMat').addClass('d-none');
        }
    });

// loads default materials table
    $('#selling-principle-tab').on('click', ()=>{
        $('#tableMat').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $('#tablePlinth table').html('');
        $('#tableBand table').html('');
        $('#places option[value=1]').prop('selected', true);
        $('#salePrinciple option[value=material]').prop('selected', true);
        if (!$('#btnAddMat').hasClass('d-none')) {
            $('#btnAddMat').addClass('d-none');
            $('#chooseAll').addClass('d-none');
        }
        $.post(
            'ajax_materials.php',
            'load_table=true',
            function (data) {
                $('#tableMat').html(data);
                $('#countMat span').text($('#tableMat table tbody tr').length);
                $('#tableMat table>tbody>tr').on('click', chooseMaterials);
                $('.showMaterial').on('click', showMaterialInfo);
            }
        );
    });

    /**
     * Sends data the catalog tree and gets the filtered materials table
     * @param place_id Number it is the id's place
     * @param folder_id Number it is the id's folder of the catalog tree
     * @param good it is a flag then shows on a tree of the good (may be mat/band/plinth)
     * @param thisLi this li's element
     * */
    function sendDataForGetTableMat(place_id, folder_id, good, thisLi) {
        if (good === "band") {
            $('#tableBand').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
            $.post(
                'ajax_band.php',
                {
                    folder_id: folder_id,
                    filter_band: true,
                },
                function (data) {
                    loadTable(data, $('#tableBand'));
                }
            );
        } else if (good === 'plinth') {
            $('#tablePlinth').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
            $.post(
                'ajax_plinth.php',
                {
                    folder_id: folder_id,
                    parent: thisLi.data('parent'),
                    filter_plinth: true,
                },
                function (data) {
                    loadTable(data, $('#tablePlinth'), 'Plinth');
                }
            );
        } else {
            $('#tableMat').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
            $('#btnAddMat').addClass('d-none');
            $('#chooseAll').addClass('d-none');

            $.post(
                "ajax_materials.php",
                {
                    place_id: place_id,
                    folder_id: folder_id,
                    salePrinciple: $('#salePrinciple').val(),
                },
                function (data) {
                    const response = JSON.parse(data);
                    if (response.error) {
                        $('#tableMat').html('<div class="alert alert-warning text-center">' + response.result + '</div>');
                        $('#countMat span').text("0");
                    } else {
                        $('#tableMat').html(response.result);
                        $('#countMat span').text($('#tableMat table tbody tr').length);
                        $('#tableMat table>tbody>tr').on('click', chooseMaterials);
                        $('.showMaterial').on('click', showMaterialInfo);
                    }
                }
            );
        }
    }
    /**
     * gets materials table by selling principal
     * @param event Object
     * */
    function getMaterialsByPrinciple(event) {
        const $this = $(event.target);
        const divTable = $('#tableMat');
        const otherSelect = ($this.attr('name') === 'place_id') ? $('#salePrinciple') : $('#places');
        divTable.html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $('#btnAddMat').addClass('d-none');
        $('#chooseAll').addClass('d-none');
        $.post(
            'ajax_materials.php',
            otherSelect.attr('name') + '=' + otherSelect.val() + '&' + $this.attr('name') + '=' + $this.val() + '&only=true',
            function (data) {
                if ($('#collapseFilter').hasClass('show')) $('#btnOpenFilter').click();
                const response = JSON.parse(data);
                if(response.error) {
                    divTable.html('<div class="alert alert-warning text-center">' + response.result + '</div>');
                    $('#countMat span').text("0");
                } else {
                    $('#tableMat').html(response.result);
                    $('#countMat span').text($('#tableMat table tbody tr').length);
                    $('#tableMat table>tbody>tr').on('click', chooseMaterials);
                    $('.showMaterial').on('click', showMaterialInfo);
                }
            }
        );
    }
    $('#places').on('change', getMaterialsByPrinciple);

// clears inputs of the filter
    $('#salePrinciple').on('change', getMaterialsByPrinciple);
    $('#btnOpenFilter').on('click', () => {
        $('#matFolder').text('');
        $('#filterCode').val('');
        $('#filterHar').val('');
    });

// gets the table of materials then works a filter by code or by characteristic
    function getMaterialTable(event) {
        const divTable = $('#tableMat');
        divTable.html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $('#btnAddMat').addClass('d-none');
        $('#chooseAll').addClass('d-none');
        const $this = $(event.target);
        const place_id = $('#places').val();
        const salePrinciple = $('#salePrinciple');
        const key = $this.attr('name');
        let request = 'place_id=' + place_id + '&' + key + '=' + $this.val() + '&' + salePrinciple.attr('name') + '=' + salePrinciple.val();
        $.post(
            'ajax_materials.php',
            request,
            function(data) {
                const response = JSON.parse(data);
                if(response.error) {
                    divTable.html('<div class="alert alert-warning text-center">' + response.result + '</div>');
                    $('#countMat span').text("0");
                } else {
                    $('#tableMat').html(response.result);
                    $('#countMat span').text($('#tableMat table tbody tr').length);
                    $('#tableMat table>tbody>tr').on('click', chooseMaterials);
                    $('.showMaterial').on('click', showMaterialInfo);
                }
            }
        );
    }
    $('#filterCode').on('input', getMaterialTable);
    $('#filterHar').on('input', getMaterialTable);

// shows inner folders of the clicked folder
    function showTreeChildren(event) {
        event.stopImmediatePropagation();

        const li = $(event.target);
        const childrenLi = li.children().children()
        if (childrenLi.hasClass('d-none')) {
            li.find('i.far:first').removeClass('fa-folder').addClass('fa-folder-open');
            childrenLi.removeClass('d-none');
        } else {
            li.find('i.far:first').removeClass('fa-folder-open').addClass('fa-folder');
            childrenLi.addClass('d-none');
        }
        if (li.children().hasClass('fas')) {
            if (li.data('good') === 'band') {
                $('#bandFolder').text(li.text());
                $('#bandFilterCode').val('');
                $('#bandFilterName').val('');
                $('#btnOpenTreeBand').click();
            } else if (li.data('good') === 'plinth') {
                $('#plinthFolder').text(li.text());
                $('#plinthFilterCode').val('');
                $('#plinthFilterName').val('');
                $('#btnOpenTreePlinth').click();
            } else {
                $('#matFolder').text(li.text());
                $('#btnOpenTree').click();
            }
            const id = li.attr('id').substring(li.attr('id').indexOf('_') + 1);
            let place_id = $('#places').val();
            sendDataForGetTableMat(place_id, id, li.data('good'), li);
        }
    }

// loads list of the catalog's tree
    $('#btnOpenTree').on('click', (e) => {
        if ($('#matTree ul').length === 0) {
            const matTree = $('#matTree');
            $.post(
                'ajax_materials.php',
                'load_tree=true',
                function (data) {
                    matTree.html(data);
                    $('#matTree li.list-group-item').on('click', showTreeChildren);
                }
            );
        }
    });

// loads a filtered table by properties
    $('#btnProperties').on('click', function(event) {
        event.preventDefault();
        const divTable = $('#tableMat');
        divTable.html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $('#btnAddMat').addClass('d-none');
        $('#chooseAll').addClass('d-none');
        const place_id = $('#places').val();
        const salePrinciple = $('#salePrinciple');
        $('#modalPropertiesFilter').modal('hide');
        const properties = [];
        for (let data of $('select.changed')) {
            let select = $(data);
            select.removeClass('changed');
            properties.push({
                type: select.data('typeval'),
                value: select.find('option:selected').text(),
                prop_id: select.prop('selected', true).val(),
            });
            select.find('option:selected').prop('selected', false);
        }
        const jsonProperties = JSON.stringify(properties);
        const request = 'place_id=' + place_id + "&" + salePrinciple.attr('name') + "=" +
            salePrinciple.val() + "&properties=" + jsonProperties;
        $.post(
            'ajax_materials.php',
            request,
            function (data) {
                const response = JSON.parse(data);
                if(response.error) {
                    divTable.html('<div class="alert alert-warning text-center">' + response.result + '</div>');
                    $('#countMat span').text("0");
                } else {
                    $('#tableMat').html(response.result);
                    $('#countMat span').text($('#tableMat table tbody tr').length);
                    $('#tableMat table>tbody>tr').on('click', chooseMaterials);
                    $('.showMaterial').on('click', showMaterialInfo);
                    $('#matFolder').text('');
                    $('#filterHar').val("");
                    $('#filterCode').val("");
                }
            }
        );
    });

// Shows a information of the selected material
    function showMaterialInfo(event) {
        event.preventDefault();
        event.stopPropagation();
        $(event.currentTarget).closest('tr').addClass('clicked');// ???
        const modal = $('#modalAddMat').modal('show');
        let modalTitle = modal.find('.modal-title').text();
        modal.find('.modal-title').text('Информация о материале');
        let matTitle = modal.find('.modal-body .text-center>p').text();
        modal.find('.modal-body .text-center>p').text('Выбранный материал');
        const listMat = $('#listChooseMat').html('');
        listMat.text($(event.currentTarget).closest('tr').find('td').eq(1).text());
        modal.find('#btnConfAddMat').text('Изменить');
        let principle = $("#salePrinciple").val();

        let data_placeId;
        if (principle !== 'material') {
            data_placeId = String ($(event.currentTarget).closest('tr').find('td.' + principle).data('placeid'));
            if (data_placeId !== undefined) {
                let placesId = (data_placeId.indexOf(',') < 0)? [data_placeId] : data_placeId.split(',');
                $('#modalSelectPlaces option').each(function () {
                    for (let id of placesId) {
                        if ($(this).val() === id) {
                            $(this).prop('selected', true);
                        }
                    }
                });
            }
        }
        let optionsSP = $('#modalSalePrincipal option');
        switch (principle) {
            case "material":
                optionsSP.eq(0).prop('selected', true);
                break;
            case "half":
                optionsSP.eq(1).prop('selected', true);
                break;
            case "m2":
                optionsSP.eq(2).prop('selected', true);
                break;
            case "m2_diff":
                optionsSP.eq(3).prop('selected', true);
                break;

        }
        if ($('#modalSalePrincipal').val() === 'm2_diff') {
            $('#forM2Diff').removeClass('d-none');
            $('#m2DiffTbody tr').not('#m2DiffRow1').remove();
            $.post(
                'ajax_materials.php',
                {
                    m2_diff_mat_id: $(event.currentTarget).closest('tr').find('th').text(),
                    m2_diff_place_id: $('#places').val(),
                    get_m2_diff: true,
                },
                function (data) {
                    let response = JSON.parse(data);
                    if (!response.error) {
                        let rows = JSON.parse(response.result);
                        if (rows.length >= 1) {
                            let row1 = $('#m2DiffRow1');
                            row1.find('td').eq(0).find('input').val(rows[0].length);
                            row1.find('td').eq(1).find('input').val(rows[0].width);
                            row1.find('td').eq(2).find('input').val(rows[0].area);
                            row1.find('td').eq(3).find('input').val(rows[0]['up_%']);
                            row1.find('td').eq(4).find('input').val(rows[0].up_uah);
                            row1.find('td').each(function () {
                                if ($(this).find('input').val() === "") {
                                    $(this).find('input').attr('disabled', true);
                                } else {
                                    $(this).find('input').attr('disabled', false);
                                }
                            });
                            for (let i = 1; i < rows.length; i++) {
                                let rowClone = $('#m2DiffTbody tr:last').clone();
                                let num = i + 1;
                                rowClone.attr('id', 'm2DiffRow' + num);
                                rowClone.find('td').eq(0).find('input').val(rows[i].length);
                                rowClone.find('td').eq(1).find('input').val(rows[i].width);
                                rowClone.find('td').eq(2).find('input').val(rows[i].area);
                                rowClone.find('td').eq(3).find('input').val(rows[i]['up_%']);
                                rowClone.find('td').eq(4).find('input').val(rows[i].up_uah);

                                rowClone.find('td').each(function () {
                                    if ($(this).find('input').val() === "") {
                                        $(this).find('input').attr('disabled', true);
                                    } else {
                                        $(this).find('input').attr('disabled', false);
                                    }
                                });
                                $('#m2DiffTbody').append(rowClone);
                                $(rowClone).find('td input[name=length]').on('input', calcArea);
                                $(rowClone).find('td input[name=width]').on('input', calcArea);
                                $(rowClone).find('td input[name=up_percent]').on('input', blockMarkupInputs);
                                $(rowClone).find('td input[name=up_uah]').on('input', blockMarkupInputs);
                                $(rowClone).find('td input[name=length]').on('input', blockInputForSize);
                                $(rowClone).find('td input[name=width]').on('input', blockInputForSize);
                                $(rowClone).find('td input[name=area]').on('input', blockInputForSize);
                            }

                        }
                    } else {
                        $('#messageM2Diff').addClass('alert alert-danger').text('Ошибка сервера!');

                    }
                }
            );
        }
        let clearModal = function () {
            $(event.currentTarget).closest('tr').removeClass('clicked');
            modal.find('.modal-title').text(modalTitle);
            modal.find('.modal-body .text-center>p').text(matTitle);
            modal.find('#btnConfAddMat').text('Установить');
            $('#modalSelectPlaces option').each(function () {
                $(this).prop('selected', false);
            });
            $('#modalSalePrincipal option').each(function () {
                $(this).prop('selected', false);
            });
            $('#m2DiffRow1 td').each(function () {
                $(this).find('input').val('');
                $(this).find('input').attr('disabled', false);
            });
            $('#m2DiffTbody tr').not('#m2DiffRow1').each(function () {
                $(this).remove();
            });
            if (!$('#forM2Diff').hasClass('d-none')) $('#forM2Diff').addClass('d-none');
        }
        modal.find('.close').on('click', clearModal);
        modal.find('#modalAddMatClose').on('click', clearModal);
    }

// gets options for the property.
    $('.listProperties>tbody>tr>td>select').on('click', function (event) {
        const select = $(event.currentTarget);
        if (select.find('option').length === 1) {
            const type = select.data('typeval');
            let url;
            switch (select.data('good')) {
                case 'band':
                    url = 'ajax_band.php';
                    break;
                case 'plinth':
                    url = 'ajax_plinth.php';
                    break;
                default:
                    url = 'ajax_materials.php';
            }

            const prop_name = select.parents('tr').find('td:first').text();
            $.post(
                url,
                {
                    type: type,
                    prop_name: prop_name,
                },
                function (data) {
                    const response = JSON.parse(data);
                    select.append(response.result);
                }
            );
        }
    });

// marks the selects then was changed
    $('.listProperties>tbody>tr>td>select').on('change', (e)=> {
        $(e.currentTarget).addClass('changed');
    });

// adds a list of selected materials for changing of a selling principle
    $('#btnAddMat').on('click', function(event) {
        event.preventDefault();
        const list = $('#listChooseMat').html('');

        for(let item of $('#tableMat table>tbody>tr.clicked')) {
            let li = "<li class='list-group-item' data-matid='" + $(item).find('th').text() + "'>" + $(item).find('td').eq(1).text() + "</li>";
            list.append(li);
        }

        const selectedPrincipal = $('#salePrinciple').val();
        $('#modalSalePrincipal option').each(function () {
            if ($(this).val() === selectedPrincipal) {
                $(this).prop('selected', true);
            }
        });
        if (selectedPrincipal !== 'm2_diff') {
            $('#forM2Diff').addClass('d-none');
        } else {
            $('#forM2Diff').removeClass('d-none');
        }
        $("#m2DiffTbody tr").not('tr:first').remove();
        $('#m2DiffRow1 td').each(function () {
            let input = $(this).find('input');
            input.val('');
            input.attr('disabled', false);
        });

    });

// rewrites data of the selling principle
    $('#btnConfAddMat').on('click', (e) => {
        e.preventDefault();
        let materials = [];

        const places = [];
        $('#modalSelectPlaces option:selected').each(function () {
            places.push($(this).val());
        });

        for(let item of $('#tableMat table>tbody>tr.clicked')) {
            let matId = $(item).find('th').text();
            let half = $(item).find('td.half').data('placeid');
            let m2 = $(item).find('td.m2').data('placeid');
            let m2_diff = $(item).find('td.m2_diff').data('placeid');
            let dataDelete = {
                half: '',
                m2: '',
                m2_diff: '',
            };
            for(let place of places) {
                if (half !== undefined) {
                    if (String(half).indexOf(place) != -1) dataDelete.half += place + ',';
                }
                if (m2 !== undefined) {
                    if (String(m2).indexOf(place) != -1) dataDelete.m2 += place + ',';
                }
                if (m2_diff !== undefined) {
                    if (String(m2_diff).indexOf(place) != -1) dataDelete.m2_diff += place + ',';
                }
            }
            dataDelete.half = (dataDelete.half !== "") ? dataDelete.half.slice(0, -1) : "";
            dataDelete.m2 = (dataDelete.m2 !== "") ? dataDelete.m2.slice(0, -1) : "";
            dataDelete.m2_diff = (dataDelete.m2_diff !== "") ? dataDelete.m2_diff.slice(0, -1) : "";
            materials.push({
                mat_id: matId,
                delete_from: dataDelete,
            });
        }
        const select = $('#modalSelectPlaces');
        if (places.length > 0) {
            select.removeClass('is-invalid');
            select.parent().find('.alert').remove();
            const sellingPrinciple = $('#modalSalePrincipal').val();
            let data = {
                materials: JSON.stringify(materials),
                places: JSON.stringify(places),
                selling_principle: $('#modalSalePrincipal').val(),
                update_selling: true,
            };
            let allow = true;
            if (sellingPrinciple === "m2_diff") {
                let dataM2Diff = [];
                const trs = $('#m2DiffTbody tr');
                trs.each(function () {
                    let $this = $(this);
                    let dataTds = {
                        length: $this.find('td input[name=length]').val(),
                        width: $this.find('td input[name=width]').val(),
                        area: $this.find('td input[name=area]').val(),
                        up_percent: $this.find('td input[name=up_percent]').val(),
                        up_uah: $this.find('td input[name=up_uah]').val(),
                    };
                    dataM2Diff.push(dataTds);
                });
                data.m2_diff = JSON.stringify(dataM2Diff);
                allow = validateM4DiffPercentOrUah(trs.not(trs.last()), trs.last());
            }
            if(allow) {
                $.post(
                    'ajax_materials.php',
                    data,
                    function (data) {
                        const response = JSON.parse(data);
                        const divTable = $('#tableMat').html('');
                        if (response.message_ins) divTable.append("<div class='alert alert-success'>Данные успешно презаписанны</div>");
                        divTable.append(response.result);
                        $('#countMat span').text($('#tableMat table tbody tr').length);
                        $('#modalAddMat').modal('hide');
                        $('#btnAddMat').addClass('d-none');
                        $('#chooseAll').addClass('d-none');
                        setTimeout(() => {
                            divTable.find('.alert').remove();
                        }, 2000);
                        $('#salePrinciple option').each(function () {
                            if ($(this).val() === $('#modalSalePrincipal').val()) $(this).prop('selected', true);
                        });
                        $('#tableMat table>tbody>tr').on('click', chooseMaterials);
                        $('.showMaterial').on('click', showMaterialInfo);
                    }
                );
            }
        } else {
            select.addClass('is-invalid');
            select.parent().append('<div class="alert alert-danger">Нужно выбрать филиал!</div>');
        }
    });

// shows added fields of entering data for selling m2 until a size of the part
    $('#modalSalePrincipal').on('change', (e) => {
        if ($(e.target).val() === 'm2_diff') {
            $('#forM2Diff').removeClass('d-none');
        } else {
            $('#forM2Diff').addClass('d-none');
        }
    });

// adds a new row of entering data for selling m2 until a size of the part
    $('#m2DiffAddInputs').on('click', (e) => {
        e.preventDefault();
        const tbody = $('#m2DiffTbody');
        const tr = tbody.find('tr');
        if (validateM4DiffPercentOrUah(tr.not(tr.last()), tr.last())) {
            const clone = $('#m2DiffRow1').clone();
            let rowNum = Number(tbody.find('tr').length) + 1;
            let rowId = 'm2DiffRow' + rowNum;
            clone.attr('id', rowId);
            clone.find('td').each(function () {
                $(this).find('input').val('');
                $(this).find('input').attr('disabled', false);
            });
            tbody.append(clone);
            $(clone).find('td input[name=length]').on('input', calcArea);
            $(clone).find('td input[name=width]').on('input', calcArea);
            $(clone).find('td input[name=up_percent]').on('input', blockMarkupInputs);
            $(clone).find('td input[name=up_uah]').on('input', blockMarkupInputs);
            $(clone).find('td input[name=length]').on('input', blockInputForSize);
            $(clone).find('td input[name=width]').on('input', blockInputForSize);
            $(clone).find('td input[name=area]').on('input', blockInputForSize);
        }
    });

    /**
     * Validates fields for entering params of the good markups
     * @param trs Collection html elements of the table without a last html tr's element
     * @param trCurrent it is a last html tr's element of the table
     * @return allow bool false | true
     * */
    function validateM4DiffPercentOrUah(trs, trCurrent) {
        let allow = true;
        if (trCurrent.find('td input[name=area]').val() === "" &&
            (trCurrent.find('td input[name=up_percent]').val() === "" || trCurrent.find('td input[name=up_uah]').val() === "")) {
            printMessageForM2Diff('Нужно ввести площадь и дополнительную наценка в % или в грн чтобы добавить способ наценки.');
            return false;
        }
        if (trs.length == 0) {
            return allow;
        } else {
            let text = "";
            let trLast = trs.last();
            if (trLast.find('td input[name=area]').val() === trCurrent.find('td input[name=area]').val()) {
                text = "Наценка с такой площадью уже есте";
                printMessageForM2Diff(text);
                allow = false;
                return allow;
            }
        }
        return allow;
    }

    /**
     * Displays a block with a validation error message
     * @param text String - a text of the message
     * */
    function printMessageForM2Diff(text) {
        $("#messageM2Diff").addClass('alert alert-danger').text(text);
        setTimeout(() => {
            $("#messageM2Diff").removeClass('alert alert-danger').text('');
        }, 3000);
    }

    /**
     * Сalculate the area of the part for the allowance
     * @param e Object - event
     * */
    function calcArea(e) {
        const $this = $(e.target);
        const other = ($this.attr('name') === 'length') ? $this.closest('tr').find('td input[name=width]') : $this.closest('tr').find('input[name=length]');
        const area = $this.closest('tr').find('td>input[name=area]');
        area.val(($this.val() * other.val())/1000000);
    }
    $('#m2DiffRow1>td>input[name=length]').on('input', calcArea);
    $('#m2DiffRow1>td>input[name=width]').on('input', calcArea);

    /**
     * Blocks a closest input if it is active
     * @param e Object - event
     */
    function blockMarkupInputs(e) {
        const $this = $(e.target);
        const other = ($this.attr('name') === 'up_percent') ? $this.closest('tr').find("td input[name=up_uah]") : $this.closest('tr').find("td input[name=up_percent]");
        if ($this.val() !== "") {
            other.attr('disabled', true);
        } else {
            other.attr('disabled', false);
        }
    }
    $("#m2DiffRow1>td>input[name=up_percent]").on('input', blockMarkupInputs);
    $('#m2DiffRow1>td>input[name=up_uah]').on('input', blockMarkupInputs);

    /**
     * Blocks inputs depending on their input,
     * if length and height are entered, the area is blocked,
     * otherwise length and height are blocked
     * @param e Object - event
     * */
    function blockInputForSize(e) {
        const $this = $(e.target);
        let other1, other2;
        switch ($this.attr('name')) {
            case 'length':
                other1 = $this.closest('tr').find('td input[name=width]');
                other2 = $this.closest('tr').find('td input[name=area]');
                break;
            case 'width':
                other1 = $this.closest('tr').find('td input[name=length]');
                other2 = $this.closest('tr').find('td input[name=area]');
                break;
            case 'area':
                other1 = $this.closest('tr').find('td input[name=length]');
                other2 = $this.closest('tr').find('td input[name=width]');
                break;
        }

        if ($this.attr('name') === 'length' || $this.attr('name') === "width") {
            if ($this.val() !== "" || other1.val() !== "") {
                other2.attr('disabled', true);
            } else {
                other2.attr('disabled', false);
            }
        } else if ($this.attr('name') === 'area') {
            if ($this.val() !== "") {
                other1.attr('disabled', true);
                other2.attr('disabled', true);
            } else {
                other1.attr('disabled', false);
                other2.attr('disabled', false);
            }
        }
    }
    $("#m2DiffRow1>td>input[name=length]").on('input', blockInputForSize);
    $("#m2DiffRow1>td>input[name=width]").on('input', blockInputForSize);
    $("#m2DiffRow1>td>input[name=area]").on('input', blockInputForSize);

    $('#nav-materials-tab').on('click', () => {
        $('#mat-default-tab').click();
    });
    /*=============================
    * End settings materials
    * =============================*/

    /*=============================
    * settings band
    * =============================*/

    // загружает список папок каталога дерева товаров
    $('#btnOpenTreeBand').on('click', function(e) {
        if ($('#bandTree ul').length === 0) {
            const bandTree = $('#bandTree');
            $.post(
                'ajax_band.php',
                'load_tree=true',
                function (data) {
                    bandTree.html(data);
                    $('#bandTree li.list-group-item').on('click', showTreeChildren);
                }
            );
        }
    });

    // загружает таблицу кромки по умолчанию
    $('#nav-band-tab').on('click', (e) => {
        $('#tableMat table').html('');
        $('#tablePlinth table').html('');
        $('#countMat span').text('0');
        const divTableBand = $('#tableBand').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $.post(
            'ajax_band.php',
            'load_table=true',
            function (data) {
                loadTable(data, divTableBand);
            }
        );
    });

    // загружает отфильтрованную таблицу кромки по свойствам
    $('#btnPropertiesBand').on('click', (e) => {
        e.preventDefault();
        const divTableBand = $('#tableBand').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        let properties = [];
        for (let data of $('#modalPropertiesFilterBand').find('select.changed')) {
            let select = $(data);
            select.removeClass('changed');
            properties.push({
                type: select.data('typeval'),
                value: select.find('option:selected').text(),
                prop_id: select.prop('selected', true).val(),
            });
            select.find('option:selected').prop('selected', false);
        }
        let propJson = JSON.stringify(properties);
        $('#modalPropertiesFilterBand').modal('hide');
        $.post(
            "ajax_band.php",
            {
                properties: propJson,
                filter_band: true,
            },
            function (data) {
                loadTable(data, divTableBand);
                $('#bandFolder').text('');
                $('#bandFilterCode').val('');
                $('#bandFilterName').val('');
            }
        );
    });

    function getFilteredBandTable(event) {
        const divTableBand = $('#tableBand').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        let $this = $(event.currentTarget);
        let data;
        if ($this.attr('name') === 'code') {
            data = {code: $this.val()};
            $('#bandFilterName').val('');
        } else  {
            data = {name: $this.val()};
            $('#bandFilterCode').val('');
        }
        $('#bandFolder').text('');
        data.filter_band = true;
        $.post(
            'ajax_band.php',
            data,
            function (data) {
                loadTable(data, divTableBand);
            }
        );
    }
    $('#bandFilterCode').on('change', getFilteredBandTable);
    $('#bandFilterName').on('change', getFilteredBandTable);

    // Показывает инфориацию выбранной кромки
    $('.showBand').on('click', (e) => {
        e.preventDefault();
        $('#modalAddBand').modal('show');
    });

    /**
     * Loads bands table if in response is it
     * @param data it is a json response from server
     * @param divTableBand it is a block for insert bands table
     * */
    function loadTable(data, divTable, good = 'Band') {

        let response = JSON.parse(data);
        if (response.error) {
            $('#quantity' + good + 's span').text(0);
            divTable.html('<div class="alert alert-warning">'+ response.message +'</div>');
        } else {
            divTable.html(response.result);
            $('#quantity' + good + 's span').text($('#table' + good + ' table>tbody>tr').length);
        }
        if ($('#table' + good + ' table>tbody>tr').hasClass('clicked')) {
            $('#btnAdd' + good).removeClass('d-none');
            $('#chooseAll' + good).removeClass('d-none');
        } else {
            $('#btnAdd' + good).addClass('d-none');
            $('#chooseAll' + good).addClass('d-none');
        }
        $('.show' + good).on('click', (e) => {
            $('#btnAddMatTo' + good + 's').addClass('d-none');
            e.preventDefault();
            e.stopPropagation();
            const modal = $('#modalAdd' + good).modal('show');
            const $this = $(e.target);
            let goodSm = good.toLowerCase();
            let modalTitle = modal.find('.modal-title');
            modalTitle.text($this.closest('tr').find('td').eq(1).text());
            modalTitle.data( goodSm + 'id', $this.closest('tr').find('th').eq(0).text());
            $('#listChoose' + good).html('');
            if (good == 'Band') cleanModalAddBand();
            if (good == 'Plinth') cleanModalAddPlinth();
            $('#' + goodSm + 'MaterialsTable>tbody').html('');
            if ($this.closest('tr').find('td').eq(2).data('matids') !== undefined) {
                if (good == "Band") bandGetListOfMaterials($this);
                if (good == "Plinth") plinthGetListOfMaterials($this);
            }
        });
        if (good == 'Band') divTable.find('tbody tr').on('click', chooseBands);
        if (good == 'Plinth') divTable.find('tbody tr').on('click', choosePlinths);
    }
    /**
     * sends request for getting list of materials
     * */
    function bandGetListOfMaterials($this) {
        const divBandMatTable = $('#bandMaterialsTable>tbody');
        $.post(
            'ajax_band.php',
            {
                load_band_materials: true,
                band_id: $this.closest('tr').find('th').text(),
            },
            function (data) {
                let response = JSON.parse(data);
                if (response.error) {
                    divBandMatTable.html('Ошибка сервера');
                } else {
                    divBandMatTable.html(response.result);
                    $('.drop-band-mat').on('click', deleteRelationMatGood);
                    if ($('#bandMaterialsTable tbody tr').length > 0) {
                        $('#btnAddMatToBands').removeClass('d-none');
                    } else {
                        $('#btnAddMatToBands').addClass('d-none');
                    }
                }
            }
        );
    }
    /**
     *  Marks selected bands from bands table
     */
    function chooseBands() {
        if (!$(this).hasClass('clicked')) {
            $(this).addClass('clicked');
            $(this).css({'background': '#00000013'});
        } else {
            $(this).removeClass('clicked');
            $(this).css({'background': 'transparent'});
        }
        if ($('#tableBand table>tbody>tr').hasClass('clicked')) {
            $('#btnAddBand').removeClass('d-none');
            $('#chooseAllBand').removeClass('d-none');
        } else {
            $('#btnAddBand').addClass('d-none');
            $('#chooseAllBand').addClass('d-none');
        }
    }

    // chooses all materials
    $('#chooseAllBand').on('click', (e) => {
        e.preventDefault();
        const $this = $(e.currentTarget);
        const bands = $('#tableBand table>tbody>tr');
        if (!$this.hasClass('click')) {
            $this.addClass('click');
            bands.css({'background': '#00000013'});
            bands.addClass('clicked');
        } else {
            $this.removeClass('click');
            bands.css({'background': 'transparent'});
            bands.removeClass('clicked');
            $this.addClass('d-none');
            $('#btnAddBand').addClass('d-none');
        }
    });
    /**
     * loads list of materials if its is
     * */
    $('#sendFilterMat').on('click', (e) => {
        e.preventDefault();
        let matList = $('#collapseMatList').html('');
        $.post(
            'ajax_band.php',
            {filter_mat: $('input[name=filter_mat]').val()},
            function (data) {
                let response = JSON.parse(data);
                if (!response.error) {
                    matList.html(response.result);
                    matList.prev().find('span.count-text').html(response.message);
                    $('#collapseMatList').find('li').on('click', activeItemCollapseMatList);
                    $('.add-relationship-mat').on('click', addMaterialToTable);
                } else {
                    matList.html("<div class='alert alert-warning'>" + response.message + "</div>");
                }
            }
        );
    });

    // marks li and adds a button for adding to the tabla of relationships
    function activeItemCollapseMatList(e) {
        let $this = $(e.currentTarget);
        if (!$this.hasClass('clicked')) {
            $this.addClass('clicked');
            $this.css({'background-color': '#e9ecef'});
            $this.find('button.add-relationship-mat').removeClass('d-none');
        } else {
            $this.removeClass('clicked');
            $this.css({'background-color': 'transparent'});
            $this.find('button.add-relationship-mat').addClass('d-none');
        }
    }

    // adds the material to the table of relationships
    function addMaterialToTable(e) {
        e.preventDefault();
        let thisLi = $(e.currentTarget).parent();
        let classGood = (thisLi.parent().attr('id') === 'collapseMatList') ? 'band' : 'plinth';
        let matCode = thisLi.data('matcode');
        let matName = thisLi.text();
        let tbody = thisLi.closest('div.row').find('table>tbody');
        let contains = false;
        let button = thisLi.closest('.modal-content').find('button.btn-primary');
        for (let tr of tbody.find('tr')) {
            if (Number ($(tr).find('td').eq(0).text()) === matCode) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            let tdCode = "<td>"+ matCode +"</td>";
            let tdName = "<td data-matid='" + thisLi.data('matid') + "'>"+ matName +"</td>";
            let tdTypeConn = "<td><select class='form-control'>" +
                "<option value=''>---</option>" +
                "<option value='same'>Такой же</option>" +
                "<option value='canbe'>Может быть</option>" +
                "</select></td>";
            let tdInPrice = "<td><input type='checkbox'></td>";
            let tdFixConn = "<td><input type='checkbox'></td>";
            let tdTrash = "<td><span class='text-danger drop-" + classGood + "-mat' style='cursor:pointer;'><i class='fas fa-trash-alt'></i></td>";
            let createTr = "<tr class='" + classGood + " added-" + classGood + "-mat'>" +
                tdCode + tdName + tdTypeConn + tdInPrice + tdFixConn + tdTrash +
                "</tr>";
            tbody.append(createTr);
        } else {
            let collapseDiv = thisLi.closest('.collapse');
            collapseDiv.prepend('<div class="alert alert-danger">"'+ matName +'" уже есть в таблице</div>');
            setTimeout(() => {
                collapseDiv.find('.alert-danger').remove();
            }, 3000);
        }
        if (tbody.find('.added-' + classGood + '-mat').length > 0) {
            button.removeClass('d-none');
        } else {
            button.addClass('d-none');
        }
        $('.drop-' + classGood + '-mat').on('click', deleteRelationMatGood);
    }

    function deleteRelationMatGood(e) {
        e.stopImmediatePropagation()
        let thisTr = $(this).closest('tr');
        let good = (thisTr.hasClass('band')) ? "Band" : "Plinth";
        if (thisTr.hasClass('added-band-mat') || thisTr.hasClass('added-plinth-mat')) {
            thisTr.remove();
        } else {
            let materialId = thisTr.find('td').eq(1).data('matid');
            let goodId, goodTableTr, url, data;
            if (thisTr.hasClass('band')) {
                goodId = $('#modalAddBand').find('.modal-title').data('bandid');
                goodTableTr = $('#tableBand table>tbody tr');
                url = 'ajax_band.php';
                data = {
                    band_id: goodId,
                    mat_id: materialId,
                    del_relation: true,
                }
            } else if (thisTr.hasClass('plinth')) {
                goodId = $('#modalAddPlinth').find('.modal-title').data('plinthid');
                goodTableTr = $('#tablePlinth table>tbody tr');
                url = 'ajax_plinth.php';
                data = {
                    plinth_id: goodId,
                    mat_id: materialId,
                    del_relation: true,
                }
            }
            $.post(
                url,
                data,
                function (data) {
                    if (data === 'success') {
                        goodTableTr.each(function() {
                            if ($(this).find('th').eq(0).text() === goodId) {
                                $(this).find('td').eq(2).find('ol li').each(function () {
                                    if($(this).text() === thisTr.find('td').eq(1).text()) this.remove();
                                });
                            }
                        });
                        thisTr.remove();
                    }
                }
            );

        }

        if ($('#' + good.toLowerCase() + 'MaterialsTable tbody tr').length > 0) {
            $('#btnAddMatTo' + good + 's').removeClass('d-none');
        } else {
            $('#btnAddMatTo' + good + 's').addClass('d-none');
        }
    }

    /**
     * shows a modal window for add bands-materials relationship
     * */
    $('#btnAddBand').on('click', () => {
        $('#btnAddMatToBands').addClass('d-none');
        const modal = $('#modalAddBand');
        const modalTitle = modal.find('.modal-title');
        if (modalTitle.data('bandid') !== undefined) modalTitle.removeAttr('data-bandid');
        modalTitle.text('Список выбранной кромки');
        const bandList = $('#tableBand>table>tbody>tr.clicked');
        let list = '';
        bandList.each(function () {
            let band = $(this).find('td').eq(1).text();
            let bandId = $(this).find('th').eq(0).text();
            let matIds = $(this).find('td').eq(2).data('matids');
            list += "<li data-bandid='" + bandId + "' data-matids='" + matIds + "' class='list-group-item'>" + band + "</li>";
        });
        $('#listChooseBand').html(list);
        cleanModalAddBand();
        $('#bandMaterialsTable>tbody').html('');
    });

    /**
     * Cleans a modal for band-materials
     * */
    function cleanModalAddBand() {
        $('#bandBtnAddMat').attr('aria-extended', false);
        if ($('#collapseMaterial').hasClass('show')) $('#collapseMaterial').removeClass('show');
        $('#filterMatForBand').val('');
        $('.count-mat>span.count-text').html('');
        $('#collapseMatList').html('');
    }

    /**
     * sends data for updating a relationship band-material
     * */
    $('#btnAddMatToBands').on('click', (e) => {
        e.preventDefault();
        let bands = [];
        let modalTitle = $('#modalAddBand').find('.modal-title');
        if (modalTitle.text() === 'Список выбранной кромки') {
            bands.push({list_band: true});
            $('#listChooseBand li').each(function () {
                bands.push({
                    band_id: $(this).data('bandid'),
                    name: $(this).text(),
                    mat_ids: $(this).data('matids'),
                });
            });
        } else {
            bands.push({list_band: false});
            bands.push({
                band_id: modalTitle.data('bandid'),
                name: modalTitle.text(),
            });
        }
        let materials = [];
        $('#bandMaterialsTable tbody tr').each(function () {
            materials.push({
                mat_id: $(this).find('td').eq(1).data('matid'),
                type_conn: $(this).find('td').eq(2).find('select').val(),
                in_price: $(this).find('td').eq(3).find('input').prop('checked'),
                fix_conn: $(this).find('td').eq(4).find('input').prop('checked'),
                fresh_mat: $(this).hasClass('added-band-mat'),
            });
        });
        let divTableBand = $('#tableBand').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $('#modalAddBand').modal('hide');
        $.post(
            'ajax_band.php',
            {
                bands: JSON.stringify(bands),
                materials: JSON.stringify(materials),
                update_band_mat: true,
            },
            function (data) {
                loadTable(data, divTableBand);
            }
        );
    });
    /*=============================
    * End settings band
    * =============================*/

    /*=============================
    * settings band
    * =============================*/

    // загружает список папок каталога дерева товаров плинтусов
    $('#btnOpenTreePlinth').on('click', function(e) {
        if ($('#plinthTree ul').length === 0) {
            const plinthTree = $('#plinthTree');
            $.post(
                'ajax_plinth.php',
                'load_tree=true',
                function (data) {
                    plinthTree.html(data);
                    $('#plinthTree li.list-group-item').on('click', showTreeChildren);
                }
            );
        }
    });

    // загружает таблицу плинтусов по умолчанию
    $('#nav-plinth-tab').on('click', (e) => {
        $('#tableMat table').html('');
        $('#tableBand').html('');
        $('#countPlinth span').text('0');
        const divTablePlinth = $('#tablePlinth').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $.post(
            'ajax_plinth.php',
            'load_table=true',
            function (data) {
                loadTable(data, divTablePlinth, 'Plinth');
            }
        );
    });

    /**
     * sends request for getting list of materials
     * */
    function plinthGetListOfMaterials($this) {
        const divPlinthMatTable = $('#plinthMaterialsTable>tbody');
        $.post(
            'ajax_plinth.php',
            {
                load_plinth_materials: true,
                plinth_id: $this.closest('tr').find('th').text(),
            },
            function (data) {
                let response = JSON.parse(data);
                if (response.error) {
                    divPlinthMatTable.html('Ошибка сервера');
                } else {
                    divPlinthMatTable.html(response.result);
                    $('.drop-plinth-mat').on('click', deleteRelationMatGood);
                    if ($('#plinthMaterialsTable tbody tr').length > 0) {
                        $('#btnAddMatToPlinth').removeClass('d-none');
                    } else {
                        $('#btnAddMatToPlinth').addClass('d-none');
                    }
                }
            }
        );
    }
    // если нет свойств у плинтусов, то скрываем фильтр по свойствам
    if ($('#modalPropertiesFilterPlinth table>tbody>tr').length === 0) $('#plinthBtnProperties').addClass('d-none');

    /**
     * shows a table of plinths by code or name
     * */
    function getFilteredPlinthTable(event) {
        const divTablePlinth = $('#tablePlinth').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        let $this = $(event.currentTarget);
        let data;
        if ($this.attr('name') === 'code') {
            data = {code: $this.val()};
            $('#plinthFilterName').val('');
        } else  {
            data = {name: $this.val()};
            $('#plinthFilterCode').val('');
        }
        $('#plinthFolder').text('');
        data.filter_plinth = true;
        $.post(
            'ajax_plinth.php',
            data,
            function (data) {
                loadTable(data, divTablePlinth, 'Plinth');
            }
        );
    }
    $('#plinthFilterCode').on('change', getFilteredPlinthTable);
    $('#plinthFilterName').on('change', getFilteredPlinthTable);

    /**
     *  Marks selected plinths from plinths table
     */
    function choosePlinths() {
        if (!$(this).hasClass('clicked')) {
            $(this).addClass('clicked');
            $(this).css({'background': '#00000013'});
        } else {
            $(this).removeClass('clicked');
            $(this).css({'background': 'transparent'});
        }
        if ($('#tablePlinth table>tbody>tr').hasClass('clicked')) {
            $('#btnAddPlinth').removeClass('d-none');
            $('#chooseAllPlinth').removeClass('d-none');
        } else {
            $('#btnAddPlinth').addClass('d-none');
            $('#chooseAllPlinth').addClass('d-none');
        }
    }

// chooses all plinths
    $('#chooseAllPlinth').on('click', (e) => {
        e.preventDefault();
        const $this = $(e.currentTarget);
        const plinths = $('#tablePlinth table>tbody>tr');
        if (!$this.hasClass('click')) {
            $this.addClass('click');
            plinths.css({'background': '#00000013'});
            plinths.addClass('clicked');
        } else {
            $this.removeClass('click');
            plinths.css({'background': 'transparent'});
            plinths.removeClass('clicked');
            $this.addClass('d-none');
            $('#btnAddPlinth').addClass('d-none');
        }
    });

    /**
     * shows a modal window for add plinth-tabletop relationship
     * */
    $('#btnAddPlinth').on('click', () => {
        $('#btnAddMatToPlinth').addClass('d-none');
        const modal = $('#modalAddPlinth');
        const modalTitle = modal.find('.modal-title');
        if (modalTitle.data('plinthid') !== undefined) modalTitle.removeAttr('data-plinthid');
        modalTitle.text('Список выбранных плинтусов');
        const plinthList = $('#tablePlinth>table>tbody>tr.clicked');
        let list = '';
        plinthList.each(function () {
            let plinth = $(this).find('td').eq(1).text();
            let plinthId = $(this).find('th').eq(0).text();
            let matIds = $(this).find('td').eq(2).data('matids');
            list += "<li data-plinthid='" + plinthId + "' data-matids='" + matIds + "' class='list-group-item'>" + plinth + "</li>";
        });
        $('#listChoosePlinth').html(list);
        cleanModalAddPlinth();
        $('#plinthMaterialsTable>tbody').html('');
    });

    /**
     * Cleans a modal for band-materials
     * */
    function cleanModalAddPlinth() {
        $('#plinthBtnAddMat').attr('aria-extended', false);
        if ($('#collapseMaterialP').hasClass('show')) $('#collapseMaterialP').removeClass('show');
        $('#filterMatForPlinth').val('');
        $('.count-mat>span.count-text').html('');
        $('#collapseMatListP').html('');
    }

    /**
     * loads list of materials if its is
     * */
    $('#sendFilterMatP').on('click', (e) => {
        e.preventDefault();
        let matList = $('#collapseMatListP').html('');
        $.post(
            'ajax_plinth.php',
            {filter_mat: $('#filterMatForPlinth').val()},
            function (data) {
                let response = JSON.parse(data);
                if (!response.error) {
                    matList.html(response.result);
                    matList.prev().find('span.count-text').html(response.message);
                    $('#collapseMatListP').find('li').on('click', activeItemCollapseMatList);
                    $('.add-relationship-mat').on('click', addMaterialToTable);
                } else {
                    matList.html("<div class='alert alert-warning'>" + response.message + "</div>");
                }
            }
        );
    });

    /**
     * sends data for updating a relationship band-material
     * */
    $('#btnAddMatToPlinths').on('click', (e) => {
        e.preventDefault();
        let plinths = [];
        let modalTitle = $('#modalAddPlinth').find('.modal-title');
        if (modalTitle.text() === 'Список выбранных плинтусов') {
            plinths.push({list_plinth: true});
            $('#listChoosePlinth li').each(function () {
                plinths.push({
                    plinth_id: $(this).data('plinthid'),
                    name: $(this).text(),
                    mat_ids: $(this).data('matids'),
                });
            });
        } else {
            plinths.push({list_plinth: false});
            plinths.push({
                plinth_id: modalTitle.data('plinthid'),
                name: modalTitle.text(),
            });
        }
        let materials = [];
        $('#plinthMaterialsTable tbody tr').each(function () {
            materials.push({
                mat_id: $(this).find('td').eq(1).data('matid'),
                type_conn: $(this).find('td').eq(2).find('select').val(),
                in_price: $(this).find('td').eq(3).find('input').prop('checked'),
                fix_conn: $(this).find('td').eq(4).find('input').prop('checked'),
                fresh_mat: $(this).hasClass('added-plinth-mat'),
            });
        });
        let divTablePlinth = $('#tablePlinth').html('<div class="text-center"><img src="RS/images/download.gif"></div>');
        $('#modalAddPlinth').modal('hide');
        $.post(
            'ajax_plinth.php',
            {
                plinths: JSON.stringify(plinths),
                materials: JSON.stringify(materials),
                update_plinth_mat: true,
            },
            function (data) {
                loadTable(data, divTablePlinth, 'Plinth');
            }
        );
    });
});