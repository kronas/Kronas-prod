<?php
// Полушин. Приставка для относительных путей подключения стилий, скриптов и шрифтов
$add_path = "";
if (isset($title)) {
    switch ($title) {
        case 'Конструктор стекла':
            $add_path = '../';
            break;
        default:
            $add_path = "";
    }

}
$execScript = preg_replace('#^.+\/#', '', $_SERVER['SCRIPT_FILENAME']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="<?=$add_path?>../js/popper.min.js"></script>
    <script src="<?=$add_path?>../js/jquery.min.js"></script>
    <?php /* https://givik.ru/posts/jquery-mask-plugin-primery-ispolzovaniya */ ?>
    <script src="<?=$add_path?>js/jquery.mask.min.js"></script>
    <script src="<?=$add_path?>js/custom.js"></script>
    <?php if ($execScript === 'write_off.php'): ?>
        <script src="newServiceApi/js/newServiceApi.js"></script>
    <?php endif ?>
    <script src="<?=$add_path?>RS/js/modal.js"></script>
    <link rel="stylesheet" href="<?=$add_path?>../css/bootstrap.min.css">
    <script src="<?=$add_path?>../js/bootstrap.min.js"></script>
    <link href="<?=$add_path?>fontawesome/css/all.css" rel="stylesheet">
<!--    <script defer src="fontawesome/js/all.js"></script>-->
    <link href="<?=$add_path?>css/custom.css" rel="stylesheet">
    <?php
    // Полушин. если подключается шапка из сетколки, подключаем дополнительные скрипты и стили
    if(isset($title)) {
        if($title === 'Конструктор стекла') {
            includeToHeaderForGlass();
        }
        if ($title === 'Настройки') echo '<script src="glass/assets/js/glass_settings.js"></script>';
        echo "<title>$title</title>";
    }
    ?>
</head>
<body>
<div id="start_checks_lodaer_containers" class="d-none">
    <div>
        <img src="<?=$add_path?>RS/images/download.gif" alt="preloader">
    </div>
</div>
<div class="container-fluid">
<div class="row">
    <?php if(!isset($toolbarclose)){ ?>
        <div class="col-sm-12 text-right">
            <?php echo toolbar($_SESSION, $add_path); ?>
        </div>
    <?php } ?>
    <?php if(isset($title)) echo "<h1 class=\"text-center col-12 mb-3\">$title</h1>"; ?>
    <?php echo messages($_SESSION,"col-md-4 offset-md-4 col-sm-12"); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Выберите клиента:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="new-order-form">
                <?php // Полушин. Тут ссылка генерируется из /RS/js/modal.js ?>
                <form action="<?= $main_dir.'?nw='.$_GET['nw']?>" method="GET" id="sac_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Клиент:</label>
                            <ul class="nav nav-tabs" id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-selected="true">Имя</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="false">Телефон</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent1">
                                <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
                                    <input type="text" class="form-control" id="filter_client_name_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                                </div>
                                <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                                    <input type="text" class="form-control" id="filter_client_phone_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                                </div>
                            </div>
                            <div id="client_variants_auth">
                                <ul class="list-group">

                                </ul>
                            </div>


                            <input type="hidden" name="client_id" id="filter_client_auth" value="">
                            <input type="hidden" name="manager_id" id="filter_manager_auth" value="<?= $_SESSION['manager_id'] ?>">
                            <input type="hidden" name="client_code" id="filter_client_code_auth" value="">
                            <input type="hidden" name="set_new_order" value="1">
                        </div>
                        <div class="form-group" id="client_auth_message">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="client_auth_form_button">Создать заказ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>