<?php

date_default_timezone_set( 'Europe/Kiev' );

include_once(__DIR__.'/../_1/config.php');
include_once(__DIR__.'/../func.php');
// check_production_client();
function _pre($i)
{
    echo '<pre>';print_r($i); echo '</pre>';
}

function get_auth_user($where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_auth_user start', $_SESSION);
    $sql = "SELECT * FROM client WHERE $where";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_auth_user end', $_SESSION);

    if($result['count'] > 0)
        return $result['data'][0];
    else
        return null;
}
function get_auth_manager($where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_auth_manager start', $_SESSION);
    $sql = "SELECT * FROM manager WHERE $where";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_auth_manager end', $_SESSION);
    if($result['count']==1)
        return $result['data'][0];
    else
        return null;
}
function get_phone_mail_is($in,$type)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_phone_mail_is start', $_SESSION);
    $_tp=array('phone'=>'tel', 'mail'=>'UPPER(TRIM(`e-mail`))');
    $sql = "SELECT COUNT(*) AS cnt FROM client WHERE ".$_tp[$type]."='".$in."'";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_phone_mail_is end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function add_client($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'add_client start', $_SESSION);
    $_new_phone=str_replace(' ','',trim($in["new_phone"]));
    $_new_mail=trim($in["new_mail"]);
    if(strlen($_new_phone)>0 && strlen($_new_phone)!=17){$_SESSION['messages']['errors'][]='Телефон не корректный!';header('Location: '.$main_dir . '/clients/add_user.php'.'?nw='.$_GET['nw']); exit();}
    if(strlen($_new_phone)>0 && get_phone_mail_is($_new_phone,'phone')>0){$_SESSION['messages']['errors'][]='Такой телефон существует!';header('Location: '.$main_dir . '/clients/add_user.php'.'?nw='.$_GET['nw']); exit();}
    if(strlen($_new_mail)>0 && get_phone_mail_is($_new_mail,'mail')>0){$_SESSION['messages']['errors'][]='Такой E-Mail существует!';header('Location: '.$main_dir . '/clients/add_user.php'.'?nw='.$_GET['nw']); exit();}
    if(strlen($_new_mail)==0) $new_mail='NULL'; else $new_mail='"'.$_new_mail.'"';
    if(strlen($_new_phone)==0) $new_phone='NULL'; else $new_phone='"'.$_new_phone.'"';
    $sql = "INSERT INTO client(name, tel, `e-mail`, pass) VALUES ('" . trim($in["new_name"]) . "', " .$new_phone. ", " .$new_mail. ", '" . md5(trim($in["new_password"])) . "')";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'add_client end', $_SESSION);
}
function get_client($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_client start', $_SESSION);
    $sql = "SELECT client_id, name, `e-mail` FROM client WHERE tel='".str_replace(' ','',trim($in))."'";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_client end', $_SESSION);
    if($result['count']==1)
        return $result['data'][0];
    else
        return null;
}

function get_clientAlt($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_clientAlt start', $_SESSION);
    $sql = "SELECT client_id, name, `e-mail`, tel FROM client WHERE ".$in;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_clientAlt end', $_SESSION);
    if($result['count']==1)
        return $result['data'][0];
    else
        return null;
}

function db_verifyGroupClient($clientIDin,$clientID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_verifyGroupClient start', $_SESSION);
    $sql = "SELECT COUNT(*) as cnt FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.client_id = $clientID)) AND client.client_id=$clientIDin";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_verifyGroupClient end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function messages(&$session,$wrapper)
{

    if((isset($session['messages']['success']) && count($session['messages']['success'])>0) || (isset($session['messages']['errors']) && count($session['messages']['errors'])>0))
    {
        $out='<div class="'.$wrapper.'">';
        if(isset($session['messages']['errors']) && count($session['messages']['errors'])>0)
        {
            foreach ($session['messages']['errors'] as $value)
            {
                $out.='<div class="alert alert-danger" role="alert">'.$value.'</div>';
            }
        }
        if(isset($session['messages']['success']) && count($session['messages']['success'])>0)
        {
            foreach ($session['messages']['success'] as $value)
            {
                $out.='<div class="alert alert-success" role="alert">'.$value.'</div>';
            }
        }
        $out.='</div>';
        unset($session['messages']);
        return $out;
    }
    else
        return '';
}
// 09.12.2020 Полушин, добавил ссылку на стеколку
function toolbar($session, $addPath='')
{
    if(!isset($_SESSION['user']) || $session['user'] === 'guest') return '<div class="alert alert-warning" role="alert">
<div>
<a href="'.$addPath.'glass/index.php?nw='.$_GET['nw'].'" title="Стеколка"><i class="far fa-window-restore"></i></a> ||
<a href="'.$addPath.'login.php" title="Авторизоваться">Авторизоваться <i class="fas fa-sign-in-alt"></i></a></div></div>';
else{
    $_roles=array(
        'client'=>'Клиент',
        'manager'=>'Менеджер',
        'admin'=>'Админ',
    );
    $_task='';
    switch ($session['user']['role'])
    {
        case 'client':
                    $main_dir=$GLOBALS['main_dir'];
                    if (is_dir($main_dir . '/giblib/')) $linkk=$GLOBALS['main_dir'] . '/giblib/';
                    else  $linkk=$GLOBALS['laravel_dir'];
                    // p_($linkk);
                    $_task='<a href="'.$linkk.'" title="GL" ><i><b>GL</b></i></a> || 
                            <a href="'.$addPath.'RS/clients/create.php" title="Раздвижные системы" ><i class="far fa-map"></i></a> ||
                            <a href="'.$addPath.'glass/index.php" title="Стеколка" ><i class="far fa-window-restore"></i></a> || 
                            <a href="'.$main_dir.'/clients/'.'docs.php" title="Документы" ><i class="far fa-file-alt"></i></a> || 
                            <a href="'.$main_dir.'/clients/'.'balance.php" title="Остатки" ><i class="fas fa-warehouse"></i></a> || 
                            <a href="'.$main_dir.'/clients/'.'orders.php" title="Заказы" ><i class="far fa-folder-open"></i></a> || ';
            break;
        case 'manager':
        case 'admin':
            $_task='<button class="RSButt" data-toggle="modal" title="Раздвижные системы" data-target="#exampleModal"><i class="far fa-map"></i></button> || 
<button class="RSButt" data-toggle="modal" title="Стеколка" data-target="#exampleModal"><i class="far fa-window-restore"></i></button> || 
<a href="'.$addPath.'index.php" title="Клиенты" ><i class="fas fa-users"></i></a> || 
<a href="'.$GLOBALS['main_dir'].'/client_registration.php" title="Добавить клиента" ><i class="fas fa-user-plus"></i></a> || 
<a href="'.$addPath.'return.php" title="Возврат" class="text-danger" ><i class="fas fa-undo-alt"></i></a> || 
<a href="'.$addPath.'into_production.php" title="В производство"><i class="fas fa-dolly"></i></a> || 
<a href="'.$addPath.'comig.php" title="Приход" ><i class="fas fa-file-import"></i></a> || 
<a href="'.$addPath.'write_off.php" title="Списание остатков" ><i class="fas fa-file-export"></i></a> || 
<a href="'.$addPath.'docs.php" title="Документы" ><i class="far fa-file-alt"></i></a> ||  
<a href="'.$addPath.'balance.php" title="Остатки" ><i class="fas fa-warehouse"></i></a>  || ';
            break;
    }
    if($session['user']['role']=='admin') $_task.='  <a href="'.$addPath.'corrects.php" title="Корректировки" ><i class="fas fa-balance-scale"></i></a>  || 
  <a href="'.$addPath.'settings.php" title="Настройки" ><i class="fas fa-cogs"></i></a>  || 
  ';
    $out='Приветствуем, '.$session['user']['name'].'!  (<small><i>'.$_roles[$session['user']['role']].'</i></small>)  || 
'.$_task.' <a href="'.$addPath.'login.php?op=logout" title="Выход"><i class="fas fa-sign-out-alt"></i></a>';
    return '<div class="alert alert-warning" role="alert"><div class="float-left"><a href="'.$GLOBALS['laravel_dir'].'"></a> </div>
<div>'.$out.'</div></div>';
}
}

function db_get_list4newdataOnWord($search_in, $contID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4newdataOnWord start', $_SESSION);
    $out='<ul class="list-group">';
    $search_in=trim($search_in);
    $sql=db_get_list4newdata_sub1($search_in, 1, 10);
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $row_cnt = $result['count'];
    if($row_cnt>0) get_list4newdata_crea($result['data'], $out, $contID);
    if($row_cnt<5 && substr_count($search_in,' ')>0)
    {
        $sql=db_get_list4newdata_sub1($search_in, 2, 10-$row_cnt);
        $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        $row_cnt2 = $result['count'];
        if($row_cnt2>0) get_list4newdata_crea($result['data'], $out, $contID);
        if(($row_cnt+$row_cnt2)<10)
        {
            $sql=db_get_list4newdata_sub1($search_in, 3, 10-($row_cnt+$row_cnt2));
            $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
            $row_cnt3 = $result['count'];
            if($row_cnt3>0) get_list4newdata_crea($result['data'], $out, $contID);
        }
    }
    $out.="</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4newdataOnWord end', $_SESSION);
    return $out;
}

function get_list4newdata_crea($result, &$out, $contID)
{
    foreach($result as $row) {
        $out.="<li  onClick=\"list4newdata_select('".$row['mid']."', '".$row['mname']."', '".$row['mcode']."', ".$row['t'].", ".$contID.");\" class=\"list-group-item\">".$row['mname']."</li>";
    }
}
function db_get_list4newdata_sub1($search_in, $op, $limit)
{
    $_where=' WHERE (CONVERT(CODE,char) LIKE "%'.$search_in.'%" OR  NAME LIKE "%'.$search_in.'%") AND LENGTH(NAME)>0   ';
    if($op==2)
    {
        $_arr_w1=explode(' ',$search_in);
        $_where=" WHERE NAME LIKE '%".implode("%' AND NAME LIKE '%",$_arr_w1)."%' ";
    }
    if($op==3)
    {
        $_arr_w1=explode(' ',$search_in);
        $_where=" WHERE NAME LIKE '%".implode("%' OR NAME LIKE '%",$_arr_w1)."%' ";
    }
    $sql = "SELECT DISTINCTROW   MATERIAL.MATERIAL_ID as mid, 
MATERIAL.T as t, 
MATERIAL.CODE as mcode,
MATERIAL.NAME as mname 
FROM 
MATERIAL 
$_where 
ORDER BY mname ASC LIMIT 0,".$limit ;
    return $sql;
}
function db_get_list4newdataOnT($search_in, $contID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4newdataOnT start', $_SESSION);
    $out='<ul class="list-group">';
    $search_in=(float) str_replace(',','.',trim($search_in));
    $sql=db_get_list4newdata_sub2($search_in, 10);
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    if($result['count']>0) get_list4newdataOnT_crea($result['data'], $out, $contID);
    $out.="</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4newdataOnT end', $_SESSION);
    return $out;
}

function db_get_list4newdata_sub2($search_in, $limit)
{
    $_where=" WHERE MY_1C_NOM = 5931 AND CAST(T AS CHAR) LIKE '$search_in%'   ";
    $sql = "SELECT * FROM MATERIAL $_where ORDER BY T ASC LIMIT 0,".$limit ;
    return $sql;
}

function get_list4newdataOnT_crea($result, &$out, $contID)
{
    foreach ($result as $row) {
        $out.="<li  onClick=\"list4newdataOnT_select('".$row['MATERIAL_ID']."','".$row['NAME']."','".$row['CODE']."', ".$row['T'].", ".$contID.");\" class=\"list-group-item\">".$row['T']."</li>";
    }
}

function coming_parse_post($post, &$mat_in, $placesID)
{
    if(isset($post['erase-tc'])) $erase=$post['erase-tc']; else  $erase=array();
    foreach ($post['code'] as $_k=>$_v)
    {
        if((int)$post['MaterialID'][$_k]>0 && in_array((int)$post['MaterialID'][$_k],$erase)) $del=true; else $del=false;
        if($_k!=0 && (strlen(trim($post['code'][$_k]))!=0 || strlen(trim($post['Code_mat'][$_k]))!=0) && !$del )
        {
            $_cell=0; if(isset($post['cells'][$_k])) $_cell=$post['cells'][$_k];
            $_arr=array(
                'M_ID'=>$post['M_ID'][$_k],
                'T'=>$post['T'][$_k],
                'Name'=>$post['Name'][$_k],
                'L'=>$post['L'][$_k],
                'W'=>$post['W'][$_k],
                'crash_checkbox'=>$post['crash_checkbox'][$_k],
                'notation'=>$post['notation'][$_k],
                'COUNT'=>$post['COUNT'][$_k],
                'placesID'=>$placesID,
                'cell'=>$_cell,
                // 20.06.2023 Тоценко. Приход материалов, присылаемых с нового сервиса
                'api_part_ids' => isset($post['api_part_ids']) ? $post['api_part_ids'] : null
            );
            if ($_arr['api_part_ids'] === null) unset($_arr['api_part_ids']);
            
                $rez=0;
                foreach ($mat_in as $_key=>$item)
                {
                    if( ((strlen(trim($post['code'][$_k]))>0 && trim($post['code'][$_k])==trim($item['code'])) ||
                        (strlen(trim($post['Code_mat'][$_k]))>0 && strlen(trim($post['Name'][$_k]))>0
                            && trim($post['Code_mat'][$_k])==trim($item['Code_mat']) && trim($post['Name'][$_k])==trim($item['Name'])))
                        && $_arr['L']==$item['L'] && $_arr['W']==$item['W'] )
                    {
                        $mat_in[$_key]['COUNT']=$mat_in[$_key]['COUNT']+$post['COUNT'][$_k];
                        $rez=1;
                    }
                }
                if($rez==0)
                {
                    $_arr['code']=strlen(trim($post['code'][$_k]))>0?trim($post['code'][$_k]):'';
                    $_arr['Code_mat']=strlen(trim($post['Code_mat'][$_k]))>0?trim($post['Code_mat'][$_k]):'';
                    $mat_in[]=$_arr;
                }
        }
    }
}
function db_get_exist_materials(&$mat_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_exist_materials start', $_SESSION);
    $where=array();
    $key_where=array();
    foreach($mat_in as $key=>$value)
    {
            $where[]= "(Code={$value['M_ID']} AND L={$value['L']} AND W={$value['W']}  AND CAST(T AS CHAR)={$value['T']} AND TRIM(Name)='".trim($value['Name'])."') ";

        $key_where[trim($value['Name']).'||'.trim($value['M_ID']) .
        '||'.$value['L'].'||'.$value['W'].'||'.$value['T']]=$key;
    }
    $sql='SELECT * FROM MATERIAL_CLIENT WHERE  '.implode(' OR ',$where).';';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

    foreach($result as $row) {
        $key_w=trim($row['Name']).'||'.trim($row['CODE']).
            '||'.$row['L'].'||'.$row['W'].'||'.$row['T'];
          if(isset($key_where[$key_w])) $mat_in[$key_where[$key_w]]['ID']=$row['mc_id'];
      }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_exist_materials end', $_SESSION);
}
function db_verification_cell_materials(&$mat_in, $placesID, $clientID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_verification_cell_materials start', $_SESSION);
    $where=array();
    foreach($mat_in as $key=>$value)
    {
        if(isset($value['ID']) && $value['ID']>0) $where[]=$value['ID'];
    }
    if(count($where)>0) {
        $sql = "SELECT  DISTINCT ms.cell, ms.material_client FROM MATERIAL_CLIENT_CELL_STOCK AS ms INNER JOIN MATERIAL_KL_STOCK_COUNT AS mc ON mc.Material_client = ms.material_client
            WHERE mc.Place = $placesID AND mc.Client = $clientID AND ms.material_client IN (" . implode(', ', $where) . ")";
        $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
        foreach ($result as $line)
        {
            foreach($mat_in as $key=>$value)
            {
                if(isset($value['ID']) && $value['ID']>0 && $value['ID']==$line['material_client'])
                {
                    if($value['cell']!=$line['cell'])
                    {
                        $mat_in[$key]['cell']=$line['cell'];
                        $_SESSION['messages']['success'][] = '('.$value['M_ID'].') '.$value['Name'].' найден в ячейке №'.$line['cell'].'! Туда помещен и приход.';
                    }
                }
            }
        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_verification_cell_materials end', $_SESSION);
}
function db_create_doc($Type, $clientID, $Reason, $Manager, $Order1C, $Comment)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_create_doc start', $_SESSION);
    $sql="INSERT INTO DOC (Datetime, Type, Manager, Client, Reason,  Comment) VALUES (".time().", '$Type', $Manager, $clientID, '$Reason',  '$Comment')";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_create_doc end', $_SESSION);
    return $result['id'];
}
function db_update_doc($docID, $Manager)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_doc start', $_SESSION);
    $sql="UPDATE DOC SET Datetime = ".time().", Manager = $Manager WHERE DOC.d_id = $docID; ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $sql="DELETE FROM MATERIAL_KL_STOCK_MOVE WHERE Doc = $docID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_doc end', $_SESSION);
}

function db_add_materials(&$mat_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_add_materials start', $_SESSION);
    $sql1='INSERT INTO MATERIAL_CLIENT ( W, L, T, Code_mat, CODE, Name, crash, notation) VALUES ';
    foreach ($mat_in as $key=>$value)
    {
        if(!isset($value['ID'])) {
        //   $codemat='';$code='';if($value['Code_mat']>0)$codemat=$value['M_ID']; else $code=$value['M_ID'];
            $codemat=$value['M_ID'];  $code=$value['M_ID'];
            $sql2 = " ({$value['W']}, {$value['L']}, {$value['T']}, '{$codemat}', '{$code}', '{$value['Name']}', '{$value['crash_checkbox']}', '{$value['notation']}' );";
            $sql = $sql1 . $sql2;
            $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
            $mat_in[$key]['ID'] =$result['id'];
        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_add_materials end', $_SESSION);
}

function db_add_materials2cells($mat_in,$docID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_add_materials2cells start', $_SESSION);
    $sql1='INSERT INTO MATERIAL_CLIENT_CELL_STOCK (cell, material_client, Doc ) VALUES ';
    foreach ($mat_in as $key=>$value)
    {
        if(isset($value['ID']) && isset($value['cell'])) {
            $sql2 = " ({$value['cell']}, {$value['ID']}, {$docID});";
            $sql = $sql1 . $sql2;
            sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_add_materials2cells end', $_SESSION);
}

function db_materials_stock_move_add($mat_in, $docID)
{
    // de($mat_in);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_materials_stock_move_add start', $_SESSION);
    $sql1='INSERT INTO MATERIAL_KL_STOCK_MOVE (Material_client, Client, Doc, Move, api_part_ids, Place) VALUES ';
    $sql2=array();

    foreach ($mat_in as $key=>$value) {
        // Установка значения для поля для обращения из файлов, не касающихся добавления материалов
        if (!isset($value['clientId'])) {
            $value['clientId'] = null;
        }
        $sql2[] = " ({$value['ID']}, {$value['clientId']}, {$docID}, {$value['COUNT']}, '{$value['part_ids_checked']}', {$value['placesID']})";
    }
        $sql=$sql1.implode(',',$sql2).';';

    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_materials_stock_move_add end', $_SESSION);

    // Переписываем идентификаторы в записях, выбранные перебрасываем в новую запись и удаляем их из старой
    redistributePartIds($mat_in);
}

/**
 * Переписываем идентификаторы в записях, выбранные перебрасываем в новую запись и удаляем их из старой
 */
function redistributePartIds($materialsArr) {

    foreach ($materialsArr as $key => $value) {

        if (isset($value['part_ids'])) {

            $q = "SELECT * FROM `MATERIAL_KL_STOCK_MOVE` WHERE `Move` > 0 AND `api_part_ids` <> '' AND `Material_client` = " . $value['ID'] . " AND `Client` = " . $value['clientIdFrom'] . "";
            $res = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
            $res = $res['data'];

            $checkedPartIdsArr = explode(',', $value['part_ids_checked']);

            foreach ($res as $entry) {
                $entryIds = explode(',', $entry['api_part_ids']);
                $endPartIds = (implode(',', array_diff($entryIds, $checkedPartIdsArr)));

                $q = "UPDATE `MATERIAL_KL_STOCK_MOVE` SET `api_part_ids` = '" . $endPartIds . "' WHERE `Move` > 0 AND `api_part_ids` = '" . $entry['api_part_ids'] . "'";
                $res = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);

                if ($res['res'] !== 1) {
                    return false;
                }
            }

        }
    }

    return true;
}

function db_materials_stock_upgate($clientID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_materials_stock_upgate start', $_SESSION);
    $sql='DELETE FROM MATERIAL_KL_STOCK_COUNT WHERE Client='.$clientID;
    $result = $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $sql="SELECT Sum(sm.Move) as ssum, sm.Place, sm.Material_client as mc 
FROM DOC INNER JOIN MATERIAL_KL_STOCK_MOVE AS sm ON sm.Doc = DOC.d_id 
WHERE DOC.Client = $clientID  AND DOC.Type<>'plan' GROUP BY sm.Place, sm.Material_client ";

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $sql1='INSERT INTO MATERIAL_KL_STOCK_COUNT (Material_client, Client, Count, Place) VALUES ';
    $sql2=array();
    $arr4cels=array();
    foreach($result as $row)  {
        $sql2[]='('.$row['mc'].', '.$clientID.', '.$row['ssum'].', '.$row['Place'].')';
        if((int)$row['ssum']<1) $arr4cels[]=array('mc'=>$row['mc'],'Place'=>$row['Place']);
    }
    if(count($sql2)>0) {
        $sql = $sql1 . implode(', ', $sql2) . ';';
        $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    }
    if(count($arr4cels)>0) {
        db_materials_stock_cells_upgate($arr4cels,$clientID);
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_materials_stock_upgate end', $_SESSION);
}

function db_materials_stock_cells_upgate($inData,$clientID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_materials_stock_cells_upgate start', $_SESSION);
    $_arr_mc = array();
    foreach ($inData as $zz) {
        $_arr_mc[] = $zz['mc'];
    }
    $sql = 'SELECT ccs.Doc, ccs.material_client AS mc FROM MATERIAL_CLIENT_CELL_STOCK AS ccs INNER JOIN DOC AS d ON ccs.Doc = d.d_id 
WHERE ccs.material_client IN (' . implode(',',$_arr_mc) . ') AND d.Client = ' . $clientID;

    $_tmp_list_docs = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

    $list_docs = array();
    $counter = 0;

    foreach ($_tmp_list_docs as $_value) {
        $list_docs[$counter][] = '(Doc=' . $_value['Doc'] . ' AND material_client=' . $_value['mc'] . ')';
        if (count($list_docs[$counter]) == 10) $counter++;
    }
    if (count($list_docs)) {
        foreach ($list_docs as $_value) {
            $sql = "DELETE FROM MATERIAL_CLIENT_CELL_STOCK WHERE " . implode(' OR ', $_value);
            sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_materials_stock_cells_upgate end', $_SESSION);
}

function db_get_listPlaces($onlyData=false, $selected=0, $req=0,$none=TRUE)
{
    $sql='SELECT * FROM PLACES';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    if($onlyData)
    {
        $out=array();
    }
    else
    {
        $_req=array('',' required','');
        if($req==1) $_req=array('the-required',' required','_auto');
        if($req==2) $_req=array('the-required','');
        $out='    <div class="form-group">
        <label for="select_places">Выберите склад</label>
        <select class="form-control '.$_req[0].'" name="select_places'.$_req[2].'" id="select_places" '.$_req[1].'>';
        if($none) $out.='<option value="" '.($selected>0?'':'selected').'>Не указано</option>';
    }
    foreach ($result as $row){
        if($onlyData)
        {
            $out[$row['PLACES_ID']]=$row;
        }
        else {
            $opsel='';
                    if($selected==$row['PLACES_ID']) $opsel=' selected ';
            $_add = '';
            if (strlen(trim($row['ADRESS'])) > 0 && strlen(trim($row['PHONE'])) > 0) $_add = ' (' . $row['ADRESS'] . ' / ' . $row['PHONE'] . ')';
            elseif (strlen(trim($row['ADRESS'])) > 0 || strlen(trim($row['PHONE'])) > 0) $_add = ' (' . $row['ADRESS'] . $row['PHONE'] . ')';
            $out .= "<option value=\"" . $row['PLACES_ID'] . "\" $opsel >" . $row['NAME'] . $_add . "</option>";
        }
    }
    if(!$onlyData) $out.="</select></div>";
    return $out;
}
function db_get_doc_details($doc)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_doc_details start', $_SESSION);
    $sql='SELECT DOC.*, sm.Material_client, sm.Move, sm.Place, mc.mc_id, mc.W, mc.L, mc.T, mc.Code_mat, mc.CODE, 
mc.Name, mc.crash, mc.notation, cl.name AS cname, cl.tel AS ctel, cl.`e-mail` AS cemail, mn.name AS mname, mt.`NAME` AS mtname, mt.`CODE` AS mtcode, mt.MY_1C_NOM, mt.MATERIAL_ID 
FROM DOC
INNER JOIN MATERIAL_KL_STOCK_MOVE AS sm ON sm.Doc = DOC.d_id
INNER JOIN MATERIAL_CLIENT AS mc ON mc.mc_id = sm.Material_client
INNER JOIN client AS cl ON cl.client_id = DOC.Client
INNER JOIN manager AS mn ON mn.id = DOC.Manager
LEFT JOIN MATERIAL AS mt ON mt.MATERIAL_ID = mc.CODE
WHERE DOC.d_id = '.$doc;
    $rez=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_doc_details end', $_SESSION);
    return $rez;
}

function db_get_balance_details($matID,$placeID,$WhereCLIENT)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_balance_details start', $_SESSION);
    $sql="SELECT MATERIAL_KL_STOCK_MOVE.Material_client, MATERIAL_KL_STOCK_MOVE.Doc, MATERIAL_KL_STOCK_MOVE.Move, MATERIAL_KL_STOCK_MOVE.Place,
                    DOC.Datetime, DOC.Type, DOC.Manager, DOC.Client, manager.`name` AS manName, PLACES.`NAME` AS plcName,
                    client.`name` AS clName, MATERIAL_CLIENT.`Name`, MATERIAL_CLIENT.W, MATERIAL_CLIENT.L, MATERIAL_CLIENT.T
                FROM MATERIAL_KL_STOCK_MOVE
                INNER JOIN DOC ON MATERIAL_KL_STOCK_MOVE.Doc = DOC.d_id
                INNER JOIN manager ON DOC.Manager = manager.id
                INNER JOIN PLACES ON MATERIAL_KL_STOCK_MOVE.Place = PLACES.PLACES_ID
                INNER JOIN client ON DOC.Client = client.client_id
                INNER JOIN MATERIAL_CLIENT ON MATERIAL_KL_STOCK_MOVE.Material_client = MATERIAL_CLIENT.mc_id
                WHERE
                MATERIAL_KL_STOCK_MOVE.Material_client = $matID AND
                $WhereCLIENT MATERIAL_KL_STOCK_MOVE.Place=$placeID";
    $rez=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_balance_details end', $_SESSION);
    return $rez;
}

function db_get_listdocs($filtr, $start, $limit, &$count_row)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listdocs start', $_SESSION);
    $where='';
    if(count($filtr)>0) $where=' WHERE '. implode(' AND ',$filtr).' ';
    $sql = "SELECT COUNT(*) as cnt  FROM DOC LEFT JOIN client ON client.client_id = DOC.Client
LEFT JOIN manager ON manager.id = DOC.Manager $where" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $count_row = $result['data'][0]['cnt'];
    if($count_row==0)
    {
        mysqli_close($db);
        return NULL;
    }
    // $sql = "SELECT * FROM DOC $where ORDER BY m.NAME ASC LIMIT $start, $limit " ;
    $sql = "SELECT DOC.d_id, DOC.Datetime, DOC.Type, client.client_id, client.name as cname, manager.name as mname  FROM DOC LEFT JOIN client ON client.client_id = DOC.Client
LEFT JOIN manager ON manager.id = DOC.Manager $where ORDER BY DOC.d_id DESC LIMIT $start, $limit " ;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $_rows =$result['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listdocs end', $_SESSION);
    return $_rows;
}
function db_get_materialClient($mc_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_materialClient start', $_SESSION);
    //.. 06.12.2022 Тоценко
    // В запрос добавлено поле "mc.crash", которое отображает повреждения материала клиента
    $sql = "SELECT mc.mc_id, gpm.VALUESTR, mt.`CODE`, mc.L, mc.W, mc.T, mc.crash, MAX( doc.Datetime ) as ddate, mc.`Name` as mcName  
            FROM MATERIAL_CLIENT AS mc
            LEFT JOIN MATERIAL AS mt ON mt.MATERIAL_ID = mc.`CODE`
            INNER JOIN GOOD_PROPERTIES_MATERIAL AS gpm ON gpm.MATERIAL_ID = mt.MATERIAL_ID 
            	INNER JOIN MATERIAL_KL_STOCK_MOVE AS stmv ON mc.mc_id = stmv.Material_client
	INNER JOIN DOC AS doc ON stmv.Doc = doc.d_id 
            WHERE mc.mc_id IN (".implode(',',$mc_id).") 
            ORDER BY gpm.PROPERTY_ID ASC";
    $docs=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_materialClient end', $_SESSION);
return $docs;
}
function db_get_listbalance($filtr, $start, $limit, &$count_row)
{


    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listbalance start', $_SESSION);
    $where='';
    if(count($filtr)>0) $where=' WHERE '. implode(' AND ',$filtr).' ';


    $sql = "SELECT COUNT(*) as cnt  FROM MATERIAL_KL_STOCK_COUNT AS stcnt 
INNER JOIN MATERIAL_CLIENT AS mc ON stcnt.Material_client = mc.mc_id 
LEFT JOIN MATERIAL_CLIENT_CELL_STOCK AS mccs ON mccs.material_client = mc.mc_id $where" ;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

    if($result['count']==0)
    {
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listbalance end', $_SESSION);
        return NULL;
    }
    $sql = "SELECT DISTINCT 
(SELECT SUM(smv.Move) FROM DOC AS sd INNER JOIN MATERIAL_KL_STOCK_MOVE AS smv ON sd.d_id = smv.Doc 
INNER JOIN client AS scl ON sd.Client = scl.client_id 
WHERE sd.Type = 'plan' AND smv.Material_client = mc.mc_id AND 
	smv.Place = plc.PLACES_ID AND  scl.`code` = clnt.`code` ) as count_plan, stcnt.Client, 
stcnt.Count,
sm.api_part_ids,
stcnt.Place,
mc.mc_id,
mc.W,
mc.L,
mc.T,
mc.`CODE`,
mc.`Name` as matName,
plc.`NAME` as placeName, 
clnt.`name` AS clName,
clnt.`tel` AS clTel,
clnt.`code` AS clCODE, 
mccs.cell 
FROM MATERIAL_KL_STOCK_COUNT AS stcnt
INNER JOIN MATERIAL_KL_STOCK_MOVE AS sm ON sm.Material_client = stcnt.Material_client
INNER JOIN MATERIAL_CLIENT AS mc ON stcnt.Material_client = mc.mc_id
INNER JOIN PLACES AS plc ON stcnt.Place = plc.PLACES_ID 
INNER JOIN client AS clnt ON stcnt.Client = clnt.client_id 
LEFT JOIN MATERIAL_CLIENT_CELL_STOCK AS mccs ON mccs.material_client = mc.mc_id
LEFT JOIN DOC ON DOC.Client = clnt.client_id AND DOC.d_id = mccs.Doc 
$where 
ORDER BY stcnt.Count DESC LIMIT $start, $limit " ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listbalance end', $_SESSION);
    return $result['data'];
}

function db_get_liststockOnClient($clientID, $materialID=0,$placeID=0)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_liststockOnClient start', $_SESSION);
    $_wherePlace=$_whereClient=$_whereMaterial='';
    if($placeID>0) $_wherePlace=" scnt.Place = $placeID AND ";
    if($clientID>0) $_whereClient=" scnt.Client IN 
                (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.client_id = $clientID))) AND ";
if($materialID>0) $_whereMaterial=" scnt.Material_client = $materialID AND ";

    $sql = "SELECT mtrl.`NAME` AS mtrl_name, matcl.W AS matcl_w, matcl.L AS matcl_l, matcl.T AS matcl_t, matcl.Code_mat, 
                            matcl.`Name` AS matcl_name, matcl.`CODE`, scnt.Material_client, scnt.Count, plc.`NAME`, plc.PLACES_ID, client.client_id, client.`name` AS clNaem
                FROM MATERIAL_KL_STOCK_COUNT AS scnt
                INNER JOIN MATERIAL_CLIENT AS matcl ON matcl.mc_id = scnt.Material_client
                INNER JOIN PLACES AS plc ON plc.PLACES_ID = scnt.Place
                LEFT JOIN MATERIAL AS mtrl ON mtrl.`CODE` = matcl.`CODE` 
                INNER JOIN client ON client.client_id = scnt.Client 
                WHERE $_wherePlace $_whereClient $_whereMaterial scnt.Count>0 
                ORDER BY plc.`NAME` ASC" ;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_liststockOnClient end', $_SESSION);
    if($result['count']==0) return NULL;

    return $result['data'];
}

function db_get_listclients($filtr, $start, $limit, &$count_row)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listclients start', $_SESSION);
    $where='';
    if(count($filtr)>0) $where=' WHERE '. implode(' AND ',$filtr).' ';


    $sql = "SELECT COUNT(*) as cnt  FROM client $where" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $count_row=$result['data'][0]['cnt'];
    if($count_row==0)
    {
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listclients end', $_SESSION);
        return NULL;
    }
    // $sql = "SELECT * FROM DOC $where ORDER BY m.NAME ASC LIMIT $start, $limit " ;
    $sql = "SELECT client_id, `name`, tel, `e-mail` FROM client $where ORDER BY client_id ASC LIMIT $start, $limit " ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_listclients end', $_SESSION);
    return $result['data'];
}

function db_get_list1C4clients($clientID, $nullwrap=TRUE)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list1C4clients start', $_SESSION);
    $sql = "SELECT ID, DB_AC_IN,DB_AC_NUM FROM ORDER1C WHERE ORDER1C.CLIENT = (SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.client_id = $clientID) ORDER BY ORDER1C.DB_AC_IN DESC" ;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $out='';
    if($nullwrap)  $out.='<div class="form-group"><label for="select_orders">Выберите Заказ</label>';
    $out.='<select class="form-control" name="select_orders" id="select_orders" '.($nullwrap?'required':'').' >
            <option value="" selected>Не указано</option>';
    foreach ($result['data'] as $row)  $out.='<option value="'.$row['ID'].'">'.$row['DB_AC_NUM'].' - '.$row['DB_AC_IN'].'</option>';
    $out.='</select>';
    if($nullwrap)  $out.='</div>';
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list1C4clients end', $_SESSION);
    return $out;
}
function db_doc_set_production($docID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_doc_set_production start', $_SESSION);
    $sql="SELECT * FROM MATERIAL_KL_STOCK_MOVE WHERE Doc=$docID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $_arr_mat=array();
    foreach($result['data'] as $row)
    {
        $_arr_mat[]=array(
            'ID'=>$row['Material_client'],
            'COUNT'=>0-$row['Move'],
            'placesID'=>$row['Place'],
        );
    }
    $_4client=db_get_doc_details($docID);
    $clientID=$_4client[0]['Client'];
    $_arrOute=array();
    foreach ($_arr_mat as $oute)
    {
        $_arrOute[$oute['placesID']][$oute['ID']]=$oute['COUNT'];
    }
    $table = db_get_liststockOnClient($clientID);
    foreach($table as $line)
    {
        if(isset($_arrOute[$line['PLACES_ID']][$line['Material_client']]))
        {
            if($line['Count']+$_arrOute[$line['PLACES_ID']][$line['Material_client']]<0)
            {
                $_SESSION['messages']['errors'][]='Ошибка ввода данных! Отрицательный остаток недопустим!!';
                $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_doc_set_production end', $_SESSION);
                header('Location: '.$main_dir . '/clients/into_production.php'.'?nw='.$_GET['nw']);
                exit();
            }
        }
    }

    $sql = "DELETE  FROM MATERIAL_KL_STOCK_MOVE WHERE Doc=$docID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    db_materials_stock_move_add($_arr_mat, $docID);
    $sql = "UPDATE DOC SET Type='out' WHERE d_id=$docID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    db_materials_stock_upgate($clientID);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_doc_set_production end', $_SESSION);
}
function db_get_doc4production($num_doc,$placeID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_doc4production start', $_SESSION);
    $sql = "SELECT DOC.d_id, client.`name` AS cname, DOC.Datetime, manager.`name` AS mname, DOC.Order1C 
            FROM DOC
            INNER JOIN client ON client.client_id = DOC.Client
            INNER JOIN manager ON manager.id = DOC.Manager
            WHERE DOC.d_id = $num_doc AND DOC.Type = 'plan' AND DOC.Order1C>0 AND (SELECT SUM(MATERIAL_KL_STOCK_MOVE.Move) AS ssum 
            FROM MATERIAL_KL_STOCK_MOVE WHERE MATERIAL_KL_STOCK_MOVE.Doc = $num_doc) >0 AND (SELECT COUNT(*) FROM MATERIAL_KL_STOCK_MOVE AS msm
            WHERE msm.Doc =  $num_doc AND msm.Place = $placeID) >0" ;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_doc4production end', $_SESSION);
    if($result['count']>0) return $result['data'][0]; else return array();
}

function get_listWriteOff($start_in, $limit, &$total_records, $_where)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_listWriteOff start', $_SESSION);
    $_where[]=' stc.Count>0 ';
    $where=implode(' AND ',$_where);
    $start=($start_in-1)*$limit;
    $sql = "SELECT COUNT(*) AS cnt FROM MATERIAL_KL_STOCK_COUNT AS stc 
INNER JOIN client AS cln ON cln.client_id = stc.Client
                INNER JOIN MATERIAL_CLIENT AS mtc ON mtc.mc_id = stc.Material_client WHERE $where";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $total_records = $result['data'][0]['cnt'];

    $_op_maxdate=' (SELECT MAX(DOC.Datetime) FROM MATERIAL_KL_STOCK_MOVE  AS stm 
INNER JOIN DOC ON DOC.d_id = stm.Doc 
WHERE stm.Material_client=stc.Material_client AND 
stm.Place=stc.Place
) AS maxdate, ';
    $sql="SELECT  DISTINCTROW $_op_maxdate  DOC.d_id, stc.Material_client, stc.Client, stc.Count, stc.Place, cln.`Name` AS cname,
                mtc.W, mtc.L, mtc.T, mtc.Code_mat, mtc.`CODE`, mtc.`Name` AS mt_name, MATERIAL.`NAME` AS mto_name, mccs.cell, 
                ( -- Объединяем значения из поля api_part_ids в одну строку
                    -- SELECT GROUP_CONCAT(stm.api_part_ids)
                    SELECT stm.api_part_ids
                    FROM MATERIAL_KL_STOCK_MOVE AS stm 
                    INNER JOIN DOC ON DOC.d_id = stm.Doc 
                    WHERE stm.Material_client = stc.Material_client 
                        AND stm.Place = stc.Place
                        -- AND stm.Client = stc.Client
                        AND stm.Move > 0
                        AND stm.api_part_ids <> ''
                        LIMIT 1
                ) AS api_part_ids -- Добавляем поле api_part_ids в виде строки с объединенными значениями
                FROM MATERIAL_KL_STOCK_COUNT AS stc
                INNER JOIN client AS cln ON cln.client_id = stc.Client
                INNER JOIN MATERIAL_CLIENT AS mtc ON mtc.mc_id = stc.Material_client 
                LEFT JOIN MATERIAL ON MATERIAL.`CODE` = mtc.`CODE`  
	            INNER JOIN MATERIAL_CLIENT_CELL_STOCK as mccs ON mtc.mc_id = mccs.material_client
	            INNER JOIN DOC ON cln.client_id = DOC.Client AND mccs.Doc = DOC.d_id  
                WHERE $where 
           ORDER BY maxdate DESC LIMIT $start, $limit";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_listWriteOff end', $_SESSION);
    return $result['data'];
}

function get_list2browse_pager2($url, $start_in, $limit, $total_records)
{
  //  $start_in=$start_in+1;
    $links=2;
//    https://code.tutsplus.com/ru/tutorials/how-to-paginate-data-with-php--net-2928

    $last       = ceil($total_records / $limit );

    $start      = ( ( $start_in - $links ) > 0 ) ? $start_in - $links : 1;
    $end        = ( ( $start_in + $links ) < $last ) ? $start_in + $links : $last;

    $html       = '<nav aria-label="Page navigation example">  <ul class="pagination pagination-sm justify-content-center">';

    $class      = ( $start_in == 1 ) ? "disabled" : "";
    $html       .= '<li class="page-item ' . $class . '"><a  class="page-link" href="'.$url.'page='. ( $start_in - 1 ).'">&laquo;</a></li>';

    if ( $start > 1 ) {
        $html   .= '<li class="page-item"><a  class="page-link" href="'.substr($url,0,strlen($url)-1).'">1</a></li>';
        $html   .= '<li class="page-item disabled"><span class="page-link" >...</span></li>';
    }

    for ( $i = $start ; $i <= $end; $i++ ) {
        $class  = ( $start_in == $i ) ? "active" : "";
        $html   .= '<li class="page-item ' . $class . '"><a  class="page-link" href="'.$url.($i==1?substr($url,0,strlen($url)-1):'page='.$i).'">'.$i.'</a></li>';
    }

    if ( $end < $last ) {
        $html   .= '<li class="page-item disabled"><span class="page-link" >...</span></li>';
        $html   .= '<li class="page-item"><a  class="page-link" href="'.$url.'page='.$last.'">'.$last.'</a></li>';
    }

    $class      = ( $start_in == $last ) ? "disabled" : "";
    $html       .= '<li class="page-item ' . $class . '"><a  class="page-link" href="'.$url.'page='. ( $start_in + 1 ) .'">&raquo;</a></li>';

    $html       .= '</ul></nav>';
    return $html;
}


function get_list2browse_pager_btn($start_in, $limit, $total_records)
{
    $links=2;

    $last       = ceil($total_records / $limit );

    $start      = ( ( $start_in - $links ) > 0 ) ? $start_in - $links : 1;
    $end        = ( ( $start_in + $links ) < $last ) ? $start_in + $links : $last;

    $html       = '<nav aria-label="Page navigation example">  <ul class="pagination pagination-sm justify-content-left">';

    $class      = ( $start_in == 1 ) ? "disabled" : "";
    $html       .= '<li class="page-item ' . $class . '"><button type="submit" name="pager" value="'. ( $start_in - 1 ).'" class="page-link ">&laquo;</button></li>';

    if ( $start > 1 ) {
        $html   .= '<li class="page-item"><button type="submit" name="pager" value="1" class="page-link ">1</button></li>';
        $html   .= '<li class="page-item disabled"><span class="page-link" >...</span></li>';
    }

    for ( $i = $start ; $i <= $end; $i++ ) {
        $class  = ( $start_in == $i ) ? "active" : "";
        $html   .= '<li class="page-item ' . $class . '">
        <button type="submit" name="pager" value="'.$i.'" class="page-link ">'.$i.'</button></a></li>';
    }

    if ( $end < $last ) {
        $html   .= '<li class="page-item disabled"><span class="page-link" >...</span></li>';
        $html   .= '<li class="page-item"><button type="submit" name="pager" value="'.$last.'" class="page-link ">'.$last.'</button></li>';
    }

    $class      = ( $start_in == $last ) ? "disabled" : "";
    $html       .= '<li class="page-item ' . $class . '"><button type="submit" name="pager" value="'.( $start_in + 1 ).'" class="page-link ">&raquo;</button></li>';

    $html       .= '</ul></nav>';
    return $html;
}

function get_user_code($uid)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_user_code start', $_SESSION);
    $sql = "SELECT clcd.code_ac AS `code`, clcd.`name` AS `name` FROM client AS cl 
INNER JOIN client_code AS clcd ON cl.`code` = clcd.code_ac WHERE cl.client_id = $uid";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_user_code end', $_SESSION);
    if($result['count']==1)
        return $result['data'][0];
    else
        return null;
}
function db_get_cells($placeID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_cells start', $_SESSION);
    $sql = 'SELECT ctock_cells.* , (SELECT COUNT(*) FROM MATERIAL_CLIENT_CELL_STOCK AS mccs
                WHERE mccs.cell = ctock_cells.id) as cnt FROM ctock_cells WHERE place = ' . $placeID . ' ORDER BY id';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_cells end', $_SESSION);
    return $result['data'];
}
function db_change_cells($newCell,$place,$uid,$mid)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_change_cells start', $_SESSION);
    $sql="UPDATE `MATERIAL_CLIENT_CELL_STOCK` INNER JOIN DOC ON DOC.d_id = MATERIAL_CLIENT_CELL_STOCK.Doc 
INNER JOIN MATERIAL_KL_STOCK_MOVE ON MATERIAL_KL_STOCK_MOVE.Doc = DOC.d_id SET `cell`=$newCell 
WHERE MATERIAL_KL_STOCK_MOVE.Place = $place AND DOC.Client = $uid  AND MATERIAL_CLIENT_CELL_STOCK.material_client = $mid ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_change_cells end', $_SESSION);
}
function db_get_allCells()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_allCells start', $_SESSION);
    $out=array();
    $sql = 'SELECT * FROM ctock_cells  ORDER BY id';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    foreach($result['data'] as $row)  {
        $out[$row['place']][$row['id']]='№'.$row['id'].' '.$row['cell_name'].' ('.$row['X'].'*'.$row['Y'].'*'.$row['Z'].') %'.$row['percents'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_allCells end', $_SESSION);
    return $out;
}
function createSelect($in, $Place, $selected, $mID, $uID)
{
    // createSelect($_arr_cells,$row['Place'], $row['cell'],$row['mc_id']);
    $out='<select class="form-control " name="cells[]" data-uid="'.$uID.'" data-place="'.$Place.'" data-mid="'.$mID.'" required="required">';
    foreach ($in[$Place] as $key=>$value){
        $_add='';
        if($key==$selected) $_add='selected=""';
        $out.='<option value="'.$key.'" '.$_add.'>'.$value.'</option>';
    }
        $out.='</select>';
    return $out;

}
function db_cells_add($cells_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_cells_add start', $_SESSION);
    $sql1='INSERT INTO ctock_cells (place, cell_name, X, Y, Z, percents) VALUES ';
    $sql=$sql1.implode(',',$cells_in).';';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_cells_add end', $_SESSION);
}
function db_cells_erase($cells_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_cells_erase start', $_SESSION);
    $sql="DELETE FROM ctock_cells WHERE id IN (".implode(',',$cells_in).")";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_cells_erase end', $_SESSION);
}
function db_cells_update($cells_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_cells_update start', $_SESSION);
        foreach($cells_in as $sql) {
            $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_cells_update end', $_SESSION);
}
function create_ini(array $a, array $parent = array())
{
    $out = '';
    foreach ($a as $k => $v)
    {
        if (is_array($v))
        {
            $sec = array_merge((array) $parent, (array) $k);
            $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
            $out .= create_ini($v, $sec);
        }
        else
        {
            $out .= "$k=$v" . PHP_EOL;
        }
    }
    return $out;
}
function save_ini($in)
{
    $inisave = create_ini($in);
    if(!is_writable(__DIR__."/config.ini")) return FALSE;
    $file_handle = fopen(__DIR__."/config.ini", "w");
    fwrite($file_handle, $inisave);
    fclose($file_handle);
    return TRUE;
}
function load_ini()
{
    $setting=parse_ini_file(__DIR__."/config.ini",TRUE);
    return $setting;

}



////////// ORDERS ///////////


//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;
//require '../PHPMailer/Exception.php';
//require '../PHPMailer/PHPMailer.php';
//require '../PHPMailer/SMTP.php';

function send_text_mail($text, $adress, $adress_name, $sender_adress, $sender_name) {
    $mail = new PHPMailer;
    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->isSMTP();
/////    $mail->SMTPDebug = 2;
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = "v-mx.dn-kronas.local";
    $mail->Port = "25"; // typically 587
    $mail->SMTPSecure = 'tls'; // ssl is depracated
    $mail->SMTPAuth = true;
    $mail->Username = "varvarich";
    $mail->Password = "qQ12345678!";

    $mail->setFrom($sender_adress, $sender_name);
    $mail->addAddress($adress, $adress_name);

    $mail->Subject = 'KRONAS: Восстановление пароля';
    $mail->msgHTML($text); // remove if you do not want to send HTML email
    $mail->AltBody = 'Ваш пароль успешно восстановлен';
    ///$mail->addAttachment('docs/brochure.pdf'); //Attachment, can be skipped

    $mail->send();
}

function db_get_orders($client_code, $page, $date = 'all', $status = 'all', $type = 'all')
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4newdataOnWord start', $_SESSION);
    $filter_date = '';
    $filter_status = '';
    $filter_type = $type;
    $_where='';
    if($type==7)
    {
        $type='all';
        $filter_type='all';
        $_where=' AND `o`.`rs_order`=1 ';
    }
    if($date != 'all') {

        $post_date = new DateTime();
        $new_date = date_sub($post_date, date_interval_create_from_date_string($date.' days'));
        $post_date = date_format($new_date,"Y-m-d H:i:s");
        $filter_date = 'AND `DB_AC_IN` between "'.$post_date.'" and "'.date("Y-m-d H:i:s").'"';

    }
    if($status != 'all') {
        $filter_status = 'AND `status` = "'.$status.'"';
    }

    if($page == 'all') {
        $limits = '';
    } else {
        $limits = 'LIMIT 25 OFFSET '.($page-1)*25;
    }

    $sql = 'SELECT `o`.`ID`, `o`.`DB_AC_NUM`, `o`.`DB_AC_IN`, `o`.`DB_AC_NUM`, `o`.`DB_AC_ID`, `o`.`status`, `c`.`name` as client_name, `p`.`NAME`, `m`.`phone`, `m`.`e-mail`, `m`.`name`, `o`.`rs_order`  
            FROM `ORDER1C` as `o`
            JOIN `manager` as `m` ON (o.manager = m.id) 
            JOIN PLACES as `p` ON o.place = p.PLACES_ID 
            JOIN client as `c` ON o.CLIENT_ID = c.client_id
            WHERE `CLIENT` = "'.$client_code.'" 
            '.$filter_date.'
            '.$filter_status.'
            '.$_where.'
            ORDER BY `o`.`DB_AC_IN` DESC
            '.$limits;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $orders_db = $result['data'];



    $orders = [];

    foreach ($orders_db as $order) {
        $check_project = db_get_project($order['ID']);
        if(is_array($check_project)) {
            ///edit_links
            $edit_links = [];
            foreach ($check_project as $links) {
                if($links['manager_id'] > 0) {
                    $author = get_auth_manager('`id` = '.$links['manager_id'])['name'];
                } else if($links['client_id'] > 0) {
                    $author = 'Клиент';
                } else {
                    $author = '';
                }
                $edit_links[] = [
                    'link' => $links['PROJECT_ID'],
                    'date' => $links['DATE'],
                    'author' => $author
                ];
            }
           // _pre($edit_links);//9934193
            if ($p_data = get_Project_file($check_project[0]['PROJECT_ID'])) {
                $check_project[0]['MY_PROJECT_OUT'] = $p_data['project_data'];
            } else $check_project[0]['MY_PROJECT_OUT'] = null;
            $order['MY_PROJECT_OUT'] = $check_project[0];
            $order['edit_links'] = $edit_links;
        }
        $check_parts = db_get_parts($order['ID']);


        if($filter_type == 1 && count($check_parts) == 0) {
            continue;
        }
        if($filter_type == 5 && count($check_parts) > 0) {
            continue;
        }

        if(is_array($check_parts)) {
            $order['parts'] = count($check_parts);
        }
        $orders[] = $order;
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4newdataOnWord end', $_SESSION);
    return $orders;
}

function db_get_orders_count($client_code, $page, $date, $status, $type) {

    $orders = db_get_orders($client_code, 'all', $date, $status, $type);

    return count($orders);
}

function db_get_parts($order) {
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_parts start', $_SESSION);
    $sql = 'SELECT `PART_ID`,`NAME`,`DESCRIPTION`,`W`,`L`,`T` 
            FROM `PART`
            WHERE `ORDER1C` = "'.$order.'"';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_parts end', $_SESSION);
    return $result['data'];
}

function db_get_project($order) {
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_project start', $_SESSION);
    $sql = 'SELECT * 
            FROM `PROJECT`
            WHERE `ORDER1C` = "'.$order.'"
            ORDER BY `DATE` DESC';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_project end', $_SESSION);
    return $result['data'];
}

function db_get_receipt($order) {
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_receipt start', $_SESSION);
    $sql = 'SELECT `o`.`ORDER1C_GOODS_ID`, `b`.`NAME` as `band_name`, `m`.`NAME` as `material_name`, `s`.`NAME` as `simple_name`, `v`.`NAME` as `service_name`,
            `m`.`MATERIAL_ID`, `b`.`BAND_ID`, `s`.`SIMPLE_ID`, `v`.`SERVICE_ID`, `o`.`PRICE`, `o`.`COUNT` 
            FROM `ORDER1C_GOODS` as `o`
            LEFT JOIN  MATERIAL as `m` ON o.MATERIAL_ID = m.MATERIAL_ID 
            LEFT JOIN BAND as `b` ON o.BAND_ID = b.BAND_ID 
            LEFT JOIN SIMPLE as `s` ON o.SIMPLE_ID = s.SIMPLE_ID
            LEFT JOIN SERVICE as `v` ON o.SERVICE_ID = v.SERVICE_ID
            WHERE `ORDER1C_GOODS_ID` = "'.$order.'"';

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

    $db_data = $result['data'];

    $new_data = [];

    foreach($db_data as $part) {

        $type = '';
        $part_id = 0;
        $name = '';
        if($part['MATERIAL_ID']) {
            $type = 'Плитный материал';
            $part_id = $part['MATERIAL_ID'];
            $name =$part['material_name'];
        } else if($part['BAND_ID']) {
            $type = 'Кромка';
            $part_id = $part['BAND_ID'];
            $name =$part['band_name'];
        } else if($part['SIMPLE_ID']) {
            $type = 'Материал';
            $part_id = $part['SIMPLE_ID'];
            $name =$part['simple_name'];
        } else if($part['SERVICE_ID']) {
            $type = 'Услуга';
            $part_id = $part['SERVICE_ID'];
            $name =$part['service_name'];
        }

        $new_data[] = [
            'order_id' => $part['ORDER1C_GOODS_ID'],
            'count' => $part['COUNT'],
            'cost' => $part['PRICE'],
            'price' => $part['COUNT']*$part['PRICE'],
            'id' => $part_id,
            'type' => $type,
            'name' => $name,
        ];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_receipt end', $_SESSION);
    return $new_data;
}

// Ну вот нахрена городить одни и те же функции???
function print_r_r_($array)
{
    echo '<pre>';
    print_r($array);
    echo  '</pre>';
}

function get_client_password($data) {
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_client_password start', $_SESSION);
    if($data['email']) $where = '`e-mail` = "'.$data['email'].'"';
    if($data['phone']) $where = '`tel` = "'.$data['phone'].'"';
    $sql = 'SELECT * FROM `client` WHERE '.$where;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_client_password end', $_SESSION);
    return $result['data'];
}
function update_client_password($c_id, $pass) {
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'update_client_password start', $_SESSION);
    $sql = 'UPDATE `client` SET `pass` = "'.$pass.'" WHERE `client_id` = '.$c_id;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'update_client_password end', $_SESSION);
}
function send_client_pin($tel) {
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'update_client_password start', $_SESSION);
    $sql = 'SELECT * FROM `client` WHERE `tel` = "'.trim($tel).'"';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $client = $result['count'];
    if($client > 0) {

        $new_pin = mt_rand(12345, 99999);
        $xml = '<?xml version="1.0" encoding="utf-8" ?><package key="3ae50516fed179f981adf58f1defa7e557de189e"><message><msg recipient="'.$tel.'" sender="KRONAS" type="0">ПИН-код для авторизации: '.$new_pin.'</msg></message></package>';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://alphasms.ua/api/xml.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$xml,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/xml; charset=UTF8",
                "Content-Type: text/plain"
            ),
        ));

        $response = curl_exec($curl);

        $client_data = $result['data'];

        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'send_client_pin end', $_SESSION);
        return $new_pin;

    } else {
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'send_client_pin end', $_SESSION);
        return false;
    }

}
function check_production_client()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_production_client start', $_SESSION);
    $setting=load_ini();
    $user_in_production= (int) $setting['misc']['user_in_production'];
    if($user_in_production<1)
    {
        $_SESSION['messages']['errors'][] = 'В системе не обнаружен ID клиента "производство"! Обратитесь к администратору!';
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_production_client end', $_SESSION);
        return false;
    }

    $sql = "SELECT COUNT(*) AS cnt FROM client WHERE client_id=".$user_in_production;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'check_production_client end', $_SESSION);
    if($result['data'][0]['cnt']<1)
    {
        $_SESSION['messages']['errors'][] = 'ID клиента "производство" не найден в БД! Обратитесь к администратору!';
        return false;
    }
    return true;
}
function db_get_orders2($filtr, $start, $limit, &$total_records)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_orders2 start', $_SESSION);
    $where='';
    if(count($filtr)>0) $where=' WHERE '. implode(' AND ',$filtr).' ';
    $_stringSQL=array(
        'o.ID, o.DB_AC_NUM, o.DB_AC_IN, o.DB_AC_NUM, o.DB_AC_ID, o.status, 
  c.name AS client_name, p.NAME, m.phone, m.`e-mail`, m.name, o.rs_order ',
        'FROM ORDER1C AS o JOIN manager AS m ON ( o.manager = m.id ) 
 JOIN PLACES AS p ON o.place = p.PLACES_ID JOIN client AS c ON o.CLIENT_ID = c.client_id ',
        'ORDER BY o.DB_AC_IN DESC'
    );
    $sql = "SELECT COUNT(*) as cnt  $_stringSQL[1] $where" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    if($result['data'][0]['cnt']==0)
    {
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_orders2 end', $_SESSION);
        return NULL;
    }
    $sql = "SELECT $_stringSQL[0] $_stringSQL[1] $where $_stringSQL[2] LIMIT $start, $limit " ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_orders2 end', $_SESSION);
    return $result['data'];
}

function db_get_orders_add(&$orders)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_orders_add start', $_SESSION);
    $_arrID=array();
    foreach ($orders as $order)
    {
        if(!in_array($order['ID'])) $_arrID[]=$order['ID'];
    }
    $sql = 'SELECT * FROM PROJECT WHERE ORDER1C IN ('.implode(',',$_arrID).') ORDER BY `DATE` DESC';
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $db_data = $result['data'];
    //_pre($db_data);
    $new_data = array();
    foreach($db_data as $check_project) {
        $new_data[$check_project['ORDER1C']][]=$check_project;
    }

    foreach ($orders as $key=>$order) {
        if(key_exists($order['ID'],$new_data))
        {
            $edit_links = [];
            foreach ($new_data[$order['ID']] as  $links) {
                //              _pre($links);
                if ($links['manager_id'] > 0) {
                    $author = get_auth_manager('`id` = ' . $links['manager_id'])['name'];
                } else if ($links['client_id'] > 0) {
                    $author = 'Клиент';
                } else {
                    $author = '';
                }
                $author='REM';
                $edit_links[] = [
                    'link' => $links['PROJECT_ID'],
                    'date' => $links['DATE'],
                    'author' => $author
                ];
            }
            $orders[$key]['MY_PROJECT_OUT'] = $new_data[$order['ID']][0];
            $orders[$key]['edit_links'] = $edit_links;

        }
    }
    foreach($db_data as $check_project) {
        if(is_array($check_project)) {
            $order=array();
            $edit_links = [];
            $order['MY_PROJECT_OUT'] = $check_project[0];
            $order['edit_links'] = $edit_links;
            $new_data[$check_project['ORDER1C']][]=$order;

        }
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_orders_add end', $_SESSION);
}


function db_update_settings($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_settings start', $_SESSION);
    foreach ($in as $key=>$value)
    {
        $result=sql_data(__LINE__,__FILE__,__FUNCTION__,"UPDATE RS_general SET `value` = '$value' WHERE `name` = '$key'");
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_settings end', $_SESSION);
}

function db_get_planDocDetails($docID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_planDocDetails start', $_SESSION);
    $sql = "SELECT d.Datetime, d.Type, mksm.Material_client, mksm.Doc, mksm.Move, mksm.Place, d.Order1C 
            FROM DOC AS d INNER JOIN MATERIAL_KL_STOCK_MOVE AS mksm ON d.d_id = mksm.Doc
            WHERE d.d_id = $docID" ;

    $_rows =sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_planDocDetails end', $_SESSION);
    return $_rows;
}
function db_erase_doc_move($docID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_erase_doc_move start', $_SESSION);
    $sql="DELETE FROM MATERIAL_KL_STOCK_MOVE WHERE Doc = $docID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_erase_doc_move end', $_SESSION);
}
function db_erase_doc($docID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_erase_doc start', $_SESSION);
    $sql="DELETE FROM DOC WHERE d_id = $docID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_erase_doc end', $_SESSION);
}
function db_update_move_doc($docID, $move, $place, $mc_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_move_doc start', $_SESSION);
    $sql="UPDATE MATERIAL_KL_STOCK_MOVE SET  Move = $move WHERE Doc = $docID AND Place=$place AND Material_client=$mc_id; ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_move_doc end', $_SESSION);
}

function db_update_matcl_ord1c($mat_cl_id, $order1c, $count)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_matcl_ord1c start', $_SESSION);
    $sql="UPDATE MATERIAL_CLIENT_ORDER1C SET  `count` = $count WHERE mat_cl_id = $mat_cl_id AND order1c=$order1c; ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_matcl_ord1c end', $_SESSION);
}

function db_erase_matcl_ord1c($mat_cl_id, $order1c)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_erase_matcl_ord1c start', $_SESSION);
    $sql="DELETE FROM MATERIAL_CLIENT_ORDER1C WHERE mat_cl_id = $mat_cl_id AND order1c=$order1c; ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_erase_matcl_ord1c end', $_SESSION);
}

function db_get_planDoc($matID, $clCODE, $place)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_planDoc start', $_SESSION);
    $sql = "SELECT d.d_id, mksm.Material_client, mksm.Move, mksm.Place, d.Order1C, mco1c.mat_cl_id, o1c.DB_AC_NUM, o1c.DB_AC_ID, cl.`code`, 
	        cl.`name` AS clname,  m.`name` AS mname, 
	mc.`Name` as matname, mc.W, mc.L, mc.T
            FROM DOC AS d
            INNER JOIN MATERIAL_KL_STOCK_MOVE AS mksm ON d.d_id = mksm.Doc
            INNER JOIN MATERIAL_CLIENT_ORDER1C AS mco1c ON d.Order1C = mco1c.order1c
            INNER JOIN ORDER1C AS o1c ON d.Order1C = o1c.ID AND mco1c.order1c = o1c.ID
            INNER JOIN client AS cl ON o1c.CLIENT_ID = cl.client_id AND d.Client = cl.client_id 
            INNER JOIN manager AS m ON d.Manager = m.id 
            INNER JOIN MATERIAL_CLIENT AS mc ON mksm.Material_client = mc.mc_id AND mco1c.mat_cl_id = mc.mc_id 
            WHERE d.Type = 'plan' AND mksm.Material_client = $matID AND mksm.Place = $place AND cl.`code` = $clCODE" ;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_planDoc end', $_SESSION);

    return $result['data'];
}

/*========================================
 for glass
========================================*/
// подключает js скрипты и стили для стеколки
function includeToHeaderForGlass() {
    echo "<link rel=\"stylesheet\" href=\"/clients/glass/assets/css/style.css\">" . PHP_EOL
        . "<script src=\"/clients/glass/assets/js/svg.min.js\"></script>" . PHP_EOL
        . "<script src=\"/clients/glass/assets/js/part_builder.js\"></script>" . PHP_EOL
        . "<script src=\"/clients/glass/assets/js/main.js\"></script>" . PHP_EOL;
}

/* ========================================
 for materials /settings.php
======================================== */
// возвращает все записи из таблицы material_general
function mat_get_material_general()
{
    require_once '../func.php';
    $sql = "SELECT * FROM material_general";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все филиалы
function mat_get_places()
{
    $sql = "SELECT  * FROM PLACES";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает теги option с филиалами для select
function mat_get_places_options()
{
    $places = mat_get_places();
    $options = "";
    foreach ($places as $place) {
        $options .= "<option value='{$place['PLACES_ID']}'>{$place['NAME']}</option>";
    }
    return $options;
}

// возвращает свойства товаров
function mat_get_good_properties()
{
    $sql = "SELECT prop.NAME, prop.ID, 	
            prop_mat.VALUESTR str, prop_mat.VALUEDIG dig FROM GOOD_PROPERTIES AS prop 
            JOIN GOOD_PROPERTIES_MATERIAL AS prop_mat ON prop.ID = prop_mat.PROPERTY_ID GROUP BY prop.NAME ORDER BY prop.ID";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все доступные значения свойств из объединенных таблиц good_properties и good_properties_material
// если передать не обязательній парамете $is_mat, то значения беруться из good_properties и good_properties_band
function mat_get_property_value_options($prop_name, $type, $good = 'mat')
{
    switch ($good) {
        case 'band':
            $join_table = "GOOD_PROPERTIES_BAND";
            break;
        case 'plinth':
            $join_table = "GOOD_PROPERTIES_SIMPLE";
            break;
        default:
            $join_table = "GOOD_PROPERTIES_MATERIAL";
    }
    $sql = "SELECT prop.ID, prop_mat.$type val FROM GOOD_PROPERTIES AS prop 
            JOIN $join_table AS prop_mat ON prop.ID = prop_mat.PROPERTY_ID 
            WHERE prop.NAME LIKE '%$prop_name%' GROUP BY prop_mat.$type";

    $_rows = sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
    $options = '';
    foreach ($_rows as $row) {
        $options .= "<option value='{$row['ID']}'>{$row['val']}</option>";
    }
    return $options;
}

// генерирует таблицу свойств
function mat_get_properties_inputs($good)
{
    switch ($good) {
        case 'band':
            $properties = band_get_good_properties();
            break;
        case 'plinth':
            $properties = plinth_get_good_properties();
            break;
        default:
            $properties = mat_get_good_properties();
    }
    $inputs = "<table class='table table-striped listProperties'><tbody>";
    foreach ($properties as $property) {
        if ($property['dig']) {
            $type = 'VALUEDIG';
        } else {
            $type = 'VALUESTR';
        }
        $inputs .= "<tr>
                        <td>{$property['NAME']}</td>
                        <td><select data-good='$good' data-typeval='$type' class='form-control' name='{$property['ID']}'><option>---</option></select></td>
                    </tr>";
    }
    $inputs .= "</tbody></table>";
    return $inputs;
}

// возврщает все записи дерева католога товаров
function mat_get_good_tree($parent_id = 0)
{
    $sql = "SELECT * FROM GOOD_TREE t WHERE t.PARENT >= $parent_id";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// формирует масив для выода дерева католога товаров
function mat_form_tree($data)
{
    if (!is_array($data)) return false;
    $tree = [];
    foreach ($data as $value) {
        $tree[$value['PARENT']][] = $value;
    }

    return $tree;
}

// строит и возвращает дерево каталога товаров
function mat_build_tree($tree, $parent_id = 0, $good = 'mat', $visible_folder = 0)
{
    if (is_array($tree) && isset($tree[$parent_id])) {

        $result = '<ul class="list-group">';
        foreach ($tree[$parent_id] as $item) {
            $hide = (intval($item['PARENT']) != $visible_folder) ? 'd-none' : '';
            $inner_ul = mat_build_tree($tree, $item['FOLDER_ID'], $good, $visible_folder);
            $class_folder = ($inner_ul) ? 'far' : 'fas';
            $result .= "<li data-good='$good' class=\"list-group-item $hide\" id=\"item_{$item['FOLDER_ID']}\" data-parent='{$item['PARENT']}'><i class=\"$class_folder fa-folder\"></i> {$item['NAME']}";
            $result .= $inner_ul;
            $result .= '</li>';
        }
        $result .= '</ul>';
    } else {
        return false;
    }

    return $result;

}
// возвращает все материалы по умолчанию при клики на вкладку материалов
function mat_get_materials_default()
{
    $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
            GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
            GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
            GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
            GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
            GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff 
            FROM MATERIAL AS mat 
            LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
            LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
            LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
            LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
            GROUP BY mat.MATERIAL_ID";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}
// возвращает материалы по филиалам
function mat_get_material_by_place($table, $place_id) {
    if ($table === 'material') {
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
            GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
            GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
            GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
            GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
            GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff 
            FROM MATERIAL AS mat 
            LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
            LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
            LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
            LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
            GROUP BY mat.MATERIAL_ID HAVING place_id != '1,2,3,4,5,6,7,8,9' OR place_id IS NULL";
    } else {
        $table = substr($table, strpos($table, '_') + 1);
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                GROUP BY mat.MATERIAL_ID HAVING $table like '%$place_id%'";
    }

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все материалы по id филиала и id папки зи таблицы good_tree
function mat_get_materials_by_place_and_category($table, $place_id, $folder_id)
{
    if ($table === 'material') {
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                JOIN GOOD_TREE_MATERIAL_CONN AS tr_mat ON mat.MATERIAL_ID = tr_mat.MATERIAL_ID
                JOIN GOOD_TREE AS gtr ON tr_mat.FOLDER_ID = gtr.FOLDER_ID 
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE gtr.FOLDER_ID = $folder_id
                GROUP BY mat.MATERIAL_ID  HAVING place_id != '1,2,3,4,5,6,7,8,9' OR place_id IS NULL";
    } else {
        $table = substr($table, strpos($table, '_') + 1);
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                JOIN GOOD_TREE_MATERIAL_CONN AS tr_mat ON mat.MATERIAL_ID = tr_mat.MATERIAL_ID
                JOIN GOOD_TREE AS gtr ON tr_mat.FOLDER_ID = gtr.FOLDER_ID 
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE gtr.FOLDER_ID = $folder_id
                GROUP BY mat.MATERIAL_ID  HAVING $table LIKE '%$place_id%'";
    }

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает материалы по свойстввам
function mat_get_materials_by_place_and_properties($table, $place_id, $json_properties)
{
    $properties = json_decode($json_properties, true);
    $added_sql = "";
    foreach ($properties as $property) {
        $value = " gp.{$property['type']} = ";
        if ($property['type'] === 'VALUESTR') {
            $value .= "'{$property['value']}'";
        } else {
            $value .= "{$property['value']}";
        }
        $added_sql .= "gp.PROPERTY_ID = {$property['prop_id']} AND $value";
        switch ($property['prop_id']) {
            case 2 :
                $added_sql .= " OR gp.PROPERTY_ID = 142 AND $value";
                break;
            case 33:
                $added_sql .= " OR gp.PROPERTY_ID = 168 AND $value";
                break;
            case 12:
                $added_sql .= " OR gp.PROPERTY_ID = 167 AND $value";
                break;
        }
        if (next($properties)) $added_sql .= " OR ";
    }

    if ($table === 'material') {
         $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE mat.MATERIAL_ID IN 
                            (SELECT gp.MATERIAL_ID FROM GOOD_PROPERTIES_MATERIAL AS gp WHERE $added_sql)
                GROUP BY mat.MATERIAL_ID HAVING place_id != '1,2,3,4,5,6,7,8,9' OR place_id IS NULL";
    } else {
        $table = substr($table, strpos($table, '_') + 1);
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE mat.MATERIAL_ID IN 
                            (SELECT gp.MATERIAL_ID FROM GOOD_PROPERTIES_MATERIAL AS gp WHERE $added_sql)
                GROUP BY mat.MATERIAL_ID  HAVING $table LIKE '%$place_id%'";
    }

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает материалы по коду
function mat_get_materials_by_place_and_code($table, $place_id, $code)
{
    if ($table === 'material') {
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE mat.CODE LIKE '%$code%'
                GROUP BY mat.MATERIAL_ID HAVING place_id != '1,2,3,4,5,6,7,8,9' OR place_id IS NULL";
    } else {
        $table = substr($table, strpos($table, '_') + 1);
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE mat.CODE LIKE '%$code%'
                GROUP BY mat.MATERIAL_ID HAVING $table LIKE '%$place_id%'";
    }

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}
// возвращает материалы по характеристики (MY_1C_HAR)
function mat_get_materials_by_place_and_har($table, $place_id, $har)
{
    if ($table === 'material') {
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE mat.MY_1C_HAR LIKE '%$har%'
                GROUP BY mat.MATERIAL_ID HAVING place_id != '1,2,3,4,5,6,7,8,9' OR place_id IS NULL";
    } else {
        $table = substr($table, strpos($table, '_') + 1);
        $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE, 
                GROUP_CONCAT(DISTINCT pl.NAME SEPARATOR ', ') places, 
                GROUP_CONCAT(DISTINCT pl.PLACES_ID SEPARATOR ',') place_id, 
                GROUP_CONCAT(DISTINCT mat_h.place_id SEPARATOR ',') half, 
                GROUP_CONCAT(DISTINCT mat_m2.place_id SEPARATOR ',') m2, 
                GROUP_CONCAT(DISTINCT mat_m2_d.place_id SEPARATOR ',') m2_diff
                FROM MATERIAL AS mat 
                LEFT JOIN MATERIAL_HALF mat_h ON mat.MATERIAL_ID = mat_h.material_id
                LEFT JOIN material_m2 mat_m2 ON mat.MATERIAL_ID = mat_m2.material_id
                LEFT JOIN material_m2_diff mat_m2_d ON mat.MATERIAL_ID = mat_m2_d.material_id
                LEFT JOIN PLACES pl ON (mat_h.place_id = pl.PLACES_ID or mat_m2.place_id = pl.PLACES_ID or mat_m2_d.place_id = pl.PLACES_ID) 
                WHERE mat.MY_1C_HAR LIKE '%$har%'
                GROUP BY mat.MATERIAL_ID HAVING $table LIKE '%$place_id%'";
    }

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// получаем все записи материала и филиала из таблицы material_m2_diff
function mat_get_m2_diff($place_id, $mat_id)
{
    $sql = "SELECT * FROM material_m2_diff where place_id = $place_id AND material_id = $mat_id";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// генерирует таблицу ванных по переданному массиву с материалами
function mat_get_materials_table($materials, $place)
{
    $places = mat_get_places();
    $data_place = [];
    foreach ($places as $place) {
        $data_place[$place['PLACES_ID']] = $place['NAME'];
    }
    $table = "<table class=\"table table-hover\">
                    <thead>
                    <tr>
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">CODE</th>
                        <th scope=\"col\">Название</th>
                        <th scope=\"col\" class='d-none'>Филиалы</th>
                        <th scope=\"col\">Продажа половинками</th>
                        <th scope=\"col\">Продажа м<sup>2</sup></th>
                        <th scope=\"col\">Продажа м<sup>2</sup> от размера детали</th>
                        <th scope=\"col\">Подробнее</th>
                    </tr>
                    </thead>
                    <tbody>";
    foreach ($materials as $material) {
        $table .= "<tr>
                    <th scope=\"row\">{$material['MATERIAL_ID']}</th>
                    <td scope=\"row\">{$material['CODE']}</td>
                    <td>{$material['NAME']}</td>";
        $table .= (!is_null($material['places'])) ? "<td class='d-none' data-placeid='{$material['place_id']}'>{$material['places']}</td>" : "<td class='d-none'>----</td>";
        $half = mat_get_name_place_by_id($data_place, $material['half']);
        $m2 = mat_get_name_place_by_id($data_place, $material['m2']);
        $m2_diff = mat_get_name_place_by_id($data_place, $material['m2_diff']);
        $table .= "<td class='half' data-placeid='{$material['half']}'>$half</td>
                   <td class='m2' data-placeid='{$material['m2']}'>$m2</td>
                   <td class='m2_diff' data-placeid='{$material['m2_diff']}'>$m2_diff</td>";

        $table .= "<td><button class='btn btn-success showMaterial'>Подробнее</button></td>";
        $table .= "</tr>";
    }
    $table .= "</tbody></table>";
    return $table;
}


function mat_get_name_place_by_id($places, $ids) {
    $result = "";
    if (is_null($ids)) {
        $result = "-----";
    } else {
        $id = explode(',', $ids);
        foreach ($id as $place_id) {
            $result .= "<p>" . $places[$place_id] . "</p>";
        }
    }
    return $result;
}

//.. 18.10.2023 Тоценко
/**
 * При выборе кратности продажи материала (включая выбранные филиалы) запрос идёт Роману
 * 
 * Пример передаваемых данных ($data)
 * Array (
 *  [materials] => [{"mat_id":"1","delete_from":{"half":"","m2":"","m2_diff":""}}]
 *  [places] => ["1","2","3"]
 *  [selling_principle] => half
 *  [update_selling] => true
 * )
 * ПОКА ЧТО НЕ РАБОТАЕТ!!! ЗАДАНИЕ БЫЛО, Я ВЫПОЛНИЛ, НО ПОТОМ ЗАВИСЛО
 */
function sendMaterialSalesRatio($data)
{
    $matAccent = null;
    $placesAccent = [];

    $materials = json_decode($data['materials'], true);
    $matId = (int) $materials[0]['mat_id'];
    $placeIds = json_decode($data['places'], true);
    $ratio = $data['selling_principle'];

    $q = "SELECT `CODE` FROM `MATERIAL` WHERE `MATERIAL_ID` = " . $matId . "";
    $res = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);
    $matAccent = (int) $res['data'][0]['CODE'];

    foreach ($placeIds as &$placeId) {
        $placeId = (int) $placeId;
        $q = "SELECT `AC_CODE` FROM `PLACES` WHERE `PLACES_ID` = " . $placeId . "";
        $res = sql_data(__LINE__, __FILE__, __FUNCTION__, $q);
        $placesAccent[] = (int) $res['data'][0]['AC_CODE'];
    }
    
    $outputData = [
        'material' => [
            'service' => $matId,
            'accent' => $matAccent
        ],
        'places' => [
            'service' => $placeIds,
            'accent' => $placesAccent
        ],
        'ratio' => $ratio
    ];

    // return json_encode($outputData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}

// удаляет связь материала с филиалом и принципом продажи
function mat_delete_selling_principle($places, $mat_id, $table)
{
    $places = explode(',', $places);
    if ($table === 'material_half') $table = mb_strtoupper($table);
    $count = 0;
    foreach ($places as $place) {
        $sql = "DELETE FROM $table WHERE place_id=$place AND material_id=$mat_id";
        if(sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)) $count++;
    }

    return $count == count($places);
}

// вставляет новые связи материалов с филиалами и принципом продажи
function mat_insert_selling_principle($places, $mat_ids, $table)
{
    $values = "VALUES ";
    foreach ($mat_ids as $mat_id) {
        foreach ($places as $place) {
            $values .= "($place, $mat_id)";
            if (next($places)) $values .= ", ";
        }
        if (next($mat_ids)) $values .= ", ";
    }
    if ($table === 'material_half') $table = mb_strtoupper($table);
    $sql = "INSERT INTO $table (place_id, material_id) $values";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}

// вставляет новые связи материалов с филиалами в таблицу materials_m2_diff
function mat_insert_in_m2_diff($places, $mat_ids, $data_m2_diff)
{
    $values = "VALUES ";
    foreach ($mat_ids as $mat_id) {
        foreach ($places as $place) {
            foreach ($data_m2_diff as $data) {
                $length = ($data['length'] !== "") ? $data['length'] : "null";
                $width = ($data['width'] !== "") ? $data['width'] : "null";
                $up_percent = ($data['up_percent'] !== "") ? $data['up_percent'] : "null";
                $up_uah = ($data['up_uah'] !== "") ? $data['up_uah'] : "null";
                $values .= "($place, 
                            $mat_id, 
                            $length, 
                            $width, 
                            {$data['area']}, 
                            $up_percent, 
                            $up_uah)";
                if (next($data_m2_diff)) $values .= ", ";
            }
            if (next($places)) $values .= ", ";
        }
        if (next($mat_ids)) $values .= ", ";
    }

    $sql = "INSERT INTO material_m2_diff (place_id, material_id, length, width, area, `up_%`, up_uah) $values";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}

/*====================================================
for band setting.php
======================================================*/
// возвращает свойства товаров
function band_get_good_properties()
{
    $sql = "SELECT prop.NAME, prop.ID, 	
        prop_band.VALUESTR str, prop_band.VALUEDIG dig FROM GOOD_PROPERTIES AS prop 
        JOIN GOOD_PROPERTIES_BAND AS prop_band ON prop.ID = prop_band.PROPERTY_ID GROUP BY prop.NAME ORDER BY prop.ID";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает список кромки по умолчанию
function band_get_band_default()
{
    $sql = "SELECT band.BAND_ID, band.CODE, band.KR_NAME, band.NAME, mbc.type_connection, mbc.in_price, mbc.fix_connection, 
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, 
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM BAND band
            LEFT JOIN MATERIAL_BAND_CONNECTION mbc ON band.BAND_ID = mbc.BAND_ID
            LEFT JOIN MATERIAL mat ON mbc.MATERIAL_ID = mat.MATERIAL_ID GROUP BY band.BAND_ID ORDER BY band.BAND_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// генерирует таблицу ванных по переданному массиву с материалами
function band_get_band_table($bands)
{
    $table = "<table class=\"table table-hover\">
                    <thead>
                    <tr>
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">CODE</th>
                        <th scope=\"col\">Название</th>
                        <th scope=\"col\">Материалы</th>
                        <th scope=\"col\">Подробнее</th>
                    </tr>
                    </thead>
                    <tbody>";
    foreach ($bands as $band) {
        $table .= "<tr>
                    <th scope=\"row\">{$band['BAND_ID']}</th>
                    <td scope=\"row\">{$band['CODE']}</td>
                    <td>{$band['NAME']}</td>";
        $mat_list = "<ol>";
        foreach (explode(', ', $band['mat']) as $mat) {
            $mat_list .= "<li>$mat</li>";
        }
        $mat_list .= "</ol>";

        $table .= (!is_null($band['mat'])) ? "<td data-matids='{$band['mat_id']}'>$mat_list</td>" : "<td>----</td>";
        $table .= "<td><button class='btn btn-success showBand'>Подробнее</button></td>";
        $table .= "</tr>";
    }
    $table .= "</tbody></table>";
    return $table;
}

function band_get_band_by_tree($folder_id)
{
    $sql = "SELECT band.BAND_ID, band.CODE, band.NAME,
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM BAND band  
            LEFT JOIN MATERIAL_BAND_CONNECTION mbc ON band.BAND_ID = mbc.BAND_ID
            LEFT JOIN MATERIAL mat ON mbc.MATERIAL_ID = mat.MATERIAL_ID 
            INNER JOIN GOOD_TREE_BAND_CONN gtr ON band.BAND_ID = gtr.BAND_ID
            INNER JOIN GOOD_TREE gt ON gtr.FOLDER_ID = gt.FOLDER_ID WHERE gtr.FOLDER_ID = $folder_id 
            GROUP BY band.BAND_ID ORDER BY band.BAND_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все записи кромки по свойствам
function band_get_band_by_properties($properties)
{
    $add_sql = "";
    foreach ($properties as $property) {
        $add_sql .= "gpb.PROPERTY_ID = {$property['prop_id']} ";

        $value = ($property['type'] === 'VALUESTR') ? "gpb.VALUESTR LIKE '{$property['value']}'" : "gpb.VALUEDIG = {$property['value']}";
        $add_sql .= "AND $value";
        switch ($property['prop_id']) {
            case 142 :
                $add_sql .= " OR gpb.PROPERTY_ID = 2 AND $value";
                break;
            case 168:
                $add_sql .= " OR gpb.PROPERTY_ID = 33 AND $value";
                break;
            case 12:
                $add_sql .= " OR gpb.PROPERTY_ID = 167 AND $value";
                break;
        }
        if (next($properties)) $add_sql .= " OR ";
    }

    $sql = "SELECT band.BAND_ID, band.CODE, band.NAME,
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM BAND band  
            LEFT JOIN MATERIAL_BAND_CONNECTION mbc ON band.BAND_ID = mbc.BAND_ID
            LEFT JOIN MATERIAL mat ON mbc.MATERIAL_ID = mat.MATERIAL_ID 
            WHERE band.BAND_ID IN (
				SELECT gpb.BAND_ID FROM GOOD_PROPERTIES_BAND gpb WHERE $add_sql
				) GROUP BY band.BAND_ID ORDER BY band.BAND_ID ASC ";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// поиск кромкит по коду
function band_get_band_by_code($code)
{
    $where = ($code === 0) ? "" : " WHERE band.CODE LIKE '%$code%'";

    $sql = "SELECT band.BAND_ID, band.CODE, band.NAME,
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM BAND band 
            LEFT JOIN MATERIAL_BAND_CONNECTION mbc ON band.BAND_ID = mbc.BAND_ID
            LEFT JOIN MATERIAL mat ON mbc.MATERIAL_ID = mat.MATERIAL_ID 
            $where GROUP BY band.BAND_ID ORDER BY band.BAND_ID ASC ";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}
// поиск кромки по наименованию
function band_get_band_by_name($name)
{
    $where = ($name === '') ? "" : " WHERE band.NAME LIKE '%$name%'";
    $sql = "SELECT band.BAND_ID, band.CODE, band.NAME,
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM BAND band  
            LEFT JOIN MATERIAL_BAND_CONNECTION mbc ON band.BAND_ID = mbc.BAND_ID
            LEFT JOIN MATERIAL mat ON mbc.MATERIAL_ID = mat.MATERIAL_ID 
            $where GROUP BY band.BAND_ID ORDER BY band.BAND_ID ASC ";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все материалы связанные с кромкой
function band_get_band_mat_by_band_id($band_id)
{
    $sql = "SELECT DISTINCT mat.MATERIAL_ID, mat.CODE, mat.NAME, 
            mat_band.type_connection type_conn, mat_band.in_price, mat_band.fix_connection fix_conn FROM MATERIAL mat
            JOIN MATERIAL_BAND_CONNECTION mat_band ON (mat_band.MATERIAL_ID = mat.MATERIAL_ID)
            WHERE mat_band.BAND_ID = $band_id";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// get all materials by NAME, CODE, MY_1C_HAR
function band_get_materials_by_filter($filter_text, $table_top = false)
{
    $for_table_top = (!$table_top) ? '': ' AND mat.ST = 1';
    $name = "mat.NAME LIKE '%$filter_text%'";
    $code = (is_numeric($filter_text)) ? " OR mat.CODE LIKE '%$filter_text%'" : "";
    $har = (is_numeric($filter_text)) ? " OR mat.MY_1C_HAR LIKE '%$filter_text%'" : "";
    $sql = "SELECT mat.MATERIAL_ID, mat.NAME, mat.CODE FROM MATERIAL AS mat WHERE $name $code $har $for_table_top";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// gets list of materials
function band_get_list_of_materials($materials)
{
    $list_mat = "";
    foreach ($materials as $material) {
        $list_mat .= "<li data-matid='{$material['MATERIAL_ID']}' data-matcode='{$material['CODE']}'>
                        <button class='btn-sm btn-secondary add-relationship-mat mr-2 d-none'><i class='fas fa-plus'></i></button>
                        {$material['NAME']}
                    </li>";
    }
    return $list_mat;
}

// updates relations band-material
function band_update_relations_with_mat($band_ids, $materials)
{

    if (!$band_ids[0]['list_band']) {
        unset($band_ids[0]);
        $add_mat = [];
        $update_mat = [];
        foreach ($materials as $mat) {
            if ($mat['fresh_mat']) {
                array_push($add_mat, $mat);
            } else {
                array_push($update_mat, $mat);
            }
        }
        if (!empty($update_mat)) good_update_mat_relation_conn($band_ids[1]['band_id'], $update_mat);
        if (!empty($add_mat)) good_add_mat_relation_conn($band_ids[1]['band_id'], $add_mat);
    } else {
        unset($band_ids[0]);
        foreach ($band_ids as $band) {
            if ($band['mat_ids'] !== 'undefined') {
                $add_mat = [];
                $update_mat = [];
                foreach ($materials as $material) {
                    if (strpos($band['mat_ids'], strval($material['mat_id'])) !== false) {
                        array_push($update_mat, $material);
                    } else {
                        array_push($add_mat, $material);
                    }
                }
                if (!empty($update_mat)) good_update_mat_relation_conn($band['band_id'], $update_mat);
                if (!empty($add_mat)) good_add_mat_relation_conn($band['band_id'], $add_mat);
            } else {
                good_add_mat_relation_conn($band['band_id'], $materials);
            }
        }
    }
    $ids = "";
    foreach ($band_ids as $id) {
        $ids .= (next($band_ids)) ? "{$id['band_id']}, ": $id['band_id'];
    }
    $sql_get_band = "SELECT band.BAND_ID, band.CODE, band.KR_NAME, band.NAME, mbc.type_connection, mbc.in_price, mbc.fix_connection,
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id,
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM BAND band
            LEFT JOIN MATERIAL_BAND_CONNECTION mbc ON band.BAND_ID = mbc.BAND_ID
            LEFT JOIN MATERIAL mat ON mbc.MATERIAL_ID = mat.MATERIAL_ID
            WHERE band.BAND_ID IN ($ids)
            GROUP BY band.BAND_ID ORDER BY band.BAND_ID ASC";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql_get_band)['data'];
}
// end band settings

/*===========================================================================
the common functions for the band and the plinth
=============================================================================*/
/**
 * generates table of relations of there materials
 * @param $materials array list of materials
 * @param $good_class string this is the html class
 * @return string
 */
function good_get_relations_mat_table($materials, $good_class)
{
    $tbody = "";
    foreach ($materials as $material) {
        if ($material['type_conn'] === 'same') {
            $nothing = '';
            $same = 'selected';
            $canbe = '';
        } elseif ($material['type_conn'] === 'canbe') {
            $nothing = '';
            $same = '';
            $canbe = 'selected';
        } else {
            $nothing = 'selected';
            $same = '';
            $canbe = '';
        }
        $in_price = ($material['in_price'] == 1) ? 'checked' : '';
        $fix_conn = ($material['fix_conn'] == 1) ? 'checked' : '';
        $tbody .= "<tr class='$good_class'>
                        <td>{$material['CODE']}</td>
                        <td data-matid='{$material['MATERIAL_ID']}'>{$material['NAME']}</td>
                        <td><select class='form-control' value='{$material['type_conn']}'>
                            <option value='' $nothing>----</option>
                            <option value='same' $same>Такой же</option>
                            <option value='canbe' $canbe>Может быть</option>
                        </select></td>
                        <td><input type='checkbox' $in_price></td>
                        <td><input type='checkbox' $fix_conn></td>
                        <td><span class='text-danger drop-$good_class-mat' style='cursor:pointer;'><i class='fas fa-trash-alt'></i></td>
                    </tr>";

    }
    return $tbody;
}
/**
 * Updates relations with materials
 * @param $good_id integer it is id of the good
 * @param $materials array list of materials
 * @param string $good type of the good, sample: band or plinth
 * @return mixed it is result of the query
 */
function good_update_mat_relation_conn($good_id, $materials, $good = 'band')
{
    $table_conn = '';
    $field_good = '';
    if ($good === 'band') {
        $table_conn = 'MATERIAL_BAND_CONNECTION';
        $field_good = "BAND_ID";
    } elseif ($good === 'plinth') {
        $table_conn = 'MATERIAL_SIMPLE_CONNECTION';
        $field_good = "SIMPLE_ID";
    }
    $type_conn = "";
    $in_price = "";
    $fix_conn = "";
    $mat_ids = "";
    foreach ($materials as $material) {
        $val_in_price = ($material['in_price']) ? "1" : "NULL";
        $val_fix_conn = ($material['fix_conn']) ? "1" : "NULL";
        $type_conn .= "when $field_good=$good_id AND MATERIAL_ID={$material['mat_id']} then '{$material['type_conn']}' ";
        $in_price .= "when $field_good=$good_id AND MATERIAL_ID={$material['mat_id']} then $val_in_price ";
        $fix_conn .= "when $field_good=$good_id AND MATERIAL_ID={$material['mat_id']} then $val_fix_conn ";
        $mat_ids .= (next($materials)) ? "{$material['mat_id']} ," : $material['mat_id'];
    }
    $sql = "UPDATE $table_conn 
            SET type_connection = CASE 
                $type_conn END,
            in_price = CASE 
                $in_price END,
            fix_connection = case 
                $fix_conn END
            WHERE MATERIAL_ID IN ($mat_ids) AND BAND_ID = $good_id";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}

/**
 * adds new relations with materials
 * @param $good_id integer it is id of the good
 * @param $materials array it is list of materials
 * @param string $good type of the good, sample: band or plinth
 * @return mixed a result of the query
 */
function good_add_mat_relation_conn($good_id, $materials, $good = 'band')
{
    $table_conn = '';
    $field_good = '';
    if ($good === 'band') {
        $table_conn = 'MATERIAL_BAND_CONNECTION';
        $field_good = "BAND_ID";
    } elseif ($good === 'plinth') {
        $table_conn = 'MATERIAL_SIMPLE_CONNECTION';
        $field_good = "SIMPLE_ID";
    }
    $values = "";
    foreach ($materials as $material) {
        $in_price = ($material['in_price']) ? '1' : "NULL";
        $fix_conn = ($material['fix_conn']) ? '1' : "NULL";
        $values .= "({$material['mat_id']}, $good_id, '{$material['type_conn']}', $in_price, $fix_conn)";
        $values .= (next($materials)) ? ", " : "";
    }
    $sql = "INSERT INTO $table_conn (MATERIAL_ID, $field_good, type_connection, in_price, fix_connection) 
            VALUES $values";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}
// end common functions

function db_get_manager($ID)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_manager start', $_SESSION);
    $sql = "SELECT `name`, phone, `e-mail`, place, active, admin, `password`
FROM manager  WHERE id=$ID";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_manager end', $_SESSION);
    return $result['data'][0];
}
function db_update_manager($id, $name, $phone, $mail, $place, $active, $isadmin, $pass)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_manager start', $_SESSION);
    $sql="UPDATE manager SET `name`='$name', phone='$phone', `e-mail`='$mail', place=$place, active=$active, admin=$isadmin, `password`='$pass' 
            WHERE id = $id; ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_update_manager end', $_SESSION);
}
function db_change_manager_status($id, $active)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_change_manager_status start', $_SESSION);
    $sql="UPDATE manager SET active=".($active=='on'?0:1)." WHERE id = $id; ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_change_manager_status end', $_SESSION);
}

/*====================================================
for plinth setting.php
======================================================*/
// возвращает все свойства товаров плинтуса
function plinth_get_good_properties()
{
    $sql = "SELECT prop.NAME, prop.ID, 	
        prop_sim.VALUESTR str, prop_sim.VALUEDIG dig FROM GOOD_PROPERTIES AS prop 
        JOIN GOOD_PROPERTIES_SIMPLE AS prop_sim ON prop.ID = prop_sim.PROPERTY_ID 
        JOIN SIMPLE sim on prop_sim.SIMPLE_ID = sim.SIMPLE_ID WHERE sim.SIMPLE_TYPE = 'plinth' 
        GROUP BY prop.NAME ORDER BY prop.ID";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все плинтуса по умолчанию
function plinth_get_plinth_default()
{
    $sql = "SELECT sim.SIMPLE_ID, sim.CODE, sim.NAME, msc.type_connection, msc.in_price, msc.fix_connection, 
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, 
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM SIMPLE sim
            LEFT JOIN MATERIAL_SIMPLE_CONNECTION msc ON sim.SIMPLE_ID = msc.SIMPLE_ID
            LEFT JOIN MATERIAL mat ON msc.MATERIAL_ID = mat.MATERIAL_ID 
			WHERE sim.SIMPLE_TYPE = 'plinth' GROUP BY sim.SIMPLE_ID ORDER BY sim.SIMPLE_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// генерирует таблицу плинтусов по переданному массиву
function plinth_get_plinth_table($plinths)
{
    $table = "<table class=\"table table-hover\">
                    <thead>
                    <tr>
                        <th scope=\"col\">#</th>
                        <th scope=\"col\">CODE</th>
                        <th scope=\"col\">Название</th>
                        <th scope=\"col\">Столешницы</th>
                        <th scope=\"col\">Подробнее</th>
                    </tr>
                    </thead>
                    <tbody>";
    foreach ($plinths as $plinth) {
        $table .= "<tr>
                    <th scope=\"row\">{$plinth['SIMPLE_ID']}</th>
                    <td scope=\"row\">{$plinth['CODE']}</td>
                    <td>{$plinth['NAME']}</td>";
        $mat_list = "<ol>";
        foreach (explode(', ', $plinth['mat']) as $mat) {
            $mat_list .= "<li>$mat</li>";
        }
        $mat_list .= "</ol>";

        $table .= (!is_null($plinth['mat'])) ? "<td data-matids='{$plinth['mat_id']}'>$mat_list</td>" : "<td>----</td>";
        $table .= "<td><button class='btn btn-success showPlinth'>Подробнее</button></td>";
        $table .= "</tr>";
    }
    $table .= "</tbody></table>";
    return $table;
}

// получает отфильтрованный массив плинтусов по каталогам
function plinth_get_plinth_by_tree($folder_id, $parent_id)
{
    $sql = "SELECT sim.SIMPLE_ID, sim.CODE, sim.NAME, msc.type_connection, msc.in_price, msc.fix_connection, 
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, 
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM SIMPLE sim
            LEFT JOIN MATERIAL_SIMPLE_CONNECTION msc ON sim.SIMPLE_ID = msc.SIMPLE_ID
            LEFT JOIN MATERIAL mat ON msc.MATERIAL_ID = mat.MATERIAL_ID 
            INNER JOIN GOOD_TREE_SIMPLE_CONN gts ON sim.SIMPLE_ID = gts.SIMPLE_ID
            INNER JOIN GOOD_TREE gt ON gts.FOLDER_ID = gt.FOLDER_ID WHERE gts.FOLDER_ID IN ($folder_id, $parent_id) 
			AND sim.SIMPLE_TYPE = 'plinth' GROUP BY sim.SIMPLE_ID ORDER BY sim.SIMPLE_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все записи плинтусов по свойствам
function plinth_get_plinth_by_properties($properties)
{
    $add_sql = "";
    foreach ($properties as $property) {
        $add_sql .= "gps.PROPERTY_ID = {$property['prop_id']} ";

        $value = ($property['type'] === 'VALUESTR') ? "gps.VALUESTR LIKE '{$property['value']}'" : "gps.VALUEDIG = {$property['value']}";
        $add_sql .= "AND $value";
        switch ($property['prop_id']) {
            case 142 :
                $add_sql .= " OR gps.PROPERTY_ID = 2 AND $value";
                break;
            case 168:
                $add_sql .= " OR gps.PROPERTY_ID = 33 AND $value";
                break;
            case 12:
                $add_sql .= " OR gps.PROPERTY_ID = 167 AND $value";
                break;
        }
        if (next($properties)) $add_sql .= " OR ";
    }

    $sql = "SELECT sim.SIMPLE_ID, sim.CODE, sim.NAME, msc.type_connection, msc.in_price, msc.fix_connection, sim.SIMPLE_TYPE, 
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, 
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM SIMPLE sim
            LEFT JOIN MATERIAL_SIMPLE_CONNECTION msc ON sim.SIMPLE_ID = msc.SIMPLE_ID
            LEFT JOIN MATERIAL mat ON msc.MATERIAL_ID = mat.MATERIAL_ID 
            WHERE sim.SIMPLE_ID IN (
                SELECT gps.SIMPLE_ID FROM GOOD_PROPERTIES_SIMPLE gps WHERE $add_sql
                ) GROUP BY sim.SIMPLE_ID ORDER BY sim.SIMPLE_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// поиск плинтуса по коду
function plinth_get_plinth_by_code($code)
{
    $where = ($code === 0) ? "" : " AND sim.CODE LIKE '%$code%'";

    $sql = "SELECT sim.SIMPLE_ID, sim.CODE, sim.NAME, msc.type_connection, msc.in_price, msc.fix_connection, sim.SIMPLE_TYPE, 
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, 
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM SIMPLE sim
            LEFT JOIN MATERIAL_SIMPLE_CONNECTION msc ON sim.SIMPLE_ID = msc.SIMPLE_ID
            LEFT JOIN MATERIAL mat ON msc.MATERIAL_ID = mat.MATERIAL_ID 
            WHERE sim.SIMPLE_TYPE LIKE 'plinth' $where   
			GROUP BY sim.SIMPLE_ID ORDER BY sim.SIMPLE_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// поиск плинтуса по наименованию
function plinth_get_plinth_by_name($name)
{
    $where = ($name === '') ? "" : " AND sim.NAME LIKE '%$name%'";

    $sql = "SELECT sim.SIMPLE_ID, sim.CODE, sim.NAME, msc.type_connection, msc.in_price, msc.fix_connection, sim.SIMPLE_TYPE, 
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id, 
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM SIMPLE sim
            LEFT JOIN MATERIAL_SIMPLE_CONNECTION msc ON sim.SIMPLE_ID = msc.SIMPLE_ID
            LEFT JOIN MATERIAL mat ON msc.MATERIAL_ID = mat.MATERIAL_ID 
            WHERE sim.SIMPLE_TYPE LIKE 'plinth' $where   
			GROUP BY sim.SIMPLE_ID ORDER BY sim.SIMPLE_ID ASC";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// возвращает все материалы связанные с плинтусом
function plinth_get_plinth_mat_by_plinth_id($plinth_id)
{
    $sql = "SELECT DISTINCT mat.MATERIAL_ID, mat.CODE, mat.NAME, 
            mat_sim.type_connection type_conn, mat_sim.in_price, mat_sim.fix_connection fix_conn FROM MATERIAL mat
            JOIN MATERIAL_SIMPLE_CONNECTION mat_sim ON (mat_sim.MATERIAL_ID = mat.MATERIAL_ID)
            WHERE mat_sim.SIMPLE_ID = $plinth_id";

    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

// updates relations plinth-table top
function plinth_update_relations_with_mat($plinth_ids, $materials)
{
    if (!$plinth_ids[0]['list_plinth']) {
        unset($plinth_ids[0]);
        $add_mat = [];
        $update_mat = [];
        foreach ($materials as $mat) {
            if ($mat['fresh_mat']) {
                array_push($add_mat, $mat);
            } else {
                array_push($update_mat, $mat);
            }
        }
        if (!empty($update_mat)) good_update_mat_relation_conn($plinth_ids[1]['plinth_id'], $update_mat, 'plinth');
        if (!empty($add_mat)) good_add_mat_relation_conn($plinth_ids[1]['plinth_id'], $add_mat, 'plinth');
    } else {
        unset($plinth_ids[0]);
        foreach ($plinth_ids as $plinth) {
            if ($plinth['mat_ids'] !== 'undefined') {
                $add_mat = [];
                $update_mat = [];
                foreach ($materials as $material) {
                    if (strpos($plinth['mat_ids'], strval($material['mat_id'])) !== false) {
                        array_push($update_mat, $material);
                    } else {
                        array_push($add_mat, $material);
                    }
                }
                if (!empty($update_mat)) good_update_mat_relation_conn($plinth['plinth_id'], $update_mat, 'plinth');
                if (!empty($add_mat)) good_add_mat_relation_conn($plinth['plinth_id'], $add_mat, 'plinth');
            } else {
                good_add_mat_relation_conn($plinth['plinth_id'], $materials, 'plinth');
            }
        }
    }
    $ids = "";
    foreach ($plinth_ids as $id) {
        $ids .= (next($plinth_ids)) ? "{$id['plinth_id']}, ": $id['plinth_id'];
    }
    $sql_get_plinth = "SELECT sim.SIMPLE_ID, sim.CODE, sim.NAME, msc.type_connection, msc.in_price, msc.fix_connection,
            GROUP_CONCAT(DISTINCT mat.MATERIAL_ID) mat_id,
            GROUP_CONCAT(DISTINCT mat.NAME SEPARATOR ', ') mat FROM SIMPLE sim
            LEFT JOIN MATERIAL_SIMPLE_CONNECTION msc ON sim.SIMPLE_ID = msc.SIMPLE_ID
            LEFT JOIN MATERIAL mat ON msc.MATERIAL_ID = mat.MATERIAL_ID
            WHERE sim.SIMPLE_ID IN ($ids)
            GROUP BY sim.SIMPLE_ID ORDER BY sim.SIMPLE_ID ASC";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql_get_plinth)['data'];
}
// end plinth settings