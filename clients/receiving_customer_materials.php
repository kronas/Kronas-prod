<?php
/**
 * Скрипт принимает материалы клиента, поставленные на приход в новом сервисе
 */
include_once(__DIR__.'/../_1/config.php');
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');
include_once('functions2.php');
include_once('func_api.php');

$output = [
	'status' => 'false',
	'mc_id' => null,
	'message' => ''
];

// Получаем JSON-данные из тела запроса
$json_data = file_get_contents('php://input');

// Парсим JSON-данные в массив
$incomingData = json_decode($json_data, true);

// Проверяем, удалось ли распарсить JSON
if ($incomingData === null) {
	// JSON некорректен
	$output['message'] = 'Bad json format';
} else {

	$headers = getallheaders();

	if (isset($headers['clntlftvrsToken']) && $headers['clntlftvrsToken'] === getToken(true)) { 

		// Запись в файл
		// $filename = date('Y-m-d_H-i-s') . '.json';
		// $file_path = DIR_FILES . 'temp/api_clients_leftovers/' . $filename;
		// $json_string = json_encode($incomingData, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
		// file_put_contents($file_path, $json_string);
		// __________________________________________

		$validate = validateIncomingData($incomingData);

		if ($validate['status'] === true) {
			
			$data = convertJsonToCurrentFormat($incomingData);
			
			if ($data[0]['redact'] === 0) {
				// Добавление
				$result = receiveMaterialsNewService($data);
			} else {
				// Изменение
				$result = changeMaterialNewService($data);
			}
			
			$output['status'] = $result['status'];
			$output['mc_id'] = $result['mc_id'];
			$output['message'] = $result['message'];

		} else {
			$output['message'] = $validate['message'];
		}

    } else {
    	$output['message'] = 'Invalid token';
    }
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>