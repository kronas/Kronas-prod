<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="css/print.css" rel="stylesheet">
</head>
<body>
<a href="" style="border: 1px solid green; padding: 5px; color: green; position: absolute; top: 50px; left: 450px;" onclick="this.style.display='none'; print()">Печать</a>
<?php
include_once('functions2.php');
$docs=array();$_user_namecode='Новый'; $_ac_code='Новый';
if(isset($_GET['details']) && (int)$_GET['details']>0) {
    $_res=db_get_materialClient(array((int)$_GET['details']));
    $_user=get_user_code((int)$_GET['client']);
    if(count($_res)>0) {
        $name=$_res[0]['L'].'*'.$_res[0]['W'].'*'.$_res[0]['T'].' ';
        foreach ($_res as $item) $name.=$item['mcName'].' ';
//        foreach ($_res as $item) $name.=$item['VALUESTR'].' ';
//        _pre($_res[0]);
        $docs =array(array(
            'Move'=>1,
            'L'=>$_res[0]['L'],
            'W'=>$_res[0]['W'],
            'T'=>$_res[0]['T'],
            'Datetime'=>$_res[0]['ddate'],
            'Material_client'=>$_res[0]['mc_id'],
            'mtcode'=>$_res[0]['CODE'],
            'Name'=>$name,
        ));
    }

}
if(isset($_GET['doc']) ) {
    $docs = db_get_doc_details($_GET['doc']);
    if(count($docs)>0) $_user=get_user_code($docs[0]['Client']);
    $_mids=array(); $arr_name=array();
      foreach ($docs as $item) $_mids[]=$item['Material_client'];
          $_res=db_get_materialClient($_mids);
    foreach ($_res as $item) $arr_name[$item['mc_id']]=$item['L'].'*'.$item['W'].'*'.$item['T'].' ';
    foreach ($_res as $item) $arr_name[$item['mc_id']].=$item['mcName'].' ';
//    foreach ($_res as $item) $arr_name[$item['mc_id']].=$item['VALUESTR'].' ';
    // foreach ($docs as $kkey=>$item) $docs[$kkey]['Name']=$arr_name[$item['Material_client']];
}

if(count($_user)==2) {$_ac_code=$_user['code']; $_user_namecode='[/'.$_user['code'].'/] <span class="name-clnt">'.substr($_user['name'],0,50).'</span>';}
    foreach ($docs as $item)
    {
        for($i=1;$i<=$item['Move'];$i++){
        ?>
<div class='main'>
    <div class="right">
        <div class="size">
            <div class="size-in">д <?=$item['L']?> мм</div>
            <div class="size-in">ш <?=$item['W']?> мм</div>
            <div class="size-in">в <?=$item['T']?> мм</div>
        </div>

        <?php //.. 06.12.2022 Тоценко. В этикетку добавлено отображение повреждений материала ?>
        <?php if ($item['crash'] == 1): ?>
        <div class="size">
            <div class="size-in"> Материал имеет повреждения</div>
        </div>
        <?php endif ?>

    </div>

    <div class="my-flex-cont">
        <div class="my-flex-box"><div class='code'>
                <div class='code-s'>
                    <?=$item['Material_client']?>
                </div>
                <div class='code-d'>
                    <?=$item['Material_client']?>
                </div>
            </div></div>
        <div class="my-flex-box"><div class="name">[/<?=$item['mtcode'].'/] '.substr($item['Name'],0,50).'<br>'.$_user_namecode?></div></div>
    </div>
    <div class="right">
        <?php /*
<div class="name">/<?=$item['mtcode'].'/ '.substr($item['Name'],0,50).'<br>'.$_user_namecode?></div> */ ?>
        <div class="mcode">
            <div class="mcode-in"><?=$item['Material_client']?></div>
            <div class="mcode-in"><?=date("d/m/Y",$item['Datetime'])?></div>
            <div class="mcode-in"><?=$item['mtcode']?></div>
        </div>
    </div>
</div>
        <hr>
<?php }} ?>

</body>
</html>