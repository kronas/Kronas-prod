<?php
session_start();
include_once('functions2.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
} elseif($_SESSION['user']['role']!='admin' && $_SESSION['user']['role']!='manager'){
    $_SESSION['messages']['errors'][]='У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}
$clientID=0;$clientName='';$materialID=0;$placeID=0;
if (isset($_POST['submit']) && $_POST['submit'] == 3) {
    if ((int)$_POST['select_places'] > 0) {
        $placeID = (int)$_POST['select_places'];
    }
}
if(isset($_GET['client']) && (int)$_GET['client']>0)
{
    $clientID=(int)$_GET['client'];
    $table = db_get_liststockOnClient($clientID);
    if(!is_array($table) || count($table)==0)
    {
        $_SESSION['messages']['errors'][]='Клиент данных не имеет!';
        header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
        exit();
    }
}
if(isset($_POST['submit']) && $_POST['submit'] == 2)
{
    $_where='';$_whereAdd=FALSE;
    $placeID = (int)$_POST['placeID'];
    if(strlen(trim($_POST['client_filtr_tel']))==19)
    {
        $_where=" tel='".str_replace(' ','', trim($_POST['client_filtr_tel'])) ."'";
        $_whereAdd=TRUE;
    }
    elseif(strlen(trim($_POST['client_filtr_mail']))>5)
    {
        $_where=" `e-mail`='".trim($_POST['client_filtr_mail'])."'";
        $_whereAdd=TRUE;
    }
    $_return=array();
    if(strlen($_where)>0) $_return=get_clientAlt($_where);
    if(count($_return)>0)
    {
        $clientName=$_return['name'];
        $clientID=(int)$_return['client_id'];
//        $table = db_get_liststockOnClient($clientID);
    }
    elseif($_whereAdd)
    {
        $_SESSION['messages']['errors'][]='Клиент не найден!';
        $clientID=0;
    }
    
    
    if((int)$_POST['material_filtr_id']>0)
    {
        $materialID=(int)$_POST['material_filtr_id'];
    }
    if($clientID>0 || $materialID>0) $table = db_get_liststockOnClient($clientID, $materialID,$placeID);

}
if(isset($_POST['submit']) && $_POST['submit'] == 1)
{
    $out=array();
    for($i=1;$i<=$_POST['countLine'];$i++)
    {
        if((int)$_POST['return_'.$i]>0)
        {
        $out[$i-1]=array(
            'ID'=>$_POST['material_'.$i],
            'COUNT'=>0-(int)$_POST['return_'.$i],
            'placesID'=>$_POST['places_'.$i]
        );
        if($_POST['clientID']==0) $out[$i-1]['client_id']=$_POST['client_id_'.$i];
        }
    }

    if(count($out)>0) {
        $clientID = $_POST['clientID'];
        $materialID = $_POST['materialID'];
        $_arrOute=array();
        foreach ($out as $oute)
        {
            $_arrOute[$oute['placesID']][$oute['ID']]=$oute['COUNT'];
        }
        $table = db_get_liststockOnClient($clientID,$materialID);
        foreach($table as $line)
        {
            if(isset($_arrOute[$line['PLACES_ID']][$line['Material_client']]))
            {
                echo $line['Count']+$_arrOute[$line['PLACES_ID']][$line['Material_client']].'<hr>';
                if($line['Count']+$_arrOute[$line['PLACES_ID']][$line['Material_client']]<0)
                {
                    $_SESSION['messages']['errors'][]='Ошибка ввода данных! Отрицательный остаток недопустим!!';
                    header('Location: '.$main_dir . '/clients/corrects.php'.'?nw='.$_GET['nw']);
                    exit();
                }
            }
        }

        $Type = 'out';
        $Reason = 'back';
        $Manager = $_SESSION['user']['ID'];
        $Order1C = 0;
        $Comment = '';
        if($materialID>0)
        {
         $_clOut=array();
         foreach ($out as $item)
             {
                 $_item=$item;
                 unset($_item['client_id']);
                 $_clOut[$item['client_id']][]=$_item;
             }
            foreach ($_clOut as $_client=>$item)
            {
                $docID = db_create_doc($Type, $_client, $Reason, $Manager, $Order1C, $Comment);
                db_materials_stock_move_add($item, $docID);
                db_materials_stock_upgate($_client);
                $_SESSION['messages']['success'][] = 'Документ №'.$docID.' сохранен!';

            }
        }
        else {
                $docID = db_create_doc($Type, $clientID, $Reason, $Manager, $Order1C, $Comment);
                db_materials_stock_move_add($out, $docID);
                db_materials_stock_upgate($clientID);
                $_SESSION['messages']['success'][] = 'Документ сохранен!';
        }
    }else{
        $_SESSION['messages']['success'][] = 'Данных нет для сохранения';
    }
    header('Location: '.$main_dir . '/clients/docs.php'.'?nw='.$_GET['nw']);
    exit();

}
$title='Возврат';
include_once('header.php');
?>
    <div class="row  align-items-center">
        <?php echo messages($_SESSION, "col-md-4 offset-md-4 col-sm-12"); ?>

    </div>
<?php if ($placeID == 0 && $clientID==0 && $materialID==0) { ?>
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-12">
            <form method="post">
                <?= db_get_listPlaces(false, 0) ?>
                <div class="col-md-12 text-center">
                    <button type="submit" name="submit" class="btn btn-success mb-2" value="3"><i
                                class="fas fa-check"></i> Выбрать
                    </button>
                </div>
            </form>
        </div>
    </div>
    <?php
}
if($clientID==0 && $materialID==0 && $placeID>0)
{
    ?>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-sm-12">

            <form class="form-inline" method="post">

                <div class="form-group mx-sm-3 mb-2">
                    <label for="client_search_tel" class="mr-4">Телефон клиента</label>
                    <input type="text" class="form-control phone" id="client_filtr_tel" name="client_filtr_tel"
                           data-mask="+38 (000) 000-00-00" placeholder="+38 (___) ___-__-__">
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="client_filtr_mail" class="mr-4">E-Mail</label>
                    <input type="text" class="form-control " id="client_filtr_mail" name="client_filtr_mail" placeholder="name@example.com">
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="material_filtr_id" class="mr-4">ID материала</label>
                    <input type="text" class="form-control onlyNumber" id="material_filtr_id" name="material_filtr_id" >
                </div>
                <input type="hidden" name="placeID" value="<?= $placeID ?>">
                <button type="submit" name="submit"  class="btn btn-primary mb-2" value="2" ><i class="fas fa-search"></i> Найти</button>
            </form>
        </div>
    </div>
    <?php
}
elseif(($clientID>0 || $materialID>0) && $placeID>0)
{
    if (is_null($table)) {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div></div>';
    } else {
        if(strlen(trim($clientName))>0)            echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-secondary" role="alert">Клиент - '.$clientName.'</div></div>';
        echo '<div class="row"><div class="col-12 pl-3 pr-3">';
        echo '<form method="post" class="return">';
        echo '<table class="table table-striped"><thead>    <tr>
        <th>ID</th>
        '.($clientID==0?'<th>Клиент</th>':'').'
        <th>Склад</th>
        <th>Материал</th>
        <th>W</th>
        <th>L</th>
        <th>T</th>
        <th>Кол-во</th>
        <th>...</th>
    </tr></thead><tbody>';
        $_cnt = 0;
        foreach ($table as $row) {
            $_cnt++;
            echo '<tr>
<td>' . $row['Material_client'] . '</td>
'.($clientID==0?'<td>' . $row['clNaem'] . '</td>':'').'
<td>' . $row['NAME'] . '</td>
<td>' . ((int)$row['Code_mat'] == 0 ? '(' . $row['CODE'] . ') ' . $row['mtrl_name'] : '(' . $row['Code_mat'] . ') ' . $row['matcl_name']) . '</td>
<td>' . $row['matcl_w'] . '</td>
<td>' . $row['matcl_l'] . '</td>
<td>' . $row['matcl_t'] . '</td>
<td>' . $row['Count'] . '</td>
<td><div class="form-group mb-0">
                    <input type="hidden" name="material_' . $_cnt . '" value="' . $row['Material_client'] . '" >
                    <input type="hidden" name="places_' . $_cnt . '" value="' . $row['PLACES_ID'] . '" >';
            if($clientID==0) echo '<input type="hidden" name="client_id_' . $_cnt . '" value="' . $row['client_id'] . '" >';
            echo '<input type = "text" class="form-control" name = "return_' . $_cnt . '" data-retmax="' . $row['Count'] . '" size="4" >
                    </div >
</td>
    </tr>';
        }
        echo '</tbody></table>';


        echo '   
                       <input type="hidden" name="clientID" value="' . $clientID . '" >
                       <input type="hidden" name="materialID" value="' . $materialID . '" >
                    <input type="hidden" name="countLine" value="' . $_cnt . '" >
        <div class="row ">
            <div class="col-12  text-center ">
                <button type="submit" name="submit" value="1" class="btn btn-success"><i class="far fa-save"></i> Провести</button>
            </div>
        </div>';
        echo '</form>';
        echo '</div></div>';
    }
}
?>


<?php include_once('footer.php'); ?>