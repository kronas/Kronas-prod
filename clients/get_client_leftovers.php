<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

session_start();
include_once('functions2.php');

$outputData = [];

// Если нужно показать списанные остатки
$writtenOffBalances = false;
$moreInfoFlag = false;

$clientTel = (1 === preg_match('#^380\d{9}$#', $_GET['tel'])) ? formatPhoneNumber($_GET['tel']) : (int)$_GET['tel'];

$getSourceInfo = getClientLeftoversByPhone($clientTel, $writtenOffBalances);

if (!empty($getSourceInfo)) {

	// Добавляем код материала в Акценте
	foreach ($getSourceInfo as $key => &$oneArr) {
		$q = "SELECT * FROM `MATERIAL` WHERE `MATERIAL_ID` = " . (int)$oneArr['CODE'] . " LIMIT 1";
		$res=sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
		if ($res['res'] === 1 && !empty($res['data'])) {
			$oneArr['AccentCode'] = $res['data'][0]['CODE'];
		} else {
			$oneArr['AccentCode'] = null;
		}
	}

	$getInfo = [];
	foreach ($getSourceInfo as $key => $value) {

		// Українська назва філії
		$q = "SELECT `NAME_UA` FROM `PLACES` WHERE `PLACES_ID` = " . $value['Place'] . " LIMIT 1";
		$res=sql_data(__LINE__,__FILE__,__FUNCTION__,$q);
		$placeName = (isset($res['data'][0])) ? $res['data'][0]['NAME_UA'] : $value['placeName'];

		// Українське наіменування матеріалу (тільки для матеріалу клієнта)
		$value['matName'] = str_replace('Материал клиента', 'Матеріал кліента', $value['matName']);
		$value['matName'] = str_replace('толщина плиты', 'товщина плити', $value['matName']);

		$arr = [];
		$arr['mc_id'] = (int) $value['mc_id'];
		$arr['client_id'] = $value['clCODE'];
		$arr['name'] = $value['matName'];
	    $arr['type'] = 'ЛДСП';
	    $arr['width'] = (int) $value['W'];
	    $arr['height'] = (int) $value['L'];
	    $arr['article'] = $value['AccentCode'];
	    $arr['thickness'] = (int) $value['T'];
	    $arr['count'] = $value['Count'];
	    $arr['placeName'] = $placeName;
		$getInfo[$key] = $arr;
	}

	$outputData['basicInfo'] = $getInfo;

	$moreInfo = [];
	$andWhereClient = '';
	foreach ($getSourceInfo as $key => $arr) {
		$moreInfo[$key] = (object) db_get_balance_details((int)$arr['mc_id'], (int)$arr['Place'], $andWhereClient);
	}

	if (!empty($moreInfo) && $moreInfoFlag === true) {
		$outputData['moreInfo'] = $moreInfo;
	}
}

$outputData = (!empty($outputData)) ? $outputData : (object) $outputData;

printf(json_encode($outputData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));





/**
 * Функция возвращает остатки клиента по номеру телефона клиента
 * Если второй параметр - true, то также возвращаются списанные остатки
 */
function getClientLeftoversByPhone($clientTel, $writtenOffBalances = false) {
	$filtr = [
	    0 =>  'stcnt.Count>0',
		1 =>  'stcnt.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` FROM client as clnt WHERE clnt.tel = "' . $clientTel . '")))'
	];

	// Если нужно показать списанные остатки
	if ($writtenOffBalances === true) {
		unset($filtr[0]);
	}

	$start = 0;
	$limit = 1000;
	$total_records = 0;
	$output = db_get_listbalance($filtr, $start, $limit, $total_records);

	return $output;
}

/**
 * Функция возвращает остатки клиента по имени клиента
 * Если второй параметр - true, то также возвращаются списанные остатки
 */
function getClientLeftoversByName($clientName, $writtenOffBalances = false) {
	$filtr = [
	    0 =>  'stcnt.Count>0',
		1 =>  'stcnt.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` FROM client as clnt WHERE clnt.name = "' . $clientName . '")))'
	];

	// Если нужно показать списанные остатки
	if ($writtenOffBalances === true) {
		unset($filtr[0]);
	}

	$start = 0;
	$limit = 1000;
	$total_records = 0;
	$output = db_get_listbalance($filtr, $start, $limit, $total_records);

	return $output;
}

/**
 * Функция форматирует номер телефона к формату записи Сервиса
 */
function formatPhoneNumber($phoneNumber) {
	
	$output = '+';
	$char_array = str_split($phoneNumber);

	$output .= $char_array[0];
	$output .= $char_array[1];
	$output .= '(';
	$output .= $char_array[2];
	$output .= $char_array[3];
	$output .= $char_array[4];
	$output .= ')';
	$output .= $char_array[5];
	$output .= $char_array[6];
	$output .= $char_array[7];
	$output .= '-';
	$output .= $char_array[8];
	$output .= $char_array[9];
	$output .= '-';
	$output .= $char_array[10];
	$output .= $char_array[11];
	
	return $output;
}

function encodeToken($plaintext, $key) {

	// Инициализация шифра
	$cipher = "AES-128-CBC";

	// Генерация случайного вектора инициализации (IV)
	$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher));

	// Шифрование
	$ciphertext = openssl_encrypt($plaintext, $cipher, $key, OPENSSL_RAW_DATA, $iv);

	// Кодирование в base64 для сохранения в виде строки
	$encryptedText = base64_encode($iv . $ciphertext);

	return $encryptedText;

}

function parseToken($encryptedText, $key) {
	
	// Инициализация шифра
	$cipher = "AES-128-CBC";

	// Декодирование из base64
	$decodedText = base64_decode($encryptedText);

	// Извлечение IV и зашифрованного текста
	$ivLength = openssl_cipher_iv_length($cipher);
	$iv = substr($decodedText, 0, $ivLength);
	$ciphertext = substr($decodedText, $ivLength);

	// Расшифрование
	$decryptedText = openssl_decrypt($ciphertext, $cipher, $key, OPENSSL_RAW_DATA, $iv);

	return $decryptedText;
}

?>