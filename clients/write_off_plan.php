<?php
session_start();
include_once('functions2.php');

if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
} elseif ($_SESSION['user']['role'] != 'admin' && $_SESSION['user']['role'] != 'manager') {
    $_SESSION['messages']['errors'][] = 'У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}


$title = 'Списание остатков';$toolbarclose=1;
include_once('header.php');

if(isset($_GET['matID']) && isset($_GET['clCODE']) && isset($_GET['place']))
{

    $table=db_get_planDoc($_GET['matID'], $_GET['clCODE'], $_GET['place']);
    if(count($table)==0)
    {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12"><div class="alert alert-danger" role="alert">';
        echo 'Данных нет.</div></div></div>';
    }
    else
    {
        $doc=$table[0];
?>
        <input type="hidden" id="matID" value="<?= $_GET['matID'] ?>">
        <input type="hidden" id="clCODE" value="<?= $_GET['clCODE'] ?>">
        <input type="hidden" id="place" value="<?= $_GET['place'] ?>">
        <input type="hidden" id="rowID" value="<?= $_GET['rowID'] ?>">
    <div class="row  align-items-center">

        <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
            <table class="table table-striped mt-3">

                <tbody>
                <tr>
                    <th>Клиент</th>
                    <td><?= $doc['clname'] ?></td>
                </tr>
                <tr>
                    <th>Материал</th>
                    <td><?= $doc['matname'] ?></td>
                </tr>
                <tr>
                    <th>Размер</th>
                    <td><?= $doc['W'].'/'.$doc['L'].'/'.$doc['T'] ?></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
<div class="row mt-4" id="write-off-plan">


    <div class="col-md-12">
        <table class="table table-striped balance"><thead>    <tr>
                <th>Документ №</th>
                <th>DB_AC_NUM</th>
                <th>DB_AC_ID</th>
                <th>Кол-во</th>
                <th>Списать</th>
            </tr></thead><tbody>
            <?php
    foreach ($table as $key=>$row) {
        echo '<tr><td>'.$row['d_id'].'</td><td>'.$row['DB_AC_NUM'].'</td><td>'.$row['DB_AC_ID'].'</td><td>'.$row['Move'].'</td>';
        echo '<td><input type="text" class="form-control" name="wop_'.$key.'" id="wop_'.$key.'" size="4">
<input type="hidden" name="docID_'.$key.'" id="docID_'.$key.'" value="'.$row['d_id'].'">
<input type="hidden" name="wopIN_'.$key.'" id="wopIN_'.$key.'" value="'.$row['Move'].'">
</td></tr>';
    }
            ?>
            </tbody>
        </table>
    </div>
    <div class="col-12 messages-write-off-plan">

    </div>
        <div class="col-md-12 text-center mt-4 mb-5">
            <button type="button" class="btn btn-success" id="saveWOP"><i class="far fa-save"></i> Провести</button>
        </div>
</div>
<?php }} ?>
<?php include_once('footer.php');  ?>