<?php
session_start();
include_once('functions2.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
} elseif ($_SESSION['user']['role'] != 'admin' && $_SESSION['user']['role'] != 'manager') {
    $_SESSION['messages']['errors'][] = 'У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}
$clientName = '';
$placeID = 0;
if (isset($_POST['submit']) && $_POST['submit'] == 6) {
    if ((int)$_POST['select_places'] > 0) {
        $placeID = (int)$_POST['select_places'];
    }
}
if ((isset($_POST['submit']) && $_POST['submit'] == 1) || (isset($_GET['client']) && (int)$_GET['client'] > 0)) {
    $clientID = 0;
    $_return = array();
    $_where = '';
    if (isset($_POST['placeID']) && (int)$_POST['placeID'] > 0) $placeID = (int)$_POST['placeID'];
    if (isset($_GET['client']) && (int)$_GET['client'] > 0) {
        $clientID = (int)$_GET['client'];
        $_return = get_clientAlt(" client_id=$clientID");
        $clientName = $_return['name'];
    } else {
        if (strlen(trim($_POST['client_filtr_tel'])) == 19) {
            $_where = " tel='" . str_replace(' ', '', trim($_POST['client_filtr_tel'])) . "'";
        } elseif (strlen(trim($_POST['client_filtr_mail'])) > 3) {
            $_where = " `e-mail`='" . trim($_POST['client_filtr_mail']) . "'";
        } elseif (strlen(trim($_POST['client_filtr_name'])) > 3) {
            $_where = " `name`='" . trim($_POST['client_filtr_name']) . "'";
        }
        if (strlen($_where) > 0) $_return = get_clientAlt($_where);
        if (count($_return) > 0) {
            $clientName = $_return['name'];
            $clientID = (int)$_return['client_id'];
        }
    }
    if ($clientID > 0)
        $table = db_get_liststockOnClient($clientID, 0, $placeID);

}
$doc4production = array();
if (isset($_POST['submit']) && $_POST['submit'] == 2) {
    if ((int)$_POST['num_doc'] > 0) {
        $doc4production = db_get_doc4production((int)$_POST['num_doc'], (int)$_POST['placeID']);
        if (count($doc4production) == 0) $_SESSION['messages']['errors'][] = 'Документ не найден!';
    }
}
if (isset($_POST['submit']) && $_POST['submit'] == 5) {
    if (isset($_POST['placeID']) && (int)$_POST['placeID'] > 0) $placeID = (int)$_POST['placeID'];
    if ((int)$_POST['search_mat-id'] > 0) {
        $materialID = (int)$_POST['search_mat-id'];
        $table = db_get_liststockOnClient(0, $materialID, $placeID);
        $list1C4 = array();
        foreach ($table as $row) {

                    $list1C4[$row['client_id']] = db_get_list1C4clients($row['client_id'], FALSE);
        }
    }

}
if (isset($_POST['submit']) && $_POST['submit'] == 3) {
    if ((int)$_POST['documentID'] > 0) {
        db_doc_set_production((int)$_POST['documentID']);
        $_SESSION['messages']['success'][] = 'Документ выдан!';
        unset($_POST);
        header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
        exit();
    }
}

if (isset($_POST['submit']) && $_POST['submit'] == 4) {
    $out = array();
    for ($i = 1; $i <= $_POST['countLine']; $i++) {
        if ((int)$_POST['return_' . $i] > 0) {
            $out[] = array(
                'ID' => $_POST['material_' . $i],
                'COUNT' => 0 - (int)$_POST['return_' . $i],
                'placesID' => $_POST['places_' . $i]
            );
        }
    }
    if (count($out) > 0) {
        $clientID = (int)$_POST['clientID'];
        $materialID=($clientID==0?$out[0]['ID']:0);
        $placeID=$out[0]['placesID'];
        $_arrOute = array();
        foreach ($out as $oute) {
            $_arrOute[$oute['placesID']][$oute['ID']] = $oute['COUNT'];
        }
        $table = db_get_liststockOnClient($clientID,$materialID,$placeID);
        foreach ($table as $line) {
            if (isset($_arrOute[$line['PLACES_ID']][$line['Material_client']])) {
                echo $line['Count'] . ' = ' . $_arrOute[$line['PLACES_ID']][$line['Material_client']] . '<br>';
                if ($line['Count'] + $_arrOute[$line['PLACES_ID']][$line['Material_client']] < 0) {
                    $_SESSION['messages']['errors'][] = 'Ошибка ввода данных! Отрицательный остаток недопустим!!';
                    header('Location: ' . $main_dir . '/clients/corrects.php'.'?nw='.$_GET['nw']);
                    exit();
                }
            }
        }

        $Type = 'out';
        $Reason = 'client';
        $Manager = $_SESSION['user']['ID'];
        if($clientID>0) {
            $Order1C = $_POST['select_orders'];
            $Comment = $_POST['comment'];
            $docID = db_create_doc($Type, $clientID, $Reason, $Manager, $Order1C, $Comment);
            db_materials_stock_move_add($out, $docID);
            db_materials_stock_upgate($clientID);
            $_SESSION['messages']['success'][] = 'Документ №'.$docID.' сохранен!';

        }else{
            $out = array();
            for ($i = 1; $i <= $_POST['countLine']; $i++) {

                if ((int)$_POST['return_' . $i] > 0) {
                    $out[$_POST['client_' . $i]] = array(
                            'orders'=>$_POST['orders_' . $i],
                            'comment'=>$_POST['comment_' . $i],
                            );
                    $out[$_POST['client_' . $i]]['data'][] =array(
                        'ID' => $_POST['material_' . $i],
                        'COUNT' => 0 - (int)$_POST['return_' . $i],
                        'placesID' => $_POST['places_' . $i]
                    );
                }
            }
            foreach ($out as $client=>$row)
            {
                $R = array($Type, $client, $Reason, $Manager, $row['orders'], $row['comment']);
                $mmat=$row['data'];
                $docID = db_create_doc($Type, $client, $Reason, $Manager, $row['orders'], $row['comment']);
                db_materials_stock_move_add($mmat, $docID);
                db_materials_stock_upgate($client);
                $_SESSION['messages']['success'][] = 'Документ №'.$docID.' сохранен!';
            }
        }

    } else {
        $_SESSION['messages']['success'][] = 'Данных нет для сохранения';
    }
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}

$title = 'В производство';
include_once('header.php');

?>
    <div class="row  align-items-center">
        <?php echo messages($_SESSION, "col-md-4 offset-md-4 col-sm-12"); ?>

    </div>
<?php if ($placeID == 0 && count($doc4production) == 0) { ?>
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-12">
            <form method="post">
                <?= db_get_listPlaces(false, 0) ?>
                <div class="col-md-12 text-center">
                    <button type="submit" name="submit" class="btn btn-success mb-2" value="6"><i
                                class="fas fa-check"></i> Выбрать
                    </button>
                </div>
            </form>
        </div>
    </div>
    <?php
}
if (($clientID == 0 && count($doc4production) == 0) && $materialID == 0 && $placeID > 0) {
    ?>
    <div class="row">
        <div class="col-md-6 offset-md-3 col-sm-12">
            <ul class="nav nav-tabs  nav-pills nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="doc-tab" data-toggle="tab" href="#doc" role="tab" aria-controls="doc"
                       aria-selected="true">По документу</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="client-tab" data-toggle="tab" href="#client" role="tab"
                       aria-controls="client" aria-selected="false">По клиенту</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="mat-id-tab" data-toggle="tab" href="#mat-id" role="tab"
                       aria-controls="mat-id" aria-selected="false">По ID материала</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active pb-5 pt-5" id="doc" role="tabpanel" aria-labelledby="doc-tab">
                    <div class="row">
                        <div class="col-md-8 offset-md-2 col-sm-12">

                            <form class="form-inline" method="post">
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="num_doc" class="mr-4">№ документа</label>
                                    <input type="text" class="form-control " id="num_doc" name="num_doc">
                                    <input type="hidden" name="placeID" value="<?= $placeID ?>">
                                </div>
                                <button type="submit" name="submit" class="btn btn-primary mb-2" value="2"><i
                                            class="fas fa-search"></i> Найти
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade pb-5 pt-5" id="client" role="tabpanel" aria-labelledby="client-tab">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">

                            <form class="form-inline" method="post">

                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="client_search_tel" class="mr-4">Телефон клиента</label>
                                    <input type="text" class="form-control phone" id="client_filtr_tel"
                                           name="client_filtr_tel"
                                           data-mask="+38 (000) 000 00 00" placeholder="+38 (___) ___ __ __">
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="client_filtr_mail" class="mr-4">E-Mail</label>
                                    <input type="text" class="form-control " id="client_filtr_mail"
                                           name="client_filtr_mail" placeholder="name@example.com">
                                    <input type="hidden" name="placeID" value="<?= $placeID ?>">
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="client_filtr_name" class="mr-4">Наименование клиента</label>
                                    <input type="text" class="form-control " id="client_filtr_name"
                                           name="client_filtr_name" placeholder="">
                                </div>
                                <div>
                                    <button type="submit" name="submit" class="btn btn-primary mb-2" value="1"><i
                                                class="fas fa-search"></i> Найти
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade pb-5 pt-5" id="mat-id" role="tabpanel" aria-labelledby="mat-id-tab">
                    <div class="row">
                        <div class="col-md-8 offset-md-2 col-sm-12">

                            <form class="form-inline" method="post">
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="search_mat-id" class="mr-4">ID материала</label>
                                    <input type="text" class="form-control " id="search_mat-id" name="search_mat-id">
                                    <input type="hidden" name="placeID" value="<?= $placeID ?>">
                                </div>
                                <button type="submit" name="submit" class="btn btn-primary mb-2" value="5"><i
                                            class="fas fa-search"></i> Найти
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
if (count($doc4production) > 0) {
    ?>
    <div class="row">
        <div class="col-md-6 offset-md-3 col-sm-12">
            <form method="post">
                <div class="alert alert-dark" role="alert">
                    <input type="hidden" name="documentID" value="<?= $doc4production['d_id'] ?>">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            Доумент №<?= $doc4production['d_id'] ?>
                            от<?= date('Y/m/d H:i', $doc4production['Datetime']) ?>
                            для <?= $doc4production['cname'] ?>, менеджера <?= $doc4production['mname'] ?> можно ли
                            выдавать?
                        </div>
                        <div class="col-md-6 text-center p-3">
                            <button type="submit" class="btn btn-danger"><i class="fas fa-times"></i> Отмена</button>
                        </div>
                        <div class="col-md-6 text-center p-3">
                            <button type="submit" name="submit" class="btn btn-warning mb-2" value="3">
                                <i class="fas fa-dolly"></i> Выдать
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
}
?>
<?php
if ($clientID > 0 || $materialID > 0) {
    if (is_null($table)) {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div>';
    } else {
        echo '<form method="post" class="into-production">';
        if ($clientID > 0) {
            if (strlen(trim($clientName)) > 0) echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-secondary" role="alert">Клиент - ' . $clientName . '</div></div>';
            echo '<div class="row"><div class="col-md-6 offset-md-3 col-sm-12">';
            echo db_get_list1C4clients($clientID);
            echo '  <div class="form-group">
    <label for="comment">Комментарий</label>
    <textarea class="form-control" name="comment" id="comment" rows="3" required></textarea>
  </div>';
            echo '</div>';
        }
        echo '<div class="col-12 pl-3 pr-3">';
        echo '<table class="table table-striped"><thead>    <tr>
            <th>ID</th>';
        if ($materialID > 0) echo '<th>Клиент</th>';
        echo '<th>Склад</th>
            <th>Материал</th>
            <th>W</th>
            <th>L</th>
            <th>T</th>
            <th>Кол-во</th>
            <th>В производство</th>';
        if ($materialID > 0) echo '<th>Заказ</th><th>Комментарий</th>';
        echo '</tr></thead><tbody>';
        $_cnt = 0;
        foreach ($table as $row) {
            $_cnt++;
            echo '<tr>
                    <td>' . $row['Material_client'] . '</td>';
            if ($materialID > 0) echo '<td>' . $row['clNaem'] . '</td>';
            echo '<td>' . $row['NAME'] . '</td>
                    <td>' . ((int)$row['Code_mat'] == 0 ? '(' . $row['CODE'] . ') ' . $row['mtrl_name'] : '(' . $row['Code_mat'] . ') ' . $row['matcl_name']) . '</td>
                    <td>' . $row['matcl_w'] . '</td>
                    <td>' . $row['matcl_l'] . '</td>
                    <td>' . $row['matcl_t'] . '</td>
                    <td>' . $row['Count'] . '</td>
                    <td><div class="form-group mb-0">
                                        <input type="hidden" name="material_' . $_cnt . '" value="' . $row['Material_client'] . '" >
                                        <input type="hidden" name="places_' . $_cnt . '" value="' . $row['PLACES_ID'] . '" >
                                            <input type = "text" class="form-control" name = "return_' . $_cnt . '" data-retmax="' . $row['Count'] . '"  data-numline="' . $_cnt . '" size="4" >
                                        </div >
                    </td>';
            if ($materialID > 0) echo '<td><div class="form-group mb-0">' . str_replace('select_orders', 'orders_' . $_cnt, $list1C4[$row['client_id']]) . '</div >
                    </td><td><div class="form-group mb-0">
                     <input type="hidden" name="client_' . $_cnt . '" value="' . $row['client_id'] . '" >
             <textarea class="form-control" name="comment_' . $_cnt . '" id="comment_' . $_cnt . '" rows="3" ></textarea>
                                        </div >
                    </td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
        echo '   
                           <input type="hidden" name="clientID" value="' . $clientID . '" >
                        <input type="hidden" name="countLine" value="' . $_cnt . '" >
            <div class="row ">
                <div class="col-12  text-center ">
                    <button type="submit" name="submit" value="4" class="btn btn-success"><i class="fas fa-dolly"></i> Выдать</button>
                </div>
            </div>';
        echo '</div></div>';
        echo '</form>';
    }
}
?>
<?php include_once('footer.php'); ?>