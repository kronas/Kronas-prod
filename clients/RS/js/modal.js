(function ($) {


    $(document).ready(function () {
        $(document).on('shown.bs.modal', '#selectData', function (event) {

            var button = $(event.relatedTarget);
            var editProfile = $('select#profile_list').find(':selected').val();
            var typeop = button.data('typeop');
            if(typeop===1 || typeop===2)
            {
                $(this).find(".modal-body-in").load("RS/getdata.php?typeop=" + typeop + "&editID=" + editProfile);
            }
            if(typeop===3)
            {
                var editDecor = button.attr('data-decortid');
                $(this).find(".modal-body-in").load("RS/decors.php?typeop=" + typeop + "&profileID=" + editProfile+ "&decorID=" + editDecor);
            }

        });
        $(document).on('hide.bs.modal', '#selectData', function (event) {
            $('.modal-backdrop').remove();
        });
        $(document).on('hidden.bs.modal', '#selectData', function (event) {
            $('.modal-backdrop').remove();
        });
        $(document).on('click', 'button#saveProfile', function (event, sel) {
            event.stopImmediatePropagation(); // off dubleclick
            if( parseFloat($('input#width_l_sheet').val())>0 &&
                parseFloat($('input#height_w_sheet').val())>0 &&
                parseFloat($('input#wdth_l_glass').val())>0 &&
                parseFloat($('input#height_w_glass').val())>0 &&
                parseFloat($('input#koef_h_door').val())>0 &&
                parseFloat($('input#koef_l_hor_pofile').val())>0 &&
                $('input#vert').val().length>0 &&
                $('input#hor_up').val().length>0 &&
                $('input#hor_down').val().length>0 &&
                $('input#napr_up').val().length>0 &&
                $('input#napr_down').val().length>0 &&
                $('input#connection').val().length>0 &&
                $('input[name="newProfile"]').val().length>3 &&
                parseFloat($('input#max_door_height').val())>0 &&
                parseFloat($('input#max_door_length').val())>0 &&
                parseFloat($('input#max_wdth_in').val())>0 &&
                parseFloat($('input#con_sheet_glass').val())>0 &&
                parseFloat($('input#con_sheet_sheet').val())>0 &&
                parseFloat($('input#con_glass_glass').val())>0 &&
                parseFloat($('input#min_door_height').val())>0 &&
                parseFloat($('input#min_door_length').val())>0 &&
                parseFloat($('input[name="newProfileWCR"]').val())>0)
            {

                var out = {};
                out['profile_id'] = parseInt($('input[name="profile_id"]').val());
                out['width_l_sheet'] = parseFloat($('input[name="width_l_sheet"]').val());
                out['height_w_sheet'] = parseFloat($('input[name="height_w_sheet"]').val());
                out['wdth_l_glass'] = parseFloat($('input[name="wdth_l_glass"]').val());
                out['height_w_glass'] = parseFloat($('input[name="height_w_glass"]').val());
                out['koef_h_door'] = parseFloat($('input[name="koef_h_door"]').val());
                out['koef_l_hor_pofile'] = parseFloat($('input[name="koef_l_hor_pofile"]').val());
                out['indent_l'] = parseFloat($('input[name="indent_l"]').val());
                out['indent_r'] = parseFloat($('input[name="indent_r"]').val());
                out['indent_t'] = parseFloat($('input[name="indent_t"]').val());
                out['indent_b'] = parseFloat($('input[name="indent_b"]').val());
                out['vert'] = $('input[name="vert"]').val();
                out['hor_up'] = $('input[name="hor_up"]').val();
                out['hor_down'] = $('input[name="hor_down"]').val();
                out['napr_up'] = $('input[name="napr_up"]').val();
                out['napr_down'] = $('input[name="napr_down"]').val();
                out['connection'] = $('input[name="connection"]').val();
                out['newprofile'] = $('input[name="newProfile"]').val();
                out['newprofilewcr'] = parseFloat($('input[name="newProfileWCR"]').val());
                out['newprofiletype'] = $('select[name="new_profile_type"]').find(':selected').val();
                out['max_door_height'] = parseFloat($('input[name="max_door_height"]').val());
                out['max_door_length'] = parseFloat($('input[name="max_door_length"]').val());
                out['max_wdth_in'] = parseFloat($('input[name="max_wdth_in"]').val());
                out['con_sheet_glass'] = parseFloat($('input[name="con_sheet_glass"]').val());
                out['con_sheet_sheet'] = parseFloat($('input[name="con_sheet_sheet"]').val());
                out['con_glass_glass'] = parseFloat($('input[name="con_glass_glass"]').val());
                out['min_door_height'] = parseFloat($('input[name="min_door_height"]').val());
                out['min_door_length'] = parseFloat($('input[name="min_door_length"]').val());

                $.ajax({
                    type: 'POST',
                    url: 'RS/ajax_resp.php',
                    data: {'data': JSON.stringify(out)},
                    success: function (resp) {
                        var resp_in = JSON.parse(resp);
                        if (resp_in.result) {
                            $('.messages').html('<div class="alert alert-success" role="alert">' + resp_in.message + '</div>');
                            $('#selectData').modal('hide');
                            $('#profiles #profile_list').html(resp_in.opt_list);
                            $('#glue_set').addClass('d-none');
                            $('#glue_del').addClass('d-none');
                            $(".forprofileDecor").html('');
                            $(".forprofileSchetka").html('');
                            $(".forprofileDovodchik").html('');
                            $(".forprofileRolick").html('');
                            $(".forprofileStopor").html('');
                            $('#profiles #erase_decor').prop( "disabled", true );
                            $('#profiles #edit_decor').prop( "disabled", true );
                            $('.modal-backdrop').remove();

                        } else {
                            $('.messages').html('<div class="alert alert-danger" role="alert">' + resp_in.message + '</div>');

                        }
                    }
                });
            }
            else
            {

                $('.messages').html('<div class="alert alert-danger" role="alert">Заполните пожалуйста все поля!</div>');
            }
            setTimeout(function () {
                $('.messages').html('');
            }, 5000);
            return false;
        });
        $(document).on('change', '.profile-setting input', function (event, sel) {
            event.stopImmediatePropagation();
            var theVal = $(this).val();
            $(this).val(theVal.replace(/\,/g, '.'));
        });
        $(document).on('click', 'button#saveDecors', function (event, sel) {
            event.stopImmediatePropagation(); // off dubleclick
            if( $('input#decor_name').val().length>2 &&
                $('input#decor_color').val().length>4 &&
                parseInt($('select#vert').val())>0  &&
                parseInt($('select#hor_up').val())>0 &&
                parseInt($('select#hor_down').val())>0 &&
                parseInt($('select#napr_up').val())>0 &&
                parseInt($('select#napr_down').val())>0 &&
                parseInt($('select#connection').val())>0 )
            {
                var out = {};
                out['decor_id'] = $('input#decor_id').val();
                out['profile_art_id'] = $('input#profile_art_id').val();
                out['profile_id'] = $('input#profile_id').val();
                out['decor_name'] = $('input#decor_name').val();
                out['decor_color'] = $('input#decor_color').val();
                out['vert'] = $('select#vert').val();
                out['hor_up'] = $('select#hor_up').val();
                out['hor_down'] = $('select#hor_down').val();
                out['napr_up'] = $('select#napr_up').val();
                out['napr_down'] = $('select#napr_down').val();
                out['connection'] = $('select#connection').val();
                  $.ajax({
                                    type: 'POST',
                                    url: 'RS/ajax_resp.php',
                                    data: {'data': JSON.stringify(out)},
                                    success: function (resp) {
                                        var resp_in = JSON.parse(resp);
                                        if (resp_in.result) {
                                            $(".forprofileDecor").html(resp_in.decors);
                                            $('#selectData').modal('hide');
                                        } else {

                                        }
                                    }
                                });
            }
            return false;
        });
        $(document).on('shown.bs.modal', '#creaDoors', function (event) {
            event.stopImmediatePropagation(); // off dubleclick
            var button = $(event.relatedTarget);
            var numDoor = button.data('numdoor');
            var numSec = button.data('numsec');
            var sectop = button.data('op');
            if(parseInt(numDoor)>0) {
                if(sectop === undefined) {
                    $('#numDoor').val(numDoor);

                    var doors_data = $('#creaRS #doors_data_' + numDoor).val();
                    var rs_size_y = $('#creaRS #rs_size_y').val();
                    var materials = $('#datalistMaterialsFull').html();
                    $(this).find(".modal-body-in").load("doors.php", {
                        'doorData': doors_data,
                        'materials': materials,
                        'doorYsize': rs_size_y
                    });
                    resetIndex();
                    recalcSections_distribute_add();
                }
                else
                {
                    $(this).find(".modal-body-in").load("filebrowse.php", {
                        'numDoor': numDoor,
                        'numSec': numSec,
                        'sectop':sectop
                    });
                }
            }else{
                $(this).find(".modal-body-in").load("browse4fill.php");
            }
            //    $(this).find(".modal-body-in").load("getdata.php?typeop="+typeop+"&editID="+editProfile);

        });

        $(document).on('click', 'button#addSection', function (event, sel) {
            event.stopImmediatePropagation(); // off dubleclick
            var doors_sections = parseInt($('#count_doors').val());
            var doors_sections_new = doors_sections + 1;
            var materials=$('#datalistMaterials').html();
            var out = {};
            out['materials'] = JSON.parse(materials);
            $.ajax({
                type: 'POST',
                url: 'ajax_cresp.php',
                data: {'newsection': JSON.stringify(out)},
                success: function (data) {
                    var resp_in = JSON.parse(data);
                    $('.sectionDoor').append(resp_in);
                    resetIndex();
                    recalcSections_distribute_add();
                }
            });
            return false;
        });

        $(document).on('click', 'button#saveDoor', function (event, sel) {
            event.stopImmediatePropagation(); // off dubleclick
            var out = {};
            var out3 = {};
            var desc = '';

            var numDoor = $('#numDoor').val();
            $('div.door-section').each(function (index, value) {
                var tmp_filler = $(this).find('select[name="sect_filler[]"]').find(':selected').val();
                var tmp_height = $(this).find('input[name="sect_height[]"]').val();
                var tmp_txt = $(this).find('select[name="sect_txt"]').val();
                desc = desc + 'Секция ,размер=' + tmp_height + ' Наполнитель='+tmp_filler;
                out[index] = {'height': tmp_height, 'filler': tmp_filler, 'index': index, 'txt_section': tmp_txt};
            });
            out3['numDoor']=numDoor;
            out3['sections']=out;
            out3['doorHeight']=$('input#door_size_y').val();
            out3['MaxSanH']=$('input#constructMaxSanH').val();
            $.ajax({
                type: 'POST',
                url: 'ajax_cresp.php',
                data: {'data': JSON.stringify(out3)},
                success: function (resp) {
                    var resp_in = JSON.parse(resp);
                    if (resp_in.result) {
                       $('.doors .row.door-' + numDoor + ' .rs-doors.inners-sections').html(resp_in.data);
                        //  $('#profiles .messages').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');

                    } else {
 //                       $('.messages').html('<div class="alert alert-danger" role="alert">' + resp_in.message + '</div>');

                    }
//                    setTimeout(function () { $('.messages').html(''); }, 5000);
                }
            });


                var out2 = JSON.stringify(out);
                $('#creaDoors').modal('hide');
                $('.doors .row.door-' + numDoor + ' #doors_data_' + numDoor).val(out2);
                $('.doors .row.door-' + numDoor + ' button').attr('data-valdoor', out2);
                $('.modal-backdrop').remove();
                changeStatusOK();
            return false;
        });
        $(document).on('change', '.sectionDoor input[name="sect_height[]"]', function (event, sel) {
            event.stopImmediatePropagation();
            var countEl = parseInt($("div.sectionDoor").attr('data-countElement'));
            numElement = parseInt($(this).parent().parent().attr('data-numElement'));
            doorY = $('input#door_size_y').val();
            recalcSections(countEl, numElement, doorY);
        });
        $(document).on('change', '.sectionDoor select[name="sect_filler[]"]', function (event, sel) {
            event.stopImmediatePropagation();
            var select_filler = $(this).find(':selected').val();
//            var select_filler_t =
            /*
            event.stopImmediatePropagation();
            var countEl = parseInt($("div.sectionDoor").attr('data-countElement'));
            numElement = parseInt($(this).parent().parent().attr('data-numElement'));
            doorY = $('input#door_size_y').val();
            recalcSections(countEl, numElement, doorY);
            */
        });
        $(document).on('click', '.modal-backdrop.show', function (event, sel) {
            event.stopImmediatePropagation();
            $('.modal-backdrop.show').remove();
        });

        function resetIndex() {
            var countEl = 0;
            $("div.door-section").each(function (indx, element) {
                $(element).attr('data-numElement', indx);
                countEl = countEl + 1;
            });
            $("div.sectionDoor").attr('data-countElement', countEl);
        }

        $(document).on('change', '.sectionDoor input[name="sect_fixheight[]"]', function (event, sel) {
            event.stopImmediatePropagation();
            var countEl = parseInt($("div.sectionDoor").attr('data-countElement'));
            numElement = parseInt($(this).parent().parent().attr('data-numElement'));
            doorY = $('input#door_size_y').val();
            if($(this).is(':checked'))
            {
                var checked_fixheight=1;
                $('div.door-section').each(function (index, value) {
                    if(!$(this).find('input[name="sect_fixheight[]"]').is(':checked')) checked_fixheight=0;
                });
                if(checked_fixheight===1)
                {
                    $(this).prop("checked", false);
                    $('.messages-result.messages-doors').html('<div class="alert alert-danger" role="alert">Все секции не фиксируются!</div>');
                }
                setTimeout(function () { $('.messages-result.messages-doors').html(''); }, 10000);
            }
            recalcSections(countEl, numElement, doorY);
        });
        function recalcSections(countEl, numElement, doorY) {
            if (countEl === 1) {
                $('div.door-section[data-numelement="0"] input[name="sect_height[]"]').val(doorY);
            } else {
                $('div.door-section input[name="sect_height[]"]').prop("disabled", false);
                freeSum=0;
                sumFixed=0;
                countFixed=0;
                currenFixed=0;
                currentVal=0;
                currentfreeline=-1;
                lastfreeline=-1;
                $('div.door-section').each(function (index, value) {
                    sumT = parseInt($(this).find('input[name="sect_height[]"]').val());
                    if(!$(this).find('input[name="sect_fixheight[]"]').is(':checked'))
                    {
                        // если не фиксирован
                        if(numElement!==index)
                        {
                            // Если не текущий
                            // freeSum=freeSum+sumT;
                        }
                        else
                        {
                            // Если текущий
                            currentVal=sumT;
                            currentfreeline=index;
                        }
                        lastfreeline=index;
                    }
                    else
                    {
                        if(numElement===index) currenFixed=1;
                        countFixed=countFixed+1;
                        if(numElement===index)
                        {
                            // Если текущий
                            currentVal=sumT;
                            // sumFixed=sumFixed+sumT;
                        }
                        else
                        {
                            // Если не текущий
                            sumFixed=sumFixed+sumT;
                        }

                    }
                });
                freeSumForR=doorY-currentVal;
                $('button#saveDoor').prop("disabled", false);
                if(currenFixed===1)
                {
                    if(countFixed===1) sumFixed=currentVal; else sumFixed=sumFixed+currentVal;
                    freeSum=doorY-sumFixed;
                    if(freeSum<100)
                    {
                        $('button#saveDoor').prop("disabled", true);
                        $('.messages-result.messages-doors').html('<div class="alert alert-danger" role="alert">Не достаточный размер для остальных секций !</div>');
                        setTimeout(function () { $('.messages-result.messages-doors').html(''); }, 10000);
                    }
                    else
                    {
                        // $('div.door-section input[name="sect_height[]"]').prop("disabled", false);
                        if(countEl-countFixed===1)
                        {
                            $('div.door-section[data-numelement="'+lastfreeline+'"] input[name="sect_height[]"]').prop("disabled", true);
                        }
                        recalcSections_distrib(countEl-countFixed,freeSum, -1, lastfreeline);
                    }
                }
                else
                {
                    freeSum=doorY-(sumFixed+currentVal);
                    if(freeSum<100)
                    {
                        $('button#saveDoor').prop("disabled", true);
                        $('.messages-result.messages-doors').html('<div class="alert alert-danger" role="alert">Не достаточный размер для остальных секций !</div>');
                        setTimeout(function () { $('.messages-result.messages-doors').html(''); }, 10000);
                    }
                    else
                    {
                        recalcSections_distrib(countEl - countFixed, freeSum, currentfreeline, lastfreeline);
                    }
                }
            }
        }
        function recalcSections_distrib(countEl, freeSum, ignored, thelastfreeline) {
            difference=0; if(ignored>-1) difference=1;
            var rez = freeSum / (countEl - difference);
            var line = parseInt(rez);
            // countEl=2    freeSum=1500   ignored=-1  thelastfreeline=2
            console.log('countEl='+countEl+'    freeSum='+freeSum+'   ignored='+ignored+'  thelastfreeline='+thelastfreeline);
            var last_line = line;
            if (rez !== line) last_line = last_line + 1;
            $('div.door-section').each(function (index, value) {
                sumT = parseInt($(this).find('input[name="sect_height[]"]').val());
                if (!$(this).find('input[name="sect_fixheight[]"]').is(':checked') && index!==ignored) {
                    if(index===thelastfreeline)
                    $(this).find('input[name="sect_height[]"]').val(last_line);
                    else
                    $(this).find('input[name="sect_height[]"]').val(line);

                }
            });

        }
        function recalcSections_sum() {
            sum = 0;
            $('div.door-section').each(function (index, value) {
                sumT = parseInt($(this).find('input[name="sect_height[]"]').val());
                if (!isNaN(sumT) && sumT > 0) sum = sum + sumT;
            });
            return sum;
        }

        function recalcSections_sumTo(to) {
            if (to === 0) return 0;
            sum = 0;
            $('div.door-section').each(function (index, value) {
                if (to > index) {
                    sumT = parseInt($(this).find('input[name="sect_height[]"]').val());
                    if (!isNaN(sumT) && sumT > 0) sum = sum + sumT;
                    // console.log('  -  sumT='+sumT+'  -  sum='+sum);
                }
            });
            return sum;
        }

        function recalcSections_distribute_add() {
            var count_sect = $('div.door-section').length;
            var doorY = $('input#door_size_y').val();
            recalcSections_distribute(doorY, count_sect, 0);
//                    $('div.door-section[data-numelement="0"] input[name="sect_height[]"]').val(doorY);
        }

        function recalcSections_distribute(sum, countEl, start) {

            var rez = sum / (countEl - start);
            var line = parseInt(rez);
            var last_line = line;
            if (rez !== line) last_line = last_line + 1;
            $('div.door-section').each(function (index, value) {
                if (index >= start) {
                    if (countEl === (index + 1)) {
                        $(this).find('input[name="sect_height[]"]').val(last_line);
                    } else {
                        $(this).find('input[name="sect_height[]"]').val(line);
                    }
                }
            });
        }

        function changeStatusOK() {
            var statusOK=true;
            $('#creaRS .consructions .doors>.rs-doors').each(function (index, value) {
                var doors_data = $('input#doors_data_'+(index+1)).val();

                if(!doors_data) statusOK=false;
            });
            if(statusOK===true)
                $('#creaRS button#send').removeClass('d-none');
            else
                $('#creaRS button#send').addClass('d-none');
        }
    });
})(jQuery);

//.. Функция для модального окна выбора контрагента
$(document).ready(function () {
    var client_filter_ckecker = 0;
    async function get_clients_filtered(client_id, phone = 0) {
        if(client_filter_ckecker == 1) {
            return false;
        }
        client_filter_ckecker = 1;
        console.log('clear');
        $('#client_variants ul').html(' ');
        console.log('clearz');
        $('#client_variants ul').html(`
            <div id="checks_lodaer_containers_auth">
              <div style="background: #f5f3f3;padding: 45px;display: flex;flex-flow: row nowrap;justify-content: center;align-items: center;position: absolute;width: 100%;height: 100%;z-index: 999999;">
                  <img src="/kronasapp/public/images/download.gif" alt="" width="45px">
              </div>
            </div>
        `);
        if(!client_id) {
            $('#filter_client').val('');
            if(phone == 1) {
                $('#filter_client_phone').val('');
            } else {
                $('#filter_client_name').val('');
            }

            return;
        }
        let request = await fetch('/manager_all_order.php?filter_client=' + client_id);
        let response = await request.json();
        let counter = 0;
        await response.forEach(function (element) {
            if(counter <= 10) {
                $('#client_variants ul').append(`
                    <li class="list-group-item" data-client="${element.code}" data-phone="${element.tel}">${element.name} (${element.agent_name})</li>
                `);
            }
            counter++;
        });
        $('#client_variants ul li').on('click', function (e) {
            let current_variant = $(e.target).attr('data-client');
            let current_phone = $(e.target).attr('data-phone');
            let current_variant_name = $(e.target).html();
            $('#filter_client').val(current_variant);
            if(phone == 1) {
                $('#filter_client_phone').val(current_phone);
            } else {
                $('#filter_client_name').val(current_variant_name);
            }
            $('#client_variants ul').html('   ');
        });
        setTimeout(function () {
            $('#checks_lodaer_containers_auth').hide();
            client_filter_ckecker = 0;
        },1000);
    }





    async function get_clients_filtered_auth(client_id, phone = 0) {
        if(client_filter_ckecker == 1) {
            return false;
        }
        client_filter_ckecker = 1;
        $('#client_variants_auth ul').html(' ');
        $('#client_variants_auth ul').html(`
            <div id="checks_lodaer_containers">
              <div style="background: #f5f3f3;padding: 45px;display: flex;flex-flow: row nowrap;justify-content: center;align-items: center;position: absolute;width: 100%;height: 100%;z-index: 999999;">
                  <img src="/kronasapp/public/images/download.gif" alt="" width="45px">
              </div>
            </div>
        `);
        if(!client_id) {
            $('#filter_client_auth').val('');
            if(phone == 1) {
                $('#filter_client_phone_auth').val('');
            } else {
                $('#filter_client_name_auth').val('');
            }
            return;
        }
        let request = await fetch('/manager_all_order.php?filter_client=' + client_id);
        // let request = await fetch('http://service.kronas.com.ua/manager_all_order.php?filter_client=' + client_id);
        let response = await request.json();
        let counter = 0;
        await response.forEach(function (element) {
            if(counter <= 10) {
                $('#client_variants_auth ul').append(`
                    <li class="list-group-item" data-client="${element.client_id}" data-code="${element.code}" data-phone="${element.tel}">${element.name} (${element.agent_name})</li>
                `);
            }
            counter++;
        });
        $('#client_variants_auth ul li').on('click', function (e) {
            let current_variant = $(e.target).attr('data-client');
            let current_code = $(e.target).attr('data-code');
            let current_phone = $(e.target).attr('data-phone');
            let current_variant_name = $(e.target).html();
            $('#filter_client_auth').val(current_variant);
            $('#filter_client_code_auth').val(current_code);
            if(phone == 1) {
                $('#filter_client_phone_auth').val(current_phone);
            } else {
                $('#filter_client_name_auth').val(current_variant_name);
            }
            $('#client_variants_auth ul').html('   ');
        });
        setTimeout(function () {
            $('#checks_lodaer_containers').hide();
            client_filter_ckecker = 0;
        },1000);
    }

    $(document).ready(function () {
        $('#filter_client_name').on('input', async function (e) {
            let client_data = $(e.target).val();
            if (client_data.lengthn > 0) {
                await get_clients_filtered(client_data);
            }
        });
        $('#filter_client_phone').on('input', async function (e) {
            let client_data = $(e.target).val();
            if (client_data.lengthn > 0) {
                await get_clients_filtered(client_data, 1);
            }
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
        $('#filter_client_phone').mask('+38(000)000-00-00', {
            placeholder: "+38(0__)___-__-__"
        });
        $('#myTab .nav-item .nav-link').on('click', function() {
            $('#filter_client_phone').val('');
            $('#filter_client_name').val('');
            $('#client_variants ul').html(' ');
        });

    });
    //.. 12.11.2020 Изменения: делегирование. $('#new-order-form').on
    $(document).ready(function () {
        $('#new-order-form').on('input', '#filter_client_name_auth', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered_auth(client_data);
        });
        $('#new-order-form').on('input', '#filter_client_phone_auth', async function (e) {
            let client_data = $(e.target).val();
            await get_clients_filtered_auth(client_data, 1);
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
        $('#filter_client_phone_auth').mask('+38(000)000-00-00', {
            placeholder: "+38(0__)___-__-__"
        });
        $('#new-order-form').on('click', '#myTab1 .nav-item .nav-link', function() {
            $('#filter_client_phone_auth').val('');
            $('#filter_client_name_auth').val('');
            $('#client_variants_auth ul').html(' ');
        });
        $('#new-order-form').on('click', '#client_auth_form_button', function (e) {
            e.preventDefault();
            $('#client_auth_message').html(' ');
            if($('#filter_client_auth').val() == null || $('#filter_client_auth').val() == '' || $('#filter_client_code_auth').val() == null || $('#filter_client_code_auth').val() == '') {
                $('#client_auth_message').html('<span style="color:red">Необходимо выбрать клиента из выпадающего списка!</span>');
            } else {
                $('#sac_form').submit();
            }
        })
    });
    //.. 12.11.2020 Функция выбора заказа
    // $(document).ready(function () {
    //     $('.order-chioce').on('click', function() {
    //         let order = $(this).attr('id');
    //         if (order == 'PZ') {
    //             $('#RS').removeClass('active-choice');
    //             if (!$('#PZ').hasClass('active-choice')) {
    //                 $('#PZ').addClass('active-choice');
    //             }
    //         }
    //         if (order == 'RS') {
    //             $('#PZ').removeClass('active-choice');
    //             if (!$('#RS').hasClass('active-choice')) {
    //                 $('#RS').addClass('active-choice');
    //             }
    //         }
    //         $.post("ajax_resp.php", {"newOrderForm": order}).done(function (data) {
    //             $('#new-order-form').fadeOut(250, function(){
    //                 $(this).html(data);
    //                 $(this).fadeIn(500);
    //             });
    //         });
    //     });
    // });

    // Полушин. Замена url при создании заказа менеджером
    $('.RSButt').on('click', function (event) {
        event.preventDefault();
        let form = $('#sac_form');
        let formAction = form.attr('action');

        let getNW = formAction.substring(formAction.indexOf('?'));
        formAction = formAction.substring(0, formAction.indexOf('?'));

        if (formAction.indexOf('/clients') > 1) formAction = formAction.substring(0, formAction.indexOf('/clients'));
        if ($(this).attr('title') === 'Стеколка') form.attr('action', formAction + "/clients/glass/index.php" + getNW);
        if ($(this).attr('title') === 'Раздвижные системы') form.attr('action', formAction + "/clients/RS/clients/create.php" + getNW);
    });
});