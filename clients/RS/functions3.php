<?php

include_once(__DIR__.'/../../_1/config.php');
//include_once('../functions2.php');
function db_profile_link_count($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_link_count start', $_SESSION);
    $sql = "SELECT 
	(( SELECT COUNT(*) FROM RS_decor_profile WHERE `profile` = pr.id )+
	( SELECT COUNT(*) FROM RS_dovodchik WHERE `profile` = pr.id )+
	( SELECT COUNT(*) FROM RS_profile_rolik WHERE `profile` = pr.id )+
	( SELECT COUNT(*) FROM RS_schetka WHERE `profile` = pr.id )+
	( SELECT COUNT(*) FROM RS_stopor WHERE `profile` = pr.id )) AS cnt  
FROM
	RS_profile AS pr 
WHERE
	pr.id =" . $profile_id;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_link_count end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function db_profile_exist($name,$width_cross)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_exist start', $_SESSION);
    $sql = "SELECT COUNT(*) AS cnt FROM RS_profile WHERE name='" . trim($name) . "' AND width_cross=" . $width_cross. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_exist end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function db_get_profile_full($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_full start', $_SESSION);
    $sql = "SELECT
	p.*, 
	pa.rs_profile_article_id, 
	pa.hor_up, 
	pa.hor_down, 
	pa.vert, 
	pa.napr_up, 
	pa.napr_down, 
	pa.`connection`
FROM
	RS_profile AS p
	INNER JOIN
	RS_profile_article AS pa
	ON 
		p.id = pa.`profile` 
WHERE p.id=$profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_full end', $_SESSION);
    return $result[0];
}
function db_profile_add($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_add start', $_SESSION);
    $sql="INSERT INTO RS_profile ( name, width_l_sheet, height_w_sheet, wdth_l_glass, height_w_glass, width_cross, profile_type, koef_h_door, 
 koef_l_hor_pofile, profile_fr_l, profile_fr_r, profile_fr_t, profile_fr_b, 
max_door_height, max_door_width, max_door_length, con_sheet_gl, con_sheet_sheet, con_mir_gl, min_door_height, min_door_width  )
VALUES ('".trim($in->newprofile)."', $in->width_l_sheet, $in->height_w_sheet, $in->wdth_l_glass, $in->height_w_glass, $in->newprofilewcr, 
'$in->newprofiletype', $in->koef_h_door, $in->koef_l_hor_pofile, $in->indent_l, $in->indent_r, $in->indent_t, $in->indent_b, 
$in->max_door_height, $in->max_door_length, $in->max_wdth_in, $in->con_sheet_glass, $in->con_sheet_sheet, $in->con_glass_glass, $in->min_door_height, $in->min_door_length);";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_add end', $_SESSION);
    return $result['id'];
}
function db_profile_edit($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_edit start', $_SESSION);
    $sql = "UPDATE RS_profile SET name = '" . trim($in->newprofile) . "', 
`width_l_sheet` = " . $in->width_l_sheet. ", 
`height_w_sheet` = " . $in->height_w_sheet. ", 
`wdth_l_glass` = " . $in->wdth_l_glass. ", 
`height_w_glass` = " . $in->height_w_glass. ", 
`width_cross` = " . $in->newprofilewcr. ", 
`profile_type` = '" . $in->newprofiletype. "', 
`koef_h_door` = " . $in->koef_h_door. ",  
`koef_l_hor_pofile` = " . $in->koef_l_hor_pofile. ",  
`profile_fr_l` = " . $in->indent_l. ", 
`profile_fr_r` = " . $in->indent_r. ", 
`profile_fr_t` = " . $in->indent_t. ", 
`profile_fr_b` = " . $in->indent_b. ", 
`max_door_height`      = " . $in->max_door_height. ", 
`max_door_width`       = " . $in->max_door_length. ", 
`max_door_length`      = " . $in->max_wdth_in. ", 
`con_sheet_gl`         = " . $in->con_sheet_glass. ", 
`con_sheet_sheet`      = " . $in->con_sheet_sheet. ", 
`con_mir_gl`           = " . $in->con_glass_glass. ", 
`min_door_height`      = " . $in->min_door_height. ", 
`min_door_width`       = " . $in->min_door_length. "



WHERE id = $in->profile_id;";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_edit end', $_SESSION);
}
function db_profile_article_add($profile, $in )
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_article_add start', $_SESSION);
    $sql = "INSERT INTO RS_profile_article(profile, hor_up, hor_down, vert, napr_up, napr_down, `connection` 
) VALUES (" . $profile. ", '" . trim($in->hor_up) . "', '" . trim($in->hor_down) . "', '" . trim($in->vert) . "', '" .
        trim($in->napr_up) . "','" . trim($in->napr_down) . "', '" . trim($in->connection) . "')";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_article_add end', $_SESSION);
}
function db_profile_article_edit($profile, $in )
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_article_edit start', $_SESSION);
    $sql = "UPDATE RS_profile_article SET 
    hor_up='" . trim($in->hor_up) . "',
     hor_down='" . trim($in->hor_down) . "', 
     vert='" . trim($in->vert) . "', 
     napr_up='" . trim($in->napr_up) . "', 
     napr_down='" . trim($in->napr_down) . "', 
     `connection`='" . trim($in->connection) . "'  WHERE profile = $profile;";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_article_edit end', $_SESSION);
}
function db_profile_erase($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_erase start', $_SESSION);
    $sql="DELETE FROM RS_profile WHERE id = $profile_id";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_erase end', $_SESSION);
}
function db_profile_article_erase($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_article_erase start', $_SESSION);
    $sql="DELETE FROM RS_profile_article WHERE profile = $profile_id";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_article_erase end', $_SESSION);
}
function db_profile_list_options($rs_profile=0)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_list_options start', $_SESSION);
    $out='<option value="none" '.($rs_profile==0?'selected':'').'>Выбрать...</option>'."\n";
    $sql = "SELECT  * FROM RS_profile ORDER BY name";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $_selected=''; if($rs_profile==$line['id']) $_selected=' selected ';
        $out.='<option value="'.$line['id'].'" '.$_selected.'>'.$line['name'].'</option>'."\n";
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_profile_list_options end', $_SESSION);
    return $out;
}
function db_decor_list_options_on_profile($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_list_options_on_profile start', $_SESSION);
    $out=array();
    $sql = "SELECT DISTINCTROW d.id, d.`name`, d.color FROM RS_decor AS d
	INNER JOIN RS_decor_profile AS dp ON d.id = dp.decor
	INNER JOIN RS_profile AS p ON p.id = dp.`profile` 
WHERE p.id = $profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['id']]=$line['name'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_list_options_on_profile end', $_SESSION);
    return $out;
}
function db_decor_add($name, $color)
{
    $clrID=(int) db_get_decor_id($name, $color);
    if($clrID>0) return $clrID;
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_add start', $_SESSION);
    $sql = "INSERT INTO RS_decor(name, `color`) VALUES ('" . trim($name) . "', '" . trim($color) . "')";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_add end', $_SESSION);
    return $result['id'];
}

function db_decor_update($name, $color, $id)
{
    $clrID=(int) db_get_decor_id($name, $color);
    if($clrID>0 && $clrID!=$id) return $clrID;
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_update start', $_SESSION);
    $sql = "UPDATE RS_decor SET 
`name`='" . trim($name) . "',
`color`='" . trim($color) . "' WHERE id =$id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_update end', $_SESSION);
    return $id;
}
function db_get_decor_id($name, $color)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor_id start', $_SESSION);
    $sql = "SELECT id FROM RS_decor WHERE name='" . trim($name) . "' AND `color`='". trim($color) . "'";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor_id end', $_SESSION);
    return $result['data'][0]['id'];
}
function db_get_decor($id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor start', $_SESSION);
    $sql = "SELECT name, `color` FROM RS_decor WHERE id=$id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor end', $_SESSION);
    return $result['data'][0];
}
function db_get_empty_decor($decor_id, $profile_id, $exclude)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_link_count start', $_SESSION);
    $sql = "select sum(cnt) count_decor  from
(SELECT COUNT(*) AS cnt  FROM 	RS_decor_profile
WHERE RS_decor_profile.rs_decor_profile_id NOT IN (".implode(',',$exclude).") AND
	RS_decor_profile.decor = 3
UNION ALL 
SELECT 	COUNT(*) AS cnt FROM RS_schetka
WHERE 	RS_schetka.decor = $decor_id AND RS_schetka.`profile`<>$profile_id) scr ";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_link_count end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function db_decor_profile_add($simple, $pr_art, $tp_art, $decor, $profile)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_profile_add start', $_SESSION);
    $sql = "INSERT INTO RS_decor_profile(simple, profile_article, type_profile_article, decor, profile) VALUES ($simple, $pr_art, '" . trim($tp_art) . "', $decor, $profile)";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_profile_add end', $_SESSION);
    return $result['id'];
}

function db_decor_profile_update($simple, $pr_art, $decor, $profile, $dp_id )
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_profile_update start', $_SESSION);
    $sql = "UPDATE RS_decor_profile SET 
simple=$simple,
profile_article=$pr_art,
decor=$decor,
profile=$profile 
 WHERE id =$dp_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_profile_update end', $_SESSION);
}


function db_decor_link_count($decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_link_count start', $_SESSION);
    $sql = "SELECT COUNT(*) AS cnt FROM RS_decor_profile WHERE decor=" . $decor_id. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_link_count end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function db_unlink_decor($profile_id, $decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_decor start', $_SESSION);
    $sql="DELETE FROM RS_decor_profile WHERE decor=$decor_id AND profile=$profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_decor end', $_SESSION);
}
function db_decor_ext_link_count($decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_ext_link_count start', $_SESSION);
    $sql = "SELECT COUNT(*) AS cnt FROM RS_schetka WHERE decor=" . $decor_id. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_ext_link_count end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function db_decor_profile_link_count($decor_id, $profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_profile_link_count start', $_SESSION);
    $sql = "SELECT COUNT(*) AS cnt FROM RS_decor_profile WHERE decor=" . $decor_id. " AND profile=" . $profile_id. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_profile_link_count end', $_SESSION);
    return $result['data'][0]['cnt'];
}
function db_decor_erase($decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_erase start', $_SESSION);
    $sql="DELETE FROM RS_decor WHERE id = $decor_id";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_decor_erase end', $_SESSION);
}
function db_link_profile_decor($decor_id, $profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_decor start', $_SESSION);
    $sql="INSERT INTO RS_decor_profile(decor, profile) VALUES ($decor_id, $profile_id)";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_decor end', $_SESSION);
}
function db_link_profile_dovodchik($profile_id, $dovodchik_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_dovodchik start', $_SESSION);
    $sql = "SELECT COUNT(*) AS cnt FROM RS_dovodchik WHERE profile=" .$profile_id. " AND dovodchik=" . $dovodchik_id.  "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $cnt=$result['data'][0]['cnt'];
    if($cnt==0) {
        $sql="INSERT INTO RS_dovodchik(dovodchik, profile) VALUES ($dovodchik_id, $profile_id)";
        sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_dovodchik end', $_SESSION);
}
function db_unlink_profile_dovodchik($profile_id, $dovodchik_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_dovodchik start', $_SESSION);
    $sql = "DELETE FROM RS_dovodchik WHERE profile=" .$profile_id. " AND dovodchik=" . $dovodchik_id.  "";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_dovodchik end', $_SESSION);
}
function db_link_profile_rolick($profile_id, $rolick_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_rolick start', $_SESSION);
    $sql="INSERT INTO RS_profile_rolik(	rolik, profile) VALUES ($rolick_id, $profile_id)";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_rolick end', $_SESSION);
}
function db_unlink_profile_rolick($profile_id, $rolick_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_rolick start', $_SESSION);
    $sql = "DELETE FROM RS_profile_rolik WHERE profile=" .$profile_id. " AND rolik=" . $rolick_id.  "";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_rolick end', $_SESSION);
}
function db_link_profile_schetka($profile_id, $decor_id,$schetka_id, $glue)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_schetka start', $_SESSION);
    $sql = "SELECT COUNT(*) AS cnt FROM RS_schetka WHERE profile=" .$profile_id. " AND decor=" . $decor_id. " AND schetka=" . $schetka_id. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $cnt=$result['data'][0]['cnt'];
    if($cnt==0) {
        $sql = "INSERT INTO RS_schetka(profile, decor, schetka, glue) VALUES ($profile_id,$decor_id,$schetka_id,$glue)";
        sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_schetka end', $_SESSION);
}
function db_schetka_change_glue($profile_id, $decor_id,$schetka_id, $glue)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_schetka_change_glue start', $_SESSION);
    $sql = "UPDATE RS_schetka SET glue =".($glue=='glue_set'?1:0)." WHERE profile=" .$profile_id. " AND decor=" . $decor_id. " AND schetka=" . $schetka_id. "";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_schetka_change_glue end', $_SESSION);
}
function db_unlink_profile_schetka($profile_id, $decor_id,$schetka_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_schetka start', $_SESSION);
    $sql = "DELETE  FROM RS_schetka WHERE profile=" .$profile_id. " AND decor=" . $decor_id. " AND schetka=" . $schetka_id. "";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_schetka end', $_SESSION);
}
function db_link_profile_stopor($profile_id, $stopor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_stopor start', $_SESSION);
    $sql="INSERT INTO RS_stopor(profile, stotpor) VALUES ($profile_id, $stopor_id)";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_link_profile_stopor end', $_SESSION);
}
function db_unlink_profile_stopor($profile_id, $stopor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_stopor start', $_SESSION);
    $sql="DELETE  FROM RS_stopor WHERE profile=$profile_id AND stotpor=$stopor_id";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_unlink_profile_stopor end', $_SESSION);
}
function db_get_profile_decors($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_decors start', $_SESSION);
    $out=array();
    $sql = "SELECT RS_decor.id, RS_decor.`name` 
FROM RS_profile
	INNER JOIN RS_decor_profile ON RS_profile.id = RS_decor_profile.`profile`
	INNER JOIN RS_decor ON RS_decor_profile.decor = RS_decor.id 
WHERE RS_profile.id = ".$profile_id;
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['id']]=$line['name'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_decors end', $_SESSION);
    return $out;
}
function get_simples_options($profile_id, $res, $selected=array())
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_simples_options start', $_SESSION);
    $_arr_search=array(
        $res['hor_up'],
        $res['hor_down'],
        $res['vert'],
        $res['napr_up'],
        $res['napr_down'],
        $res['connection'],
    );
    $_arr_search2=array(
        $res['hor_up'] => 'hor_up',
        $res['hor_down'] => 'hor_down',
        $res['vert'] => 'vert',
        $res['napr_up'] => 'napr_up',
        $res['napr_down'] => 'napr_down',
        $res['connection'] => 'connection',
    );
    $list_opt=array('hor_up'=>'','hor_down'=>'','vert'=>'','napr_up'=>'','napr_down'=>'','connection'=>''    );
    foreach ($list_opt as $key=>$value)
    {
        // if(isset($selected[$key]) && (int)$selected[$key]>0)
        $selded='';
//        if(!isset($selected[$key])) $selded=' selected ';
        $list_opt[$key]='<option value="none" '.$selded.'>Выбрать</option>'."\n";
    }
    $sql = "SELECT SIMPLE_ID, `NAME`, MY_1C_NOM FROM SIMPLE WHERE MY_1C_NOM IN (".implode(',',$_arr_search).")";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $_key=$_arr_search2[$line['MY_1C_NOM']];
        $selded='';
        if(isset($selected[$_key]) && $line['SIMPLE_ID']==$selected[$_key]) $selded=' selected ';
        $list_opt[$_key].='<option value="'.$line['SIMPLE_ID'].'" '.$selded.'>'.$line['NAME'].'</option>'."\n";
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_simples_options end', $_SESSION);
    return $list_opt;
}
function get_simples_options_selected($profile_id,$decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_simples_options_selected start', $_SESSION);
    $out=array();
    $sql = "SELECT * FROM RS_decor_profile 
WHERE 	decor=$decor_id  AND `profile` = $profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['type_profile_article']]=$line['simple'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'get_simples_options_selected end', $_SESSION);
    return $out;
}

function create_profile_decors($in, &$out, $selected=0)
{if(count($in)>0)
{
    $out ='<select multiple class="form-control" id="profileDecors" name="profileDecors">';
    foreach ($in as $key=>$line)
    {
        $_selected=''; if($key==$selected) $_selected=' selected ';
        $out.='<option value="'.$key.'" '.$_selected.'>'.$line.'</option>';
    }
    $out.='</select><div class="profileDecorsMess"></div>';
}
}
function db_get_decor_color($decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor_color start', $_SESSION);
    $sql = "SELECT color  FROM RS_decor WHERE id=" . $decor_id. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor_color end', $_SESSION);
    return $result['data'][0]['color'];
}
function db_get_profile_type($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor_color start', $_SESSION);
    $sql = "SELECT profile_type FROM RS_profile WHERE id=" . $profile_id. "";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_decor_color end', $_SESSION);
    return $result['data'][0]['profile_type'];

}
function db_get_profile_schetka($profile_id, $decor_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_schetka start', $_SESSION);
    $out=array();
    $sql = "SELECT SIMPLE.SIMPLE_ID, SIMPLE.`CODE`, SIMPLE.`NAME`, RS_schetka.glue 
            FROM RS_schetka INNER JOIN SIMPLE ON RS_schetka.schetka = SIMPLE.SIMPLE_ID
                WHERE RS_schetka.`profile` = $profile_id AND RS_schetka.decor = $decor_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $glue='';
        if($line['glue']==1) $glue='[ Клеевая ] ';
        $out[$line['SIMPLE_ID']]='('.$line['CODE'].') '.$glue.$line['NAME'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_schetka end', $_SESSION);
    return $out;
}
function create_profile_schetka($in, &$out, $selected=0)
{
    if(count($in)>0)
    {
        $out ='<select multiple class="form-control" id="profileSchetka" name="profileSchetka">';
        foreach ($in as $key=>$line)
        {
            $out.='<option value="'.$key.'" '.($selected==$key?' selected ':'').'>'.$line.'</option>';
        }
        $out.='</select><div class="profileSchetkaMess"></div>';

    }
}

function db_get_profile_dovodchik($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_dovodchik start', $_SESSION);
    $out=array();
    $sql = "SELECT SIMPLE.SIMPLE_ID, SIMPLE.`CODE`, SIMPLE.`NAME`
            FROM SIMPLE INNER JOIN RS_dovodchik ON SIMPLE.SIMPLE_ID = RS_dovodchik.dovodchik
WHERE RS_dovodchik.`profile` = $profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['SIMPLE_ID']]='('.$line['CODE'].') '.$line['NAME'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_dovodchik end', $_SESSION);
    return $out;
}
function db_get_exist_my1cnom($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_exist_my1cnom start', $_SESSION);
    $in=array_unique($in);
    $sql="SELECT SUM(DISTINCT CASE ";
    $counter=1; $verify=0;
    foreach ($in as $value)
    {
        $sql.="WHEN SIMPLE.MY_1C_NOM = $value 
        THEN $counter \n";
        $verify=$verify+$counter;
        $counter++;
    }
    $sql.="END) AS myVer FROM SIMPLE WHERE SIMPLE.MY_1C_NOM IN (".implode(',', $in).")";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_exist_my1cnom end', $_SESSION);
    return $result['data'][0]['myVer']==$verify?TRUE:FALSE;
}
function create_profile_dovodchik($in, &$out, $selected=0, $addNone=FALSE)
{
    if(count($in)>0)
    {
        $out ='<select multiple class="form-control" id="profileDovodchik" name="profileDovodchik">';
        if($addNone) $out.='<option value="0" '.($selected==0?' selected ':'').'>Без доводчика</option>';
        foreach ($in as $key=>$line)
        {
            $out.='<option value="'.$key.'" '.($selected==$key?' selected ':'').'>'.$line.'</option>';
        }
        $out.='</select><div class="profileDovodchikMess"></div>';
    }
}
function db_get_profile_rolick($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_rolick start', $_SESSION);
    $out=array();
    $sql = "SELECT SIMPLE.SIMPLE_ID, SIMPLE.`CODE`, SIMPLE.`NAME`
            FROM SIMPLE INNER JOIN RS_profile_rolik ON SIMPLE.SIMPLE_ID = RS_profile_rolik.rolik
WHERE RS_profile_rolik.`profile` = $profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['SIMPLE_ID']]='('.$line['CODE'].') '.$line['NAME'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_rolick end', $_SESSION);
    return $out;
}
function create_profile_rolick($in, &$out, $selected=0)
{
    if(count($in)>0)
    {
        $out ='<select multiple class="form-control" id="profileRolick" name="profileRolick">';
        foreach ($in as $key=>$line)
        {
            $out.='<option value="'.$key.'" '.($selected==$key?' selected ':'').'>'.$line.'</option>';
        }
        $out.='</select><div class="profileRolickMess"></div>';
    }
}
function db_get_profile_stopor($profile_id)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_stopor start', $_SESSION);
    $out=array();
    $sql = "SELECT SIMPLE.SIMPLE_ID, SIMPLE.`CODE`, SIMPLE.`NAME`
            FROM SIMPLE INNER JOIN RS_stopor ON SIMPLE.SIMPLE_ID = RS_stopor.stotpor
WHERE RS_stopor.`profile` = $profile_id";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['SIMPLE_ID']]='('.$line['CODE'].') '.$line['NAME'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_profile_stopor end', $_SESSION);
    return $out;
}
function create_profile_stopor($in, &$out, $selected=0, $addNone=FALSE)
{
    if(count($in)>0)
    {
        $out ='<select multiple class="form-control" id="profileStopor" name="profileStopor">';
        foreach ($in as $key=>$line)
        {
            $out.='<option value="'.$key.'" '.($selected==$key?' selected ':'').'>'.$line.'</option>';
        }
        if($addNone) $out.='<option value="0" '.($selected==0?' selected ':'').'>Без стопора</option>';
        $out.='</select><div class="profileStoporkMess"></div>';
    }
}

function db_get_list4searchSimple($search_in, $objectName)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4searchSimple start', $_SESSION);
    $out='<ul class="list-group">';
    $search_in=trim($search_in);
    $sql=db_get_list_simple($search_in, 1, 10);
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    get_list4searchSimple($result, $out, $objectName);
    $out.="</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4searchSimple end', $_SESSION);
    return $out;
}
function get_list4searchSimple($result, &$out, $objectName)
{
    foreach($result as $row)  {
        $out.="<li  onClick=\"list4searchSimple('".$row['SIMPLE_ID']."', '(".$row['CODE'].') '.$row['NAME']."', '".$objectName."');\" class=\"list-group-item\">(".$row['CODE'].') '.$row['NAME']."</li>";
    }
}
function db_get_list4searchOnTextFilling($search_in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4searchOnTextFilling start', $_SESSION);
    $out='<ul class="list-group">';
    $search_in=trim($search_in);
    $sql="SELECT * FROM MATERIAL WHERE (RS_PLATE = 1 OR RS_GLASS=1 OR RS_MIRROR=1) AND 
    (`NAME` LIKE \"%$search_in%\" OR `CODE`  LIKE \"%$search_in%\") AND `NAME` NOT LIKE \"%оргстекло%\" LIMIT 0,10";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    list_OnTextFilling_create($result, $out);
    $out.="</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_list4searchOnTextFilling end', $_SESSION);
    return $out;
}
function list_OnTextFilling_create($result, &$out)
{
    foreach ($result as $row) {
        $out.="<li  onClick=\"OnTextFilling(".$row['CODE'].", '".$row['NAME']."');\" class=\"list-group-item\">(".$row['CODE'].') '.$row['NAME']."</li>";
    }
}
function db_get_list_simple($search_in, $op, $limit)
{
$_where=" WHERE (CONVERT(CODE,char) LIKE '$search_in%' OR  NAME LIKE '%$search_in%')   ";

    $sql = "SELECT DISTINCTROW   * 
FROM 
SIMPLE 
$_where 
ORDER BY `NAME` ASC LIMIT 0,".$limit ;
    return $sql;
}
function createFullMaterials($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'createFullMaterials start', $_SESSION);
    $_keys=array_keys($in);

    $out=array();
    $sql = "
SELECT
MATERIAL.`NAME` as mname,
	MATERIAL.`CODE` as mcode,
	gtreeparent.`NAME` as mtype,
	gtreeparent2.`NAME` AS mtype2  
FROM
	MATERIAL
	INNER JOIN GOOD_TREE_MATERIAL_CONN ON MATERIAL.MATERIAL_ID = GOOD_TREE_MATERIAL_CONN.MATERIAL_ID
	INNER JOIN GOOD_TREE ON GOOD_TREE_MATERIAL_CONN.FOLDER_ID = GOOD_TREE.FOLDER_ID
	LEFT JOIN GOOD_TREE AS gtreeparent ON GOOD_TREE.PARENT = gtreeparent.FOLDER_ID
	LEFT JOIN GOOD_TREE AS gtreeparent2 ON gtreeparent.PARENT = gtreeparent2.FOLDER_ID 
WHERE
	MATERIAL.`CODE` IN (".implode(',', $_keys).")";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['mcode']]=$in[$line['mcode']];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'createFullMaterials end', $_SESSION);
    return $out;
}

function rs_create_ini(array $a, array $parent = array())
{
    $out = '';
    foreach ($a as $k => $v)
    {
        if (is_array($v))
        {
            $sec = array_merge((array) $parent, (array) $k);
            $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
            $out .= rs_create_ini($v, $sec);
        }
        else
        {
            $out .= "$k=$v" . PHP_EOL;
        }
    }
    return $out;
}
function rs_save_ini($in)
{
    $inisave = rs_create_ini($in);
    $file_handle = fopen("config.ini", "w");
    fwrite($file_handle, $inisave);
    fclose($file_handle);

}
function rs_load_ini()
{
    $setting=parse_ini_file("config.ini",TRUE);
    return $setting;

}
function db_change_limit($in)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_change_limit start', $_SESSION);
    $sql = "UPDATE RS_profile SET name = '" . trim($in->newprofile) . "', 
width_l_sheet = " . $in->width_l_sheet. ", 
height_w_sheet = " . $in->height_w_sheet. ", 
wdth_l_glass = " . $in->wdth_l_glass. ", 
height_w_glass = " . $in->height_w_glass. ", 
width_cross = " . $in->newprofilewcr. ", 
profile_type = '" . $in->newprofiletype. "' WHERE id = $in->profile_id;";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_change_limit end', $_SESSION);
}

function db_get_setting_profile()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_setting_profile start', $_SESSION);
    $out=array();
    $sql = "SELECT * FROM RS_general";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $out[$line['name']]=$line['value'];
    }
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_setting_profile end', $_SESSION);
    return $out;
}
function db_get_setting_profile_full()
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_setting_profile start', $_SESSION);
    $sql = "SELECT * FROM RS_general";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_setting_profile end', $_SESSION);
    return $result;
}
function db_change_setting_profile($maxdh, $maxdl, $mindh, $mindl, $maxwi, $consg, $conss, $congg, $MaxSanH)
{

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_setting_profile start', $_SESSION);
    $sql = "
UPDATE RS_general
SET value = CASE name
    WHEN 'max_door_height' THEN $maxdh
    WHEN 'max_door_length' THEN $maxdl
    WHEN 'min_door_height' THEN $mindh
    WHEN 'min_door_length' THEN $mindl
    WHEN 'max_wdth_in' THEN $maxwi
    WHEN 'con_sheet_glass' THEN $consg
    WHEN 'con_sheet_sheet' THEN $conss
    WHEN 'con_glass_glass' THEN $congg
    WHEN 'max_sandblasting_height' THEN $MaxSanH
END
WHERE name in ('max_door_height', 'max_door_length', 'min_door_height', 'min_door_length', 'max_wdth_in', 'con_sheet_glass', 'con_sheet_sheet', 'con_glass_glass', 'max_sandblasting_height')
    ";
    sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'db_get_setting_profile end', $_SESSION);
}

