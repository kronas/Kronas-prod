<?php
session_start();
include_once('../functions2.php');
include_once('functions3.php');
$theCHLD=4;
include_once('header.php');
$profile_id=0;
if(isset($_GET) && $_GET['typeop'] && $_GET['profileID'] && (int)$_GET['typeop']==3 && (int)$_GET['profileID']>0)
{
    $profile_id=(int)$_GET['profileID'];
    $decor_id=(int)$_GET['decorID'];
    $res=db_get_profile_full($profile_id);
    $decor=db_get_decor($decor_id);
    $sel_opts=get_simples_options_selected($profile_id,$decor_id);
    $selected=get_simples_options($profile_id, $res, $sel_opts);
}
?>
    <input type="hidden" name="profile_art_id"  id="profile_art_id" value="<?=$res['rs_profile_article_id']?>">
    <input type="hidden" name="profile_id"  id="profile_id" value="<?=$profile_id?>">
    <input type="hidden" name="decor_id"  id="decor_id" value="<?=$decor_id?>">
    <input type="hidden" name="dp_vert_id"  id="dp_vert_id" value="<?=$sel_opts['vert']?>">
    <input type="hidden" name="dp_hor_up_id"  id="dp_hor_up_id" value="<?=$sel_opts['hor_up']?>">
    <input type="hidden" name="dp_hor_down_id"  id="dp_hor_down_id" value="<?=$sel_opts['hor_down']?>">
    <input type="hidden" name="dp_napr_up_id"  id="dp_napr_up_id" value="<?=$sel_opts['napr_up']?>">
    <input type="hidden" name="dp_napr_down_id"  id="dp_napr_down_id" value="<?=$sel_opts['napr_down']?>">
    <input type="hidden" name="dp_connection_id"  id="dp_connection_id" value="<?=$sel_opts['connection']?>">


    <div class="row">
    <div class="col-12">
        <h3 class="text-center mb-4">Детали профиля <?=$res['name']?></h3>
    </div>
</div>
<div class="row decors">
    <div class="col-md-3">
        <div class="form-group" >
            <label for="decor_name">Название декора</label>
            <input type = "text" class="form-control" id = "decor_name" name = "decor_name" value="<?=$decor['name']?>">
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="decor_color">Цвет декора (#000000)</label>
            <input type = "text" class="form-control" id = "decor_color" name = "decor_color" value="<?=$decor['color']?>">
        </div >
    </div>
</div>
    <div class="row decors">
        <div class="col-md-4">
            <div class="form-group" >
                <label for="vert">Вертикальный</label>
                <select  class="form-control" id="vert" name="vert">
                    <?=$selected['vert']?>
                </select>
            </div >
        </div>
        <div class="col-md-4">
            <div class="form-group" >
                <label for="hor_up">Горизонтальный верхний</label>
                <select  class="form-control" id="hor_up" name="hor_up">
                    <?=$selected['hor_up']?>
                </select>
            </div >
        </div>
        <div class="col-md-4">
            <div class="form-group" >
                <label for="hor_down">Горизонтальный нижний</label>
                <select  class="form-control" id="hor_down" name="hor_down">
                    <?=$selected['hor_down']?>
                </select>
            </div >
        </div>
        <div class="col-md-4">
            <div class="form-group" >
                <label for="napr_up">Направляющий верхний</label>
                <select  class="form-control" id="napr_up" name="napr_up">
                    <?=$selected['napr_up']?>
                </select>
            </div >
        </div>
        <div class="col-md-4">
            <div class="form-group" >
                <label for="napr_down">Направляющий нижний</label>
                <select  class="form-control" id="napr_down" name="napr_down">
                    <?=$selected['napr_down']?>
                </select>
            </div >
        </div>
        <div class="col-md-4">
            <div class="form-group" >
                <label for="connection">Соединительный</label>
                <select  class="form-control" id="connection" name="connection">
                    <?=$selected['connection']?>
                </select>
            </div >
        </div>
    </div>
    <div class="col-12 messages">
    </div>
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-success" disabled value="save" id="saveDecors">Сохранить</button>
    </div>
<?php include_once('footer.php'); ?>