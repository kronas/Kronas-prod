<?php

date_default_timezone_set( 'Europe/Kiev' );

include_once(__DIR__.'/../../_1/config.php');
include_once('../functions2.php');
include_once('functions3.php');

if(isset($_POST)) {
       if(!empty($_POST['data'])) {
           $_out= array('result'=>false,'message'=>'ошибка pdata');
           $in=json_decode($_POST['data']);
           if(isset($in->newprofile) && isset($in->newprofilewcr)) {
               if(db_get_exist_my1cnom(array($in->hor_up, $in->hor_down, $in->vert, $in->napr_up,$in->napr_down,$in->connection))) {

                   if (db_profile_exist($in->newprofile, $in->newprofilewcr) == 0 && $in->profile_id == 0) {
                       $profileID = db_profile_add($in);
                       db_profile_article_add($profileID, $in);
                       $opt_list = db_profile_list_options();
                       $_out = array('result' => true, 'message' => 'профиль добавлен!', 'opt_list' => $opt_list);
                   } elseif ($in->profile_id > 0) {
                       db_profile_edit($in);
                       db_profile_article_edit($in->profile_id, $in);
                       $profileID = $in->profile_id;
                       $opt_list = db_profile_list_options();
                       $_out = array('result' => true, 'message' => 'профиль обновлен!', 'opt_list' => $opt_list);
                   }
               }
               else
               {
                   $_out= array('result'=>false,'message'=>'Введен не верный код профиля!');
               }
               $data = json_encode($_out);
               print $data;
               exit();
           }
           if(isset($in->decor_name) && isset($in->decor_color)) {
               if($in->decor_id==0) {
                   $decor_id=db_decor_add($in->decor_name, $in->decor_color);
               }else{
                   $decor_id=db_decor_update($in->decor_name, $in->decor_color, $in->decor_id);
               }
               $decor_profile=array();
               if($in->dp_vert_id==0)
                   $decor_profile[]=db_decor_profile_add($in->vert, $in->profile_art_id, 'vert', $decor_id, $in->profile_id);
               else
               {
                   $decor_profile[]=$in->dp_vert_id;
                   db_decor_profile_update($in->vert, $in->profile_art_id, $decor_id, $in->profile_id,$in->dp_vert_id);
               }
               if($in->dp_hor_up_id==0)
                   $decor_profile[]=db_decor_profile_add($in->hor_up, $in->profile_art_id, 'hor_up', $decor_id, $in->profile_id);
               else
               {
                   $decor_profile[]=$in->dp_vert_id;
                   db_decor_profile_update($in->hor_up, $in->profile_art_id, $decor_id, $in->profile_id,$in->dp_hor_up_id);
               }
               if($in->dp_hor_down_id==0)
                   $decor_profile[]=db_decor_profile_add($in->hor_down, $in->profile_art_id, 'hor_down', $decor_id, $in->profile_id);
               else
               {
                   $decor_profile[]=$in->dp_vert_id;
                   db_decor_profile_update($in->hor_down, $in->profile_art_id, $decor_id, $in->profile_id,$in->dp_hor_down_id);
               }
               if($in->dp_napr_up_id==0)
                   $decor_profile[]=db_decor_profile_add($in->napr_up, $in->profile_art_id, 'napr_up', $decor_id, $in->profile_id);
               else
               {
                   $decor_profile[]=$in->dp_vert_id;
                   db_decor_profile_update($in->napr_up, $in->profile_art_id, $decor_id, $in->profile_id,$in->dp_napr_up_id);
               }
               if($in->dp_napr_down_id==0)
                   $decor_profile[]=db_decor_profile_add($in->napr_down, $in->profile_art_id, 'napr_down', $decor_id, $in->profile_id);
               else
               {
                   $decor_profile[]=$in->dp_vert_id;
                   db_decor_profile_update($in->napr_down, $in->profile_art_id, $decor_id, $in->profile_id,$in->dp_napr_down_id);
               }
               if($in->dp_connection_id==0)
                   $decor_profile[]=db_decor_profile_add($in->connection, $in->profile_art_id, 'connection', $decor_id, $in->profile_id);
               else
               {
                   $decor_profile[]=$in->dp_vert_id;
                   db_decor_profile_update($in->connection, $in->profile_art_id, $decor_id, $in->profile_id,$in->dp_connection_id);
               }

               if($in->decor_id>0 && $decor_id!=$in->decor_id) {
                   $decor_conut = db_get_empty_decor($in->decor_id, $in->profile_id, $decor_profile);
                   if($decor_conut==0) db_decor_erase($in->decor_id);
               }
               $_out = array('result' => true, 'decors' => '');
  //             $_out['decors']=str_replace('multiple','',$_out['decors']);
               $_tmp=db_get_profile_decors($in->profile_id);
               create_profile_decors($_tmp, $_out['decors']);

               $data = json_encode($_out);
               print $data;
               exit();
           }
     }
       if(isset($_POST['erase_proflie']) && (int)$_POST['erase_proflie']>0) {

           if(db_profile_link_count((int)$_POST['erase_proflie'])==0) {
               db_profile_article_erase((int)$_POST['erase_proflie']);
               db_profile_erase((int)$_POST['erase_proflie']);
               $opt_list = db_profile_list_options();

               $_out= array('result'=>true,'message'=>'Профиль удален !' ,'opt_list'=>$opt_list);
           }else {

               $_out = array('result' => false, 'message' => 'Профиль имеет связи !');
           }
               $data = json_encode($_out);
               print $data;
               exit();

        }
       /*
       if(isset($_POST['searchProfile'])) {

         $_out = db_get_list4searchProfile($_POST['searchProfile'],$_POST['objectName']);
       //        $_out='<li>dsd</li><li>dsd</li>';
                  print $_out;
               exit();

        }
       */
       if(isset($_POST['searchSimple'])) {

         $_out = db_get_list4searchSimple($_POST['searchSimple'],$_POST['objectName']);
                  print $_out;
               exit();

        }
       if(isset($_POST['profileDetails'])) {
           $_out=array( 'decors' =>'','dovodchiks'=>'','rolick'=>'','stopor'=>'');
           $_tmp=db_decor_list_options_on_profile($_POST['profileDetails']);
           create_profile_decors($_tmp, $_out['decors']);

           $_tmp=db_get_profile_dovodchik($_POST['profileDetails']);
           create_profile_dovodchik($_tmp, $_out['dovodchiks']);

           $_tmp=db_get_profile_rolick($_POST['profileDetails']);
           create_profile_rolick($_tmp, $_out['rolick']);

           $_tmp=db_get_profile_stopor($_POST['profileDetails']);
           create_profile_stopor($_tmp,$_out['stopor']);


           $data = json_encode($_out);
           print $data;
               exit();

        }
       if(isset($_POST['profileIDforSchetka']) && isset($_POST['selectedDecor'])) {
           $_out=array( 'schetka' =>'');
           $_tmp=db_get_profile_schetka($_POST['profileIDforSchetka'], $_POST['selectedDecor']);
           create_profile_schetka($_tmp, $_out['schetka']);
           $data = json_encode($_out);
           print $data;
               exit();

        }
       if(isset($_POST['glueProfileID']) && isset($_POST['glueDecor'])) {
           db_schetka_change_glue($_POST['glueProfileID'], $_POST['glueDecor'],$_POST['glueSchetka'], $_POST['op']);
           $_out=array( 'schetka' =>'');
           $_tmp=db_get_profile_schetka($_POST['glueProfileID'], $_POST['glueDecor']);
           create_profile_schetka($_tmp, $_out['schetka']);
           $data = json_encode($_out);
           print $data;
               exit();

        }
       /*
    if(isset($_POST['newDecor'])) {
        db_decor_add(trim($_POST['newDecor']), trim($_POST['newDecorColor']));
       $opt_list=db_decor_list_options();
        $_out= array('result'=>true,'message'=>'Декор добавлен!' ,'opt_list'=>$opt_list);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    */
    if(isset($_POST['eraseDecor'])) {
        db_decor_erase((int)$_POST['eraseDecor']);
        $_tmp=db_decor_list_options_on_profile($_POST['onProfile']);
        create_profile_decors($_tmp, $opt_list);

        $_out= array('result'=>true,'message'=>'Декор удален!' ,'opt_list'=>$opt_list);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['link_decor']) && !isset($_POST['profile'])) {
        $dpllc=db_decor_link_count((int) $_POST['link_decor']);
        if($dpllc>0)
        {
            $_out= array('result'=>false);
        }
        else{
            $_out= array('result'=>true);
        }
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['link_decor']) && isset($_POST['profile'])) {
        $dpllc=db_decor_profile_link_count((int) $_POST['link_decor'], (int) $_POST['profile']);
           if($dpllc>0)
        {
            $_out= array('result'=>false,'message'=>'Декор уже связан!');
        }
        else{
            $_decors='';
            db_link_profile_decor((int) $_POST['link_decor'], (int) $_POST['profile']);
            $_tmp=db_get_profile_decors($_POST['profile']);
            create_profile_decors($_tmp, $_decors);
            $_out= array('result'=>true,'message'=>'Декор связан!', 'decors'=>$_decors );
        }
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['remove_decor']) && isset($_POST['profile'])) {

        if(db_decor_ext_link_count((int) $_POST['remove_decor'])>0)
        {
            $_out= array('result'=>false,'message'=>'Декор имеет связь! Удалить нельзя!');
        }else {
            $_decors='';
            db_unlink_decor((int) $_POST['profile'], (int) $_POST['remove_decor']);
            $_tmp=db_get_profile_decors($_POST['profile']);
            create_profile_decors($_tmp, $_decors);
            $_out= array('result'=>true,'message'=>'Декор отвязан!', 'decors'=>$_decors);
        }
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['link_schetka']) && isset($_POST['decor']) && isset($_POST['profile'])) {
           db_link_profile_schetka((int) $_POST['profile'],(int) $_POST['decor'], (int) $_POST['link_schetka'],(int) $_POST['glue']);

        $schetka='';
        $_tmp=db_get_profile_schetka((int) $_POST['profile'], (int) $_POST['decor']);
        create_profile_schetka($_tmp, $schetka);
        $_out= array('result'=>true,'message'=>'Щетку привязяли!='.(int) $_POST['glue'], 'schetka'=>$schetka);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['unlink_schetka']) && isset($_POST['decor']) && isset($_POST['profile'])) {
        db_unlink_profile_schetka((int) $_POST['profile'],(int) $_POST['decor'], (int) $_POST['unlink_schetka']);
        $schetka='';
        $_tmp=db_get_profile_schetka((int) $_POST['profile'], (int) $_POST['decor']);
        create_profile_schetka($_tmp, $schetka);
        $_out= array('result'=>true,'message'=>'Щетку отвязяли!', 'schetka'=>$schetka);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['link_dovodchik']) && isset($_POST['profile'])) {
        db_link_profile_dovodchik( (int) $_POST['profile'],(int) $_POST['link_dovodchik']);
        $dovodchik='';
        $_tmp=db_get_profile_dovodchik((int) $_POST['profile']);
        create_profile_dovodchik($_tmp, $dovodchik);

        $_out= array('result'=>true,'message'=>'Доводчик привязяли!', 'dovodchik'=>$dovodchik);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['unlink_dovodchik']) && isset($_POST['profile'])) {

        db_unlink_profile_dovodchik((int) $_POST['profile'],(int) $_POST['unlink_dovodchik']);
        $dovodchik='';
        $_tmp=db_get_profile_dovodchik((int) $_POST['profile']);
        create_profile_dovodchik($_tmp, $dovodchik);


        $_out= array('result'=>true,'message'=>'Доводчик отвязяли!', 'dovodchik'=>$dovodchik);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['link_rolick']) && isset($_POST['profile'])) {
        db_link_profile_rolick( (int) $_POST['profile'],(int) $_POST['link_rolick']);
        $rolick='';
        $_tmp=db_get_profile_rolick((int) $_POST['profile']);
        create_profile_rolick($_tmp, $rolick);


        $_out= array('result'=>true,'message'=>'Ролик привязяли!', 'rolick'=>$rolick);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['unlink_rolick']) && isset($_POST['profile'])) {
        db_unlink_profile_rolick((int) $_POST['profile'],(int) $_POST['unlink_rolick']);
        $rolick='';
        $_tmp=db_get_profile_rolick((int) $_POST['profile']);
        create_profile_rolick($_tmp, $rolick);


        $_out= array('result'=>true,'message'=>'Ролик отвязяли!', 'rolick'=>$rolick);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['link_stopor']) && isset($_POST['profile'])) {
        db_link_profile_stopor( (int) $_POST['profile'],(int) $_POST['link_stopor']);
        $stopor='';
        $_tmp=db_get_profile_stopor((int) $_POST['profile']);
        create_profile_stopor($_tmp, $stopor);


        $_out= array('result'=>true,'message'=>'Стопор привязяли!', 'stopor'=>$stopor);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['unlink_stopor']) && isset($_POST['profile'])) {
        db_unlink_profile_stopor((int) $_POST['profile'],(int) $_POST['unlink_stopor']);
        $stopor='';
        $_tmp=db_get_profile_stopor((int) $_POST['profile']);
        create_profile_stopor($_tmp, $stopor);


        $_out= array('result'=>true,'message'=>'Стопор отвязяли!', 'stopor'=>$stopor);
        $data = json_encode($_out);
        print $data;
        exit();
    }
    $data = json_encode($_out);
    print $data;
}