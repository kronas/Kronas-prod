<?php
session_start();
include_once('../../functions2.php');
include_once('../functions3.php');
include_once('funct_clients.php');
$theCHLD=1;
$title='Создание двери';
$toolbarclose=1;
include_once('../header.php');

if(isset($_POST['doorData'])) $doorData= json_decode($_POST['doorData']);
else $doorData=null;
if(isset($_POST['materials'])) $materials= (array) json_decode($_POST['materials']);
else $materials=array();
if(isset($_POST['doorYsize'])) $doorYsize= json_decode($_POST['doorYsize']);
else $doorYsize=null;

$countElement=1; if(count((array)$doorData)>0) $countElement=count((array)$doorData);
?>
<div class="sectionDoor" data-countElement="<?=$countElement?>">
    <input type="hidden" id="door_size_y" name="door_size_y" value="<?=$doorYsize?>">
    <?php
    /*
    $_materials=array();
foreach ($materials as $_k=>$_v)
{
    $_materials[$_k]=$_v->name;
}
       echo doors_section_crea($doorData,$_materials,$doorYsize);
*/
    echo doors_section_crea($doorData,$materials,$doorYsize);
    ?>
</div>
 <div class="row">
    <div class="col-12 messages-result">

    </div>
    <div class="col-12 text-right">
        <button type="button" class="btn btn-primary" id="addSection">Добавить секцию</button>
        <button type="button" class="btn btn-success" id="saveDoor">Сохранить</button>
    </div>
</div>



<?php include_once('../footer.php'); ?>