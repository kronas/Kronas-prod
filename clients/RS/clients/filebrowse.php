<?php
session_start();
include_once(__DIR__.'/../../../_1/config.php');
include_once('../../functions2.php');
include_once('../functions3.php');
include_once('funct_clients.php');
$theCHLD=3;
$title='Выбрать изображение';
$toolbarclose=1;
include_once('../header.php');

$setting=load_ini();
$base_dir    ='../../../pic_rs';
$custom_dir_add = 'pic_custom';
$custom_dir    = __DIR__ . 'filebrowse.php/' .$custom_dir_add;
$curentDir='';

if(isset($_POST['numDoor'])) $numDoor= $_POST['numDoor'];
if(isset($_POST['numSec'])) $numSec= $_POST['numSec'];
if(isset($_POST['sectop'])) $sectop= $_POST['sectop'];
if($sectop=='G') $base_dir='../../../'.$setting['misc']['dirPic2print'];
if($sectop=='M') $base_dir='../../../'.$setting['misc']['dirPic2sanArtEm'];
?>
<div class="row browse-display">
    <?=$message?>



    <div class="col-md-12 base-browse">
        <?php
        echo file_browses($base_dir,$curentDir,$numDoor,$numSec);
        ?>
    </div>

    <div class="col-md-12 file-upload">

        <form class="form-inline" action="" method="POST" enctype="multipart/form-data">
            <div class="form-group mb-2 col-2">
                <label for="filePersonal">Собственный файл</label>
            </div>
            <div class="form-group mb-2 col-4">
                <input type="file" class="form-control-file" id="filePersonal" name="filePersonal">
            </div>
            <div class="form-group mb-2 col-2">
                <input type="button" class="btn btn-primary mb-2" id="uploadFile" value="Загрузить">
            </div>

            <div class="form-group mb-2 col-4 d-none picturesSelect">
                <img src="" class="rounded m-2 picSelImage" height="100px">
                <input type="hidden" name="picSelVal" value="">
                <input type="hidden" name="picnumDoor" value="<?=$numDoor?>">
                <input type="hidden" name="picnumSec" value="<?=$numSec?>">
                <button type="button" class="btn btn-success mb-2" id="picSave" name="picSave">Сохранить</button>
            </div>
            <div class="col-12 file-message"></div>
        </form>
    </div>
</div>


<?php include_once('../footer.php'); ?>