<?php

date_default_timezone_set( 'Europe/Kiev' );

include_once(__DIR__.'/../../../_1/config.php');
include_once('../../functions2.php');
include_once('../functions3.php');
include_once('funct_clients.php');
// print_r($_POST);
if(isset($_POST)) {
    $_out= array('result'=>false,'message'=>'ошибка pdata2');
    if(isset($_FILES) && isset($_FILES['file']) && isset($_FILES['file']['name'])) {
        $custom_dir_add = 'pic_custom';
        $custom_dir    = __DIR__  .'/'.$custom_dir_add;
        /*
            $error = false;
            $files = array();

            $uploaddir = $custom_dir.'/';
            foreach ($_FILES as $file) {
                if (move_uploaded_file($file['tmp_name'], $uploaddir . basename( $file['name']))) {
                    $files[] = $uploaddir . $file['name'];
                } else {
                    $error = true;
                }
            }
*/

        $errors= array();
        $file_name = $_FILES['file']['name'];
        $file_size =$_FILES['file']['size'];
        $file_tmp =$_FILES['file']['tmp_name'];
        $file_type=$_FILES['file']['type'];
        $file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));

        $extensions= array("jpeg","jpg","png");

        if(in_array($file_ext,$extensions)=== false){
            $errors[]="Доступные расширения для загрузки: JPG, JPEG, PNG, GIF";
        }

        if($file_size >  4194304){
            $errors[]='Размер файла должен быть до 4Mb';
        }

        if(empty($errors)==true){
            $file_status=TRUE;
            $file_name=time().'_'.time().'.'.$file_ext;
            move_uploaded_file($file_tmp,$custom_dir."/".$file_name);
            // creaDoors
            $_out= array('result'=>true,'message'=>'Файл загружен!'.$custom_dir,'file'=>$custom_dir_add."/".$file_name);
        }else{
            $_out= array('result'=>false,'message'=>implode('<br>',$errors));
        }
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_FILES) && isset($_FILES['file-rs']) && isset($_FILES['file-rs']['name'])) {
        if(isset($_FILES))
        {
            $_out['qqq']=serialize($_FILES);
        }
        $data = json_encode($_out);
        print $data;
        exit();
        $custom_dir_add = 'pic_custom';
        $custom_dir    = __DIR__  .'/'.$custom_dir_add;
        $errors= array();
        $file_name = $_FILES['file-rs']['name'];
        $file_size =$_FILES['file-rs']['size'];
        $file_tmp =$_FILES['file-rs']['tmp_name'];
        $file_type=$_FILES['file-rs']['type'];
        $file_ext=strtolower(end(explode('.',$_FILES['file-rs']['name'])));


        if($file_ext=='RS'){
            $errors[]="Доступные расширения для загрузки: RS";
        }


        if(empty($errors)==true){
            $file_status=TRUE;
            $file_name=time().'_'.time().'.'.$file_ext;
            move_uploaded_file($file_tmp,$custom_dir."/".$file_name);
            // creaDoors
            $_out= array('result'=>true,'message'=>'Файл загружен!'.$custom_dir,'file-rs'=>$custom_dir_add."/".$file_name);
        }else{
            $_out= array('result'=>false,'message'=>implode('<br>',$errors));
        }
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(!empty($_POST['data'])) {
        $_out= array('result'=>false,'message'=>'ошибка pdata3');
        $in=json_decode($_POST['data']);
        if(isset($in->sections)) {
            $_out= array('result'=>true,'message'=>'Готово');
            $_arr=array();
            foreach ($in->sections as $key=>$value)
            {
                $_arr[]=array('filler'=>$value->filler,'height'=>$value->height,'id'=>$value->index);
            }
            $_out['data']=doors_inner_section_crea($_arr,$in->numDoor,$in->doorHeight,array(),$in->MaxSanH);

            $data = json_encode($_out);
            print $data;
            exit();
        }

    }
        if(isset($_POST['createArrayDoors'])) {
            $_out=array( 'doors' =>'');
            for($i=1;$i<=$_POST['createArrayDoors'];$i++) {
                $_out['doors'] .= doors_crea($i,(int)$_POST['createArrayDoors']);
            }
            $data = json_encode($_out);
            print $data;
            exit();
        }
       if(isset($_POST['newsection'])) {
           $in=json_decode($_POST['newsection']);
           $_out=doors_section_crea(null, (array)$in->materials);
           $data = json_encode($_out);
           print $data;
               exit();
        }
       if(isset($_POST['createFullMaterials'])) {
           $in=json_decode($_POST['createFullMaterials']);
           $_out= array('result'=>true,'message'=>'good');
           $_out['data']=createFullMaterials((array)$in->materials);

           $data = json_encode($_out);
           print $data;
               exit();
        }
       if(isset($_POST['RSprofile'])) {
           $_out=array( 'decors' =>'');
           $_tmp=db_get_profile_decors($_POST['RSprofile']);
           create_profile_decors($_tmp, $_out['decors']);
           $_out['decors']=str_replace('multiple','',$_out['decors']);
           $data = json_encode($_out);
           print $data;
               exit();
        }
        if(isset($_POST['keywordOnText'])) {
           if (stripos($_POST['keywordOnText'], 'select') === false && stripos($_POST['keywordOnText'], 'insert') === false && stripos($_POST['keywordOnText'], 'delete') === false && stripos($_POST['keywordOnText'], 'update') === false && stripos($_POST['keywordOnText'], 'union all') === false) {
             $data = db_get_list4searchOnTextFilling($_POST['keywordOnText']);
           }
           print $data;
           exit();
        }
       if(isset($_POST['RSprofile2']) && isset($_POST['RSdecor'])) {
           $_out=array( 'schetka' =>'','dovodchiks'=>'','rolick'=>'','stopor'=>'','decor_color'=>'');
           $profile=db_get_profile_full($_POST['RSprofile2']);
           if($profile['min_door_height']<1 || $profile['max_door_height']<1 ||
               $profile['min_door_width']<1 || $profile['max_door_width']<1 ||
               $profile['min_door_height']>$profile['max_door_height'] ||
               $profile['min_door_width']>$profile['max_door_width']
           )
           {
               $_out['result']=false;
               $_out['message']='Для данного профиля не установлены параметры, или установлены не верно!<br>Обратитесь к администратору!';
               $data = json_encode($_out);
               print $data;
               exit();
           }
           elseif($profile['min_door_height']>$_POST['RSsizeY'] || $profile['max_door_height']<$_POST['RSsizeY'] ||
               $profile['min_door_width']>$_POST['RSsizeX'] || $profile['max_door_length']<$_POST['RSsizeX'])
           {
               $_out['result']=false;
               $_out['message']='Для данного профиля не верно установлены параметры!<br>
                                Высота от '.$profile['min_door_height'].'мм до '.$profile['max_door_height'].'мм , ширина от'.$profile['min_door_width'].'мм до '.$profile['max_door_length'].'мм.';
               $data = json_encode($_out);
               print $data;
               exit();
           }
           $_out['result']=true;
           $_tmp=db_get_profile_schetka($_POST['RSprofile2'], $_POST['RSdecor']);
           create_profile_schetka($_tmp, $_out['schetka']);

           $_tmp=db_get_profile_dovodchik($_POST['RSprofile2']);
           create_profile_dovodchik($_tmp, $_out['dovodchiks'],0,TRUE);

           $_tmp=db_get_profile_rolick($_POST['RSprofile2']);
           create_profile_rolick($_tmp, $_out['rolick']);

           $_tmp=db_get_profile_stopor($_POST['RSprofile2']);
           create_profile_stopor($_tmp,$_out['stopor'],-1,TRUE);

           $_out['schetka']=str_replace('multiple','',$_out['schetka']);
           $_out['dovodchiks']=str_replace('multiple','',$_out['dovodchiks']);
           $_out['rolick']=str_replace('multiple','',$_out['rolick']);
           $_out['stopor']=str_replace('multiple','',$_out['stopor']);
           $_out['decor_color']=db_get_decor_color($_POST['RSdecor']);
           $_out['profile_type']=db_get_profile_type($_POST['RSprofile2']);

           $data = json_encode($_out);
           print $data;
               exit();
        }

    if(isset($_POST['fbtarget']) && isset($_POST['fbbase'])) {

        $_out= array('result'=>true,'message'=>$_POST['fbbase'].$_POST['fbtarget'],
        'data'=>file_browses($_POST['fbbase'], $_POST['fbtarget'], $_POST['numDoor'], $_POST['numSec']));
        $data = json_encode($_out);
        print $data;
        exit();
    }
    $data = json_encode($_out);
    print $data;
}