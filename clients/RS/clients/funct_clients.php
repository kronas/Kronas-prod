<?php

function doors_crea($num,$count_doors, $inner_section_prot='',$inner_section='')
{
    $tmp_add_data='';
    if(strlen($inner_section_prot)>0)
    {
        $tmp_add_data='<textarea class="d-none" id="door-indata-'.$num.'">'.$inner_section_prot.'</textarea>';
    }
    $copy_door='<select class="form-control door-clone" id="door-clone-'.$num.'" data-num="'.$num.'"><option value="0">Скопировать из ...</option>';
    for ($i=1;$i<=$count_doors;$i++)
    {
        if($num!=$i)
        {
            $copy_door.='<option value="'.$i.'">Скопировать из №'.$i.'</option>';

        }
    }
    $copy_door.='</select>';
    // <input type="number" step="1" name="width_door_'.$num.'" value="">
    return '<div class="shadow-lg mb-4 row rs-doors door-'.$num.'">
                        <h6 class="container-fluid">Дверь №'.$num.'</h6>
                        
                        <div class="status-door container-fluid m-1"></div>
                        <div class="rs-doors">
                        <div class="rs-doors-sections">
                        '.$tmp_add_data.'
                        <input type="hidden" id="doors_data_'.$num.'" name="doors_data_'.$num.'" value="">
                        
                        <button type="button" class="btn btn-sm btn-secondary m-3" data-toggle="modal"
                                data-target="#creaDoors" data-valdoor=""
                                data-numdoor="'.$num.'">Собрать дверь
                        </button>
                        <br>'.$copy_door.'
                        </div>
                                <div class="rs-doors inners-sections">
                                '.$inner_section.'
                                </div>
                        </div>
                    </div>';
}

function doors_inner_section_crea($in, $num,$doorHeight, $default=array(), $MaxSanH)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'doors_inner_section_crea start', $_SESSION);
$in2=$in;
$in=array();
    for ($i=count($in2)-1;$i>=0;$i--)
{
    $in[]=$in2[$i];
}
    $pos=0;
    for ($i=0;$i<count($in);$i++)
    {
        $in[$i]['position']=(int) $pos;
        $in[$i]['connector']=$i!=0?1:0;
        $pos=$pos+((100/$doorHeight)*$in[$i]['height']);
    }
//    _pre($in);
$out='';
    $arr_sql=array();
    foreach ($in as $value)
    {
        $arr_sql[]=$value['filler'];
    }
    $arr_data=array();
    $sql = "SELECT DISTINCT m.MATERIAL_ID, m.L, m.W, m.T, m.`NAME`, m.`CODE`, m.RS_PLATE, m.RS_MIRROR, m.RS_GLASS, 	mp.FILE_NAME_BIG, mp.FILE_NAME_SMALL   
FROM MATERIAL AS m
	LEFT JOIN MATERIAL_PIC AS mp ON m.MATERIAL_ID = mp.MATERIAL_ID  WHERE 	m.`CODE` IN (".implode(', ',$arr_sql).")";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $arr_data[$line['CODE']]=$line;
    }
    $select_lakobel='<option value="none" selected="">Выбрать</option>';
    $sql = "SELECT MATERIAL.CODE, MATERIAL.`NAME` FROM GOOD_TREE INNER JOIN GOOD_TREE_MATERIAL_CONN ON GOOD_TREE.FOLDER_ID = GOOD_TREE_MATERIAL_CONN.FOLDER_ID INNER JOIN MATERIAL ON 
            GOOD_TREE_MATERIAL_CONN.MATERIAL_ID = MATERIAL.MATERIAL_ID WHERE GOOD_TREE.FOLDER_ID IN (2702,2703)";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $select_lakobel.='<option value="'.$line['CODE'].'">'.$line['NAME'].'</option>';
    }
    $select_oracal='<option value="none" selected="">Выбрать</option>';
    $sql = "SELECT	MATERIAL.`NAME`, MATERIAL.`CODE`FROM MATERIAL WHERE MATERIAL.MY_1C_NOM =7210 ORDER BY MATERIAL.`NAME`";
    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    foreach ($result as $line)
    {
        $select_oracal.='<option value="'.$line['CODE'].'">'.$line['NAME'].'</option>';
    }
    $in2=$in;
    $in=array();
    for ($i=count($in2)-1;$i>=0;$i--)
    {
        $in[]=$in2[$i];
    }
    foreach ($in as $key=>$value)
    {
        $type='Плита'; $type2='P';$select_type='';
        $newFillImage="./res/img/slab_mat.jpg";
        if($arr_data[$value['filler']]['RS_MIRROR']==1) {
            $type='Зеркало'; $type2='M';$newFillImage="./res/img/mirror.jpg";
            $selProcess=array('','','','','','','');
            $btnHidden='d-none';
            $imageSelected=array('','');
            if(count($default)>0 && count($default[$key])>0  && $default[$key]['op']==$type2)
            {
                if(isset($default[$key]['selected']))
                {
                    $_selected=(int) $default[$key]['selected'];
                    $btnHidden='';
                }
                if(isset($default[$key]['image']) && strlen($default[$key]['image'])>0)
                {
                    $imageSelected=array($default[$key]['image'],
                        '<img src="'.$default[$key]['image'].'" class="rounded m-2" height="100px">');
                }
            }
            $selProcess[$_selected]='selected=""';
            $select_type='<select class="custom-select m-1" name="sectionprocessing[]" data-op="M">
                        <option value="none" '.$selProcess[0].'>Без обработки</option>
                        <option value="1" '.$selProcess[1].'>Пескоструй</option>
                        <option value="2" '.$selProcess[2].'>Снятие амальгамы</option>
                        <option value="3" '.$selProcess[3].'>Снятие амальгамы с прозрачной плёнкой</option>
                        <option value="6" '.$selProcess[6].'>Плёнка Frost</option>
                        </select>
                    <button type="button" class="btn btn-primary '.$btnHidden.'" data-toggle="modal" data-valdoor="" data-op="M" data-numdoor="'.$num.'"  data-numsec="'.$key.'" id="addImage"
                            data-target="#creaDoors">Выбрать
                    </button>
                    <input type="hidden" name="selectedImage[]" value="'.$imageSelected[0].'">
                    <div class="image float-right">'.$imageSelected[1].'</div>
                        ';
            if($value['height']>=$MaxSanH) $select_type='<select class="custom-select m-1 d-none" name="sectionprocessing[]" data-op="M">
                        <option value="none" selected>Без обработки</option>
                        </select>
                    <input type="hidden" name="selectedImage[]" value="">
                    <div class="image float-right"></div>
                        ';
        }
        if($arr_data[$value['filler']]['RS_GLASS']==1)  {
            $type='Стекло'; $type2='G';$newFillImage="./res/img/glass.jpg";
            $selProcess=array('','');
            $btnHidden='d-none';
            $lakobelHidden='d-none';
            $oracalHidden='d-none';
            $imageSelected=array('','','','','');
            if(count($default)>0 && count($default[$key])>0  && $default[$key]['op']==$type2)
            {
                if(isset($default[$key]['selected']))
                {
                    $_selected=(int) $default[$key]['selected'];
                    if($default[$key]['selected']==1 || $default[$key]['selected']==2) $btnHidden='';
                    if($default[$key]['selected']==3)
                    {
                        $select_lakobel=str_replace('"'.$default[$key]['lakobel'].'"','"'.$default[$key]['lakobel'].'" selected="" ', $select_lakobel);
                        $lakobelHidden='';
                    }
                     if($default[$key]['selected']==4)
                    {
                        $select_oracal=str_replace('"'.$default[$key]['oracal'].'"','"'.$default[$key]['oracal'].'" selected="" ', $select_oracal);
                        $oracalHidden='';
                    }
                }
                if(isset($default[$key]['image']) && strlen($default[$key]['image'])>0)
                {
                    $imageSelected=array($default[$key]['image'],
                        '<img src="'.$default[$key]['image'].'" class="rounded m-2" height="100px">');
                }
            }
            $selProcess[$_selected]='selected=""';
            $select_type='<select class="custom-select m-1" name="sectionprocessing[]" data-op="G">
                        <option value="none" '.$selProcess[0].'>Без обработки</option>
                        <option value="7" '.$selProcess[7].'>Без обработки с прозрачной плёнкой</option>
                        <option value="1" '.$selProcess[1].'>Фото печать</option>
                        <option value="2" '.$selProcess[2].'>УФ печать</option>
                        <option value="5" '.$selProcess[5].'>УФ печать с заливкой белого фона</option>
                        <option value="4" '.$selProcess[4].'>Стекло – прозрачное + оракал сзади</option>
                        <option value="8" '.$selProcess[8].'>Стекло с пескоструем</option>
                        <option value="6" '.$selProcess[6].'>Плёнка Frost</option>
                        </select>
                    <button type="button" class="btn btn-primary '.$btnHidden.'" data-toggle="modal" data-valdoor="" data-op="G" data-numdoor="'.$num.'"  data-numsec="'.$key.'" id="addImage"
                            data-target="#creaDoors">Выбрать
                    </button>
                    <input type="hidden" name="selectedImage[]" value="'.$imageSelected[0].'">
                    <div class="image float-right">'.$imageSelected[1].'</div>
                    <select class="custom-select m-1 '.$lakobelHidden.'" id="sectionlakobel" name="sectionlakobel[]">
                        '.$select_lakobel.'
                        </select>    
                    <select class="custom-select m-1 '.$oracalHidden.'" id="sectionoracal" name="sectionoracal[]">
                        '.$select_oracal.'
                        </select>    
                        ';
        }

$_tmp_arr=array();
        $_tmp_arr['pic']=$newFillImage;
        $_tmp_arr['name']=$arr_data[$value['filler']]['NAME'];
        $_tmp_arr['height']=$value['height'];
        $_tmp_arr['position']=$value['position'];
        $_tmp_arr['connector']=$value['connector'];
        $_tmp_arr2=json_encode($_tmp_arr);
        $out.='<div class="rs-doors-sections doors-inner" data-numsect="'.$key.'" data-numdoor="'.$num.'">
<textarea class="d-none" id="door_sect_data-'.$num.'-'.$key.'">'.$_tmp_arr2.'</textarea>
<div class="text-center">'.$type.'</div>
<div class="text-center">'.$arr_data[$value['filler']]['NAME'].'</div>
<div class="text-center">Высота: '.$value['height'].' мм</div>';
        $out.=$select_type;
        $out.='</div>';
    }

    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'doors_inner_section_crea end', $_SESSION);
    return $out;
}
function doors_section_crea($doorData=null,$materials=array(), $defval='')
{
    if(is_object($doorData))
    {
        $out='';$index=0;
        foreach ($doorData as $key=>$value)
        {
            $out.= _doors_section_crea($value->height,$value->tst,$index,$materials,$value->filler);
            $index++;
        }
    }
    else
    {
        $out= _doors_section_crea($defval, '',0,$materials);
    }
    return $out;
}
function _doors_section_crea($height='',$tst='',$index=0, $filler=array(),$sel_filler=0)
{

    $out='<div class="row door-section" data-numElement="'.$index.'">
    <div class="col-2 pt-4">
        <input class="form-check-input" type="checkbox" value="" name="sect_fixheight[]">
        <label class="form-check-label" for="sect_fixheight[]">Фиксировать высоту</label>
    </div>
    <div class="col-3">
        <label for="sect_height[]">Высота секции</label>
        <input type="number" class="form-control" name="sect_height[]" value="'.$height.'">
    </div>
    ';

    if(count($filler)>0)
    {
        $out.='    <div class="col-3"><label for="sect_filler[]">Наполнитель</label>
    <select class="form-control" name="sect_filler[]" id="sect_fill">';
        foreach ($filler as $key=>$value)
        {
            $key_res=sql_data(__LINE__,__FILE__,__FUNCTION__,'SELECT * FROM MATERIAL WHERE CODE='.$key.';')['data'][0]['RS_PLATE'];
            $key_res == 1 ? 1 : 0;
            $selected=''; if($key==$sel_filler) $selected=' selected="selected"';
            $out.='<option data-type="'.$key_res.'" value="'.$key.'" '.$selected.'>'.$value.'</option>';
        }
        $out.='</select>

  </div>
  ';
  
  $out.= '
  <div class="col-md-3" class="txt_material_door">
        <label>Текстура горизонтально</label><br>
        <select name="sect_txt">
            <option value="no">не важно</option>
            <option value="gor">текстура горизонтально</option>
            <option value="vert">текстура вертикально</option>
        </select>
  </div>  ';
//   print_r($filler);
    }
    $out.='    

    </div>';
    return $out;
}


function file_browses($base_dir, $curentDir, $numDoor=0,$numSec=0)
{
    $listDir = '<ul class="list-group file-browses">';
    $listPict='<div class="row">';
    if (strlen($curentDir)>0) {
        $prevdir = substr($curentDir, 0, strripos($curentDir, '/'));
        $listDir .= '  <li class="list-group-item" data-fbbase="' . $base_dir . '" data-fbtarget="' . $prevdir . '" data-fbnumDoor="' . $numDoor . '" data-fbnumSec="' . $numSec . '">..</li>';
    }
    $in = scandir($base_dir . $curentDir);
    $out = '';
    $arr_ext = array('jpg', 'jpeg', 'bmp', 'gif');
    foreach ($in as $value) {
        if ($value != '.' && $value != '..') {
            if (is_dir($base_dir.$curentDir.'/'.$value)) {
                $listDir .= '  <li class="list-group-item" data-fbbase="' . $base_dir . '" data-fbtarget="' . $curentDir.'/'.$value . '" data-fbnumDoor="' . $numDoor . '" data-fbnumSec="' . $numSec . '">' . $value . '</li>';
            } else {
                $ext = pathinfo($in . '/' . $value, PATHINFO_EXTENSION);
                if (in_array($ext, $arr_ext)) {
                    $listPict.='<img height="100px" src="'.$base_dir. $curentDir.'/'.$value.'"  class="rounded m-2" data-fbnumDoor="' . $numDoor . '" data-fbnumSec="' . $numSec . '">';
                }
            }

        }
    }
    $listDir .= '</ul>';
    $listPict.='</div>';
    return $listDir.$listPict;
}