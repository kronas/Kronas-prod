<?php
session_start();
 include_once('../functions3.php');

$theCHLD=1;
$title='Создание конструкции';
include_once('../header.php');
?>


    <form method="post" id="creaRS">
    <div class="row">
        <div class="col-md-4"  id="forCanvas">

        </div>
        <div class="col-md-8">
            <div class="row">
                <h5>Размер конструкции и профиль</h5>
                <div class="col-md-12">
                    <div class="row">
                        
                        <div class="col">
                            <label for="rs_size_y">Высота</label>
                            <input type="text" class="form-control" id="rs_size_y" value="2100" >
                        </div>
                        <div class="col">
                            <label for="rs_size_x">Ширина</label>
                            <input type="text" class="form-control" id="rs_size_x" value="3500" >
                        </div>
                        <div class="col">
                            <label for="rs_profile">Профиль</label>
                            <select class="form-control" id="rs_profile">
                                <option value="none">Выбрать...</option>
                                <option value="1" selected="">Profles1</option>
                            </select>
                        </div>
                        <div class="col">
                            <label for="tstcolor">(test) Цвет</label>
                            <input type="text" class="form-control" id="tstcolor" value="#d1e1fe">
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-success" id="testset1">Далее</button>
                        </div>
                            <div class="col-12 messages-set1">

                            </div>

                    </div>
                </div>
            </div>

            <div class="row complectives">

                <h5>Комплектация профиля</h5>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col">
                            <label>Щетка с зажимом</label>
                            <div class="forprofileSchetka"><select class="form-control" id="profileSchetka" name="profileSchetka"><option value="33852">(23850) MultiTech метабокс 150/350 белый (0.050.055)</option></select><div class="profileSchetkaMess"></div></div>
                        </div>
                        <div class="col">
                            <label>Доводчик</label>
                            <div class="forprofileDovodchik"><select class="form-control" id="profileDovodchik" name="profileDovodchik"><option value="33852">(23850) MultiTech метабокс 150/350 белый (0.050.055)</option></select><div class="profileDovodchikMess"></div></div>
                        </div>
                        <div class="col">
                            <label>Ролик</label>
                            <div class="forprofileRolick"><select class="form-control" id="profileRolick" name="profileRolick"><option value="35103">(34193) 9.104.107 Угловой соединитель лицевой панели, 70 мм серый</option></select><div class="profileRolickMess"></div></div>
                        </div>
                        <div class="col">
                            <label>Стопор</label>
                            <div class="forprofileStopor"><select class="form-control" id="profileStopor" name="profileStopor"><option value="33879">(10877) [/РАСПРОДАЖА/] Боковина ящика Hettich InnoTech 350*70 левая серебро (1.062.015)</option></select><div class="profileStoporkMess"></div></div>
                        </div>

                        <div class="col">
                            <label for="rs_count_doors">Количество дверей</label>
                            <input type="text" class="form-control" id="rs_count_doors" value="4">
                            добавить контроль на {N:1,K:2} , {N:3,K:4}, {N:2,K:4}
                        </div>
                        <div class="col">
                            <label for="rs_overlap">Перехлестов</label>
                            <input type="text" class="form-control" id="rs_overlap" value="2">
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-success" id="testset2">Далее</button>
                        </div>
                        <div class="col-12 messages-set2">

                        </div>

                    </div>
                </div>

            </div>


            <div class="row filling">
                <div class="col-12">
                    <h5>Заполнение</h5></div>
                <div class="col-8">
                    <div class="form-group">

                        <textarea class="d-none" id="datalistMaterials">{"20550":"Стекло 10 мм прозрачное","57617":"OSB/3 2500*1250*10","71065":"Стекло 10 мм сверхпрозрачное","88920":"ЛДСП 2800*2070*18 (18,4) FA90 Sprigato/Matrix - (CLEAF) (-)"}</textarea>
                        <textarea class="d-none" id="datalistMaterialsFull"></textarea>
                        <label for="listMaterials">Перечень материала для наполнения</label>
                        <select multiple="" class="form-control" id="listMaterials"><option value="20550">Стекло 10 мм прозрачное</option><option value="57617">OSB/3 2500*1250*10</option><option value="71065">Стекло 10 мм сверхпрозрачное</option><option value="88920">ЛДСП 2800*2070*18 (18,4) FA90 Sprigato/Matrix - (CLEAF) (-)</option></select>
                    </div>
                </div>
                <div class="col-4 text-center">

                </div>
                <div class="col-12 messages-result"></div>
            </div>
            <div class="row consructions">

                <h5>Конструкция</h5>
                <div class="col-md-12 doors"><div class="row rs-doors door-1">
                        <h6 class="container-fluid">Дверь №1</h6>

                        <div class="status-door container-fluid m-1">Дверь...Секция ,размер=300 Наполнитель=G20550Секция ,размер=500 Наполнитель=57617Секция ,размер=1300 Наполнитель=20550</div>
                        <div class="rs-doors">
                            <div class="rs-doors-sections">

                                <input type="hidden" id="doors_data_1" name="doors_data_1" value="{&quot;0&quot;:{&quot;height&quot;:&quot;300&quot;,&quot;filler&quot;:&quot;G20550&quot;},&quot;1&quot;:{&quot;height&quot;:&quot;500&quot;,&quot;filler&quot;:&quot;57617&quot;},&quot;2&quot;:{&quot;height&quot;:&quot;1300&quot;,&quot;filler&quot;:&quot;20550&quot;}}">
                                <button type="button" class="btn btn-sm btn-secondary m-3" data-toggle="modal" data-target="#creaDoors" data-valdoor="{&quot;0&quot;:{&quot;height&quot;:&quot;300&quot;,&quot;filler&quot;:&quot;G20550&quot;},&quot;1&quot;:{&quot;height&quot;:&quot;500&quot;,&quot;filler&quot;:&quot;57617&quot;},&quot;2&quot;:{&quot;height&quot;:&quot;1300&quot;,&quot;filler&quot;:&quot;20550&quot;}}" data-numdoor="1">Собрать дверь
                                </button>
                                <button type="button" class="btn btn-sm btn-secondary m-3" id="filltest" >тест заполнения</button>

                            </div>
                            <div class="rs-doors inners-sections">

                            </div>
                        </div>
                    </div><div class="row rs-doors door-2">
                        <h6 class="container-fluid">Дверь №2</h6>

                        <div class="status-door container-fluid m-1">Дверь...Секция ,размер=1050 Наполнитель=P88920Секция ,размер=1050 Наполнитель=20550</div>
                        <div class="rs-doors">
                            <div class="rs-doors-sections">

                                <input type="hidden" id="doors_data_2" name="doors_data_2" value="{&quot;0&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;P88920&quot;},&quot;1&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;20550&quot;}}">
                                <button type="button" class="btn btn-sm btn-secondary m-3" data-toggle="modal" data-target="#creaDoors" data-valdoor="{&quot;0&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;P88920&quot;},&quot;1&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;20550&quot;}}" data-numdoor="2">Собрать дверь
                                </button>

                            </div>
                            <div class="rs-doors inners-sections">

                            </div>
                        </div>
                    </div><div class="row rs-doors door-3">
                        <h6 class="container-fluid">Дверь №3</h6>

                        <div class="status-door container-fluid m-1">Дверь...Секция ,размер=1050 Наполнитель=P88920Секция ,размер=1050 Наполнитель=20550</div>
                        <div class="rs-doors">
                            <div class="rs-doors-sections">

                                <input type="hidden" id="doors_data_3" name="doors_data_3" value="{&quot;0&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;P88920&quot;},&quot;1&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;20550&quot;}}">
                                <button type="button" class="btn btn-sm btn-secondary m-3" data-toggle="modal" data-target="#creaDoors" data-valdoor="{&quot;0&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;P88920&quot;},&quot;1&quot;:{&quot;height&quot;:&quot;1050&quot;,&quot;filler&quot;:&quot;20550&quot;}}" data-numdoor="3">Собрать дверь
                                </button>

                            </div>
                            <div class="rs-doors inners-sections">

                            </div>
                        </div>
                    </div></div>
            </div>
        </div>

        <div class="col-md-12 text-right">

            <button type="button" class="btn btn-sm btn-secondary m-3" id="send">Ок</button>
        </div>
        <div class="col-12 messages-result">

        </div>
    </div>
    </form>


<div class="modal fade" id="creaDoors" tabindex="-1" role="dialog" aria-labelledby="creaDoorsLabel" aria-hidden="true">
    <div class="modal-dialog " role="document" style="min-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="creaDoorsLabel">Дверь</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <input type="hidden" id="numDoor" name="numDoor" value="">
                <?php /*
                    <input type="hidden" id="targetid" name="targetid" value="">
                    <input type="hidden" id="typeop" name="typeop" value="">
                    <input type="hidden" id="t" name="t" value="">
                    <input type="hidden" id="cut" name="cut" value="">
                    <input type="hidden" id="editProfile" name="editProfile" value="0">
                    */ ?>
                <div class="modal-body-in">

                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once('../footer.php'); ?>
<script>
    function OnTextFilling(code, name) {

        $("#searchOnText-box").show();
        $("#searchOnText-box").html('');

        var listdata_in=$('#datalistMaterials').html();
        if(listdata_in.length===0)
        {
            var listdata = {};
        }
        else {
            listdata = JSON.parse(listdata_in);
        }
        listdata[code]=name;
        var out=JSON.stringify(listdata);
        $('#datalistMaterials').html(out);

        var options='';

        $.each(listdata,function(index,value){
            options=options+'<option value="'+index+'">'+value+'</option>';

        });
        $('#listMaterials').html(options);
        $('.filling .messages-result').html('<div class="alert alert-success" role="alert">Наполнитель добавлен!</div>');
        setTimeout(function(){  $('.filling .messages-result').html('');$('input[name="searchOnText"]').val('')}, 5000);

    }
</script>
