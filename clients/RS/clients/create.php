<?php
session_start();
include_once('../../../_1/config.php');
include_once('../../../func_mini.php');
include_once('../../functions2.php');
include_once('../functions3.php');
include_once('funct_clients.php');

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

$temp_storage = $serv_main_dir.'/files/temp/';
$project_storage = $serv_main_dir.'/files/project_out/';
// p_($_SESSION);exit;
//..24.11.2020 Если менеджер переходит сюда без клиента, переадресовуем его на index.php
if (!isset($_GET['session_id'])) {
    if (!isset($_GET['order_project_id'])) {
        if (((isset($_SESSION['manager_id']) || (isset($_SESSION['user']) && $_SESSION['user']['role'] == 'manager'))) && !isset($_GET['client_id'])) {
            unset ($_SESSION);
            //.. Восстанавливаем сессию
            if (isset($session_restore['project_manager'])) {
                $_SESSION['project_manager'] = $session_restore['project_manager'];
            }
            if (isset($session_restore['project_client'])) {
                $_SESSION['project_client'] = $session_restore['project_client'];
            }
            if (isset($session_restore['user'])) {
                $_SESSION['user'] = $session_restore['user'];
            }
            if (isset($session_restore['tec_info'])) {
                $_SESSION['tec_info'] = $session_restore['tec_info'];
            }
        }
    }
}
// exit ('11');

//..20.11.2020
if(isset($_GET['session_id'])) {
    //.. Сохраняем сессию авторизации
    if (isset($_SESSION['user'])) {
        $session_backup = [];
        if (isset($_SESSION['manager_id'])) {
            $session_backup['manager_id'] = $_SESSION['manager_id'];
            $session_backup['manager_name'] = $_SESSION['manager_name'];
            $session_backup['isAdmin'] = $_SESSION['isAdmin'];
        }
        $session_backup['user']['ID'] = $_SESSION['user']['ID'];
        $session_backup['user']['name'] = $_SESSION['user']['name'];
        $session_backup['user']['phone'] = $_SESSION['user']['phone'];
        $session_backup['user']['mail'] = $_SESSION['user']['mail'];
        $session_backup['user']['role'] = $_SESSION['user']['role'];
        $session_backup['user']['code'] = $_SESSION['user']['code'];
    }
    $_SESSION = get_temp_file_data($_GET["session_id"]);
    //.. Тут почему-то важны двойные кавычки
    $project_data = RS_from_project($_SESSION["project_data"]);
    //.. Восстанавливаем данные юзера
    if (isset($session_backup) && !empty($session_backup)) {
        if (isset($session_backup['manager_id'])) {
            $_SESSION['manager_id'] = $session_backup['manager_id'];
            $_SESSION['manager_name'] = $session_backup['manager_name'];
            $_SESSION['isAdmin'] = $session_backup['isAdmin'];
        }
        $_SESSION['user']['ID'] = $session_backup['user']['ID'];
        $_SESSION['user']['name'] = $session_backup['user']['name'];
        $_SESSION['user']['phone'] = $session_backup['user']['phone'];
        $_SESSION['user']['mail'] = $session_backup['user']['mail'];
        $_SESSION['user']['role'] = $session_backup['user']['role'];
        $_SESSION['user']['code'] = $session_backup['user']['code'];
    }
    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}
// В GET передаётся параметр, с помошью которого мы получаем данные проекта
// Условие если создаётся новый проект из кабинета менеджера
if(isset($_GET['client_id'])) {
    if (!isset($_SESSION['client_id'])) {
        $_SESSION['client_id'] = $_GET['client_id'];
    }
}

// Передаём идентификатор
if (isset($_GET['order_project_id']) && !empty($_GET['order_project_id'])) {
    $fieldOfProject = 'MY_PROJECT_OUT';
    // Если проект есть в базе (база данных либо файловое хранилище)
    if (preg_match('#^[\d]+$#', $_GET['order_project_id'])) {
        if($p_data = get_Project_file($_GET['order_project_id'])) {
                $project_data = RS_from_project($p_data['project_data']);
        } else if ($result = sql_data(__LINE__,__FILE__,__FUNCTION__,'SELECT * FROM `PROJECT` WHERE `PROJECT_ID` = "'.$_GET['order_project_id'].'"')['data']) {
            if ($client_id = sql_data(__LINE__,__FILE__,__FUNCTION__,'SELECT `CLIENT_ID` FROM `ORDER1C` WHERE `ID` = "'.$result[0]['ORDER1C'].'"')['data']){
                $_SESSION['client_id'] = $client_id[0]['CLIENT_ID'];
                if ($result[0][$fieldOfProject] != '') {
                    $project_data = RS_from_project(base64_decode($result[0][$fieldOfProject]));
                }
            }
        }
    // Условие если вновь созданный проект идёт из zzz.php
    // zzz.php создаёт временный файл и в GET передаёт его имя (начинается с приставки st_)
    } elseif (preg_match('#^st_[\d]+$#', $_GET['order_project_id'])) {
        $temp_storage = $serv_main_dir.'/files/temp/';
        if (file_exists($temp_storage.$_GET['order_project_id'].'.tmp')) {
            $_SESSION['RSArray'] = file_get_contents($temp_storage.$_GET['order_project_id'].'.tmp');
        }
        $project_data = unserialize(base64_decode($_SESSION['RSArray']));
    }
}

$theCHLD=1;
$title='Создание конструкции';
include_once('../header.php');
$def_val=array(
'rs_size_x' => '',
'rs_size_y' => '',
'rs_profile' => '',
'profileDecors' => '',
'decors' => '',
'schetka' => '',
'dovodchiks' => '',
'rolick' => '',
'stopor' => '',
'datalm' => '',
'datalmf' => '',
'listMaterials' => '',
'doors' => '',
'place_id' => 1
);

$setting = rs_load_ini(); //   _pre($setting);
$setting_profile = db_get_setting_profile();// _pre($setting_profile);

$_satatusShowed=' d-none ';
$_sendDataShowed=' d-none ';
$_buttonShowedDisabled=' disabled ';
$_buttonShowedEnabled=' ';
$_rs_count_doors=0;

if(isset($_POST) && count($_POST)>0 && isset($_POST['data']) && !$project_data)
{
    $project_data=json_decode($_POST['data']);
}

if($project_data)
{
// echo $_POST['data'].'<hr>';
    $_sendDataShowed='';
    $_satatusShowed=''; 
    $_buttonShowedEnabled=' disabled ';
    $_buttonShowedDisabled=' ';
    $data=json_decode(json_encode($project_data), FALSE);
    // _pre($data);
    $_rs_count_doors=(int) substr($data->rs_count_doorsOverlap,0,1);
    $def_val['place_id']=$data->user_place;
    $def_val['rs_size_x']=$data->rs_size_x;
    $def_val['rs_size_y']=$data->rs_size_y;
    $def_val['rs_profile']=$data->rs_profile;
    $def_val['rs_profile_val']=$data->rs_profile_val;
    $def_val['profileDecors']=$data->profileDecors;
    $def_val['rs_decor_color']=$data->rs_decor_color;
    $def_val['rsCDO']=array("2_1"=>'',"3_2"=>'',"4_2"=>'',"4_3"=>'',"5_4"=>'',"6_5"=>'');


    $def_val['rsCDO'][$data->rs_count_doorsOverlap]=' selected ';
    $def_val['decors']='';
        $_tmp=db_get_profile_decors($data->rs_profile);
        create_profile_decors($_tmp, $def_val['decors'],$data->profileDecors);
        $def_val['decors']=str_replace('multiple','',$def_val['decors']);

        $_tmp=db_get_profile_schetka($data->rs_profile, $data->profileDecors);
        create_profile_schetka($_tmp, $def_val['schetka'], $data->profileSchetka);

        $_tmp=db_get_profile_dovodchik($data->rs_profile);
        create_profile_dovodchik($_tmp, $def_val['dovodchiks'], $data->profileDovodchik,TRUE);

        $_tmp=db_get_profile_rolick($data->rs_profile);
        create_profile_rolick($_tmp, $def_val['rolick'], $data->profileRolick);

        $_tmp=db_get_profile_stopor($data->rs_profile);
        create_profile_stopor($_tmp, $def_val['stopor'], $data->profileStopor,TRUE);

        $def_val['schetka']=str_replace('multiple','',$def_val['schetka']);
        $def_val['dovodchiks']=str_replace('multiple','',$def_val['dovodchiks']);
        $def_val['rolick']=str_replace('multiple','',$def_val['rolick']);
        $def_val['stopor']=str_replace('multiple','',$def_val['stopor']);
        $def_val['datalm']=json_encode($data->datalm);
        $def_val['datalmf']=json_encode($data->datalmf);
        $def_val['listMaterials']='';
        foreach ($data->datalm as $dkey=>$dvalue)
        {
            $def_val['listMaterials'].='<option value="'.$dkey.'">'.$dvalue.'</option>';
        }
    for($i=1;$i<count($data->doors);$i++) {
        $_arr_door=array();
        $_arr_door_def=array();
        foreach($data->doors_data[$i] as $_key=>$_tmp_door)
        {
            $_arr_door[]=array('filler'=>$_tmp_door->filler,'height'=>$_tmp_door->height,'id'=>$_tmp_door->index);
            $_arr_door_def[]=(array) $data->doors[$i][$_key]->processing;
            // _pre($data->doors[$i][$_key]);
        }
        $_inner_section=doors_inner_section_crea($_arr_door,$i,$data->rs_size_y, $_arr_door_def,$setting_profile['max_sandblasting_height']);

        $_inner_section_prot=json_encode($_arr_door,JSON_FORCE_OBJECT);
        $_inner_section_prot=str_replace('id', 'index',$_inner_section_prot);
        $def_val['doors'] .= doors_crea($i, count($data->doors)-1, $_inner_section_prot,$_inner_section);
    }

}

?>

    <form method="post"  id="loadConstruct">
        <input type="hidden" name="data" id="data" value="">
    </form>
    <form method="post" action="../../../calculate.php<?='?nw='.$_GET['nw']?>" id="sendData">
        <input type="hidden" name="cr_res" id="cr_res" value="">
    </form>
    <form method="post" id="creaRS">
        <input type="hidden" name="notNew" id="notNew" value="<?=$_rs_count_doors?>">

<input type="hidden" id="transp_glass" value="<?=$setting['packing']['transp_glass']?>">
<input type="hidden" id="uv_print" value="<?=$setting['packing']['uv_print']?>">
<input type="hidden" id="print_on_film" value="<?=$setting['packing']['print_on_film']?>">
<input type="hidden" id="constructMaxSanH" value="<?=$setting_profile['max_sandblasting_height']?>">



    <div class="row">
        <div class="col-lg-4 d-none d-lg-block d-xl-block">
            <div class="row">
                <div class="col-md-12 "  id="forCanvas">

                </div>
                <div class="col-md-12 text-center">
                    <div style="margin: 10px;display: inline-block;text-align: center; width: 100%;font-size: 14px; font-weight: bold; color: red;">
                        Расшифровка
                    </div>
                    <div style="margin: 10px;display: inline-block;text-align: center;font-size: 10px; ">
                        <div style="background: #ff9966; float:left;  padding: 2px 10px;margin: 3px;">Плитный материал</div>
                        <div style="background: #3399ff; float:left;  padding: 2px 10px;margin: 3px;">Стекло</div>
                        <div style="background: #99cc00; float:left;  padding: 2px 10px;margin: 3px;">Зеркало</div>
                    </div>
                    <?php /* ?>
                    <ul class="list-group list-group-horizontal-xl">
                        <li class="list-group-item" style="background: #ff9966;">Плитный материал</li>
                        <li class="list-group-item" style="background: #3399ff;">Стекло</li>
                        <li class="list-group-item" style="background: #99cc00;">Зеркало</li>
                    </ul>
                    <?php */ ?>
                </div>

            </div>
        </div>
        <div class="col-lg-8 col-md-12">

            <div class="row">
                <div class="col-md-12">
                        <a class="text-primary w-100 text-right float-right mb-3" data-toggle="collapse" href="#uploagCustom" role="button" aria-expanded="false" aria-controls="uploagCustom">
                            Загрузить свой проект
                        </a>
                    <div class="collapse" id="uploagCustom">
                    <div class="row border border-primary mb-4 pt-3" >
                        <div class="form-group col-4">
                            <input type="file" class="form-control-file" id="file-rs" name="file-rs">
                        </div>
                        <div class="form-group col-2">
                            <input type="button" class="btn btn-primary mb-2" id="load-rs" value="Загрузить">
                        </div>
                    </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <?= db_get_listPlaces(false, $def_val['place_id'],1,FALSE) ?>
                </div>
                <h5>Размер конструкции и профиль</h5>
                <div class="col-md-12">
                    <div class="row">
                        
                        <div class="col-lg-3 col-md-6">
                            <label for="rs_size_y">Высота</label>
                            <input type="text" class="form-control" id="rs_size_y" value="<?=$def_val['rs_size_y']?>">
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label for="rs_size_x">Ширина</label>
                            <input type="text" class="form-control" id="rs_size_x" value="<?=$def_val['rs_size_x']?>" >
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label for="rs_profile">Профиль</label>
                            <input type="hidden" id="rs_profile_val" name="rs_profile_val" value="<?=$def_val['rs_profile_val']?>">
                            <select class="form-control" id="rs_profile">
                                <?=db_profile_list_options($def_val['rs_profile'])?>
                            </select>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label for="rs_decor">Декор</label>
                            <input type="hidden" id="rs_decor_color" name="rs_decor_color" value="<?=$def_val['rs_decor_color']?>">
                            <div class="forprofileDecor">
                                <?=$def_val['decors']?>
                            </div>
                        </div>
                        <div class="col-12 text-right pt-2 pb-3">
                            <button type="button" class="btn btn-sm btn-success" id="set1" <?=$_buttonShowedEnabled?>>Далее</button>
                            <button type="button" class="btn btn-sm btn-danger" id="unset1" <?=$_buttonShowedDisabled?>><i class="fas fa-undo-alt"></i> Вернуть</button>
                        </div>
                            <div class="col-12 messages-set1">

                            </div>

                    </div>
                </div>
            </div>

            <div class="row <?=$_satatusShowed?> complectives">

                <h5>Комплектация профиля</h5>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <label >Щетка с зажимом</label>
                            <div class="forprofileSchetka"><?=$def_val['schetka']?>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label >Доводчик</label>
                            <div class="forprofileDovodchik"><?=$def_val['dovodchiks']?>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label >Количество доводчиков</label>
                            <div class="countDovodchik">
                            <select  class="countDovodchik" id="countDovodchik" name="countDovodchik"><option value="1"  selected >Один</option><option value="2" >Два</option></select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label >Ролик</label>
                            <div class="forprofileRolick"><?=$def_val['rolick']?>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <label >Стопор</label>
                            <div class="forprofileStopor"><?=$def_val['stopor']?>
                            </div>
                         </div>
                    </div>
                        <div class="row justify-content-end pb-3">
                        <div class="col-lg-3 col-md-6">
                            <label for="rs_count_doorsOverlap">Дверей и перехлестов</label>
                            <select class="form-control" id="rs_count_doorsOverlap">


                                <option value="2_1" <?=$def_val['rsCDO']['2_1']?> >2 двери, 1 перехлест</option>
                                <option value="3_2" <?=$def_val['rsCDO']['3_2']?> >3 двери, 2 перехлеста</option>
                                <option value="4_2" <?=$def_val['rsCDO']['4_2']?> >4 двери, 2 перехлеста</option>
                                <option value="4_3" <?=$def_val['rsCDO']['4_3']?> >4 двери, 3 перехлеста</option>
                                <option value="5_4" <?=$def_val['rsCDO']['5_4']?> >5 дверей, 4 перехлеста</option>
                                <option value="6_5" <?=$def_val['rsCDO']['6_5']?> >6 дверей, 5 перехлестов</option>


                            </select>
                        </div>
                        <div class="col-lg-3 col-md-6 text-right pt-2 align-self-end">
                            <button type="button" class="btn btn-sm btn-success" id="set2" <?=$_buttonShowedEnabled?>>Далее</button>
                            <button type="button" class="btn btn-sm btn-danger" id="unset2" <?=$_buttonShowedDisabled?>><i class="fas fa-undo-alt"></i> Вернуть</button>
                        </div>
                            <div class="col-12 messages-set2">

                            </div>

                    </div>
                </div>
            </div>


            <div class="row filling <?=$_satatusShowed?> ">
                <div class="col-12">
                <h5>Заполнение</h5></div>
                <div class="col-lg-8 col-md-12 ">
                    <div class="form-group">

                        <textarea class="d-none" id="datalistMaterials"><?=$def_val['datalm']?></textarea>
                        <textarea class="d-none" id="datalistMaterialsFull"><?=$def_val['datalmf']?></textarea>
                        <label for="listMaterials">Перечень материала для наполнения</label>
                        <select multiple class="form-control" id="listMaterials">
                            <?=$def_val['listMaterials']?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12  text-center">
                    <div class="form-group" >
                        <div class="spinner-border text-primary loading-search-on-text " role = "status" >
                            <span class="sr-only" > Loading...</span >
                        </div >
                        <label for="searchOnText" > Найти по коду </label >
                        <input type = "text" class="form-control searchOnText" name = "searchOnText" >
                        <div class="searchOnText-box" id="searchOnText-box" ></div >
                    </div ><br>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-valdoor="" data-numdoor="0" id="addMaterial"
                        <?=$_buttonShowedEnabled?>  data-target="#creaDoors">Добавить
                    </button>
                    <button type="button" class="btn btn-sm btn-danger" id="eraseMaterial" <?=$_buttonShowedEnabled?>>Удалить</button>
                    <button type="button" class="btn btn-sm btn-success" id="set3" <?=$_buttonShowedDisabled?>>Далее</button>
                    <button type="button" class="btn btn-sm btn-danger" id="unset3" <?=$_buttonShowedDisabled?>><i class="fas fa-undo-alt"></i> Вернуть</button>
                </div>
                <div class="col-12 messages-result">

                </div>
            </div>
            <div class="row consructions <?=$_satatusShowed?>">
                <h5>Конструкция</h5>
                <div class="col-md-12 doors">
<?=$def_val['doors']?>
                </div>
            </div>
        </div>

        <div class="col-md-12 text-right">
            <a download="file.rs" class="btn btn-warning d-none" id="download_file_rs" href="#">Скачайте Ваш проект <i class="fas fa-download"></i></a>
            <button type="button" class="btn btn-success <?=$_satatusShowed?>" id="save-rs">Сохранить <i class="far fa-save"></i></button>
           <button type="button" class="btn btn-sm btn-sm btn-secondary m-3 <?=$_satatusShowed?>" id="send">Ок</button>
        </div>
        <div class="col-12 messages-result">

        </div>
    </div>
    </form>


<div class="modal fade" id="creaDoors" tabindex="-1" role="dialog" aria-labelledby="creaDoorsLabel" aria-hidden="true">
    <div class="modal-dialog " role="document" style="min-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="creaDoorsLabel">Дверь</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <input type="hidden" id="numDoor" name="numDoor" value="">
                <?php /*
                    <input type="hidden" id="targetid" name="targetid" value="">
                    <input type="hidden" id="typeop" name="typeop" value="">
                    <input type="hidden" id="t" name="t" value="">
                    <input type="hidden" id="cut" name="cut" value="">
                    <input type="hidden" id="editProfile" name="editProfile" value="0">
                    */ ?>
                <div class="modal-body-in">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalMessage" role="dialog">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <p>Ошибка, повторите заполнение!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<?php include_once('../footer.php'); ?>
<script>
    function OnTextFilling(code, name) {

        $("#searchOnText-box").show();
        $("#searchOnText-box").html('');

        var listdata_in=$('#datalistMaterials').html();
        if(listdata_in.length===0)
        {
            var listdata = {};
        }
        else {
            listdata = JSON.parse(listdata_in);
        }
        listdata[code]=name;
        var out=JSON.stringify(listdata);
        $('#datalistMaterials').html(out);

        var options='';

        $.each(listdata,function(index,value){
            options=options+'<option value="'+index+'">'+value+'</option>';

        });
        $('#listMaterials').html(options);
        $('#creaRS button#set3').prop("disabled", false);
        $('.filling .messages-result').html('<div class="alert alert-success" role="alert">Наполнитель добавлен!</div>');
        setTimeout(function(){  $('.filling .messages-result').html('');$('input[name="searchOnText"]').val('')}, 5000);

    }
    jQuery(document).ready(function() {
        tmp_count_doors=$('#notNew').val();
        if(tmp_count_doors>0)
        {
            for (i=1;i<=tmp_count_doors;i++)
            {
                in_data=$('#door-indata-'+i).html();
                $('input#doors_data_'+i).val(in_data);
                $('button[data-numdoor="'+i+'"]').attr("data-valdoor", in_data);
            }
        }
    });
</script>
