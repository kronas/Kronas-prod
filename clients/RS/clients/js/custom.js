jQuery(document).ready(function() {
    var shkaf3d;
    $(document).on('hide.bs.modal', '#creaDoors', function (event) {

//   create3d();
        setTimeout(function () {            create3d();        }, 3000);
    });
    $(document).on('shown.bs.modal', '#creaDoors', function (event) {

//        $('#creaRS #forCanvas canvas#canvas').html('');
        //      shkaf3d.disable_drawing();
    });
    $(document).on('click', '#creaRS #send', function (event, sel) {
        event.stopImmediatePropagation(); // off dubleclick
        $("#start_checks_lodaer_containers").removeClass('d-none');
        var out=readForm();
        var outJS=JSON.stringify(out);
        $.ajax({
            type: 'POST',
            url: 'ajax_send.php',
            data: {'data': outJS},
            success: function (resp) {
                var resp_in = JSON.parse(resp);
                if (resp_in.result) {
                     $('#sendData #cr_res').val(JSON.stringify(resp_in.res));
                     $( '#sendData' ).submit();
                } else {
                    $('#creaRS button#unset1').prop("disabled", false);
                    $('#creaRS button#unset2').prop("disabled", false);
                    $('#creaRS button#unset3').prop("disabled", false);
                    $('#creaRS .consructions button').prop("disabled", false);
                    $('#creaRS .consructions input').prop("disabled", false);
                    $('#creaRS .consructions select').prop("disabled", false);
                    $('#modalMessage .modal-body').html(resp_in.mess_error);
                    $("#start_checks_lodaer_containers").addClass('d-none');
                    $('#modalMessage').modal('show');
                    $('body,html').animate({
                        scrollTop: 0
                    }, 400);
                }
            }
        });
        return false;
    });

    $(document).on('click', '#creaRS #set1', function (event, sel) {
        /*
        var constructMinHeight = $('input#constructMinHeight').val();
        var constructMaxHeight = $('input#constructMaxHeight').val();
        var constructMinWidth = $('input#constructMinWidth').val();
        var constructMaxWidth = $('input#constructMaxWidth').val();
*/

        var rs_size_x = parseFloat($('#creaRS #rs_size_x').val());
        var rs_size_y = parseFloat($('#creaRS #rs_size_y').val());
        var rs_profile = parseInt($('#creaRS select#rs_profile').find(':selected').val());
        var rs_decor = parseInt($('#creaRS select#profileDecors').find(':selected').val());
        if (!isNaN(rs_size_x) &&
            !isNaN(rs_size_y) &&
            !isNaN(rs_profile) && rs_profile > 0 &&
            !isNaN(rs_decor) && rs_decor > 0) {
            $.ajax({
                type: 'POST',
                url: 'ajax_cresp.php',
                data: 'RSprofile2=' + rs_profile + '&RSdecor=' + rs_decor+ '&RSsizeX=' + rs_size_x+ '&RSsizeY=' + rs_size_y,
                success: function (data) {
                    var resp_in = JSON.parse(data);
                    if (resp_in.result) {
                        $('#creaRS #rs_size_x').prop("disabled", true);
                        $('#creaRS #rs_size_y').prop("disabled", true);
                        $('#creaRS select#rs_profile').prop("disabled", true);
                        $('#creaRS select#profileDecors').prop("disabled", true);
                        $('#creaRS button#set1').prop("disabled", true);
                        $('#creaRS button#unset1').prop("disabled", false);
                        $(".complectives").removeClass('d-none');
                        $(".forprofileSchetka").html(resp_in.schetka);
                        $(".forprofileDovodchik").html(resp_in.dovodchiks);
                        $(".forprofileRolick").html(resp_in.rolick);
                        $(".forprofileStopor").html(resp_in.stopor);
                        $("input[name='rs_decor_color']").val(resp_in.decor_color);
                        $("input[name='rs_profile_val']").val(resp_in.profile_type);
                        create3d();
                    }else
                    {

                        $('#creaRS .messages-set1').html('<div class="alert alert-danger" role="alert">'+resp_in.message+'</div>');
                    }
                }
            });

        } else {
            $('#creaRS .messages-set1').html('<div class="alert alert-danger" role="alert">Введите корректниые данные!</div>');
        }
        setTimeout(function () {
            $('#creaRS .messages-set1').html('');
        }, 5000);
        return false;
    });

    $(document).on('click', '#creaRS #unset1', function (event, sel) {
        $('#creaRS #rs_size_x').prop("disabled", false);
        $('#creaRS #rs_size_y').prop("disabled", false);
        $('#creaRS select#rs_profile').prop("disabled", false);
        $('#creaRS select#profileDecors').prop("disabled", false);
        $('#creaRS button#set1').prop("disabled", false);
        $('#creaRS button#unset1').prop("disabled", true);
        $(".forprofileSchetka").html('');
        $(".forprofileDovodchik").html('');
        $(".forprofileRolick").html('');
        $(".forprofileStopor").html('');
        // $("input[name='rs_decor_color']").val('');
        // $("input[name='rs_profile_val']").val('');
        $('input[name="searchOnText"]').prop("disabled", false);
        $('#rs_count_doorsOverlap option[value="2_1"]').prop('selected', true);
        $('#creaRS button#set2').prop("disabled", false);
        $('#creaRS button#unset2').prop("disabled", true);

        $("#listMaterials").html('');
        $("#datalistMaterials").html('');
        $("#datalistMaterialsFull").html('');
        $(".consructions .doors").html('');

        $('#creaRS button#addMaterial').prop("disabled", false);
        $('#creaRS button#eraseMaterial').prop("disabled", false);
        $('#creaRS button#set3').prop("disabled", false);
        $('#creaRS button#unset3').prop("disabled", true);

        $(".complectives").addClass('d-none');
        $(".filling").addClass('d-none');
        $(".consructions").addClass('d-none');
        $('#creaRS button#send').addClass('d-none');
        create3d();


        $('#creaRS .messages-set1').html('<div class="alert alert-success" role="alert">Перешли к первому шагу!</div>');
        setTimeout(function () {
            $('#creaRS .messages-set1').html('');
        }, 5000);
        return false;
    });

    $(document).on('click', '#creaRS #set2', function (event, sel) {
        var doorsOverlap=$('#rs_count_doorsOverlap').find(':selected').val();
        var count_doors = parseInt(doorsOverlap[0]);
        var overlaps = parseInt(doorsOverlap[2]);

            $('#creaRS button#set2').prop("disabled", true);
            $('#creaRS button#unset2').prop("disabled", false);
            $('#creaRS .complectives select').prop("disabled", true);
            $('#creaRS #rs_count_doorsOverlap').prop("disabled", true);
            $(".filling").removeClass('d-none');
            $.ajax({
                type: 'POST',
                url: 'ajax_cresp.php',
                data: 'createArrayDoors=' + count_doors,
                success: function (data) {
                    var resp_in = JSON.parse(data);
                    $(".consructions .doors").html(resp_in.doors);
                }
            });
            create3d();

        setTimeout(function () {
            $('#creaRS .messages-set2').html('');
        }, 5000);
        return false;
    });

    $(document).on('click', '#creaRS #unset2', function (event, sel) {

        $(".forprofileSchetka").prop("disabled", false);
        $(".forprofileDovodchik").prop("disabled", false);
        $(".forprofileRolick").prop("disabled", false);
        $(".forprofileStopor").prop("disabled", false);
        $('input[name="searchOnText"]').prop("disabled", false);
        $('#rs_count_doorsOverlap option[value="2_1"]').prop('selected', true);
        $("#rs_count_doorsOverlap").prop("disabled", false);
        $("input#rs_overlap").prop("disabled", false);
        $('#creaRS button#set2').prop("disabled", false);
        $('#creaRS button#unset2').prop("disabled", true);
        $("#listMaterials").html('');
        $("#datalistMaterials").html('');
        $("#datalistMaterialsFull").html('');
        $(".consructions .doors").html('');

        $('#creaRS button#addMaterial').prop("disabled", false);
        $('#creaRS button#eraseMaterial').prop("disabled", false);
        $('#creaRS button#set3').prop("disabled", false);
        $('#creaRS button#unset3').prop("disabled", true);

        $(".filling").addClass('d-none');
        $(".consructions").addClass('d-none');
        $('#creaRS button#send').addClass('d-none');
        create3d();

        $('#creaRS .messages-set2').html('<div class="alert alert-success" role="alert">Перешли к второму шагу!</div>');
        setTimeout(function () {
            $('#creaRS .messages-set2').html('');
        }, 5000);
        return false;
    });

    $(document).on('click', '#creaRS #set3', function (event, sel) {
        var doorsOverlap=$('#rs_count_doorsOverlap').find(':selected').val();
        var count_doors = parseInt(doorsOverlap[0]);
        $('#creaRS button#set3').prop("disabled", true);
        $('#creaRS button#unset3').prop("disabled", false);
        $('#creaRS button#eraseMaterial').prop("disabled", true);
        $('#creaRS button#addMaterial').prop("disabled", true);
        $('#creaRS .listMaterials').prop("disabled", true);
        $('#creaRS .searchOnText').prop("disabled", true);
        $(".consructions").removeClass('d-none');
        $.ajax({
            type: 'POST',
            url: 'ajax_cresp.php',
            data: 'createArrayDoors=' + count_doors,
            success: function (data) {
                var resp_in = JSON.parse(data);
                $(".consructions .doors").html(resp_in.doors);
            }
        });
        createFullMaterials();
    });


    $(document).on('click', '#creaRS #unset3', function (event, sel) {

        $('input[name="searchOnText"]').prop("disabled", false);

        $(".consructions .doors").html('');

        $('#creaRS button#addMaterial').prop("disabled", false);
        $('#creaRS button#eraseMaterial').prop("disabled", false);
        $('#creaRS button#set3').prop("disabled", false);
        $('#creaRS button#unset3').prop("disabled", true);

        $(".consructions").addClass('d-none');
        $('#creaRS button#send').addClass('d-none');
        create3d();

        $('#creaRS .messages-set3').html('<div class="alert alert-success" role="alert">Перешли к третьему шагу!</div>');
        setTimeout(function () {
            $('#creaRS .messages-set3').html('');
        }, 5000);
        return false;
    });

    $(document).on('click', '#creaDoors #uploadFile', function (event, sel) {
        var file_data = $("#filePersonal").prop("files")[0];
        var form_data = new FormData();
        form_data.append("file", file_data);
        $.ajax({
            url: 'ajax_cresp.php',
            type: 'post',
            data: form_data,
            contentType: false,
            processData: false,
            success: function (data) {
                var resp_in = JSON.parse(data);
                if (resp_in.result) {
                    $('#creaDoors .file-message').html('<div class="alert alert-success" role="alert">' + resp_in.message + '</div>');
                    $('.picturesSelect').removeClass('d-none');
                    $('.picturesSelect .picSelImage').attr('src', resp_in.file);
                    $('.picturesSelect [name="picSelVal"]').val(resp_in.file);
                } else {
                    $('#creaDoors .file-message').html('<div class="alert alert-danger" role="alert">' + resp_in.message + '</div>');
                }
            },
        });
        setTimeout(function () {
            $('#creaRS .file-message').html('');
        }, 5000);
    });

    $(document).on('click', '#creaRS #eraseMaterial', function (event, sel) {
        var tmp = 0;
        var selectedMaterial = 0;
        $("#creaRS select#listMaterials option:selected").each(function () {
            selectedMaterial = $(this).val();
            tmp++;
        });
        if (tmp === 1) {
            var listdata_in = $('#datalistMaterials').html();
            if (listdata_in.length === 0) {
                var listdata = {};
            } else {
                listdata = JSON.parse(listdata_in);
            }
            var options = '';
            var newlistdata = {};
            $.each(listdata, function (index, value) {
                if (parseInt(index) !== parseInt(selectedMaterial)) {
                    options = options + '<option value="' + index + '">' + value + '</option>';
                    newlistdata[index] = value;
                }
            });
            var out = JSON.stringify(newlistdata);
            $('#datalistMaterials').html(out);
            $('#listMaterials').html(options);
            if(options.length===0) $('#creaRS button#set3').prop("disabled", true);
            $('.filling .messages-result').html('<div class="alert alert-success" role="alert">Наполнитель удален!</div>');
        } else {
            $('.filling .messages-result').html('<div class="alert alert-danger" role="alert">Выберите один наполнитель для удаления!</div>');
        }
        setTimeout(function () {
            $('.filling .messages-result').html('');
        }, 5000);
    });
    $(document).on('change', '#creaRS select[name="sectionprocessing[]"]', function (event, sel) {
        var select_process = parseInt($(this).find(':selected').val());
        var typeOP = $(this).attr('data-op');
        var numDoor = $(this).parent().attr('data-numdoor');
        var numSect = $(this).parent().attr('data-numsect');
        if (typeOP === 'G') {
            $('.rs-doors-sections[data-numsect="' + numSect + '"][data-numdoor="' + numDoor + '"] input[name="selectedImage[]"]').val('');
            $('.rs-doors-sections[data-numsect="' + numSect + '"][data-numdoor="' + numDoor + '"] div.image').html('');
            if (select_process === 1 || select_process === 2 || select_process === 5) {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionoracal').addClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').removeClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionlakobel').addClass('d-none');
            } else if(select_process === 8) {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').removeClass('d-none');
            } else if (select_process === 3) {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionoracal').addClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionlakobel').removeClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').addClass('d-none');
            } else if (select_process === 4) {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionoracal').removeClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionlakobel').addClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').addClass('d-none');
            } else {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionoracal').addClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').addClass('d-none');
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] #sectionlakobel').addClass('d-none');
            }
        }

        if (typeOP === 'M') {
            $('.rs-doors-sections[data-numsect="' + numSect + '"][data-numdoor="' + numDoor + '"] input[name="selectedImage[]"]').val('');
            $('.rs-doors-sections[data-numsect="' + numSect + '"][data-numdoor="' + numDoor + '"] div.image').html('');
            if (select_process === 1 || select_process === 2 || select_process === 3) {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').removeClass('d-none');
            } else {
                $('#creaRS div[data-numdoor="' + numDoor + '"][data-numsect="' + numSect + '"] button').addClass('d-none');
            }
        }

    });

    $(document).on('change', '#sect_fill', function(event) {
        let select = $(event.target);
        let option = $(select).find('option:selected');
        let option_type = $(option).attr('data-type');
        if(option_type == 1) {
            $(select).closest('.door-section').find('.txt_material_door').show('slow');
        } else {
            $(select).closest('.door-section').find('.txt_material_door').hide('slow');
        }
    })

    $(document).on('change', '#creaRS select#rs_profile', function (event, sel) {
        var select_profile = parseInt($(this).find(':selected').val());
        if (!isNaN(select_profile)) {
            /*
            $('#profiles #profile_edit').prop( "disabled", false );
            $('#profiles #erase_proflie').prop( "disabled", false );
            $('button#profile_edit').attr("data-targetid",select_profile);
            */
            $.ajax({
                type: 'POST',
                url: 'ajax_cresp.php',
                data: 'RSprofile=' + select_profile,
                success: function (data) {
                    var resp_in = JSON.parse(data);
                    $(".forprofileDecor").html(resp_in.decors);
                }
            });
        } else {
            $(".forprofileDecor").html('');
            $('#profiles #profile_edit').prop("disabled", true);
            $('#profiles #erase_proflie').prop("disabled", true);
            $('button#profile_edit').attr("data-targetid", 0);
        }
    });

    $('input[name="searchOnText"]').keyup(function () {
        $.ajax({
            type: "POST",
            url: "ajax_cresp.php",
            data: 'keywordOnText=' + $(this).val(),
            beforeSend: function () {
                $(".loading-search-on-text").show();
            },
            success: function (data) {
                $("#searchOnText-box").show();
                $("#searchOnText-box").html(data);
                $(".loading-search-on-text").hide();
            }
        });
    });

    $(document).on('click', '#creaRS #save-rs', function (event, sel) {
        event.stopImmediatePropagation(); // off dubleclick

        $("#start_checks_lodaer_containers").removeClass('d-none');
        var out=readForm();
        var outJS=JSON.stringify(out);
        var type = 'data:application/octet-stream;text, ';
        var text = outJS;
        var base = btoa(unescape(encodeURIComponent(text)));
        var res = type + base;
        $("#download_file_rs").attr('href', res);
        $("#download_file_rs").removeClass('d-none');
        setTimeout(function () {

            $("#start_checks_lodaer_containers").addClass('d-none');
            $("#download_file_rs")[0].click();
        }, 1000);

        return false;
    });


    $(document).on('click', '#creaRS #load-rs', function (event, sel) {
        $("#start_checks_lodaer_containers").removeClass('d-none');
        var file_data = $("#file-rs").prop("files")[0];
        var reader = new FileReader();
        reader.onload = function() {
            var fdata = reader.result;
            var fdata2 = decodeURIComponent(escape(atob(fdata)));
            $('form#loadConstruct #data').val(fdata2);
            $('form#loadConstruct').submit();
        };
        reader.readAsText(file_data);

        return false;
    });



    if(!$("#creaRS #forCanvas canvas#canvas").length)
{
    divforCanvasW = $("#creaRS #forCanvas").width();

    var canvasContainer = '<canvas height="' + (divforCanvasW - 25) + '" width="' + (divforCanvasW - 25) + '" id="canvas"></canvas>';

    $('#creaRS #forCanvas').html(canvasContainer);

    var canvas = document.getElementById("canvas");
    shkaf3d = new Shkaf3D(canvas);// создаем класс визуализации шкафа купе
    shkaf3d.enable_drawing();// включить отрисовку, на случай если окно canvas будет не активне, надо обязательно отключить, вызов shkaf3d.enable_drawing();
    function WindowResize() {
        shkaf3d.WindowResize();	// вызываем при изменении размера окна canvas
    }

    window.addEventListener('resize', WindowResize);
    var profile = "Slider_Quadro";
    var state = {
        height: 200, width: 400,/*width можно не указивать, размер будет получен автоматически*/
        K: 2, N: 1,/*согласно схемы конструкции */
        background: ""/*background - фон */,
        /*background :"./res/img/b1c94eea13a05e3c9b4b824695ea57f9.jpg"background - фон ,*/
        profile_color: "#000000" /*profile_color - цвет профилей*/,
        rails: {top: profile, bottom: profile}/*top,bottom направляющие профиля*/,
        doors: [{
            profile_vertical: profile,
            profile_top: profile,
            profile_bottom: profile,
            listfill: [{p: 0, d: 1, texture: "./res/img/990617_1.jpeg", texture_size: 200, opacity: 0.3}]
        }
            ,
            {
                profile_vertical: profile,
                profile_top: profile,
                profile_bottom: profile,
                listfill: [{p: 0, d: 1, texture: "./res/img/990617_1.jpeg", texture_size: 200, opacity: 0.3}]
            }]
    };

    shkaf3d.setState(state);//задаем состояния шкафа, передаем объект со свойствами, при изменении состояния повторно вызываем
}

    $(document).on('change', '.consructions .rs-doors-sections select.door-clone', function (event, sel) {
        event.stopImmediatePropagation(); // off dubleclick
        if(parseInt($(this).val())>0)
    {
        var out3 = {};
        var targetDoor = $(this).data('num');
        var fromDoor = parseInt($(this).find(':selected').val());
        var getData0 = $('input#doors_data_' + fromDoor).val();
        $('input#doors_data_' + targetDoor).val(getData0);
        $('.doors .row.door-' + targetDoor + ' button').attr('data-valdoor', getData0);

        var getData1 = JSON.parse(getData0);
        out3['numDoor'] = String(targetDoor);
        out3['sections'] = getData1;
        out3['doorHeight'] = $('input#rs_size_y').val();
        out3['MaxSanH']=$('input#constructMaxSanH').val();
        $.ajax({
            type: 'POST',
            url: 'ajax_cresp.php',
            data: {'data': JSON.stringify(out3)},
            success: function (resp) {
                var resp_in = JSON.parse(resp);
                if (resp_in.result) {
                    $('.doors .row.door-' + targetDoor + ' .rs-doors.inners-sections').html(resp_in.data);
                    //  $('#profiles .messages').html('<div class="alert alert-success" role="alert">'+resp_in.message+'</div>');

                } else {
                    //                       $('.messages').html('<div class="alert alert-danger" role="alert">' + resp_in.message + '</div>');

                }
//                    setTimeout(function () { $('.messages').html(''); }, 5000);
            }
        });
// ********************************************************************************
         //   var targetDoor = $(this).data('num');
        //    var fromDoor = parseInt($(this).find(':selected').val());
            setTimeout(function () {
                cloneDoors(fromDoor, targetDoor );
            }, 2000);

//****************************************************************************
       // $($(this)).val("0").change();
            $($(this)).val("0").change();
            changeStatusOK();
    }
/*
        var out2 = JSON.stringify(out);
        $('#creaDoors').modal('hide');
        $('.doors .row.door-' + targetDoor + ' #doors_data_' + targetDoor).val(out2);
        $('.doors .row.door-' + targetDoor + ' button').attr('data-valdoor', out2);
        $('.modal-backdrop').remove();
        changeStatusOK();
        */
        return false;

    });

    tmp_count_doors=$('#notNew').val();
    if(tmp_count_doors>0)
    {
        create3d();
    }
    function cloneDoors(fromDoor, targetDoor ) {

        $('.rs-doors.door-'+fromDoor+' .inners-sections .doors-inner').each(function(i,elem) {
            //     var numdoor=$(this).data('numdoor');
            var numsect=$(this).data('numsect');
            var sectInfo = $('textarea#door_sect_data-'+fromDoor+'-'+numsect).html();
            $('textarea#door_sect_data-'+targetDoor+'-'+numsect).html(sectInfo);
            //$('#rs_count_doorsOverlap').find(':selected').val();
            var sectObj=JSON.parse(sectInfo);
            /* out['doors'][numdoor][numsect]={
                'pic':sectObj.pic,
                'name':sectObj.name,
                'height':sectObj.height,
                'position':sectObj.position,
                'connector':sectObj.connector
            };
             */
            // out['doors'][numdoor][numsect]['processing']=[];
            var processing=[];
            if($(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionprocessing[]']").length)
            {
                var decorSelected=$(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionprocessing[]']").find(':selected').val();
                $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] select[name='sectionprocessing[]']").val(decorSelected);

            }
            if($(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] input[name='selectedImage[]']").length )
            {
                if(!$(".rs-doors-sections.doors-inner button#addImage.d-none[data-numdoor="+fromDoor+"][data-numsec="+numsect+"]").length)
                {
                    $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"]  button[id='addImage']").removeClass('d-none');
                    $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] input[name='selectedImage[]']").val(
                        $(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] input[name='selectedImage[]']").val()
                    );
                    $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] div.image").html(
                        $(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] div.image").html()
                    );
                }
            }

            if($(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']").length)
            {
                var decorLakobelSelected=$(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']").find(':selected').val();
                $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']").val(decorLakobelSelected);
                if(!$(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']").hasClass('d-none'))
                {
                    $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']").removeClass('d-none');
                }

            }
            if($(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']").length)
            {
                var decorOracalSelected=$(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']").find(':selected').val();
                $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']").val(decorOracalSelected);
                if(!$(".rs-doors-sections.doors-inner[data-numdoor="+fromDoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']").hasClass('d-none'))
                {
                    $(".rs-doors-sections.doors-inner[data-numdoor="+targetDoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']").removeClass('d-none');
                }
            }
            //  out['doors'][numdoor][numsect]['processing']=Object.assign({},processing);


        });
        changeStatusOK();
        create3d();
    }
    function changeStatusOK() {
        var statusOK=true;
        $('#creaRS .consructions .doors>.rs-doors').each(function (index, value) {
            var doors_data = $('input#doors_data_'+(index+1)).val();

            if(!doors_data) statusOK=false;
        });
        if(statusOK===true){
            $('#creaRS button#save-rs').removeClass('d-none');
            $('#creaRS button#send').removeClass('d-none');
        }
        else{
            $('#creaRS button#save-rs').addClass('d-none');
            $('#creaRS button#send').addClass('d-none');
        }
    }
    function createFullMaterials() {
        var listdata_in=$('#datalistMaterials').html();
        var out = {};
        out['materials'] = JSON.parse(listdata_in);
        // data: {'createFullMaterials': JSON.stringify(out)},

        $.ajax({
            type: 'POST',
            url:'ajax_cresp.php',
                data: {'createFullMaterials': JSON.stringify(out)},
            success: function(data){
                var resp_in =JSON.parse(data);
                console.log(resp_in);
                if(resp_in.result)
                {
                    $('#datalistMaterialsFull').html(JSON.stringify(resp_in.data));
                }
            }
        });
    }

    function create3d()
    {
        var doorsOverlap=$('#rs_count_doorsOverlap').find(':selected').val();
        var count_doors = parseInt(doorsOverlap[0]);
        var overlaps = parseInt(doorsOverlap[2]);
        var rs_size_x = parseInt($('#creaRS #rs_size_x').val());
        var rs_size_y = parseInt($('#creaRS #rs_size_y').val());
        var rs_profile = $("input[name='rs_profile_val']").val();
        var rs_decor = $("input[name='rs_decor_color']").val();
        var empty_door=                {
            profile_vertical:rs_profile,
            profile_top:rs_profile,
            profile_bottom:rs_profile,
            listfill:[{p:0,d:1,texture:"./res/img/990617_1.jpeg",texture_size:200,opacity:0.3}]
        };
        if(isNaN(count_doors)) { count_doors=2; overlaps=1;}

        var state = {
            height:rs_size_y/10, width:rs_size_x/10,/*width можно не указивать, размер будет получен автоматически*/
            K:count_doors,N:overlaps,/*согласно схемы конструкции */
            background : "",
            /*background :"./res/img/b1c94eea13a05e3c9b4b824695ea57f9.jpg"background - фон ,*/
            profile_color:rs_decor /*profile_color - цвет профилей*/,
            rails:{top:rs_profile,bottom:rs_profile}/*top,bottom направляющие профиля*/,
            doors:[]
        };
        for (var i = 0; i < count_doors; i++) {
            if($("div").is(".doors .door-"+(i+1)))
            {
                    var door_tmp={
                        profile_vertical:rs_profile,
                        profile_top:rs_profile,
                        profile_bottom:rs_profile,
                        listfill:[]
                    };
                    var secID=0;
                    var countSec=$(".doors .door-"+(i+1)+" .rs-doors-sections.doors-inner").length;
                $(".doors .door-"+(i+1)+" .rs-doors-sections.doors-inner ").each(function(t,elem) {
                    // var secIn=$("textarea#door_sect_data-1-0").val(); var as= $.parseJSON(secIn); console.log(as);
                     var secIn=$("textarea#door_sect_data-"+(i+1)+"-"+secID).val();
                    var section = $.parseJSON(secIn);
                    console.log(section);
                        var textures = "./res/img/990617_1.jpeg";
                        var pic = section.pic;
                        var opacityin=0.3;
                        var textureSize=200;
                        if(pic!== null){
                            var textures = pic;
                            //var textures = "../../../pic/"+pic;
                            var opacityin=1;
                            var textureSize=0;
                        }

                        var tmp_connector = "";
                         if(section.connector===1) var tmp_connector = rs_profile;
                    var tmp_sect={p:section.position,
                        d:(i+1),
                        texture:textures,
                        profile_connector:tmp_connector,
                        texture_size:textureSize,opacity:opacityin};
                    door_tmp.listfill[countSec-(secID+1)] = Object.assign({},tmp_sect);
                    secID++;
                });

                state.doors[i] = Object.assign({},door_tmp);
            }
            else
            {
                state.doors[i] = Object.assign({},empty_door);
            }
        }
        // console.log(state);
        // console.log(JSON.stringify(state));

        // var canvas = document.getElementById("canvas");
        // var shkaf3d = new Shkaf3D(canvas);// создаем класс визуализации шкафа купе
        // shkaf3d.enable_drawing();// включить отрисовку, на случай если окно canvas будет не активне, надо обязательно отключить, вызов shkaf3d.enable_drawing();

        if (shkaf3d === undefined) {
            var canvas = document.getElementById("canvas");
            shkaf3d = new Shkaf3D(canvas);
        }
        shkaf3d.setState(state);//задаем состояния шкафа, передаем объект со свойствами, при изменении состояния повторно вызываем

    }
    function readForm()
    {

        var out={};
        out['doors']= [];
        out['user_place'] = parseInt($('#creaRS select#select_places').find(':selected').val());
        out['rs_size_x']= $("#creaRS #rs_size_x").val();
        out['rs_size_y']= $("#creaRS #rs_size_y").val();
        out['rs_profile'] = $("#creaRS #rs_profile").val();
        out['rs_profile_val'] = $("#creaRS #rs_profile_val").val();
        out['profileDecors'] = $("#creaRS #profileDecors").val();
        out['rs_decor_color'] = $("#creaRS #rs_decor_color").val();
        out['profileSchetka'] = $("#creaRS #profileSchetka").val();
        out['profileDovodchik'] = $("#creaRS #profileDovodchik").val();
        out['countDovodchik'] = $("#creaRS #countDovodchik").val();
        out['profileRolick'] = $("#creaRS #profileRolick").val();
        out['profileStopor'] = $("#creaRS #profileStopor").val();
        out['rs_count_doorsOverlap'] = $("#creaRS #rs_count_doorsOverlap").val();
        // $('#sendData #datalm').val($("#creaRS #datalistMaterials").val());
        // $('#sendData #datalmf').val($("#creaRS #datalistMaterialsFull").val());
        out['datalm'] = JSON.parse($("#creaRS #datalistMaterials").val());
        out['datalmf'] = JSON.parse($("#creaRS #datalistMaterialsFull").val());
        out['doors_data']=[];
        $coun_doors=parseInt(out['rs_count_doorsOverlap'][0]);
        for(i=1;i<=$coun_doors;i++)
        {
            out['doors'][i]= [];
            out['doors_data'][i]=JSON.parse($("#creaRS input#doors_data_"+i).val());
            //    out['Filling']['door-'+i]=$("#creaRS .rs-doors.door-"+i+" .inners-sections").html();
            $('.rs-doors.door-'+i+' .inners-sections .doors-inner').each(function(i,elem) {
                var numdoor=$(this).data('numdoor');
                var numsect=$(this).data('numsect');
                var sectInfo = $('textarea#door_sect_data-'+numdoor+'-'+numsect).html();
                //$('#rs_count_doorsOverlap').find(':selected').val();
                var sectObj=JSON.parse(sectInfo);
                out['doors'][numdoor][numsect]={
                    'pic':sectObj.pic,
                    'name':sectObj.name,
                    'height':sectObj.height,
                    'position':sectObj.position,
                    'connector':sectObj.connector,
                    'width_door': $('#width_door_' + numsect).val()
                };
                console.log(out['doors'][numdoor][numsect]);    
                // out['doors'][numdoor][numsect]['processing']=[];
                var processing=[];
                if($(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] select[name='sectionprocessing[]']").length)
                {
                    var decorSelect=$(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] select[name='sectionprocessing[]']");
                    processing['op']=decorSelect.data('op');
                    processing['selected']=decorSelect.find(':selected').val();
                }
                if($(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] input[name='selectedImage[]']").length)
                {
                    processing['image']=$(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] input[name='selectedImage[]']").val();
                }
                if($(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']").length)
                {
                    var decorLakobel=$(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] select[name='sectionlakobel[]']");
                    processing['lakobel']=decorLakobel.find(':selected').val();
                }
                if($(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']").length)
                {
                    var decorOracal=$(".rs-doors-sections.doors-inner[data-numdoor="+numdoor+"][data-numsect="+numsect+"] select[name='sectionoracal[]']");
                    processing['oracal']=decorOracal.find(':selected').val();
                }
                out['doors'][numdoor][numsect]['processing']=Object.assign({},processing);

            });
        }
        return out;
    }
});