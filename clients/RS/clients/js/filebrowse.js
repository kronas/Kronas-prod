(function ($) {


    $(document).ready(function () {
        $(document).on('click', '.base-browse .file-browses  li', function (event, sel) {

            var fbtarget = $(this).data('fbtarget');
            var fbbase = $(this).data('fbbase');
            var numDoor = $(this).data('fbnumdoor');
            var numSec = $(this).data('fbnumsec');
            console.log('fbtarget='+fbtarget+'&fbbase='+fbbase+'&numDoor='+numDoor+'&numSec='+numSec);
            $.ajax({
                type: 'POST',
                url:'ajax_cresp.php',
                data:'fbtarget='+fbtarget+'&fbbase='+fbbase+'&numDoor='+numDoor+'&numSec='+numSec,
                success: function (data, textStatus) {
                    var resp = jQuery.parseJSON(data);
                    $('.base-browse').html(resp.data);

                },    error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

        });
        $(document).on('click', '.base-browse img', function (event, sel) {

            var numDoor = $(this).data('fbnumdoor');
            var numSec = $(this).data('fbnumsec');
            var imgsrc = $(this).attr('src');
            var tagImage = '<img src="'+imgsrc+'" class="rounded m-2" height="100px">';
            $('.rs-doors-sections[data-numsect="'+numSec+'"][data-numdoor="'+numDoor+'"] input[name="selectedImage[]"]').val(imgsrc);
            $('.rs-doors-sections[data-numsect="'+numSec+'"][data-numdoor="'+numDoor+'"] div.image').html(tagImage);
            $('#creaDoors').modal('hide');
            $('.modal-backdrop.fade.show').remove();
        });
        $(document).on('click', '#creaDoors #picSave', function (event, sel) {

            var numDoor = $('input[name="picnumDoor"]').val();
            var numSec = $('input[name="picnumSec"]').val();
            var imgsrc = $('input[name="picSelVal"]').val();
            var tagImage = '<img src="'+imgsrc+'" class="rounded m-2" height="100px">';
            $('.rs-doors-sections[data-numsect="'+numSec+'"][data-numdoor="'+numDoor+'"] input[name="selectedImage[]"]').val(imgsrc);
            $('.rs-doors-sections[data-numsect="'+numSec+'"][data-numdoor="'+numDoor+'"] div.image').html(tagImage);
            $('#creaDoors').modal('hide');
            $('.modal-backdrop.fade.show').remove();
        });
    });


})(jQuery);
