function Deg2Rad(in_deg){
	return in_deg * (Math.PI/180);
}


function Door(shkaf3d,data)
{
	this.data = data;	
	this.shkaf3d = shkaf3d;	
	this.object_wapper = new THREE.Object3D();
	this.object = new THREE.Object3D();
	this.object.add(this.object_wapper);
	
	let object_nill = new THREE.Mesh(new THREE.BoxGeometry(data.width,data.height, 1),new THREE.MeshLambertMaterial({transparent:true,opacity:0}));
	
	this.object_wapper.add(object_nill);
	
    var profile_material =  new THREE.MeshLambertMaterial( { color:data.profile_color || 0x666666 } );
	
	if (data.profile_vertical)
	{
		let R = this.shkaf3d.resources.profiles.vertical[data.profile_vertical].mesh.clone();
		R.material = profile_material;
		
		R.scale.set(1,1,data.height);
			
		let L = R.clone();
	
		R.rotation.x = Deg2Rad(90);
		R.position.x = data.width / 2;
		R.position.z = 1;
		L.rotation.x = Deg2Rad(90);
		L.rotation.y = Deg2Rad(180);
		L.position.x = -data.width / 2;	
		L.position.z = 1;
		this.object_wapper.add(R);
		this.object_wapper.add(L);
		
	}
	
	
	
	if (data.profile_top)
	{
		var object = this.shkaf3d.resources.profiles.top[data.profile_top].mesh.clone();
		object.material = profile_material;
		object.scale.set(1,1,data.width);
		object.rotation.y = Deg2Rad(90);
		object.rotation.x = Deg2Rad(180);
		object.position.y = (data.height / 2)-1;
		object.position.z = 1;
		this.object_wapper.add(object);	
	}
	
	if (data.profile_bottom)
	{
		var object = this.shkaf3d.resources.profiles.bottom[data.profile_bottom].mesh.clone();
		object.material = profile_material;
		object.scale.set(1,1,data.width);
		object.rotation.y = Deg2Rad(-90);
		object.rotation.x = Deg2Rad(180);
		object.position.y = -(data.height / 2);
		object.position.z = 1;
		this.object_wapper.add(object);	
	}
	
	if (data.listfill)	
	{

		var fill_h = data.height;
		var fill_w = data.width;

		var doorfill = new THREE.Object3D();
		
		
		
		var listfill = data.listfill.concat([{p:100}]);
		
		for (var i = 1, l = listfill.length; i < l; i++)
		{
			let item1 = listfill[i-1];
			let item2 = listfill[i];	
	
			let v1 = ((1 / 100) * item1.p) * fill_h;
			let v2 = ((1 / 100) * item2.p) * fill_h;
			let w = fill_w;
			let h = v2 - v1;
			let geometry = new THREE.BoxGeometry(w,h, item1.d);
			
			
			
			if (item1.texture)
			{
			
				var texture = new THREE.TextureLoader().load(item1.texture);
			
				texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
			
				var  material =  new THREE.MeshPhongMaterial({map: texture,transparent: item1.opacity !== undefined, opacity: item1.opacity || 1});	
			}else
			{
				material =  new THREE.MeshPhongMaterial({color: item1.color || 0xffffff,transparent: item1.opacity !== undefined, opacity: item1.opacity || 1});	
			}
			
			let Plane = new THREE.Mesh(geometry,material);
			Plane.position.y = v1 + ((v2 - v1)/2);
			Plane.position.z = 0.8;
			doorfill.add(Plane);
			
			
			if (item1.texture_size)
			{
		
				
				let size = new THREE.Box3().setFromObject(Plane);
			
	
				var scale = 1 / item1.texture_size;
				
				
				
				
				
				geometry.faceVertexUvs[0].forEach(function(v){
						for (let i = 0; i < 3; i++)
						{
							v[i].x = (v[i].x * ((size.max.x - size.min.x) * scale));	
							v[i].y = (v[i].y * ((size.max.y - size.min.y) * scale));	
						}	
					});
				geometry.uvsNeedUpdate = true;
			}}


		for (var i = 1, l = listfill.length - 1; i < l; i++)
		{
			let item = listfill[i];	
			let v = ((1 / 100) * item.p) * fill_h;

			if (item.profile_connector)
			{
				
				let object = this.shkaf3d.resources.profiles.connector[item.profile_connector].mesh.clone();
				
				
				object.material = profile_material;
			
			
				object.scale.set(1,1,data.width);
				object.rotation.y = Deg2Rad(90);
				object.rotation.x = Deg2Rad(90);
				object.position.y = v;
				object.position.z = 0.1;
			
				doorfill.add(object);
			}
		}	
		doorfill.position.y = -fill_h/2;
		this.object_wapper.add(doorfill);
	}
	
	
	

	
}


Door.prototype.open  = function(value)
{
	this.object_wapper.position.x = ((1 / 100) * value) * (this.data.openSide ? this.data.width : -this.data.width);	
}



function Doors(shkaf3d,data)
{
	this.shkaf3d = shkaf3d;
	this.data = data;
	this.object = new THREE.Object3D();	
	this.list = [];

	var schemas = [
		{z:[false,true],openSide:[false,true]},
		{z:[false,true,false,true],openSide:[false,true,false,true]},
		{z:[false,true,true,false],openSide:[false,true,true,false]},
		{z:[false,true,false,true,false],openSide:[false,true,false,true,false]},
		{z:[false,true,false,true,false,true],openSide:[false,true,false,true,false,true]},
		{z:[false,true,true,false,true],openSide:[false,true,true,false,true]},
		{z:[false,true,true,false,true,false],openSide:[false,true,true,false,true,false]},
		{z:[false,true,false],openSide:[false,true,false]},
		{z:[false,true,false,true,false],openSide:[false,true,false,true,false]},
		{z:[false,true,false,true,false,true],openSide:[false,true,false,true,false,true]},

	];

	console.log(data);

	if (data.N == 1 && data.K == 2)
	{
		var schema = schemas[0];  	
	}else
	if (data.N == 2 && data.K == 3)
	{
		schema = schemas[7];
	}else
	if (data.N == 3 && data.K == 4)
	{
		schema = schemas[1];  	
	}else
	if (data.N == 2 && data.K == 4)
	{
		schema = schemas[2];  	
	}else if (data.N == 3 && data.K == 5)
	{
		schema = schemas[3];  	
	}else if (data.N == 3 && data.K == 6)
	{
		schema = schemas[4];  	
	}else if (data.N == 2 && data.K == 5)
	{
		schema = schemas[5];  	
	}else if (data.N == 2 && data.K == 6)
	{
		schema = schemas[6];  	
	}else if (data.N == 4 && data.K == 5)
	{
		schema = schemas[8];
	}else if (data.N == 5 && data.K == 6)
	{
		schema = schemas[9];
	}
	
	

	var offset = 0;

	for (var i = 0, l = data.K; i < l; i++)
	{
	
		var datadoor = data.doors[i] || {};
		
		datadoor.openSide =   schema.openSide[i];
		if (!datadoor.profile_color && data.profile_color)  
		datadoor.profile_color = data.profile_color;
    
		if (!datadoor.height)datadoor.height = data.height;
		if (!datadoor.width)datadoor.width =  data.width / data.K;	
		var door = new Door(this.shkaf3d,datadoor);
		door.object.position.x = offset + (datadoor.width/2);
		
		if (datadoor.open)door.open(datadoor.open);
		
		offset += datadoor.width;
    
		door.object.position.z =   schema.z[i] ? 0 : -3.8;
		this.list.push(door);
		this.object.add(door.object);

	}
	
	this.object.position.x -= offset / 2;
}



function Rails(shkaf3d,data)
{
	this.object = new THREE.Object3D();	
	var material =  new THREE.MeshPhongMaterial( { color:data.profile_color || 0x666666  } );

	var b = new THREE.Box3().setFromObject(shkaf3d.doors.object);
	var w = Math.max(b.max.x - b.min.x,data.width || 0);
	

	
	if (data.rails.top)
	{
		
		let object = shkaf3d.resources.profiles.rail_top[data.rails.top].mesh.clone();
		
		object.scale.set(1,1,w);
		object.rotation.y = Deg2Rad(90);
		object.position.y = (data.height/2)-1.5;
		object.position.x =  b.min.x + (w/2);
		object.material = material;
		this.object.add(object);	
	}
	
	
	
	if (data.rails.bottom)
	{
		let object = shkaf3d.resources.profiles.rail_bottom[data.rails.bottom].mesh.clone();
		object.scale.set(1,1,w);
		object.rotation.y = Deg2Rad(90);
		object.position.y = -(data.height/2)-1.0;
		object.position.x = b.min.x + (w/2);
		object.material = material;
		this.object.add(object);	
	}
}




function Box(shkaf3d,data)
{
	var s = 1;
	var d = 50;


	var b = new THREE.Box3().setFromObject(shkaf3d.rails.object);
	
	
	
	this.object = new THREE.Object3D();	


	var material =  new THREE.MeshPhongMaterial( { color:"#668899" } );

	var panel_top = new THREE.Mesh(new THREE.BoxGeometry(b.max.x - b.min.x + 2,s,d),material);
	
	panel_top.position.y = b.max.y +(s/2);
	
	
	panel_top.position.z = b.max.z -(d / 2) + 1;
	this.object.add(panel_top);
	
	
	var panel_bottom = panel_top.clone();
	panel_bottom.position.y = b.min.y -(s/2);
	
	this.object.add(panel_bottom);	
	
	var panel_left = new THREE.Mesh(new THREE.BoxGeometry(s,b.max.y - b.min.y,d),material);
	
	panel_left.position.y = b.max.y -((b.max.y - b.min.y)/2);
	panel_left.position.x = b.min.x -(s/2); 
	    
	panel_left.position.z = b.max.z -(d / 2) + 1;
	    
	this.object.add(panel_left);
	    
	    
	var panel_right = panel_left.clone();
	panel_right.position.x = b.max.x +(s/2); 
	    
	this.object.add(panel_right);
	
	
	
	
}


function Shkaf3D(canvas)
{
	 
	
	this.resources =  {profiles:{"vertical":{"Slider_Air":{"src":"./res/obj/profiles/vertical/Slider_Air.obj"},"Slider_Avenue":{"src":"./res/obj/profiles/vertical/Slider_Avenue.obj"},"Slider_Castle":{"src":"./res/obj/profiles/vertical/Slider_Castle.obj"},"Slider_Expert":{"src":"./res/obj/profiles/vertical/Slider_Expert.obj"},"Slider_Fusion":{"src":"./res/obj/profiles/vertical/Slider_Fusion.obj"},"Slider_Hard":{"src":"./res/obj/profiles/vertical/Slider_Hard.obj"},"Slider_Line":{"src":"./res/obj/profiles/vertical/Slider_Line.obj"},"Slider_Modus":{"src":"./res/obj/profiles/vertical/Slider_Modus.obj"},"Slider_Premium":{"src":"./res/obj/profiles/vertical/Slider_Premium.obj"},"Slider_Project":{"src":"./res/obj/profiles/vertical/Slider_Project.obj"},"Slider_Quadro":{"src":"./res/obj/profiles/vertical/Slider_Quadro.obj"}},"top":{"Slider_Air":{"src":"./res/obj/profiles/top/Slider_Air.obj"},"Slider_Avenue":{"src":"./res/obj/profiles/top/Slider_Avenue.obj"},"Slider_Castle":{"src":"./res/obj/profiles/top/Slider_Castle.obj"},"Slider_Expert":{"src":"./res/obj/profiles/top/Slider_Expert.obj"},"Slider_Fusion":{"src":"./res/obj/profiles/top/Slider_Fusion.obj"},"Slider_Hard":{"src":"./res/obj/profiles/top/Slider_Hard.obj"},"Slider_Line":{"src":"./res/obj/profiles/top/Slider_Line.obj"},"Slider_Modus":{"src":"./res/obj/profiles/top/Slider_Modus.obj"},"Slider_Premium":{"src":"./res/obj/profiles/top/Slider_Premium.obj"},"Slider_Project":{"src":"./res/obj/profiles/top/Slider_Project.obj"},"Slider_Quadro":{"src":"./res/obj/profiles/top/Slider_Quadro.obj"}},"bottom":{"Slider_Air":{"src":"./res/obj/profiles/bottom/Slider_Air.obj"},"Slider_Avenue":{"src":"./res/obj/profiles/bottom/Slider_Avenue.obj"},"Slider_Castle":{"src":"./res/obj/profiles/bottom/Slider_Castle.obj"},"Slider_Expert":{"src":"./res/obj/profiles/bottom/Slider_Expert.obj"},"Slider_Fusion":{"src":"./res/obj/profiles/bottom/Slider_Fusion.obj"},"Slider_Hard":{"src":"./res/obj/profiles/bottom/Slider_Hard.obj"},"Slider_Line":{"src":"./res/obj/profiles/bottom/Slider_Line.obj"},"Slider_Modus":{"src":"./res/obj/profiles/bottom/Slider_Modus.obj"},"Slider_Premium":{"src":"./res/obj/profiles/bottom/Slider_Premium.obj"},"Slider_Project":{"src":"./res/obj/profiles/bottom/Slider_Project.obj"},"Slider_Quadro":{"src":"./res/obj/profiles/bottom/Slider_Quadro.obj"}},"rail_top":{"Slider_Air":{"src":"./res/obj/profiles/rail_top/Slider_Air.obj"},"Slider_Avenue":{"src":"./res/obj/profiles/rail_top/Slider_Avenue.obj"},"Slider_Castle":{"src":"./res/obj/profiles/rail_top/Slider_Castle.obj"},"Slider_Expert":{"src":"./res/obj/profiles/rail_top/Slider_Expert.obj"},"Slider_Fusion":{"src":"./res/obj/profiles/rail_top/Slider_Fusion.obj"},"Slider_Hard":{"src":"./res/obj/profiles/rail_top/Slider_Hard.obj"},"Slider_Line":{"src":"./res/obj/profiles/rail_top/Slider_Line.obj"},"Slider_Modus":{"src":"./res/obj/profiles/rail_top/Slider_Modus.obj"},"Slider_Premium":{"src":"./res/obj/profiles/rail_top/Slider_Premium.obj"},"Slider_Project":{"src":"./res/obj/profiles/rail_top/Slider_Project.obj"},"Slider_Quadro":{"src":"./res/obj/profiles/rail_top/Slider_Quadro.obj"}},"rail_bottom":{"Slider_Air":{"src":"./res/obj/profiles/rail_bottom/Slider_Air.obj"},"Slider_Avenue":{"src":"./res/obj/profiles/rail_bottom/Slider_Avenue.obj"},"Slider_Castle":{"src":"./res/obj/profiles/rail_bottom/Slider_Castle.obj"},"Slider_Expert":{"src":"./res/obj/profiles/rail_bottom/Slider_Expert.obj"},"Slider_Fusion":{"src":"./res/obj/profiles/rail_bottom/Slider_Fusion.obj"},"Slider_Hard":{"src":"./res/obj/profiles/rail_bottom/Slider_Hard.obj"},"Slider_Line":{"src":"./res/obj/profiles/rail_bottom/Slider_Line.obj"},"Slider_Modus":{"src":"./res/obj/profiles/rail_bottom/Slider_Modus.obj"},"Slider_Premium":{"src":"./res/obj/profiles/rail_bottom/Slider_Premium.obj"},"Slider_Project":{"src":"./res/obj/profiles/rail_bottom/Slider_Project.obj"},"Slider_Quadro":{"src":"./res/obj/profiles/rail_bottom/Slider_Quadro.obj"}},"connector":{"Slider_Air":{"src":"./res/obj/profiles/connector/Slider_Air.obj"},"Slider_Avenue":{"src":"./res/obj/profiles/connector/Slider_Avenue.obj"},"Slider_Castle":{"src":"./res/obj/profiles/connector/Slider_Castle.obj"},"Slider_Expert":{"src":"./res/obj/profiles/connector/Slider_Expert.obj"},"Slider_Fusion":{"src":"./res/obj/profiles/connector/Slider_Fusion.obj"},"Slider_Hard":{"src":"./res/obj/profiles/connector/Slider_Hard.obj"},"Slider_Line":{"src":"./res/obj/profiles/connector/Slider_Line.obj"},"Slider_Modus":{"src":"./res/obj/profiles/connector/Slider_Modus.obj"},"Slider_Premium":{"src":"./res/obj/profiles/connector/Slider_Premium.obj"},"Slider_Project":{"src":"./res/obj/profiles/connector/Slider_Project.obj"},"Slider_Quadro":{"src":"./res/obj/profiles/connector/Slider_Quadro.obj"}}}};
	
	this.isLoading = true;
	
	this.canvas = canvas; 
	
	this.scene = new THREE.Scene();
	
	this.camera = new THREE.PerspectiveCamera( 35, this.canvas.width / this.canvas.height, 0.1, 10000 );
	this.camera.position.z = 500.0;
	this.camera.lookAt(new THREE.Vector3(0, 0, 0));	
	
	
	this.renderer = new THREE.WebGLRenderer({ antialias: true,canvas:canvas});
	this.renderer.setSize(this.canvas.width,this.canvas.height);
	this.renderer.setClearColor( 0xFFFFFF, 1);
	this.renderer.setPixelRatio(window.devicePixelRatio);
	
	this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
	
	this.light = new THREE.DirectionalLight(0xFFFFFF, 1);
	this.scene.add(this.light);	
	

	

    

	this.loader = new THREE.OBJLoader();
    
    
	for (var key in this.resources.profiles)
	{
		let profiles = this.resources.profiles[key];	
    
    

		for (var key2 in profiles)
		{
			let profile = profiles[key2];
	
	
			this.loader.load(profile.src,
				function (object) {
	
					object.traverse( function ( child ) {
							if (child instanceof THREE.Mesh ) {
								child.geometry.computeVertexNormals( true );
								profile.mesh = child.clone();
							}}.bind(this));
		
				}.bind(this)); 
		}
    
	}
    
	
	
	
	
	this.loader.manager.onLoad = function(){
		
		if (this.isLoading)	
		{
			this.isLoading = false;	
			if (this.old_state) this.setState(this.old_state);	
		}
	}.bind(this);
	
 
}



Shkaf3D.prototype.setState = function(state)
{
	if(state.background) this.scene.background = new THREE.TextureLoader().load(state.background); 
	
	if (!this.isLoading)
	{
		if (this.shkaf)	this.scene.remove(this.shkaf);
		    this.shkaf  = new THREE.Object3D();	
		    this.scene.add(this.shkaf);
		
		
		this.doors = new Doors(this,state);
		this.shkaf.add( this.doors.object);
	
		
		this.rails = new Rails(this,state);
		this.shkaf.add( this.rails.object);
		
		
		var b = new THREE.Box3().setFromObject(this.shkaf);//фокусируем камеру на объект
		var r = Math.max(b.max.x - b.min.x,b.max.y - b.min.y)/2; 
	    var h = r / Math.tan( this.camera.fov / 2 * Math.PI / 180 );
        this.camera.position.z = h * 1.2;


	}
	this.old_state =  state;
}


Shkaf3D.prototype.WindowResize = function()
{
	this.renderer.setSize(this.canvas.width,this.canvas.height);
	this.camera.aspect = this.canvas.width / this.canvas.height;
	this.camera.updateProjectionMatrix();
}






Shkaf3D.prototype.render = function()
{
	this.light.position.copy(this.camera.position ); 
	
	this.renderer.render(this.scene, this.camera);		
	
	
}

Shkaf3D.prototype.enable_drawing = function()
{
	if (!this.is_drawing)
	{	
		this.is_drawing = true;	
		var UpDate = function ()
		{
			if (this.is_drawing)
			{
				try 
				{
					this.render();	
				} catch(error) {}
				window.requestAnimationFrame(UpDate);	
			}	
		}.bind(this);
		
		UpDate();
	
	}	
}


Shkaf3D.prototype.disable_drawing = function()
{
	this.is_drawing = false;	
}

