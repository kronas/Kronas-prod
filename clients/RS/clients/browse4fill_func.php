<?php



function browse4fill_get_folders($where, $mode, &$ret=array(),&$active=0)
{
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'browse4fill_get_folders start', $_SESSION);
    $_add_where=" ";
    if(count($where)==1 && $where[0]==0 && $mode==0) $_add_where=" AND GOOD_TREE.`NAME` IN ('Плитные материалы', 'Стекло', 'Зеркало')";
    // if(count($where)==1 && $where[0]==0 && $mode==0) $_add_where=" AND GOOD_TREE.`NAME` IN ('Плитные материалы', 'Стекло', 'Зеркало')";
    if(count($where)==3 && in_array(973,$where) && in_array(2694,$where) && in_array(2694,$where))
    {
        $where=array(2694,2694);
        $_add_where=" OR (GOOD_TREE.PARENT=973 AND GOOD_TREE.`NAME` IN ('Декоративная плита', 'ЛДСП', 'МДФ', 'Фанера клеенная', 'Материал клиента', 'Стеновая панель') )";
    }
    $sql = "SELECT
GOOD_TREE.FOLDER_ID,
GOOD_TREE.`NAME`,
GOOD_TREE.PARENT,
GOOD_TREE.1CCODE,
GOOD_TREE.`CODE`
FROM
GOOD_TREE
WHERE
GOOD_TREE.PARENT IN (".implode(',',$where).") ".$_add_where;

    $result=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    $out='<ul class="list-group mode-'.$mode.'">'; $is_first=1;
    foreach($result as $row) {
        $disp_children='';$is_active='';
        if($mode==0){ $ret[]=$row['FOLDER_ID']; if($is_first) { $is_active=' active '; $active=$row['FOLDER_ID'];}} else {
            if($active!=$row['PARENT']) $disp_children=' style="display:none;" ';

        }

        $out .= '<li class="list-group-item '.$is_active.'" '.
            ($mode==0?' data-children="'.$row['FOLDER_ID'].'" ':' data-target="'.$row['FOLDER_ID'].'"  data-parent="'.$row['PARENT'].'" '.$disp_children.' ').
            '>'.$row['NAME'].'</li>';
        $is_first=0;
    }
    $out .= "</ul>";
    $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'browse4fill_get_folders end', $_SESSION);
    return $out;
}

function browse4fill_create_screen($where, $type)
{
    $out='';
    $tablerows= al2b_db_get_list_data_properties($type, $where);

    $arr_prop_ids=array();
    $arr_prop_name=array();
    $arr_exclude=array('Артикул Акцент','RS','тип кромки клиента','Тип','Тип покрытия','Статус товара');
    foreach ($tablerows as $row)
    {
        if(!in_array(trim($row['NAME']),$arr_exclude)) {
            $arr_prop_ids[$row['PROPERTY_ID']][$row['VALUESTR'] . $row['VALUEDIG']] = $row['VALUESTR'] . $row['VALUEDIG'];
            $arr_prop_name[$row['PROPERTY_ID']] = array('NAME' => $row['NAME'], 'DESCRIPTION' => $row['DESCRIPTION']);
        }
    }
    $out.='<div class="accordion" id="accordionExample">';

    foreach ($arr_prop_ids as $key=>$vsl) {

        $out.= '<div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                        <button class="btn btn-secondary btn-sm btn-block" type="button" data-toggle="collapse" data-target="#clps-'.$key.'" aria-expanded="false" aria-controls="clps-'.$key.'">
                        '.$arr_prop_name[$key]['NAME'].'
                        </button>
                      </h2>
                    </div>
                
                    <div id="clps-'.$key.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body" style="max-height: 80vh; overflow: scroll;">';
        $counter=0;
        foreach ($vsl as $items) {
            $out.= '<div class="form-check">
                  <input class="form-check-input" type="checkbox" value="'.$items.'" id="check-'.$key.'-'.$counter.'">
                  <label class="form-check-label" for="check-'.$key.'-'.$counter.'">'.$items.'</label></div>';
            $counter++;
        }
        $out.= '</div>
            </div>
          </div>';
    }
    $out.= '</div>';
    return $out;
}

