<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $ptcorrect='';
    ?>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <?php
    if(isset($theCHLD) && $theCHLD==1) {
        $ptcorrect='../';
        echo '<script src="js/three.min.js"></script>';
        echo '<script src="js/OBJLoader.js"></script>';
        echo '<script src="js/OrbitControls.js"></script>';
        echo '<script src="js/shkaf3d.js?v6"></script>';


        echo '<script src="js/custom.js"></script>';
  //      echo '<script src="js/3dtest.js"></script>';
        echo '<script src="'.$ptcorrect.'js/modal.js"></script>';
        if(isset($filebrowse) && $filebrowse==1) {
            echo '<script src="js/filebrowse.js"></script>';

        }
    }
    elseif(isset($theCHLD) && $theCHLD==2) {
        $ptcorrect='../';
        echo '<script src="'.$ptcorrect.'js/custom.js"></script>';
        echo '<script src="'.$ptcorrect.'js/modal.js"></script>';
        echo '<script src="test.js"></script>';
    }
    elseif(isset($theCHLD) && $theCHLD==3) {
        $ptcorrect='../';
        echo '<script src="'.$ptcorrect.'js/custom.js"></script>';
        echo '<script src="'.$ptcorrect.'js/modal.js"></script>';
        echo '<script src="js/filebrowse.js"></script>';
    }
    elseif(isset($theCHLD) && $theCHLD==4) {
        echo '<script src="js/custom.js"></script>';
        echo '<script src="js/jquery.mask.min.js"></script>';
    }
    else
    {
        echo '<script src="'.$ptcorrect.'js/custom.js"></script>';
        echo '<script src="'.$ptcorrect.'js/modal.js"></script>';
    }
?>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <?php if(isset($theCHLD) && $theCHLD==4) { ?>
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <?php }else{ ?>
    <link href="<?=$ptcorrect?>../fontawesome/css/all.css" rel="stylesheet">
    <?php } ?>
    <link href="<?=$ptcorrect?>css/custom.css" rel="stylesheet">
    <?php if(isset($title)) echo "<title>$title</title>"; ?>
</head>
<body>
<div id="start_checks_lodaer_containers" class="d-none">
    <div>
        <img src="<?=$ptcorrect?>images/download.gif" alt="">
    </div>
</div>
<div class="container-fluid">
<div class="row">
    <?php if(!isset($_GET['typeop']) && !isset($toolbarclose)){ ?>
    <div class="col-sm-12 text-right">
        <?php echo toolbar($_SESSION, '../'.$ptcorrect); ?>
    </div>
    <?php } ?>
    <?php if(isset($title)) echo "<h1 class=\"text-center col-12 mb-3\">$title</h1>"; ?>
    <?php echo messages($_SESSION,"col-md-4 offset-md-4 col-sm-12"); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Выберите клиента:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="new-order-form">
                <?php // Полушин. Тут ссылка генерируется из /RS/js/modal.js ?>
                <form action="<?= $main_dir.'?nw='.$_GET['nw']?>" method="GET" id="sac_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Клиент:</label>
                            <ul class="nav nav-tabs" id="myTab1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-selected="true">Имя</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="false">Телефон</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent1">
                                <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
                                    <input type="text" class="form-control" id="filter_client_name_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                                </div>
                                <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                                    <input type="text" class="form-control" id="filter_client_phone_auth" placeholder="введите имя контрагента" name="filter_client_name" <?php if(isset($_GET['filter_client_name'])) echo 'value="'.$_GET['filter_client_name'].'"' ?>>
                                </div>
                            </div>
                            <div id="client_variants_auth">
                                <ul class="list-group">

                                </ul>
                            </div>


                            <input type="hidden" name="client_id" id="filter_client_auth" value="">
                            <input type="hidden" name="manager_id" id="filter_manager_auth" value="<?= $_SESSION['manager_id'] ?>">
                            <input type="hidden" name="client_code" id="filter_client_code_auth" value="">
                            <input type="hidden" name="set_new_order" value="1">
                        </div>
                        <div class="form-group" id="client_auth_message">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="client_auth_form_button">Создать заказ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>