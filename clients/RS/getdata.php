<?php
session_start();
include_once('../functions2.php');
include_once('functions3.php');
$theCHLD=4;
include_once('header.php');
$res=array();
$profile_id=0;
if(isset($_GET) && $_GET['typeop'] && $_GET['editID'] && (int)$_GET['typeop']==2 && (int)$_GET['editID']>0)
{
    $profile_id=(int)$_GET['editID'];
    $res=db_get_profile_full($profile_id);
}
?>
<div class="profile-setting">
<input type="hidden" name="profile_id" value="<?=$profile_id?>">
<div class="row">
    <div class="form-group m-1">
        <label for="new_profile">Название</label>
        <input type="text" class="form-control" id="new_profile" name="newProfile" value="<?=count($res)>0?$res['name']:''?>">
    </div>
    <div class="form-group m-1">
        <label for="new_profile_wcr">Нахлест</label>
        <input type="text" class="form-control" id="new_profile_wcr" name="newProfileWCR"  value="<?=count($res)>0?$res['width_cross']:''?>">
    </div>
    <div class="form-group m-1">
        <?php
        $_arr_profile_type=array('Slider_Air', 'Slider_Avenue', 'Slider_Castle', 'Slider_Expert', 'Slider_Fusion',
            'Slider_Hard', 'Slider_Line', 'Slider_Line_S', 'Slider_Modus', 'Slider_Premium', 'Slider_Project', 'Slider_Quadro');
        ?>
        <label for="new_profile_type">Тип профиля</label>
        <select class="custom-select m-1" name="new_profile_type" >
            <?php
foreach ($_arr_profile_type as $value)
{
    echo '<option value="'.$value.'" '.($res['profile_type']==$value?'selected':'').'>'.$value.'</option>';
}
            ?>
        </select>
    </div>
</div>
<div class="row new-profile">
    <div class="col-md-2">
        <div class="form-group" >
            <label for="width_l_sheet">Коэффициент ширины заполнения для плитных</label>
            <input type = "text" class="form-control" id = "width_l_sheet" name = "width_l_sheet" value="<?=count($res)>0?$res['width_l_sheet']:''?>" >
        </div >
    </div>
    <div class="col-md-2">
        <div class="form-group" >
            <label for="height_w_sheet">Коэффициент высоты заполнения для плитных</label>
            <input type = "text" class="form-control" id = "height_w_sheet" name = "height_w_sheet" value="<?=count($res)>0?$res['height_w_sheet']:''?>" >
        </div >
    </div>
    <div class="col-md-2">
        <div class="form-group" >
            <label for="wdth_l_glass">Коэффициент ширины заполнения для стекла/зеркала</label>
            <input type = "text" class="form-control" id = "wdth_l_glass" name = "wdth_l_glass" value="<?=count($res)>0?$res['wdth_l_glass']:''?>" >
        </div >
    </div>
    <div class="col-md-2">
        <div class="form-group" >
            <label for="height_w_glass">Коэффициент высоты заполнения для стекла/зеркала</label>
            <input type = "text" class="form-control" id = "height_w_glass" name = "height_w_glass" value="<?=count($res)>0?$res['height_w_glass']:''?>" >
        </div >
    </div>
    <div class="col-md-2">
        <div class="form-group" >
            <label for="height_w_glass">Коэффициент высоты двери</label>
            <input type = "text" class="form-control" id = "koef_h_door" name = "koef_h_door" value="<?=count($res)>0?$res['koef_h_door']:''?>" >
        </div >
    </div>
    <div class="col-md-2">
        <div class="form-group" >
            <label for="height_w_glass">Коэффициент длины горизонтальных профилей</label>
            <input type = "text" class="form-control" id = "koef_l_hor_pofile" name = "koef_l_hor_pofile" value="<?=count($res)>0?$res['koef_l_hor_pofile']:''?>" >
        </div >
    </div>
</div>

<div class="row new-profile border border-secondary">
    <h6 class="w-100 pl-2 pt-2">Отступ для фрезерования ДСП</h6>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="indent_l">Лево</label>
            <input type = "text" class="form-control" id = "indent_l" name = "indent_l" value="<?=count($res)>0?$res['profile_fr_l']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="indent_r">Право</label>
            <input type = "text" class="form-control" id = "indent_r" name = "indent_r" value="<?=count($res)>0?$res['profile_fr_r']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="indent_t">Верх</label>
            <input type = "text" class="form-control" id = "indent_t" name = "indent_t" value="<?=count($res)>0?$res['profile_fr_t']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="indent_b">Низ</label>
            <input type = "text" class="form-control" id = "indent_b" name = "indent_b" value="<?=count($res)>0?$res['profile_fr_b']:''?>" >
        </div >
    </div>
</div>

<div class="row new-profile">
    <div class="col-md-4">
        <div class="form-group" >
            <label for="vert">Вертикальный</label>
            <input type = "text" class="form-control" id = "vert" name = "vert" value="<?=count($res)>0?$res['vert']:''?>" >
        </div >
    </div>
    <div class="col-md-4">
        <div class="form-group" >
            <label for="hor_up">Горизонтальный верхний</label>
            <input type = "text" class="form-control" id = "hor_up" name = "hor_up" value="<?=count($res)>0?$res['hor_up']:''?>" >
        </div >
    </div>
    <div class="col-md-4">
        <div class="form-group" >
            <label for="hor_down">Горизонтальный нижний</label>
            <input type = "text" class="form-control" id = "hor_down" name = "hor_down" value="<?=count($res)>0?$res['hor_down']:''?>" >
        </div >
    </div>
    <div class="col-md-4">
        <div class="form-group" >
            <label for="napr_up">Направляющий верхний</label>
            <input type = "text" class="form-control" id = "napr_up" name = "napr_up" value="<?=count($res)>0?$res['napr_up']:''?>" >
        </div >
    </div>
    <div class="col-md-4">
        <div class="form-group" >
            <label for="napr_down">Направляющий нижний</label>
            <input type = "text" class="form-control" id = "napr_down" name = "napr_down" value="<?=count($res)>0?$res['napr_down']:''?>" >
        </div >
    </div>
    <div class="col-md-4">
        <div class="form-group" >
            <label for="connection">Соединительный</label>
            <input type = "text" class="form-control" id = "connection" name = "connection" value="<?=count($res)>0?$res['connection']:''?>" >
        </div >
    </div>
</div>
<div class="row new-profile border border-secondary mb-2">
    <div class="col-md-3">
        <div class="form-group" >
            <label for="max_door_height">Максимальная высота проема</label>
            <input type = "text" class="form-control" id = "max_door_height" name = "max_door_height" value="<?=count($res)>0?$res['max_door_height']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="max_door_length">Максимальная длина двери</label>
            <input type = "text" class="form-control" id = "max_door_length" name = "max_door_length" value="<?=count($res)>0?$res['max_door_width']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="max_wdth_in">Максимальная ширина проёма</label>
            <input type = "text" class="form-control" id = "max_wdth_in" name = "max_wdth_in" value="<?=count($res)>0?$res['max_door_length']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="min_door_height">Минимальная высота проема</label>
            <input type = "text" class="form-control" id = "min_door_height" name = "min_door_height" value="<?=count($res)>0?$res['min_door_height']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="min_door_length">Минимальная длина двери</label>
            <input type = "text" class="form-control" id = "min_door_length" name = "min_door_length" value="<?=count($res)>0?$res['min_door_width']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="con_sheet_glass">Высота соединения стекло/зеркало с плитой</label>
            <input type = "text" class="form-control" id = "con_sheet_glass" name = "con_sheet_glass" value="<?=count($res)>0?$res['con_sheet_gl']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="con_sheet_sheet">Высота соединения плиты с плитой</label>
            <input type = "text" class="form-control" id = "con_sheet_sheet" name = "con_sheet_sheet" value="<?=count($res)>0?$res['con_sheet_sheet']:''?>" >
        </div >
    </div>
    <div class="col-md-3">
        <div class="form-group" >
            <label for="con_glass_glass">Высота соединения стекло со стеклом</label>
            <input type = "text" class="form-control" id = "con_glass_glass" name = "con_glass_glass" value="<?=count($res)>0?$res['con_mir_gl']:''?>" >
        </div >
    </div>
</div>
    <div class="col-12 messages">
    </div>
<div class="col-md-12 text-right">
<button type="submit" class="btn btn-success" <?=$profile_id==0?"disabled='disabled'":"";?> value="save" id="saveProfile">Сохранить</button>
</div>
</div>


<?php include_once('footer.php'); ?>

<script>
    /*
    function list4searchProfile(sid, stringval, objectName) {
        $("input[name='"+objectName+"']").val(stringval);
        $("input[name='"+objectName+"_val']").val(sid);
        // console.log(s);
        //$("."+objectName+" input").val('ss');
        $("."+objectName+" .searchProfile-box").hide();
        $("."+objectName+" .loading-search").hide();
        profile_form_ready_copy();
    }
    */

    function profile_form_ready_copy() {
        if( parseFloat($('input#width_l_sheet').val())>0 &&
            parseFloat($('input#height_w_sheet').val())>0 &&
            parseFloat($('input#wdth_l_glass').val())>0 &&
            parseFloat($('input#height_w_glass').val())>0 &&
            $('input#vert').val().length>0 &&
            $('input#hor_up').val().length>0 &&
            $('input#hor_down').val().length>0 &&
            $('input#napr_up').val().length>0 &&
            $('input#napr_down').val().length>0 &&
            $('input#connection').val().length>0 &&
            $('input[name="newProfile"]').val().length>3 &&
            parseFloat($('input[name="newProfileWCR"]').val())>0 &&
            parseFloat($('input#max_door_height').val())>0 &&
            parseFloat($('input#max_door_length').val())>0 &&
            parseFloat($('input#max_wdth_in').val())>0 &&
            parseFloat($('input#con_sheet_glass').val())>0 &&
            parseFloat($('input#con_sheet_sheet').val())>0 &&
            parseFloat($('input#con_glass_glass').val())>0 &&
            parseFloat($('input#min_door_height').val())>0 &&
            parseFloat($('input#min_door_length').val())>0)
        {
            $("button#saveProfile").prop( "disabled", false );
        }else
        {
            $("button#saveProfile").prop( "disabled", true );
        }


    }
</script>
