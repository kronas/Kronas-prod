<?php
session_start();
include_once('functions2.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
}
$def_filterDate=array("all"=>'', "1"=>'', "7"=>'', "30"=>'', "365"=>'');
$def_filterStatus=array("all"=>'', "в работе"=>'', "выполнен"=>'', "отменен"=>'');
$def_filterType=array("all"=>'', "1"=>'', "5"=>'', "7"=>'');

$total_records = 0;
$filtr = array();
$_url = 'orders3.php?';
//$filtr[] = ' CLIENT= "'.$_SESSION['user']['code'].'" ';
$filtr[] = ' CLIENT= "164180" ';    $_SESSION['messages']['success'][]='для теста - юзаем пользователя 164180!';
if(isset($_GET['order_filter_date']) && key_exists($_GET['order_filter_date'], $def_filterDate) && $_GET['order_filter_date']!='all')
{
    $def_filterDate[$_GET['order_filter_date']]=' selected ';
    $post_date = new DateTime();
    $new_date = date_sub($post_date, date_interval_create_from_date_string($_GET['order_filter_date'].' days'));
    $post_date = date_format($new_date,"Y-m-d H:i:s");
    $filtr[] = ' DB_AC_IN between "'.$post_date.'" and "'.date("Y-m-d H:i:s").'" ';
}
else $def_filterDate["all"]=' selected ';

if(isset($_GET['order_filter_status']) && key_exists($_GET['order_filter_status'], $def_filterStatus) && $_GET['order_filter_status']!='all')
{
    $def_filterStatus[$_GET['order_filter_status']]=' selected ';
    $filtr[] = ' status = "'.$_GET['order_filter_status'].'" ';
}
else $def_filterStatus["all"]=' selected ';

if(isset($_GET['order_filter_type']) && key_exists($_GET['order_filter_type'], $def_filterType) && $_GET['order_filter_type']!='all')
{
    $def_filterType[$_GET['order_filter_type']]=' selected ';
    switch ($_GET['order_filter_type'])
    {
        case 1:
            $filtr[] = ' (SELECT COUNT(*) FROM PART WHERE ORDER1C = o.ID) >0 ';
            break;
        case 5:
            $filtr[] = ' (SELECT COUNT(*) FROM PART WHERE ORDER1C = o.ID) =0 ';
            break;
        case 7:
            $filtr[] = ' o.rs_order=1 ';
            break;
    }
}
else $def_filterType["all"]=' selected ';
?>


<?php if($_GET['order_s']):
    $title='Спецификация деталей для заказа #'.base64_decode($_GET['order_name']);
    include_once('header.php');

    $parts = db_get_parts(base64_decode($_GET['order_s']));
    ?>
    <div class="row">
        <div class="col-12 pl-3 pr-3">
            <a class="btn btn-danger" href="<?= $main_dir ?>/clients/orders.php?back=1">
                Назад
            </a>
            <br><br><br>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Ширина</th>
                    <th>Длина</th>
                    <th>Толщина</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($parts as $part): ?>
                    <tr>
                        <td><?= $part['PART_ID'] ?></td>
                        <td><?= $part['NAME'] ?></td>
                        <td><?= $part['DESCRIPTION'] ?></td>
                        <td><?= $part['W'].' мм' ?></td>
                        <td><?= $part['L'].' мм' ?></td>
                        <td><?= $part['T'].' мм' ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>


<?php elseif($_GET['order_r']):
    $title='Счет для заказа #'.base64_decode($_GET['order_name']);
    include_once('header.php');

    $parts = db_get_receipt(base64_decode($_GET['order_r']));

    $total = 0;

//    print_r_($parts);
//    exit;
    ?>
    <div class="row">
        <div class="col-12 pl-3 pr-3">
            <a class="btn btn-danger" href="<?= $main_dir ?>/clients/orders.php?back=1">
                Назад
            </a>
            <br><br><br>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Стоимость</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($parts as $part): ?>
                    <?php $total += $part['price']; ?>
                    <tr>
                        <td><?= $part['id'] ?></td>
                        <td><?= $part['name'] ?></td>
                        <td><?= $part['type'] ?></td>
                        <td><?= $part['count'] ?></td>
                        <td><?= number_format($part['cost'], 2, ',', ' '); ?></td>
                        <td><?= number_format(round($part['price'], 2), 2, ',', ' '); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4" class="text-right">
                        <h3>Всего: </h3>
                    </td>
                    <td>
                        <h3> <?= number_format(round($total, 2), 2, ',', ' '); ?></h3>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

<?php else: ?>




<?php

$title='Заказы';
include_once('header.php');
?>
    <div class="row  align-items-center">
        <?php echo messages($_SESSION, "col-md-4 offset-md-4 col-sm-12"); ?>
        <?php
        // db_materials_stock_upgate(227822); /
        ?>

    </div>
<?php

$limit = 25;

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$start = $page == 1 ? 0 : (($limit * ($page - 1)));
// $table = db_get_listclients($filtr, $start, $limit, $total_records);
$orders = db_get_orders2($filtr, $start, $limit, $total_records);
db_get_orders_add($orders);
echo '<div>
    <a href="'.$laravel_dir.'/new_project_from_account" class="btn btn-success">+ Новый заказ</a>
    <hr>
</div>';

echo '<form action="'.$main_dir.'/clients/orders3.php'.'?nw='.$_GET['nw'].'" method="GET">
    <div class="row">
        <div class="form-group col-md-3">
            <label for="order_filter_date">Период:</label>
            <select id="order_filter_date" name="order_filter_date" class="form-control">
                <option value="all" '.$def_filterDate['all'].'>все</option>
                <option value="1" '.$def_filterDate['1'].'>За сегодняшний день</option>
                <option value="7" '.$def_filterDate['7'].'>За прошлую неделю</option>
                <option value="30" '.$def_filterDate['30'].'>За прошлых 30 дней</option>
                <option value="365" '.$def_filterDate['365'].'>За весь год</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="order_filter_status">Статус:</label>
            <select id="order_filter_status" name="order_filter_status" class="form-control">
                <option value="all"      '.$def_filterStatus['all'].'>все</option>
                <option value="в работе" '.$def_filterStatus['в работе'].'>в работе</option>
                <option value="выполнен" '.$def_filterStatus['выполнен'].'>выполнен</option>
                <option value="отменен"  '.$def_filterStatus['отменен'].'>отменен</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="order_filter_type">Тип:</label>
            <select id="order_filter_type" name="order_filter_type" class="form-control">
                <option value="all" '.$def_filterType['all'].' >все</option>
                <option value="1" '.$def_filterType['1'].' >Пильный</option>
                <option value="5" '.$def_filterType['5'].' >Обычный</option>
                <option value="7" '.$def_filterType['7'].' >Раздвижная система</option>
            </select>
        </div>
        <div class="form-group col-md-3 confirm_filters_button">
            <label for="">&nbsp;&nbsp;&nbsp;</label>
            <button class="btn btn-success">Фильтровать</button>
        </div>
    </div>
</form>';


if (is_null($orders)) {
    echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div>';
} else {

    echo '<div class="row"><div class="col-12 pl-3 pr-3">';
    echo '<table class="table table-striped"><thead>
            <tr>
                    <th>Дата создания</th>
                    <th>Номер заказа</th>
                    <th>Контрагент</th>
                    <th>Участок</th>
                    <th>Менеджер</th>
                    <th>Статус</th>
                    <th>Действия</th>
            </tr>
            </thead><tbody>';
    ?>
    <?php
    $tmp=0;
    foreach ($orders as $order):
        ?>
        <tr>
            <td><?php echo $order['DB_AC_IN'] ? $order['DB_AC_IN'] : 'Неизвестно' ?></td>
            <td><?= $order['DB_AC_ID'].' / ('.$order['DB_AC_NUM'].')'; ?></td>
            <td><?= $order['client_name'] ?></td>
            <td><?= $order['NAME'] ?></td>

            <td>
                <span><?= $order['name'] ?></span><br>
                <small>email: <a href="mailto:<?= $order['e-mail'] ?>"><?= $order['e-mail'] ?></a></small><br>
                <small>телефон: <?php echo $order['phone'] ? $order['phone'] : '+38(044)390-00-07'; ?></small><br>

            </td>
            <td><?= $order['status'] ?></td>
            <td class="table_actions_container">
                <?php if(isset($order['MY_PROJECT_OUT'])):?>
                    <a href="#" title="PDF" class="order_project_pdf_<?= $order['ID'] ?>" data-project=""><i class="fas fa-file-pdf"></i></a>
                    <?php if($_SESSION['user']['role'] == 'admin'): ?>
                        <a href="<?= $main_dir ?>/refresh_project_programm.php?project=<?= $order['ID'] ?>" class="refresh_buttons_programm_<?= $order['ID'] ?>" target="_blank" title="Обновить программы заказа" target="_blank">
                            <i class="fas fa-sync-alt"></i>
                        </a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if($order['parts'] > 0):?>
                    <a href="orders3.php?order_s=<?= base64_encode($order['ID']) ?>&order_name=<?= base64_encode($order['DB_AC_ID'].' / ('.$order['DB_AC_NUM'].')'); ?>" target="_blank" title="Спецификация"><i class="fas fa-list"></i></a>
                <?php endif; ?>
                <a href="orders3.php?order_r=<?= base64_encode($order['ID']) ?>&order_name=<?= base64_encode($order['DB_AC_ID'].' / ('.$order['DB_AC_NUM'].')'); ?>" target="_blank" title="Счет"><i class="fas fa-file-invoice-dollar"></i></a>

                <?php $order['status'] == 1 ? $order_status = '2_' : $order_status = '1_';  ?>
                <?php if($order['parts'] > 0):?>
                    <div style="display: flex; position: relative; text-align: left;">
                        <a id="show_link_container_<?=$order['ID']?>">
                            <i class="fas fa-external-link-alt" title="Редактировать этот проект"></i>
                        </a>
                        <span class="edit_link_variant" id="link_container_<?=$order['ID']?>">
                                    <?php foreach ($order['edit_links'] as $link): ?>
                                        <a href="<?= $laravel_dir ?>/projects_new/<?= base64_encode($order_status.$order['ID']).'?order_project_id='.$link['link'] ?>" target="_blank" ><?= $link['author'] ?> от <?= $link['date'] ?></a>
                                    <?php endforeach; ?>
                                </span>

                    </div>
                <?php endif; ?>
                <script>
                    $('.order_project_pdf_<?= $order['ID'] ?>').on('click', async function(e) {
                        e.preventDefault();
                        let formData = new FormData();
                        formData.append('project_data', `<?= base64_decode($order['MY_PROJECT_OUT']['MY_PROJECT_OUT']) ?>`);
                        formData.append('action', 'get_all_pdf_manager' );
                        formData.append('is_admin', 'false' );

                        let request = await fetch('<?= $main_dir ?>/pdf_send.php', {
                            method: 'POST',
                            headers: {
                                ///'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            body: formData
                        });
                        let body = await request.text();


                        if(body.indexOf('.pdf') > -1){
                            window.open('<?= $main_dir ?>/pdf_send.php?show=' + body);
                        }
                    });
                    $('.refresh_buttons_programm_<?= $order['ID'] ?>').on('click', async function (e) {
                        e.preventDefault();

                        $('#start_checks_lodaer_containers').show();

                        let url = $(e.target).closest('a').attr('href');

                        let req_refresh = await fetch(url, {
                                method: 'GET',
                            }
                        );
                        let req_body = await req_refresh.text();
                        console.log(req_body.trim());
                        if(req_body.trim() == 'Программы успешно обновлены!') {
                            $(e.target).closest('a').html(`
                                            <i class="fa fa-check-circle" aria-hidden="true" style="color:green!important;"></i>
                                        `);
                            $(e.target).closest('a').attr('href', '');
                        }
                        $('#start_checks_lodaer_containers').hide();
                    });
                    $(document).ready(function () {
                        $('#show_link_container_<?=$order['ID']?> > *').on('click', async function () {
                            $('#link_container_<?=$order['ID']?>').toggleClass('open');
                        });
                    });
                </script>
                <?php
                if($order['rs_order']==1) echo '<a href="RS/clients/create.php?order_project_id='.$order['ID'].'" target="_blank" title="Проект"><i class="fas fa-external-link-alt text-dark"></i></a>';
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php
    echo '</tbody></table>';

    if ($total_records > $limit) echo get_list2browse_pager2($_url, $page, $limit, $total_records);
    echo '</div></div>';
}
?>
<?php endif; ?>

<div id="start_checks_lodaer_containers" style="display: none;">
    <div>
        <img src="<?= $laravel_dir ?>/images/download.gif" alt="">
    </div>
</div>

<?php include_once('footer.php'); ?>


