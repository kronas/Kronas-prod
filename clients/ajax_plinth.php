<?php

include_once 'functions2.php';
// массивы для экранирования текстовых запросов от sql инъекций
$search = ['\\', "\0", "\n", "\r", "'", '"', "\x1a"];
$replace = ['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'];
// Подгружает варианты свойств по выбраному селекту
if (isset($_POST['type']) && isset($_POST['prop_name'])) {
    $response = [
        'result' => mat_get_property_value_options($_POST['prop_name'], $_POST['type'], 'plinth'),
    ];
    echo json_encode($response);
}
// выводит дерево католога товаров
if (isset($_POST['load_tree']) && $_POST['load_tree']) {
    $tree = mat_get_good_tree(1849);
    $tree = mat_form_tree($tree);
    echo mat_build_tree($tree, 2026, 'plinth', 2026);
    die;
}

// дерево каталога
if (isset($_POST['parent_id'])) {
    $response = [];
    $parent_id = intval($_POST['parent_id']);
    $tree = mat_get_good_tree($parent_id);
    $tree = mat_form_tree($tree);
    $response = [
        'error' => false,
        'result' => mat_build_tree($tree, $parent_id, 'plinth', 2026),
    ];
    echo json_encode($response);
    die;
}

// загружает таблицу плинтусов по умолчанию
if (isset($_POST['load_table'])) {
    $response = [
        'error' => false,
        'result' => plinth_get_plinth_table(plinth_get_plinth_default()),
        'message' => '',
    ];
    echo json_encode($response);
    die;
}

// фильтр выборки плинтусов
if (isset($_POST['filter_plinth']) && $_POST['filter_plinth']) {
    $response = [];
    // делает выборку кромки по дереву
    if (isset($_POST['folder_id'])) {
        $folder_id = intval($_POST['folder_id']);
        $parent_id = intval($_POST['parent']);
        $plinths = plinth_get_plinth_by_tree($folder_id, $parent_id);
        $message = 'Нет плинтусов в этой категории';
    }
    // делает выборку кромки по свойствам
    if (isset($_POST['properties'])) {
        $properties = json_decode($_POST['properties'], true);
        $plinths = plinth_get_plinth_by_properties($properties);
    }
    // делает выборку по коду
    if (isset($_POST['code'])) {
        $code = intval($_POST['code']);
        $plinths = plinth_get_plinth_by_code($code);
        $message = "Нет плинтуса с таким кодом: $code";
    }
    // делает выборку по названию
    if (isset($_POST['name'])) {
        // от sql инъекций
        $name = str_replace($search, $replace, $_POST['name']);
        $plinths = plinth_get_plinth_by_name($name);
        $name_out = htmlspecialchars($_POST['name']);
        $message = "Нет плинтуса с таким наименованием: $name_out";
    }
    if (!empty($plinths)) {
        $response = [
            'error' => false,
            'result' => plinth_get_plinth_table($plinths),
            'message' => '',
        ];
    } else {
        $response = [
            'error' => true,
            'result' => null,
            'message' => $message,
        ];
    }
    echo json_encode($response);
    die;
}

// загружает связь материалов с кромкой
if (isset($_POST['load_plinth_materials']) && $_POST['load_plinth_materials']) {
    $response = [];
    $plinth_id = intval($_POST['plinth_id']);
    $materials = plinth_get_plinth_mat_by_plinth_id($plinth_id);
    if (!empty($materials)) {
        $response = [
            'error' => false,
            'result' => good_get_relations_mat_table($materials, 'plinth'),
            'message' => '',
        ];
    } else {
        $response = [
            'error' => true,
            'result' => null,
            'message' => 'Ошибка сервера!',
        ];
    }
    echo json_encode($response);
    die;
}

// загружает список найденых материалов, если они найдены
if (isset($_POST['filter_mat'])) {
    $response = [];
    $filter_mat = str_replace($search, $replace, $_POST['filter_mat']);
    $materials = band_get_materials_by_filter($filter_mat, true);
    if (!empty($materials) && is_array($materials)) {
        $response = [
            'error' => false,
            'result' => band_get_list_of_materials($materials),
            'message' => "<p class='h5 text-primary'>количество найденных строк - " . count($materials) . '</p>',
        ];
    } else {
        $response = [
            'error' => true,
            'message' => 'Ничего не найдено',
        ];
    }
    echo json_encode($response);
    die;
}

// обновляет связь материалов с кромкой
if (isset($_POST['update_plinth_mat']) && $_POST['update_plinth_mat']) {
    $response = [];
    $plinth_ids = json_decode($_POST['plinths'], true);
    $materials = json_decode($_POST['materials'], true);
    $plinths = plinth_update_relations_with_mat($plinth_ids, $materials);
    if (!empty($plinths)) {
        if (is_array($plinths)) {
            $response = [
                'error' => false,
                'result' => plinth_get_plinth_table($plinths),
                'message' => '',
            ];
        } else {
            $response = [
                'result' => '',
                'error' => true,
                'message' => 'getting not array ~\|/~',
            ];
        }
    } else {
        $response = [
            'result' => '',
            'error' => true,
            'message' => '',
        ];
    }
    echo json_encode($response);
    die;
}

// deletes the relation between the plinth and the material
if (isset($_POST['del_relation']) && $_POST['del_relation']) {
    $sql = "DELETE FROM MATERIAL_SIMPLE_CONNECTION WHERE  MATERIAL_ID={$_POST['mat_id']} AND SIMPLE_ID={$_POST['plinth_id']}";
    if (sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)) {
        echo 'success';
        die;
    } else {
        echo 'error';
        die;
    }
}



