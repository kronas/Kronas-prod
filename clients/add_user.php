<?php
session_start();
include_once('functions2.php');
if(!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][]='Авторизуйтесь пожалуйста!';
header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
exit();
} elseif($_SESSION['user']['role']!='admin' && $_SESSION['user']['role']!='manager'){
    $_SESSION['messages']['errors'][]='У вас не прав!';
header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
exit();
}
if(isset($_POST) && isset($_POST['submit']))
{
    add_client($_POST);
    $_SESSION['messages']['success'][]='Клиент добавлен!';
}
?>
<?php include_once('header.php'); ?>
<div class="row">
    <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
        <h2>Добавить клиента</h2>
        <form method="post" class="add-user">

                    <div class="form-group">
                        <label for="new_mail" class="label-required">E-mail</label>
                        <input type="email" class="form-control required" id="new_mail" name="new_mail" data-valid="0" required="required" placeholder="name@example.com">
                        <small id="mailHelp" class="form-text text-muted"></small>
                    </div>
                    <div class="form-group">
                        <label for="new_phone" class="label-required">Телефон</label>
                        <input type="text" class="form-control phone" id="new_phone" name="new_phone" data-valid="0" required="required"  data-mask="+380 (00) 000 00 00" placeholder="+380 (__) ___ __ __" aria-describedby="phoneHelp">
                        <small id="phoneHelp" class="form-text text-muted"></small>
                    </div>
                    <div class="form-group input-name" style="display: none;">
                        <label for="new_name">Имя</label>
                        <input type="text" class="form-control" id="new_name" name="new_name" required="required">
                    </div>
                        <div class="form-group input-password" style="display: none;">
                            <label for="new_password">Пароль</label>
                            <input type="password" class="form-control" id="new_password" name="new_password" required="required">
                        </div>
                        <div class="form-group input-password" style="display: none;">
                            <label for="new_password2">Повторите пароль</label>
                            <input type="password" class="form-control" id="new_password2" name="new_password2" required="required">
                            <small id="passwordHelp" class="form-text text-muted"></small>
                        </div>
            <button style="display: none;" type="submit" name="submit" class="btn btn-success" disabled>Добавить</button>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>