<?php
session_start();
include_once('functions2.php');
include_once('func_api.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
} elseif ($_SESSION['user']['role'] != 'admin' && $_SESSION['user']['role'] != 'manager') {
    $_SESSION['messages']['errors'][] = 'У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}
function _add_whereOnDay($dayCount, $placeID,&$_where)
{

    $_where[] = ' stc.Place=' . $placeID;
    $_where[] = ' (SELECT MAX(DOC.Datetime) FROM MATERIAL_KL_STOCK_MOVE  AS stm 
INNER JOIN DOC ON DOC.d_id = stm.Doc 
WHERE stm.Material_client=stc.Material_client AND 
stm.Place=stc.Place
)<=' . (time() - ($dayCount * 86400)); // .(time()-($dayCount*86400)))
}


$setting=load_ini();
$user_in_production = (int) $setting['misc']['user_in_production'];
$user_in_fasart_1 = (int) $setting['misc']['user_in_fasart_1'];
$user_in_fasart_2 = (int) $setting['misc']['user_in_fasart_2'];
$user_in_fasart_3 = (int) $setting['misc']['user_in_fasart_3'];
$user_in_fasart_4 = (int) $setting['misc']['user_in_fasart_4'];
$placeID = 0;
$numMat = 0;
$check_inprod = 0;
$dayCount=0;
$without_in_production=0;
$dataOut = array();

if (isset($_POST['dataser'])) $dataOut = unserialize($_POST['dataser']);

if (isset($_POST['select_places'])) $placeID = (int)$_POST['select_places'];
if (isset($_POST['dayCount'])) $dayCount = (int)$_POST['dayCount'];
if (isset($_POST['numMat'])) $numMat = (int)$_POST['numMat'];
if (isset($_POST['check_inprod']) && $_POST['check_inprod']=='on') $check_inprod = 1;
$limit = 25;
$total_records = 0;
$start_in = 1;
$_where = array();
if (isset($_POST['pager'])) $start_in = $_POST['pager'];
if (isset($_POST['limit'])) $limit = $_POST['limit'];
if (isset($_POST['onpage'])) {
    $start_in = 1;
    $limit = $_POST['onpage'];
}
    if (isset($_POST['user_actives']) ) $user_actives = (int) $_POST['user_actives']; else $user_actives=0;
    if (isset($_POST['client_filtr_w']) && (float)$_POST['client_filtr_w']>0) $_where[] = ' mtc.W=' . (float)$_POST['client_filtr_w'];
    if (isset($_POST['client_filtr_l']) && (float)$_POST['client_filtr_l']>0) $_where[] = ' mtc.L=' . (float)$_POST['client_filtr_l'];
    if (isset($_POST['client_filtr_t']) && (float)$_POST['client_filtr_t']>0) $_where[] = ' mtc.T=' . (float)$_POST['client_filtr_t'];
    if (isset($_POST['client_filtr_name']) && strlen(trim($_POST['client_filtr_name']))>0) $_where[] = ' cln.`name` LIKE "%' .trim($_POST['client_filtr_name']).'%" ';
    if (isset($_POST['without_in_production']) && $_POST['without_in_production']==1) {
        $without_in_production = 1;
        $_where[] = ' stc.Client<>' . $user_in_production;
    }
    if (isset($_POST['without_in_fasart']) && $_POST['without_in_fasart']==1) {
        $without_in_fasart = 1;
        $_where[] = ' stc.Client<>' . $user_in_fasart_1;
        $_where[] = ' stc.Client<>' . $user_in_fasart_2;
        $_where[] = ' stc.Client<>' . $user_in_fasart_3;
        $_where[] = ' stc.Client<>' . $user_in_fasart_4;
    }


if($dayCount>0 && $placeID>0) _add_whereOnDay($dayCount, $placeID,$_where);

if (isset($_POST['countLine']) && $_POST['countLine'] > 0) {
    for ($i = 1; $i <= $_POST['countLine']; $i++) {

        $dataOut[$_POST['client_' . $i]][$_POST['material_' . $i]] = array(
            'return' => $_POST['return_' . $i],
            'cause_ret' => $_POST['cause_ret_' . $i],
            'comment' => $_POST['returncomment_' . $i],
            'days_left' => $_POST['days_left_' . $i],
            'matCode' => $_POST['matCode_' . $i],
            'clientId' => $_POST['client_' . $i],
            'place' => $_POST['place_' . $i],
            'cell' => $_POST['cell_' . $i],
            'old_count' => $_POST['old_count_' . $i],
            'part_ids' => (isset($_POST['part_ids_' . $i])) ? $_POST['part_ids_' . $i] : '',
            'part_ids_checked' => (isset($_POST['part_ids_checked_' . $i])) ? $_POST['part_ids_checked_' . $i] : '',
        );
    }
}
// Списание
if (isset($_POST['submit']) && $_POST['submit'] == 1) {
    // da($_POST);
    $_rez = FALSE;

    foreach ($dataOut as $key_client => $line_client) {
        $out_ref = array();
        $out_time = array();
        $out_inprod1 = array();
        $out_inprod2 = array();
        $comment_ref = '';
        $comment_time = '';
        $comment_innprod1 = '';
        $comment_innprod2 = '';
        foreach ($line_client as $key_material => $line_material) {
            if (count($line_material) > 0 && (int)$line_material['return'] > 0) {
                $placeID_val = $placeID > 0 ? $placeID : $line_material['place'];
                if ($line_material['cause_ret'] == 'refuse') {
                    $out_ref[] = array(
                        'ID' => $key_material,
                        'COUNT' => 0 - (int)$line_material['return'],
                        'clientIdFrom' => $line_material['clientId'],
                        'clientId' => 0,
                        'placesID' => $placeID_val,
                        'cell' => $line_material['cell'],
                        'old_count' => $line_material['old_count'],
                        'part_ids' => $line_material['part_ids'],
                        'part_ids_checked' => $line_material['part_ids_checked']
                    );
                    $comment_ref .= $line_material['matCode'] . ' - ' . $line_material['comment'] . "<br>";
                } elseif ($line_material['cause_ret'] == 'time') {
                    $out_time[] = array(
                        'ID' => $key_material,
                        'COUNT' => 0 - (int)$line_material['return'],
                        'clientIdFrom' => $line_material['clientId'],
                        'clientId' => 0,
                        'placesID' => $placeID_val,
                        'cell' => $line_material['cell'],
                        'old_count' => $line_material['old_count'],
                        'part_ids' => $line_material['part_ids'],
                        'part_ids_checked' => $line_material['part_ids_checked']
                    );
                    $comment_time .= $line_material['matCode'] . " - списание из-за превышения времени хранения. В момент списания хранение {$line_material['days_left']} дней <br>";
                } else {
                    $out_inprod1[] = array(
                        'ID' => $key_material,
                        'COUNT' => 0 - (int)$line_material['return'],
                        'clientIdFrom' => $line_material['clientId'],
                        'clientId' => $user_in_production,
                        'placesID' => $placeID_val,
                        'cell' => $line_material['cell'],
                        'old_count' => $line_material['old_count'],
                        'part_ids' => $line_material['part_ids'],
                        'part_ids_checked' => $line_material['part_ids_checked']
                    );
                    $out_inprod2[] = array(
                        'ID' => $key_material,
                        'COUNT' => (int)$line_material['return'],
                        'clientIdFrom' => $line_material['clientId'],
                        'clientId' => $user_in_production,
                        'placesID' => $placeID_val,
                        'cell' => $line_material['cell'],
                        'old_count' => $line_material['old_count'],
                        'part_ids' => $line_material['part_ids'],
                        'part_ids_checked' => $line_material['part_ids_checked']
                    );
                    $comment_innprod1 .= $line_material['matCode'] . " - В производство <br>";
                    $comment_innprod2 .= $line_material['matCode'] . " - Передача остатка производству <br>";
                }
            }
        }

        $_arrOute = array();
        foreach ($out_ref as $oute) {
            $_arrOute[$oute['placesID']][$oute['ID']] = $oute['COUNT'];
        }
        foreach ($out_time as $oute) {
            $_arrOute[$oute['placesID']][$oute['ID']] = $oute['COUNT'];
        }
        $clientID = $_POST['clientID'];
        $table = db_get_liststockOnClient($key_client);

        foreach ($table as $line) {
            if (isset($_arrOute[$line['PLACES_ID']][$line['Material_client']])) {
                if ($line['Count'] + $_arrOute[$line['PLACES_ID']][$line['Material_client']] < 0) {
                    $_SESSION['messages']['errors'][] = 'Ошибка ввода данных! Отрицательный остаток недопустим!!';
                    header('Location: ' . $main_dir . '/clients/corrects.php'.'?nw='.$_GET['nw']);
                    exit();
                }
            }
        }
        if (count($out_ref) > 0 && ( $check_inprod==1 || $key_client==$user_in_production || $_SESSION['user']['ID']==36)) {
            $_rez = TRUE;
            $Type = 'out';
            $Reason = 'other';
            $Manager = $_SESSION['user']['ID'];
            $Order1C = 0;
            $Comment = $comment_ref;
            $docID = db_create_doc($Type, $key_client, $Reason, $Manager, $Order1C, $Comment);
            $_SESSION['messages']['success'][] = "Документ №$docID списание с производства !";

            db_materials_stock_move_add($out_ref, $docID);

            if($_SESSION['user']['ID']==36) {
                $_SESSION['messages']['success'][] = "Документ №$docID списание (брак)!";
                db_materials_stock_upgate($key_client);
            }
            else {
                $_SESSION['messages']['success'][] = "Документ №$docID списание с производства !";
                db_materials_stock_upgate($user_in_production);
            }

        }
        if (count($out_time) > 0 && ( $check_inprod==1 || $key_client==$user_in_production || $_SESSION['user']['ID']==36)) {
            $_rez = TRUE;
            $Type = 'out';
            $Reason = 'time';
            $Manager = $_SESSION['user']['ID'];
            $Order1C = 0;
            $Comment = $comment_time;
            $docID = db_create_doc($Type, $key_client, $Reason, $Manager, $Order1C, $Comment);
            db_materials_stock_move_add($out_time, $docID);
            if($_SESSION['user']['ID']==36) {
                $_SESSION['messages']['success'][] = "Документ №$docID списание по времени!";
                db_materials_stock_upgate($key_client);
            }
            else {
                $_SESSION['messages']['success'][] = "Документ №$docID списание с производства по времени!";
                db_materials_stock_upgate($user_in_production);
            }


        }
        if (count($out_inprod1) > 0) {
            $_rez = TRUE;
            $Type = 'correct';
            $Reason = 'other';
            $Manager = $_SESSION['user']['ID'];
            $Order1C = 0;
            $Comment = $comment_innprod1;
            $docID = db_create_doc($Type, $key_client, $Reason, $Manager, $Order1C, $Comment);
            $_SESSION['messages']['success'][] = "Документ №$docID списания для клиента №$key_client создан (в производство) !";
            db_materials_stock_move_add($out_inprod1, $docID);
            
            // Отсылка списываемого материала в новый сервис
            preSendWriteOffMaterial($out_inprod1, DIR_LOGS);

            db_materials_stock_upgate($key_client);
            $Comment = $comment_innprod2;
            $docID = db_create_doc($Type, $user_in_production, $Reason, $Manager, $Order1C, $Comment);
            $_SESSION['messages']['success'][] = "Документ №$docID приход от клиента №$key_client в производство !";
            db_materials_stock_move_add($out_inprod2, $docID);
            db_materials_stock_upgate($user_in_production);
            $arr2cell=array();
            foreach ($out_inprod1 as $valueadd)
            {
                $arr2cell[]=array('cell'=>$valueadd['cell'], 'ID'=>$valueadd['ID']);
            }
            db_add_materials2cells($arr2cell,$docID);
        }

    }
    if ($_rez) {
        header('Location: ' . $main_dir . '/clients/write_off.php'.'?nw='.$_GET['nw']);
        exit();
    }
}
// Списание по дням
if (isset($_POST['submit']) && $_POST['submit'] == 3 && (int)$_POST['dayCount'] > 0) {
    $dayCount = (int)$_POST['dayCount'];
    $placeID = (int)$_POST['select_places_auto'];
    _add_whereOnDay($dayCount, $placeID,$_where);

    if ($check_inprod == 1) $_where[]=' cln.client_id='.$user_in_production.' ';
    $_tr = 0;
    $_tmp1ff = get_listWriteOff(1, 1, $_tr, $_where);

    if ($_tr == 0) {
        $_SESSION['messages']['errors'][] = 'Данных нет!';
        header('Location: ' . $main_dir . '/clients/write_off.php'.'?nw='.$_GET['nw']);
        exit();
    }
    $listWriteOff = get_listWriteOff(1, $_tr + 1, $total_records, $_where);
    $dataOut = array();
    if (count($listWriteOff) > 0) {
        foreach ($listWriteOff as $line) {
            $dataOut[$line['Client']][$line['Material_client']] = array(
                'return' => $line['Count'],
                'cause_ret' => 'time',
                'comment' => '',
                'days_left' => floor((time() - $line['maxdate']) / 86400),
                'matCode' => $line['CODE'],
                'cell' => $line['cell'],
            );
        }
    }
    $listWriteOff = get_listWriteOff($start_in, $limit, $total_records, $_where);
}
// выбор склада
if (isset($_POST['submit']) && $_POST['submit'] == 2) {
    if (isset($_POST['select_places'])) $placeID = (int)$_POST['select_places'];
    if ($placeID > 0) $_where[] = ' stc.Place=' . $placeID;
    if ($numMat > 0) $_where[] = ' stc.Material_client=' . $numMat;
    if ($check_inprod == 1) $_where[]=' cln.client_id='.$user_in_production.' ';
    $listWriteOff = get_listWriteOff($start_in, $limit, $total_records, $_where);
        if (count($listWriteOff) == 0) {
            $_SESSION['messages']['errors'][] = 'Данных нет!';
            header('Location: ' . $main_dir . '/clients/write_off.php'.'?nw='.$_GET['nw']);
            exit();
        }
    if (isset($_SESSION['user']['ID'])) $user_actives = (int)$_SESSION['user']['ID'];
}
if(!isset($_POST['submit']) && count($dataOut)>0)
{
    if ($check_inprod == 1) $_where[]=' cln.client_id='.$user_in_production.' ';
    $listWriteOff = get_listWriteOff($start_in, $limit, $total_records, $_where);
}

$title = 'Списание остатков';
include_once('header.php');
?>
<form method="post" class="return write-off" action="write_off.php<?='?nw='.$_GET['nw']?>">
    <?php if ($placeID == 0 && $numMat == 0) { ?>
        <div class="row  align-items-center">

        </div>
        <div class="row ">


            <div class="col-md-6 offset-md-3 col-sm-12">
                <ul class="nav nav-tabs  nav-pills nav-fill" id="writeOff" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active" id="auto-tab" data-toggle="tab" href="#auto" role="tab"
                           aria-controls="auto"
                           aria-selected="true">Автоматическое списание</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="manual-tab" data-toggle="tab" href="#manual" role="tab"
                           aria-controls="manual" aria-selected="false">Ручное списание</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade  show active  pb-5 pt-5" id="auto" role="tabpanel"
                         aria-labelledby="auto-tab">
                        <div class="row  align-items-center"">
                                      <div class="col-md-12 col-sm-12 border">
                            <?= db_get_listPlaces(false, 0, 1) ?>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="form-group row mt-1 ">
                                <label for="dayCount" class="col-sm-6 col-form-label">Кол-во дней</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control the-required" name="dayCount" id="dayCount"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">

                            <button type="submit" name="submit" value="3" class="btn btn-success"><i
                                        class="far fa-save"></i> Выбрать
                            </button>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade pb-5 pt-5" id="manual" role="tabpanel" aria-labelledby="manual-tab">
                    <div class="row  align-items-center">
                    <div class="col-sm-9">
                        <?= db_get_listPlaces(false, 0, 2) ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group  ">
                            <label for="numMat" class="col-form-label">№ материала</label>
                            <input type="text" class="form-control" name="numMat" id="numMat">
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 text-center">
                        <input type="checkbox" class="form-check-input" name="check_inprod" id="check_inprod">
                        <label class="form-check-label" for="check_inprod">Показать все что в производстве</label>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">

                        <button type="submit" name="submit" value="2" class="btn btn-success"><i
                                    class="far fa-save"></i> Выбрать
                        </button>
                    </div>
                </div>
              </div>
        </div>
        </div>
        </div>
    <?php } elseif (count($listWriteOff) > 0) { ?>
        <textarea name="dataser" style="display: none;"><?= serialize($dataOut) ?></textarea>

    <?php if ($_SESSION['user']['role'] == 'manager' || $_SESSION['user']['role'] == 'admin') { ?>
            <div class="row">
                <div class="col-md-1 offset-md-3 col-sm-12">


                    <label for="client_filtr_w" class="mr-4">W остатка</label>
                    <input type="text" class="form-control" id="client_filtr_w" name="client_filtr_w" <?php if (isset($_POST['client_filtr_w'])) echo 'value="'.$_POST['client_filtr_w'].'"'?>>


                </div>
                <div class="col-md-1 col-sm-12">

                    <label for="client_filtr_l" class="mr-4">L остатка</label>
                    <input type="text" class="form-control" id="client_filtr_l" name="client_filtr_l" <?php if (isset($_POST['client_filtr_l'])) echo 'value="'.$_POST['client_filtr_l'].'"'?>>

                </div>
                <div class="col-md-1 col-sm-12">

                    <label for="client_filtr_t" class="mr-4">T остатка</label>
                    <input type="text" class="form-control" id="client_filtr_t" name="client_filtr_t" <?php if (isset($_POST['client_filtr_t'])) echo 'value="'.$_POST['client_filtr_t'].'"'?>>

                </div>
                <?php if ($check_inprod == 0) { ?>
                <div class="col-md-2 col-sm-12">

                    <label for="client_filtr_name" class="mr-4">Клиент</label>
                    <input type="text" class="form-control" id="client_filtr_name" name="client_filtr_name" <?php if (isset($_POST['client_filtr_name'])) echo 'value="'.$_POST['client_filtr_name'].'"'?>>

                </div>
            <?php } ?>
                <div class="col-md-4 col-sm-12">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="without_in_production" value="1" <?php if ($without_in_production==1) echo ' checked '?>>
                        <label class="form-check-label" for="inlineCheckbox1">Без остатков производства</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox11" name="without_in_fasart" value="1" <?php if ($without_in_fasart==1) echo ' checked '?>>
                        <label class="form-check-label" for="inlineCheckbox11">Без остатков клиента «ФасАрт»</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" name="filtr" value="1" class="mt-4 btn btn-success">
                        <i class="fas fa-filter"></i>
                        Фильтровать
                    </button>
                </div>
            </div>
    <?php } ?>
        <div class="row">
            <div class="col-12 pl-3 pr-3 mt-2">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Деталь</th>
                        <th>Код</th>
                        <th>W</th>
                        <th>L</th>
                        <th>T</th>
                        <th>Клиент</th>
                        <th>Дата</th>
                        <th>Дней</th>
                        <th>Количество</th>
                        <th>В списание</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    // $_where=array(' stc.Place='.$placeID);
                    //$listWriteOff=get_listWriteOff($start_in, $limit, $total_records,$_where);
                    $_cnt = 0;

                    foreach ($listWriteOff as $line) {
                        $line['api_part_ids'] = preg_replace('#,+$#', '', $line['api_part_ids']);
                        // if($_cnt==0) _pre($line);
                        $_value = '';
                        $return_opt_style = "display: none;";
                        $returncomment_style = "display: none;";
                        $returncomment_value = '';
                        $cause_ret = array('checked', '', '');

                        if (isset($dataOut[$line['Client']][$line['Material_client']]['return']) && $dataOut[$line['Client']][$line['Material_client']]['return'] > 0) {
                            $_value = $dataOut[$line['Client']][$line['Material_client']]['return'];
                            $return_opt_style = '';
                            if ($dataOut[$line['Client']][$line['Material_client']]['cause_ret'] == 'refuse') {
                                $cause_ret = array('', 'checked', '');
                                $returncomment_style = '';
                                $returncomment_value = $dataOut[$line['Client']][$line['Material_client']]['comment'];
                            }elseif ($dataOut[$line['Client']][$line['Material_client']]['cause_ret'] == 'in_production') {
                                $cause_ret = array('', '', 'checked');
                            }
                        }
                        $_cnt++;

                        if($line['Client']==$user_in_production)
                        {

                            $return_opt_style =$dayCount==0?"display: none;":"";
                            $return_opt_list='
<input class="form-check-input" type="radio" id="crt1' . $_cnt . '" name="cause_ret_' . $_cnt . '" value="time" ' . $cause_ret[0] . '>
<label class="form-check-label mr-2" for="crt1' . $_cnt . '">Время</label></div>
<div class="form-check"><input class="form-check-input" type="radio" id="crt2' . $_cnt . '" name="cause_ret_' . $_cnt . '"  value="refuse" ' . $cause_ret[1] . '>
<label class="form-check-label" for="crt2' . $_cnt . '">Брак</label></div>';
                        }
                        else
                        {

                            $return_opt_style = "display: none;";
                            $returncomment_style = "display: none;";
                            $returncomment_value = '';
                            $return_opt_list='
<input class="form-check-input d-none" type="radio" id="crt3' . $_cnt . '" name="cause_ret_' . $_cnt . '"  value="in_production" checked >
<label class="form-check-label d-none" for="crt3' . $_cnt . '">В производство</label>';
                            if($user_actives == 36 && ((!isset($_POST['dayCount']) || (isset($_POST['dayCount']) && (int)$_POST['dayCount']==0))))
                            {
                                $return_opt_style = "display: none;";
                                $returncomment_style = "display: none;";
                                $returncomment_value = '';
                                $return_opt_list='
<input class="form-check-input" type="radio" id="crt1' . $_cnt . '" name="cause_ret_' . $_cnt . '" value="time" ' . $cause_ret[0] . '>
<label class="form-check-label mr-2" for="crt1' . $_cnt . '">Время</label></div>
<div class="form-check"><input class="form-check-input" type="radio" id="crt2' . $_cnt . '" name="cause_ret_' . $_cnt . '"  value="refuse" ' . $cause_ret[1] . '>
<label class="form-check-label" for="crt2' . $_cnt . '">Брак</label></div>
<div class="form-check"><input class="form-check-input" type="radio" id="crt3' . $_cnt . '" name="cause_ret_' . $_cnt . '"  value="in_production" ' . $cause_ret[2] . ' >
<label class="form-check-label" for="crt3' . $_cnt . '">В производство</label>';
                            }
                        }

                        echo '
    <td>' . $line['Material_client'] . '</td>
    <td>' . $line['mt_name'] . $line['mto_name'] . '</td>
    <td>' . $line['CODE'] .  '</td>
    <td>' . $line['W'] . '</td>
    <td>' . $line['L'] . '</td>
    <td>' . $line['T'] . '</td>
    <td>' . $line['cname'] . '</td>
    <td>' . date("Y/m/d H:i", $line['maxdate']) . '</td>
    <td>' . floor((time() - $line['maxdate']) / 86400) . '</td>
    <td>' . $line['Count'] . '</td><td>
    <div class="form-group mb-0">
    <div class="wrf-cnt">
    <div class="wrf-in">
                        <input type="hidden" name="days_left_' . $_cnt . '" value="' . floor((time() - $line['maxdate']) / 86400) . '" >
                    <input type="hidden" name="matCode_' . $_cnt . '" value="' . $line['CODE'] . $line['Code_mat'] . '" >
                    <input type="hidden" name="material_' . $_cnt . '" value="' . $line['Material_client'] . '" >
                    <input type="hidden" name="client_' . $_cnt . '" value="' . $line['Client'] . '" >
                    <input type="hidden" name="place_' . $_cnt . '" value="' . $line['Place'] . '" >
                    <input type="hidden" name="cell_' . $_cnt . '" value="' . $line['cell'] . '" >
                    <input type="hidden" name="old_count_' . $_cnt . '" value="' . $line['Count']. '" >
                    <input type="hidden" name="client_name_' . $_cnt . '" value="' . $line['cname'] . '" >
                    <input type="hidden" name="mat_name_' . $_cnt . '" value="' . $line['mt_name'] . '" >
                    <input type="hidden" name="part_ids_' . $_cnt . '" value="' . $line['api_part_ids'] . '" >
                    <input type="hidden" name="part_ids_checked_' . $_cnt . '" value="" >
                        <input value="' . $_value . '" type = "text" style="width:50px;" class="form-control" name = "return_' . $_cnt . '" data-index="' . $_cnt . '" data-retmax="' . $line['Count'] . '" size="4" >
<span class="return-opt-' . $_cnt . '" data-index="' . $_cnt . '" style="' . $return_opt_style . '">                        
<label class="form-check-label mr-5" >Причина</label>                        
<div class="form-check">
'.$return_opt_list.'
  </div>
  </span>
</div>    <div class="wrf-in returncomment-' . $_cnt . '" style="' . $returncomment_style . '">
    <label for="retcmtm-' . $_cnt . '">Комментарий</label>
    <textarea id="retcmtm-' . $_cnt . '" name="returncomment_' . $_cnt . '" rows="3" >' . $returncomment_value . '</textarea>
</div>
</div>
                    </div >
</td>
        </tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php

        if ($total_records > $limit) {
            $onpage_style = array(
                25 => 'btn-outline-secondary',
                50 => 'btn-outline-secondary',
                100 => 'btn-outline-secondary',
                200 => 'btn-outline-secondary',
                1500 => 'btn-outline-secondary',
            );
            $onpage_style[$limit] = 'btn-outline-primary disbled';
            ?>
            <div class="row ">
                <div class="col-6  text-right p-5">
                    <div class=" justify-content-right" role="toolbar" aria-label="Toolbar with button groups">
                        <div class=" mr-2 btn-group-sm" id="limitCount" role="group" aria-label="Second group">
                            <input type="hidden" name="limit" value="<?= $limit ?>">
                            <label class="mr-4">Строк на странице</label>
                            <button type="submit" class="btn <?= $onpage_style[25] ?>" name="onpage" value="25">25
                            </button>
                            <button type="submit" class="btn <?= $onpage_style[50] ?>" name="onpage" value="50">50
                            </button>
                            <button type="submit" class="btn <?= $onpage_style[100] ?>" name="onpage" value="100">100
                            </button>
                            <button type="submit" class="btn <?= $onpage_style[200] ?>" name="onpage" value="200">200
                            </button>
                            <button type="submit" class="btn <?= $onpage_style[1500] ?>" name="onpage" value="1500">1500
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-6 p-5">
                    <?= get_list2browse_pager_btn($start_in, $limit, $total_records) ?>
                </div>
            </div>
        <?php } ?>
        <input type="hidden" name="countLine" value="<?= $_cnt ?>">
        <input type="hidden" name="select_places" value="<?= $placeID ?>">
        <input type="hidden" name="numMat" value="<?= $numMat ?>">
        <input type="hidden" name="check_inprod" value="<?= $check_inprod ?>">
        <input type="hidden" name="user_in_production" value="<?= $user_in_production ?>">
        <input type="hidden" name="user_actives" value="<?= $user_actives ?>">
        <input type="hidden" name="dayCount" value="<?= $dayCount ?>">
        <div class="row ">
            <div class="col-12  text-center pb-5 ">
                <button type="submit" name="submit" value="1" class="btn btn-success" id="preSubmit"><i class="far fa-save"></i>
                    Провести
                </button>
            </div>
        </div>
        <?php // 24.06.2023 Тоценко. Добавлено модальное окно выбора идентификаторов списываемых материалов ?>
        <div id="submitModalField" class="submitModalField">
            <i class="far fa-2x fa-times-circle" name="closeModal"></i>
            <div class="submitModal">
                <h2 class="tac">Выберите артикулы списываемых материалов</h2>
                <div class="container addition-ids-data"></div>
                <div class="col-12  text-center pb-5 ">
                    <button type="submit" name="submit" value="1" class="btn btn-success" id="finallySubmit" disabled><i class="far fa-save"></i>
                        Провести
                    </button>
                </div>
            </div>
        </div>
    <?php } ?>
</form>
<?php include_once('footer.php'); ?>
