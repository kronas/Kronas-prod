$(document).ready(function() {
  
  // Закрытие модального окна
  // ##############################################################################
  // Получаем элементы модального окна и кнопку закрытия
  var $modalField = $('#submitModalField');
  var $modalContent = $('.submitModal');
  var $closeModalButton = $('[name="closeModal"]');

  // Функции для плавного открытия/закрытия модального окна
  function openModal() {
    // Получить высоту страницы
    var pageHeight = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight, document.body.offsetHeight, document.documentElement.offsetHeight, document.body.clientHeight, document.documentElement.clientHeight);

    // Присвоить высоту модальному окну
    $('#submitModalField').css('height', pageHeight + 'px');
    
    $modalField.fadeIn();
    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }

  function closeModal() {
    $modalField.fadeOut();
    $('.addition-ids-data').fadeOut(500, function() {
      // По завершению анимации вызывается колбэк, где можно удалить содержимое блока
      $(this).empty();
      // Или можно использовать $(this).html(''); вместо $(this).empty(); для плавного удаления
    }).fadeIn(500);
  }

  // Обработчик события клика на модальное окно
  $modalField.on('click', function(event) {
    // Проверяем, был ли клик по элементам внутри модального окна
    if ($(event.target).is($modalField)) {
      closeModal();
    }
  });

  // Обработчик события клика на кнопку закрытия
  $closeModalButton.on('click', closeModal);

  // Обработчик события нажатия клавиши Escape
  $(document).on('keydown', function(event) {
    if (event.key === 'Escape') {
      closeModal();
    }
  });

  // Предотвращаем закрытие модального окна при клике внутри него
  $modalContent.on('click', function(event) {
    event.stopPropagation();
  });
  // ______________________________________________________________________________


  // Отправка данных формы
  // ##############################################################################
  $('form').on('submit', function(event) {
    var submitButton = $(this).find('button[name="submit"]');
    if (submitButton.length && submitButton.is(':focus') && submitButton.attr('id') === 'preSubmit') {
      event.preventDefault(); // Прерываем отправку данных формы
      openModal();
      var formData = $('form').serialize();

      if (formData && formData.length > 0) {
        var searchParams = new URLSearchParams(formData);
        var dataOut = [];
        var materialCount = Number(searchParams.get('countLine'));
        var idsFlag = 0;

        for (var i = 1; i <= materialCount; i++) {
          var materialObj = {};
          materialObj['material_' + i] = searchParams.get('material_' + i);
          materialObj['mat_name_' + i] = searchParams.get('mat_name_' + i);
          materialObj['client_' + i] = searchParams.get('client_' + i);
          materialObj['client_name_' + i] = searchParams.get('client_name_' + i);
          materialObj['part_ids_' + i] = searchParams.get('part_ids_' + i);
          materialObj['old_count_' + i] = searchParams.get('old_count_' + i);
          materialObj['return_' + i] = searchParams.get('return_' + i);
          if (searchParams.get('part_ids_' + i) !== null) {
            idsFlag ++;
          }
          dataOut.push(materialObj);
        }

      }

      // Перебор отфильтрованных объектов и создание HTML-структуры
      var maxCheckboxCounts = []; // Массив для хранения максимального количества выбранных чекбоксов (для каждого материала)
      var selectedCheckboxCounts = []; // Массив для хранения количества уже выбранных чекбоксов (для каждого материала)

      for (var i = 0; i < dataOut.length; i++) {

        // Если поля "part_ids_" и "return" не пустые
        if (dataOut[i]['part_ids_' + (i + 1)] && dataOut[i]['return_' + (i + 1)] !== '') {

          // Создаем HTML-структуру
          var rowDiv = $('<div>').addClass('row one-client-mat');
          var col1Div = $('<div>').addClass('col-6');
          col1Div.append($('<p>').text(dataOut[i]['material_' + (i + 1)]));
          col1Div.append($('<p>').text(dataOut[i]['mat_name_' + (i + 1)]));
          col1Div.append($('<p>').text(dataOut[i]['client_name_' + (i + 1)]));
          var col2Div = $('<div>').addClass('col-6');
          var checkboxesIds = $('<div>').addClass('checkboxes-ids');

          // Разбиваем строку с запятыми на массив
          var partIdsArray = dataOut[i]['part_ids_' + (i + 1)].split(',');

          // Создаем чекбоксы для каждого элемента массива
          for (var j = 0; j < partIdsArray.length; j++) {
            var oneCheckbox = $('<div>').addClass('one-checkbox');
            var checkbox = $('<input>').attr({
              class: 'checkIds',
              type: 'checkbox',
              name: (i + 1),
              value: partIdsArray[j]
            });
            var checkboxText = $('<p>').text(partIdsArray[j]);
            oneCheckbox.append(checkbox);
            oneCheckbox.append(checkboxText);

            col2Div.append(oneCheckbox);
          }

          // Устанавливаем максимальное количество выбираемых чекбоксов и инициализируем количество выбранных
          maxCheckboxCounts[(i + 1)] = dataOut[i]['return_' + (i + 1)]
          selectedCheckboxCounts[(i + 1)] = 0;

          // Добавляем созданные элементы в HTML
          rowDiv.append(col1Div, col2Div);
          $('.addition-ids-data').append(rowDiv);
        }
      }

      // Отслеживаем изменения состояния чекбоксов
      $('.one-checkbox input[type="checkbox"]').on('change', function() {

        // Если выбраны все материалы на списание, то при клике на любой чекбокс, выбираются все сразу
        if (dataOut[this.name - 1]['old_count_' + this.name] === maxCheckboxCounts[this.name]) {
          if ($(this).prop('checked')) $('.one-checkbox input[name="' + this.name + '"]').prop('checked', true);
          else $('.one-checkbox input[name="' + this.name + '"]').prop('checked', false);
        }

        selectedCheckboxCounts[this.name] = $('.one-checkbox input[name="' + this.name + '"]:checked');

        // Проверяем количество выбранных чекбоксов
        if (selectedCheckboxCounts[this.name].length >= maxCheckboxCounts[this.name]) {
          // Если выбрано максимальное количество чекбоксов,
          // делаем остальные невыбираемыми
          $('.one-checkbox input[name="' + this.name + '"]').not(':checked').attr('disabled', true);
        } else {
          // Если выбрано менее максимального количества чекбоксов,
          // включаем все чекбоксы
          $('.one-checkbox input[name="' + this.name + '"]').attr('disabled', false);
        }
        
        // Получаем все выбранные чекбоксы
        var selectedCheckboxes = $('input[name="' + this.name + '"]:checked');
        
        // Формируем строку значений выбранных чекбоксов
        var selectedValues = selectedCheckboxes.map(function() {
          return $(this).val();
        }).get().join(',');

        // Обновляем значения полей "part_ids_checked_" в форме
        $('input[name^="part_ids_checked_' + this.name + '"]').val(selectedValues);

        // Проверяем сколько всего чекбоксов должно быть выбрано
        var maxCheckedCount = 0
        maxCheckboxCounts.forEach(function(subArray) {
          if (subArray > 0) maxCheckedCount += +subArray;
        });

        // и сколько всего чекбоксов выбрано
        var checkedCount = 0
        selectedCheckboxCounts.forEach(function(subArray) {
          if (subArray.length > 0) checkedCount += subArray.length;
        });

        if (maxCheckedCount === checkedCount) {
          $('#finallySubmit').removeAttr('disabled');
        } else {
          if (!$('#finallySubmit').is(':disabled')) {
            $('#finallySubmit').attr('disabled', 'disabled');
          }
        }

      });

      // Если выбранные материалы не содержат идентификаторов, то активируем кнопку подтверждения
      if (!maxCheckboxCounts.length) {
        $('#finallySubmit').removeAttr('disabled');
      }

      // Отправка данных формы
      $('#finallySubmit').on('click', function() {
        // Очищаем чекбоксы, они были нужны, чтобы собрать идентификаторы.
        // Если их не очистить, то они тоже попадают в пересылаемый массив, а это не нужно.
        $('.one-checkbox input[class="checkIds"]').prop('checked', false);
        $('form').off('submit').submit();
      })

    }
  });
  // ______________________________________________________________________________



});