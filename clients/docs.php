<?php
session_start();
include_once('functions2.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php');
    exit();
}
if (isset($_POST['submit']) && $_POST['submit'] == 1) {
    $out_cancel=array();
    foreach ($_POST['mc_id'] as $key=>$data) {
        $out_cancel[] = array(
            'ID' => $data,
            'COUNT' => abs($_POST['Move'][$key]),
            'placesID' => $_POST['Place'][$key]
        );
    }
    $Type = 'correct';
    $Reason = 'other';
    $Manager = $_SESSION['user']['ID'];
    $Order1C = 0;
    $Comment = "Аннулирование документа №".$_POST['d_id'];
    $docID = db_create_doc($Type, $_POST['Client'], $Reason, $Manager, $Order1C, $Comment);
    $_SESSION['messages']['success'][] = "Документ №$docID аннулирования документа №".$_POST['d_id']." для клиента №".$_POST['Client']." создан  !";
    db_materials_stock_move_add($out_cancel, $docID);
    db_materials_stock_upgate($_POST['Client']);
    $_SESSION['messages']['success'][]='Документ сохранен!';
    header('Location: '.$main_dir . '/clients/docs.php'.'?nw='.$_GET['nw']);
    exit();
}
?>

<?php
$title='Документы';
include_once('header.php');
$_types=array(
    'in'=>'приход',
    'out'=>'расход',
    'plan'=>'в производство',
    'correct'=>'Корректировка',
);
if (isset($_GET['doc'])) {
    $docs = db_get_doc_details($_GET['doc']);

    if (count($docs) > 0) {
        $listPlaces = db_get_listPlaces(true);
        $doc = $docs[0];
        ?>
        <div class="row  align-items-center">
            <div class="col-md-1 col-sm-6 "><a href="docs.php">
                    <div class="alert alert-dark" role="alert">
                        <<< назад
                    </div>
                </a></div>
        </div>
        <?php

        if ($_SESSION['user']['role'] == 'client' && db_verifyGroupClient($doc['Client'],$_SESSION['user']['ID'])==0) {
            echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div>';

        } else {
            ?>
            <div class="row  align-items-center">

                <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
                    <table class="table table-striped mt-3">

                        <tbody>
                        <tr>
                            <th>№ Документа</th>
                            <td><?= $doc['d_id'] ?></td>
                        </tr>
                        <tr>
                            <th>Дата</th>
                            <td><?= date('Y/m/d H:i', $doc['Datetime']) ?></td>
                        </tr>
                        <tr>
                            <th>Тип</th>
                            <td><?= $_types[$doc['Type']] ?></td>
                        </tr>
                        <tr>
                            <th>Менеджер</th>
                            <td><?= $doc['mname'] ?></td>
                        </tr>
                        <tr>
                            <th>Клиент</th>
                            <td><?= $doc['cname'] ?></td>
                        </tr>
                        <tr>
                            <th>Причина</th>
                            <td><?= $doc['Reason'] ?></td>
                        </tr>
                        <?php
                        if($doc['Type']=='in')
                        {
                        ?>
                        <tr>
                            <th>Склад</th>
                            <td><?= $listPlaces[$doc['Place']]['NAME'] ?></td>
                        </tr>
                        <?php
                        }
                        if (!empty($doc['Comment'])) {
                        ?>
                        <tr>
                            <th>Комментарий</th>
                            <td><?= $doc['Comment'] ?></td>
                        </tr>
                        <?php
                        }
                        if (!empty($doc['notation'])) {
                        ?>
                        <tr>
                            <th>Примечание</th>
                            <td><?= $doc['notation'] ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
                if ($_SESSION['user']['role'] == 'admin' || $_SESSION['user']['role'] == 'manager')
                {
                    echo  '<div class="col-12 text-center mt-3 mb-2">';
                    echo  '<a target="_blank" class="btn btn-success" href="check.php?doc=' . $doc['d_id']  . '" title="Печать наклейки"><i class="fas fa-print"></i> Печать наклейки</a>';
                    echo  '</div>';
                }
                ?>
            </div>
            <div class="row"><div class="col-12 pl-3 pr-3 mt-2">
            <table class="table table-striped">
            <thead>
            <tr>
                <?php
                if($doc['Type']=='out') echo ' <th>Склад</th>';
                ?>
                <th>Деталь</th>
                <th>Код</th>
                <th>W</th>
                <th>L</th>
                <th>T</th>
                <th>Движение</th>
            </tr>
            </thead><tbody>
            <?php
            foreach ($docs as $row) {
                echo '<tr>';
                if($doc['Type']=='out') echo ' <td>'.$listPlaces[$row['Place']]['NAME'].'</td>';
                echo '
    <td>' . $row['Name'] . $row['mat_name'] . '</td>
    <td>' . $row['Material_client'] . '</td>
    <td>' . $row['W'] . '</td>
    <td>' . $row['L'] . '</td>
    <td>' . $row['T'] . '</td>
    <td>' . $row['Move'] . '</td>
        </tr>';
            }
            echo '</tbody></table></div>';
            if (($_SESSION['user']['role'] == 'admin' || $_SESSION['user']['role'] == 'manager') && $doc['Type']=='out') {
                echo '<div class="col-12 text-center"><form method="post" class="mt-4 text-center" action="docs.php'.'?nw='.$_GET['nw'].'">';
                echo '<input type="hidden" name="d_id" value="'.$doc['d_id'].'">';
                echo '<input type="hidden" name="Client" value="'.$doc['Client'].'">';
                foreach ($docs as $_doc)
                {
                    echo '<input type="hidden" name="mc_id[]" value="'.$_doc['mc_id'].'">';
                    echo '<input type="hidden" name="Move[]" value="'.$_doc['Move'].'">';
                    echo '<input type="hidden" name="Place[]" value="'.$_doc['Place'].'">';
                }
               echo '<button type="submit" name="submit" value="1" class="btn btn-danger"><i class="fas fa-undo-alt"></i>Аннулировать</button>';
               echo '</form></div>';
            }
//    echo get_list2browse_pager($_url, $page, $limit, $total_records);
            echo '</div>';
        }
    } else {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div></div>';
    }
} else {
    if ($_SESSION['user']['role'] == 'manager' || $_SESSION['user']['role'] == 'admin') {
        ?>
        <div class="row">
            <div class="col-md-4 offset-md-4 col-sm-12">

                <form class="form-inline" method="get" action="docs.php<?='?nw='.$_GET['nw']?>">

                    <div class="form-group mx-sm-3 mb-2">
                        <label for="client_search_tel" class="mr-4">Телефон клиента</label>
                        <input type="text" class="form-control phone" id="client_filtr_tel" name="client_filtr_tel"
                               data-mask="+380 (00) 000 00 00" placeholder="+380 (__) ___ __ __">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Фильтровать</button>
                </form>
            </div>
        </div>
        <?php
    }


    $total_records = 0;
    $filtr = array();
    $_url = 'docs.php?';
    if (isset($_GET['client_filtr_tel']) && strlen(trim($_GET['client_filtr_tel'])) == 17) {
        $filtr[] = ' DOC.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.tel = "' . str_replace(' ','',trim($_GET['client_filtr_tel'])) . '"))) ';
        $_url .= 'client_filtr_tel=' . urlencode(trim($_GET['client_filtr_tel'])) . '&';
    }
    if ($_SESSION['user']['role'] == 'client') {
        $filtr[] = ' DOC.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.tel = "' . str_replace(' ','',trim($_SESSION['user']['phone'])) . '" OR clnt.`e-mail` = "' . trim($_SESSION['user']['mail']) . '")))';

    }
    $limit = 50;
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $start = $page == 1 ? 0 : (($limit * ($page - 1)));
    $table = db_get_listdocs($filtr, $start, $limit, $total_records);
    if (is_null($table)) {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div></div>';
    } else {
        echo '<div class="row"><div class="col-12 pl-3 pr-3">';
        echo '<table class="table table-striped"><thead>    <tr>
        <th>#</th>
        <th>Дата</th>
        <th>Тип</th>
        <th>Менеджер</th>
        ' . ($_SESSION['user']['role'] == 'client' ? '' : '<th>Клиент</th>') . '
        <th>...</th>
    </tr></thead><tbody>';
        $r=0;

        foreach ($table as $row) {
            $_lnkPrint='';
            if ($row['Type']=='in' && ($_SESSION['user']['role'] == 'admin' || $_SESSION['user']['role'] == 'manager')) $_lnkPrint='<a target="_blank" href="check.php?doc=' . $row['d_id']  . '" title="Печать наклейки"><i class="fas fa-print"></i></a>';

            echo '<tr>
<td>' . $row['d_id'] . '</td>
<td>' . date('Y/m/d H:i', $row['Datetime']) . '</td>
<td>' . $_types[$row['Type']] . '</td>
<td>' . $row['mname'] . '</td>
' . ($_SESSION['user']['role'] == 'client' ? '' : '<td>' . $row['cname'] . '</td>') . '
<td><a href="docs.php?doc=' . $row['d_id'] . '" title="Подробнее"><i class="fas fa-search"></i></a>'.$_lnkPrint.'</td>
    </tr>';
        }
        echo '</tbody></table>';

        if ($total_records > $limit) echo get_list2browse_pager2($_url, $page, $limit, $total_records);


        echo '</div></div>';
    }
    ?>

<?php } ?>
<?php include_once('footer.php'); ?>