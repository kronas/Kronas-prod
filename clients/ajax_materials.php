<?php

include_once '../func.php';
include_once 'functions2.php';

// updates data on the material_general table
if (
    isset($_POST['trimL']) &&
    isset($_POST['trimW']) &&
    isset($_POST['minDL']) &&
    isset($_POST['minDW']) &&
    isset($_POST['double_ok']) &&
    isset($_POST['price_from_size'])
) {
    $trimL = intval($_POST['trimL']);
    $trimW = intval($_POST['trimW']);
    $minDL = intval($_POST['minDL']);
    $minDW = intval($_POST['minDW']);
    $double_ok = intval($_POST['double_ok']);
    $price_from_size = intval($_POST['price_from_size']);
    $sql = "UPDATE material_general
                SET value_dig = CASE material_general_id WHEN 1 THEN $trimL WHEN 2 THEN $trimW WHEN 3 THEN $minDL  WHEN 4 THEN $minDW
                END, value_bool = CASE material_general_id WHEN 5 THEN $double_ok WHEN 6 THEN $price_from_size
                END WHERE material_general_id IN (1,2,3,4,5,6)";

    if (sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)) {
        $response = [
            'error' => false,
            'message' => "Данные успешно обновлены"
        ];
    } else {
        $response = [
            'error' => true,
            'message' => "Ошибака базы данных"
        ];
    }
    echo json_encode($response);
    die;
}

// загружает таблицу материалов по умолчанию
if (isset($_POST['load_table'])) {
    echo mat_get_materials_table(mat_get_materials_default(), 1);
    die;
}

// дерево каталога
if (isset($_POST['parent_id'])) {
    $response = [];
    $parent_id = intval($_POST['parent_id']);
    $tree = mat_get_good_tree($parent_id);
    $tree = mat_form_tree($tree);
    $response = [
        'error' => false,
        'result' => mat_build_tree($tree, $parent_id),
    ];
    echo json_encode($response);
    die;
}

// название таблиц для проверки
$principle_tables = ['material_half', 'material', 'material_m2', 'material_m2_diff'];

// дает отфильтрованную таличную часть материалов
if (isset($_POST['place_id'])) {
    $response = [];
    $place_id = intval($_POST['place_id']);
    if (isset($_POST['salePrinciple'])) {
        $table = ($_POST['salePrinciple'] === 'material') ? $_POST['salePrinciple'] : "material_{$_POST['salePrinciple']}";
        if (isset($_POST['only'])) {
            $materials = (in_array($table, $principle_tables)) ? mat_get_material_by_place($table, $place_id) : [];
            $message = "Нет материалов";
        }
    }
    if (isset($_POST['folder_id'])) {
        $folder_id = intval($_POST['folder_id']);
        $materials = (in_array($table, $principle_tables)) ? mat_get_materials_by_place_and_category($table, $place_id,
            $folder_id) : [];
        $message = 'Нет материалов в этой категории';
    }
    if (isset($_POST['properties'])) {

        $materials = mat_get_materials_by_place_and_properties($table, $place_id, $_POST['properties']);
        $message = 'С такаими свойтствами нет материалов';
    }
    if (isset($_POST['code'])) {
        $code = ($_POST['code'] === "") ? "" : intval($_POST['code']);
        $materials = (in_array($table, $principle_tables)) ? mat_get_materials_by_place_and_code($table, $place_id,
            $code) : [];
        $message = "Нет материалов с таким кодом: $code";
    }
    if (isset($_POST['har'])) {
        $har = ($_POST['har'] === "") ? "" : intval($_POST['har']);
        $materials = (in_array($table, $principle_tables)) ? mat_get_materials_by_place_and_har($table, $place_id,
            $har) : [];
        $message = "Нет материалов с такой характеристикой: $har";
    }
    if (!empty($materials)) {
        $response = [
            'error' => false,
            'result' => mat_get_materials_table($materials, $place_id),
        ];
    } else {
        $response = [
            'error' => true,
            'result' => $message,
        ];
    }
    echo json_encode($response);
    die;
}

// Подгружает варианты свойств по выбраному селекту
if (isset($_POST['type']) && isset($_POST['prop_name'])) {
    $response = [
        'result' => mat_get_property_value_options($_POST['prop_name'], $_POST['type']),
    ];
    echo json_encode($response);
}

// обновляет связи материалов и прринципа пордажи
if (isset($_POST['update_selling']) && $_POST['update_selling']) {
    $materials = json_decode($_POST['materials'], true);
    $places = json_decode($_POST['places'], true);
    $material_ids = [];
    $response = [];
    sendMaterialSalesRatio($_POST);
    $ins_table = (in_array('material_' . $_POST['selling_principle'], $principle_tables))
        ? 'material_' . $_POST['selling_principle'] : "";
    foreach ($materials as $material) {

        if ($material['delete_from']['half'] !== "") {
            $table = 'material_half';
            $response['message_del'][$material['mat_id']]['half'] = [
                'mat_id' => $material['mat_id'],
                'result' => mat_delete_selling_principle($material['delete_from']['half'], $material['mat_id'], $table)
            ];

        }
        if ($material['delete_from']['m2'] !== "") {
            $table = 'material_m2';
            $response['message_del'][$material['mat_id']]['m2'] = [
                'mat_id' => $material['mat_id'],
                'result' => mat_delete_selling_principle($material['delete_from']['m2'], $material['mat_id'], $table)
            ];
        }if ($material['delete_from']['m2_diff'] !== "") {
            $table = 'material_m2_diff';
            $response['message_del'][$material['mat_id']]['m2_diff'] = [
                'mat_id' => $material['mat_id'],
                'result' => mat_delete_selling_principle($material['delete_from']['m2_diff'], $material['mat_id'], $table)
            ];
        }
        array_push($material_ids, $material['mat_id']);
    }
    if ($ins_table === 'material_m2_diff') {
        $response['message_ins'] = mat_insert_in_m2_diff($places, $material_ids, json_decode($_POST['m2_diff'], true));
    } elseif ($ins_table) {
        $response['message_ins'] = mat_insert_selling_principle($places, $material_ids, $ins_table);
    }
    $response['result'] = mat_get_materials_table(mat_get_material_by_place(($ins_table === "") ? 'material': $ins_table, $places[0]), $places[0]);
    echo json_encode($response);
}

// выводит дерево католога товаров
if (isset($_POST['load_tree']) && $_POST['load_tree']) {
    $tree = mat_get_good_tree();
    $tree = mat_form_tree($tree);
    echo mat_build_tree($tree);
    die;
}

// возвращате все записи из таблицы material_m2_diff по id материалу и id филиала
if (isset($_POST['get_m2_diff']) && $_POST['get_m2_diff']) {
    $response = [];
    $data = mat_get_m2_diff($_POST['m2_diff_place_id'], $_POST['m2_diff_mat_id']);
    if (is_array($data)) {
        $response = [
            'result' => json_encode($data),
            'error' => false,
        ];
    } else {
        $response['error'] = true;
    }
    echo json_encode($response);
    die;
}