<?php
session_start();
include_once('functions2.php');
include_once('func_api.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
} elseif($_SESSION['user']['role']!='admin' && $_SESSION['user']['role']!='manager'){
    $_SESSION['messages']['errors'][]='У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}

if (isset($_POST['submit']) && $_POST['submit'] == 1) {
    $mat_in = array();
    $_POST['crash_checkbox'][0] = '';
    coming_parse_post($_POST, $mat_in, $_POST['select_places']);

    if (count($mat_in) > 0) {

        db_get_exist_materials($mat_in);
        $Type = 'in';
        $placesID = $_POST['select_places'];
        $clientID = $_POST['clientID'];
        db_verification_cell_materials($mat_in, $placesID, $clientID);
        $Reason = 'client';
        $Manager = $_SESSION['user']['ID'];
        $Order1C = 0;
        $Comment = '';
        $docID=(int)$_POST['docID'];
        if($docID>0)
        {
            db_update_doc($docID, $Manager);
        }
        else {
            $docID = db_create_doc($Type, $clientID, $Reason, $Manager, $Order1C, $Comment);
        }

        db_add_materials($mat_in);

        // 15.06.2023 Тоценко. Отсылка материалов на новый сервис
        foreach ($mat_in as &$value) {
            $value['clientId'] = $_POST['clientID'];
            $leftoversJson = getLeftoversJson($_POST['clientID'], $value['placesID'], $value);
            $sendResponse = (array) json_decode(sendMaterials($leftoversJson));

            if ($sendResponse['success'] === true) {
                $value['part_ids_checked'] = getApiPartIds($sendResponse['data']);
            }
        }
        
        db_add_materials2cells($mat_in,$docID);
        db_materials_stock_move_add($mat_in, $docID);
        db_materials_stock_upgate($clientID);
        $_SESSION['messages']['success'][]='Документ сохранен!';
        header('Location: '.$main_dir . '/clients/docs.php'.'?nw='.$_GET['nw']);
        exit();
    }
}
$client_search_tel='';
$clientData='';
$formStyle='display: none;';
$def_clientID='';
$selPlace=0;
$editdoc=0;
$submit_name='Создать';
/*
if(isset($_GET['editdoc']) ) {
    $submit_name='Сохранить';
    $editdoc=$_GET['editdoc'];
    $doc_details=db_get_doc_details($editdoc);
    $_doc=$doc_details[0];
    $formStyle='';
    $def_clientID=' value="'.$_doc['Client'].'"  ';
    $client_search_tel=' value="'.$_doc['ctel'].'" readonly ';
    $clientData='<span>Добавить остатки клиенту <h3>'.$_doc['cname'].' / '.$_doc['cemail'].'</h3></span>';
    $selPlace=$_doc['Place'];
}
*/
if(isset($_GET['client']) && (int) $_GET['client'] >0 ) {
    $_client=get_clientAlt('client_id='.$_GET['client']);
    if(count($_client)>0) {
        $formStyle = '';
        $def_clientID = ' value="' . $_client['client_id'] . '"  ';
        $client_search_tel = ' value="' . $_client['tel'] . '" readonly ';
        $clientData = '<span>Добавить остатки клиенту <h3>' . $_client['name'] . ' ' . $_client['e-mail'] . '</h3></span>';
    }
}

$title='Приход';
include_once('header.php');
?>
    <div class="row  align-items-center">
        <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
        </div>
        <?php echo messages($_SESSION, "col-md-4 offset-md-4 col-sm-12"); ?>
        <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
            <div class="form-group">
                <label for="client_search_tel">Телефон клиента</label>
                <input type="text" class="form-control phone" id="client_search_tel" name="client_search_tel"
                       data-mask="+38 (000) 000 00 00" placeholder="+38 (___) ___ __ __" <?=$client_search_tel?> >
                <small id="client_searchStatus" class="form-text text-muted"></small>
            </div>
        </div>

    </div>
    <form method="post" class="coming" style="<?=$formStyle?>">
        <input type="hidden" name="clientID" <?=$def_clientID?> >
        <input type="hidden" name="docID" value="<?=$editdoc?>" >
        <div class="container-fluid coming-in">
            <div class="row">
                <div class="col-md-4 offset-md-2 col-sm-12 border bg-light client-data  mt-2 mb-4 pt-3 pb-3"
                     style="<?=$formStyle?>">
                    <?= $clientData; ?>
                </div>
                <div class="col-md-4  col-sm-12 border bg-light mt-2 mb-4 pt-3 pb-3">
                    <?= db_get_listPlaces(false,$selPlace) ?>
                </div>
            </div>
            <?php

            function _create_add_line($num_line,$dcount, $stline, $required, $data)
            {
                $setting=load_ini();
                $wrap1='<div class="row line-'.$num_line.' add-lines tmpl" '.$dcount.' '.$stline.' data-index = "'.$num_line.'">';
                $wrap2='</div>';
                $_b_line='
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-md-2">
                            <input type = "hidden"  name = "MaterialID[]" value="'.$data[0].'" >
                            <input type = "hidden"  name = "M_ID[]"  '.$data[9].'  >
                            <div class="form-group" >
                                <div class="spinner-border text-primary loading-search ls-onCODE" role = "status" >
                                    <span class="sr-only" > Loading...</span >
                                </div>
                                <label for="login_tel" > Код мат. </label >
                                <input type = "text" class="form-control searchOnCode" name = "code[]" '.$data[1].'>
                                <div class="searchOnCode-box" ></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" >
                                <div class="spinner-border text-primary loading-search ls-onT" role = "status" >
                                    <span class="sr-only" > Loading...</span >
                                </div>
                                <label for="T[]" > Найти по "T" </label >
                                <input type = "text" class="form-control searchOnT" name = "T[]" '.$data[2].' >
                                <div class="searchOnT-box" ></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" >
                                <label for="Name[]">Наименование</label >
                                <input type = "text" class="form-control" name = "Name[]" '.$data[3].' >
                            </div>
                        </div>
                        <div class="col-md-3" >
                            <div class="form-group" >
                                <label for="L[]" > Длина вдоль текстуры</label >
                                <input type = "number" class="form-control coming-size" min="'.$setting['min4coming']['L'].'" name = "L[]" '.$required.'  '.$data[4].'>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label for="W[]" > Ширина</label >
                                <input type = "number" class="form-control coming-size" min="'.$setting['min4coming']['W'].'" name = "W[]" '.$required.' '.$data[5].'>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label for="Code_mat[]" > Code_mat</label >
                                <input type = "text" class="form-control" name = "Code_mat[]" '.$data[6].' >
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group" >
                                <label for="COUNT[]" > Кол-во</label >
                                <input type = "text" class="form-control" name = "COUNT[]" '.$required.' '.$data[7].'>
                            </div>
                        </div>                
                        <div class="col-md-2">
                            <div class="form-group" >
                                <label for="cells[]" >Ячейка</label >
                                <select class="form-control " name="cells[]" '.$required.'>
                                <option value="" selected="">Не указано</option></select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group" >
                                <label for="crash_checkbox[]" >Повр-я</label >
                                <input class="form-control" id="crash_checkbox" name="crash_checkbox[]" type="range" min="0" max="1" value="0" title="Установите если есть повреждения" '.$data[10].'>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group" >
                                <label for="notation[]" >Примечание</label >
                                <input class="form-control" id="crash_notation" name="notation[]" type="text" maxlength="255" title="Добавьте примечание" '.$data[11].'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">'.$data[8].'</div>';
                
                return $wrap1.$_b_line.$wrap2;
            }
            
            $data=array('','','','','','','','',
                '<div class="form-group times" ><i class="fa fa-times erase-tr" title="Удалить"></i></div>','','','');
              echo _create_add_line(0,' data-count="0" ',' style = "display: none;" ','',$data);
              /*
            if($editdoc==0)
                echo _create_add_line(0,' data-count="0" ',' style = "display: none;" ','',$data);
            else
            {
                echo _create_add_line(0,' data-count="'.count($doc_details).'" ',' style = "display: none;" ', ' ',$data);
                $num=1;
                foreach ($doc_details as $doc)
                {
//                    mt.`NAME` AS mtname, mt.`CODE` AS mtcode, mt.MY_1C_NOM
                    if($doc['MY_1C_NOM']==5931)
                    {
                        $fcode='';$mcode=$doc['mtcode'];
                    }
                    else
                    {
                        $fcode=$doc['mtcode'];$mcode='';
                    }
                    $data=array(
                            0=>$doc['mc_id'],
                        1=>' value="'.$fcode.'" readonly ',
                        2=>' value="'.$doc['T'].'" readonly ',
                        3=>' value="'.$doc['Name'].'" readonly ',
                        4=>' value="'.$doc['L'].'" ',
                        5=>' value="'.$doc['W'].'" ',
                        6=>' value="'.$mcode.'" readonly ',
                        7=>' value="'.$doc['Move'].'" ',
                        8=>'<div class="form-check mt-4">
                            <input class="form-check-input" type="checkbox" value="'.$doc['mc_id'].'" name="erase-tc[]" id="erase-tc-'.$doc['mc_id'].'">
                            <label class="form-check-label" for="erase-tc-'.$doc['mc_id'].'">Удалить</label></div> ',
                        9=>' value="'.$doc['MATERIAL_ID'].'" ',
                    );
                        echo _create_add_line($num,'','',' required ',$data);
                    $num++;
                }
            }
            */
            ?>
        </div>

        <div class="row ">
            <div class="col-6 text-center ">
                <button type="button" name="add" id="add" class="btn btn-primary"><i class="far fa-calendar-plus"></i> Еще</button>
            </div>
            <div class="col-6  text-center ">
                <button type="submit" name="submit" value="1" disabled class="btn btn-success"><i class="far fa-save"></i> <?=$submit_name?></button>
            </div>
        </div>
    </form>

    <script>

        function list4newdata_select(mid,mname, code, t, contID) {
            $('.line-' + contID + ' input[name="M_ID[]"]').val(mid);
            $('.line-' + contID + ' input[name="code[]"]').val(code);
            $('.line-' + contID + ' input[name="T[]"]').val(t);
            $('.line-' + contID + ' input[name="Name[]"]').val(mname);
            $('.line-' + contID + ' input[name="Code_mat[]"]').val('');
            $('.line-' + contID + ' input[name="COUNT[]"]').val('1');
            $('.line-' + contID + ' input[name="code[]"]').prop("readonly", true);
            $('.line-' + contID + ' input[name="T[]"]').prop("readonly", true);
            $('.line-' + contID + ' input[name="Name[]"]').prop("readonly", true);
            $('.line-' + contID + ' input[name="Code_mat[]"]').prop("readonly", true);
            $(".searchOnCode-box").hide();
            $(".loading-search.ls-onCODE").hide();
        }

        function list4newdataOnT_select(mid,name, code, t, contID) {
            $('.line-' + contID + ' input[name="M_ID[]"]').val(mid);
            $('.line-' + contID + ' input[name="code[]"]').val('');
            $('.line-' + contID + ' input[name="T[]"]').val(t);
            $('.line-' + contID + ' input[name="Name[]"]').val(name);
            $('.line-' + contID + ' input[name="Code_mat[]"]').val(code);
            $('.line-' + contID + ' input[name="COUNT[]"]').val('1');
            $('.line-' + contID + ' input[name="code[]"]').prop("readonly", true);
            $('.line-' + contID + ' input[name="T[]"]').prop("readonly", true);
            $('.line-' + contID + ' input[name="Code_mat[]"]').prop("readonly", true);
            $(".searchOnT-box").hide();
            $(".loading-search.ls-onT").hide();
        }

    </script>

<?php include_once('footer.php'); ?>