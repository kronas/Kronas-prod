<?php

/**
 * Функция возвращает токен, вынесен в отдельную функцию для единого доступа
 */
function getToken($onlyToken = false) {

	if ($onlyToken === false) {
		return 'clntlftvrsToken: productionResidues#vaCeq3!piDvmpEkl/3CHz-=noNsW1NHsDlUa4A=wY7sDQsjJHfM92=?JFCXVGwOTX?MIMrWhN/01Pp3HvPJTIvQxzkOj1JkfULzDgQyI5dOtX3h=6a8azmTTstPD3zNuP!ZeQrEIx1sq4i5-S=IerQ9E4I3R!VKS!-h3xUR7=nsWnZh5Iy!Dngz!HO5M9NLV2UYdbb6q237B0/Fc2q2C420aSc=YUh!KTXlrkS0NKrF3b';
	} else {
		return 'productionResidues#vaCeq3!piDvmpEkl/3CHz-=noNsW1NHsDlUa4A=wY7sDQsjJHfM92=?JFCXVGwOTX?MIMrWhN/01Pp3HvPJTIvQxzkOj1JkfULzDgQyI5dOtX3h=6a8azmTTstPD3zNuP!ZeQrEIx1sq4i5-S=IerQ9E4I3R!VKS!-h3xUR7=nsWnZh5Iy!Dngz!HO5M9NLV2UYdbb6q237B0/Fc2q2C420aSc=YUh!KTXlrkS0NKrF3b';
	}
	
}

/**
 * Функция возвращает ссылку для отправки материала, поставленного на приход
 */
function getAddMatLink() {
	// return 'https://test-store.kronas.dev/api/v2/residues/material/client-remainder/save';
	return 'https://store.kronas.com.ua/api/v2/residues/material/client-remainder/save';
}

/**
 * Функция возвращает ссылку для отправки списываемого материала
 */
function getRemoveMatLink() {
	// return 'https://test-store.kronas.dev/api/v2/residues/material/';
	return 'https://store.kronas.com.ua/api/v2/residues/material/';
}

/**
 * Функция возвращает ссылку для печати материалов
 */
function getPrintMatLink($api_part_ids) {
	// return 'https://test-store.kronas.dev/residues/material/list?ids=' . $api_part_ids;
	return 'https://store.kronas.com.ua/residues/material/list?ids=' . $api_part_ids;
}

/**
 * Функция записывает логи
 */
function writeLog($path, $data) {
	file_put_contents($path, $data, FILE_APPEND);
}

/**
 * Функция возвращает идентификатор менеджера по коду Акцента
 */
function getManagerId($accentCode) {
	$q = "SELECT `id` FROM `manager` WHERE `code` = " . $accentCode . " LIMIT 1";
	$managerId = getTableData($q);
	return $managerId[0]['id'];
}

/**
 * Функция возвращает идентификатор материала по коду Акцента
 */
function getMaterialId($accentCode) {
	$q = "SELECT `MATERIAL_ID` FROM `MATERIAL` WHERE `CODE` = " . $accentCode . " LIMIT 1";
	$materialId = getTableData($q);
	return $materialId[0]['MATERIAL_ID'];
}

/**
 * Функция возвращает код участка Акцента по идентификатору
 */
function getPlaceCode($placeId) {
	$q = "SELECT `AC_CODE` FROM `PLACES` WHERE `PLACES_ID` = '" . $placeId . "' LIMIT 1";
	$materialId = getTableData($q);
	return $materialId[0]['AC_CODE'];
}

/**
 * Функция пересобирает массив материалов, чтобы подставить нужные ключи, и конвертирует его в .json
 * для отправки по API на новый сервис
 **/
function getLeftoversJson($clientId, $placeId, $leftoversArr) {

	$q = "SELECT `AC_CODE` FROM `PLACES` WHERE `PLACES_ID` = " . $placeId . "";
	$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
	$place = $res['data'][0]['AC_CODE'];

	$q = "SELECT * FROM `client` WHERE `client_id` = " . $clientId . " LIMIT 1";
	$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
	$clientName = $res['data'][0]['name'];
	$clientPhone = $res['data'][0]['tel'];
	$clientCode = $res['data'][0]['code']; // ID Акцента

	$outputArr = [
		'clientId' => (string) $clientCode,
		'clientPhone' => $clientPhone,
		'clientFullName' => $clientName,
		'filialCode' => (string) $place,
		'materials' => []
	];

	$tmpArr = [];

	$tmpArr['vendorCode'] = (!empty($leftoversArr['code'])) ? $leftoversArr['code'] : $leftoversArr['Code_mat'];
	$typeAndName = getMaterialType($leftoversArr['Name'], $leftoversArr['T']);

	$tmpArr['mc_id'] = $leftoversArr['ID'];
	$tmpArr['typeMaterial'] = $typeAndName['type'];
	$tmpArr['type'] = $typeAndName['name'];
	$tmpArr['count'] = (string) $leftoversArr['COUNT'];
	$tmpArr['thickness'] = (string) $leftoversArr['T'];
	$tmpArr['name'] = $leftoversArr['Name'];
	$tmpArr['height'] = (string) $leftoversArr['L'];
	$tmpArr['width'] = (string) $leftoversArr['W'];
	$tmpArr['damage'] = $leftoversArr['crash_checkbox'];
	
	if (!empty($leftoversArr['notation'])) {
		$tmpArr['description'] = $leftoversArr['notation'];
	}

	$outputArr['materials'][0] = $tmpArr;

	return json_encode($outputArr, JSON_PRETTY_PRINT);
}

/**
 * Функция отсылает материалы, поставленные на приход
 * $materials в .json формате
 **/
function sendMaterials($materials) {

	$clntlftvrsToken = getToken();

	$url = getAddMatLink();

	$headers = array(
	    'Accept: application/json',
	    'Content-Type: application/json',
	    $clntlftvrsToken
	);

	$q = "select COLUMN_NAME
from INFORMATION_SCHEMA.COLUMNS
where TABLE_NAME='MATERIAL_KL_STOCK_MOVE'";
	$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
	$tmp = '';
	foreach ($res['data'] as $key => $value) {
		$tmp .= $value['COLUMN_NAME'];
		$tmp .= ', ';
	}

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $materials);

	$response = curl_exec($ch);
	curl_close($ch);

	return $response;
}

/**
 * Функция принимает списываемые материалы и путь к папке логирования
 * $materials - массив с параметрами списываемого материала
 **/
function preSendWriteOffMaterial($materials, $logPath) {

	$fullPath = $logPath . 'clients/writeoff/' . date('Y_m');
	$fullErrorsPath = $logPath . 'clients/writeoff/errors';
	$errors = [];

	if (!file_exists($logPath . 'clients')) {
		mkdir($logPath . 'clients', 0777, true);
		mkdir($logPath . 'clients/writeoff', 0777, true);
	} else {
		if (!file_exists($logPath . 'clients/writeoff')) {
			mkdir($logPath . 'clients/writeoff', 0777, true);
		}
		if (!file_exists($logPath . 'clients/writeoff/errors')) {
			mkdir($logPath . 'clients/writeoff/errors', 0777, true);
		}
	}

	foreach ($materials as $material) {

		$parts = explode(',', $material['part_ids_checked']);

		foreach ($parts as $partId) {
			$response = (array) json_decode(sendWriteOffMaterial($partId));
			if ($response['success'] === false) {
				$tmp = [
					'MatId' => $material['ID'],
					'PartId' => $partId
				];
				array_push($errors, $tmp);
				$response['addition'] = $tmp;
				// writeLog($fullErrorsPath . '/' . $material['ID'], json_encode($response, JSON_PRETTY_PRINT));
			}

			$respSuccess = $response['success'] === false ? 'ERROR' : 'Ok';
			// $text = date('Y-m-d H:i:s') . ' -> Mat: ' . $material['ID'] . ' Part: ' . $partId . ' Status: ' . $respSuccess . "\r\n";
			// writeLog($fullPath, $text);
			usleep(300000); // 300ms
		}
	}

	if (empty($errors)) return true;
	else return false;

}

/**
 * Функция отсылает списываемые материалы
 * $material - массив с параметрами списываемого материала
 **/
function sendWriteOffMaterial($material, $action = 'dec') {
	// act, res, dec
	// активный, резерв, списанный
	$clntlftvrsToken = getToken();
	$url = getRemoveMatLink();
	$url .= $material . '/' . $action;

	$headers = array(
	    $clntlftvrsToken
	);

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, ''); // Устанавливаем пустое тело запроса
	// curl_setopt($ch, CURLOPT_POSTFIELDS, $materials);

	$response = curl_exec($ch);
	curl_close($ch);

	return $response;
}

/**
 * Функция определяет тип материала и возвращает его
 */
function getMaterialType($materialName, $thickness) {
	$type = '';
	$types = [
		'ПФ' => 'TT', // стільниця
		'ДВП' => 'DVP',
		'МДФ' => 'MDF',
		'Фанера' => 'FN',
		'ЛДСП' => 'LDSP',
		'Компакт-плита' => 'CP',
		'Скло' => 'GL',
		'Лакобель' => 'LCB',
		'Дзеркало' => 'MR',
	];
	$backTypes = [
		'TT' => 'Стільниця', // стільниця
		'DVP' => 'ДВП',
		'MDF' => 'МДФ',
		'FN' => 'Фанера',
		'LDSP' => 'ЛДСП',
		'CP' => 'Компакт-плита',
		'GL' => 'Скло',
		'LCB' => 'Лакобель',
		'MR' => 'Дзеркало',
	];
	switch ($materialName) {
		case strpos($materialName, 'ЛДСП'):
			$type = $types['ЛДСП'];
			break;
		case strpos($materialName, 'ПФ'):
			$type = $types['ПФ'];
			break;
		case strpos($materialName, 'МДФ'):
		case strpos($materialName, 'AGT'):
			$type = $types['МДФ'];
			break;
		case strpos($materialName, 'ДВП'):
		case strpos($materialName, 'HDF'):
			$type = $types['ДВП'];
			break;
		case strpos($materialName, 'Стекло'):
		case strpos($materialName, 'стекло'):
			$type = $types['Скло'];
			break;
		case strpos($materialName, 'Зеркало'):
			$type = $types['Дзеркало'];
			break;
		case strpos($materialName, 'Фанера'):
			$type = $types['Фанера'];
			break;
		case strpos($materialName, 'Материал клиента'):
			$type = ($thickness <= 7) ? $types['ДВП'] : ($thickness > 27) ? $types['ПФ'] : $types['ЛДСП'];
			break;
		default:
			$type = $types['ЛДСП'];
			break;
	}

	$name = $backTypes[$type];
	return ['type' => $type, 'name' => $name];
}

/**
 * В новом сервисе у каждого куска свой идентификатор, и чтобы корректно списывать остатки нужно понимать какие куски удаляются
 * Но в текущем сервисе нет идентификаторов для каждого куска, просто меняется их количество при списании
 * и чтобы не менять рабочий функционал, собираем все идентификаторы в строку (через запятую) и при удалении конкретного куска
 * просто удаляем его идентификатор (только для совместимости/синхронизации с новым сервисом)
 */
function getApiPartIds($details) {

	$ids = '';

	foreach ($details as $key => $value) {
		$value = (array) $value;
		$ids .= $value['id'] . ',';
	}

	$ids = preg_replace('#\,$#', '', $ids);

	return $ids;
}

/**
 * Функция возвращает наименьший идентификатор (это значит, что это приход)
 */
function getMinDoc($docsArr) {

	$docs = [];

	foreach ($docsArr as $docArr) {
		$docs[] = $docArr['Doc'];
	}

	return min($docs);
}

/**
 * Функция возвращает наименьший идентификатор (это значит, что это приход)
 */
function setSync($mc_id) {

	$q = "UPDATE `MATERIAL_CLIENT` SET `sync` = 1 WHERE `mc_id` = " . $mc_id . "";
	$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
}

/**
 * Функция возвращает наименьший идентификатор (это значит, что это приход)
 */
function clearSyncFields() {

	$q = "UPDATE `MATERIAL_CLIENT` SET `sync` = null";
	$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
}

/**
 * Функция валидирует обязательные поля входящих данных для прихода материала клиента
 * на эндпоинт receiving_customer_materials.php (отсюда и вызывается)
 * Параметр $data это присланный распарсенный .json
 */
function validateIncomingData($data) {

	$output = [
		'status' => false,
		'message' => 'Ok'
	];

	$issetClient = null;
	if (isset($data['clientId']) || !empty($data['clientId'])) {
		$q = 'SELECT * FROM `client` WHERE `code` = ' . $data['clientId'] . ' LIMIT 1';
		$issetClient = getTableData($q);
	}

	if (!isset($data['clientId']) || empty($data['clientId'])) {
		if (!isset($data['clientId']))  $output['message'] = "Очікується поле 'clientId'";
		else $output['message'] = "Поле 'clientId' порожнє";
	} elseif (empty($issetClient)) {
		$output['message'] = "Клієнт з id " . $data['clientId'] . " не знайдений";
	} elseif (!isset($data['filialCode']) || empty($data['filialCode'])) {
		if (!isset($data['filialCode']))  $output['message'] = "Очікується поле 'filialCode'";
		else $output['message'] = "Поле 'filialCode' порожнє";
	} elseif (!isset($data['materials']) || empty($data['materials'])) {
		if (!isset($data['materials']))  $output['message'] = "Очікується поле 'materials'";
		else $output['message'] = "Поле 'materials' порожнє";
	} elseif (isset($data['materials']) || !empty($data['materials'])) {

		foreach ($data['materials'] as $key => $value) {

			$issetMaterial = null;
			if (isset($data['materials'][$key]['vendorCode']) || !empty($data['materials'][$key]['vendorCode'])) {
				$q = 'SELECT `MATERIAL_ID` FROM `MATERIAL` WHERE `code` = ' . $data['materials'][$key]['vendorCode'] . ' LIMIT 1';
				$issetMaterial = getTableData($q);
			}

			if (!isset($data['materials'][$key]['height']) || empty($data['materials'][$key]['height'])) {
				if (!isset($data['materials'][$key]['height']))  $output['message'] = "Очікується поле 'materials." . $key . '.' . "height'";
				else $output['message'] = "Поле 'materials." . $key . '.' . "height' порожнє";
				break;
			} elseif (!isset($data['materials'][$key]['vendorCode']) || empty($data['materials'][$key]['vendorCode'])) {
				if (!isset($data['materials'][$key]['vendorCode']))  $output['message'] = "Очікується поле 'materials." . $key . '.' . "vendorCode'";
				else $output['message'] = "Поле 'materials." . $key . '.' . "vendorCode' порожнє";
				break;
			} elseif (empty($issetMaterial)) {
				$output['message'] = "Матеріал 'materials." . $key . "." . $data['materials'][$key]['vendorCode'] . "' не знайдений";
				break;
			} elseif (!isset($data['materials'][$key]['width']) || empty($data['materials'][$key]['width'])) {
				if (!isset($data['materials'][$key]['width']))  $output['message'] = "Очікується поле 'materials." . $key . '.' . "width'";
				else $output['message'] = "Поле 'materials." . $key . '.' . "width' порожнє";
				break;
			} elseif (!isset($data['materials'][$key]['thickness']) || empty($data['materials'][$key]['thickness'])) {
				if (!isset($data['materials'][$key]['thickness']))  $output['message'] = "Очікується поле 'materials." . $key . '.' . "thickness'";
				else $output['message'] = "Поле 'materials." . $key . '.' . "thickness' порожнє";
				break;
			} elseif (!isset($data['materials'][$key]['name']) || empty($data['materials'][$key]['name'])) {
				if (!isset($data['materials'][$key]['name']))  $output['message'] = "Очікується поле 'materials." . $key . '.' . "name'";
				else $output['message'] = "Поле 'materials." . $key . '.' . "name' порожнє";
				break;
			} elseif (!isset($data['materials'][$key]['count']) || empty($data['materials'][$key]['count'])) {
				if (!isset($data['materials'][$key]['count']))  $output['message'] = "Очікується поле 'materials." . $key . '.' . "count'";
				else $output['message'] = "Поле 'materials." . $key . '.' . "count' порожнє";
				break;
			}
		}
		if ($output['message'] === 'Ok') {
			$output['status'] = true;
		}

	} else $output['status'] = true;

	return $output;
}

/**
 * Функция валидирует обязательные поля входящих данных для списания материала клиента
 */
function validateWriteOff($data) {

	$output = [
		'status' => false,
		'message' => 'Ok'
	];

	if (!isset($data['vendorcode']) || empty($data['vendorcode'])) {
		$output['message'] = 'Не вистачає поля "vendorcode", або воно пусте';
	} elseif (!isset($data['id']) || empty($data['id'])) {
		$output['message'] = 'Не вистачає поля "id", або воно пусте';
	} elseif (!isset($data['mc_id']) || empty($data['mc_id'])) {
		$output['message'] = 'Не вистачає поля "mc_id", або воно пусте';
	} elseif (!isset($data['manager_id']) || empty($data['manager_id'])) {
		$output['message'] = 'Не вистачає поля "manager", або воно пусте';
	} else {
		$output['status'] = true;
	}

	return $output;
}

/**
 * Функция конвертирует присланный массив мамтериала, поставленного на приход в новом сервисе
 * в формат текущего сервиса, чтобы поставить на приход
 * вызывается из receiving_customer_materials.php
 */
function convertJsonToCurrentFormat($data) {
	// {
	//   "redact": "1",
	//   "clientId": "28778",
	//   "clientPhone": "+38(096)334-88-34",
	//   "clientFullName": "Шабатин Сергей ",
	//   "filialCode": "54096",
	//   "materials": [
	//     {
	//       "id": 210,
	//       "vendorCode": "118045",
	//       "typeMaterial": "CP", // +
	//       "type": "ЛДСП", // +
	//       "count": "1",
	//       "thickness": "22", // +
	//       "name": "Материал клиента (толщина плиты 22 мм)", // +
	//       "height": "444", // +
	//       "width": "333", // +
	//       "damage": "0", // +
	//       "description": "ффф" // +
	//     }
	//   ]
	// }

    $qc = 'SELECT * FROM `client` WHERE `code` = ' . $data['clientId'] . ' LIMIT 1';
	$client = getTableData($qc);
	$clientId = $client[0]['client_id'];
	$clientPhone = $client[0]['tel'];
	$clientName = $client[0]['name'];

	$qm = 'SELECT `MATERIAL_ID` FROM `MATERIAL` WHERE `code` = ' . $data['materials'][0]['vendorCode'] . ' LIMIT 1';
	$material = getTableData($qm);
	$materialId = $material[0]['MATERIAL_ID'];

	$qp = 'SELECT `PLACES_ID` FROM `PLACES` WHERE `AC_CODE` = ' . $data['filialCode'] . ' LIMIT 1';
	$place = getTableData($qp);
	$placeId = $place[0]['PLACES_ID'];

	$redact = isset($data['redact']) ? (int) $data['redact'] : 0;

	$output = [
		0 => [
			'redact' => $redact,
			'M_ID' => (string) $materialId,
			'mc_id' => $data['materials'][0]['mc_id'],
	        'T' => (string) $data['materials'][0]['thickness'],
	        'Name' => (string) $data['materials'][0]['name'],
	        'L' => (string) $data['materials'][0]['height'],
	        'W' => (string) $data['materials'][0]['width'],
	        'COUNT' => '1',
	        'clientId' => $clientId,
	        'placesID' => $placeId,
	        'cell' => '1',
	        'code' => (0 === preg_match('#^1000#', $data['materials'][0]['vendorCode']) && 9 > iconv_strlen((string) $data['materials'][0]['vendorCode'])) ? (string) $data['materials'][0]['vendorCode'] : '',
	        'Code_mat' => (1 === preg_match('#^1000#', $data['materials'][0]['vendorCode']) && 9 === iconv_strlen((string) $data['materials'][0]['vendorCode'])) ? (string) $data['materials'][0]['vendorCode'] : '',
	        'crash_checkbox' => (string) $data['materials'][0]['damage'],
	        'notation' => (isset($data['materials'][0]['damage'])) ? $data['materials'][0]['damage'] . ' (New service)' : 'New service',
	        'part_ids_checked' => (string) $data['materials'][0]['id']
	    ]
	];

	return $output;
}

/**
 * Функция добавляет материалы, поставленные на приход в новом сервисе, в БД
 */
function receiveMaterialsNewService($mat_in) {

	$output = [
		'status' => 'true',
		'mc_id' => null, 
		'message' => 'Матеріал успішно доданий!'
	];

	db_get_exist_materials($mat_in);
    $Type = 'in';
    $placesID = $mat_in[0]['placesID'];
    $clientID = $mat_in[0]['clientId'];
    db_verification_cell_materials($mat_in, $placesID, $clientID);
    $Reason = 'client';
    $Manager = 184;
    $Order1C = 0;
    $Comment = '';
    $docID = (int)$_POST['docID'];

    if($docID > 0) db_update_doc($docID, $Manager);
    else $docID = db_create_doc($Type, $clientID, $Reason, $Manager, $Order1C, $Comment);

    db_update_doc($docID, $Manager);
	db_add_materials($mat_in);
    db_add_materials2cells($mat_in,$docID);
    db_materials_stock_move_add($mat_in, $docID);
    db_materials_stock_upgate($clientID);

    $output['mc_id'] = $mat_in[0]['ID'];

    return $output;
}

/**
 * Функция изменяет параметры материала, изменённые в новом сервисе
 */
function changeMaterialNewService($mat_data) {

	$output = [
		'status' => 'true',
		'mc_id' => $mat_data[0]['redact'],
		'message' => ''
	];

	// $q = "SELECT *  FROM `MATERIAL_KL_STOCK_COUNT` AS cnt 
	// 		LEFT JOIN `MATERIAL_CLIENT` AS mc ON mc.mc_id = cnt.Material_client 
	// 		WHERE cnt.Client = " . $mat_data[0]['clientId'] . " AND mc.CODE_MAT = " . $mat_data[0]['M_ID'] . "";
	$q = "SELECT *  FROM `MATERIAL_CLIENT` WHERE `mc_id` = " . $mat_data[0]['mc_id'] . "";
	$res = getTableData($q);

	if (!empty($res)) {
		$r = $res[0];
		$mc_id = $r['mc_id'];
		$fields = "`W` = '" . $mat_data[0]['W'] . "', `L` = '" . $mat_data[0]['L'] . "', `T` = '" . $mat_data[0]['T'] . "', `Name` = '" . $mat_data[0]['Name'] . "', `crash` = '" . (int) $mat_data[0]['crash_checkbox'] . "', `notation` = '" . $mat_data[0]['notation'] . "'";
		$q = "UPDATE `MATERIAL_CLIENT` SET $fields WHERE `mc_id` = $mc_id";
		$resChange = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);
		
		if ($resChange['res'] !== 1) {
			$output['status'] = 'false';
			$output['message'] = 'Не вдалося оновити дані для матеріалу ' . $r['code'] . '.';
		} else {
			$output['message'] = 'Дані для матеріалу ' . $r['code'] . ' успішно оновлено.';
		}
		
	} else {
		$output['status'] = 'false';
		$output['message'] = 'Матеріал для оновлення не знайдений.';
	}
	
	return $output;
}

/**
 * Функция списывает материал с клиента (когда он был списан в новом сервисе).
 */
function writeOffMaterialsNewService($line_material, $user_in_production) {
	
	// [return] => 1
    // [cause_ret] => in_production
    // [comment] => 
    // [days_left] => 4
    // [matCode] => 561561
    // [clientId] => 849925
    // [place] => 1
    // [cell] => 1
    // [old_count] => 5
    // [part_ids] => 749,750,751,752,753
    // [part_ids_checked] => 749

    $out_inprod1 = [];
    $out_inprod2 = [];
    $comment_innprod1 = '';
    $comment_innprod2 = '';
    $key_client = $line_material['clientId'];

	$out_inprod1[0] = array(
        'ID' => $line_material['mc_id'],
        'COUNT' => 0 - (int)$line_material['return'],
        'clientIdFrom' => $line_material['clientId'],
        'clientId' => $user_in_production,
        'placesID' => $line_material['place'],
        'cell' => $line_material['cell'],
        'old_count' => $line_material['old_count'],
        'part_ids' => $line_material['part_ids'],
        'part_ids_checked' => $line_material['part_ids_checked']
    );
    $out_inprod2[0] = array(
        'ID' => $line_material['mc_id'],
        'COUNT' => (int)$line_material['return'],
        'clientIdFrom' => $line_material['clientId'],
        'clientId' => $user_in_production,
        'placesID' => $line_material['place'],
        'cell' => $line_material['cell'],
        'old_count' => $line_material['old_count'],
        'part_ids' => $line_material['part_ids'],
        'part_ids_checked' => $line_material['part_ids_checked']
    );
    $comment_innprod1 .= $line_material['matCode'] . " - В производство <br>";
    $comment_innprod2 .= $line_material['matCode'] . " - Передача остатка производству <br>";

    $table = db_get_liststockOnClient($key_client);

    foreach ($table as $line) {
        if (isset($_arrOute[$line['PLACES_ID']][$line['Material_client']])) {
            if ($line['Count'] + $_arrOute[$line['PLACES_ID']][$line['Material_client']] < 0) {
                $_SESSION['messages']['errors'][] = 'Ошибка ввода данных! Отрицательный остаток недопустим!!';
                header('Location: ' . $main_dir . '/clients/corrects.php'.'?nw='.$_GET['nw']);
                exit();
            }
        }
    }

	$_rez = TRUE;
	$Type = 'correct';
	$Reason = 'other';
	$Manager = $line_material['managerId'];
	$Order1C = 0;
	$Comment = $comment_innprod1;
	$docID = db_create_doc($Type, $key_client, $Reason, $Manager, $Order1C, $Comment);
	$_SESSION['messages']['success'][] = "Документ №$docID списания для клиента №$key_client создан (в производство) !";
	db_materials_stock_move_add($out_inprod1, $docID);

	db_materials_stock_upgate($key_client);
	$Comment = $comment_innprod2;
	$docID = db_create_doc($Type, $user_in_production, $Reason, $Manager, $Order1C, $Comment);
	$_SESSION['messages']['success'][] = "Документ №$docID приход от клиента №$key_client в производство !";
	db_materials_stock_move_add($out_inprod2, $docID);
	db_materials_stock_upgate($user_in_production);
	$arr2cell=array();
	foreach ($out_inprod1 as $valueadd)
	{
	    $arr2cell[]=array('cell'=>$valueadd['cell'], 'ID'=>$valueadd['ID']);
	}
	db_add_materials2cells($arr2cell,$docID);
}
?>