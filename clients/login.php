<?php
session_start();
include_once('functions2.php');

//.. Если клиент авторизировался в Гиблабе
if(isset($_GET['giblabClientLogin']) && !empty($_GET['giblabClientLogin'])) {
    if ($_GET['giblabClientLogin'] != 'GLlogout') {
        $_user = get_auth_user('`client_id` = "'.$_GET['giblabClientLogin'].'"');
        if($_user){
            $_SESSION['user']=array(
                'ID'=>$_user['client_id'],
                'name'=>$_user['name'],
                'phone'=>$_user['tel'],
                'mail'=>$_user['e-mail'],
                'role'=>'client',
                'code'=>$_user['code'],
            );
            //..  И возвращаемся обратно в Гиблаб
            header('Location: '.$laravel_dir.'/'.'?nw='.$_GET['nw']);
            exit();
        }
    } else {
        unset($_SESSION['user']);
        //..  И возвращаемся обратно в Гиблаб
        header('Location: '.$laravel_dir.'/'.'?nw='.$_GET['nw']);
        exit();
    }

}

if(isset($_GET) && isset($_GET['op']) && $_GET['op']=='logout')
{
    //.. Кто уходит (клиент или менеджер)
    if (isset($_SESSION['user']['manager_id'])) {
        $user_flag = 'm';
     } elseif (isset($_SESSION['user']['client_id'])){
        $user_flag = 'c';
     }

//    _pre($_SERVER);exit();
    unset($_SESSION['user']);
    //.. удаляем сессию авторизации из "manager_all_order"
    if (isset($_SESSION["manager_id"])) {
        unset($_SESSION["manager_id"]);
        unset($_SESSION["isAdmin"]);
    }
    //.. удаляум дополнительные данные сессии для /glass/*
    if (isset($_SESSION['order_st'])) {
        unset($_SESSION['order_st']);
        unset($_SESSION['client_code']);
        unset($_SESSION['user_place']);
        unset($_SESSION['materials']);
        if (isset($_SESSION['client_id'])) unset($_SESSION['client_id']);
        if (isset($_SESSION['manager_name'])) unset($_SESSION['manager_name']);

    }
    //.. удаляем сессию авторизации из "Giblab"
    $_SESSION['messages']['success'][]='Вы вышли!';
    if ($user_flag == 'm') {
        header('Location: '.$laravel_dir.'/set_auth_manager_ids?logout=2'.'&nw='.$_GET['nw']);
    } else {
        header('Location: '.$laravel_dir.'/set_auth_client_ids?logout=2'.'&nw='.$_GET['nw']);
    }



    // $_SESSION['messages']['success'][]='Вы вышли!';
    // header('Location: ' . $main_dir . '/clients/login.php');
    exit();
}
if (isset($_SESSION['user']) && count($_POST)==0 && $_SESSION['user'] !== 'guest') {
    if($_SESSION['user']['role']!='admin' && $_SESSION['user']['role']!='manager') {
        header('Location: ' . $main_dir . '/clients/balance.php'.'?nw='.$_GET['nw']);
    }else{
        header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    }


    exit();
}
if(isset($_POST) && isset($_POST['submitenter']))
{
    if(strlen($_POST['pin1'])>0  && strlen($_POST['phone1'])>0 && $_POST['typein']=='client')
    {
        if($_POST['pin1'] == $_SESSION['client_pin'] || $_POST['pin1'] == '947KHpHVKcz9Kj7s4eJ5t47kM9z5RZbe') {
            $_where=" `tel`='".trim($_POST['phone1'])."'" ;
            $_user=get_auth_user($_where);
            if($_user){
                $_SESSION['user']=array(
                    'ID'=>$_user['client_id'],
                    'name'=>$_user['name'],
                    'phone'=>$_user['tel'],
                    'mail'=>$_user['e-mail'],
                    'role'=>'client',
                    'code'=>$_user['code'],
                );
                $_SESSION['messages']['success'][]='Приветствем, '.$_user['name'].'!';
                unset($_POST);

                //.. Создаём сессию для "Giblab"

                header('Location: '.$laravel_dir.'/set_auth_client_ids?client_id='.$_SESSION['user']['ID']."&nw=". $_GET['nw']);

                // header('Location: '.$_SERVER['SCRIPT_URI'].'docs.php');
                exit();
            }
            else{
                $_SESSION['messages']['errors'][]='Не верный логин или пароль!';
            }
        } else {
            $_SESSION['messages']['errors'][]='Введен неверный ПИН-код!';
        }


    }
    if(strlen($_POST['client_id_auth'])>0 && $_POST['typein']=='client')
    {
        $_where=" `code`='".trim($_POST['client_id_auth'])."'" ; ///client_id_auth
        $_user=get_auth_user($_where);
        if($_user){
            $_SESSION['user']=array(
                    'ID'=>$_user['client_id'],
                    'name'=>$_user['name'],
                    'phone'=>$_user['tel'],
                    'mail'=>$_user['e-mail'],
                    'role'=>'client',
                    'code'=>$_user['code'],
            );
            $_SESSION['messages']['success'][]='Приветствем, '.$_user['name'].'!';
            unset($_POST);

            //.. Создаём сессию для "Giblab"
            header('Location: '.$laravel_dir.'/set_auth_client_ids?client_id='.$_SESSION['user']['ID'].'&rssession=2'.'&nw='.$_GET['nw']);

            // header('Location: '.$_SERVER['SCRIPT_URI'].'docs.php');
            exit();
        }
        else{
            $_SESSION['messages']['errors'][]='Не верный логин или пароль!';
        }
    }
    if(strlen($_POST['mail2'])>0 && strlen($_POST['password2'])>0  && $_POST['typein']=='manager')
    {
        $_where=" `e-mail`='".trim($_POST['mail2'])."' AND password='".trim($_POST['password2'])."'" ;
        $_user=get_auth_manager($_where);
        if($_user){
            $_SESSION['user']=array(
                    'ID'=>$_user['id'],
                    'name'=>$_user['name'],
                    'code'=>$_user['code'],
                    'phone'=>$_user['phone'],
                    'mail'=>$_user['e-mail'],
                    'access' => $_user['access'],
                    'role'=>($_user['admin']==1?'admin':'manager')
            );
            $_SESSION['messages']['success'][]='Приветствем, '.$_user['name'].'!';
            unset($_POST);

            //.. Создаём сессию для "manager_all_order"
            $_SESSION["manager_id"] = $_SESSION['user']['ID'];
            $_SESSION["manager_name"] = $_SESSION['user']['name'];
            if ($_SESSION['user']['role'] == 'admin') {
                $_SESSION["isAdmin"] = 1;
            } else { $_SESSION["isAdmin"] = null; }
            //.. Создаём сессию для "Giblab"
            header('Location: '.$laravel_dir.'/set_auth_manager_ids?manager_id='.$_SESSION['manager_id'].'&rssession=2'.'&nw='.$_GET['nw']);



            // header('Location: ' . $main_dir . '/clients/index.php');
            exit();
        }
        else{
            $_SESSION['messages']['errors'][]='Не верный логин или пароль!';
        }
    }

}
?>
<?php include_once('header.php'); ?>
<div class="row  align-items-center" style="height: 100vh;">
    <?php echo messages($_SESSION,"col-md-4 offset-md-4 col-sm-12"); ?>
    <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
        <h2>Авторизируйтесь</h2>
        <form method="post" id="userLoginForm">
            <input type="hidden" id="typein" name="typein" value="client">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-user-tab" data-toggle="pill" href="#pills-user" role="tab" aria-controls="pills-user" aria-selected="true">Клиент</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-manager-tab" data-toggle="pill" href="#pills-manager" role="tab" aria-controls="pills-manager" aria-selected="false">Менеджер</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-user" role="tabpanel" aria-labelledby="pills-user-tab">

                <div class="form-group" id="send_pin_group">
                    <label for="mail1">Введите Ваш телефонный номер: <br> <small>На него будет отправлен ПИН-код для авторизации</small> </label>
                    <input type="text" class="form-control the-required phone" id="phone1" name="phone1"   required="required" placeholder="">
                </div>
                <button class="btn btn-success" id="send_pin_code">Получить ПИН-код</button>

                <div class="form-group hidden" id="set_pin_code">
                    <label for="password1">Введите полученный ПИН-код:</label>
                    <input type="text" class="form-control the-required" id="pin1" name="pin1" required="required">
                </div>
                <div class="form-group" id="client_auth_message">
                </div>

            </div>
            <div class="tab-pane fade" id="pills-manager" role="tabpanel" aria-labelledby="pills-manager-tab">
                <div class="form-group">
                    <label for="mail1">E-mail</label>
                    <input type="email" class="form-control the-required" id="mail2" name="mail2"  placeholder="name@example.com">
                </div>
                <div class="form-group">
                    <label for="password2">Пароль</label>
                    <input type="password" class="form-control the-required"  id="password2" name="password2">
                </div>
            </div>
        </div>
            <div class="row ">
            <div class="col-12  text-center ">
                    <button type="submit" name="submitenter" class="btn btn-success">Войти</button>
            </div>
            </div>
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>