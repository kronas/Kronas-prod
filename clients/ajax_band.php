<?php
include_once 'functions2.php';
// массивы для экранирования текстовых запросов от sql инъекций
$search = ['\\', "\0", "\n", "\r", "'", '"', "\x1a"];
$replace = ['\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'];
// Подгружает варианты свойств по выбраному селекту
if (isset($_POST['type']) && isset($_POST['prop_name'])) {
    $response = [
        'result' => mat_get_property_value_options($_POST['prop_name'], $_POST['type'], 'band'),
    ];
    echo json_encode($response);
}
// выводит дерево католога товаров
if (isset($_POST['load_tree']) && $_POST['load_tree']) {
    $tree = mat_get_good_tree(682);
    $tree = mat_form_tree($tree);
    echo mat_build_tree($tree, 682, 'band', 682);
    die;
}

// дерево каталога
if (isset($_POST['parent_id'])) {
    $response = [];
    $parent_id = intval($_POST['parent_id']);
    $tree = mat_get_good_tree($parent_id);
    $tree = mat_form_tree($tree);
    $response = [
        'error' => false,
        'result' => mat_build_tree($tree, $parent_id, 'band', 682),
    ];
    echo json_encode($response);
    die;
}

// загружает таблицу кромки по умолчанию
if (isset($_POST['load_table'])) {
    $response = [
        'error' => false,
        'result' => band_get_band_table(band_get_band_default()),
        'message' => '',
    ];
    echo json_encode($response);
    die;
}

// фильтр выборки кромки
if (isset($_POST['filter_band']) && $_POST['filter_band']) {
    $response = [];
    // делает выборку кромки по дереву
    if (isset($_POST['folder_id'])) {
        $folder_id = intval($_POST['folder_id']);
        $bands = band_get_band_by_tree($folder_id);
        $message = 'Нет кромки в этой категории';
    }
    // делает выборку кромки по свойствам
    if (isset($_POST['properties'])) {
        $properties = json_decode($_POST['properties'], true);
        $bands = band_get_band_by_properties($properties);
    }
    // делает выборку по коду
    if (isset($_POST['code'])) {
        $code = intval($_POST['code']);
        $bands = band_get_band_by_code($code);
        $message = "Нет кромки с таким кодом: $code";
    }
    // делает выборку по названию
    if (isset($_POST['name'])) {
        // от sql инъекций
        $name = str_replace($search, $replace, $_POST['name']);
        $bands = band_get_band_by_name($name);
        $name_out = htmlspecialchars($_POST['name']);
        $message = "Нет кромки с таким наименованием: $name_out";
    }

    if (!empty($bands)) {
        $response = [
            'error' => false,
            'result' => band_get_band_table($bands),
            'message' => '',
        ];
    } else {
        $response = [
            'error' => true,
            'result' => null,
            'message' => $message,
        ];
    }
    echo json_encode($response);
    die;
}

// загружает связь материалов с кромкой
if (isset($_POST['load_band_materials']) && $_POST['load_band_materials']) {
    $response = [];
    $band_id = intval($_POST['band_id']);
    $materials = band_get_band_mat_by_band_id($band_id);
    if (!empty($materials)) {
        $response = [
            'error' => false,
            'result' => good_get_relations_mat_table($materials, 'band'),
            'message' => '',
        ];
    } else {
        $response = [
            'error' => true,
            'result' => null,
            'message' => 'Ошибка сервера!',
        ];
    }
    echo json_encode($response);
    die;
}

// загружает список найденых материалов, если они найдены
if (isset($_POST['filter_mat'])) {
    $response = [];
    $filter_mat = str_replace($search, $replace, $_POST['filter_mat']);
    $materials = band_get_materials_by_filter($filter_mat);
    if (!empty($materials) && is_array($materials)) {
        $response = [
            'error' => false,
            'result' => band_get_list_of_materials($materials),
            'message' => "<p class='h5 text-primary'>количество найденных строк - " . count($materials) . '</p>',
        ];
    } else {
        $response = [
            'error' => true,
            'message' => 'Ничего не найдено',
        ];
    }
    echo json_encode($response);
    die;
}

// обновляет связь материалов с кромкой
if (isset($_POST['update_band_mat']) && $_POST['update_band_mat']) {
    $response = [];
    $band_ids = json_decode($_POST['bands'], true);
    $materials = json_decode($_POST['materials'], true);
    $bands = band_update_relations_with_mat($band_ids, $materials);
    if (!empty($bands)) {
        if (is_array($bands)) {
            $response = [
                'error' => false,
                'result' => band_get_band_table($bands),
                'message' => '',
            ];
        } else {
            $response = [
                'result' => $bands,
                'error' => true,
                'message' => $bands,
            ];
        }
    } else {
        $response = [
            'result' => '$bands',
            'error' => true,
            'message' => '$bands',
        ];
    }
    echo json_encode($response);
    die;
}

// deletes the relation between the band and the material
if (isset($_POST['del_relation']) && $_POST['del_relation']) {
    $sql = "DELETE FROM MATERIAL_BAND_CONNECTION WHERE  MATERIAL_ID={$_POST['mat_id']} AND BAND_ID={$_POST['band_id']}";
    if (sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)) {
        echo 'success';
        die;
    } else {
        echo 'error';
        die;
    }
}

