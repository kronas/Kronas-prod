<?php
session_start();
include_once('functions2.php');

if (!isset($_SESSION['user'])) {
   // $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';

    header('Location: ' . $main_dir . '/clients/RS/clients/create.php'.'?nw='.$_GET['nw']);
    exit();
} elseif($_SESSION['user']['role']!='admin' && $_SESSION['user']['role']!='manager'){
    $_SESSION['messages']['errors'][]='У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}

$title='Клиенты';
include_once('header.php');
?>
    <div class="row  align-items-center">
        <?php echo messages($_SESSION, "col-md-4 offset-md-4 col-sm-12"); ?>
    </div>
<?php
$def_value=array('','');
$total_records = 0;
$filtr = array();
$_url = 'index.php?';
if (isset($_GET['client_filtr_tel']) && strlen(trim($_GET['client_filtr_tel'])) == 17) {
    $filtr[] = 'client.tel = "' . str_replace(' ','', trim($_GET['client_filtr_tel'])) . '"';
    $_url .= 'client_filtr_tel=' . urlencode(trim($_GET['client_filtr_tel'])) . '&';
    $def_value[0]=' value="' . trim($_GET['client_filtr_tel']) . '" ';
}
if (isset($_GET['client_filtr_mail']) && strlen(trim($_GET['client_filtr_mail'])) >0) {
    $filtr[] = 'client.`e-mail` LIKE "%' . trim($_GET['client_filtr_mail']) . '%"';
    $_url .= 'client_filtr_mail=' . urlencode(trim($_GET['client_filtr_mail'])) . '&';
    $def_value[1]=' value="' . trim($_GET['client_filtr_mail']) . '" ';
}
?>
    <div class="row">
        <div class="col-md-8 offset-md-3 col-sm-12">

            <form class="form-inline" method="get" action="index.php<?='?nw='.$_GET['nw']?>">

                <div class="form-group mx-sm-3 mb-2">
                    <label for="client_search_tel" class="mr-4">Телефон клиента</label>
                    <input type="text" class="form-control phone" id="client_filtr_tel" name="client_filtr_tel"
                           data-mask="+38 (000) 000-00-00" placeholder="+38 (___) ___-__-__" <?=$def_value[0]?> >
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="client_filtr_mail" class="mr-4">E-Mail</label>
                    <input type="text" class="form-control " id="client_filtr_mail" name="client_filtr_mail" placeholder="name@example.com" <?=$def_value[1]?> >
                </div>
                <button type="submit" class="btn btn-primary mb-2">Фильтровать</button>
            </form>
        </div>
    </div>
<?php


$limit = 50;
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$start = $page == 1 ? 0 : (($limit * ($page - 1)));
$table = db_get_listclients($filtr, $start, $limit, $total_records);
if (is_null($table)) {
    echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div>';
} else {
    echo '<div class="row"><div class="col-12 pl-3 pr-3">';
    echo '<table class="table table-striped"><thead>    <tr>
        <th>#</th>
        <th>Имя</th>
        <th>№ тел.</th>
        <th>E-Mail</th>
        <th>...</th>
    </tr></thead><tbody>';
    foreach ($table as $row) {
        echo '<tr>
<td>' . $row['client_id'] . '</td>
<td>' . $row['name'] . '</td>
<td>' . $row['tel'] . '</td>
<td>' . $row['e-mail'] . '</td>
<td><a href="return.php?client=' . $row['client_id'] . '" title="Вернут" class="text-danger"><i class="fas fa-undo-alt"></i></a> 
<a href="into_production.php?client=' . $row['client_id'] . '" title="В производство"><i class="fas fa-dolly"></i></a> 
<a href="comig.php?client=' . $row['client_id'] . '" title="Приход" ><i class="fas fa-file-import"></i></a></td>
    </tr>';
    }
    echo '</tbody></table>';
    if ($total_records > $limit) echo get_list2browse_pager2($_url, $page, $limit, $total_records);
    echo '</div></div>';
}
?>
<?php include_once('footer.php'); ?>