<?php
function loadDataOfCurrentPart($data, $index) {
    $init_data = parse_ini_file('../config/php.ini');
    $url = $init_data['load_data_of_current_part'];

    $current_part = [
        $index => $data,
    ];
    $jsonCurrentPart = json_encode($current_part, JSON_UNESCAPED_UNICODE);
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonCurrentPart);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonCurrentPart))
    );
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}