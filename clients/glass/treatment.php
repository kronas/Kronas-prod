<?php
session_start();

include_once '../../_1/config.php';
include '../functions2.php';
include 'functions.php';
$ini_data = glass_parse_all_settings();

// If there is no order, then it redirects to the main page
if (!isset($_SESSION['order_st'])) {
    header('Location: /clients/glass/index.php'.'?nw='.$_GET['nw']);
    $error = "<div class='alert alert-danger text-center'>Выберете материал для заказа</div>";
    $_SESSION['error'] = $error;
    die;
}

$order_st = json_decode($_SESSION['order_st'], true);

// If there is no a number of the detail in a GET request
if (isset($_GET['num_detail']) && isset($order_st['parts'][$_GET['num_detail']])) {
    $numCurrentPart = $_GET['num_detail'];
    $currentPart = $order_st['parts'][$_GET['num_detail']];
} else {
    header('Location: /clients/glass/details.php'.'?nw='.$_GET['nw']);
    $error = "<div class='alert alert-danger text-center'>Такой детали нет</div>";
    $_SESSION['error'] = $error;
    die;
}
$optionsHandle = [
    "shlif" => 'Шлифовка',
    'poli' => 'Полировка',
    'facet' => 'Фацет',
];
$optionsFacet = [
    'strai' => 'Прямо',
    'curve' => 'Криволинейно',
];
$material = json_decode($_SESSION['materials'], true);
//p_($order_st);
//print_r($material);
$title = 'Конструктор стекла';
include "$serv_main_dir/clients/header.php";
?>
<!-- start/ content -->
<div class="container-fluid">

    <div class="row">
        <!-- start/ toolbar-->
        <div class="col-sm-12 nav-tabs" id="toolbar">
            <div class="row">
                <div class="col-sm-3 d-flex flex-row justify-content-around">
                    <a class="btn btn-sm btn-dark" href="details.php"><i class="fab fa-buffer"></i> Назад к деталям</a>
                    <a class="btn btn-sm btn-primary" href="<?= $currentPart['path'][0] ?>" target="_blank" id="pdfCurrentPart"><i class="fas fa-search-plus"></i> Скачать чертеж</a>
                </div>
                <div class="col-sm-5">
                    <h5 class="text-center">Деталь № <?= $numCurrentPart ?>: длина - <span
                                id="partL"><?= $currentPart['l'] ?></span> мм; ширина - <span
                                id="partW"><?= $currentPart['w'] ?></span> мм; толщина - <span
                                id="partHeight"><?= $material['height'] ?></span> мм.</h5>
                </div>
                <div class="col-sm-4">
                    <span class="hidden" id="maxSizeForEdgeWithRoundCorner">
                        <?= $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1] ?>
                    </span>
                    <span class="hidden" id="maxSizeForEdge">
                        <?= $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1] ?>
                    </span>
                    <span class="hidden" id="minSizeForEdge">
                        <?= $ini_data['min_size_for_edge'] ?>
                    </span>
                    <span class="hidden" id="minSizeForEdgeWithRoundCorner">
                        <?= $ini_data['min_size_for_edge_with_rounded_corner'] ?>
                    </span>
                    <div class="nav d-flex flex-row justify-content-end" id="nav-tab" role="tablist">
                        <?php
                            if (
                                    isset($currentPart['operations']['corner']['ru']['radius']) ||
                                    isset($currentPart['operations']['corner']['lu']['radius']) ||
                                    isset($currentPart['operations']['corner']['rd']['radius']) ||
                                    isset($currentPart['operations']['corner']['ld']['radius'])
                            ) {
                                $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                                $min_side = $ini_data['min_size_for_edge_with_rounded_corner'];
                            } else {
                                $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                                $min_side = $ini_data['min_size_for_edge'];

                            }
                            $current_related = $currentPart['l'] * $currentPart['w'];
                            $big_size_for_edges = ($current_related <= $max_related);
                            $small_size_for_edges = ($current_related >= $min_side ** 2);
                        ?>
                        <a class="btn-sm btn-light mr-1 nav-link <?php
                        if ($big_size_for_edges && $small_size_for_edges) echo 'active';
                        if (!$big_size_for_edges || !$small_size_for_edges) echo 'hidden';
                        ?>" data-toggle="tab" id="treatmentSides"
                           href="#nav-treatment-sides" role="tab" aria-controls="nav-treatment-sides"
                           aria-selected="true"
                           title="Оработка сторон">
                            <i class="fas fa-border-style text-primary"></i>
                        </a>

                        <a class="btn-sm btn-light mr-1 nav-link <?php
                        if (!$big_size_for_edges || !$small_size_for_edges) echo 'active';
                        ?>" data-toggle="tab" id="rСutCorners"
                           title="Скругление углов"
                           href="#nav-radius-cut-corners" role="tab" aria-controls="nav-radius-cut-corners"
                           aria-selected="false">
                            <i class="fas fa-stop text-primary"></i>
                        </a>

                        <a class="btn-sm btn-light mr-1 nav-link" data-toggle="tab" id="srezCorners" title="Срез углов"
                           href="#nav-srez-corners" role="tab" aria-controls="nav-srez-corners" aria-selected="false">
                            <i class="fas fa-sticky-note text-primary"></i>
                        </a>

                        <a class="btn-sm btn-light mr-1 nav-link" data-toggle="tab" id="zarezCorners"
                           title="Зарез углов"
                           href="#nav-zarez-corners" role="tab" aria-controls="nav-zarez-corners" aria-selected="false">
                            <i class="fas fa-box text-primary"></i>
                        </a>

                        <a class="btn-sm btn-light mr-1 nav-link" data-toggle="tab" id="bore" title="Сверление"
                           href="#nav-bore" role="tab" aria-controls="nav-bore" aria-selected="false">
                            <i class="fas fa-bullseye text-primary"></i>
                        </a>

                        <a class="btn-sm btn-light mr-1 nav-link" data-toggle="tab" id="boreOval"
                           title="Сверление овальное"
                           href="#nav-bore-oval" role="tab" aria-controls="nav-bore-oval" aria-selected="false">
                            <i class="fas fa-plug text-primary"></i>
                        </a>

                        <a class="btn-sm btn-light mr-1 nav-link" data-toggle="tab" id="surfaceTreatment"
                           title="Обработка поверхности детали"
                           href="#nav-surface-treatment" role="tab" aria-controls="nav-surface-treatment"
                           aria-selected="false">
                            <i class="fas fa-paint-roller text-primary"></i>
                        </a>
                        <a class="btn-sm btn-light mr-1 nav-link" data-toggle="tab" id="surfaceTreatment"
                           title="Декорация обратной стороны детали"
                           href="#nav-decoration-back-side" role="tab" aria-controls="nav-decoration-back-side"
                           aria-selected="false">
                            <i class="fas fa-image text-primary"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div> <!-- end/ toolbar-->
        <!-- start/ draw-->
        <div class="col-sm-9 text-center justify-content-center" id="draw">
            <img src="<?= $currentPart['path'][1] ?>" class="img-current-part d-none" id="imgCurrentPart">
        </div>
        <!-- end/ draw-->
        <!-- start/ tab-content-->
        <div class="col-sm-3 right-panel overflow">
            <div class="tab-content" id="nav-tabContent">
                <!-- start/ treatment-sides -->
                <div class="tab-pane fade <?php
                if ($big_size_for_edges && $small_size_for_edges) echo 'show active';
                if (!$big_size_for_edges || !$small_size_for_edges) echo 'hidden';
                ?>" id="nav-treatment-sides" role="tabpanel"
                     aria-labelledby="nav-treatment-sides">
                    <h5 class="m-2">Обработка торцов</h5>
                    <form>
                        <input type="hidden" id="maxRelated" value="<?= $max_related ?>">
                        <input type="hidden" id="maxFacet" value="<?php
                            if ($material['height'] <= 4) {
                                echo $ini_data['to_4mm'];
                            } elseif($material['height'] > 4) {
                                echo $ini_data['over_4mm'];
                            }
                        ?>">
                        <input type="hidden" name="currentPart" value="<?=$numCurrentPart?>">
                        <div class="form-group <?php
                         if (!isset($currentPart['operations']['corner']['lu']['radius']) && !isset($currentPart['operations']['corner']['ld']['radius'])) {
                             $min_size = $ini_data['min_size_for_edge'];
                             $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                         } else {
                             $min_size = $ini_data['min_size_for_edge_with_rounded_corner'];
                             $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                         }
                         if ($current_related > $max_related || $current_related < ($min_size ** 2)) {
                             echo 'hidden';
                         }
                        ?>" id="divEdgeLeft">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="l_edge">Лево</label>
                                </div>
                                <div class="col-sm-9 mb-3">
                                    <select class="form-control select-for-edge" id="l_edge" name="l_edge">
                                        <option>-Нет-</option>
                                        <?php
                                        foreach ($optionsHandle as $key => $val) {
                                            ?>
                                            <option
                                                <?php
                                                if (isset($currentPart['l_edge']) && $currentPart['l_edge'] === $key) {
                                                    echo 'selected ';
                                                }
                                                ?>value="<?= $key ?>"><?= $val ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="d-flex flex-row flex-wrap
                                    <?php
                                    if (!isset($currentPart['l_facet'])) echo ' hidden';
                                    ?> addedFields">
                                    <div class="col-sm-6">
                                        <label for="length">Ширина фацета, мм</label>
                                    </div>
                                    <div class='col-sm-6'>
                                        <input type="number" class="form-control" value="<?php
                                        if (isset($currentPart['l_facet']['l_length'])) {
                                            echo $currentPart['l_facet']['l_length'];
                                        }
                                        ?>" name="l_length">
                                    </div>
                                </div>
                                <div class="message-added-fields"></div>
                            </div> <!-- end/ row -->
                        </div>
                        <div class="form-group <?php
                        if (!isset($currentPart['operations']['corner']['ru']['radius']) &&
                            !isset($currentPart['operations']['corner']['rd']['radius'])) {
                            $min_size = $ini_data['min_size_for_edge'];
                            $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                        } else {
                            $min_size = $ini_data['min_size_for_edge_with_rounded_corner'];
                            $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                        }
                        if ($current_related > $max_related || $current_related < ($min_size ** 2)) {
                            echo 'hidden';
                        }
                        ?>" id="divEdgeRight">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="r_edge">Право</label>
                                </div>
                                <div class="col-sm-9 mb-3">
                                    <select class="form-control select-for-edge" id="r_edge" name="r_edge">
                                        <option>-Нет-</option>
                                        <?php
                                        foreach ($optionsHandle as $key => $val) {
                                            ?>
                                            <option
                                                <?php
                                                if (isset($currentPart['r_edge']) && $currentPart['r_edge'] === $key) {
                                                    echo 'selected ';
                                                }
                                                ?>value="<?= $key ?>"><?= $val ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="d-flex flex-row flex-wrap addedFields<?php if (!isset($currentPart['r_facet'])) echo ' hidden';?>">
                                    <div class="col-sm-6">
                                        <label for="length">Ширина фацета, мм</label>
                                    </div>
                                    <div class='col-sm-6'>
                                        <input type="number" class="form-control" value="<?php
                                        if (isset($currentPart['r_facet']['r_length'])) {
                                            echo $currentPart['r_facet']['r_length'];
                                        }
                                        ?>" name="r_length">
                                    </div>
                                </div>
                                <div class="message-added-fields"></div>
                            </div>
                        </div>
                        <div class="form-group <?php
                        if (!isset($currentPart['operations']['corner']['lu']['radius']) &&
                            !isset($currentPart['operations']['corner']['ru']['radius'])) {
                            $min_size = $ini_data['min_size_for_edge'];
                            $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                        } else {
                            $min_size = $ini_data['min_size_for_edge_with_rounded_corner'];
                            $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                        }
                        if ($current_related > $max_related || $current_related < ($min_size ** 2)) {
                            echo 'hidden';
                        }
                        ?>" id="divEdgeTop">
                            <div class="row">
                                <div class="col-sm-3 mb-3">
                                    <label for="t_edge">Верх</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select-for-edge" id="t_edge" name="t_edge">
                                        <option>-Нет-</option>
                                        <?php
                                        foreach ($optionsHandle as $key => $val) {
                                            ?>
                                            <option
                                                <?php
                                                if (isset($currentPart['t_edge']) && $currentPart['t_edge'] === $key) {
                                                    echo 'selected ';
                                                }
                                                ?>value="<?= $key ?>"><?= $val ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="d-flex flex-row flex-wrap addedFields <?php if (!isset($currentPart['t_facet'])) echo ' hidden';?>">
                                    <div class="col-sm-6">
                                        <label for="length">Ширина фацета, мм</label>
                                    </div>
                                    <div class='col-sm-6'>
                                        <input type="number" class="form-control" value="<?php if (isset($currentPart['t_facet']['t_length'])) echo $currentPart['t_facet']['t_length'];?>" name="t_length">
                                    </div>
                                </div>
                                <div class="message-added-fields"></div>
                            </div>
                        </div>
                        <div class="form-group <?php
                        if (!isset($currentPart['operations']['corner']['ld']['radius']) &&
                            !isset($currentPart['operations']['corner']['rd']['radius'])) {
                            $min_size = $ini_data['min_size_for_edge'];
                            $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                        } else {
                            $min_size = $ini_data['min_size_for_edge_with_rounded_corner'];
                            $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                        }
                        if ($current_related > $max_related || $current_related < ($min_size ** 2)) {
                            echo 'hidden';
                        }
                        ?>" id="divEdgeBottom">
                            <div class="row">
                                <div class="col-sm-3 mb-3">
                                    <label for="b_edge">Низ</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select-for-edge" id="b_edge" name="b_edge">
                                        <option>-Нет-</option>
                                        <?php
                                        foreach ($optionsHandle as $key => $val) {
                                            ?>
                                            <option
                                                <?php
                                                if (isset($currentPart['b_edge']) && $currentPart['b_edge'] === $key) {
                                                    echo 'selected ';
                                                }
                                                ?>value="<?= $key ?>"><?= $val ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="d-flex flex-row flex-wrap addedFields<?php if (!isset($currentPart['b_facet'])) echo ' hidden'; ?>">
                                    <div class="col-sm-6">
                                        <label for="length">Ширина фацета, мм</label>
                                    </div>
                                    <div class='col-sm-6'>
                                        <input type="number" class="form-control" value="<?php
                                        if (isset($currentPart['b_facet']['b_length'])) echo $currentPart['b_facet']['b_length'];
                                        ?>" name="b_length">
                                    </div>
                                </div>
                                <div class="message-added-fields"></div>
                            </div>
                        </div>
                        <button id="edgeSubmit" name="edge" class="btn btn-primary mb-3">Сохранить</button>
                        <div class="row justify-content-around">
                            <div class="col-sm-8 mb-2">Полировка </div>
                            <div class="col-sm-2 mb-2" style="background-color: #4df15942;"></div>
                            <div class="col-sm-8 mb-2">Шлифовка </div>
                            <div class="col-sm-2 mb-2" style="background-color: #5643FAF1;"></div>
                            <div class="col-sm-8 mb-2">Фацет </div>
                            <div class="col-sm-2 mb-2" style="background-color: #f3d2d2;"></div>
                        </div>
                    </form>
                </div> <!-- end/ treatment-sides -->
                <!-- start/ radius-cut-corners -->
                <div class="tab-pane fade <?php
                if (!$big_size_for_edges || !$small_size_for_edges) echo 'show active';
                ?>" id="nav-radius-cut-corners" role="tabpanel"
                     aria-labelledby="nav-radius-cut-corners">
                    <h5 class="mb-3">Радиус скругления углов</h5>
                    <form>
                        <div class="form-group lu" id="radiusLu">
                            <label for="radius_lu">Левый верхний, мм</label>
                            <input type="number" class="form-control radius" name="radius_lu"
                            value="<?php
                                if (isset($currentPart['operations']['corner']['lu']['radius'])) echo $currentPart['operations']['corner']['lu']['radius'];
                            ?>">
                            <div class="messageForRoundingCorner"></div>
                        </div>
                        <div class="form-group ld" id="radiusLd">
                            <label for="radius_ld">Левый нижний, мм</label>
                            <input type="number" class="form-control radius" name="radius_ld"
                                   value="<?php
                                   if (isset($currentPart['operations']['corner']['ld']['radius'])) echo $currentPart['operations']['corner']['ld']['radius'];
                                   ?>">
                            <div class="messageForRoundingCorner"></div>
                        </div>
                        <div class="form-group ru" id="radiusRu">
                            <label for="radius_ru">Правый верхний, мм</label>
                            <input type="number" class="form-control radius" name="radius_ru"
                                   value="<?php
                                   if (isset($currentPart['operations']['corner']['ru']['radius'])) echo $currentPart['operations']['corner']['ru']['radius'];
                                   ?>">
                            <div class="messageForRoundingCorner"></div>
                        </div>
                        <div class="form-group rd" id="radiusRd">
                            <label for="radius_rd">Правый нижний, мм</label>
                            <input type="number" class="form-control radius" name="radius_rd"
                                   value="<?php
                                   if (isset($currentPart['operations']['corner']['rd']['radius'])) echo $currentPart['operations']['corner']['rd']['radius'];
                                   ?>">
                            <div class="messageForRoundingCorner"></div>
                        </div>
                        <button id="radiusSubmit" name="radius" class="btn btn-primary">Сохранить</button>
                    </form>
                </div> <!-- start/ radius-cut-corners -->
                <!-- start/ srez-corners -->
                <div class="tab-pane fade " id="nav-srez-corners" role="tabpanel"
                     aria-labelledby="nav-srez-corners">
                    <h5 class="mb-2">Срез углов</h5>
                    <form>
                        <!-- start/ left up -->
                        <div class="form-group row pr-3 lu" id="srezLu">
                            <p class="col-sm-12 h6 text-center">Левый верхний</p>
                            <label class="col-sm-8 mb-3" for="lu_x2">Смещение по Х мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['lu']['srez']['x2']))
                                echo $currentPart['operations']['corner']['lu']['srez']['x2'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="lu_x2" id="lu_x2">
                            <label class="col-sm-8" for="lu_y1">Смещение по Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['lu']['srez']['y1']))
                                echo $currentPart['operations']['corner']['lu']['srez']['y1'];
                            ?>" type="number" class="col-sm-4 form-control" name="lu_y1" id="lu_y1">
                        </div><!-- end/ left up -->
                        <!-- start/ left down -->
                        <div class="form-group row pr-3 ld" id="srezLd">
                            <p class="col-sm-12 h6 text-center">Левый нижний</p>
                            <label class="col-sm-8 mb-3" for="ld_x1">Смещение по Х мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ld']['srez']['x1']))
                                echo $currentPart['operations']['corner']['ld']['srez']['x1'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="ld_x1" id="ld_x1">
                            <label class="col-sm-8 mb-3" for="ld_y2">Смещение по Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ld']['srez']['y2']))
                                echo $currentPart['operations']['corner']['ld']['srez']['y2'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="ld_y2" id="ld_y2">
                        </div> <!-- end/ left down -->
                        <!-- start/ right up -->
                        <div class="form-group row pr-3 ru" id="srezRu">
                            <p class="col-sm-12 h6 text-center">Правый верхний</p>
                            <label class="col-sm-8 mb-3" for="ru_x1">Смещение по X мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ru']['srez']['x1']))
                                echo $currentPart['operations']['corner']['ru']['srez']['x1'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="ru_x1" id="ru_x1">
                            <label class="col-sm-8 mb-3" for="ru_y2">Смещение по Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ru']['srez']['y2']))
                                echo $currentPart['operations']['corner']['ru']['srez']['y2'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="ru_y2" id="ru_y2">
                        </div> <!-- end/ right up -->
                        <!-- start/ right down -->
                        <div class="form-group row pr-3 rd" id="srezRd">
                            <p class="col-sm-12 h6 text-center">Правый нижний</p>
                            <label class="col-sm-8 mb-3" for="rd_x2">Смещение по X мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['rd']['srez']['x2']))
                                echo $currentPart['operations']['corner']['rd']['srez']['x2'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="rd_x2" id="rd_x2">
                            <label class="col-sm-8 mb-3" for="rd_y1">Смещение по Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['rd']['srez']['y1']))
                                echo $currentPart['operations']['corner']['rd']['srez']['y1'];
                            ?>" type="number" class="col-sm-4 mb-3 form-control" name="rd_y1" id="rd_y1">
                        </div> <!-- end/ right down -->

                        <button id="srezSubmit" name="srez" class="btn btn-primary">Сохранить</button>
                    </form>
                </div> <!-- start/ srez-corners -->
                <!-- start/ zarez-corners -->
                <div class="tab-pane fade " id="nav-zarez-corners" role="tabpanel"
                     aria-labelledby="nav-zarez-corners">
                    <h5 class="mb-2">Зарез углов</h5>
                    <form>
                        <div class="form-group row pr-3 lu" id="zarezLu">
                            <p class="col-sm-12">Левый верхний</p>
                            <label class="col-sm-3" for="lu_x">X мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['lu']['zarez']['x']))
                                echo $currentPart['operations']['corner']['lu']['zarez']['x'];
                            ?>" type="number" class="col-sm-3 form-control" name="lu_x" id="lu_x">
                            <label class="col-sm-3" for="lu_y">Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['lu']['zarez']['y']))
                                echo $currentPart['operations']['corner']['lu']['zarez']['y'];
                            ?>" type="number" class="col-sm-3 form-control" name="lu_y" id="lu_y">
                        </div>

                        <div class="form-group row pr-3 ld" id="zarezLd">
                            <p class="col-sm-10">Левый нижний</p>
                            <label class="col-sm-3" for="ld_x">X мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ld']['zarez']['x']))
                                echo $currentPart['operations']['corner']['ld']['zarez']['x'];
                            ?>" type="number" class="col-sm-3 form-control" name="ld_x" id="ld_x">
                            <label class="col-sm-3" for="ld_y">Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ld']['zarez']['y']))
                                echo $currentPart['operations']['corner']['ld']['zarez']['y'];
                            ?>" type="number" class="col-sm-3 form-control" name="ld_y" id="ld_y">
                        </div>
                        <div class="form-group row pr-3 ru" id="zarezRu">
                            <p class="col-sm-10">Правый верхний</p>
                            <label class="col-sm-3" for="ru_x">X мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ru']['zarez']['x']))
                                echo $currentPart['operations']['corner']['ru']['zarez']['x'];
                            ?>" type="number" class="col-sm-3 form-control" name="ru_x" id="ru_x">
                            <label class="col-sm-3" for="ru_y">Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['ru']['zarez']['y']))
                                echo $currentPart['operations']['corner']['ru']['zarez']['y'];
                            ?>" type="number" class="col-sm-3 form-control" name="ru_y" id="ru_y">
                        </div>
                        <div class="form-group row pr-3 rd" id="zarezRd">
                            <p class="col-sm-10">Правый нижний</p>
                            <label class="col-sm-3" for="rd_x">X мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['rd']['zarez']['x']))
                                echo $currentPart['operations']['corner']['rd']['zarez']['x'];
                            ?>" type="number" class="col-sm-3 form-control" name="rd_x" id="rd_x">
                            <label class="col-sm-3" for="rd_y">Y мм</label>
                            <input value="<?php
                            if (isset($currentPart['operations']['corner']['rd']['zarez']['y']))
                                echo $currentPart['operations']['corner']['rd']['zarez']['y'];
                            ?>" type="number" class="col-sm-3 form-control" name="rd_y" id="rd_y">
                        </div>
                        <button id="zarezSubmit" name="zarez" class="btn btn-primary">Сохранить</button>
                    </form>
                </div> <!-- start/ zarez-corners -->
                <!-- start/ bore -->
                <div class="tab-pane fade " id="nav-bore" role="tabpanel"
                     aria-labelledby="nav-bore">
                    <h5 class="mb-2 mt-3" id="titleBore">Сверление отверстий</h5>
                    <form class=" mb-3">
                        <div class="form-group row">
                            <label class="col-sm-4" for="bore_x">X мм</label>
                            <input type="number" class="col-sm-6 form-control" name="bore_x" id="bore_x">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4" for="bore_y">Y мм</label>
                            <input type="number" class="col-sm-6 form-control" name="bore_y" id="bore_y">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4" for="bore_d">Диаметр мм</label>
                            <select type="number" class="col-sm-6 form-control" name="bore_d" id="bore_d">
                                <option></option>
                                <?php
                                    $diameters = $ini_data['hole_diameter'];
                                    foreach ($diameters as $d) {
                                        ?>
                                        <option value="<?=$d?>"><?=$d?> мм</option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-6 col-form-label" for="zenk">Зенкование</label>
                            <input type="checkbox" class="col-sm-2 form-check-input position-relative mt-3" name="zenk"
                                   id="zenk">
                        </div>
                        <button id="boreSubmit" name="bore" class="btn btn-primary">Добавить</button>
                        <button id="boreEdit" name="bore" class="btn btn-primary hidden" disabled>Редактировать</button>
                    </form>
                    <div class="bore-list">
                        <?php
                        if (isset($currentPart['operations']['face']['bore'])) {
                            $bores = $currentPart['operations']['face']['bore'];
                            $part = $currentPart;
                            include $serv_main_dir . '/clients/glass/layouts/bore_list.php';
                        }
                        ?>
                    </div>
                </div> <!-- start/ bore -->
                <!-- start/ bore-oval -->
                <div class="tab-pane fade " id="nav-bore-oval" role="tabpanel"
                     aria-labelledby="nav-bore-oval">
                    <h5 class="mb-2" id="titleOval">Сверление отверстий под розетку</h5>
                    <p class="text-danger">Сверление под розетку - это 2 отверстия радиусом 30 мм, которые потом
                        соединяются резом</p>
                        <p>Координаты первого отверстия</p>
                        <div class="form-group row">
                            <label class="col-sm-4" for="oval_x1">X<sub>1</sub> мм</label>
                            <input type="number" class="col-sm-6 form-control" name="oval_x1" id="oval_x1">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4" for="oval_y1">Y<sub>1</sub> мм</label>
                            <input type="number" class="col-sm-6 form-control" name="oval_y1" id="oval_y1">
                        </div>
                        <p>Координаты второго отверстия</p>
                        <div class="form-group row">
                            <label class="col-sm-4" for="oval_x2">X<sub>2</sub> мм</label>
                            <input type="number" class="col-sm-6 form-control" name="oval_x2" id="oval_x2">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4" for="oval_x2">Y<sub>2</sub> мм</label>
                            <input type="number" class="col-sm-6 form-control" name="oval_y2" id="oval_x2">
                        </div>
                        <button id="ovalSubmit" name="oval" class="btn btn-primary">Добавить</button>
                        <button id="ovalEdit" name="oval" class="btn btn-primary hidden" disabled>Редактировать</button>
                        <div class="oval-list">
                            <?php
                            if (isset($currentPart['operations']['face']['oval'])) {
                                $ovals = $currentPart['operations']['face']['oval'];
                                $part = $currentPart;
                                include $serv_main_dir . '/clients/glass/layouts/oval_list.php';
                            }
                            ?>
                        </div>
                </div> <!-- start/ bore-oval -->
                <!-- start/ nav-surface-treatment -->
                <div class="tab-pane fade " id="nav-surface-treatment" role="tabpanel"
                     aria-labelledby="nav-surface-treatment">
                    <h5 class="mb-2 h6">Работа с поверхностью детали</h5>
                    <form id="surfaceForm">
                        <div class="form-group row">
                            <label class="col-sm-10 col-form-label" for="mat">Матирование поверхности</label>
                            <input type="checkbox" <?php
                            if (isset($currentPart['operations']['mat']) && $currentPart['operations']['mat']) echo 'checked';
                            ?> class="col-sm-2 form-check-input position-relative mt-3" name="mat">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-10 col-form-label" for="gfh">Нанесение гидрофобной жидкости</label>
                            <input type="checkbox" <?php
                            if (isset($currentPart['operations']['gfh']) && $currentPart['operations']['gfh']) echo 'checked';
                            ?> class="col-sm-2 form-check-input position-relative mt-3" name="gfh">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-10 col-form-label" for="film">Поклейка защитной пленки</label>
                            <input type="checkbox" <?php
                            if (isset($currentPart['operations']['film']) && $currentPart['operations']['film']) echo 'checked';
                            ?> class="col-sm-2 form-check-input position-relative mt-3" name="film">
                        </div>
                        <div class="form-group">
                            <label for="oracal">Наклейка плёнки оракал</label>
                            <select name="oracal" id="oracal" class="form-control">
                                <option>-Нет-</option>
                                <?php
                                $oracals = glass_get_all_oracals();
                                foreach ($oracals as $oracal) {
                                    $checked = '';
                                    if (isset($currentPart['operations']['oracal']) &&
                                        $currentPart['operations']['oracal'] === $oracal['CODE']) $checked = 'selected';
                                    echo "<option value='{$oracal['CODE']}' $checked>{$oracal['NAME']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                        $aspect_ratio_ini_photo_print = $ini_data['max_size_for_photo_print'][0] * $ini_data['max_size_for_photo_print'][1];
                        $aspect_ratio_current = $currentPart['l'] * $currentPart['w'];
                        $aspect_ratio_ini_sandblasting = $ini_data['max_size_for_sandblasting'][0] * $ini_data['max_size_for_sandblasting'][1];
                        ?>
                        <div class="form-group">
                            <label for="options_decoration">Декорация поверхности</label>
                            <select name="decoration_options" id="decoration_options" class="form-control select-decoration-side">
                                <option>-Нет-</option>
                                <?php
                                $optionsFront = [
                                    'photo_print' => 'photo_print_client',
                                    'UF_print' => 'UF_photo_print_client',
                                    'UF_print_white'=> 'UF_photo_print_client_white',
                                    'sand_file' => 'sand_client',
                                    'amalgama' => 'amalgama_client',
                                ];
                                $hidden = 'hidden';
                                foreach ($optionsFront as $key => $value) {
                                    if (isset($currentPart['operations'][$key])) {
                                        $imgPathFront = $currentPart['operations'][$key];
                                        $hidden = '';
                                    }
                                    if (isset($currentPart['operations'][$value])) {
                                        $imgPathFront = $currentPart['operations'][$value];
                                        $hidden = '';
                                    }
                                }
                                if ($aspect_ratio_current <= $aspect_ratio_ini_photo_print) {
                                    ?>
                                        <option value="photo_print" <?php
                                        if (isset($currentPart['operations']['photo_print']) ||
                                            isset($currentPart['operations']['photo_print_client'])) echo 'selected';
                                        ?>>Фотопечать</option>
                                        <option value="UF_print" <?php
                                        if (isset($currentPart['operations']['UF_print']) ||
                                            isset($currentPart['operations']['UF_photo_print_client'])) echo 'selected';
                                        ?>>УФ печать</option>
                                        <option value="UF_print_white" <?php
                                        if (isset($currentPart['operations']['UF_print_white']) ||
                                            isset($currentPart['operations']['UF_photo_print_client_white'])) echo 'selected';
                                        ?>>УФ печать с заливкой белого фона</option>
                                    <?php
                                }
                                ?>

                                <?php
                                    if ($aspect_ratio_current <= $aspect_ratio_ini_sandblasting) {
                                        $selected = (isset($currentPart['operations']['sand_file']) ||
                                            isset($currentPart['operations']['sand_client']))? "selected" : "";
                                        echo "<option value=\"sand_file\" $selected>Пескоструй</option>";
                                    }

                                    if ($order_st['order_st']['data_material']['type_material_name'] === "Зеркало") {
                                ?>
                                <option value="amalgama_back" <?php
                                if (isset($currentPart['operations']['amalgama']) ||
                                    isset($currentPart['operations']['amalgama_client'])) echo 'selected';
                                ?>>Художественное снятие амальгамы</option> <?php } ?>
                            </select>
                            <div class="result"></div>
                            <div class="loadImg <?= $hidden ?> mt-3"><img src="<?= $imgPathFront ?>" width="50px" height="50px"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-10 col-form-label" for="film_art">Наклейка плёнки Frost</label>
                            <input type="checkbox" <?php
                            if (isset($currentPart['operations']['film_art']) && $currentPart['operations']['film_art']) echo 'checked';
                            ?> class="col-sm-2 form-check-input position-relative mt-3" name="film_art">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-10 col-form-label" for="zakalka">Закалка стекла</label>
                            <input type="checkbox" <?php
                            if (isset($currentPart['operations']['zakalka']) && $currentPart['operations']['zakalka']) echo 'checked';
                            ?> class="col-sm-2 form-check-input position-relative mt-3" name="zakalka">
                        </div>
                        <button type="submit" name="surface-treatment" id="surfaceTreatmentSubmit" class="btn btn-primary">Добавить
                        </button>
                    </form>
                </div> <!-- end/ nav-surface-treatment -->
                <!-- start/ nav-decoration-back-side -->
                <div class="tab-pane fade " id="nav-decoration-back-side" role="tabpanel"
                     aria-labelledby="nav-decoration-back-side">
                    <h5 class="mb-2 h6">Работа с обратной стороной поверхности детали</h5>
                    <form id="backSide">
                        <div class="form-group">
                            <label for="send_file_back">Декорация обратной стороны</label>
                            <select name="send_file_back" id="send_file_back" class="form-control select-decoration-side">
                                <option>-Нет-</option>
                                <?php
                                $options = [
                                  'sand_file_back', 'sand_client_back', 'amalgama_back', 'amalgama_client_back',
                                ];
                                $hidden = 'hidden';
                                foreach ($options as $option) {
                                    if (isset($currentPart['operations'][$option])) {
                                        $imgPath = $currentPart['operations'][$option];
                                        $hidden = '';
                                    }
                                }
                                if ($aspect_ratio_current <= $aspect_ratio_ini_sandblasting)
                                    $selected = (isset($currentPart['operations']['sand_file_back']) ||
                                        isset($currentPart['operations']['sand_client_back']))? "selected" : "";
                                    echo "<option value=\"sand_file_back\" $selected>Пескоструй</option>";
                                if ($order_st['order_st']['data_material']['type_material_name'] === "Зеркало") {
                                ?>
                                <option value="amalgama_back" <?php
                                if (isset($currentPart['operations']['amalgama_back']) ||
                                isset($currentPart['operations']['amalgama_client_back'])) echo 'selected';
                                ?>>Художественное снятие амальгамы</option> <?php } ?>
                            </select>
                            <div class="result"></div>
                            <div class="loadImg <?= $hidden ?> mt-3"><img src="<?= $imgPath ?>" width="50px" height="50px"></div>
                        </div>
                    </form>
                </div> <!-- end/ nav-decoration-back-side -->
            </div>
        </div><!-- end/ tab-content-->
    </div>

</div> <!-- end/ content -->
<!-- start/ modal -->
<div class="modal fade " id="photoGallery" aria-labelledby="photoGalleryTitle"  tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable photo-gallery" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="photoGalleryTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body row" id="serves_library">

            </div>
            <div class="modal-footer">
                <div class="mr-5" id="image-path"></div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="saveImage">Загрузить</button>
            </div>
        </div>
    </div>
</div>
<?php
include '../footer.php';

