<?php
session_start();
include $_SERVER['DOCUMENT_ROOT'] . '/_1/config.php';
// If there is no order in the SESSION, it redirects to the home page to create a new order
if (!isset($_SESSION['order_st'])) {
    header('Location: /'.'?nw='.$_GET['nw']);
    $error = "<div class='alert alert-danger text-center'>Выберете материал для заказа</div>";
    $_SESSION['error'] = $error;
    die;
} else {
    // Retrieved from SESSION and decoded json into an array for adding or editing data
    $order_st = json_decode($_SESSION['order_st'], true);
}

// Edits in Json the dimensions and quantity of the part according to the number in the order
if (
    isset($_POST['width']) && isset($_POST['length']) && isset($_POST['quantity']) && isset($_POST['num_detail'])
    && !is_null($_POST['width']) && !is_null($_POST['length']) && !is_null($_POST['quantity']) && !is_null($_POST['num_detail'])
) {
    $width = intval($_POST['width']);
    $length = intval($_POST['length']);
    $quantity = intval($_POST['quantity']);
    $currentKey = intval($_POST['num_detail']);

    $order_st['parts'][$currentKey]['l'] = $length;
    $order_st['parts'][$currentKey]['w'] = $width;
    $order_st['parts'][$currentKey]['q'] = $quantity;

}

// Adds or edits data to handel edges of the part in Json
if (isset($_POST['edges']) && !is_null($_POST['edges']) && isset($_POST['num_detail']) && !is_null($_POST['num_detail'])) {
    $currentKey = intval($_POST['num_detail']);

    if (isset($_POST['l_edge']) && !is_null($_POST['l_edge'])) {
        $edge = $_POST['l_edge'];
        $order_st['parts'][$currentKey]['l_edge'] =  $edge;
        if (
            isset($order_st['parts'][$currentKey]['operations']['corner']['lu']['radius']) ||
            isset($order_st['parts'][$currentKey]['operations']['corner']['ld']['radius'])
        ) {
            $order_st['parts'][$currentKey]['l_type'] = 'curve';
        } else {
            $order_st['parts'][$currentKey]['l_type'] = 'strait';
        }

        if (isset($order_st['parts'][$currentKey]['l_facet'])) unset($order_st['parts'][$currentKey]['l_facet']);
        if ($edge === "facet") {
            if (isset($_POST['l_length']) && !is_null($_POST['l_length'])) {
                $length = intval($_POST['l_length']);
                $order_st['parts'][$currentKey]['l_facet'] =  [
                    'l_length' => $length,
                ];
                $order_st['parts'][$currentKey]['l_type'] = 'curve';
            }
        } elseif ($edge === "-Нет-") {
            unset($order_st['parts'][$currentKey]['l_edge']);
            unset($order_st['parts'][$currentKey]['l_type']);
        }
    }

    if (isset($_POST['r_edge']) && !is_null($_POST['r_edge'])) {
        $edge = $_POST['r_edge'];
        $order_st['parts'][$currentKey]['r_edge'] =  $edge;
        if (
            isset($order_st['parts'][$currentKey]['operations']['corner']['ru']['radius']) ||
            isset($order_st['parts'][$currentKey]['operations']['corner']['rd']['radius'])
        ) {
            $order_st['parts'][$currentKey]['r_type'] = 'curve';
        } else {
            $order_st['parts'][$currentKey]['r_type'] = 'strait';
        }
        if (isset($order_st['parts'][$currentKey]['r_facet'])) unset($order_st['parts'][$currentKey]['r_facet']);
        if ($edge === "facet") {
            if (isset($_POST['r_length']) && !is_null($_POST['r_length'])) {
                $length = intval($_POST['r_length']);
                $order_st['parts'][$currentKey]['r_facet'] = [
                    'r_length' => $length,
                ];
                $order_st['parts'][$currentKey]['r_type'] = 'curve';
            }
        } elseif ($edge === "-Нет-") {
            unset($order_st['parts'][$currentKey]['r_edge']);
            unset($order_st['parts'][$currentKey]['r_type']);
        }
    }

    if (isset($_POST['t_edge']) && !is_null($_POST['t_edge'])) {
        $edge = $_POST['t_edge'];
        $order_st['parts'][$currentKey]['t_edge'] =  $edge;
        if (
            isset($order_st['parts'][$currentKey]['operations']['corner']['lu']['radius']) ||
            isset($order_st['parts'][$currentKey]['operations']['corner']['ru']['radius'])
        ) {
            $order_st['parts'][$currentKey]['t_type'] = 'curve';
        } else {
            $order_st['parts'][$currentKey]['t_type'] = 'strait';
        }
        if (isset($order_st['parts'][$currentKey]['t_facet'])) unset($order_st['parts'][$currentKey]['t_facet']);
        if ($edge === "facet") {
            if (isset($_POST['t_length']) && !is_null($_POST['t_length'])) {
                $length = intval($_POST['t_length']);
                $order_st['parts'][$currentKey]['t_facet'] = [
                    't_length' => $length,
                ];
                $order_st['parts'][$currentKey]['t_type'] = 'curve';
            }
        } elseif ($edge === "-Нет-") {
            unset($order_st['parts'][$currentKey]['t_edge']);
            unset($order_st['parts'][$currentKey]['t_type']);
        }
    }

    if (isset($_POST['b_edge']) && !is_null($_POST['b_edge'])) {
        $edge = $_POST['b_edge'];
        $order_st['parts'][$currentKey]['b_edge'] =  $edge;
        if (
            isset($order_st['parts'][$currentKey]['operations']['corner']['ld']['radius']) ||
            isset($order_st['parts'][$currentKey]['operations']['corner']['rd']['radius'])
        ) {
            $order_st['parts'][$currentKey]['b_type'] = 'curve';
        } else {
            $order_st['parts'][$currentKey]['b_type'] = 'strait';
        }
        if (isset($order_st['parts'][$currentKey]['b_facet'])) unset($order_st['parts'][$currentKey]['b_facet']);
        if ($edge === "facet") {
            if (isset($_POST['b_length']) && !is_null($_POST['b_length'])) {
                $length = intval($_POST['b_length']);
                $order_st['parts'][$currentKey]['b_facet'] = [
                    'b_length' => $length,
                ];
                $order_st['parts'][$currentKey]['b_type'] = 'curve';
                            }
        } elseif ($edge === "-Нет-") {
            unset($order_st['parts'][$currentKey]['b_edge']);
            unset($order_st['parts'][$currentKey]['b_type']);
        }
    }

}

// adds addition operations
if (
    isset($_POST['addition_operations']) && !is_null($_POST['addition_operations']) &&
    isset($_POST['num_detail']) && !is_null($_POST['num_detail'])
) {
    $currentKey = $_POST['num_detail'];

    // adds cutting of corners
    if (isset($_POST['corner']) && !is_null($_POST['corner'])) {
        // radius cutting for a left up corner
        if (isset($_POST['radius_lu']) && !is_null($_POST['radius_lu'])) {
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['lu']['radius'])) unset($order_st['parts'][$currentKey]['operations']['corner']['lu']['radius']);
            $radius = intval($_POST['radius_lu']);
            if ($radius > 0) {
                $order_st['parts'][$currentKey]['operations']['corner']['lu']['radius'] = $radius;
                if (isset($order_st['parts'][$currentKey]['t_edge'])) {
                    unset($order_st['parts'][$currentKey]['t_edge']);
                    unset($order_st['parts'][$currentKey]['t_type']);
                    unset($order_st['parts'][$currentKey]['t_facet']);
                }
                if (isset($order_st['parts'][$currentKey]['l_edge'])) {
                    unset($order_st['parts'][$currentKey]['l_edge']);
                    unset($order_st['parts'][$currentKey]['l_type']);
                    unset($order_st['parts'][$currentKey]['l_facet']);
                }
            } else {
                unset($order_st['parts'][$currentKey]['operations']['corner']['lu']);
            }
        }
        // radius cutting for a left down corner
        if (isset($_POST['radius_ld']) && !is_null($_POST['radius_ld'])) {
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['ld']['radius'])) unset($order_st['parts'][$currentKey]['operations']['corner']['ld']['radius']);
            $radius = intval($_POST['radius_ld']);
            if ($radius > 0) {
                $order_st['parts'][$currentKey]['operations']['corner']['ld']['radius'] = $radius;
                if (isset($order_st['parts'][$currentKey]['b_edge'])) {
                    unset($order_st['parts'][$currentKey]['b_edge']);
                    unset($order_st['parts'][$currentKey]['b_type']);
                    unset($order_st['parts'][$currentKey]['b_facet']);
                }
                if (isset($order_st['parts'][$currentKey]['l_edge'])) {
                    unset($order_st['parts'][$currentKey]['l_edge']);
                    unset($order_st['parts'][$currentKey]['l_type']);
                    unset($order_st['parts'][$currentKey]['l_facet']);
                }
            } else {
                unset($order_st['parts'][$currentKey]['operations']['corner']['ld']);
            }
        }
        // radius cutting for a right up corner
        if (isset($_POST['radius_ru']) && !is_null($_POST['radius_ru'])) {
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['ru']['radius'])) unset($order_st['parts'][$currentKey]['operations']['corner']['ru']['radius']);
            $radius = intval($_POST['radius_ru']);
            if ($radius > 0) {
                $order_st['parts'][$currentKey]['operations']['corner']['ru']['radius'] = $radius;
                if (isset($order_st['parts'][$currentKey]['t_edge'])) {
                    unset($order_st['parts'][$currentKey]['t_edge']);
                    unset($order_st['parts'][$currentKey]['t_type']);
                    unset($order_st['parts'][$currentKey]['t_facet']);
                }
                if (isset($order_st['parts'][$currentKey]['r_edge'])) {
                    unset($order_st['parts'][$currentKey]['r_edge']);
                    unset($order_st['parts'][$currentKey]['r_type']);
                    unset($order_st['parts'][$currentKey]['r_facet']);
                }
            } else {
                unset($order_st['parts'][$currentKey]['operations']['corner']['ru']);
            }
        }
        // radius cutting for a right down corner
        if (isset($_POST['radius_rd']) && !is_null($_POST['radius_rd'])) {
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['rd']['radius'])) unset($order_st['parts'][$currentKey]['operations']['corner']['rd']['radius']);
            $radius = intval($_POST['radius_rd']);
            if ($radius > 0) {
                $order_st['parts'][$currentKey]['operations']['corner']['rd']['radius'] = $radius;
                if (isset($order_st['parts'][$currentKey]['b_edge'])) {
                    unset($order_st['parts'][$currentKey]['b_edge']);
                    unset($order_st['parts'][$currentKey]['b_type']);
                    unset($order_st['parts'][$currentKey]['b_facet']);
                }
                if (isset($order_st['parts'][$currentKey]['r_edge'])) {
                    unset($order_st['parts'][$currentKey]['r_edge']);
                    unset($order_st['parts'][$currentKey]['r_type']);
                    unset($order_st['parts'][$currentKey]['r_facet']);
                }
            } else {
                unset($order_st['parts'][$currentKey]['operations']['corner']['rd']);
            }
        }
        // slope(srez) cutting for a left up corner
        if (isset($_POST['lu_x2']) && !is_null($_POST['lu_x2']) && isset($_POST['lu_y1']) && !is_null($_POST['lu_y1'])) {
            $x1 = 0;
            $x2 = intval($_POST['lu_x2']);
            $y1 = intval($_POST['lu_y1']);
            $y2 = 0;
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['lu']['srez'])) unset($order_st['parts'][$currentKey]['operations']['corner']['lu']['srez']);
            $order_st['parts'][$currentKey]['operations']['corner']['lu']['srez'] = [
                'x1' => $x1,
                'x2' => $x2,
                'y1' => $y1,
                'y2' => $y2,
            ];
        }
        // slope(srez) cutting for a left down corner
        if (isset($_POST['ld_x1']) && !is_null($_POST['ld_x1']) && isset($_POST['ld_y2']) && !is_null($_POST['ld_y2'])) {
            $x1 = intval($_POST['ld_x1']);
            $x2 = 0;
            $y1 = 0;
            $y2 = intval($_POST['ld_y2']);
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['ld']['srez'])) unset($order_st['parts'][$currentKey]['operations']['corner']['ld']['srez']);
            $order_st['parts'][$currentKey]['operations']['corner']['ld']['srez'] = [
                'x1' => $x1,
                'x2' => $x2,
                'y1' => $y1,
                'y2' => $y2,
            ];
        }
        // slope(srez) cutting for a right up corner
        if (isset($_POST['ru_x1']) && !is_null($_POST['ru_x1']) && isset($_POST['ru_y2']) && !is_null($_POST['ru_y2'])) {
            $x1 = intval($_POST['ru_x1']);
            $x2 = 0;
            $y1 = 0;
            $y2 = intval($_POST['ru_y2']);
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['ru']['srez'])) unset($order_st['parts'][$currentKey]['operations']['corner']['ru']['srez']);
            $order_st['parts'][$currentKey]['operations']['corner']['ru']['srez'] = [
                'x1' => $x1,
                'x2' => $x2,
                'y1' => $y1,
                'y2' => $y2,
            ];
        }
        // slope(srez) cutting for a right down corner
        if (isset($_POST['rd_x2']) && !is_null($_POST['rd_x2']) && isset($_POST['rd_y1']) && !is_null($_POST['rd_y1'])) {
            $x1 = 0;
            $x2 = intval($_POST['rd_x2']);
            $y1 = intval($_POST['rd_y1']);
            $y2 = 0;
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['rd']['srez'])) unset($order_st['parts'][$currentKey]['operations']['corner']['rd']['srez']);
            $order_st['parts'][$currentKey]['operations']['corner']['rd']['srez'] = [
                'x1' => $x1,
                'x2' => $x2,
                'y1' => $y1,
                'y2' => $y2,
            ];
        }

        if (isset($_POST['lu_y1']) && $_POST['lu_y1'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['lu']['srez']);
        if (isset($_POST['ru_y2']) && $_POST['ru_y2'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['ru']['srez']);
        if (isset($_POST['rd_y1']) && $_POST['rd_y1'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['rd']['srez']);
        if (isset($_POST['ld_y2']) && $_POST['ld_y2'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['ld']['srez']);
        // (zarez) cutting for a left up corner
        if (isset($_POST['lu_x']) && !is_null($_POST['lu_x']) && isset($_POST['lu_y']) && !is_null($_POST['lu_y'])) {
            $x = intval($_POST['lu_x']);
            $y = intval($_POST['lu_y']);
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['lu']['zarez']))
                unset($order_st['parts'][$currentKey]['operations']['corner']['lu']['zarez']);
            $order_st['parts'][$currentKey]['operations']['corner']['lu']['zarez'] = [
                'x' => $x,
                'y' => $y,
            ];
        }
        // (zarez) cutting for a left down corner
        if (isset($_POST['ld_x']) && !is_null($_POST['ld_x']) && isset($_POST['ld_y']) && !is_null($_POST['ld_y'])) {
            $x = intval($_POST['ld_x']);
            $y = intval($_POST['ld_y']);
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['ld']['zarez']))
                unset($order_st['parts'][$currentKey]['operations']['corner']['ld']['zarez']);
            $order_st['parts'][$currentKey]['operations']['corner']['ld']['zarez'] = [
                'x' => $x,
                'y' => $y,
            ];
        }
        // (zarez) cutting for a right up corner
        if (isset($_POST['ru_x']) && !is_null($_POST['ru_x']) && isset($_POST['ru_y']) && !is_null($_POST['ru_y'])) {
            $x = intval($_POST['ru_x']);
            $y = intval($_POST['ru_y']);
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['ru']['zarez']))
                unset($order_st['parts'][$currentKey]['operations']['corner']['ru']['zarez']);
            $order_st['parts'][$currentKey]['operations']['corner']['ru']['zarez'] = [
                'x' => $x,
                'y' => $y,
            ];
        }
        // (zarez) cutting for a right down corner
        if (isset($_POST['rd_x']) && !is_null($_POST['rd_x']) && isset($_POST['rd_y']) && !is_null($_POST['rd_y'])) {
            $x = intval($_POST['rd_x']);
            $y = intval($_POST['rd_y']);
            if (isset($order_st['parts'][$currentKey]['operations']['corner']['rd']['zarez']))
                unset($order_st['parts'][$currentKey]['operations']['corner']['rd']['zarez']);
            $order_st['parts'][$currentKey]['operations']['corner']['rd']['zarez'] = [
                'x' => $x,
                'y' => $y,
            ];
        }
        if (isset($_POST['lu_y']) && $_POST['lu_y'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['lu']['zarez']);
        if (isset($_POST['ld_y']) && $_POST['ld_y'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['ld']['zarez']);
        if (isset($_POST['ru_y']) && $_POST['ru_y'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['ru']['zarez']);
        if (isset($_POST['rd_y']) && $_POST['rd_y'] == -1) unset($order_st['parts'][$currentKey]['operations']['corner']['rd']['zarez']);
    }
    // add treatments face detail
    if (isset($_POST['face']) && !is_null($_POST['face'])) {
        if (
            isset($_POST['bore_x']) && !is_null($_POST['bore_x']) &&
            isset($_POST['bore_y']) && !is_null($_POST['bore_y']) &&
            isset($_POST['bore_d']) && !is_null($_POST['bore_d'])
        ) {
            $x = intval($_POST['bore_x']);
            $y = intval($_POST['bore_y']);
            $d = intval($_POST['bore_d']);
            $zenk = (isset($_POST['zenk'])) ? 1 : 0;
            if (
                isset($order_st['parts'][$currentKey]['operations']['face']['bore']) &&
                !empty($order_st['parts'][$currentKey]['operations']['face']['bore'])
            ) {
                $order_st['parts'][$currentKey]['operations']['face']['bore'][] = [
                    'x' => $x,
                    'y' => $y,
                    'd' => $d,
                    'zenk' => $zenk,
                ];
            } else {
                $order_st['parts'][$currentKey]['operations']['face']['bore'] = [
                    1 => [
                        'x' => $x,
                        'y' => $y,
                        'd' => $d,
                        'zenk' => $zenk,
                    ],
                ];
            }
        } // end if for bore
        // add an oval hole
        if (
            isset($_POST['oval_x1']) && !is_null($_POST['oval_x1']) &&
            isset($_POST['oval_y1']) && !is_null($_POST['oval_y1']) &&
            isset($_POST['oval_x2']) && !is_null($_POST['oval_x2']) &&
            isset($_POST['oval_y2']) && !is_null($_POST['oval_y2'])
        ) {
            $x1 = intval($_POST['oval_x1']);
            $x2 = intval($_POST['oval_x2']);
            $y1 = intval($_POST['oval_y1']);
            $y2 = intval($_POST['oval_y2']);
            if ( isset($order_st['parts'][$currentKey]['operations']['face']['oval']) &&
                !empty($order_st['parts'][$currentKey]['operations']['face']['oval'])
            ) {
                $order_st['parts'][$currentKey]['operations']['face']['oval'][] = [
                    'x1' => $x1,
                    'y1' => $y1,
                    'x2' => $x2,
                    'y2' => $y2,
                ];
            } else {
                $order_st['parts'][$currentKey]['operations']['face']['oval'] = [
                    1 => [
                        'x1' => $x1,
                        'y1' => $y1,
                        'x2' => $x2,
                        'y2' => $y2,
                    ],
                ];
            }

        }
    } // end if for $_POST[face]
    // add decoration options
    if (isset($_POST['decoration_options']) && !is_null($_POST['decoration_options'])) {
        if (isset($order_st['parts'][$currentKey]['operations']['mat']))
            unset($order_st['parts'][$currentKey]['operations']['mat']);
        $order_st['parts'][$currentKey]['operations']['mat'] = (isset($_POST['mat']) && !is_null($_POST['mat'])) ? 1 : 0;
        if (isset($order_st['parts'][$currentKey]['operations']['gfh']))
            unset($order_st['parts'][$currentKey]['operations']['gfh']);
        $order_st['parts'][$currentKey]['operations']['gfh'] = (isset($_POST['gfh']) && !is_null($_POST['gfh'])) ? 1 : 0;
        if (isset($order_st['parts'][$currentKey]['operations']['film']))
            unset($order_st['parts'][$currentKey]['operations']['film']);
        $order_st['parts'][$currentKey]['operations']['film'] = (isset($_POST['film']) && !is_null($_POST['film'])) ? 1 : 0;
        if (isset($order_st['parts'][$currentKey]['operations']['film_art']))
            unset($order_st['parts'][$currentKey]['operations']['film_art']);
        $order_st['parts'][$currentKey]['operations']['film_art'] = (isset($_POST['film_art']) && !is_null($_POST['film_art'])) ? 1 : 0;
        if (isset($order_st['parts'][$currentKey]['operations']['zakalka']))
            unset($order_st['parts'][$currentKey]['operations']['zakalka']);
        $order_st['parts'][$currentKey]['operations']['zakalka'] = (isset($_POST['zakalka']) && !is_null($_POST['zakalka'])) ? 1 : 0;
        if (isset($_POST['oracal']) && !is_null($_POST['oracal'])) {
            if (isset($order_st['parts'][$currentKey]['operations']['oracal']))
                unset($order_st['parts'][$currentKey]['operations']['oracal']);
            $order_st['parts'][$currentKey]['operations']['oracal'] = htmlspecialchars($_POST['oracal']);
        }
    }
}
// sends data on a remove server and gets update the pdf file with new part
include_once('../../../func.php');
$result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
// обновляем чертеж если он создан
if (!isset($_POST['simple_part'])) include $serv_main_dir . '/clients/glass/modules/update_current_part.php';
$result['glass_arr'] = $order_st;
$project_arr = base64_encode(serialize($result));
$jsonOrderSt = json_encode($order_st);
$_SESSION['order_st'] = $jsonOrderSt;
if (isset($_POST['bore_x']) && isset($_POST['bore_y']) && isset($_POST['bore_d'])) {
    $bores = $order_st['parts'][$currentKey]['operations']['face']['bore'];
    $part = $order_st['parts'][$currentKey];
    include $serv_main_dir . '/clients/glass/layouts/bore_list.php';
} else if (isset($_POST['oval_x1']) && isset($_POST['oval_x2']) && isset($_POST['oval_y1']) && isset($_POST['oval_y2'])) {
    $ovals = $order_st['parts'][$currentKey]['operations']['face']['oval'];
    $part = $order_st['parts'][$currentKey];
    include $serv_main_dir . '/clients/glass/layouts/oval_list.php';
} else if (!isset($_POST['addition_operations'])) {
    $parts = $order_st['parts'];
    include $serv_main_dir . '/clients/glass/layouts/tbody_details_table.php';
} else {
    echo json_encode($order_st['parts'][$currentKey]);
}
unset($order_st);




