<?php
session_start();
if (isset($_POST['num_detail']) && isset($_POST['decoration_option']) && isset($_POST['image'])) {
    $currentKey = intval($_POST['num_detail']);
    $order_st = json_decode($_SESSION['order_st'], true);
    $decoration_options = [
        'photo_print' => 'photo_print_client',
        'UF_print' => 'UF_photo_print_client',
        'UF_print_white' => 'UF_photo_print_client_white',
        'sand_file'=> 'sand_client',
        'amalgama' => 'amalgama_client',
    ];
    $options_for_back_side = [
        'sand_file_back' => 'sand_client_back',
        'amalgama_back' => 'amalgama_client_back',
        ];
    if ($_POST['decoration_option'] === 'sand_file_back' || $_POST['decoration_option'] === 'amalgama_back') {
        foreach ($options_for_back_side as $key => $val) {
            if (isset($order_st['parts'][$currentKey]['operations'][$key])) unset($order_st['parts'][$currentKey]['operations'][$key]);
            if (isset($order_st['parts'][$currentKey]['operations'][$val])) {
                if (file_exists($order_st['parts'][$currentKey]['operations'][$val]))
                    unlink($order_st['parts'][$currentKey]['operations'][$val]);
                unset($order_st['parts'][$currentKey]['operations'][$val]);
            }
        }
    } else {
        foreach ($decoration_options as $key => $val) {
            if (isset($order_st['parts'][$currentKey]['operations'][$key])) unset($order_st['parts'][$currentKey]['operations'][$key]);
            if (isset($order_st['parts'][$currentKey]['operations'][$val])) {
                if (file_exists($order_st['parts'][$currentKey]['operations'][$val]))
                    unlink($order_st['parts'][$currentKey]['operations'][$val]);
                unset($order_st['parts'][$currentKey]['operations'][$val]);
            }
        }
    }

    if (array_key_exists($_POST['decoration_option'], $decoration_options) || array_key_exists($_POST['decoration_option'], $options_for_back_side)) {
        $order_st['parts'][$currentKey]['operations'][$_POST['decoration_option']] = $_POST['image'];
    }

    //include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/modules/update_current_part.php';

    echo json_encode($order_st['parts'][$currentKey]);
    $jsonOrderSt = json_encode($order_st);
    $_SESSION['order_st'] = $jsonOrderSt;
}
