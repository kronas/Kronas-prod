<?php
include '../../../_1/config.php';
include '../functions.php';
$response = ['message' => '', 'result' => ''];

// adds hole diameters
if (isset($_POST['add_diameter'])) {
    if ( glass_add_new_hole_diameter( intval( $_POST['add_diameter']) ) ) {
        $response['message'] = 'ok';
        $response['result'] = glass_get_holes_list_options();
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits hole diameters
if (isset($_POST['edit_diameter']) && isset($_POST['id'])) {
    if ( glass_edit_hole_diameter( intval($_POST['id']), intval( $_POST['edit_diameter']) ) ) {
        $response['message'] = 'ok';
        $response['result'] = glass_get_holes_list_options();
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// deletes hole diameters
if (isset($_POST['delete_id'])) {
    if ( glass_delete_hole_diameter( intval($_POST['delete_id']) ) ) {
        $response['message'] = 'ok';
        $response['result'] = glass_get_holes_list_options();
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits paths to a dir for showing pictures
if (isset($_POST['path_to_print']) && isset($_POST['path_to_sandblasting'])) {
    $path_to_print = htmlspecialchars($_POST['path_to_print']);
    $path_to_sandblasting = htmlspecialchars($_POST['path_to_sandblasting']);
    if (!is_dir($serv_main_dir . "/" . $path_to_print)) {
        $response['message'] .= "$path_to_print такого каталога нет. " . PHP_EOL;
    } else {
        glass_update_value_by_name('path_to_print', $path_to_print);
    }
    if (!is_dir($serv_main_dir . "/" . $path_to_sandblasting)) {
        $response['message'] .= "$path_to_sandblasting такого каталога нет. ";
    } else {
        glass_update_value_by_name('path_to_sandblasting', $path_to_sandblasting);
    }
    if ($response['message'] === "") $response['result'] = 'Данные успешно обновлены';
    echo json_encode($response);
    die;
}

// edits value min size for treatment of edges
if (isset($_POST['min_size_for_edge'])) {
    if (glass_update_value_by_name('min_size_for_edge', intval($_POST['min_size_for_edge']))) {
        $response['message'] = 'ok';
        $response['result'] = intval($_POST['min_size_for_edge']);
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits value min size for treatment of edges with rounded corners
if (isset($_POST['min_size_for_edge_r'])) {
    if (glass_update_value_by_name('min_size_for_edge_with_rounded_corner', intval($_POST['min_size_for_edge_r']))) {
        $response['message'] = 'ok';
        $response['result'] = intval($_POST['min_size_for_edge_r']);
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits value min size for zarez
if (isset($_POST['min_size_for_zarez'])) {
    if (glass_update_value_by_name('min_size_for_zarez', intval($_POST['min_size_for_zarez']))) {
        $response['message'] = 'ok';
        $response['result'] = intval($_POST['min_size_for_zarez']);
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits values of max size of details for treatment of edges
if (isset($_POST['max_length_for_edge']) && isset($_POST['max_width_for_edge'])) {
    if (glass_update_value_by_name('max_length_for_edge', intval($_POST['max_length_for_edge'])) &&
        glass_update_value_by_name('max_width_for_edge', intval($_POST['max_width_for_edge']))) {
        $response['message'] = 'ok';
        $response['result'] = [
            intval($_POST['max_length_for_edge']), intval($_POST['max_width_for_edge'])
        ];
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits values of max width of details for treatment of edges
if (isset($_POST['to_4mm']) && isset($_POST['over_4mm'])) {
    if (glass_update_value_by_name('to_4mm', intval($_POST['to_4mm'])) &&
        glass_update_value_by_name('over_4mm', intval($_POST['over_4mm']))) {
        $response['message'] = 'ok';
        $response['result'] = [
            intval($_POST['to_4mm']), intval($_POST['over_4mm'])
        ];
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits values of max size of details for treatment of edges with rounded corners
if (isset($_POST['max_length_for_edge_with_rounded_corner']) && isset($_POST['max_width_for_edge_with_rounded_corner'])) {
    if (glass_update_value_by_name('max_length_for_edge_with_rounded_corner', intval($_POST['max_length_for_edge_with_rounded_corner'])) &&
        glass_update_value_by_name('max_width_for_edge_with_rounded_corner', intval($_POST['max_width_for_edge_with_rounded_corner']))) {
        $response['message'] = 'ok';
        $response['result'] = [
            intval($_POST['max_length_for_edge_with_rounded_corner']), intval($_POST['max_width_for_edge_with_rounded_corner'])
        ];
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits values of max size of details for photo print
if (isset($_POST['max_length_for_photo_print']) && isset($_POST['max_width_for_photo_print'])) {
    if (glass_update_value_by_name('max_length_for_photo_print', intval($_POST['max_length_for_photo_print'])) &&
        glass_update_value_by_name('max_width_for_photo_print', intval($_POST['max_width_for_photo_print']))) {
        $response['message'] = 'ok';
        $response['result'] = [
            intval($_POST['max_length_for_photo_print']), intval($_POST['max_width_for_photo_print'])
        ];
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}

// edits values of max size of details for sandblasting
if (isset($_POST['max_length_for_sandblasting']) && isset($_POST['max_width_for_sandblasting'])) {
    if (glass_update_value_by_name('max_length_for_sandblasting', intval($_POST['max_length_for_sandblasting'])) &&
        glass_update_value_by_name('max_width_for_sandblasting', intval($_POST['max_width_for_sandblasting']))) {
        $response['message'] = 'ok';
        $response['result'] = [
            intval($_POST['max_length_for_sandblasting']), intval($_POST['max_width_for_sandblasting'])
        ];
    } else {
        $response['message'] = 'Server error';
    }
    echo json_encode($response);
    die;
}