<?php
session_start();
// If there is no order in the SESSION, it redirects to the home page to create a new order
if (!isset($_SESSION['order_st'])) {
    header('Location: /clients/glass/'.'?nw='.$_GET['nw']);
    $error = "<div class='alert alert-danger text-center'>Выберете материал для заказа</div>";
    $_SESSION['error'] = $error;
    die;
} else {
    // Retrieved from SESSION and decoded json into an array for adding or editing data
    $order_st = json_decode($_SESSION['order_st'], true);
}
// removes hole on the detail
if (
    isset($_POST['num_detail']) && !is_null($_POST['num_detail']) &&
    isset($_POST['face']) && !is_null($_POST['face'])
) {
    include_once "../../../func.php";
    $currentKey = intval($_POST['num_detail']);

    if (isset($_POST['num_bore']) && !is_null($_POST['num_bore'])) {
        $currentBore = intval($_POST['num_bore']);
        if (isset($order_st['parts'][$currentKey]['operations']['face']['bore'][$currentBore])) {
            unset($order_st['parts'][$currentKey]['operations']['face']['bore'][$currentBore]);
            if (empty($order_st['parts'][$currentKey]['operations']['face']['bore']))
                unset($order_st['parts'][$currentKey]['operations']['face']['bore']);
        }
        // sends to get updated the pdf file with new part
        $result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/modules/update_current_part.php';
        $bores = ($order_st['parts'][$currentKey]['operations']['face']['bore']) ?? '';
        $part = $order_st['parts'][$currentKey];
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/layouts/bore_list.php';
    }
    if (isset($_POST['num_oval']) && !is_null($_POST['num_oval'])) {
        $currentOval = intval($_POST['num_oval']);
        if (isset($order_st['parts'][$currentKey]['operations']['face']['oval'][$currentOval])) {
            unset($order_st['parts'][$currentKey]['operations']['face']['oval'][$currentOval]);
            if (empty($order_st['parts'][$currentKey]['operations']['face']['oval']))
                unset($order_st['parts'][$currentKey]['operations']['face']['oval']);
        }
        // sends data to get updated the pdf file with new part
        $result = glass_to_project($order_st,($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/modules/update_current_part.php';
        $ovals = ($order_st['parts'][$currentKey]['operations']['face']['oval']) ?? '';
        $part = $order_st['parts'][$currentKey];
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/layouts/oval_list.php';
    }
    if (empty($order_st['parts'][$currentKey]['operations']['face'])) {
        unset($order_st['parts'][$currentKey]['operations']['face']);
    }

    $jsonOrderSt = json_encode($order_st);
    $_SESSION['order_st'] = $jsonOrderSt;
}
