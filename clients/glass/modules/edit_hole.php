<?php
session_start();
// If there is no order in the SESSION, it redirects to the home page to create a new order
if (!isset($_SESSION['order_st'])) {
    header('Location: /'.'?nw='.$_GET['nw']);
    $error = "<div class='alert alert-danger text-center'>Выберете материал для заказа</div>";
    $_SESSION['error'] = $error;
    die;
} else {
    // Retrieved from SESSION and decoded json into an array for adding or editing data
    $order_st = json_decode($_SESSION['order_st'], true);
}
// edit the hole
if (
    isset($_POST['num_detail']) && !is_null($_POST['num_detail']) &&
    isset($_POST['face']) && !is_null($_POST['face'])
) {

    $currentKey = intval($_POST['num_detail']);
    // add a bore hole
    if (
        isset($_POST['num_bore']) && !is_null($_POST['num_bore']) &&
        isset($_POST['bore_x']) && !is_null($_POST['bore_x']) &&
        isset($_POST['bore_y']) && !is_null($_POST['bore_y']) &&
        isset($_POST['bore_d']) && !is_null($_POST['bore_d'])
    ) {
        $currentBore = intval($_POST['num_bore']);
        $x = intval($_POST['bore_x']);
        $y = intval($_POST['bore_y']);
        $d = intval($_POST['bore_d']);
        $zenk = (isset($_POST['zenk'])) ? 1 : 0;
        if (isset($order_st['parts'][$currentKey]['operations']['face']['bore'][$currentBore])) {
            $order_st['parts'][$currentKey]['operations']['face']['bore'][$currentBore] = [
                'x' => $x,
                'y' => $y,
                'd' => $d,
                'zenk' => $zenk,
            ];
        }
        // sends data to get updated the pdf file with new part
        include_once('../../../func.php');
        $result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/modules/update_current_part.php';
        $bores = ($order_st['parts'][$currentKey]['operations']['face']['bore']) ?? '';
        $part = $order_st['parts'][$currentKey];
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/layouts/bore_list.php';
    }
    // add an oval hole
    if (
        isset($_POST['num_oval']) && !is_null($_POST['num_oval']) &&
        isset($_POST['oval_x1']) && !is_null($_POST['oval_x1']) &&
        isset($_POST['oval_y1']) && !is_null($_POST['oval_y1']) &&
        isset($_POST['oval_x2']) && !is_null($_POST['oval_x2']) &&
        isset($_POST['oval_y2']) && !is_null($_POST['oval_y2'])
    ) {
        $currentOval = intval($_POST['num_oval']);
        $x1 = intval($_POST['oval_x1']);
        $x2 = intval($_POST['oval_x2']);
        $y1 = intval($_POST['oval_y1']);
        $y2 = intval($_POST['oval_y2']);
        if (isset($order_st['parts'][$currentKey]['operations']['face']['oval'][$currentOval])) {
            $order_st['parts'][$currentKey]['operations']['face']['oval'][$currentOval] = [
                'x1' => $x1,
                'y1' => $y1,
                'x2' => $x2,
                'y2' => $y2,
            ];
        }
        // sends data to get updated the pdf file with new part
        include_once('../../../func.php');
        $result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/modules/update_current_part.php';
        $ovals = ($order_st['parts'][$currentKey]['operations']['face']['oval']) ?? '';
        $part = $order_st['parts'][$currentKey];
        include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/layouts/oval_list.php';
    }

    $jsonOrderSt = json_encode($order_st);
    $_SESSION['order_st'] = $jsonOrderSt;
}