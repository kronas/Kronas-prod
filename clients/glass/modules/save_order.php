<?php
session_start();
if (isset($_POST['order_st']) && !is_null($_POST['order_st'])) {

    $order_st = json_decode($_POST['order_st'], true);
    foreach ($order_st['parts'] as $key => $val) {
        if (isset($val['path'])) {
            $file_content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $val['path'][0]);
            $order_st['parts'][$key]['pdf_content'] = $file_content;
        }
    }

    echo base64_encode(serialize($order_st));
}
