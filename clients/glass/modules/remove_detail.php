<?php
session_start();
if (!isset($_SESSION['order_st'])) {
    header('Location: /'.'?nw='.$_GET['nw']);
    $error = "<div class='alert alert-danger text-center'>Выберете материал для заказа</div>";
    $_SESSION['error'] = $error;
    die;
}

if (isset($_POST['num_detail']) && !is_null($_POST['num_detail']) && strripos($_SESSION['order_st'], '"parts":{')) {

    $currentKey = intval($_POST['num_detail']);
    $order_st = json_decode($_SESSION['order_st'], true);
    if (isset($order_st['parts'][$currentKey]['path'])) {
        foreach ($order_st['parts'][$currentKey]['path'] as $path) {
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $path))
                unlink($_SERVER['DOCUMENT_ROOT'] . $path);
        }
    }
    unset($order_st['parts'][$currentKey]);
    if (empty($order_st['parts'])) unset($order_st['parts']);
    $jsonOrderSt = json_encode($order_st);
    include_once('../../../func.php');
    $result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
    $result['glass_arr'] = $order_st;
    $project_arr = base64_encode(serialize($result));
    $_SESSION['order_st'] = $jsonOrderSt;
    $parts = ($order_st['parts']) ?? [];
    unset($order_st);
    include $_SERVER['DOCUMENT_ROOT'] . "/clients/glass/layouts/tbody_details_table.php";
}
