<?php
session_start();
include $_SERVER['DOCUMENT_ROOT'] . '/_1/config.php';
if (
    isset($_POST['width']) && isset($_POST['length']) && isset($_POST['quantity'])
    && !is_null($_POST['width']) && !is_null($_POST['length']) && !is_null($_POST['quantity'])
) {

    $width = intval($_POST['width']);
    $length = intval($_POST['length']);
    $quantity = intval($_POST['quantity']);
    $order_st = json_decode($_SESSION['order_st'], true);
    if (isset($order_st['parts']) && !empty($order_st['parts'])) {
        $order_st['parts'][] = [
            'l' => $length,
            'w' => $width,
            'q' => $quantity,
        ];
    } else {
        $order_st['parts'] = [
            1 => [
                'l' => $length,
                'w' => $width,
                'q' => $quantity,
            ],
        ];
    }
    $index = array_key_last($order_st['parts']);
//    include $serv_main_dir . '/clients/glass/api/load_data_of_current_part.php';
//    $response = loadDataOfCurrentPart($order_st['parts'][$index], $index);
//    include $serv_main_dir . '/clients/glass/services/write_pdf_file.php';
//    $file_path = writePdf($response, $index);
//    $order_st['parts'][$index]['path'] = $file_path;
    include_once('../../../func.php');
    $result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
    $result['glass_arr'] = $order_st;
    $project_arr = base64_encode(serialize($result));
    $jsonOrderSt = json_encode($order_st);
    $_SESSION['order_st'] = $jsonOrderSt;
    $parts = $order_st['parts'];
    include $serv_main_dir . '/clients/glass/layouts/tbody_details_table.php';
    unset($order_st);
}
