<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/functions.php';
session_start();

if (isset($_FILES)) {
    $response = [];
    $file = $_FILES[0];
    $ext = explode('.', $file['name']);
    $ext = array_pop($ext);
    if ($ext === 'krgl') {
        $file_content = file_get_contents($file['tmp_name']);
        $order_st = unserialize(base64_decode($file_content));
        $order_st = pdf_and_img_restore($order_st);
        $_SESSION['order_st'] = json_encode($order_st);
        $_SESSION['materials'] = json_encode($order_st['order_st']);
        session_restore($order_st['order_st']['data_user']);
        $response['error'] = false;
    } else {
        $response['error'] = true;
        $response['message'] = 'Файл не содержит данных о заказе!';
    }
    unlink($file['tmp_name']);
    echo json_encode($response);
}
//if (isset($_POST['file_path']) && !is_null($_POST['file_path'])) {
//    $file_path = $_POST['file_path'];
//    if (file_exists($file_path)) {
//        $file_content = file_get_contents($file_path);
//        $order_st = unserialize(base64_decode($file_content));
//        $_SESSION['order_st'] = json_encode($order_st);
//    } else {
//        echo 'file don\'t exists';
//        die;
//    }
//
//}