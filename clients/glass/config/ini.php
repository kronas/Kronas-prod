<?php
////return [
//    // url for getting the group of materials
//    'url_get_materials_group' => 'http://glassdetailing.zzz.com.ua/db/get_materials_group.php',
//
//    // url for getting materials
//    'url_get_materials' => 'http://glassdetailing.zzz.com.ua/db/get_materials.php',
//
//    // url for loading data of the current part on a remote server
//    'load_data_of_current_part' => 'https://stanisap.site/remote_server/api/get_pdf_current_part.php',
//
//    // array for the diameter of the permissible holes for drilling.
//    'hole_diameter' => [5,6,7,8,10,12,15,20,22,26,27,35,40,48,50,60,70],
//
//    // the path to a library of pictures
//    'path_to_print' => '../../images/photo_print',
//    'path_to_sandblasting' => '../../images/sandblasting',
//
//    // this is a max width for a facet treatment of the edge
//    'to_4mm' => 25,
//    'over_4mm' => 35,
//
//    // the max size for treatment edges
//    'max_size_for_edge' => [2600, 1150],
//
//    // the min size for treatment edges
//    'min_size_for_edge' => 120,
//
//    // the max size for treatment of edges if there is a adjacent rounded corner
//    'max_size_for_edge_with_rounded_corner' => [1600, 1200],
//
//    // the min size for treatment edges if there is a adjacent rounded corner
//    'min_size_for_edge_with_founded_corner' => 100,
//
//    // the min size for zarez of the corner
//    'min_size_for_zarez' => 100,
//
//    // the max size for a photo print
//    'max_size_for_photo_print' => [2600, 1150],
//
//    // the max size for sandblasting
//    'max_size_for_sandblasting' => [2600, 1500],
//];