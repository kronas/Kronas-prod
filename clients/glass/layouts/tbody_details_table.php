<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/_1/config.php';
?>
<div class="hidden" id="order_st"><?= $jsonOrderSt ?></div>
<div class="hidden" id="project_arr"><?= $project_arr ?></div>
<table class="table table-sm-bordered table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Деталь</th>
        <th scope="col">Кол.</th>
        <th scope="col">Длина</th>
        <th scope="col">Ширина</th>
        <th scope="col">Лево</th>
        <th scope="col">Право</th>
        <th scope="col">Верх</th>
        <th scope="col">Низ</th>
        <th scope="col">Обработка</th>
        <th scope="col">Действия</th>
    </tr>
    </thead>
    <tbody id="tbodyParts">
    <?php
    if (!isset($ini_data)) {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/clients/glass/functions.php');
        //$ini_data = parse_ini_file($_SERVER['DOCUMENT_ROOT'].'/clients/glass/config/php.ini');
        $ini_data = glass_parse_all_settings();
    }

    $optionsHandle = [
        "shlif" => 'Шлифовка',
        'poli' => 'Полировка',
        'facet' => 'Фацет',
    ];

    if (is_array($parts)) {
        while ($part = current($parts)) {
            $currentKey = key($parts);
            ?>
            <tr>
                <th scope="row"><?= $currentKey ?></th>
                <th scope="row" id="draw_<?=$currentKey?>">
                <?php
                    if (isset($part['path'])) {
                        echo "<a href='{$part['path'][0]}' target='_blank'><img src='{$part['path'][1]}' height='80px'></a></th>";
                    }
                ?>
                <td class="quantity"><input onchange="changingCreatedPart(this, <?= $part['l'] ?>, <?= $part['w'] ?>, <?= $currentKey ?>)" type="number" name="quantity" class="form-control input-current-part" value="<?= $part['q'] ?>"></td>
                <td class="length"><input onchange="changingCreatedPart(this, <?= $part['q'] ?>, <?= $part['w'] ?>, <?= $currentKey ?>)" type="number" name="length" class="form-control input-current-part" value="<?= $part['l'] ?>"></td>
                <td class="width"><input onchange="changingCreatedPart(this, <?= $part['q'] ?>, <?= $part['l'] ?>, <?= $currentKey ?>)" type="number" name="width" class="form-control input-current-part" value="<?= $part['w'] ?>"></td>
                <td>
                    <div class="form-group">
                        <select onchange="edgeHandle(this, <?= $currentKey ?>);" class="form-control" name="l_edge">
                            <option>-Нет-</option>
                            <?php
                            if (
                                isset($part['operations']['corner']['lu']['radius']) ||
                                isset($part['operations']['corner']['ld']['radius'])
                            ) {
                                $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                                $min_related = $ini_data['min_size_for_edge_with_founded_corner'] ** 2;
                            } else {
                                $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                                $min_related = $ini_data['min_size_for_edge'] ** 2;
                            }
                            $current_related = $part['l'] * $part['w'];

                            if ($current_related <= $max_related && $current_related >= $min_related) {
                                foreach ($optionsHandle as $key => $val) {
                                    ?>
                                    <option
                                        <?php
                                        if (isset($part['l_edge']) && $part['l_edge'] === $key) {
                                            echo 'selected ';
                                        }
                                        ?>value="<?= $key ?>"><?= $val ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select onchange="edgeHandle(this, <?= $currentKey ?>);" class="form-control" name="r_edge">
                            <option>-Нет-</option>
                            <?php
                            if (
                                isset($part['operations']['corner']['ru']['radius']) ||
                                isset($part['operations']['corner']['rd']['radius'])
                            ) {
                                $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                                $min_related = $ini_data['min_size_for_edge_with_founded_corner'] ** 2;
                            } else {
                                $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                                $min_related = $ini_data['min_size_for_edge'] ** 2;
                            }
                            if ($current_related <= $max_related && $current_related >= $min_related) {
                                foreach ($optionsHandle as $key => $val) {
                                    ?>
                                    <option
                                        <?php
                                        if (isset($part['r_edge']) && $part['r_edge'] === $key) {
                                            echo 'selected ';
                                        }
                                        ?>value="<?= $key ?>"><?= $val ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select onchange="edgeHandle(this, <?= $currentKey ?>);" class="form-control" name="t_edge">
                            <option>-Нет-</option>
                            <?php
                            if (
                                isset($part['operations']['corner']['ru']['radius']) ||
                                isset($part['operations']['corner']['lu']['radius'])
                            ) {
                                $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                                $min_related = $ini_data['min_size_for_edge_with_founded_corner'] ** 2;
                            } else {
                                $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                                $min_related = $ini_data['min_size_for_edge'] ** 2;
                            }
                            if ($current_related <= $max_related && $current_related >= $min_related) {
                                foreach ($optionsHandle as $key => $val) {
                                    ?>
                                    <option
                                        <?php
                                        if (isset($part['t_edge']) && $part['t_edge'] === $key) {
                                            echo 'selected ';
                                        }
                                        ?>value="<?= $key ?>"><?= $val ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <select onchange="edgeHandle(this, <?= $currentKey ?>);" class="form-control" name="b_edge">
                            <option>-Нет-</option>
                            <?php
                            if (
                                isset($part['operations']['corner']['rd']['radius']) ||
                                isset($part['operations']['corner']['ld']['radius'])
                            ) {
                                $max_related = $ini_data['max_size_for_edge_with_rounded_corner'][0] * $ini_data['max_size_for_edge_with_rounded_corner'][1];
                                $min_related = $ini_data['min_size_for_edge_with_founded_corner'] ** 2;
                            } else {
                                $max_related = $ini_data['max_size_for_edge'][0] * $ini_data['max_size_for_edge'][1];
                                $min_related = $ini_data['min_size_for_edge'] ** 2;
                            }
                            if ($current_related <= $max_related && $current_related >= $min_related) {
                                foreach ($optionsHandle as $key => $val) {
                                    ?>
                                    <option
                                        <?php
                                        if (isset($part['b_edge']) && $part['b_edge'] === $key) {
                                            echo 'selected ';
                                        }
                                        ?>value="<?= $key ?>"><?= $val ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </td>
                <td>
                    <a href="<?= $main_dir ?>/clients/glass/treatment.php?num_detail=<?= $currentKey ?>" class="btn-sm btn-success cont">Подробнее</a>
                </td>
                <td class="d-flex justify-content-around">
                    <a onclick="removeDetail(<?= $currentKey ?>);" href="#" title="удалить"><i
                                class="fas fa-trash-alt text-danger"></i></a>
                </td>
            </tr>
            <?php
            next($parts);
        }
    }

    ?>
    </tbody>
</table>

