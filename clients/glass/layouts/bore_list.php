<span class="hidden"><?= json_encode($part) ?></span>
<table id="bore-table" width="100%">
    <?php
    if (is_array($bores)) {
        ?>
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">x</th>
            <th scope="col">y</th>
            <th scope="col">d</th>
            <th scope="col">зен</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        while ($bore = current($bores)) {
            $index = key($bores);
            ?>
            <tr class="bore-item" onclick="editBore(this, <?= $index ?>, <?= $bore['x'] ?>, <?= $bore['y'] ?>, <?= $bore['d'] ?>, <?= $bore['zenk'] ?>)">
                <th scope="row"><?= $index ?></th>
                <td class="boreX"><?= $bore['x'] ?></td>
                <td class="boreY"><?= $bore['y'] ?></td>
                <td><?= $bore['d'] ?></td>
                <td><?php
                    if ($bore['zenk'] == 1) {
                        echo "да";
                    } else {
                        echo "нет";
                    }
                    ?></td>
                <td>
                    <button class="btn btn-light" onclick="removeBore(this, <?= $index ?>)"><i class="fas fa-trash-alt text-danger"></i></button>
                </td>
            </tr>
            <?php
            next($bores);
        }
        ?>
            </tbody>
        </table>
        <?php
    }
    ?>
