<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script src="assets/js/jquery.mask.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <link href="assets/fontawesome/css/all.css" rel="stylesheet">

    <script src="assets/js/main.js"></script>
    <title>Document</title>
</head>
<body>
<!-- start/ Header -->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 text-right">
            <div class="alert alert-warning" role="alert">
                <div class="float-left">
                    <a href="<?= $laravel_dir ?>">В пильный сервис</a>
                </div>
                <div>Приветствуем, Полушин Полушин! (<small><i>Клиент</i></small>) ||
                    <a href="docs.php" title="Документы">
                        <i class="far fa-file-alt"></i>
                    </a> ||
                    <a href="balance.php" title="Остатки">
                        <i class="fas fa-warehouse"></i>
                    </a> ||
                    <a href="orders.php" title="Заказы">
                        <i class="far fa-folder-open"></i>
                    </a> ||
                    <a href="login.php?op=logout" title="Выход">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end/ Header -->
