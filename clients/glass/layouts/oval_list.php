<span class="hidden"><?= json_encode($part) ?></span>
<table id="oval-table" width="100%">
    <?php
    if (is_array($ovals)) {
        ?>
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">x1</th>
            <th scope="col">y1</th>
            <th scope="col">x2</th>
            <th scope="col">y2</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        while ($oval = current($ovals)) {
            $index = key($ovals);
            ?>
            <tr class="oval-item" onclick="editOval(this, <?= $index ?>, <?= $oval['x1'] ?>, <?= $oval['y1'] ?>, <?= $oval['x2'] ?>, <?= $oval['y2'] ?>);">
                <th scope="row"><?= $index ?></th>
                <td class="ovalX1"><?= $oval['x1'] ?></td>
                <td class="ovalY1"><?= $oval['y1'] ?></td>
                <td class="ovalX2"><?= $oval['x2'] ?></td>
                <td class="ovalY2"><?= $oval['y2'] ?></td>
                <td>
                    <button class="btn btn-light" onclick="removeOval(this, <?= $index ?>);"><i class="fas fa-trash-alt text-danger"></i></button>
                </td>
            </tr>
            <?php
            next($ovals);
        }
        ?>
            </tbody>
        </table>
        <?php
    }
    ?>
