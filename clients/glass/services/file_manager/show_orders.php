<?php
$extensions = 'krgl';
if (isset($_SESSION['manager_id']) && $_SESSION['client_id']) {
    $folder = $_SESSION['manager_id'] . "_" . $_SESSION['client_id'];
} elseif (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] === 'client') {
    $folder = $_SESSION['user']['ID'];
} else {
    $folder = session_id();
}
$path = $_SERVER['DOCUMENT_ROOT'].'/clients/glass/storage/files/orders/' . $folder . '/';
if (is_dir($path)) {
    $directoryIterator = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
    $iteratorIterator  = new RecursiveIteratorIterator($directoryIterator, RecursiveIteratorIterator::LEAVES_ONLY);

    foreach ($iteratorIterator as $file) {
        if ($file->getExtension() === $extensions) {
            $file_name = explode('/', $file->getPathname());
            $file_name = array_pop($file_name);
            ?>
            <div class="col-sm-6 list-img">
                <div class="card m-2">
                    <form action="modules/load_save_order.php<?='?nw='.$_GET['nw']?>" method="POST">
                        <input type="hidden" name="file_path" value="<?= $file->getPathname() ?>">
                        <div class="d-flex flex-row custom-file overflow-hidden">
                            <span class="ml-2"><i class="fas fa-file text-warning"></i></span>
                            <p class="ml-2"><?= $file_name; ?></p>
                        </div>
                    </form>

                </div>
            </div>
            <?php
        }
    }
} else {
    echo "<div class='alert alert-warning'>У Вас еще нет сохраненных заказов.</div>";
}

