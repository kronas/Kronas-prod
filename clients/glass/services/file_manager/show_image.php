<?php
include $_SERVER['DOCUMENT_ROOT'] . '/_1/config.php';
include __DIR__ . '/../../functions.php';
//$ini_data = parse_ini_file('../../config/php.ini');
$ini_data = glass_parse_all_settings();
$for_print = ['photo_print', 'UF_print', 'UF_print_white'];
$for_sandblasting = ['sand_file', 'amalgama', 'sand_file_back', 'amalgama_back', 'amalgama_back'];
if (isset($_POST['decoration_options']) && !is_null($_POST['decoration_options'])) {
    $request = $_POST['decoration_options'];
    if (in_array($request, $for_print)) {
        $path = '../../../../'. $ini_data['path_to_print'];
    } elseif (in_array($request, $for_sandblasting)) {
        $path = '../../../../'. $ini_data['path_to_sandblasting'];
    } else {
        echo 'path_error';
        die;
    }
} else {
    echo 'request_error';
    die;
}
$current_dir =  ($_POST['current_dir']) ?? "";
$allowFolders = scandir($path . $current_dir);
$extensions = array('png', 'jpg', 'jpeg');
?>
<ul class="list-grou row">
    <?php
    if (strlen($current_dir) > 0) {
        $prev_dir = explode('/', $current_dir);
        echo "<li class='list-group-item image-folder col-sm-10 mb-1' data-current_dir='$prev_dir[0]'><i class='fas fa-backward'></i></li>";
    }
    foreach ($allowFolders as $folder) {
        if ($folder !== '.' && $folder !== '..') {
            if (is_dir($path.'/'.$current_dir. '/' .$folder)) {
                echo "<li class='list-group-item image-folder  col-sm-10 mb-1' data-current_dir='$current_dir/$folder'><i class='fas fa-folder'></i> $folder </li>";
            } else {
                $extension = pathinfo($path . '/' . $folder, PATHINFO_EXTENSION);
                if (in_array($extension, $extensions)) {
                    ?>
                    <div class="col-sm-4 list-img">
                        <div class="card m-2">
                            <div class="card-body">
                                <img src="<?= $path . $current_dir . '/' . $folder?>" class="img-from-library" style="width: 100%; height: 200px">
                            </div>
                            <div class="card-footer text-center"><?= $folder ?></div>
                        </div>
                    </div>
                    <?php
                }
            }
        }
    }
    ?>
</ul>
