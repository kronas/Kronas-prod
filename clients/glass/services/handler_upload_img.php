<?php
session_start();
// this serves checks files and moves them into /images
if (isset($_SESSION['order_st'])) {
    $order_st = json_decode($_SESSION['order_st'], true);
    $options = [
        'photo_print' => 'photo_print_client',
        'UF_print' => 'UF_photo_print_client',
        'UF_print_white'=> 'UF_photo_print_client_white',
        'sand_file' => 'sand_client',
        'amalgama' => 'amalgama_client',
    ];
    $options_back = [
        'sand_file_back' => 'sand_client_back',
        'amalgama_back' => 'amalgama_client_back',
        ];

    if(isset($_FILES) ) {
        $image = $_FILES[0];

        if ($image['size'] > 500000) {
            $response[0] = 'error_size';
            echo json_encode($response);
            die();
        }
        $imageFormat = explode('.', $image['name']);
        $imageFormat = array_pop($imageFormat);
        $imageFullName = $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/images/clients/' . hash('crc32',time()) . '.' . $imageFormat;
        $imageType = $image['type'];
        if ($imageType == 'image/jpeg' || $imageType == 'image/png') {
            if (isset($_POST['num_detail']) && isset($_POST['decoration_option'])) {
                $currentKey = intval($_POST['num_detail']);
                if (move_uploaded_file($image['tmp_name'], $imageFullName) ) {
                    if ($_POST['decoration_option'] === 'sand_file_back' || $_POST['decoration_option'] === 'amalgama_back') {
                        $check_options = $options_back;
                    } else {
                        $check_options = $options;
                    }
                    foreach ($check_options as $key => $val) {
                        if (isset($order_st['parts'][$currentKey]['operations'][$key])) unset($order_st['parts'][$currentKey]['operations'][$key]);
                        if (isset($order_st['parts'][$currentKey]['operations'][$val])) {
                            unlink($order_st['parts'][$currentKey]['operations'][$val]);
                            unset($order_st['parts'][$currentKey]['operations'][$val]);
                        }
                    }
                    $imageShortName = substr($imageFullName, strripos($imageFullName, 'images'));
                    if (array_key_exists($_POST['decoration_option'], $options)) {
                        $order_st['parts'][$currentKey]['operations'][$options[$_POST['decoration_option']]] = $imageShortName;
                    }
                    if (array_key_exists($_POST['decoration_option'], $options_back)) {
                        $order_st['parts'][$currentKey]['operations'][$options_back[$_POST['decoration_option']]] = $imageShortName;
                    }

                    //include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/modules/update_current_part.php';

                    $jsonOrderSt = json_encode($order_st);
                    $_SESSION['order_st'] = $jsonOrderSt;
                    $response = [
                        'success',
                        $order_st['parts'][$currentKey],
                    ];
                    echo json_encode($response);
                } else {
                    $response[0] = 'error';
                    echo json_encode($response);
                    die;
                }
            }
        } else {
            $response[0] = 'error_format';
            echo json_encode($response);
            die;
        }
    }
} else {
    $response[0] = 'error_session';
    echo json_encode($response);
}
