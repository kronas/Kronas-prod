<?php
function writePdf($content, $index) {
    $file_path = $_SERVER['DOCUMENT_ROOT'] . "/clients/glass/storage/pdf/" . hash('crc32',time()) . "_$index.pdf";
    $img_path = $_SERVER['DOCUMENT_ROOT'] . "/clients/glass/storage/img/" . hash('crc32',time()) . "_$index.jpg";
    if (file_exists($file_path)) unlink($file_path);
    file_put_contents($file_path, $content);
    $imagick = new Imagick();
    $imagick->setResolution(300,300);
    $imagick->readImage($file_path);
    $imagick->setBackgroundColor('white');
    $imagick->rotateimage("#fff", 90);
    $imagick->setImageCompression(imagick::COMPRESSION_NO  );
    $imagick->setImageCompressionQuality(91);
    $imagick->setCompressionQuality(95);
    $imagick->setBackgroundColor('white');
    $imagick->setImageCompressionQuality(95);
    $imagick->setImageFormat('jpg');
    $imagick->writeImage($img_path);

//    $img = new Imagick();
//    //$img->setSize(300, 300);
//    try {
//        $img->readImage($file_path);
//    } catch (ImagickException $e) {
//        echo $e;
//    }
    //$img->writeImage($img_path);
    $namePDF = explode('/', $file_path);
    $nameIMG = explode('/', $img_path);
    $namePDF = array_pop($namePDF);
    $nameIMG = array_pop($nameIMG);
    return ["/clients/glass/storage/pdf/$namePDF", "/clients/glass/storage/img/$nameIMG"];
}