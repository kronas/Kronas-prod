jQuery(document).ready(function(){

    $('.phone').mask('+00(000)000-00-00', {placeholder: "+__ (___) ___-__-__"});

    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        var idT=e.target.attributes.getNamedItem("aria-controls").value;
        var idR=e.relatedTarget.attributes.getNamedItem("aria-controls").value;
        if(idT=='pills-user') $('#typein').val('client'); else $('#typein').val('manager');
        $('div#'+idT+'  .the-required').attr('required',"required");
        $('div#'+idR+'  .the-required').removeAttr('required');
    });

    $('#writeOff a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var idT=e.target.attributes.getNamedItem("aria-controls").value;
        var idR=e.relatedTarget.attributes.getNamedItem("aria-controls").value;
        $('div#'+idT+'  .the-required').attr('required',"required");
        $('div#'+idR+'  .the-required').removeAttr('required');
    });
    /*
    $(document).on('keyup blur', '#pills-user input#mail1', function (event, sel) {
        $('div#pills-user input#mail1').attr('required',"required");
        $('div#pills-user input#login_tel').removeAttr('required');
        $('div#pills-user input#login_tel').val('');
    });

    $(document).on('keyup blur', '#pills-user input#login_tel', function (event, sel) {
        $('div#pills-user input#login_tel').attr('required',"required");
        $('div#pills-user input#mail1').removeAttr('required');
        $('div#pills-user input#mail1').val('');
    });
    */
    $(document).on('keyup', '.add-user input#new_phone', function (event, sel) {
       if($(this).val().length==17)
       {
           $('#new_phone').attr('data-valid','0');
           var out={};
           out['new_phone'] = $(this).val();
           $.ajax({
               type: 'POST',
               url:'ajax_resp.php',
               data:out,
               success:function(resp){
                   if(resp==1) {
                       $('#phoneHelp').html('<span style="color: red;">Такой телефон уже существует! </span>');
                       $('label[for="new_mail"]').addClass('label-required');
                       $('input#new_mail').attr('required',"required");
                       $('#new_phone').attr('data-valid','0');
                       add_user_showhide();
                   }
                   else
                      {
                          $('#phoneHelp').html('<span style="color: green;">Такой телефон не существует, продолжайте... </span>');
                          $('#mailHelp').html('');
                          $('label[for="new_mail"]').removeClass('label-required');
                          $('input#new_mail').removeAttr('required');
                          $('#new_phone').attr('data-valid','1');
                          add_user_showhide();
                      }

               }
           });
       }
       else
       {
           $('#phoneHelp').html('<span style="color: red;">Введите телефон! </span>');
           $('label[for="new_mail"]').addClass('label-required');
           $('input#new_mail').attr('required',"required");
           $('#new_phone').attr('data-valid','0');
           add_user_showhide();
       }
    });
    $(document).on('keyup', '.add-user input#new_mail', function (event, sel) {
        if ($(this).is(':valid')) {
            $('#new_mail').attr('data-valid', '0');
            var out={};
            out['new_mail'] = $(this).val();
            $.ajax({
                type: 'POST',
                url: 'ajax_resp.php',
                data: out,
                success: function (resp) {
                    if (resp == 1) {
                        $('#mailHelp').html('<span style="color: red;">Такой E-mail уже существует! </span>');
                        $('label[for="new_phone"]').addClass('label-required');
                        $('input#new_phone').attr('required',"required");
                        $('#new_mail').attr('data-valid', '0');
                        add_user_showhide();
                    }
                    else {
                        $('#mailHelp').html('<span style="color: green;">Такой E-mail не существует, продолжайте... </span>');
                        $('#phoneHelp').html('');
                        $('label[for="new_phone"]').removeClass('label-required');
                        $('input#new_phone').removeAttr('required');
                        $('#new_mail').attr('data-valid','1');
                        add_user_showhide();
                    }

                }
            });
        }
        else
        {
            $('#mailHelp').html('<span style="color: red;">Введите E-mail</span>');
            $('label[for="new_phone"]').addClass('label-required');
            $('input#new_phone').attr('required',"required");
            $('#new_mail').attr('data-valid','0');
            add_user_showhide();
        }
    });
    $(document).on('keyup', '.add-user .input-password input', function (event, sel) {
        var theID=$(this).attr('id');
        if(theID=='new_password')  var theID2='new_password2'; else var  theID2='new_password';
        if($(this).val()==$('#'+theID2).val())
        {
            $('#passwordHelp').html('<span style="color: green;">Пароли совпадают </span>');
            $(".add-user button").prop( "disabled", false );
        }
        else
        {
            $('#passwordHelp').html('<span style="color: red;">Пароли не совпадают </span>');
            $(".add-user button").prop( "disabled", true );
        }

    });
    $(document).on('keyup', 'input#client_search_tel', function (event, sel) {

        if($(this).val().length==17)
        {
            var out={};
            out['client_search_tel'] = $(this).val();
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:out,
                success:function(resp){
                    var resp_in =JSON.parse(resp);
                    if(resp_in.result) {
                        $('#client_searchStatus').html('<span style="color: green;">Клиент найден! </span>');
                        var resp_mail='';
                        var resp_name='';
                        if(resp_in.mail !== null)  resp_mail=resp_in.mail;
                        if(resp_in.name !== null)  resp_name=resp_in.name;

                        $('.client-data').html('<span>Добавить остатки клиенту <h3>'+resp_name+' '+resp_mail+'</h3></span>');
                        $('input[name="clientID"]').val(resp_in.client_id);
                        $('.client-data').show();

                        $('#client_search_tel').prop( "disabled", true );
                        $('form.coming').show();
                    }
                    else
                    {
                        $('#client_searchStatus').html('<span style="color: red;">Такой телефон не существует, повторите поиск... </span>');
                        $('.client-data').html();
                        $('.client-data').hide();
                    }
                }
            });
        }
        else
        {
            $('#client_searchStatus').html();
        }
    });
    // $(document).on('input', 'input#client_search_tel', function (event, sel) {
    //
    //     if($(this).val().length==17)
    //     {
    //         var out={};
    //         out['client_search_tel'] = $(this).val();
    //         $.ajax({
    //             type: 'POST',
    //             url:'ajax_resp.php',
    //             data:out,
    //             success:function(resp){
    //                 var resp_in =JSON.parse(resp);
    //                 if(resp_in.result) {
    //                     $('#client_searchStatus').html('<span style="color: green;">Клиент найден! </span>');
    //                     var resp_mail='';
    //                     var resp_name='';
    //                     if(resp_in.mail !== null)  resp_mail=resp_in.mail;
    //                     if(resp_in.name !== null)  resp_name=resp_in.name;
    //
    //                     $('.client-data').html('<span>Добавить остатки клиенту <h3>'+resp_name+' '+resp_mail+'</h3></span>');
    //                     $('input[name="clientID"]').val(resp_in.client_id);
    //                     $('.client-data').show();
    //
    //                     $('#client_search_tel').prop( "disabled", true );
    //                     $('form.coming').show();
    //                 }
    //                 else
    //                 {
    //                     $('#client_searchStatus').html('<span style="color: red;">Такой телефон не существует, повторите поиск... </span>');
    //                     $('.client-data').html();
    //                     $('.client-data').hide();
    //                 }
    //             }
    //         });
    //     }
    //     else
    //     {
    //         $('#client_searchStatus').html();
    //     }
    // });
    $(document).on('click', '.coming #add', function (event, sel) {
        var cnt=parseInt( $('.coming .tmpl').data("count"))+1;
        $('.coming .tmpl').data("count",cnt);
        var Cline=$('.coming .tmpl').html();
        $('.coming .coming-in').append('<div class="row line-'+cnt+' add-lines" data-index="'+cnt+'">'+Cline+'</div>');
        $('.coming  .line-'+cnt+' input').attr("required", true);
        $('.coming  .line-'+cnt+' select').attr("required", true);
        $(".coming   button.btn-success").prop( "disabled", false );
    });
    $(document).on('click', '.list-cells #addLine', function (event, sel) {
        var teLine=$('tr#template');
        var teLine2='<tr>'+teLine[0]['innerHTML']+'</tr>';
       $('.list-cells table tr:last').after(teLine2);
        $('.list-cells table tr:last input').attr("required", true);
        var count = $(".list-cells tbody tr").length;
        $('.list-cells #countCells').val(count);

    });
    /*
    $(document).on('change', '.coming .add-lines input[name="L[]"]', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        double_verify(container);
//        var cnt=parseInt( $('.coming .tmpl').data("count"))+1;
    });
    */
    $(document).on('change', 'form.return input[name^="return_"]', function (event, sel) {
        var retmax=$(this).data("retmax");
        var ret=$(this).val();
        if(ret>retmax) $(this).val(retmax);
        if(ret<0) $(this).val(0);
    });
    $(document).on('keypress keyup blur', 'form.into-production input[name^="return_"]', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
        var retmax=$(this).data("retmax");
        var numline=$(this).data("numline");
        var ret=$(this).val();
        if(ret>retmax) $(this).val(retmax);
        if(ret<0) $(this).val(0);
        var newval=$(this).val();
        if(newval>0)
        {
            $("form.into-production #orders_"+numline).attr('required',"required");
            $("form.into-production #comment_"+numline).attr('required',"required");
        }
        else
        {
            $("form.into-production #orders_"+numline).val('');
            $("form.into-production #comment_"+numline).val('');
            $("form.into-production #orders_"+numline).removeAttr('required');
            $("form.into-production #comment_"+numline).removeAttr('required');
        }
    });
    $(document).on('change', 'form.return input[name^="corrects_"]', function (event, sel) {
        var retmax=parseInt($(this).data("retmax"));
        var ret=parseInt($(this).val());
        if((ret+retmax)<0) $(this).val(0-retmax);
    });
     $(document).on('keyup', 'form.write-off input[name^="return_"]', function (event, sel) {
        var index=$(this).data("index");
        var ret=$(this).val();
        if(ret>0)
        {
            $(".return-opt-"+index).show();
        }
        else
        {
            $(".returncomment-"+index+" textarea").val('');
            $(".returncomment-"+index+" textarea").removeAttr('required');
            $(".returncomment-"+index).hide();
            $('input:radio[name="cause_ret_'+index+'"][value="time"]').prop('checked', true);
            $(".return-opt-"+index).hide();
        }
    });
    $(document).on('change', 'form.write-off input[name^="cause_ret_"]', function (event, sel) {
        var index=$(this).parent().parent().data("index");
        var ret=$(this).val();
        if(ret=='refuse')
        {
            $(".returncomment-"+index).show();
            $(".returncomment-"+index+" textarea").attr('required',"required");
        }
        else
        {
            $(".returncomment-"+index+" textarea").val('');
            $(".returncomment-"+index+" textarea").removeAttr('required');
            $(".returncomment-"+index).hide();
        }
    });
    $(document).on('click', '.erase-tr', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        $("div.line-"+container).remove();
        if($(".coming .row.add-lines").length==1) $(".coming   button.btn-success").prop( "disabled", true );
    });
    $(document).on('click', '.cell-erase-tr', function (event, sel) {
        $(this).parent().parent().remove();

    });

    $(document).on('keyup', '.searchOnCode', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        $.ajax({
            type: 'POST',
            url:'ajax_resp.php',
            data:'keyword='+$(this).val()+'&contID='+container,
            beforeSend: function(){
                $(".line-"+container+" .loading-search.ls-onCODE").show();
            },
            success: function(data){
                $(".line-"+container+" .searchOnCode-box").show();
                $(".line-"+container+" .searchOnCode-box").html(data);
                $(".line-"+container+" .loading-search.ls-onCODE").hide();
            }
        });
    });
    $(document).on('blur', '.searchOnCode', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        setTimeout(function(){  $(".line-"+container+" .searchOnCode-box").hide();}, 1500);

    });
    $(document).on('keyup', '.searchOnT', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        $.ajax({
            type: 'POST',
            url:'ajax_resp.php',
            data:'keyT='+$(this).val()+'&contID='+container,
            beforeSend: function(){
                $(".line-"+container+" .loading-search.ls-onT").show();
            },
            success: function(data){
                $(".line-"+container+" .searchOnT-box").show();
                $(".line-"+container+" .searchOnT-box").html(data);
                $(".line-"+container+" .loading-search.ls-onT").hide();
            }
        });
    });
    $(document).on('keypress keyup blur', '.onlyNumber', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
    });
    $(document).on('blur', '.searchOnT', function (event, sel) {
        var container=$(this).parent().parent().parent().data("index");
        setTimeout(function(){  $(".line-"+container+" .searchOnT-box").hide(); }, 500);

    });
    $(document).on('keypress keyup blur', '.searchOnT', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
    });

    $(document).on('keypress keyup blur', '.write-off #manual input#numMat', function (event, sel) {
        $(this).val($(this).val().replace(/[^\d][\.][^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            if (event.which != 46) event.preventDefault();
        }
        if(parseInt($(this).val())>0)
        {
            $(".write-off #manual input#numMat").attr('required',"required");
            $(".write-off #manual select#select_places").removeAttr('required');
        }
    });
    $(document).on('change', '.write-off #manual select#select_places', function (event, sel) {
        var recipient = parseInt($(this).find(':selected').val());
        if(!isNaN(recipient))
        {
            $(".write-off #manual input#numMat").removeAttr('required');
            $(".write-off #manual select#select_places").attr('required',"required");
        }
    });
    $(document).on('change', '.balance select[name="cells[]"]', function (event, sel) {
        var newCell = parseInt($(this).find(':selected').val());
        if(!isNaN(newCell))
        {
            var uid=$(this).data('uid');
            var place=$(this).data('place');
            var mid=$(this).data('mid');
            var trID=mid+'-'+place+'-'+uid;
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'newCell='+newCell+'&uid='+uid+'&place='+place+'&mid='+mid,
                beforeSend: function(){
                    $('<span style="color: red; font-size: 12px;" class="'+trID+'-write">Пишем...</span>').insertAfter('#'+trID+' select');
                },
                success: function(data){
                    $('.'+trID+'-write').remove();
                    $('<span style="color: green; font-size: 12px;" class="'+trID+'-apple">Записали...</span>').insertAfter('#'+trID+' select');
console.log(data);
                    setTimeout(function(){$('.'+trID+'-apple').remove();}, 3000);
                }
            });
        }
    });
    $(document).on('change', '.coming select#select_places', function (event, sel) {
        var recipient = parseInt($(this).find(':selected').val());
        if(!isNaN(recipient))
        {
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'selectCells='+recipient,
                success: function(data){
                    $(".coming select[name='cells[]']").html(data);
                }
            });
        }
    });
    $(document).on('change', '.balance select#select_places', function (event, sel) {
        var recipient = parseInt($(this).find(':selected').val());
        if(!isNaN(recipient))
        {
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'selectCells='+recipient,
                success: function(data){
                    $(".balance select[name='cells']").html(data);
                }
            });
        }else{
            $(".balance select[name='cells']").html('<option value="" selected="">Выберите склад</option>');
        }
    });
function add_user_showhide() {
    var m=parseInt($('#new_mail').attr('data-valid'));
    var p=parseInt($('#new_phone').attr('data-valid'));
    if (m+p>0) {
        $(".input-name input").val('');
        $(".input-password input").val('');
        $(".input-name").show();
        $(".input-password").show();
        $(".add-user button").show();
    } else {
        $(".input-name input").val('');
        $(".input-password input").val('');
        $(".input-name").hide();
        $(".input-password").hide();
        $(".add-user button").hide();
    }
}

    $('#send_pin_code').on('click', async function (e) {
        e.preventDefault();
        var tel = $('#phone1').val();
        console.log(tel);
        if(tel)
        {
            $.ajax({
                type: 'POST',
                url:'ajax_resp.php',
                data:'client_tel='+tel,
                success: function(data){
                    if(data == 1) {
                        $('#send_pin_group').hide();
                        $('#send_pin_code').hide();
                        $('#set_pin_code').show();
                    } else {
                        $('#client_auth_message').html(data);
                    }
                }
            });
        }
    });

});