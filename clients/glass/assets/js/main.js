
$(function () {
    /**======================
     * variables
     * ======================*/
    let matLength = $('#matLength').text();// only on /details.php
    let matWidth = $('#matWidth').text();// only on /details.php
    let inputLength = $('#addedLengthPart');// only on /details.php
    let inputWidth = $('#addedWidthPart');// only on /details.php
    let inputQuantity = $('#addedQuantityPart');// only on /details.php
    const preloader = $('#start_checks_lodaer_containers');

    /**
     * Creates a new form's field for showing variables of materials. Works on /index.php
     * */
    $("#typeMaterial").on('change',function () {
        let val = $(this).val();
        let url = "get_materials.php";
        let parent = $(this).parent().parent();
        if (this.value !== "---") {
            $.post(
                url,
                {
                    type_material: val,
                },
                function(data) {
                    let materials = JSON.parse(data);
                    let typeMaterial = $('#typeMaterial option:selected').text();
                    let nw = $('#getNW').val();
                    let additionalSelect = "<div class='loadedForm'>\n" +
                        "                       <h4 class=\"text-center mb-3\">Теперь выберете материал из выподающего списка</h4>\n" +
                        "                       <form action=\"details.php?nw="+nw+"\" method=\"POST\">\n" +
                        "                         <div class=\"form-group\">\n" +
                        "                           <input type='hidden' name='type_material' value='"+ typeMaterial +"'>\n" +
                        "                           <label class='mb-3' for=\"material\">Выберете материал из списка</label>\n" +
                        "                           <select class=\"form-control\" id=\"material\" name='material_id'>\n";
                    for (let material of materials) {
                        additionalSelect += "           <option value='" + material.id + "'>" + material.name + "</option>\n";
                    }
                    additionalSelect += "           </select>\n" +
                        "                          </div>\n" +
                    "                              <button type=\"submit\" class=\"btn btn-primary mb-3\">Создать</button>\n" +
                        "                       </form>\n</div>\n";
                    parent.find('.loadedForm').remove();
                    parent.append(additionalSelect);
                }
            );
        } else {
            parent.find('.loadedForm').remove();
        }
    });

    /**
     * Redirects to the content selection page
     * */
    $('#forExit').on('click', function () {
       let message = '<div id="messageForExit" class="row alert alert-danger justify-content-around" style="margin: 0 auto;">\n' +
           '<h4 class="col-sm-10 text-center">Не сохораненные данные будут потеряны</h4>' +
           '<button id="cancel" class="col-sm-4 btn btn-light text-center">Отмена</button>' +
           '<a href="/clients/glass/index.php" class="col-sm-4 btn btn-primary text-center">Выйти</a>' +
           '          </div>';
       $('#pageTitle').prepend(message);
       $('#cancel').on('click', function (event) {
            event.preventDefault();
            $('#pageTitle').find('#messageForExit').remove();
       });
    });

    $('#saveOrder').on('click', function(event){
        event.preventDefault();
        let url = $('#formSaveOrder').attr('action');
        let order_st = $('input[name=order_st]').val();
        let downloadOrder = $('#downloadOrder');
        preloader.removeClass('d-none');
        $.post(
            url,
            'order_st=' + order_st,
            function (data) {
                const type = 'data:application/octet-stream;text, ';
                downloadOrder.attr('href', type + data);
                downloadOrder.removeClass('hidden');
                preloader.addClass('d-none');
                downloadOrder.click();
            }
        );
    });

    /**
     * Loaded a saved project of the part
     * */
    $('.custom-file').on('click', function () {
        $('.custom-file').css({'border': 'none'});
        $(this).css({'border': 'solid 2px #181a1b91'});
        let file = $(this).prev().val();
        let url = $(this).parents('form').attr('action');
        let divMessage = $(this).parents('.modal-content').find('.modal-footer').find('#message');
        divMessage.addClass('alert alert-warning').text('Не сохраненные данные будут потеряны. ' +
            'Если вы не сохранили данные, закройте это окно и сохраните их');
        $('#chooseOrder').on('click', function (event) {
            event.preventDefault();
            $.post(
                url,
                "file_path=" + file,
                function() {
                    location.reload();
                }
            );
        });
    });

    $('#fileOrder').on('change', function (event) {
        event.preventDefault();
        preloader.removeClass('d-none');
        let divMessage = $('#messageForUploadOrder');
        let formData = new FormData();
        $.each(this.files, function (k, v) {
            formData.append(k, v);
        });
        $.ajax({
            type: 'POST',
            url: 'modules/load_save_order.php',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                let response = JSON.parse(data);
               if (response.error) {
                   divMessage.addClass('alert alert-danger').text(response.message);
                   preloader.addClass('d-none');
                   setTimeout(function() {
                       divMessage.removeClass('alert alert-danger').text('');
                   }, 2000);
               } else {
                   location.reload();
               }
            },
            error: function (data) {
                console.error(data);
            }
        });
    });

    // $('#orderSubmit').on('click', function (event) {
    //     event.preventDefault();
    //     let url = $('#sendOrder').attr('action');
    //     let sendData = $('input[name=order_st]').val();
    //     $.post(
    //         url,
    //         'order_st=' + sendData,
    //         function(data) {
    //             console.log(data);
    //         }
    //     );
    // });

    if (document.URL.indexOf('details') > 0) {
        // inserts a draw of parts
        setDrawOfParts();
    } else if (document.URL.indexOf('treatment') > 0) {
        if ($('#imgCurrentPart').attr('src') === '') {
            $('#pdfCurrentPart').hide();
            drawParts('#draw', Number($('#partL').text()), Number($('#partW').text()));
            checkerSelectOfEdge($('#l_edge'));
            checkerSelectOfEdge($('#r_edge'));
            checkerSelectOfEdge($('#t_edge'));
            checkerSelectOfEdge($('#b_edge'));
        } else {
            $('#imgCurrentPart').removeClass('d-none');
        }
    }



    /**
     * Validates fields for creating a new detail. Works on /details.php
     * */
    // validates length
    inputLength.on('change', function () {
        $("#btnCreateDetail").attr('disabled', true).addClass('btn-light');
        let message = $("#messageCreatDetail");
        if (Number(this.value) > Number(matLength)) {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-invalid');
            message.addClass('alert alert-danger').text('Длина не должна привышать ' + matLength + ' мм');
            setTimeout(function () {
                inputLength.val('');
            }, 500);
        } else {
            message.removeClass('alert alert-danger').text('');
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    // validates width
    inputWidth.on('change', function () {
        let message = $("#messageCreatDetail");
        if (Number(this.value) > Number(matWidth)) {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-invalid');
            message.addClass('alert alert-danger').text('Ширина не должна привышать ' + matWidth + ' мм');
            setTimeout(function () {
                inputWidth.val('');
            }, 500);
        } else {
            message.removeClass('alert alert-danger').text('');
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    //validates quantity of parts
    inputQuantity.on('change', function () {
        let maessage = $('#messageCreatDetail');
        let val = Number(this.value);
        
        if (val <= 0) {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-invalid');
            maessage.addClass('alert alert-danger').text('Не верное количество');
        } else if (inputLength.hasClass('is-valid') && inputWidth.hasClass('is-valid')) {
            maessage.removeClass('alert alert-danger').text('');
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
            $("#btnCreateDetail").attr('disabled', false).removeClass('btn-light');
        }
    });

    /**
     * Creates a new detail. Works on /details.php
     * */
    $("#btnCreateDetail").on('click', function (event) {
        event.preventDefault();
        let inputLength = $('#addedLengthPart');
        let inputWidth = $('#addedWidthPart');
        let inputQuantity = $('#addedQuantityPart');
        let width = inputWidth.val();
        let length = inputLength.val();
        let quantity = inputQuantity.val();
        let url = $('#formCreateDetail').attr('action');
        preloader.removeClass('d-none');
        $.post(
            url,
            {
                width: width,
                length: length,
                quantity: quantity,
            },
            function (data) {
                setRowsInTable(data);

                let inputs = [inputQuantity, inputLength, inputWidth];
                clearAllInputs(inputs);
                for (input of inputs) {
                    input.removeClass('is-valid');
                }
                preloader.addClass('d-none');
            }
        );
        $("#forAddDetails").modal('hide');
    });

    /**
     * This function monitors corner editing and controls access to operations for changing them
     * */
    function monitorCornerEditing() {
        let radiusCorners = $('#rСutCorners');
        let srezCorners = $('#srezCorners');
        let zarezCorners = $('#zarezCorners');
        let radiusTab = $('#nav-radius-cut-corners');
        let srezTab = $('#nav-srez-corners');
        let zarezTab = $('#nav-zarez-corners');
        let radiusLu = $('#radiusLu');
        let radiusRu = $('#radiusRu');
        let radiusLd = $('#radiusLd');
        let radiusRd = $('#radiusRd');
        let inputRadLu = $('input[name=radius_lu]');
        let inputRadRu = $('input[name=radius_ru]');
        let inputRadLd = $('input[name=radius_ld]');
        let inputRadRd = $('input[name=radius_rd]');
        let srezLu = $('#srezLu');
        let srezRu = $('#srezRu');
        let srezLd = $('#srezLd');
        let srezRd = $('#srezRd');
        let srezInputs = srezTab.find('input');
        let zarezLu = $('#zarezLu');
        let zarezRu = $('#zarezRu');
        let zarezLd = $('#zarezLd');
        let zarezRd = $('#zarezRd');
        let zarezInputs = zarezTab.find('input');
        if (inputRadLu.val() !== '') {
            srezLu.addClass('hidden');
            zarezLu.addClass('hidden');
        } else {
            if (zarezInputs[0].value === "" && zarezInputs[1].value === '') srezLu.removeClass('hidden');
            if (srezInputs[0].value === "" && srezInputs[1].value === '') zarezLu.removeClass('hidden');
        }
        if (inputRadRu.val() !== '') {
            srezRu.addClass('hidden');
            zarezRu.addClass('hidden');
        } else {
            if (zarezInputs[2].value === "" && zarezInputs[3].value === '') srezRu.removeClass('hidden');
            if (srezInputs[2].value === "" && srezInputs[3].value === '') zarezRu.removeClass('hidden');
        }
        if (inputRadRd.val() !== '') {
            srezRd.addClass('hidden');
            zarezRd.addClass('hidden');
        } else {
            if (zarezInputs[4].value === "" && zarezInputs[5].value === '') srezRd.removeClass('hidden');
            if (srezInputs[4].value === "" && srezInputs[5].value === '') zarezRd.removeClass('hidden');
        }
        if (inputRadLd.val() !== '') {
            srezLd.addClass('hidden');
            zarezLd.addClass('hidden');
        } else {
            if (zarezInputs[6].value === "" && zarezInputs[7].value === '') srezLd.removeClass('hidden');
            if (srezInputs[6].value === "" && srezInputs[7].value === '') zarezLd.removeClass('hidden');
        }

        if (srezInputs[0].value !== '' && srezInputs[1].value !== '') {
            zarezLu.addClass('hidden');
            radiusLu.addClass('hidden');
        } else {
            if (inputRadLu === '') zarezLu.removeClass('hidden');
            if (zarezInputs[0].value === '' && zarezInputs[1].value === '') radiusLu.removeClass('hidden');
        }
        if (srezInputs[2].value !== '' && srezInputs[3].value !== '') {
            zarezLd.addClass('hidden');
            radiusLd.addClass('hidden');
        } else {
            if (inputRadLd === '') zarezLd.removeClass('hidden');
            if (zarezInputs[2].value === '' && zarezInputs[3].value === '') radiusLd.removeClass('hidden');
        }
        if (srezInputs[4].value !== '' && srezInputs[5].value !== '') {
            zarezRu.addClass('hidden');
            radiusRu.addClass('hidden');
        } else {
            if (inputRadRu === '') zarezRu.removeClass('hidden');
            if (zarezInputs[4].value === '' && zarezInputs[5].value === '') radiusRu.removeClass('hidden');
        }
        if (srezInputs[6].value !== '' && srezInputs[7].value !== '') {
            zarezRd.addClass('hidden');
            radiusRd.addClass('hidden');
        } else {
            if (inputRadRd === '') zarezRd.removeClass('hidden');
            if (zarezInputs[6].value === '' && zarezInputs[7].value === '') radiusRd.removeClass('hidden');
        }

        if (zarezInputs[0].value !== '' && zarezInputs[0].value !== '') {
            radiusLu.addClass('hidden');
            srezLu.addClass('hidden');
        } else {
            if (srezInputs[0].value === '' && srezInputs[1].value === '') radiusLu.removeClass('hidden');
            if (inputRadLu.val() === '') srezLu.removeClass('hidden');
        }
        if (zarezInputs[2].value !== '' && zarezInputs[3].value !== '') {
            radiusLd.addClass('hidden');
            srezLd.addClass('hidden');
        } else {
            if (srezInputs[2].value === '' && srezInputs[3].value === '') radiusLd.removeClass('hidden');
            if (inputRadLd.val() === '') srezLd.removeClass('hidden');
        }
        if (zarezInputs[4].value !== '' && zarezInputs[5].value !== '') {
            radiusRu.addClass('hidden');
            srezRu.addClass('hidden');
        } else {
            if (srezInputs[4].value === '' && srezInputs[5].value === '') radiusRu.removeClass('hidden');
            if (inputRadRu.val() === '') srezRu.removeClass('hidden');
        }
        if (zarezInputs[6].value !== '' && zarezInputs[7].value !== '') {
            radiusRd.addClass('hidden');
            srezRd.addClass('hidden');
        } else {
            if (srezInputs[6].value === '' && srezInputs[7].value === '') radiusRd.removeClass('hidden');
            if (inputRadRd.val() === '') srezRd.removeClass('hidden');
        }
        if (radiusLu.hasClass('hidden') && radiusLd.hasClass('hidden') &&
            radiusRu.hasClass('hidden') && radiusRd.hasClass('hidden')) {
            radiusCorners.addClass('hidden');
            radiusTab.addClass('hidden');
        } else {
            radiusCorners.removeClass('hidden');
            radiusTab.removeClass('hidden');
        }
        if (srezLu.hasClass('hidden') && srezLd.hasClass('hidden') &&
            srezRu.hasClass('hidden') && srezRd.hasClass('hidden')) {
            srezCorners.addClass('hidden');
            srezTab.addClass('hidden');
        } else {
            srezCorners.removeClass('hidden');
            srezTab.removeClass('hidden');
        }
        if (zarezLu.hasClass('hidden') && zarezLd.hasClass('hidden') &&
            zarezRu.hasClass('hidden') && zarezRd.hasClass('hidden')) {
            zarezCorners.addClass('hidden');
            zarezTab.addClass('hidden');
        } else {
            zarezCorners.removeClass('hidden');
            zarezTab.removeClass('hidden');
        }
    }
    // the function works on /treatment.php
    if (document.URL.indexOf('treatment.php') !== -1) monitorCornerEditing();

    /**
     * Changes options of treatment of edges if setup radius cutting. Works on /treatment.php
     * */
    $('.select-for-edge').on('change', function () {
        let edge1 = $(this);
        let edge2, edge3, corner1, corner2;
        let btn = $('#edgeSubmit');
        if (edge1.attr('name').indexOf('l') > -1) {
            edge2 = $('#b_edge');
            edge3 = $('#t_edge');
            corner1 = $('input[name=radius_ld]');
            corner2 = $('input[name=radius_lu]');
        }
        if (edge1.attr('name').indexOf('t') > -1) {
            edge2 = $('#l_edge');
            edge3 = $('#r_edge');
            corner1 = $('input[name=radius_lu]');
            corner2 = $('input[name=radius_ru]');
        }
        if (edge1.attr('name').indexOf('r') > -1) {
            edge2 = $('#t_edge');
            edge3 = $('#b_edge');
            corner1 = $('input[name=radius_ru]');
            corner2 = $('input[name=radius_rd]');
        }
        if (edge1.attr('name').indexOf('b') > -1) {
            edge2 = $('#r_edge');
            edge3 = $('#l_edge');
            corner1 = $('input[name=radius_rd]');
            corner2 = $('input[name=radius_ld]');
        }

        if (corner1.val() !== '') {
            btn.attr('disabled', true).addClass('btn-light');
            if (edge1.val() !== edge2.val()) {
                edge2.parent().find(".alert").remove();
                edge2.parent().append('<div class="alert ' +
                    'alert-warning">Прилегающая сторона должна одинаково обрабатываться.</div>');
            } else {
                edge2.parent().find(".alert").remove();
                edge1.parent().find(".alert").remove();
            }
            if (edge2.val() === edge1.val()) {
                btn.attr('disabled', false).removeClass('btn-light');
            }
        }
        if (corner2.val() !== '') {
            btn.attr('disabled', true).addClass('btn-light');
            if (edge1.val() !== edge3.val()) {
                edge3.parent().find(".alert").remove();
                edge3.parent().append('<div class="alert ' +
                    'alert-warning">Прилегающая сторона должна одинаково обрабатываться.</div>');
            } else {
                edge3.parent().find(".alert").remove();
                edge1.parent().find(".alert").remove();
            }

            if (edge3.val() === edge1.val()) {
                btn.attr('disabled', false).removeClass('btn-light');
            }
        }
    });

    /**
     * checks a width of the facets
     * */
    $('.addedFields input').on('change', function () {
        let input1 = $(this);
        let input2, input3, corner1, corner2;
        let btn = $('#edgeSubmit').attr('disabled', true).addClass('btn-light');
        if (input1.attr('name').indexOf('l') > -1) {
            input2 = $('input[name=b_length]');
            input3 = $('input[name=t_length]');
            corner1 = $('input[name=radius_ld]');
            corner2 = $('input[name=radius_lu]');
        }
        if (input1.attr('name').indexOf('t') > -1) {
            input2 = $('input[name=l_length]');
            input3 = $('input[name=r_length]');
            corner1 = $('input[name=radius_lu]');
            corner2 = $('input[name=radius_ru]');
        }
        if (input1.attr('name').indexOf('r') > -1) {
            input2 = $('input[name=t_length]');
            input3 = $('input[name=b_length]');
            corner1 = $('input[name=radius_ru]');
            corner2 = $('input[name=radius_rd]');
        }
        if (input1.attr('name').indexOf('b') > -1) {
            input2 = $('input[name=r_length]');
            input3 = $('input[name=l_length]');
            corner1 = $('input[name=radius_rd]');
            corner2 = $('input[name=radius_ld]');
        }

        if (corner1.val() !== '') {
            if (input1.val() !== input2.val()) {
                input2.parent().find(".alert").remove();
                input2.parent().parent().removeClass('hidden');
                input2.addClass('is-invalid');
                input2.parent().append('<div class="col-sm-12 alert ' +
                    'alert-warning">Ширина фацета должна быть такая же, как и у прилегающей стороны</div>');
            } else {
                input2.removeClass('is-invalid');
                input2.parent().find(".alert").remove();
                input1.parent().find(".alert").remove();
            }
        }
        if (corner2.val() !== '') {
            if (input1.val() !== input3.val()) {
                input3.parent().find(".alert").remove();
                input3.parent().parent().removeClass('hidden');
                input3.addClass('is-invalid');
                input3.parent().append('<div class="col-sm-12 alert ' +
                    'alert-warning">Ширина фацета должна быть такая же, как и у прилегающей стороны</div>');
            } else {
                input3.removeClass('is-invalid');
                input3.parent().find(".alert").remove();
                input1.parent().find(".alert").remove();
            }
        }
    });


    /**
     * Creates new form's fields for treatment an edge of the detail. Works on /treatment.php
     * */
    $(".select-for-edge").on('click',function () {
        let idChangedSelect = $(this).attr('id');
        $('#' + idChangedSelect).on('change',function () {
            let parent = $(this).parent().parent();
            let message = parent.find('.message-added-fields');
            let addedFields = parent.find('.addedFields').removeClass('hidden');
            let inputLength = addedFields.find('.col-sm-6').find('input');
            let selected = this.value;
            if (selected === 'facet') {
                let maxFacet = $('#maxFacet').val();
                let button = $('#edgeSubmit');
                let selects = $('select.select-for-edge');
                if (inputLength.val() === '') message
                    .addClass('alert alert-warning')
                    .text('Ширина фацета должна быть от 1 мм до ' + maxFacet + ' мм');
                inputLength.on('change', function () {
                    let val = Number(inputLength.val());
                    let max = Number(maxFacet);
                    if (inputLength.val() !== '') {
                        if (val > max || val <= 0) {
                            button.attr('disabled', true);
                            selects.attr('disabled', true);
                            button.removeClass('btn-primary').addClass('btn-light');
                            message.removeClass('alert alert-warning').text();
                            message.addClass('alert alert-danger').text('Значение болше ' + maxFacet + 'мм или меньше нуля');
                            inputLength.removeClass('is-valid').addClass('is-invalid');
                        } else {
                            selects.attr('disabled', false);
                            message.removeClass('alert alert-warning').text();
                            inputLength.addClass('is-valid').removeClass('is-invalid');
                            message.removeClass('alert alert-danger').text('');
                            button.attr('disabled', false);
                            button.removeClass('btn-light').addClass('btn-primary');
                        }
                    } else {
                        inputLength.removeClass('is-valid').removeClass('is-invalid');
                        selects.attr('disabled', false);
                        message.removeClass('alert alert-warning').text();
                        message.removeClass('alert alert-danger').text('');
                        button.attr('disabled', false);
                        button.removeClass('btn-light').addClass('btn-primary');
                    }
                });
            } else {
                message.removeClass('alert alert-danger').text('');
                parent.find('.addedFields').addClass('hidden');
                inputLength.removeClass('is-valid').removeClass('is-invalid');
                inputLength.val('');
            }
        });
    });

    /**
     * Updates links <img> <a> of the current part.
     * @param data this a string html code of response form the server
     * */
    function updateLinksImgAndPdf(data) {
        if (data.path === undefined) {
            $('#pdfCurrentPart').hide();
        } else {
            $('#pdfCurrentPart').show();
            $('#imgCurrentPart').removeClass('d-none');
            $('#pdfCurrentPart').attr('href', data.path[0]);
            $('#imgCurrentPart').attr('src', data.path[1]);
            $('#draw>svg').remove();
        }
    }

    /**
     * Edits edges of the detail. Works on /treatment.php.
     * */
    $('#edgeSubmit').on('click', function (event) {
        event.preventDefault();
        let request = "simple_part=true&edges=1&num_detail=" + $('input[name=currentPart]').val() +
                        "&l_edge=" + $("#l_edge").val() + "&r_edge=" + $("#r_edge").val() +
                        "&t_edge=" + $("#t_edge").val() + "&b_edge=" + $("#b_edge").val();
        if ($("#l_edge").val() === 'facet' && $('input[name=l_length]').val() !== '') {
            request += "&l_length=" + $('input[name=l_length]').val();
        }
        if ($("#r_edge").val() === 'facet' && $('input[name=r_length]').val() !== '') {
            request += "&r_length=" + $('input[name=r_length]').val();
        }
        if ($("#t_edge").val() === 'facet' && $('input[name=t_length]').val() !== '') {
            request += "&t_length=" + $('input[name=t_length]').val();
        }
        if ($("#b_edge").val() === 'facet' && $('input[name=b_length]').val() !== '') {
            request += "&b_length=" + $('input[name=b_length]').val();
        }
        preloader.removeClass('d-none');
        $.post(
            'modules/edit_detail.php',
            request,
            function (data) {
                let html = $(data);
                let order = JSON.parse(html.eq(0).text());
                updateLinksImgAndPdf(order.parts[$('input[name=currentPart]').val()]);
                preloader.addClass('d-none');
            }
        );
        $(this).parent().find('input.form-control').removeClass('is-valid');
    });

    /**
     * Validations for rounding corners
     * */
    let nameFirstCorner = '';
    let isFirstCorner = false;
    let firstCornerValue = '';
    let relatedCorners = [];
    function validationRoundingCorner() {
        $(this).removeClass('is-valid is-invalid');
        let message = $(this).parent().find('.messageForRoundingCorner');
        let button = $("#radiusSubmit");
        let inputs = $('input.radius').not(this);
        if (nameFirstCorner === '') {
            for(input of inputs) {
                if ($(input).val() !== '') {
                    isFirstCorner = true;
                    nameFirstCorner = $(input).attr('name');
                    firstCornerValue = Number($(input).val());
                    if (nameFirstCorner === 'radius_ru') {
                        relatedCorners = ['radius_lu', 'radius_rd', 'radius_ld'];
                    } else if (nameFirstCorner === 'radius_rd') {
                        relatedCorners = ['radius_ru', 'radius_ld', 'radius_lu'];
                    } else if (nameFirstCorner === 'radius_ld') {
                        relatedCorners = ['radius_rd', 'radius_lu', 'radius_ru'];
                    } else if (nameFirstCorner === 'radius_lu') {
                        relatedCorners = ['radius_ld', 'radius_ru', 'radius_rd'];
                    }
                    break;
                }
            }
        }
        let length = Number($('#partL').text());
        let width = Number($('#partW').text());
        let maxR = Math.sqrt((length * length) + (width * width)) -1;
        let numValue = Number(this.value);
        let thisInputName = $(this).attr('name');
        if (thisInputName === nameFirstCorner) {
            firstCornerValue = Number($(this).val());
            inputs.val('');
            inputs.removeClass('is-invalid').removeClass('is-valid');
            $('.messageForRoundingCorner').removeClass('alert alert-danger').text('');
            if ($(this).val() === '') nameFirstCorner = '';
        }
        let adjSide1 = (nameFirstCorner === 'radius_ru' || nameFirstCorner === 'radius_ld') ? length : width;
        let adjSide2 = (nameFirstCorner === 'radius_rd' || nameFirstCorner === 'radius_lu') ? length : width;
        let allowRadius1 = adjSide1 - firstCornerValue;
        let allowRadius2 = adjSide2 - firstCornerValue;
        let allowRadius3 = maxR - firstCornerValue;

        if (this.value !== '') {
            if (isFirstCorner && thisInputName !== nameFirstCorner) {
                if (thisInputName === relatedCorners[0]) {
                    if (numValue > allowRadius1 || numValue <= 0) {
                        let text = 'Радиус должен быть от 1 мм до ' + allowRadius1 + " мм";
                        makeInvalidRadiusField($(this), button, text);
                    } else {
                        makeValidRadiusField($(this), button);
                    }
                }
                if (thisInputName === relatedCorners[1]) {
                    if (numValue > allowRadius2 || numValue <= 0) {
                       let text = 'Радиус должен быть от 1 мм до ' + allowRadius2 + " мм";
                       makeInvalidRadiusField($(this), button, text);
                    } else {
                        makeValidRadiusField($(this), button);
                    }
                }
                if (thisInputName === relatedCorners[2]) {
                    if (numValue > allowRadius3 || numValue <= 0) {
                        let text = 'Радиус должен от 1 мм до ' + Math.round(allowRadius3) + " мм";
                        makeInvalidRadiusField($(this), button, text);
                    } else {
                        makeValidRadiusField($(this), button);
                    }
                }
            } else {
                if (numValue <= 0 || numValue > maxR) {
                   let text = 'Радиус должен быть от 1 мм и до ' + Math.round(maxR) + ' мм';
                   makeInvalidRadiusField($(this), button, text);
                } else {
                    makeValidRadiusField($(this), button);
                }
            }
        } else {
            inputs.attr('disabled', false);
            $(this).removeClass('is-valid')
            message.removeClass('alert alert-danger').text('');
            button.removeClass('btn-light');
            $(this).removeClass('is-invalid');
            button.attr('disabled', false);
        }
        let adjacentEdges = getAdjacentSideOfEdging($(this).attr('name'));
        if (adjacentEdges[0].val() !== adjacentEdges[1].val()) {
            message.addClass('alert alert-warning').text('2 прилегающие стороны должны одинаково обрабатываться!' +
                'При сохранении округления этого угла, обработка прилегающих торцов удалится.' +
                ' Выберете обработку этих торцов.');
            setTimeout(function () {
                message.removeClass('alert-warning').text('');
            }, 30000);
        }
        adjacentEdges[0].val('-Нет-');
        adjacentEdges[1].val('-Нет-');
    }
    /**
     * Returns adjacent side of edging by the corner
     * @param corner this is a name the corner
     * @return array adjacent side of edging
     * */
    function getAdjacentSideOfEdging(corner) {
        let edges = [];
        if (corner === 'radius_lu') edges = [$('#l_edge'), $('#t_edge')];
        if (corner === 'radius_ld') edges = [$('#b_edge'), $('#l_edge')];
        if (corner === 'radius_ru') edges = [$('#t_edge'), $('#r_edge')];
        if (corner === 'radius_rd') edges = [$('#r_edge'), $('#b_edge')];

        return edges;
    }

    /**
     * Makes the field valid for entering radius cutting corner
     * @param field the input field
     * @param btn this is button for this form
     * */
    function makeValidRadiusField(field, btn) {
        let message = $(field).parent().find('.messageForRoundingCorner');
        $('input.radius').not(field).attr('disabled', false);
        $(field).removeClass('is-valid')
        message.removeClass('alert alert-danger').text('');
        btn.removeClass('btn-light');
        $(field).removeClass('is-invalid').addClass('is-valid');
        btn.attr('disabled', false);
    }

    /**
     * Makes the field invalid for entering radius cutting corner
     * @param field the input field
     * @param btn this is button for this form
     * @param textMessage this is text for message
     * */
    function makeInvalidRadiusField(field, btn, textMessage) {
        let message = $(field).parent().find('.messageForRoundingCorner');
        $('input.radius').not(field).attr('disabled', false);
        message.removeClass('alert alert-danger').text('');
        btn.removeClass('btn-light');
        $(field).removeClass('is-invalid');
        btn.attr('disabled', true);
        btn.addClass('btn-light');
        $(field).addClass('is-invalid');
        message.addClass('alert alert-danger').text(textMessage);
    }

    // validates values entering for radius cutting corners
    $('.radius').on('change', validationRoundingCorner);

    /**
     * Edits radius cutting of corners of the detail. Works on /treatment.php.
     * */

    $("#radiusSubmit").on('click', function (event) {
        let radiusLu = $('input[name=radius_lu]');
        let radiusLd = $('input[name=radius_ld]');
        let radiusRu = $('input[name=radius_ru]');
        let radiusRd = $('input[name=radius_rd]');
        event.preventDefault();
        let partL = Number($('#partL').text());
        let partW = Number($('#partW').text());
        let currentRelated = partL * partW;
        let maxRelated, minSize;
        if (radiusLu.val() !== '' || radiusLd.val() !== '' ||
            radiusRd.val() !== '' || radiusRu.val() !== '') {
            maxRelated = Number($('#maxSizeForEdgeWithRoundCorner').text());
            minSize = Number($('#minSizeForEdgeWithRoundCorner').text());
        } else {
            maxRelated = Number($('#maxSizeForEdge').text());
            minSize = Number($('#minSizeForEdge').text());
        }
        let minRelated = minSize * minSize;
        let lEdge = $('#divEdgeLeft');
        let tEdge = $('#divEdgeTop');
        let rEdge = $('#divEdgeRight');
        let bEdge = $('#divEdgeBottom');
        let url = 'modules/edit_detail.php';
        let request = "operation=1&addition_operations=1&corner=1&num_detail=" + $('input[name=currentPart]').val();
        if (radiusLu.val() !== '') {
            if (currentRelated <= maxRelated && currentRelated >= minRelated) {
                lEdge.removeClass('hidden');
                tEdge.removeClass('hidden');
            } else {
                if (!lEdge.hasClass('hidden')) lEdge.addClass('hidden');
                if (!tEdge.hasClass('hidden')) tEdge.addClass('hidden');
            }
            request += '&radius_lu=' + $('input[name=radius_lu]').val();
        } else if (currentRelated > maxRelated || currentRelated < minRelated) {
            if (radiusLd.val() === '') {
                if (!lEdge.hasClass('hidden')) lEdge.addClass('hidden');
            }
            if (radiusRu.val() === '') {
                if (!tEdge.hasClass('hidden')) tEdge.addClass('hidden');
            }
        }
        if (radiusLd.val() !== '') {
            if (currentRelated <= maxRelated && currentRelated >= minRelated) {
                lEdge.removeClass('hidden');
                bEdge.removeClass('hidden');
            } else {
                if (!lEdge.hasClass('hidden')) lEdge.addClass('hidden');
                if (!bEdge.hasClass('hidden')) bEdge.addClass('hidden');
            }
            request += '&radius_ld=' + $('input[name=radius_ld]').val();
        } else if (currentRelated > maxRelated || currentRelated < minRelated)  {
            if (radiusLu.val() === '') {
                if (!lEdge.hasClass('hidden')) lEdge.addClass('hidden');
            }
            if (radiusRd.val() === '') {
                if (!bEdge.hasClass('hidden')) bEdge.addClass('hidden');
            }
        }
        if (radiusRu.val() !== '') {
            if (currentRelated <= maxRelated && currentRelated >= minRelated) {
                rEdge.removeClass('hidden');
                tEdge.removeClass('hidden');
            } else {
                if (!rEdge.hasClass('hidden')) rEdge.addClass('hidden');
                if (!tEdge.hasClass('hidden')) tEdge.addClass('hidden');
            }
            request += '&radius_ru=' + $('input[name=radius_ru]').val();
        } else if (currentRelated > maxRelated || currentRelated < minRelated)  {
            if (radiusRd.val() === '') {
                if (!rEdge.hasClass('hidden')) rEdge.addClass('hidden');
            }
            if (radiusLu.val() === '') {
                if (!tEdge.hasClass('hidden')) tEdge.addClass('hidden');
            }
        }
        if (radiusRd.val() !== '') {
            if (currentRelated <= maxRelated && currentRelated >= minRelated) {
                rEdge.removeClass('hidden');
                bEdge.removeClass('hidden');
            } else {
                if (!rEdge.hasClass('hidden')) rEdge.addClass('hidden');
                if (!bEdge.hasClass('hidden')) bEdge.addClass('hidden');
            }
            request += '&radius_rd=' + $('input[name=radius_rd]').val();
        } else if (currentRelated > maxRelated || currentRelated < minRelated)  {
            if (radiusRu.val() === '') {
                if (!rEdge.hasClass('hidden')) rEdge.addClass('hidden');
            }
            if (radiusLd.val() === '') {
                if (!bEdge.hasClass('hidden')) bEdge.addClass('hidden');
            }
        }
        if (radiusRu.val() === '') request += '&radius_ru=-1';
        if (radiusRd.val() === '') request += '&radius_rd=-1';
        if (radiusLu.val() === '') request += '&radius_lu=-1';
        if (radiusLd.val() === '') request += '&radius_ld=-1';
        let navTab = $('#nav-treatment-sides');
        let linkTab = $('#treatmentSides');
        if (lEdge.hasClass('hidden') && rEdge.hasClass('hidden') &&
            tEdge.hasClass('hidden') && bEdge.hasClass('hidden')) {
            navTab.addClass('hidden');
            linkTab.addClass('hidden');
        } else {
            navTab.removeClass('hidden');
            linkTab.removeClass('hidden');
        }
        preloader.removeClass('d-none');
        $.post(
            url,
            request,
            function (data) {
                updateLinksImgAndPdf(JSON.parse(data));
                preloader.addClass('d-none');
            }
        );
        $('input.radius').removeClass('is-valid');
        monitorCornerEditing();
    });

    /**
     * Validates fields for scope(srez) cutting of corners
     * */
    function validationsScopeCorners() {
        let input1 = $(this);
        let input2;
        let side1, side2;
        let message = '<div class="message alert alert-danger"></div>';
        if (input1.attr('name').indexOf('lu') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=lu_y1]');
        if (input1.attr('name').indexOf('lu') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=lu_x2]');
        if (input1.attr('name').indexOf('ld') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=ld_y2]');
        if (input1.attr('name').indexOf('ld') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=ld_x1]');
        if (input1.attr('name').indexOf('ru') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=ru_y2]');
        if (input1.attr('name').indexOf('ru') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=ru_x1]');
        if (input1.attr('name').indexOf('rd') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=rd_y1]');
        if (input1.attr('name').indexOf('rd') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=rd_x2]');
        if (input1.attr('name').indexOf('x') > -1) {
            side1 = Number($('#partL').text());
            side2 = Number($('#partW').text());
        } else {
            side1 = Number($('#partW').text());
            side2 = Number($('#partL').text());
        }
        let inputs = $('#nav-srez-corners').find('input').not(input1).not(input2);
        let value1 = Number(input1.val());
        let value2 = Number(input2.val());
        let btn = $('#srezSubmit').attr('disabled', true).addClass('btn-light');
        inputs.attr('disabled', true);
        if (input1.val() !== '' && input2.val() !== '') {
            $(this).parent().find('.message-all').remove();
            if (value1 < 5 || value1 > side1) {
                input1.parent().find('.message1').remove();
                input1.addClass('is-invalid').removeClass('is-valid');
                input1.after('<div class="col-sm-12 message1 alert alert-danger">' +
                    'Значение должно быть от 5 мм до ' + side1 + ' мм' +
                    '</div>');
            } else {
                input1.removeClass('is-invalid').addClass('is-valid');
                input1.parent().find('.message1').remove();
            }
            if (value2 < 5 || value2 > side2) {
                input2.parent().find('.message2').remove();
                input2.addClass('is-invalid').removeClass('is-valid');
                input2.after('<div class="col-sm-12 message2 alert alert-danger">' +
                    'Значение должно быть от 5 мм до ' + side2 + ' мм' +
                    '</div>');
            } else {
                input2.removeClass('is-invalid').addClass('is-valid');
                input2.parent().find('.message2').remove();
            }
        } else {
            if (input2.val() === '') input2.removeClass('is-valid').addClass('is-invalid');
            if (input1.val() === '') input1.removeClass('is-valid').addClass('is-invalid');
            $(this).parent().find('.message-all').remove();
            $(this).parent().append('<div class="col-sm-12 message-all alert alert-warning">Два поля должны быть заполнены</div>')
            if (input1.val() === '' && input2.val() === '') {
                $(this).parent().find('.message-all').remove();
                input1.removeClass('is-invalid');
                input2.removeClass('is-invalid');
            }
        }
        if (!input1.hasClass('is-invalid') && !input2.hasClass('is-invalid')) {
            inputs.attr('disabled', false);
            btn.removeClass('btn-light').attr('disabled', false);
        }
    }
    $('#nav-srez-corners').find('input').on('change', validationsScopeCorners);
    /**
     * Edits slope(srez) cutting of corners of the detail. Works on /treatment.php.
     * */
    $('#srezSubmit').on('click', function (event) {
        event.preventDefault();
        let url = 'modules/edit_detail.php';
        let request = "operation=1&addition_operations=1&corner=1&num_detail=" + $('input[name=currentPart]').val();
        let leftUpInputs = [$('input[name=lu_x2]'), $('input[name=lu_y1]')];
        let leftDownInputs = [$('input[name=ld_x1]'), $('input[name=ld_y2]')];
        let rightUpInputs = [$('input[name=ru_x1]'), $('input[name=ru_y2]')];
        let rightDownInputs = [$('input[name=rd_x2]'), $('input[name=rd_y1]')];
        request += getReadyStringForRequest(leftUpInputs);
        request += getReadyStringForRequest(leftDownInputs);
        request += getReadyStringForRequest(rightUpInputs);
        request += getReadyStringForRequest(rightDownInputs);
        preloader.removeClass('d-none');
        $.post(
            url,
            request,
            function (data) {
                updateLinksImgAndPdf(JSON.parse(data));
                preloader.addClass('d-none');
            }
        );
        monitorCornerEditing();
        $('#nav-srez-corners').find('input').removeClass('is-valid');
    });

    /**
     * Validates fields for scope(srez) cutting of corners
     * */
    function validationsZarezCorners() {
        let input1 = $(this);
        let input2;
        let side1, side2;
        if (input1.attr('name').indexOf('lu') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=lu_y]');
        if (input1.attr('name').indexOf('lu') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=lu_x]');
        if (input1.attr('name').indexOf('ld') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=ld_y]');
        if (input1.attr('name').indexOf('ld') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=ld_x]');
        if (input1.attr('name').indexOf('ru') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=ru_y]');
        if (input1.attr('name').indexOf('ru') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=ru_x]');
        if (input1.attr('name').indexOf('rd') > -1 && input1.attr('name').indexOf('x') > -1) input2 = $('input[name=rd_y]');
        if (input1.attr('name').indexOf('rd') > -1 && input1.attr('name').indexOf('y') > -1) input2 = $('input[name=rd_x]');
        if (input1.attr('name').indexOf('x') > -1) {
            side1 = Number($('#partL').text());
            side2 = Number($('#partW').text());
        } else {
            side1 = Number($('#partW').text());
            side2 = Number($('#partL').text());
        }
        let inputs = $('#nav-zarez-corners').find('input').not(input1).not(input2);
        let value1 = Number(input1.val());
        let value2 = Number(input2.val());
        let btn = $('#zarezSubmit').attr('disabled', true).addClass('btn-light');
        inputs.attr('disabled', true);
        if (input1.val() !== '' && input2.val() !== '') {
            $(this).parent().find('.message-all').remove();
            if (value1 < 10 || value1 > side1) {
                input1.parent().find('.message1').remove();
                input1.addClass('is-invalid').removeClass('is-valid');
                input1.after('<div class="col-sm-12 message1 alert alert-danger">' +
                    'Значение должно быть от 10 мм до ' + side1 + ' мм' +
                    '</div>');
            } else {
                input1.removeClass('is-invalid').addClass('is-valid');
                input1.parent().find('.message1').remove();
            }
            if (value2 < 10 || value2 > side2) {
                input2.parent().find('.message2').remove();
                input2.addClass('is-invalid').removeClass('is-valid');
                input2.after('<div class="col-sm-12 message2 alert alert-danger">' +
                    'Значение должно быть от 10 мм до ' + side2 + ' мм' +
                    '</div>');
            } else {
                input2.removeClass('is-invalid').addClass('is-valid');
                input2.parent().find('.message2').remove();
            }
        } else {
            if (input2.val() === '') input2.removeClass('is-valid').addClass('is-invalid');
            if (input1.val() === '') input1.removeClass('is-valid').addClass('is-invalid');
            $(this).parent().find('.message-all').remove();
            $(this).parent().append('<div class="col-sm-12 message-all alert alert-warning">Два поля должны быть заполнены</div>')
            if (input1.val() === '' && input2.val() === '') {
                $(this).parent().find('.message-all').remove();
                input1.removeClass('is-invalid');
                input2.removeClass('is-invalid');
            }
        }
        if (!input1.hasClass('is-invalid') && !input2.hasClass('is-invalid')) {
            inputs.attr('disabled', false);
            btn.removeClass('btn-light').attr('disabled', false);
        }
    }
    $('#nav-zarez-corners').find('input').on('change', validationsZarezCorners);
    /**
     * Edits (zarez) cutting of corners of the detail. Works on /treatment.php.
     * */
    $('#zarezSubmit').on('click', function (event) {
        event.preventDefault();
        let url = 'modules/edit_detail.php';
        let request = "operation=1&addition_operations=1&corner=1&num_detail=" + $('input[name=currentPart]').val();
        let leftUpInputs = [$('input[name=lu_x]'), $('input[name=lu_y]')];
        let leftDownInputs = [$('input[name=ld_x]'), $('input[name=ld_y]')];
        let rightUpInputs = [$('input[name=ru_x]'), $('input[name=ru_y]')];
        let rightDownInputs = [$('input[name=rd_x]'), $('input[name=rd_y]')];
        request += getReadyStringForRequest(leftUpInputs);
        request += getReadyStringForRequest(leftDownInputs);
        request += getReadyStringForRequest(rightUpInputs);
        request += getReadyStringForRequest(rightDownInputs);
        preloader.removeClass('d-none');
        $.post(
            url,
            request,
            function (data) {

                updateLinksImgAndPdf(JSON.parse(data));
                preloader.addClass('d-none');
            }
        );
        monitorCornerEditing();
        $('#nav-zarez-corners').find('input').removeClass('is-valid');
    });

    /**
     * Makes bore holes in the edited part. Works on /treatment.php.
     * */
    $('#boreSubmit').on('click', function (event) {
        event.preventDefault();
        let url = 'modules/edit_detail.php';
        let request = "operation=1&addition_operations=1&face=1&num_detail=" + $('input[name=currentPart]').val();
        let x = $('input[name=bore_x]');
        let y = $('input[name=bore_y]');
        let d = $('select[name=bore_d]');
        let values = [x, y, d];
        let zenk = $('input[name=zenk]');
        let check = validationForBore(x, y, d);
        request += getReadyStringForRequest(values);
        request += (zenk.is(':checked')) ? "&zenk=1" : '';
        if (!check) {
            preloader.removeClass('d-none');
            $.post(
                url,
                request,
                function (data) {
                    y.parent().parent().find('.alert').remove();
                    $('.bore-list').find('span.hidden').remove();
                    $('#bore-table').remove();
                    $('.bore-list').append(data);
                    clearAllInputs(values);
                    zenk.prop('checked', false);
                    x.removeClass('is-valid');
                    y.removeClass('is-valid');
                    let json = JSON.parse($('.bore-list').find('span.hidden').text());
                    updateLinksImgAndPdf(json);
                    preloader.addClass('d-none');
                }
            );
        }

    });

    /**
     * Makes oval holes in the edited part. Works on /treatment.php.
     * */
    $('#ovalSubmit').on('click', function (event) {
        event.preventDefault();
        let x1 = $('input[name=oval_x1]');
        let y1 = $('input[name=oval_y1]');
        let x2 = $('input[name=oval_x2]');
        let y2 = $('input[name=oval_y2]');
        let url = 'modules/edit_detail.php';
        let request = "operation=1&addition_operations=1&face=1&num_detail=" + $('input[name=currentPart]').val();
        let values = [x1, y1, x2, y2];
        request += getReadyStringForRequest(values);
        if (!validationForOval(x1, y1, x2, y2)) {
            preloader.removeClass('d-none');
            $.post(
                url,
                request,
                function (data) {
                    $('#oval-table').remove();
                    $('.oval-list').find('span.hidden').remove();
                    $('.oval-list').append(data);
                    x1.removeClass('is-valid');
                    y1.removeClass('is-valid');
                    x2.removeClass('is-valid');
                    y2.removeClass('is-valid');
                    let json = JSON.parse($('.oval-list').find('span.hidden').text());
                    updateLinksImgAndPdf(json);
                    preloader.addClass('d-none');
                }
            );
            clearAllInputs(values);
        }
    });

    /**
     * Uploads clients photos on server
     * */
    let loadClientImg = function (event) {
        let loaderImages = $(event.target).parents('.loaderImages');
        let select = loaderImages.parent().find('.select-decoration-side');
        let divMess = loaderImages.parent().find('.result');
        let divImg = loaderImages.prev();
        if (this.files && this.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                divImg.removeClass('hidden');
                divImg.find('img').attr('src', e.target.result);
            }
            let formData = new FormData();
            formData.append('decoration_option', select.val());
            formData.append('num_detail', $('input[name=currentPart]').val());
            $.each(this.files, function (k, v) {
                formData.append(k, v);
            });
            $.ajax({
                type: 'POST',
                url: 'services/handler_upload_img.php',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    let response = JSON.parse(data);
                    printMessage(divMess, response[0]);
                    if (response[1] !== undefined) updateLinksImgAndPdf(response[1]);
                    $("#frontLoadImg").addClass('hidden');
                    loaderImages.remove();
                },
                error: function (data) {
                    console.error(data);
                }
            });
            reader.readAsDataURL(this.files[0]);
        }
    }

    /**
     * Sends data for the surface treatment
     * */
    $('#surfaceTreatmentSubmit').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        let url = 'modules/edit_detail.php';
        let request = "operation=1&addition_operations=1&decoration_options=1&num_detail=" + $('input[name=currentPart]').val();
        request += ($('input[name=mat]').prop('checked')) ? '&mat=1' : '';
        request += ($('input[name=gfh]').prop('checked')) ? '&gfh=1' : '';
        request += ($('input[name=film]').prop('checked')) ? '&film=1' : '';
        request += ($('input[name=film_art]').prop('checked')) ? '&film_art=1' : '';
        request += ($('input[name=zakalka]').prop('checked')) ? '&zakalka=1' : '';
        let oracal = $('select[name=oracal]');
        request += (oracal.val() === '-Нет-') ? '' : "&oracal=" + oracal.val();
        request += "&simple_part=true";
        preloader.removeClass('d-none');
        $.post(
            url,
            request,
            function (data) {
                updateLinksImgAndPdf(JSON.parse(data));
                preloader.addClass('d-none');
            }
        );
    });


    function loadingImage() {
        $(this).parent().find('.loaderImages').remove();
        let selects = $('.select-decoration-side');
        for (let select of selects) {
            if ($(select).attr('name') !== $(this).attr('name')) {
                $(select).parent().find('.loaderImages').remove();
            }
        }
        let $this = $(this);
        let selected = this.value;
        let loaderFields = getGeneratedLoadingImage();
        $(this).parent().append(loaderFields);
        let path;
        let pathImg = '';
        if (selected === "-Нет-") {
            $(this).parent().find('.loaderImages').remove();
            $(this).parent().find('.result').removeClass('alert').text('');
            $(this).parent().find('.loadImg').addClass('hidden');
        } else {
            $.post(
                'services/file_manager/show_image.php',
                {
                    decoration_options: selected,
                },
                function (data) {
                    if (data === 'error_path') {
                        console.error(data);
                    } else {
                        $('#serves_library').html(data);
                        function sendCurrentDir() {
                            const dir = $(this).data('current_dir');
                            $.post(
                                'services/file_manager/show_image.php',
                                {
                                    current_dir: dir,
                                    decoration_options: selected,
                                },
                                function (data) {
                                    $('#serves_library').html(data);
                                    $('.image-folder').on('click', sendCurrentDir);
                                    $('.img-from-library').on('click', function (event) {
                                        event.preventDefault();
                                        event.stopPropagation();
                                        $('.card').css({'border': 'none'});
                                        $(this).parents('.card').css({'border': '5px solid #1b1b1c6b'});
                                        path = $(this).attr('src');
                                        pathImg = path.substring(path.indexOf('images'), path.length);
                                    });
                                }
                            );
                        }
                        $('.image-folder').on('click', sendCurrentDir);
                    }
                }
            );

            $('#image').change(loadClientImg);
            $('#libraryOpen').on('click', function (event) {
                event.preventDefault();
                $('#photoGallery').modal('show');
                $('#saveImage').on('click', function (event) {
                    event.preventDefault();
                    let divLoadImg = $this.parent().find('.loadImg');
                    let divMess = $this.parent().find('.result');
                    printMessage(divMess, 'success');
                    divLoadImg.removeClass('hidden');
                    divLoadImg.find('img').attr('src', pathImg);
                    $('#photoGallery').modal('hide');
                    let request = 'num_detail=' + $('input[name=currentPart]').val() +
                        '&decoration_option=' + selected +
                        '&image=' + pathImg + "&simple_part=true";
                    $.post(
                        'modules/add_image.php',
                        request,
                        function (data) {
                            updateLinksImgAndPdf(JSON.parse(data));
                        });
                    $this.parent().find('.loaderImages').remove();
                    $('#saveImage').off('click');
                });
            });
        }
    }



    /**
     * Shows new form's fields for loading an image to decorate a side
     * */
    $("#decoration_options").on('change', loadingImage);

    /**
     * Shows new form's fields for loading an image to decorate a back side
     * */
    $('#send_file_back').on('change', loadingImage);

});
/**
 * Removes the detail
 * */
function removeDetail(currentKey) {
    $('#start_checks_lodaer_containers').removeClass('d-none');
    $.post(
        'modules/remove_detail.php',
        {
            num_detail: currentKey,
        },
        function (data) {
            setRowsInTable(data);
            $('#start_checks_lodaer_containers').addClass('d-none');
        }
    );
}

/**
 * Removes the bore
 * */
function removeBore($this, currentBore) {
    let x = $('input[name=bore_x]');
    let y = $('input[name=bore_y]');
    let d = $('select[name=bore_d]');

    setTimeout(function () { // TODO: must find another way
        $('#boreSubmit').attr('disabled', false).removeClass('hidden');
        let editSubmit = $('#boreEdit').attr('disabled', true);
        if (!editSubmit.hasClass('hidden')) editSubmit.addClass('hidden');
        $('#titleBore').text("Сверление отверстий");
        clearAllInputs([x, y, d]);
        $('input[name=zenk]').prop('checked', false);
    }, 10);
    $('#start_checks_lodaer_containers').removeClass('d-none');
    $.post(
        'modules/remove_hole.php',
        {
            num_detail: $('input[name=currentPart]').val(),
            num_bore: currentBore,
            face: 1,
        },
        function (data) {
            x.parent().parent().find('.alert').remove();
            $('.bore-list').find('span.hidden').remove();
            $('#bore-table').remove();
            $('.bore-list').append(data);
            let json = JSON.parse($('.bore-list').find('span.hidden').text());
            $('#imgCurrentPart').attr('src', json.path[1]);
            $('#pdfCurrentPart').attr('href', json.path[0]);
            $('#start_checks_lodaer_containers').addClass('d-none');
        });
    $($this).off('click');
}



/**
 * Validates fields for making the bore
 * @param x the x coordinate.
 * @param y the y coordinate.
 * @param d the diameter of the bore.
 * @param oldX the old x coordinate.
 * @param oldY the old y coordinate.
 * */
function validationForBore(x, y, d, oldX= -1, oldY = -1) {
    x.parent().parent().find('.alert').remove();
    let check = 0;
    let arrX = [];
    let arrY = [];
    let contains = false;
    let table = $('#bore-table');
    for (let tdX of $('td.boreX', table)) {
        arrX.push(Number(tdX.innerText));
    }
    for (let tdY of $('td.boreY', table)) {
        arrY.push(Number(tdY.innerText));
    }
    for (let i = 0; i < arrX.length; i++) {
        if (arrX[i] === oldX && arrY[i] === oldY) continue;
        if (arrX[i] === Number(x.val()) && arrY[i] === Number(y.val())) {
            contains = true;
            break;
        }
    }
    if (contains) {
        let messageY = $('<div class="alert alert-danger for-contains"></div>');
        x.addClass('is-invalid');
        y.addClass('is-invalid');
        y.parent().after(messageY.text('Отверстие с такими координатами уже есть'));
        check++;
    } else {
        y.parent().parent().find('.for-contains').remove();
        x.removeClass('is-invalid');
        x.addClass('is-valid');
        y.removeClass('is-invalid');
        y.addClass('is-valid');
        contains = false;
    }

    if (Number(x.val()) < 10 || Number(x.val()) > Number($('#partL').text()) - 10) {
        let messageX = $('<div class="alert alert-danger for-x"></div>');
        x.addClass('is-invalid');
        x.parent().after(messageX.text('Координата по X должна быть в пределах длины детали, с отступом 10 мм от края'));
        check++;
    } else {
        x.parent().parent().find('.for-x').remove();
        if (!contains) {
            x.removeClass('is-invalid');
            x.addClass('is-valid');
        }
    }
    if (Number(y.val()) < 10 || Number(y.val()) > Number($('#partW').text()) - 10) {
        y.addClass('is-invalid');
        y.parent().after('<div class="alert alert-danger for-y">Координата по Y должна быть в пределах ширины детали, с отступом 10 мм от края</div>');
        check++;
    } else {
        y.parent().parent().find('.for-y').remove();
        if (!contains) {
            y.removeClass('is-invalid');
            y.addClass('is-valid');
        }
    }
    if (y.val() === '' || x.val() === '' || d.val() === '') {
        d.parent().after('<div class="alert alert-danger for-empty-fields">все поля должны быть заполнены</div>');
        check++;
    } else {
        d.parent().parent().find('for-empty-fields').remove();
    }
    return check;
}

/**
 * Edits the bore
 * */
function editBore($this, currentBore, x, y, d, z) {
    $('.bore-item').css({'border': 'none'});
    $($this).css({'border': 'solid 2px #17a2b8'});
    let boreSubmit = $('#boreSubmit');
    let editSubmit = $('#boreEdit');
    let boreX = $('input[name=bore_x]');
    let boreY = $('input[name=bore_y]');
    let boreD = $('select[name=bore_d]');
    let zenk = ($('input[name=zenk]'));
    let inputs = [boreX, boreY, boreD];
    let titleBore = $('#titleBore');
    let titleBoreText = titleBore.text();
    titleBore.text('Изменить отверстие №' + currentBore);
    boreSubmit.addClass('hidden').attr('disable', true);
    editSubmit.removeClass('hidden').attr('disabled', false);
    boreX.val(x);
    boreY.val(y);
    boreD.val(d);
    let isChecked = (Number(z) === 1);
    zenk.prop('checked', isChecked);
    editSubmit.on('click', function (event) {
        event.preventDefault();
        let request = "face=1&num_detail=" + $('input[name=currentPart]').val() + "&num_bore=" + currentBore;
        request += getReadyStringForRequest(inputs);
        request += (zenk.prop('checked')) ? "&zenk=1" : "";
        let check = validationForBore(boreX, boreY, boreD, x, y);
        if (!check) {
            $('#start_checks_lodaer_containers').removeClass('d-none');
            $.post(
                'modules/edit_hole.php',
                request,
                function (data) {
                    $('#bore-table').remove();
                    $('.bore-list').find('span.hidden').remove();
                    $('.bore-list').append(data);
                    boreX.removeClass('is-valid');
                    boreY.removeClass('is-valid');
                    let json = JSON.parse($('.bore-list').find('span.hidden').text());
                    $('#imgCurrentPart').attr('src', json.path[1]);
                    $('#pdfCurrentPart').attr('href', json.path[0]);
                    $('#start_checks_lodaer_containers').addClass('d-none');
                });
            titleBore.text(titleBoreText);
            boreSubmit.removeClass('hidden').attr('disabled', false);
            editSubmit.addClass('hidden').attr('disabled', true);
            $($this).css({'border': 'none'});
            clearAllInputs(inputs);
            zenk.prop('checked', false);
            editSubmit.off('click');
            boreD.parent().parent().find('.alert').remove();
        }
    });
    $($this).off('click');
}

/**
 * Removes the oval hole
 * */
function removeOval($this, currentOval) {
    setTimeout(function () { // TODO: must find another way
        $('#titleOval').text("Сверление отверстий под резетку");
        $('#ovalSubmit').removeClass('hidden').attr('disabled', false);
        $('#ovalEdit').addClass('hidden').attr('disabled', true);
        clearAllInputs([$('input[name=oval_x1]'), $('input[name=oval_y1]'), $('input[name=oval_x2]'), $('input[name=oval_y2]')]);
    }, 10);
    $('#start_checks_lodaer_containers').removeClass('d-none');
    $.post(
        'modules/remove_hole.php',
        {
            num_detail: $('input[name=currentPart]').val(),
            face: 1,
            num_oval: currentOval,
        },
        function (data) {
            $('#oval-table').remove();
            $('.oval-list').find('span.hidden').remove();
            $('.oval-list').append(data);
            let json = JSON.parse($('.oval-list').find('span.hidden').text());
            $('#imgCurrentPart').attr('src', json.path[1]);
            $('#pdfCurrentPart').attr('href', json.path[0]);
            $('#start_checks_lodaer_containers').addClass('d-none');
        }
    );
    $($this).off('click');
}

/**
 * Validates fields for the oval bore
 * */
function validationForOval(x1, y1, x2, y2, oldX1 = -1, oldY1 = -1, oldX2 = -1, oldY2 = -1) {
    x1.parent().parent().find('.alert').remove();
    let check = 0;
    let arrX1 = [];
    let arrY1 = [];
    let arrX2 = [];
    let arrY2 = [];
    let contains = false;
    let table = ('#oval-table');
    for (let tdX1 of $('td.ovalX1', table)) {
        arrX1.push(Number(tdX1.innerText));
    }
    for (let tdY1 of $('td.ovalY1', table)) {
        arrY1.push(Number(tdY1.innerText));
    }
    for (let tdX2 of $('td.ovalX2', table)) {
        arrX2.push(Number(tdX2.innerText));
    }
    for (let tdY2 of $('td.ovalY2', table)) {
        arrY2.push(Number(tdY2.innerText));
    }
    for (let i = 0; i < arrX1.length; i++) {
        if (arrX1[i] === oldX1 && arrY1[i] === oldY1 && arrX2[i] === oldX2 && arrY2[i] === oldY2) continue;
        if ((arrX1[i] === Number(x1.val()) && arrY1[i] === Number(y1.val())) ||
            (arrX2[i] === Number(x2.val()) && arrY2[i] === Number(y2.val()))) {
            contains = true;
            break;
        }
        // Math.abs(arrX1[i] - x1.val()) > 59 && Math.abs(arrY1[i] - y1.val()) > 59  &&
        // Math.abs(arrX2[i] - x2.val()) > 59 && Math.abs(arrY2[i] - y2.val()) > 59
    }

    if (contains) {
        let message = $('<div class="alert alert-danger for-contains"></div>');
        x1.addClass('is-invalid');
        y1.addClass('is-invalid');
        x2.addClass('is-invalid');
        y2.addClass('is-invalid');
        y2.parent().after(message.text('Отверстие с такими координатами уже есть'));
        check++;
    } else {
        y2.parent().parent().find('.for-contains').remove();
        x1.removeClass('is-invalid').addClass('is-valid');
        x2.removeClass('is-invalid').addClass('is-valid');
        y1.removeClass('is-invalid').addClass('is-valid');
        y2.removeClass('is-invalid').addClass('is-valid');
        contains = false;
    }
    let allowLength = Number($('#partL').text()) - 10;
    if (Number(x1.val()) < 10 || Number(x1.val()) > allowLength) {
        let messageX = $('<div class="alert alert-danger for-x1"></div>');
        x1.addClass('is-invalid');
        x1.parent().after(messageX.text('Координата по X1 должна быть в пределах длины детали, ' +
            'с отступом 10 мм от края'));
        check++;
    } else {
        x1.parent().parent().find('.for-x1').remove();
        if (!contains) {
            x1.removeClass('is-invalid');
            x1.addClass('is-valid');
        }
    }
    if (Number(x2.val()) < 10 || Number(x2.val()) > allowLength) {
        let messageX = $('<div class="alert alert-danger for-x2"></div>');
        x2.addClass('is-invalid');
        x2.parent().after(messageX.text('Координата по X2 должна быть в пределах длины детали, ' +
            ' с отступом 10 мм от края'));
        check++;
    } else {
        x2.parent().parent().find('.for-x2').remove();
        if (!contains) {
            x2.removeClass('is-invalid');
            x2.addClass('is-valid');
        }
    }
    let allowWidth = Number($('#partW').text()) - 10;
    if (Number(y1.val()) < 10 || Number(y1.val()) > allowWidth) {
        y1.addClass('is-invalid');
        y1.parent().after('<div class="alert alert-danger for-y1">Координата по Y1 должна быть ' +
            'в пределах ширины детали, с отступом 10 мм от края</div>');
        check++;
    } else {
        y1.parent().parent().find('.for-y1').remove();
        if (!contains) {
            y1.removeClass('is-invalid');
            y1.addClass('is-valid');
        }
    }
    if (Number(y2.val()) < 10 || Number(y2.val()) > allowWidth) {
        y2.addClass('is-invalid');
        y2.parent().after('<div class="alert alert-danger for-y1">Координата по Y2 должна быть ' +
            'в пределах ширины детали, с отступом 10 мм от края</div>');
        check++;
    } else {
        y2.parent().parent().find('.for-y1').remove();
        if (!contains) {
            y2.removeClass('is-invalid');
            y2.addClass('is-valid');
        }
    }
    if (y1.val() === '' || x1.val() === '' || x2.val() === '' || y2.val() === '') {
        y2.parent().after('<div class="alert alert-danger for-empty-fields">все поля должны быть заполнены</div>');
        check++;
    } else {
        y2.parent().parent().find('for-empty-fields').remove();
    }
    console.log((x1.val() !== x2.val() && (y2.val() - y1.val()) < 59), (y1.val() !== y2.val() && (x2.val() - x1.val()) < 59))
    if ((x1.val() === x2.val() && (y2.val() - y1.val()) > 59) || (y1.val() === y2.val() && (x2.val() - x1.val()) > 59)) {
        y2.parent().parent().find('for-empty-fields').remove();
        x1.removeClass('is-invalid');
        x2.removeClass('is-invalid');
        y1.removeClass('is-invalid');
        y2.removeClass('is-invalid');
    } else {
        y2.parent().after('<div class="alert alert-danger for-empty-fields">Отверстия под розетку должны быть вертикальные или горизонтальные на расстоянии 60 мм друг от друга</div>');
        x1.addClass('is-invalid');
        x2.addClass('is-invalid');
        y1.addClass('is-invalid');
        y2.addClass('is-invalid');
        check++;
    }

    return check;
}

/**
 * Edits the bore
 * */
function editOval($this, currentOval, x1, y1, x2 ,y2) {
    $('.oval-item').css({'border': 'none'});
    $($this).css({'border': 'solid 2px #17a2b8'});
    let ovalSubmit = $('#ovalSubmit');
    let ovalEdit = $('#ovalEdit');
    let ovalX1 = $('input[name=oval_x1]');
    let ovalY1 = $('input[name=oval_y1]');
    let ovalX2 = $('input[name=oval_x2]');
    let ovalY2 = $('input[name=oval_y2]');
    let inputs = [ovalX1, ovalY1, ovalX2, ovalY2];
    let titleOval = $('#titleOval');
    let textTitleOval = titleOval.text();
    titleOval.text("Изменить отверстие под резетку №" + currentOval);
    ovalX1.val(x1);
    ovalY1.val(y1);
    ovalX2.val(x2);
    ovalY2.val(y2);
    ovalSubmit.addClass('hidden').attr('disabled', true);
    ovalEdit.removeClass('hidden').attr('disabled', false);
    ovalEdit.on('click', function (event) {
        event.preventDefault();
        let request = "face=1&num_detail=" + $('input[name=currentPart]').val() + "&num_oval=" + currentOval;
        request += getReadyStringForRequest(inputs);
        if (!validationForOval(ovalX1, ovalY1, ovalX2, ovalY2, x1, y1, x2, y2)) {
            $('#start_checks_lodaer_containers').removeClass('d-none');
            $.post(
                'modules/edit_hole.php',
                request,
                function (data) {
                    $('.oval-list').find('span.hidden').remove();
                    $('#oval-table').remove();
                    $('.oval-list').append(data);
                    ovalX1.removeClass('is-valid');
                    ovalY1.removeClass('is-valid');
                    ovalX2.removeClass('is-valid');
                    ovalY2.removeClass('is-valid');
                    let json = JSON.parse($('.oval-list').find('span.hidden').text());
                    $('#imgCurrentPart').attr('src', json.path[1]);
                    $('#pdfCurrentPart').attr('href', json.path[0]);
                    $('#start_checks_lodaer_containers').addClass('d-none');
                }
            );
            titleOval.text(textTitleOval);
            ovalSubmit.removeClass('hidden').attr('disabled', false);
            ovalEdit.addClass('hidden').attr('disabled', true);
            clearAllInputs(inputs);
            $($this).css({'border': 'none'});
            ovalEdit.off('click');

        }
    });
    $($this).off('click');
}

/**
 * Edits the detail
 * */
function changingCreatedPart($this, val1, val2, currentKey) {
    let inputs = $($this).parents('tr').find('.input-current-part');
    let request = 'num_detail=' + currentKey;
    request += ($($this).closest('tr').find('th').eq(1).find('a').length === 0) ? '&simple_part=true' : "";
    $($this).closest('tr').find('th').eq(1).html('<img src="../RS/images/download.gif" width="50px" height="50px">')
    let thisName = $($this).attr('name');
    for (let input of inputs) {
        let name = $(input).attr('name');
        let val = $(input).val();
        if (name !== thisName) request += '&' + name + '=' + val;
    }
    let parent = $($this).parent();
    let check;
    let compareVal;
    if (thisName === 'width') {
        compareVal = Number($('#matWidth').text());
    } else if (thisName === 'length') {
        compareVal = Number($('#matLength').text());
    } else if (thisName === 'quantity') compareVal = 1000;
    if (Number($this.value) <= 0 || Number($this.value) > compareVal) {
        check = false;
        parent.find('.alert').remove();
        $($this).addClass('is-invalid');
        parent.append('<div class="alert alert-danger">не верное значение</div>')
    } else {
        check = true;
        parent.find('.alert').remove();
        $($this).addClass('is-valid').removeClass('is-invalid');
        request += "&" + thisName + '=' + $this.value;
    }
    if (check) {
        //$('#start_checks_lodaer_containers').removeClass('d-none');
        $.post(
            'modules/edit_detail.php',
            request,
            function (data) {
                setRowsInTable(data);
                $($this).removeClass('is-valid');
                //$('#start_checks_lodaer_containers').addClass('d-none');
            }
        );
    }
}

/**
 * It tracks changes to handle the left edge of the part
 * */
function edgeHandle($this, currentKey) {
    let url = 'modules/edit_detail.php';
    let request = "simple_part=true&edges=1&" + $this.name + "=" + $this.value + "&num_detail=" + currentKey;
    let index = $this.name.substr(0, 1);
    let $thisTr = $($this).parent().parent().parent();
    let maxFacet = $('#maxFacet').val();
    if ($this.value === 'facet') {
        let modal = $('#forFacetHandleEdge');
        let btn = $('#btnHandelFacetEdge');
        let title = $('#titleFacetHandelEdge');
        let edgeName = getEdgeName($this.name);
        let message = $("#messageHandleFace");
        title.text('Введите ширину фацета для ' + edgeName + ' стороны детали №' + currentKey + '. ширина должна быть не более ' + maxFacet + ' мм');
        modal.modal('show');
        btn.on('click', function () {
            let inputLength = $('#facetHandelLength');
            let length = inputLength.val();
            if ((Number(length) > Number(maxFacet)) || (Number(length) < 0)) {
                inputLength.removeClass('is-invalid');
                message.addClass('alert alert-danger').text('Ширина фацета должна быть не более ' + maxFacet + ' мм');
                inputLength.addClass('is-invalid');
            } else {
                message.removeClass('alert alert-danger').text('');
                inputLength.removeClass('is-invalid');
                request += "&" + index + "_length=" + length;
                $('#start_checks_lodaer_containers').removeClass('d-none');
                $.post(url, request, function (data) {
                    setRowsInTable(data);
                    preloader.addClass('d-none');
                });
                modal.modal('hide');
                btn.off('click');
            }
            inputLength.val("");
        });
    } else {
        $('#start_checks_lodaer_containers').removeClass('d-none');
        $.post(url, request, function (data) {
            setRowsInTable(data);
            $('#start_checks_lodaer_containers').addClass('d-none');
        });
    }
}

/***********************
 * helpers functions
 * **********************/

/**
 * Prints a message about the result of the ajax request
 * @param destination it is ID of element to print a message
 * @param msg this is a message from the server about the loading file
 * */
function printMessage(destination, msg) {
    $(destination).removeClass('.alert');
    if (msg === 'success') {
        $(destination).addClass('alert alert-success').text('Файл успешно загружен.');
    }
    if (msg === 'error') {
        $(destination).addClass('alert alert-danger').text('Произошла ошибка при загрузке файла.');
    }
    if (msg === 'error_size') {
        $(destination).addClass('alert alert-danger').text('Размер файла не должен привышать 500 Кб.');
    }
    if (msg === 'error_format') {
        $(destination).addClass('alert alert-danger').text('Файл должен быть .png или .jpeg.');
    }
    if (msg === 'error_session') {
        $(destination).addClass('alert alert-danger').text('error session');
    }

}

/**
 * Returns a name of the edge by the name's attribute.
 * */
function getEdgeName(attr) {
    let name = '';
    switch (attr) {
        case 'l_edge':
            name = 'левой';
            break;
        case 'r_edge':
            name = 'правой';
            break;
        case 't_edge':
            name = 'верхней';
            break;
        case 'b_edge':
            name = 'нижней';
            break;
    }
    return name;
}

/**
 * Clears inputs of the modal window for creating/edition a part.
 * */
function clearAllInputs(inputs) {
    for (let input of inputs) {
        input.val("");
    }
}

/**
 * sets new rows in the table for showing parts.
 * */
function setRowsInTable(data) {
    let divParts = $('#parts');
    divParts.children().remove();
    divParts.append(data);
    let jsonOrder = divParts.find('#order_st').text();
    let project = divParts.find('#project_arr').text();
    $('input[name=glass_project]').val(project);
    $('input[name=order_st]').val('');
    $('input[name=order_st]').val(jsonOrder);
    setDrawOfParts();
    if ($('#tbodyParts').children().length > 0) {
        $('#formSaveOrder').removeClass('hidden');
        $('#sendOrder').removeClass('hidden');
    } else {
        $('#formSaveOrder').addClass('hidden');
        $('#sendOrder').addClass('hidden');
    }
}

/**
 * inserts a drawing of the part if there is no generated drawing from the server
 * */
function setDrawOfParts() {
    $('#tbodyParts tr').each(function () {
        if ($(this).find('th').eq(1).find('a').length === 0) {
            let selector = $(this).find('th').eq(1).attr('id');
            let partL = $(this).find('td.length>input').val();
            let partW = $(this).find('td.width>input').val();
            drawParts("#"+selector, Number(partL), Number(partW));
        }
    });
}

/**
 * Generates additional fields for loading an image
 * @param title, this is a string value to get a name for this fields.
 * @param key, this is a string value to get a name for attributes id, for end name.
 * */
function getGeneratedLoadingImage() {
    return "<div class=\"row mt-3 loaderImages\">\n" +
        "      <label class=\"col-sm-8 col-form-label\" for='libraryOpen'>Выбрать из библиотеки</label>\n" +
        "      <div class=\" col-sm-4 mt-2\">\n" +
        "          <button id='libraryOpen' class=\"btn-sm btn-warning\">Выбрать</button>\n" +
        "      </div>\n" +
        "      <label class=\"col-sm-8 col-form-label\" for='image'>Загрузить свое изображение</label>\n" +
        "      <div class=\" col-sm-4 mt-2\" id='uploadImage'>\n" +
        "          <label class=\"btn btn-sm btn-success btn-file\">Загрузить\n" +
        "              <input type=\"file\" class=\"d-none\" id='image'>\n" +
        "          </label>\n" +
        "      </div>\n" +
        "</div>";
}

/**
 * Checks transferred inputs for emptiness and returns a ready string for a request
 * @param an array with the inputs
 * */
function getReadyStringForRequest(inputs) {
    let result = '';
    for (let input of inputs) {
        if (input.val() !== '') {
            result += "&" + input.attr('name') + "=" + input.val();
        } else {
            result = '&' + input.attr('name') + '=-1';
        }
    }
    return result;
}
