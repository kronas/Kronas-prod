function drawParts(selector, partL, partW) {
    /**==================
     * Builds the part
     * ==================*/
    // let partW = Number($('#partW').text());
    // let partL = Number($('#partL').text());
    let heightSVG = (selector === "#draw") ? '600' : '80';
    let widthSVG = (selector === "#draw") ? '100%' : '80';
    let viewHeight = partW + 216;
    let viewWidth = partL + 216;
    let topEdge = ['l', partL, 0];
    let rightEdge = ['l', 0, partW];
    let bottomEdge = ['l', -partL, 0];
    let leftEdge = ['l', 0, -partW];
    let part = [
        ['M', 0, 0],//0
        topEdge,    //1
        ['l', 0, 0],//2 ru
        rightEdge,  //3
        ['l', 0, 0],//4 rd
        bottomEdge, //5
        ['l', 0, 0],//6 ld
        leftEdge,   //7
        ['l', 0, 0],//8 lu
    ];

    let draw = SVG().addTo(selector).size(widthSVG, heightSVG).viewbox(-108, -108, viewWidth, viewHeight);
    let sideTop = draw.rect(partL, 10).fill('none').stroke('#000').addClass('side-top');
    sideTop.x(0).y(-20);
    let sideRight = draw.rect(10, partW).fill('none').stroke('#000').addClass('side-right');
    sideRight.move(partL + 10, 0);
    let sideBottom = draw.rect(partL, 10).fill('none').stroke('#000').addClass('side-bottom');
    sideBottom.move(0, partW + 10);
    let sideLeft = draw.rect(10, partW).fill('none').stroke('#000').addClass('side-left');
    sideLeft.move(-20, 0);
    let currentPart = draw.path(part).attr({stroke: '#000', fill: 'none'});
    let groupCurrP = draw.group().addClass('current-part');
    groupCurrP.add(currentPart);
    let lineLength = draw.line(0, 0, partL, 0).stroke({color: '#000', with: 1 });
    lineLength.move(0, -30);
    let lineWith = draw.line(0, 0, 0, partW).stroke({color: "#000", with: 1});
    lineWith.move(partL + 30, 0);
    let pointerLeft = draw.path('M0,0 L20,-4 16,0 20,4 z M0,-10 L0,10 M0,0 L27,0').move(0, -40);
    let pointerRight = draw.path('M0,0 L20,-4 16,0 20,4 z M0,-10 L0,10 M0,0 L27,0').move(partL - 30, -40).transform({rotate: 180});
    let pointerUp = draw.path('M0,0 L20,-4 16,0 20,4 z M0,-10 L0,10 M0,0 L27,0').move(partL + 16, 4).transform({rotate: 90});
    let pointerDown = draw.path('M0,0 L20,-4 16,0 20,4 z M0,-10 L0,10 M0,0 L27,0').move(partL + 16, partW - 26).transform({rotate: -90});
    let textWidth = draw.text(partW.toString())
        textWidth.move(partL + 60, partW/2 - textWidth.length()).transform({rotate: 90}).font({
        family: 'Noto Sans',
        weight: 700,
            size: 40,
    });
    let textLength = draw.text(partL.toString());
    textLength.move(partL/2 - textLength.length(), -90).font({
        family: 'Noto Sans',
        weight: 700,
        size: 40,
    });
    let groupLineLength = draw.group().addClass('svg-part-length');
    let groupLineWidth= draw.group().addClass('svg-part-width');
    groupLineLength.add(lineLength).add(pointerLeft).add(pointerRight).add(textLength);
    groupLineWidth.add(lineWith).add(pointerUp).add(pointerDown).add(textWidth);


    // let divRadiusLu = $('#radiusLu');
    // let divRadiusLd = $('#radiusLd');
    // let divRadiusRu = $('#radiusRu');
    // let divRadiusRd = $('#radiusRd');
    // let divSrezLu = $('#srezLu ');
    // let divSrezLd = $('#srezLd ');
    // let divSrezRu = $('#srezRu ');
    // let divSrezRd = $('#srezRd ');
    // let divZarezLu = $('#zarezLu ');
    // let divZarezLd = $('#zarezLd ');
    // let divZarezRu = $('#zarezRu ');
    // let divZarezRd = $('#zarezRd ');
    // let btnRadiusCuttingCorners = $('#rСutCorners');
    // let btnSrezCuttingCorners = $('#srezCorners');
    // let btnzarezCuttingCorners = $('#zarezCorners');
    // let radiusLu = $('input[name=radius_lu]');
    // let radiusRu = $('input[name=radius_ru]');
    // let radiusLd = $('input[name=radius_ld]');
    // let radiusRd = $('input[name=radius_rd]');




    // /**
    //  * Hides or shows buttons for working with treatment of corners.
    //  * */
    // function managerTabsToCorners() {
    //     if (
    //         divRadiusLu.hasClass('hidden') && divRadiusRu.hasClass('hidden') &&
    //         divRadiusLd.hasClass('hidden') && divRadiusRd.hasClass('hidden')
    //     ) {
    //         btnRadiusCuttingCorners.addClass('hidden');
    //     } else {
    //         btnRadiusCuttingCorners.removeClass('hidden');
    //     }
    //     if (
    //         divSrezLu.hasClass('hidden') && divSrezRu.hasClass('hidden') &&
    //         divSrezLd.hasClass('hidden') && divSrezRd.hasClass('hidden')
    //     ) {
    //         btnSrezCuttingCorners.addClass('hidden');
    //     } else {
    //         btnSrezCuttingCorners.removeClass('hidden');
    //     }
    //     if (
    //         divZarezLu.hasClass('hidden') && divZarezRu.hasClass('hidden') &&
    //         divZarezLd.hasClass('hidden') && divZarezRd.hasClass('hidden')
    //     ) {
    //         btnzarezCuttingCorners.addClass('hidden');
    //     } else {
    //         btnzarezCuttingCorners.removeClass('hidden');
    //     }
    // }
    // // Hides all tabs of treatment of corners if is facet treatment of edge
    // managerTabsToCorners();
    // $('#edgeSubmit').on('click', managerTabsToCorners);

    // function roundLu() {
    //     let radius = Number(this.value);
    //     let currentArr = currentPart.array();
    //     let currentL = currentArr[1][1];
    //     let currentW = currentArr[3][2];
    //     console.log(currentW, currentL);
    //     currentArr[0] = ['M', radius, 0];
    //     currentArr[1] = ['l', currentL - radius, 0];
    //     currentArr[7] = ['l', 0, -currentW + radius];
    //     currentArr[8] = ['q', 0, -radius, radius, -radius];
    //     // let arr = [
    //     //     ['M', radius, 0],           //0
    //     //     ['l', partL - radius, 0],   //1
    //     //     ['l', 0, 0],                //2 ru
    //     //     ['l', 0, partW],            //3
    //     //     ['l', 0, 0],                //4 rd
    //     //     ['l', -partL, 0],           //5
    //     //     ['l', 0, 0],                //6 ld
    //     //     ['l', 0, -partW + radius],  //7
    //     //     ['q', 0, -radius, radius, -radius], //8 lu
    //     // ];
    //     currentPart.plot(currentArr);
    // }
    // radiusLu.on('change', roundLu);
    //
    // function roundRu() {
    //     let radius = Number(this.value);
    //     let currentArr = currentPart.array();
    //     let currentL = partL - currentArr[0][1];
    //     let currentW = currentArr[3][2];
    //     // let currentL = currentArr[1][1];
    //     // let currentW = currentArr[3][2];
    //     currentArr[1] = ['l', currentL - radius, 0];
    //     currentArr[2] = ['q', +radius, 0, +radius, radius];
    //     currentArr[3] = ['l', 0, currentW - radius];
    //     // let arr = [
    //     //     ['M', 0, 0],                //0
    //     //     ['l', partL - radius, 0],   //1
    //     //     ['q', +radius, 0, +radius, radius],     //2 ru
    //     //     ['l', 0, partW - radius],            //3
    //     //     ['l', 0, 0],                //4 rd
    //     //     ['l', -partL, 0],           //5
    //     //     ['l', 0, 0],                //6 ld
    //     //     ['l', 0, -partW],  //7
    //     //     ['q', 0, 0, 0, 0], //8 lu
    //     // ];
    //     currentPart.plot(currentArr);
    // }
    //radiusRu.on('change', roundRu);


    // /**
    //  * Gets all cornets that corresponds to the modified edge of the part to hide them
    //  * @param name this is a value of the attribute "name".
    //  * */
    // function getCornersForEdge(name) {
    //     let corners = [];
    //     switch (name) {
    //         case 'l_edge':
    //             corners = [divRadiusLu, divRadiusLd, divSrezLu, divSrezLd, divZarezLu, divZarezLd];
    //             break;
    //         case 'r_edge':
    //             corners = [divRadiusRu, divRadiusRd, divSrezRu, divSrezRd, divZarezRu, divZarezRd ];
    //             break;
    //         case 't_edge':
    //             corners = [divRadiusRu, divRadiusLu, divSrezRu, divSrezLu, divZarezRu, divZarezLu ];
    //             break;
    //         case 'b_edge':
    //             corners = [divRadiusRd, divRadiusLd, divSrezRd, divSrezLd, divZarezRd, divZarezLd ];
    //             break;
    //     }
    //     return corners;
    // }
    // /**
    //  * Hides all nodes
    //  * @param elements this is an array of nodes
    //  * */
    // function hideFields(elements) {
    //     for (let element of elements) {
    //         if (!element.hasClass('hidden')) {
    //             element.addClass('hidden');
    //         }
    //     }
    // }
    //
    // /**
    //  * Shows all nodes that corresponds to the modified edge of the part
    //  * @param elements this is an array of nodes
    //  * @param name this is a value of the attribute "name".
    //  * */
    // function showFields(elements, name) {
    //     let side1 = '';
    //     let side2 = '';
    //     if (name === 'l_edge' || name === 'r_edge') {
    //         side1 = selectTopEdge.val();
    //         side2 = selectBottomEdge.val();
    //     }
    //     if (name === 't_edge' || name === 'b_edge') {
    //         side1 = selectLeftEdge.val();
    //         side2 = selectRightEdge.val();
    //     }
    //     console.log(name);
    //     for (let element of elements) {
    //         console.log(element.hasClass('hidden'), side1, side2);
    //         if (element.hasClass('hidden')) {
    //             if (side1 !== 'facet' && name === 'l_edge' && element.hasClass('lu')) element.removeClass('hidden');
    //             if (side2 !== 'facet' && name === 'l_edge' && element.hasClass('ld')) element.removeClass('hidden');
    //             if (side1 !== 'facet' && name === 'r_edge' && element.hasClass('ru')) element.removeClass('hidden');
    //             if (side2 !== 'facet' && name === 'r_edge' && element.hasClass('rd')) element.removeClass('hidden');
    //             if (side1 !== 'facet' && name === 't_edge' && element.hasClass('lu')) element.removeClass('hidden');
    //             if (side2 !== 'facet' && name === 't_edge' && element.hasClass('ru')) element.removeClass('hidden');
    //             if (side1 !== 'facet' && name === 'd_edge' && element.hasClass('ld')) element.removeClass('hidden');
    //             if (side2 !== 'facet' && name === 'd_edge' && element.hasClass('rd')) element.removeClass('hidden');
    //
    //         }
    //     }
    // }

    // let arr = [ // radius ru corner
    //     ['M', 0, 0],
    //     ['h', partL - 30],
    //     ['q', +30, 0, +30, 30],
    //     ['v', partW - 30],
    //     ['h', -partL],
    //     ['v', -partW],
    // ]
    // let arr = [ // scope ru corner
    //     ['m', 0, 0],
    //     ['h', partL - 30],
    //     ['l', +30, +30],
    //     ['v', partW - 30],
    //     ['h', -partL],
    //     ['v', -partW],
    // ]
}

/**
 * Colors svg rects and hides or shows fields for treatment of corners.
 * */
function handlerSelectOfEdge() {
    let name = $(this).attr('name');
    if (this.value === 'facet') {
        getSVGSide(name).attr('fill', '#f3d2d2');
        //hideFields(getCornersForEdge(name), name);
    }
    if (this.value === 'shlif') {
        getSVGSide(name).attr('fill', '#5643FAF1');
        //showFields(getCornersForEdge(name), name);
    }
    if (this.value === 'poli') {
        getSVGSide(name).attr('fill', '#4df15942');
        //showFields(getCornersForEdge(name), name);
    }
    if (this.value === '-Нет-') {
        getSVGSide(name).attr('fill', 'none');
        //showFields(getCornersForEdge(name), name);
    }
}

function checkerSelectOfEdge(select) {
    let name = select.attr('name');
    if (select.val() === 'facet') {
        getSVGSide(name).attr('fill', '#f3d2d2');
        //hideFields(getCornersForEdge(name), name);
    }
    if (select.val()  === 'shlif') {
        getSVGSide(name).attr('fill', '#5643FAF1');
        //showFields(getCornersForEdge(name), name);
    }
    if (select.val()  === 'poli') {
        getSVGSide(name).attr('fill', '#4df15942');
        //showFields(getCornersForEdge(name), name);
    }
    if (select.val() === '-Нет-') {
        getSVGSide(name).attr('fill', 'none');
        //showFields(getCornersForEdge(name), name);
    }
}

/**
 * Gets the svg rect that corresponds to the modified edge of the part.
 * @param name this is a value of an attribute "name".
 * */
function getSVGSide(name) {
    if (name === 'l_edge') {
        return $('.side-left');
    } else if (name === 'r_edge') {
        return $('.side-right');
    } else if (name === 't_edge') {
        return $('.side-top');
    } else if (name === 'b_edge') {
        return $('.side-bottom');
    } else {
        console.error('side for this ' + name + ' not found!');
    }

}
$(function () {
    if (document.URL.indexOf('treatment') > 0) {
        let selectLeftEdge = $('#l_edge');
        let selectRightEdge = $('#r_edge');
        let selectTopEdge = $('#t_edge');
        let selectBottomEdge = $('#b_edge');

        // Listeners an event of changing and colors the svg rect
        selectLeftEdge.on('change', handlerSelectOfEdge);
        selectRightEdge.on('change', handlerSelectOfEdge);
        selectTopEdge.on('change', handlerSelectOfEdge);
        selectBottomEdge.on('change', handlerSelectOfEdge);
    }
});