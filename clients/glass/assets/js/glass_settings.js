$(function () {
    const urlForSettings = 'glass/modules/ajax_settings.php';
    // off/on buttons for editing or deleting if the select changed
    $('#holeList').on('change', function () {
        let btnEdit = $('#btnHoleEdit');
        let btnDelete = $('#btnHoleDelete');
        if ($(this).val() !== '---') {
            btnDelete.attr('disabled', false);
            btnEdit.attr('disabled', false);
        } else {
            btnDelete.attr('disabled', true);
            btnEdit.attr('disabled', true);
        }
    });

    // adds/edits hole diameters
    function sendDataOfHoleDiameter(event) {
        let $this = $(this);
        event.preventDefault();
        let divEditHole = $('#editHole').removeClass('d-none');
        let input = divEditHole.find('input[name=glass_d_bore]');
        let holeSelect = $('#holeList');
        let sendData = $('#sendData');
        let divMessage = $('.messages-hole');
        sendData.on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            let contains = false;
            for(let holeDiam of holeSelect.find('option')) {
                if (input.val() === holeDiam.innerText) {
                    contains = true;
                    break;
                }
            }
            if (contains) {
                divMessage.addClass('alert alert-danger').text('Такой диаметр уже есть');
                input.addClass('is-invalid');
                setTimeout(function () {
                    divMessage.removeClass('alert alert-danger').text('');
                    input.removeClass('is-invalid');
                }, 2000);
            } else if(Number(input.val()) < 1 || Number(input.val()) > 100) {
                divMessage.addClass('alert alert-danger').text('Значение должно быть от 1 до 100 мм');
                input.addClass('is-invalid');
                setTimeout(function () {
                    divMessage.removeClass('alert alert-danger').text('');
                    input.removeClass('is-invalid');
                }, 2000);
            } else {
                let request = "";
                let messChenge = "";
                if ($this.data('post') === 'add_diameter') {
                    request += $this.data('post') + '=' + input.val();
                    messChenge = "добавлены";
                } else if ($this.data('post') === 'edit_diameter') {
                    request += $this.data('post') + '=' + input.val() + "&id=" + holeSelect.val();
                    messChenge = "изменены";
                }
                $.post(
                    urlForSettings,
                    request,
                    function(data) {
                        let response = JSON.parse(data);
                        if (response.message === 'ok') {
                            holeSelect.find('option.d-hole').remove();
                            holeSelect.append(response.result);
                            divMessage.addClass('alert alert-success').text('Данные успешно ' + messChenge);
                            setTimeout(function () {
                                divMessage.removeClass('alert alert-success').text('');
                            }, 2000);
                        } else if (response.message === "Server error") {
                            divMessage.addClass('alert alert-warning').text('Ошибка сервера');
                            setTimeout(function () {
                                divMessage.removeClass('alert alert-warning').text('');
                            }, 2000);
                        }
                        divEditHole.addClass('d-none');
                        input.val('');
                        sendData.off('click');
                    }
                );
            }
        });
    }

    // add a new hole diameter
    $('#btnHoleAdd').on('click', sendDataOfHoleDiameter);

    // edit the hole diameter
    $('#btnHoleEdit').on('click', sendDataOfHoleDiameter);

    // delete the hole diameter
    $('#btnHoleDelete').on('click', function (event) {
        event.preventDefault();
        let holeSelect = $('#holeList');
        holeSelect.css({'background-color': '#f7d7da'});
        let divMessage = $('.messages-hole').addClass('alert alert-danger');
        divMessage.html('<p class="col-sm-10 text-center">Вы действительно хотите удалить этот диаметр</p> \n' +
            '           <button id="cancel" class="col-sm-4 btn btn-light text-center">Отмена</button> \n' +
            '           <button class="col-sm-4 btn btn-primary text-center ml-3" id="delete">Удалить</button> \n'
            );
        $('#cancel').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            divMessage.removeClass('alert alert-danger').html('');
            holeSelect.css({'background-color': 'transparent'});
        });
        $('#delete').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            divMessage.removeClass('alert alert-danger').html('');
            $.post(
                urlForSettings,
                'delete_id=' + holeSelect.val(),
                function (data) {
                    let response = JSON.parse(data);
                    if (response.message === 'ok') {
                        holeSelect.find('option.d-hole').remove();
                        holeSelect.append(response.result);
                        divMessage.addClass('alert alert-success').text('Данные успешно удалены');
                        setTimeout(function () {
                            divMessage.removeClass('alert alert-success').text('');
                        }, 2000);
                    } else if (response.message === "Server error") {
                        divMessage.addClass('alert alert-warning').text('Ошибка сервера');
                        setTimeout(function () {
                            divMessage.removeClass('alert alert-warning').text('');
                        }, 2000);
                    }
                    holeSelect.css({'background-color': 'transparent'});
                }
            );
        });
    });
    // old value of inputs for min size of details
    const oldPathToPrint = $('input[name=path_to_print]').val();
    const oldPathToSandblasting = $('input[name=path_to_sandblasting]').val();
    // Changes paths to a dir for pictures
    $('#pathPicSubmit').on('click', function (event) {
        event.preventDefault();
        const printInput = $('input[name=path_to_print]');
        const sandblastingInput = $('input[name=path_to_sandblasting]');
        const divMessage = $('.messages-for-pic').removeClass('alert alert-danger alert-success').text("");
        $.post(
            urlForSettings,
            {
                path_to_print: printInput.val(),
                path_to_sandblasting: sandblastingInput.val()
            },
            function (data) {
                let response = JSON.parse(data);
                if (response.message) {
                    divMessage.addClass('alert alert-danger').text(response.message);
                    printInput.val(oldPathToPrint);
                    sandblastingInput.val(oldPathToSandblasting);
                } else {
                    divMessage.addClass('alert alert-success').text(response.result);
                }
                setTimeout(function () {
                    divMessage.removeClass('alert alert-danger alert-success').text("");
                }, 2000);
            }
        );
    });

    // sends data of min sizes of details on server for edits.
    function changeMinValueEdge() {
        const $this = $(this);
        const input = $this.parents('.card').find('.form-group>input');
        const divMessage = $this.parents('.card').find('.message-for-min-size');
        divMessage.removeClass('alert alert-success alert-danger').text('');
        console.log(input.val());
        if (Number(input.val()) >= 0) {
            $.post(
                urlForSettings,
                input.attr('name') + '=' + input.val(),
                function (data) {
                    const response = JSON.parse(data);
                    if (response.message === 'ok') {
                        divMessage.addClass('alert alert-success').text('Данные успешно обновлены');
                    } else {
                        divMessage.addClass('alert alert-danger').text(response.message);
                    }
                    setTimeout(function () {
                        divMessage.removeClass('alert alert-success alert-danger').text('');
                    }, 2000);
                }
            );
        } else {
            divMessage.addClass('alert alert-danger').text('Значение должно быть положительным числом');
            setTimeout(function () {
                divMessage.removeClass('alert alert-danger').text('');
            }, 2000);
        }
    }

    $('#minSizeSubmit').on('click', changeMinValueEdge);
    $('#minSizeSubmitRad').on('click', changeMinValueEdge);
    $('#minSizeSubmitZarez').on('click', changeMinValueEdge);

    // sends data of max sizes for details on the server.
    function changeMaxValueDetails(event) {
        console.log(event.target);
        event.preventDefault();
        const $this = $(this);
        const inputs = $this.parents('.card').find('input');
        const divMessage = $this.parents('.card').find('.message-for-max-size');
        divMessage.removeClass('alert alert-success alert-danger').text('');
        if (Number(inputs[0].value) >= 0 && Number(inputs[1].value) >= 0) {
            $.post(
                urlForSettings,
                $(inputs[0]).attr('name') + '=' + inputs[0].value + '&' + $(inputs[1]).attr('name') + '=' + inputs[1].value,
                function (data) {
                    const response = JSON.parse(data);
                    if (response.message === 'ok') {
                        divMessage.addClass('alert alert-success').text('Данные успешно обновлены');
                        inputs.each(function () {
                            $(this).attr('prevval', $(this).val());
                        });
                    } else {
                        divMessage.addClass('alert alert-danger').text(response.message);
                        inputs.each(function () {
                            $(this).val($(this).attr('prevval'));
                        });
                    }
                    setTimeout(function () {
                        divMessage.removeClass('alert alert-success alert-danger').text('');
                    }, 2000);
                    $(inputs[0]).val(response.result[0]);
                    $(inputs[1]).val(response.result[1]);
                }
            );
        } else {
            divMessage.addClass('alert alert-danger').text('Значение должно быть положительным числом');
            inputs.each(function () {
                $(this).val($(this).attr('prevval'));
            });
            setTimeout(function () {
                divMessage.removeClass('alert alert-danger').text('');
            }, 2000);
        }
    }
    $("#maxDetailSize").on('click', changeMaxValueDetails);
    $('#maxSizeForTreatment').on('click', changeMaxValueDetails);
    $('#maxWidthFacet').on('click', changeMaxValueDetails);
    $('#maxSizeForTreatmentR').on('click', changeMaxValueDetails);
    $('#maxSizeForPrint').on('click', changeMaxValueDetails);
    $('#maxSizeForSandblasting').on('click', changeMaxValueDetails);
});