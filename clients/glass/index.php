<?php
    session_start();
    include "../functions2.php";
    //Если менеджер переходит сюда без клиента, переадресовуем его на index.php
    if (isset($_SESSION['manager_id']) && !isset($_GET['client_id'])) {
        $_SESSION['messages']['errors'][] = 'Вы не выбрали контрагента! Чтобы перейти к созданию РС, пожалуйста выберите контрагента.';
        header('Location: '.$main_dir.'/clients/index.php'.'?nw='.$_GET['nw']);
        exit();
    }
    if (isset($_GET['client_id'])) {
        $_SESSION['client_id'] = $_GET['client_id'];
    }
    $title = 'Конструктор стекла';
    include "../header.php";
    unset($_SESSION['order_st']);
    unset($_SESSION['materials']);

    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
?>

<!-- start/ content -->
<div class="container-fluid">

    <div class="row justify-content-center">
        <div class="col-sm-6">
            <h4 class="text-center mb-5">Выберете тип материал из выподающего списка</h4>
            <div class="form-group">
                <input type="hidden" id="getNW" value="<?= $_GET['nw'] ?>">
                <label for="type_material">Тип материала</label>
                <select class="form-control" id="typeMaterial" name="type_material">
                    <option>---</option>
                    <option value="1">Стекло</option>
                    <option value="2">Зеркало</option>
                </select>
            </div>
        </div>
    </div>
<?php
include '../footer.php';

