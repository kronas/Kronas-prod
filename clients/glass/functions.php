<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/_1/config.php';
include_once $serv_main_dir . '/clients/functions2.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/services/write_pdf_file.php';

/**
 * Returns all rows of glasses of mirrors form the table MATERIAL.
 * @param $type int
 * @return array
 */
function glass_get_materials_by_type($type)
{
    $types = [1 => 'RS_GLASS', 2 => 'RS_MIRROR'];
    $type = intval($type);
    $type = $types[$type];
    $sql = "SELECT MATERIAL.MATERIAL_ID AS id, MATERIAL.NAME as name FROM MATERIAL WHERE $type = 1";
    $rows = sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
    return $rows;
}

/**
 * Returns name and id of the material by id form the table MATERIAL.
 * @param $id int
 * @return array
 */
function glass_get_materials_by_id($id)
{
    $sql = "SELECT MATERIAL.MATERIAL_ID as id, MATERIAL.NAME as name FROM MATERIAL WHERE MATERIAL.MATERIAL_ID = $id";
    $result = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    return $result[0];
}
 /**
 * Returns all oracals
 */
function glass_get_all_oracals()
{
    $sql = "SELECT	MATERIAL.`NAME`, MATERIAL.`CODE`FROM MATERIAL WHERE MATERIAL.MY_1C_NOM =7210";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
}

 /**
 * Returns all hole diameters from the table glass_general
 */
 function glass_get_all_hole_diameters()
 {
     $sql = "SELECT * FROM glass_general WHERE glass_general.name like 'hole_diameter'";
     $result = sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'];
     return $result;
 }

 /**
 * Returns list of all hole diameters
 */
 function glass_get_holes_list_options()
 {
     $options = '';
     $holes = glass_get_all_hole_diameters();
     foreach ($holes as $hole) {
        $options .= "<option class='d-hole' value={$hole['id']}>{$hole['value']}</option>";
     }
     return $options;
 }

/**
 * Adds a new hole diameter in the table glass_general
 * @param $diameter int
 * @return bool|mysqli_result
 */
function glass_add_new_hole_diameter($diameter)
{
    $sql = "INSERT INTO glass_general (name, value, name_rus) VALUES ('hole_diameter', '$diameter', 'Диаметр сверления отверстия')";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}

/**
 * Edits the hole diameter in the table glass_general
 * @param $id int
 * @param $diameter int
 * @return bool|mysqli_result
 */
function glass_edit_hole_diameter($id, $diameter)
{
    $sql = "UPDATE glass_general SET value='$diameter' WHERE id=$id";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}

/**
 * Deletes the hole diameter in the table glass_general
 * @param $id int
 * @return bool|mysqli_result
 */
function glass_delete_hole_diameter($id)
{
    $sql = "DELETE FROM glass_general WHERE  id=$id";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}


/**
 * Gets all rows form the glass_general table without rows where is name is hole_diameter
 * @return array
 */
function glass_get_settings_without_bore()
{
    $sql = "SELECT * FROM glass_general WHERE glass_general.name NOT like 'hole_diameter'";
    $result = sql_data(__LINE__,__FILE__, __FUNCTION__, $sql)['data'];
    $settings = [];
    foreach ($result as $item) {
        $settings[$item['name']] = $item;
    }

    return $settings;
}

function glass_update_value_by_name($name, $value)
{
    $sql = "UPDATE glass_general SET value='$value' WHERE name='$name'";
    return sql_data(__LINE__, __FILE__, __FUNCTION__, $sql);
}

function glass_parse_all_settings()
{
    $result = [];
    $settings = glass_get_settings_without_bore();
    $holes = glass_get_all_hole_diameters();
    $result['max_length'] = $settings['max_length']['value'];
    $result['max_width'] = $settings['max_width']['value'];
    $result['path_to_print'] = $settings['path_to_print']['value'];
    $result['path_to_sandblasting'] = $settings['path_to_sandblasting']['value'];
    $result['to_4mm'] = $settings['to_4mm']['value'];
    $result['over_4mm'] = $settings['over_4mm']['value'];
    $result['max_size_for_edge'] = [
        $settings['max_length_for_edge']['value'],
        $settings['max_width_for_edge']['value']
    ];
    $result['min_size_for_edge'] =  $settings['min_size_for_edge']['value'];
    $result['max_size_for_edge_with_rounded_corner'] = [
        $settings['max_length_for_edge_with_rounded_corner']['value'],
        $settings['max_width_for_edge_with_rounded_corner']['value']
    ];
    $result['min_size_for_edge_with_rounded_corner'] =  $settings['min_size_for_edge_with_rounded_corner']['value'];

    $result['max_size_for_photo_print'] = [
        $settings['max_length_for_photo_print']['value'],
        $settings['max_width_for_photo_print']['value']
    ];
    $result['max_size_for_sandblasting'] = [
        $settings['max_length_for_sandblasting']['value'],
        $settings['max_width_for_sandblasting']['value']
    ];
    $result['hole_diameter'] = [];
    foreach ($holes as $hole) {
        $result['hole_diameter'][] = $hole['value'];
    }
    return $result;
}

function general_img_and_pdf($order_st, $project)
{
    include $_SERVER['DOCUMENT_ROOT'] . '/clients/glass/services/write_pdf_file.php';
    foreach ($order_st['parts'] as $key => $val) {
        if ($val['path']) {
            $id = $project['parts'][$key];
            //p_($id);
            $r = build_req_for_project($project['project'], 'xnc_get_card_one');
            $response = gib_serv($r, 'C4F6336B','Чертёж',__LINE__,__FILE__,__FUNCTION__,$project['project']);
            //p_($pdf_file);
            foreach ($val['path'] as $path) {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . $path))
                    unlink($_SERVER['DOCUMENT_ROOT'] . $path);
            }

            $file_path = writePdf($response, $key);
            $order_st['parts'][$key]['path'] = $file_path;
            p_($order_st['parts'][$key]['path']);
            exit;
        }
    }
    return $order_st;
}

// clears session after zzz.php
function clear_sess_after_zzz()
{
    if (isset($_SESSION['project_data'])) unset($_SESSION['project_data']);
    if (isset($_SESSION['vals'])) unset($_SESSION['vals']);
    if (isset($_SESSION['is_manager'])) unset($_SESSION['is_manager']);
    if (isset($_SESSION['is_client'])) unset($_SESSION['is_client']);
    if (isset($_SESSION['arr_parts'])) unset($_SESSION['arr_parts']);
    if (isset($_SESSION['client_fio'])) unset($_SESSION['client_fio']);
    if (isset($_SESSION['client_phone'])) unset($_SESSION['client_phone']);
    if (isset($_SESSION['client_email'])) unset($_SESSION['client_email']);
    if (isset($_SESSION['order_comment'])) unset($_SESSION['order_comment']);
    if (isset($_SESSION['simple_price_extra'])) unset($_SESSION['simple_price_extra']);
    if (isset($_SESSION['user_place_f_rs'])) unset($_SESSION['user_place_f_rs']);
    if (isset($_SESSION['error'])) unset($_SESSION['error']);
    if (isset($_SESSION['manager_id'])) unset($_SESSION['manager_id']);
    if (isset($_SESSION['manager_name'])) unset($_SESSION['manager_name']);
    if (isset($_SESSION['isAdmin'])) unset($_SESSION['isAdmin']);
    if (isset($_SESSION['client_id'])) unset($_SESSION['client_id']);
}

function session_restore($data_sess)
{
    $_SESSION['user'] = $data_sess['user'];
    $_SESSION['client_code'] = $data_sess['client_code'];
    $_SESSION['user_place'] = $data_sess['user_place'];
    if (isset($data_sess['manager_name'])) $_SESSION['manager_name'] = $data_sess['manager_name'];
    if (isset($data_sess['client_id'])) $_SESSION['client_id'] = $data_sess['client_id'];
    if (isset($data_sess['manager_id'])) $_SESSION['manager_id'] = $data_sess['manager_id'];
    if (isset($data_sess['isAdmin'])) $_SESSION['isAdmin'] = $data_sess['isAdmin'];
}

function pdf_and_img_restore($order_st)
{

    foreach ($order_st['parts'] as $key => $val) {
        if (isset($val['pdf_content'])) {
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $val['path'][0])) {
                $files_path = writePdf($val['pdf_content'], $key);
                $order_st['parts'][$key]['path'] = $files_path;
            }
            unset($order_st['parts'][$key]['pdf_content']);
        }
    }
    return $order_st;
}


