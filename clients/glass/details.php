<?php
session_start();

include_once '../../_1/config.php';
include '../functions2.php';
include_once 'functions.php';

$ini_data = glass_parse_all_settings();

if (isset($_POST['material_id']) && !is_null($_POST['material_id'])) {
    if (!isset($_SESSION['user_place'])) {
        if (isset($_SESSION['user']['role']) && $_SESSION['user']['role'] != 'client') {
            $user = get_auth_manager("id = " . $_SESSION['user']['ID']);
            $_SESSION['user_place'] = $user['place'];
        } else {
            $_SESSION['user_place'] = 1;
        }
    }
    if (!isset($_SESSION['client_code'])) {
        if (isset($_SESSION['client_id'])) {
            $sql = "SELECT client.code FROM client WHERE client.client_id = {$_SESSION['client_id']}";
            $client_code = sql_data(__LINE__, __FILE__, __FUNCTION__, $sql)['data'][0]['code'];
            $_SESSION['client_code'] = ($client_code !== '') ? $client_code : 123855;
        } elseif(isset($_SESSION['user']) && $_SESSION['user']['role'] === 'client') {
            $_SESSION['client_code'] = $_SESSION['user']['code'];
        } else {
            $_SESSION['client_code'] = 123855;
        }
    }
    $material_id = intval($_POST['material_id']);
    $data = glass_get_materials_by_id($material_id);

    $data['type_material_name'] = $_POST['type_material'];
    $data['height'] = preg_replace('/[^0-9]/', '', $data['name']);
    $jsonData = json_encode($data);
    $project_arr = "";
    $_SESSION['materials'] = $jsonData;
    if (isset($_SESSION['order_st'])) {
        $order_st = json_decode($_SESSION['order_st'], true);
    } else {
        $order_st = [
            'order_st' => [
                'material' => $data['id'],
                'data_material' => [
                    'name' => $data['name'],
                    'height' => $data['height'],
                    'type_material_name' => $data['type_material_name'],
                ],
                'data_user' => [
                    'user' => ($_SESSION['user']) ?? 'guest',
                    'user_place' => $_SESSION['user_place'],
                    'client_code' => $_SESSION['client_code'],
                ],
            ],
        ];
        // если пришел менеджер или админ дописываем доп. данные
        if (isset($_SESSION['manager_name'])) $order_st['order_st']['data_user']['manager_name'] = $_SESSION['manager_name'];
        if (isset($_SESSION['client_id'])) $order_st['order_st']['data_user']['client_id'] = $_SESSION['client_id'];
        if (isset($_SESSION['manager_id'])) $order_st['order_st']['data_user']['manager_id'] = $_SESSION['manager_id'];
        if (isset($_SESSION['isAdmin'])) $order_st['order_st']['data_user']['isAdmin'] = $_SESSION['isAdmin'];
    }
    $jsonOrderSt = json_encode($order_st);
    $_SESSION['order_st'] = $jsonOrderSt;
    unset($order_st);
}
if (!isset($_SESSION['materials'])) {
    if (isset($_POST['glass_arr'])) {
        // чистим сессию после zzz.php, а то от туда много лишнего и непонятного приходит ))
        clear_sess_after_zzz();
        $order_st = json_decode($_POST['glass_arr'], true);
        // востанавливаем сессию после возврата из zzz.php
        $_SESSION['user'] = $order_st['order_st']['data_user']['user'];
        $_SESSION['client_code'] = $order_st['order_st']['data_user']['client_code'];
        $_SESSION['user_place'] = $order_st['order_st']['data_user']['user_place'];
        $_SESSION['order_st'] = $_POST['glass_arr'];
        $_SESSION['materials'] = json_encode($order_st['order_st']);
        if (isset($order_st['order_st']['data_user']['manager_name'])) $_SESSION['manager_name'] = $order_st['order_st']['data_user']['manager_name'];
        if (isset($order_st['order_st']['data_user']['client_id'])) $_SESSION['client_id'] = $order_st['order_st']['data_user']['client_id'];
        if (isset($order_st['order_st']['data_user']['manager_id'])) $_SESSION['manager_id'] = $order_st['order_st']['data_user']['manager_id'];
        if (isset($order_st['order_st']['data_user']['isAdmin'])) $_SESSION['isAdmin'] = $order_st['order_st']['data_user']['isAdmin'];
        $return_from_zzz = true;
    } else {
        header('Location: /'.'?nw='.$_GET['nw']);
        $error = "<div class='alert alert-danger text-center'>Выберете материал для заказа</div>";
        $_SESSION['error'] = $error;
        die;
    }
}

$title = 'Конструктор стекла';
include "$serv_main_dir/clients/header.php";

if (isset($_SESSION['error'])) {
    echo $_SESSION['error'];
    unset($_SESSION['error']);
}

$order_st = json_decode($_SESSION['order_st'], true);
//p_($order_st);
// если уже создалась деталь то перегоняем массив в project и генерируем чертежи, если были до отправки заказа на zzz.php
if (isset($order_st['parts'])) {
    $result = glass_to_project($order_st, ($_SESSION['client_code']) ?? 123855,($_SESSION['user_place']) ?? 1);
// todo: нужно генерить картинки при возврате из zzz.php или при сохранении заказа
//    if (isset($return_from_zzz) && $return_from_zzz) {
//        $order_st = general_img_and_pdf($order_st, $result);
//        $jsonOrderSt = json_encode($order_st);
//        $_SESSION['order_st'] = $jsonOrderSt;
//        $return_from_zzz = false;
//    }
    $result['glass_arr'] = $order_st;
    $project_arr = base64_encode(serialize($result));
}
// Плучаем детали для вывода в layouts/tbody_details_table.php
$parts = (isset($_SESSION['order_st']) && strripos($_SESSION['order_st'],'"parts":{'))
    ? json_decode($_SESSION['order_st'], true)['parts'] : [];
//print_r(json_decode($_SESSION['order_st'], true));
//p_($_SESSION);
?>
<!-- start/ content -->
<div class="container-fluid">
    <div id="pageTitle" class="row">
        <div class="col-sm-12">
            <h4 class="text-center"><?= $order_st['order_st']['data_material']['type_material_name'] ?></h4>
            <h5 class="text-center"><?= $order_st['order_st']['data_material']['name'] . " <span class='hidden' 
            id='matLength'>{$ini_data['max_length']}</span><span class='hidden'
            id='matWidth'>{$ini_data['max_width']}</span><span class='hidden'
            id='matHeight'>{$order_st['order_st']['data_material']['height']}</span>" ?></h5>
        </div>
        <div class="col-sm-12">
            <div class="row mt-5">
                <div class="col-sm-1 d-flex flex-column">
                    <span id="forExit" title="Вернуться к материалам">
                        <i class="btn-for-details fas fa-door-open h1"></i>
                    </span>
                    <form id="sendOrder" class="<?php
                    if (empty($parts)) echo 'hidden';

                    ?>" action="<?= $main_dir ?>/zzz.php<?='?nw='.$_GET['nw']?>" method="POST" title="Отправить заказ">
                        <input type="hidden" name="glass_project" value="<?= $project_arr ?>">
                        <button class="btn-save-order" type="submit" id="orderSubmit">
                            <i class="btn-for-details fas fa-dolly-flatbed text-danger h1"></i>
                        </button>
                    </form>
                    <div>
                        <label class="btn-save-order m-0" title="Загрузить сохраненный проект"><i class="btn-for-details fas fa-upload text-black-50 h1"></i>
                            <input type="file" class="d-none" id="fileOrder">
                        </label>
                    </div>
                    <span data-toggle="modal" data-target="#forAddDetails" title="Добавить новую деталь">
                        <i class="btn-for-details fas fa-plus-square text-success h1"></i>
                    </span>
                    <form id="formSaveOrder" class="<?php
                    if (empty($parts)) echo 'hidden';

                    ?>" action="modules/save_order.php<?='?nw='.$_GET['nw']?>" method="POST" title="Сохранить проект">
                        <input type="hidden" name="order_st" value='<?= $_SESSION['order_st'] ?>'>
                        <button class="btn-save-order" type="submit" id="saveOrder">
                            <i class="btn-for-details fas fa-save text-primary h1"></i>
                        </button>
                    </form>
                    <div>
                        <a download="order.krgl" href="#" id="downloadOrder" class="btn btn-save-order hidden" title="Скачать проект">
                            <i class="btn-for-details fas fa-download text-warning h1"></i>
                        </a>
                    </div>
                </div>
                <div class="col-sm-11" id="parts">
                    <?php
                    include "$serv_main_dir/clients/glass/layouts/tbody_details_table.php";
                    ?>
                </div>
            </div>
        </div>
        <!-- end/ content -->
    </div>
</div>
<!-- end/ content -->
<!-- start/ modals-->
<!-- start/ modal create details -->
<div class="modal fade" id="forAddDetails" tabindex="-1" role="dialog" aria-labelledby="modalForAddDetails"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Новая деталь</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formCreateDetail" action="modules/add_detail.php<?='?nw='.$_GET['nw']?>" method="POST">
                    <input type="hidden" name="material_width" value="<?= $data['width'] ?>">
                    <input type="hidden" name="material_length" value="<?= $data['length'] ?>">
                    <div class="form-group row m-3">
                        <label class="col-sm-6" for="length">Длина, мм</label>
                        <input type="number" class="col-sm-6 form-control" id="addedLengthPart" name="length">
                    </div>
                    <div class="form-group row m-3">
                        <label class="col-sm-6" for="width">Ширина, мм</label>
                        <input type="number" class="col-sm-6 form-control" id="addedWidthPart" name="width">
                    </div>
                    <div class="form-group row m-3">
                        <label class="col-sm-6" for="quantity">Количество, шт</label>
                        <input type="number" class="col-sm-6 form-control" id="addedQuantityPart" name="quantity">
                    </div>
                    <div id="messageCreatDetail"></div>
                    <button class="btn btn-primary" id="btnCreateDetail">Создать</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end/ modal create details -->

<!-- start/ modal facet handle -->
<div class="modal fade" id="forFacetHandleEdge" tabindex="-1" role="dialog" aria-labelledby="facetHandleEdge"
     aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="text-center modal-title" id="titleFacetHandelEdge"></h5>
            </div>
            <div class="modal-body row justify-content-center">
                <div class="form-group col-sm-6">
                    <label for="length">Ширина фацета, мм</label>
                </div>
                <div class="form-group col-sm-6">
                    <input type="hidden" value="<?php
                        if ($order_st['order_st']['data_material']['height'] <= 4) {
                            echo $ini_data['to_4mm'];
                        } elseif ($order_st['order_st']['data_material']['height'] > 4) {
                            echo $ini_data['over_4mm'];
                        }
                    ?>" id="maxFacet">
                    <input type="number" class="form-control" id="facetHandelLength" name="length">
                </div>
                <div id="messageHandleFace"></div>
                <button class="btn-sm btn-primary text-center" id="btnHandelFacetEdge">Выбрать</button>
            </div>
        </div>
    </div>
</div>
<!-- end/ modal efacet handle -->
<!--start/ modal show saved orders-->
<div class="modal fade " id="savedOrders" aria-labelledby="savedOrders"  tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Выберете фаил</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mt-3">
                    <label class="col-sm-7 col-form-label ml-2" for="fileOrder">Загрузить свой проект</label>
                    <div class=" col-sm-4 mt-2" id='uploadOrder'>
                        <label class="btn btn-sm btn-success btn-file">Загрузить
                            <input type="file" class="d-none" id="fileOrder">
                        </label>
                    </div>
                </div>
                <div id="messageForUploadOrder"></div>
            </div>
            <div class="modal-footer">
                <div class="mr-5" id="message"></div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary hidden" id="chooseOrder">Загрузить</button>
            </div>
        </div>
    </div>
<?php
include '../footer.php';
