<?php
session_start();
include_once('functions2.php');
//include_once('../_1/config.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
}
/*
$_SESSION['user']['ID'] = 2;
$_SESSION['user']['role'] = 'manager';
$_SESSION['messages']['errors'][9] = 'dev';
*/
// echo 'удали подставку'; 
// $_SESSION['user']['code']=25860;
?>
<style>
    #start_checks_lodaer_containers {
        position: fixed;
        top: 0;
        left: 0;
        background: rgba(255,255,255,0.4);
        width: 100vw;
        height: 100vh;
        display: none;
        z-index: 999999999;
    }
    #start_checks_lodaer_containers > div {
        width: 100%;
        display: flex;
        flex-flow: row nowrap;
        justify-content: center;
        align-items: center;
        z-index: 9999;
        height: 100%;
    }
    #start_checks_lodaer_containers > div > img {
        opacity: 1;
    }
    .table td {
        min-width: 100px;
    }
</style>

<?php if($_GET['order_s']):
    $title='Спецификация деталей для заказа #'.base64_decode($_GET['order_name']);
    include_once('header.php');

    $parts = db_get_parts(base64_decode($_GET['order_s']));
    ?>
    <div class="row">
        <div class="col-12 pl-3 pr-3">
            <a class="btn btn-danger" href="<?= $main_dir ?>/clients/orders.php?back=1">
                Назад
            </a>
            <br><br><br>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Ширина</th>
                    <th>Длина</th>
                    <th>Толщина</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($parts as $part): ?>
                    <tr>
                        <td><?= $part['PART_ID'] ?></td>
                        <td><?= $part['NAME'] ?></td>
                        <td><?= $part['DESCRIPTION'] ?></td>
                        <td><?= $part['W'].' мм' ?></td>
                        <td><?= $part['L'].' мм' ?></td>
                        <td><?= $part['T'].' мм' ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>


<?php elseif($_GET['order_r']):
    $title='Счет для заказа #'.base64_decode($_GET['order_name']);
    include_once('header.php');

    $parts = db_get_receipt(base64_decode($_GET['order_r']));

    $total = 0;

//    print_r_($parts);
//    exit;
    ?>
    <div class="row">
        <div class="col-12 pl-3 pr-3">
            <a class="btn btn-danger" href="<?= $main_dir ?>/clients/orders.php?back=1">
                Назад
            </a>
            <br><br><br>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Стоимость</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($parts as $part): ?>
                    <?php $total += $part['price']; ?>
                    <tr>
                        <td><?= $part['id'] ?></td>
                        <td><?= $part['name'] ?></td>
                        <td><?= $part['type'] ?></td>
                        <td><?= $part['count'] ?></td>
                        <td><?= number_format($part['cost'], 2, ',', ' '); ?></td>
                        <td><?= number_format(round($part['price'], 2), 2, ',', ' '); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4" class="text-right">
                        <h3>Всего: </h3>
                    </td>
                    <td>
                        <h3> <?= number_format(round($total, 2), 2, ',', ' '); ?></h3>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

<?php else: ?>


    <?php
    $title='Заказы';
    include_once('header.php');

    $page = 1;

    if($_GET['page'] && !$_GET['order_r'] && !$_GET['order_s']) {
        $page = $_GET['page'];
    }
    $filter_date = isset($_GET['order_filter_date']) ? $_GET['order_filter_date'] : 'all';
    $filter_status = isset($_GET['order_filter_status']) ? $_GET['order_filter_status'] : 'all';
    $filter_type = isset($_GET['order_filter_type']) ? $_GET['order_filter_type'] : 'all';

    $orders = db_get_orders(164180, $page, $filter_date, $filter_status, $filter_type);
?>
<div>
    <a href="<?= $laravel_dir ?>/new_project_from_account" class="btn btn-success">+ Новый заказ</a>
    <hr>
</div>
<form action="<?= $main_dir ?>/clients/orders.php<?='?nw='.$_GET['nw']?>" method="GET">
    <div class="row">
        <div class="form-group col-md-3">
            <label for="order_filter_date">Период:</label>
            <select id="order_filter_date" name="order_filter_date" class="form-control">
                <option value="all" <?php if($filter_date == 'all') echo 'selected'; ?> >все</option>
                <option value="1" <?php if($filter_date == 1) echo 'selected'; ?> >За сегодняшний день</option>
                <option value="7" <?php if($filter_date == 7) echo 'selected'; ?> >За прошлую неделю</option>
                <option value="30" <?php if($filter_date == 30) echo 'selected'; ?> >За прошлых 30 дней</option>
                <option value="365" <?php if($filter_date == 365) echo 'selected'; ?> >За весь год</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="order_filter_status">Статус:</label>
            <select id="order_filter_status" name="order_filter_status" class="form-control">
                <option value="all" <?php if($filter_status == 'all') echo 'selected'; ?> >все</option>
                <option value="в работе" <?php if($filter_status == 'в работе') echo 'selected'; ?> >в работе</option>
                <option value="выполнен" <?php if($filter_status == 'выполнен') echo 'selected'; ?> >выполнен</option>
                <option value="отменен" <?php if($filter_status == 'отменен') echo 'selected'; ?> >отменен</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="order_filter_type">Тип:</label>
            <select id="order_filter_type" name="order_filter_type" class="form-control">
                <option value="all" <?php if($filter_type == 'all') echo 'selected'; ?> >все</option>
                <option value="1" <?php if($filter_type == 1) echo 'selected'; ?> >Пильный</option>
                <option value="5" <?php if($filter_type == 5) echo 'selected'; ?> >Обычный</option>
                <option value="7" <?php if($filter_type == 7) echo 'selected'; ?> >Раздвижная система</option>
            </select>
        </div>
        <div class="form-group col-md-3 confirm_filters_button">
            <label for="">&nbsp;&nbsp;&nbsp;</label>
            <button class="btn btn-success">Фильтровать</button>
        </div>
    </div>
</form>

<?php
    if(count($orders) < 1) {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div>';
        exit;
    }

    $orders_count = db_get_orders_count($_SESSION['user']['code'], $page, $filter_date, $filter_status, $filter_type);

    $pages = $orders_count/25;
    if(round($pages) >= $pages) {
        $pages = round($pages);
    } else if(round($pages) < $pages) {
        $pages = round($pages) + 1;
    }

    ?>


    <div class="row">
        <div class="col-12 pl-3 pr-3">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Дата создания</th>
                    <th>Номер заказа</th>
                    <th>Контрагент</th>
                    <th>Участок</th>
                    <th>Менеджер</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($orders as $order): ?>
                <?php if($order['ID']==254294)_pre($order) ?>
                    <tr>
                        <td><?php echo $order['DB_AC_IN'] ? $order['DB_AC_IN'] : 'Неизвестно' ?></td>
                        <td><?= $order['DB_AC_ID'].' / ('.$order['DB_AC_NUM'].')'; ?></td>
                        <td><?= $order['client_name'] ?></td>
                        <td><?= $order['NAME'] ?></td>
                        <td>
                            <span><?= $order['name'] ?></span><br>
                            <small>email: <a href="mailto:<?= $order['e-mail'] ?>"><?= $order['e-mail'] ?></a></small><br>
                            <small>телефон: <?php echo $order['phone'] ? $order['phone'] : '+38(044)390-00-07'; ?></small><br>

                        </td>
                        <td><?= $order['status'] ?></td>
                        <td class="table_actions_container">
                            <?php if(isset($order['MY_PROJECT_OUT'])):?>
                            <a href="#" title="PDF" class="order_project_pdf_<?= $order['ID'] ?>" data-project=""><i class="fas fa-file-pdf"></i></a>
                            <?php if($_SESSION['user']['role'] == 'admin'): ?>
                                <a href="<?= $main_dir ?>/refresh_project_programm.php?project=<?= $order['ID'] ?>" class="refresh_buttons_programm_<?= $order['ID'] ?>" target="_blank" title="Обновить программы заказа" target="_blank">
                                    <i class="fas fa-sync-alt"></i>
                                </a>
                            <?php endif; ?>
                            <?php endif; ?>
                            <?php if($order['parts'] > 0):?>
                            <a href="orders.php?order_s=<?= base64_encode($order['ID']) ?>&order_name=<?= base64_encode($order['DB_AC_ID'].' / ('.$order['DB_AC_NUM'].')'); ?>" target="_blank" title="Спецификация"><i class="fas fa-list"></i></a>
                            <?php endif; ?>
                            <a href="orders.php?order_r=<?= base64_encode($order['ID']) ?>&order_name=<?= base64_encode($order['DB_AC_ID'].' / ('.$order['DB_AC_NUM'].')'); ?>" target="_blank" title="Счет"><i class="fas fa-file-invoice-dollar"></i></a>

                            <?php $order['status'] == 1 ? $order_status = '2_' : $order_status = '1_';  ?>
                            <?php if($order['parts'] > 0):?>
                            <div style="display: flex; position: relative; text-align: left;">
                                <a id="show_link_container_<?=$order['ID']?>">
                                    <i class="fas fa-external-link-alt" title="Редактировать этот проект"></i>
                                </a>
                                <span class="edit_link_variant" id="link_container_<?=$order['ID']?>">
                                    <?php foreach ($order['edit_links'] as $link): ?>
                                        <a href="<?= $laravel_dir ?>/projects_new/<?= base64_encode($order_status.$order['ID']).'?order_project_id='.$link['link'] ?>" target="_blank" ><?= $link['author'] ?> от <?= $link['date'] ?></a>
                                    <?php endforeach; ?>
                                </span>

                            </div>
                            <?php endif; ?>
                            <script>
                                $('.order_project_pdf_<?= $order['ID'] ?>').on('click', async function(e) {
                                    e.preventDefault();
                                    let formData = new FormData();
                                    formData.append('project_data', `<?= base64_decode($order['MY_PROJECT_OUT']['MY_PROJECT_OUT']) ?>`);
                                    formData.append('action', 'get_all_pdf_manager' );
                                    formData.append('is_admin', 'false' );

                                    let request = await fetch('<?= $main_dir ?>/pdf_send.php', {
                                        method: 'POST',
                                        headers: {
                                            ///'Content-Type': 'application/x-www-form-urlencoded'
                                        },
                                        body: formData
                                    });
                                    let body = await request.text();


                                    if(body.indexOf('.pdf') > -1){
                                        window.open('<?= $main_dir ?>/pdf_send.php?show=' + body);
                                    }
                                });
                                $('.refresh_buttons_programm_<?= $order['ID'] ?>').on('click', async function (e) {
                                    e.preventDefault();

                                    $('#start_checks_lodaer_containers').show();

                                    let url = $(e.target).closest('a').attr('href');

                                    let req_refresh = await fetch(url, {
                                            method: 'GET',
                                        }
                                    );
                                    let req_body = await req_refresh.text();
                                    console.log(req_body.trim());
                                    if(req_body.trim() == 'Программы успешно обновлены!') {
                                        $(e.target).closest('a').html(`
                                            <i class="fa fa-check-circle" aria-hidden="true" style="color:green!important;"></i>
                                        `);
                                        $(e.target).closest('a').attr('href', '');
                                    }
                                    $('#start_checks_lodaer_containers').hide();
                                });
                                $(document).ready(function () {
                                    $('#show_link_container_<?=$order['ID']?> > *').on('click', async function () {
                                        $('#link_container_<?=$order['ID']?>').toggleClass('open');
                                    });
                                });
                            </script>
                            <?php
                            if($order['rs_order']==1) echo '<a href="RS/clients/create.php?order_project_id='.$order['ID'].'" target="_blank" title="Проект"><i class="fas fa-external-link-alt text-dark"></i></a>';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php for($i = 1; $i <= $pages; $i++): ?>
                    <li class="page-item <?php echo $page == $i ? 'active' : ''; ?>"><a class="page-link" href="<?= $main_dir ?>/clients/orders.php?page=<?=$i?>&order_filter_date=<?= $filter_date ?>&order_filter_status=<?= $filter_status ?>&order_filter_type=<?= $filter_type ?>"><?=$i?></a></li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>

    <style>
        .table_actions_container a {
            font-size: 35px;
            padding: 0px 15px;
            text-align: center;
        }
    </style>


<?php endif; ?>

<div id="start_checks_lodaer_containers" style="display: none;">
    <div>
        <img src="<?= $laravel_dir ?>/images/download.gif" alt="">
    </div>
</div>