<?php

/**
 * Скрипт відсилає матеріали клієнта в новий сервіс (разова акція)
 */
ini_set('max_execution_time', '1800');// 300 - 5хв // 1800 - 30хв

session_start();

include_once(__DIR__.'/../_1/config.php');
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');
// include_once('functions2.php');
include_once('func_api.php');

// ##############################################
// Небезпечний файл категорично не рекомендується $activateTransfer встановлювати в true;
// якщо немає повного розуміння всіх наслідків!
$activateTransfer = false;
// ##############################################

if ($activateTransfer === true) {
	
	// Вибірка даних, які будуть передаватись
	$q = "SELECT 
			mc.`W`, 
			mc.`L`, 
			mc.`T`, 
			mc.`CODE_MAT`, 
			mc.`Name` AS 'mat_name', 
			mc.`crash`, 
			mc.`notation`, 
			mc.`sync`, 
			m.`Move`,
			m.`Doc` AS 'd_id',
			m.`Material_client` AS 'mc_id',
			c.`Client` AS 'client_id',
			cl.`name` AS 'client_name', 
			cl.`tel` AS 'client_tel', 
			cl.`code` AS 'client_code', 
			c.`Count`,
			c.`Place`
		FROM 
		    `MATERIAL_KL_STOCK_MOVE` AS m
		INNER JOIN 
		    `MATERIAL_KL_STOCK_COUNT` AS c ON m.`Material_client` = c.`Material_client`
		INNER JOIN 
		    `MATERIAL_CLIENT` AS mc ON mc.`mc_id` = c.`Material_client`
		INNER JOIN `client` AS cl ON cl.`client_id` = c.`Client`
		WHERE 
		    c.`Client` NOT IN (313695) AND m.`Move` > 0 AND c.`Count` > 0
		AND 
			m.`Doc` = (
				SELECT MIN(Doc) 
				FROM `MATERIAL_KL_STOCK_MOVE` 
				WHERE `Material_client` = m.`Material_client` AND `Move` > 0
			);";

	$clientMaterials = getTableData($q);
	// da(count($clientMaterials));
	// de($clientMaterials);
	if (!empty($clientMaterials)) {

		$materialsReady = [];
		$entriesCount = 0;
		$partsCount = 0;
		foreach ($clientMaterials as $key => $value) {

			if (!empty($value['client_code'])) {
				$tmp = [];
				$tmpType = getMaterialType($value['mat_name'], $value['T']);
				$value['typeMaterial'] = $tmpType['type'];
				$value['type'] = $tmpType['name'];

				// Підготовка массива до відправки
				$tmp['clientId'] = $value['client_code'];
				$tmp['clientPhone'] = $value['client_tel'];
				$tmp['clientFullName'] = $value['client_name'];
				$tmp['filialCode'] = getPlaceCode($value['Place']);
				$tmp['materials'][0]['vendorCode'] = $value['CODE_MAT'];
				$tmp['materials'][0]['mc_id'] = $value['mc_id'];
				$tmp['materials'][0]['typeMaterial'] = $value['typeMaterial'];
				$tmp['materials'][0]['type'] = $value['type'];
				$tmp['materials'][0]['count'] = $value['Count'];
				$tmp['materials'][0]['thickness'] = $value['T'];
				$tmp['materials'][0]['name'] = $value['mat_name'];
				$tmp['materials'][0]['height'] = $value['L'];
				$tmp['materials'][0]['width'] = $value['W'];
				$tmp['materials'][0]['damage'] = ($value['crash'] === null) ? 0 : $value['crash'];
				$tmp['materials'][0]['description'] = isset($value['notation']) ? $value['notation'] : "0";

				$docId = $value['d_id'];
				// Відправка
				// $sendResponse = (array) json_decode(sendMaterials(json_encode($tmp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)));

				if ($sendResponse['success'] === true) {
					// Тут поки що тільки сбір ідентифікаторів
		            $apiIds = getApiPartIds($sendResponse['data']);

		            // $q = "UPDATE `MATERIAL_KL_STOCK_MOVE` SET `api_part_ids` = '" . $apiIds . "', `Client` = '" . $value['client_id'] . "' WHERE `Material_client` = " . $value['mc_id'] . " AND `Doc` = " . $docId . "";
					$res = sql_data(__LINE__,__FILE__,__FUNCTION__,$q);

					$entriesCount++;
					$partsCount += $value['Count'];

				} else {
					// Ну і якщо запит не пройшов:
					echo 'mc_id: ' . $value['mc_id'] . '<br>';
					echo 'Material code: ' . $value['CODE_MAT'] . '<br>';
					echo 'iteration: ' . $key . '<br>';
					echo '<pre>';
					print_r(json_encode($tmp, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
					echo '</pre>';
					echo '<br>';
					echo '<pre>';
					print_r($sendResponse);
					echo '</pre>';
					exit('Error');
				}

				$materialsReady[$key] = $tmp;
					
			}
			setSync($value['mc_id']);
		}
	}

	echo 'Records transferred successfully' . "<br>";
	echo 'Count of entries: <b>' . $entriesCount . "</b><br>";
	echo 'Count of parts: <b>' . $partsCount . "</b><br>";

} else {

	// clearSyncFields();

	echo 'Hello there!';
    exit();
}


?>