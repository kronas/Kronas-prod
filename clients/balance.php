<?php
session_start();
include_once('functions2.php');
include_once('func_api.php');
if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
}
?>

<?php
$title = 'Остатки';
include_once('header.php'); ?>
<?php
if (isset($_GET['details']) && isset($_GET['place']) && (int)$_GET['details'] > 0 && (int)$_GET['place'] > 0) {
    $_where = '';
    if ($_SESSION['user']['role'] == 'client') {
        $_where = ' DOC.Client IN 
                (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.tel = "' . $_SESSION['user']['phone'] . '" OR clnt.`e-mail` = "' . trim($_SESSION['user']['mail']) . '"))) AND ';
    }
    $balance = db_get_balance_details((int)$_GET['details'], (int)$_GET['place'], $_where);
    ?>
    <div class="row  align-items-center">
        <div class="col-md-1 col-sm-6 "><a href="balance.php">
                <div class="alert alert-dark" role="alert">
                    <<< назад
                </div>
            </a></div>
    </div>

    <?php
    if (count($balance) > 0) {
        $brief = $balance[0];
        $total = 0;
        foreach ($balance as $value) $total = $total + $value['Move'];
        ?>

        <div class="row  align-items-center">

            <div class="col-md-4 offset-md-4 col-sm-12 border bg-light">
                <table class="table table-striped mt-3">

                    <tbody>
                    <tr>
                        <th>Материал</th>
                        <td><?= $brief['Name'] ?></td>
                    </tr>
                    <tr>
                        <th>W</th>
                        <td><?= $brief['W'] ?></td>
                    </tr>
                    <tr>
                        <th>L</th>
                        <td><?= $brief['L'] ?></td>
                    </tr>
                    <tr>
                        <th>T</th>
                        <td><?= $brief['T'] ?></td>
                    </tr>
                    <tr>
                        <th>Склад</th>
                        <td><?= $brief['plcName'] ?></td>
                    </tr>
                    <tr>
                        <th>Остаток</th>
                        <td><?= $total ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row"><div class="col-12 pl-3 pr-3 mt-2">
        <table class="table table-striped">
        <thead>
        <tr>
            <?php if ($_SESSION['user']['role'] != 'client') echo '   <th>Клиент</th>'; ?>
            <th>№ документа</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Движение</th>
            <th>...</th>
        </tr>
        </thead><tbody>
        <?php
        foreach ($balance as $row) {
            echo '<tr>';
            if ($_SESSION['user']['role'] != 'client') echo '<td>' . $row['clName'] . '</td>';
            echo '
    <td>' . $row['Doc'] . '</td>
    <td>' . date('Y/m/d H:i', $row['Datetime']) . '</td>
    <td>' . $row['manName'] . '</td>
    <td>' . $row['Move'] . '</td>
    <td><a href="docs.php?doc=' . $row['Doc'] . '" title="Подробнее"><i class="fas fa-search"></i></a> </td>
        </tr>';
        }
        echo '</tbody></table>';


        echo '</div></div>';
    } else {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div></div>';
    }
} else {

    ?>
    <div class="row">
        <div class="col-sm-12">

            <form class="form balance" method="get" action="balance.php<?='?nw='.$_GET['nw']?>">


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="client_search_t" class="mr-4">Толщина остатка</label>
                            <input type="text" class="form-control" id="client_filtr_t" name="client_filtr_t" <?php if (isset($_GET['client_filtr_t'])) echo 'value="'.$_GET['client_filtr_t'].'"'?>>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="client_search_code" class="mr-4">CODE</label>
                            <input type="text" class="form-control" id="client_filtr_code" name="client_filtr_code" <?php if (isset($_GET['client_filtr_code'])) echo 'value="'.$_GET['client_filtr_code'].'"'?>>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="client_search_name" class="mr-4">Название</label>
                            <input type="text" class="form-control" id="client_filtr_name" name="client_filtr_name" <?php if (isset($_GET['client_filtr_name'])) echo 'value="'.$_GET['client_filtr_name'].'"'?>>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="client_search_id" class="mr-4">По ID материала</label>
                            <input type="text" class="form-control" id="client_search_id" name="client_search_id" <?php if (isset($_GET['client_search_id'])) echo 'value="'.$_GET['client_search_id'].'"'?>>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php if ($_SESSION['user']['role'] == 'manager' || $_SESSION['user']['role'] == 'admin') { ?>
                        <div class="form-group mx-sm-2 mb-2">
                            <label for="client_search_tel" class="mr-4">Телефон клиента</label>
                            <input type="text" class="form-control phone" id="client_filtr_tel" name="client_filtr_tel"
                                   data-mask="+380 (00) 000 00 00" placeholder="+380 (__) ___ __ __" <?php if (isset($_GET['client_filtr_tel'])) echo 'value="'.$_GET['client_filtr_tel'].'"'?>>
                        </div>
                    <div class="col-md-3">
                        <?php
                        $cells_list='<option value="" selected="">Выберите склад </option>';
                        $_selpl=0;
                        if (isset($_GET['select_places']) && (int) $_GET['select_places']>0)  $_selpl=(int) $_GET['select_places'];
                                    echo db_get_listPlaces(false, $_selpl, 2);
                       if($_selpl>0) {
                           $cells = db_get_cells($_selpl);
                           $cells_list = '';
                           $cells_list = '';
                           $cell_selected = 0;
                           if (isset($_GET['cells']) && (int)$_GET['cells'] > 0) $cell_selected = (int)$_GET['cells'];
                           $_tmp1 = 0;
                           foreach ($cells as $cell) {
                               $_tmp2 = '';
                               if ($cell['id'] == $cell_selected) {
                                   $_tmp1 = 1;
                                   $_tmp2 = ' selected=""';
                               }
                               $cells_list .= '<option value="' . $cell['id'] . '" ' . $_tmp2 . '>№' . $cell['id'] . ' (' . $cell['X'] . '*' . $cell['Y'] . '*' . $cell['Z'] . ')</option>';
                           }
                           $cells_list = '<option value="" ' . ($_tmp1 == 0 ? 'selected=""' : '') . '>Не указано</option>' . $cells_list;
                       }
                        ?>
                    </div>
                    <div class="form-group mx-sm-2 mb-2">
                        <div class="form-group" >
                            <label for="cells" >Наименование клиента</label >
                            <input type="text" class="form-control" name="client_filtr_title" <?php if (isset($_GET['client_filtr_title'])) echo 'value="'.$_GET['client_filtr_title'].'"'?>>
                        </div >
                    </div >

                    <?php } ?>
                    <div class="col-md-2 pt-4">
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="with_empty" name="with_empty" <?php if (isset($_GET['with_empty'])) echo 'checked'?>>
                            <label class="form-check-label" for="with_empty">Показать 0 остатки</label>
                        </div>
                    </div>

                    <div class="form-group mx-sm-3 mb-2 mt-3">
                        <button type="submit" class="btn btn-primary mb-2">Фильтровать</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php


    $total_records = 0;
    $filtr = array();
    $_url = 'balance.php?';
    if (isset($_GET['client_filtr_tel']) && strlen(trim($_GET['client_filtr_tel'])) == 17) {
        $filtr[] = ' stcnt.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.tel = "' . str_replace(' ', '', trim($_GET['client_filtr_tel'])) . '"))) ';
        $_url .= 'client_filtr_tel=' . urlencode(trim($_GET['client_filtr_tel'])) . '&';
    }
    if (isset($_GET['select_places']) && (int)$_GET['select_places'] > 0) {
        $filtr[] = ' stcnt.Place= ' . (int)$_GET['select_places'];
        $_url .= 'select_places=' . (int)$_GET['select_places'] . '&';
    }
    if (isset($_GET['cells']) && (int)$_GET['cells'] > 0) {
        $filtr[] = ' mccs.cell= ' . (int)$_GET['cells'];
        $_url .= 'cells=' . (int)$_GET['cells'] . '&';
    }
    if (isset($_GET['client_filtr_t']) && (int)$_GET['client_filtr_t'] > 0) {
        $filtr[] = ' mc.T= ' . (int)$_GET['client_filtr_t'];
        $_url .= 'client_filtr_t=' . (int)$_GET['client_filtr_t'] . '&';
    }
    if (isset($_GET['client_filtr_code']) && strlen(trim($_GET['client_filtr_code'])) > 0) {
        $filtr[] = ' mc.`CODE`=' . trim($_GET['client_filtr_code']);
        $_url .= 'client_filtr_code=' . urlencode(trim($_GET['client_filtr_code'])) . '&';
    }
    if (isset($_GET['client_filtr_name']) && strlen(trim($_GET['client_filtr_name'])) > 0) {
        $filtr[] = ' mc.`Name` LIKE "%' . trim($_GET['client_filtr_name']) . '%" ';
        $_url .= 'client_filtr_name=' . urlencode(trim($_GET['client_filtr_name'])) . '&';
    }
    if (isset($_GET['client_filtr_title']) && strlen(trim($_GET['client_filtr_title'])) > 0) {
        $filtr[] = ' stcnt.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.name LIKE "%' . $_GET['client_filtr_title'] . '%"))) ';
        $_url .= 'client_filtr_title=' . urlencode(trim($_GET['client_filtr_title'])) . '&';
    }
    if (isset($_GET['client_search_id']) && (int)$_GET['client_search_id'] > 0) {
        $filtr[] = ' mc.mc_id=' . (int)$_GET['client_search_id'] . ' ';
        $_url .= 'client_search_id=' . (int)$_GET['client_search_id'] . '&';
    }

    if (!isset($_GET['with_empty'])) {
        $filtr[] = ' stcnt.Count>0';

    }else{
        $_url .= 'with_empty=on&';
    }
    if ($_SESSION['user']['role'] == 'client') {
        $filtr[] = ' stcnt.Client IN (SELECT client.client_id FROM client WHERE client.`code` IN ((SELECT clnt.`code` 
                FROM client as clnt WHERE clnt.tel = "' . str_replace(' ', '', trim($_SESSION['user']['phone'])) . '" OR clnt.`e-mail` = "' . trim($_SESSION['user']['mail']) . '")))';

    }
    $limit = 50;
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $start = $page == 1 ? 0 : (($limit * ($page - 1)));
    $table = db_get_listbalance($filtr, $start, $limit, $total_records);
//    print_r_($table);
    if (is_null($table)) {
        echo '<div class="row  align-items-center"><div class="col-md-4 offset-md-4 col-sm-12 alert alert-warning" role="alert">Данных нет</div></div>';
    } else {
        echo '<div class="row"><div class="col-12 pl-3 pr-3">';
        echo '<table class="table table-striped balance"><thead>    <tr>
        <th>ID</th>
        <th>Склад</th>
        ' . ($_SESSION['user']['role'] == 'client' ? '' : '<th>Ячейка</th>') . '
        <th>Деталь</th>
        ' . ($_SESSION['user']['role'] == 'client' ? '' : '<th>Клиент</th>') . '
        <th>W</th>
        <th>L</th>
        <th>T</th>
        <th>Кол-во</th>
        <th>...</th>
    </tr></thead><tbody>';
        $_arr_cells=db_get_allCells();
        $r = 0;
        foreach ($table as $row) {
             // if( $row['mc_id'] == 2118){_pre($row); $r=1;}
            $_selectPlace='';
            if($row['Place']>0 && $row['cell']>0) $_selectPlace=createSelect($_arr_cells,$row['Place'], $row['cell'],$row['mc_id'],$row['Client']);
$_lnkPrint='';
$_lnkPrintNewService='';
        if ($_SESSION['user']['role'] == 'admin' || $_SESSION['user']['role'] == 'manager') $_lnkPrint='<a target="_blank" href="check.php?details=' . $row['mc_id'] . '&client=' . $row['Client'] . '" title="Печать наклейки"><i class="fas fa-print"></i></a>';
        
        if (isset($row['api_part_ids']) && !empty($row['api_part_ids']) && $_SESSION['user']['role'] == 'admin' || $_SESSION['user']['role'] == 'manager') {
            $printLink = getPrintMatLink($row['api_part_ids']);
            $_lnkPrintNewService = '<a target="_blank" href="' . $printLink . '" title="Печать наклейки в новом сервисе"><i class="fas fa-print" style="color: green"></i></a>';
        }

            echo '<tr id="' . $row['mc_id'] . '-' . $row['Place'] . '-' . $row['Client'] . '">
<td>' . $row['mc_id'] . '</td>
<td>' . $row['placeName'] . '</td>
' . ($_SESSION['user']['role'] == 'client' ? '' : '<td>' . $_selectPlace . '</td>') . '
<td>' . $row['matName'] . '</td>
' . ($_SESSION['user']['role'] == 'client' ? '' : '<td>' . $row['clName'] . '<br>' .$row['clTel']. '</td>') . '
<td>' . $row['W'] . '</td>
<td>' . $row['L'] . '</td>
<td>' . $row['T'] . '</td>
<td class="wop-td">' . $row['Count'] ;
if((int)$row['count_plan']>0) echo '<button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#wopWindow" 
 data-clcode="' . $row['clCODE'] . '" data-mcid="' . $row['mc_id'] . '" data-place="' . $row['Place']. '" data-rowID="' . $row['mc_id'] . '-' . $row['Place'] . '-' . $row['Client'] . '">(-' . (int)$row['count_plan'] . ')</button>';
echo '</td>
<td>
<a href="balance.php?details=' . $row['mc_id'] . '&place=' . $row['Place'] . '" title="Подробнее"><i class="fas fa-search"></i></a>  ' . $_lnkPrint . $_lnkPrintNewService . '
</td>
    </tr>';
        }
        echo '</tbody></table>';
        if ($total_records > $limit) echo get_list2browse_pager2($_url, $page, $limit, $total_records);
        echo '</div></div>';
    }
    ?>

<?php } ?>

    <div class="modal fade" id="wopWindow" tabindex="-1" role="dialog" aria-labelledby="wopWindowLabel" aria-hidden="true">
        <div class="modal-dialog " role="document" style="min-width: 80% !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="wopWindowLabel">Списание</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="modal-body-in">

                    </div>
                </div>
            </div>
        </div>
    </div>




<?php include_once('footer.php'); ?>