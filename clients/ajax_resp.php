<?php

include_once('functions2.php');

if(isset($_POST)) {

    if(isset($_POST['MaxDH']) && isset($_POST['MaxDL']) && isset($_POST['MinDH']) && isset($_POST['MinDL']) && isset($_POST['MaxWI'])
        && isset($_POST['ConSG']) && isset($_POST['ConSS']) && isset($_POST['ConGG'])) {
        db_change_setting_profile(trim($_POST['MaxDH']), trim($_POST['MaxDL']), trim($_POST['MinDH']), trim($_POST['MinDL']), trim($_POST['MaxWI']),
            trim($_POST['ConSG']), trim($_POST['ConSS']), trim($_POST['ConGG']), trim($_POST['MaxSanH']));
        $_out= array('result'=>true,'message'=>'Настройки сохранили!');
        $data = json_encode($_out);
        print $data;
        exit();
    }


    if(!empty($_POST['data'])) {
        $_out = array('result' => false, 'message' => 'ошибка pdata21');
        $in = json_decode($_POST['data']);
        $data=array();
        if (isset($in->limits)) {
            foreach ($in->limits as $key=>$value)
            {
                $data[$key]=$value;
            }
            db_update_settings($data);
            $_out = array('result' => true, 'message' => 'Настройки сохранены!');
        }
        if (isset($in->matID) && isset($in->clCODE) && isset($in->place)) {
            $_out = array('result' => false, 'message' => 'ошибка pdata2133333');
            foreach ($in->wof as $key=>$value)
            {
                $_out = array('result' => false, 'message' => '='.$value->id);

                                $docLines=db_get_planDocDetails($value->id);

                   if(count($docLines)==1)
                                {
                                    if($value->old==$value->wop)
                                    {
                                        db_erase_doc_move($value->id);
                                        db_erase_doc($value->id);
                                        db_erase_matcl_ord1c($in->matID, $docLines[0]['Order1C']);
                                        $_out = array('result' => true, 'message' => 'Документ и движение удалено!', 'target' => $in->rowID, 'resVal' => 0);
                                    }
                                    else
                                    {
                                        db_update_move_doc($value->id, ($value->old-$value->wop), $in->place, $in->matID );
                                        db_update_matcl_ord1c($in->matID, $docLines[0]['Order1C'], ($value->old-$value->wop));
                                        $_out = array('result' => true, 'message' => 'Движение обновлено!', 'target' => $in->rowID, 'resVal' => ($value->old-$value->wop));
                                    }
                                }
                                else
                                {
                                    db_update_move_doc($value->id, ($value->old-$value->wop), $in->place, $in->matID );
                                    foreach ($docLines as $dline){
                                        if($dline['Material_client']==$in->matID) {
                                            if($value->old==$value->wop)
                                            {
                                                db_erase_matcl_ord1c($in->matID, $dline['Order1C']);
                                            }
                                            else
                                                db_update_matcl_ord1c($in->matID, $dline['Order1C'], ($value->old-$value->wop));
                                        }
                                    }
                                    $_out = array('result' => true, 'message' => 'Движение обновлено!', 'target' => $in->rowID, 'resVal' => ($value->old-$value->wop));
                                }

            }

        }
        $data = json_encode($_out);
        print $data;
        exit();
    }
    if(isset($_POST['new_phone'])) {
        $data =get_phone_mail_is(str_replace(' ','', trim($_POST['new_phone'])),'phone');
        }
    if(isset($_POST['new_mail'])) {
        $data =get_phone_mail_is(strtoupper(trim($_POST['new_mail'])) ,'mail');
    }
    if(isset($_POST['client_search_tel'])) {
        $_out=get_client($_POST['client_search_tel']);
        if(is_null($_out))
            $_out= array('result'=>false);
        else {
            $_out['result'] = true;
            $_out['mail']=$_out['e-mail'];
            unset($_out['e-mail']);
        }
        $data = json_encode($_out);
    }
    if(isset($_POST['keyword'])) {
        $data=db_get_list4newdataOnWord($_POST['keyword'], $_POST['contID']);
    }
    if(isset($_POST['keyT'])) {
        $data=db_get_list4newdataOnT($_POST['keyT'], $_POST['contID']);
    }
    if(isset($_POST['selectCells'])) {


        $cells=db_get_cells($_POST['selectCells']);
        $data='<option value="" selected="">Не указано</option>';
        foreach($cells as $cell) $data.='<option value="'.$cell['id'].'" >№'.$cell['id'].' '.$cell['cell_name'].' ('.$cell['X'].'*'.$cell['Y'].'*'.$cell['Z'].')</option>';
    }
    // newCell='+newCell+'&uid='+uid+'&place='+place+'&mid='+mid,
    if(isset($_POST['newCell']) && isset($_POST['uid']) && isset($_POST['mid']) && isset($_POST['place'])) {
        $data=db_change_cells($_POST['newCell'],$_POST['place'],$_POST['uid'],$_POST['mid']);
    }
    if(isset($_POST['client_tel'])) {
        session_start();
        $tel = '+'.trim($_POST['client_tel']);
        $pin = send_client_pin($tel);
        if(!$pin) {
            $data = 'Указанного телефона нету в базе данных';
        } else {
            $_SESSION['client_pin'] = $pin;
            $data = 1;
        }
    }
    print $data;
}