<?php

/**
 * Скрипт принимает материалы клиента, поставленные на приход в новом сервисе
 */
include_once(__DIR__.'/../_1/config.php');
include_once(DIR_FUNCTIONS . 'fs_universal_queries.php');
include_once('functions2.php');
include_once('func_api.php');

$setting = load_ini();
$user_in_production = (int) $setting['misc']['user_in_production'];

$output = [
	'status' => 'false',
	'message' => ''
];

// Получаем JSON-данные из тела запроса
$json_data = file_get_contents('php://input');

// Парсим JSON-данные в массив
$incomingData = json_decode($json_data, true);

// Проверяем, удалось ли распарсить JSON
if ($incomingData === null) {
	// JSON некорректен
	$output['message'] = 'Bad json format';
} else {

	$headers = getallheaders();

	if (!isset($headers['clntlftvrsToken']) || $headers['clntlftvrsToken'] !== getToken(true)) { 

		$output['message'] = 'Не валідний токен.';
		
	} else {

		$validate = validateWriteOff($incomingData);

		if ($validate['status'] === false) {
			
			$output['message'] = $validate['message'];
			
		} else {

			// [return] => 1
		    // [cause_ret] => in_production
		    // [comment] => 
		    // [days_left] => 4
		    // [matCode] => 561561
		    // [clientId] => 849925
		    // [place] => 1
		    // [cell] => 1
		    // [old_count] => 5
		    // [part_ids] => 749,750,751,752,753
		    // [part_ids_checked] => 749

			$q = "SELECT DISTINCT
					cnt.Client,
					cnt.api_part_ids,
					cnt.Place,
					cnt.Move,
					mc.CODE_MAT,
					mccs.cell
				FROM `MATERIAL_CLIENT` AS mc
				LEFT JOIN `MATERIAL_KL_STOCK_MOVE` AS cnt ON cnt.Material_client = mc.mc_id
				LEFT JOIN `MATERIAL_CLIENT_CELL_STOCK` AS mccs ON mccs.material_client = mc.mc_id
				WHERE mc.mc_id = " . $incomingData['mc_id'] . "
					AND mc.CODE_MAT = " . getMaterialId($incomingData['vendorcode']) . "
					AND cnt.api_part_ids <> ''
					AND cnt.`Client` <> 313695";
			$res = sql_data(__LINE__,__FILE__,__FUNCTION__, $q);

			$r = $res['data'][0];
			
			$oldCount = count(explode(',', $r['api_part_ids']));

			$lineMaterial = [
				'return' => 1,
				'cause_ret' => 'in_production',
				'comment' => '',
				'days_left' => 4,
				'mc_id' => $incomingData['mc_id'],
				'matCode' => $r['CODE_MAT'],
				'clientId' => $r['Client'],
				'managerId' => getManagerId($incomingData['manager_ag_id']) !== null ? getManagerId($incomingData['manager_ag_id']) : getManagerId($incomingData['manager_id']) !== null ? getManagerId($incomingData['manager_id']) : 39,
				'place' => $r['Place'],
				'cell' => $r['cell'],
				'old_count' => $oldCount,
				'part_ids' => $r['api_part_ids'],
				'part_ids_checked' => $incomingData['id']
			];

			writeOffMaterialsNewService($lineMaterial, $user_in_production);

			$output['status'] = 'true';
			$output['message'] = 'Матеріал з ID ' . $incomingData['id'] . ' успішно списаний.';

			// Запись в файл
			// $filename = date('Y-m-d_H-i-s') . '_writeoff' . '.json';
			// $file_path = DIR_FILES . 'temp/api_clients_leftovers/' . $filename;
			// $json_string = json_encode($incomingData, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
			// file_put_contents($file_path, $json_string);

		}
	}
}

echo json_encode($output, JSON_PRETTY_PRINT);

?>