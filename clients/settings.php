<?php
session_start();

include_once(__DIR__.'/../_1/config.php');
include_once('functions2.php');
include_once('RS/functions3.php');
include_once('glass/functions.php');

if (!isset($_SESSION['user'])) {
    $_SESSION['messages']['errors'][] = 'Авторизуйтесь пожалуйста!';
    header('Location: ' . $main_dir . '/clients/login.php'.'?nw='.$_GET['nw']);
    exit();
} elseif($_SESSION['user']['role']!='admin'){
    $_SESSION['messages']['errors'][]='У вас не прав!';
    header('Location: ' . $main_dir . '/clients/index.php'.'?nw='.$_GET['nw']);
    exit();
}



$setting=load_ini(); // _pre($setting);
$arr_active=array(array('active','show active'),array('',''),array('',''),array('',''), array('', ''), array('', ''), array('', ''), array('', ''));
$placeID=0;
if(isset($_POST['submit']) && $_POST['submit'] == 1)
{
    $setting=load_ini();
    $setting['min4coming']['L']=$_POST['minL'];
    $setting['min4coming']['W']=$_POST['minW'];
    save_ini($setting);
    $_SESSION['messages']['success'][] = 'Настройки сохранены!';
}
if(isset($_POST['submit']) && $_POST['submit'] == 2)
{
    if ((int)$_POST['select_places'] > 0) {
        $placeID = (int)$_POST['select_places'];
        $listcells=db_get_cells($placeID);
    }
    $arr_active=array(array('',''),array('',''),array('active','show active'),array('',''), array('', ''), array('', ''), array('', ''), array('', ''));
}
if(isset($_POST['submit']) && $_POST['submit'] == 3)
{
    $placeID = (int)$_POST['placeID'];
    $countCells = (int)$_POST['countCells'];
    $arr_insert=array();
    $arr_update=array();
    $arr_erase=array();
    for($i=1;$i<$countCells;$i++)
    {
        if($_POST['cellsID'][$i]>0)
        {
            if(in_array($_POST['cellsID'][$i],$_POST['erase']))

                $arr_erase[]=$_POST['cellsID'][$i];
            else {
                $_validated=$_POST['cellsID'][$i].'|'.$_POST['size_x'][$i].'|'.$_POST['size_y'][$i].'|'.$_POST['size_z'][$i].'|'.$_POST['percent'][$i];
                if($_validated!=$_POST['validated'][$i]) $arr_update[] = "UPDATE ctock_cells SET cell_name = '{$_POST['cell_name'][$i]}', X = {$_POST['size_x'][$i]}, Y = {$_POST['size_y'][$i]}, Z = {$_POST['size_z'][$i]}, percents = {$_POST['percent'][$i]} WHERE id = {$_POST['cellsID'][$i]}; ";
            }
        }
        elseif($_POST['size_x'][$i]>0 && $_POST['size_y'][$i]>0 && $_POST['size_z'][$i]>0 && $_POST['percent'][$i]>0)
        {
            $arr_insert[]="({$placeID},'{$_POST['cell_name'][$i]}',{$_POST['size_x'][$i]},{$_POST['size_y'][$i]},{$_POST['size_z'][$i]},{$_POST['percent'][$i]})";
        }
    }
    if(count($arr_erase)>0) db_cells_erase($arr_erase);
    if(count($arr_update)>0) db_cells_update($arr_update);
    if(count($arr_insert)>0) db_cells_add($arr_insert);

    if(count($arr_erase)>0 || count($arr_update)>0 || count($arr_insert)>0) { $_SESSION['messages']['success'][] = 'Данные сохранились!'; $placeID=0;}

    $arr_active=array(array('',''),array('active','show active'),array('',''),array('',''), array('', ''), array('', ''), array('', ''));
}
if(isset($_POST['submit']) && $_POST['submit'] == 4) {
    $setting=load_ini();
    $setting['misc']['user_in_production']=$_POST['inproduction'];
    $setting['misc']['dirPic2print']=$_POST['dirPic2print'];
    $setting['misc']['dirPic2sanArtEm']=$_POST['dirPic2sanArtEm'];
//    _pre($setting['misc']);
    if(save_ini($setting)) {
        $_SESSION['messages']['success'][] = 'Настройки сохранены!';
    }else{
        $_SESSION['messages']['errors'][] = 'Ошибка записи файла настроек! Обратитесь к администратору!';
    }
    $arr_active = array(array('', ''), array('', ''), array('', ''), array('active', 'show active'), array('', ''), array('', ''), array('', ''), array('', ''));
}
$title='Настройки';
include_once('header.php');

$setting_profile = db_get_setting_profile_full();
?>


<div class="row">
    <div class="col-md-12">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link <?=$arr_active[0][0]?>" id="nav-minIn-tab" data-toggle="tab" href="#nav-minIn" role="tab" aria-controls="nav-minIn" aria-selected="true">Минимум для "Приход"</a>
            <a class="nav-item nav-link <?=$arr_active[1][0]?>" id="nav-cells-tab" data-toggle="tab" href="#nav-cells" role="tab" aria-controls="nav-cells" aria-selected="false">Ячейки</a>
            <a class="nav-item nav-link <?=$arr_active[2][0]?>" id="nav-cells-tab" data-toggle="tab" href="#nav-profiles" role="tab" aria-controls="nav-profiles" aria-selected="false">Профили</a>
            <a class="nav-item nav-link <?=$arr_active[3][0]?>" id="nav-cells-tab" data-toggle="tab" href="#nav-misc" role="tab" aria-controls="nav-misc" aria-selected="false">Разное</a>
            <a class="nav-item nav-link <?=$arr_active[4][0]?>" id="nav-glass-tab" data-toggle="tab" href="#nav-glass" role="tab" aria-controls="nav-glass" aria-selected="false">Стеколка</a>
            <a class="nav-item nav-link <?=$arr_active[5][0]?>" id="nav-materials-tab" data-toggle="tab" href="#nav-materials" role="tab" aria-controls="nav-materials" aria-selected="false">Материалы</a>
            <a class="nav-item nav-link <?=$arr_active[6][0]?>" id="nav-band-tab" data-toggle="tab" href="#nav-band" role="tab" aria-controls="nav-band" aria-selected="false">Кромка</a>
            <a class="nav-item nav-link <?=$arr_active[7][0]?>" id="nav-plinth-tab" data-toggle="tab" href="#nav-plinth" role="tab" aria-controls="nav-plinth" aria-selected="false">Кухонный плинтус</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade <?=$arr_active[0][1]?>" id="nav-minIn" role="tabpanel" aria-labelledby="nav-minIn-tab">
            <div class="row">
                <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Минимум для "Приход"</h3></div>
                <div class="col-md-6 offset-md-3 col-sm-12">
                    <form method="post">
                        <div class="form-group row">
                            <label for="minL" class="col-sm-6 text-right col-form-label">Минимум для "длина вдоль текстуры"(L)</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control onlyNumber" id="minL" name="minL" value="<?=$setting['min4coming']['L']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="minW" class="col-sm-6 text-right col-form-label">Минимум для "ширина"(W)</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control onlyNumber" id="minW" name="minW" value="<?=$setting['min4coming']['W']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" name="submit" value="1" class="btn btn-success"><i class="far fa-save"></i> Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="tab-pane fade <?=$arr_active[1][1]?>" id="nav-cells" role="tabpanel" aria-labelledby="nav-cells-tab">
            <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Ячейки</h3></div>
<?php if ($placeID == 0) { ?>
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-12">
            <form method="post">
                <?= db_get_listPlaces(false, 0) ?>
                <div class="col-md-12 text-center">
                    <button type="submit" name="submit" class="btn btn-success mb-2" value="2"><i
                                class="fas fa-check"></i> Выбрать
                    </button>
                </div>
            </form>
        </div>
    </div>
<?php }else{
$countCells=2;
?>
            <div class="row">
                <div class="col-md-12">
                    <form method="post" class="list-cells">
                        <table class="table table-striped"><thead><tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>X</th>
                                <th>Y</th>
                                <th>Z</th>
                                <th>%</th>
                                <th>...</th>
                            </tr></thead><tbody>
                            <?php
                            function createLine($tr_propetys, $required,$defVal=array('','','','','','','',''))
                            {
                                $_num='*';
                                if($defVal[0]==0)
                                    $erase='<span class="cell-erase-tr" ><i class="close-sel" > x</i > Удалить</span >';
                                else
                                {
                                    if($defVal[6]==0) {
                                        $erase = '<input class="form-check-input" type="checkbox" id="erase_' . $defVal[0] . '" name="erase[]" value="' . $defVal[0] . '">
                                                      <label class="form-check-label" for="erase_' . $defVal[0] . '">Удалить</label>';
                                    }
                                    else
                                    {
                                        $erase = 'На складе ' . $defVal[6] . ' шт.';
                                    }
                                    $_num=$defVal[0];
                                }
                                $defVal2=$defVal; unset($defVal2[6]);
                                $dateLine='<tr '.$tr_propetys.'>
                                <td><input type="hidden" name="cellsID[]" value="'.$defVal[0].'" >'.$_num.'<input type="hidden" name="validated[]" value="'.implode('|',$defVal2).'" ></td>
                                <td><input type = "text" class="form-control" value="'.$defVal[1].'" name = "cell_name[]" ></td>
                                <td><input type = "text" class="form-control onlyNumber" value="'.$defVal[2].'" name = "size_x[]" '.$required.' ></td>
                                <td><input type = "text" class="form-control onlyNumber" value="'.$defVal[3].'" name = "size_y[]" '.$required.' ></td>
                                <td><input type = "text" class="form-control onlyNumber" value="'.$defVal[4].'" name = "size_z[]" '.$required.' ></td>
                                <td><input type = "number" max="100" class="form-control onlyNumber" value="'.$defVal[5].'" name ="percent[]" '.$required.' ></td>
                                <td align="center">'.$erase.'</td>
                            </tr>';
                                return $dateLine;
                            }
                            echo createLine(' id="template" style="display:none;" ','');

                            if(count($listcells)>0)
                            {
                                foreach ($listcells as $line)
                                {
                                    $countCells++;
                                    echo createLine('',' required="required" ',array( $line['id'], $line['cell_name'], $line['X'], $line['Y'], $line['Z'], $line['percents'], $line['cnt']));
                                }
                            }
                            else
                            echo createLine('',' required="required" ');
                            ?>
                            </tbody></table>
                        <input type="hidden" id="countCells" name="countCells" value="<?=$countCells?>" >
                        <input type="hidden" id="placeID" name="placeID" value="<?=$placeID?>" >
                        <div class="row">
                            <div class="col-4 text-center ">
                                <button type="button" name="addLine" id="addLine" class="btn btn-primary"><i class="far fa-calendar-plus"></i> Еще</button>
                            </div>
                            <div class="col-4  text-center ">
                                <button type="submit" name="submit" value="3" class="btn btn-success"><i class="far fa-save"></i> Сохранить</button>
                            </div>
                            <div class="col-4  text-center ">
                                <button type="submit" name="submit" value="4" class="btn btn-warning"><i class="fas fa-times"></i> Отмена</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php } ?>
    </div>
        <div class="tab-pane fade <?=$arr_active[2][1]?>" id="nav-profiles" role="tabpanel" aria-labelledby="nav-profiles-tab">
            <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Профили</h3></div>
            <form method="post" id="profiles">
            <div class="row">
                <div class="col-md-4 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Профиль</div>
                        <div class="card-body text-dark">

                            <div class="row">
                                <div class="form-group col-12">
                                    <select class="custom-select m-1" id="profile_list" name="profile_list">
                                        <?= db_profile_list_options()  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 messages-profile">

                                </div>
                            </div>
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-typeop="1"
                                    data-target="#selectData"
                                    data-targetid="0">Добавить
                            </button>
                            <button type="button" disabled class="btn btn-sm btn-primary" data-toggle="modal" data-typeop="2"  id="profile_edit"
                                    data-target="#selectData"
                                    data-targetid="0">Редактировать
                            </button>
                            <button type="submit" disabled class="btn btn-sm btn-danger" value="erase_proflie" id="erase_proflie">Удалить</button>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Декор</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="form-group col-12 forprofileDecor">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 messages-decor">

                                </div>
                            </div>
                            <button type="button" disabled class="btn btn-sm btn-success" data-toggle="modal" data-typeop="3"  id="add_decor"
                                    data-target="#selectData"
                                    data-targetid="0">Добавить
                            </button>
                            <button type="button" disabled class="btn btn-sm btn-primary" data-toggle="modal" data-typeop="3"  id="edit_decor"
                                    data-target="#selectData"
                                    data-targetid="0" data-decortid="0">Редактировать
                            </button>

                            <button type="submit" disabled class="btn btn-sm btn-danger"  value="erase_decor" name="erase_decor" id="erase_decor">Удалить</button>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Щетка с зажимом</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="form-group col-12 forprofileSchetka">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-10">
                                    <div class="form-group schetka" >
                                        <div class="spinner-border text-primary loading-search" role = "status" >
                                            <span class="sr-only" > Loading...</span >
                                        </div >
                                        <input type = "text" class="form-control searchSimple" name = "schetka" data-sel1="0" >
                                        <input type="hidden" name="schetka_val" value="">
                                        <div class="search-box" ></div >
                                    </div >
                                </div>
                                <div class="form-group col-2">
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" name="schetka_glue" id="schetka_glue">
                                        <label class="form-check-label" for="schetka_glue">Клеевая</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 messages-schetka">

                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary" value="add_schetka" name="add_schetka" id="add_schetka">Связать с профилем</button>
                            <button type="submit" class="btn btn-sm btn-danger" value="remove_schetka" name="remove_schetka" id="remove_schetka">Отвязять от профиля</button>
                            <button type="submit" class="btn btn-sm btn-primary glue-setting-schetka d-none" value="glue_set" name="glue_set" id="glue_set">Сделать клеевой</button>
                            <button type="submit" class="btn btn-sm btn-danger glue-setting-schetka d-none" value="glue_del" name="glue_del" id="glue_del">Сделать не клеевой</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Доводчик</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="form-group col-12 forprofileDovodchik">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <div class="form-group dovodchik" >
                                        <div class="spinner-border text-primary loading-search" role = "status" >
                                            <span class="sr-only" > Loading...</span >
                                        </div >
                                        <input type = "text" class="form-control searchSimple" name = "dovodchik" data-sel1="0" >
                                        <input type="hidden" name="dovodchik_val" value="">
                                        <div class="search-box" ></div >
                                    </div >

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 messages-dovodchik">

                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary" value="add_dovodchik" name="add_dovodchik" id="add_dovodchik">Связать с профилем</button>
                            <button type="submit" class="btn btn-sm btn-danger" value="remove_dovodchik" name="remove_dovodchik" id="remove_dovodchik">Отвязять от профиля</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Ролик</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="form-group col-12 forprofileRolick">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <div class="form-group rolick" >
                                        <div class="spinner-border text-primary loading-search" role = "status" >
                                            <span class="sr-only" > Loading...</span >
                                        </div >
                                        <input type = "text" class="form-control searchSimple" name = "rolick" data-sel1="0" >
                                        <input type="hidden" name="rolick_val" value="">
                                        <div class="search-box" ></div >
                                    </div >

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 messages-rolick">

                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary" value="add_rolick" name="add_rolick" id="add_rolick">Связать с профилем</button>
                            <button type="submit" class="btn btn-sm btn-danger" value="remove_rolick" name="remove_rolick" id="remove_rolick">Отвязять от профиля</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Стопор</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="form-group col-12 forprofileStopor">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <div class="form-group stopor" >
                                        <div class="spinner-border text-primary loading-search" role = "status" >
                                            <span class="sr-only" > Loading...</span >
                                        </div >
                                        <input type = "text" class="form-control searchSimple" name = "stopor" data-sel1="0" >
                                        <input type="hidden" name="stopor_val" value="">
                                        <div class="search-box" ></div >
                                    </div >

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 messages-stopor">

                                </div>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary" value="add_stopor" name="add_stopor" id="add_stopor">Связать с профилем</button>
                            <button type="submit" class="btn btn-sm btn-danger" value="remove_stopor" name="remove_stopor" id="remove_stopor">Отвязять от профиля</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Настройки</div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="container-setting-profile">
                                        <table class="table table-striped">
                                            <tbody>
                                            <?php

                                            foreach ($setting_profile as $sp_line)
                                            {
                                                echo '<tr><td>'.$sp_line['name_rus'].'</td><td>
                                            <input type="text" class="form-control" name="'.$sp_line['name'].'" value="'.$sp_line['value'].'"></td></tr>';
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <button type="submit" class="btn mt-4 btn-success " value="save_limits" name="save_limits" id="save_limits">Сохранить</button>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-12 messages-limits">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="tab-pane fade <?=$arr_active[3][1]?>" id="nav-misc" role="tabpanel" aria-labelledby="nav-misc-tab">
            <div class="row">
                <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Разное</h3></div>
                <div class="col-md-6 offset-md-3 col-sm-12">
                    <form method="post">
                        <div class="form-group row">
                            <label for="inproduction" class="col-sm-6 text-right col-form-label">ID клиента "производство"</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control onlyNumber" id="inproduction" name="inproduction" value="<?=$setting['misc']['user_in_production']?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dirPic2print" class="col-sm-6 text-right col-form-label">Каталог с изображением для печати</label>
                            <div class="col-sm-6">
                            <input type="text" class="form-control" id="dirPic2print" name="dirPic2print" value="<?=$setting['misc']['dirPic2print']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dirPic2sanArtEm" class="col-sm-6 text-right col-form-label">Каталог с изображением для печати пескоструя/художественного снятия амальгамы</label>
                            <div class="col-sm-6">
                            <input type="text" class="form-control" id="dirPic2sanArtEm" name="dirPic2sanArtEm" value="<?=$setting['misc']['dirPic2sanArtEm']?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" name="submit" value="4" class="btn btn-success"><i class="far fa-save"></i> Сохранить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--    start /nav-glass    -->
        <div class="tab-pane fade <?=$arr_active[4][1]?>" id="nav-glass" role="tabpanel" aria-labelledby="nav-glass-tab">
            <div >
                <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Стеколка</h3></div>
                <?php
                $settings = glass_get_settings_without_bore();
                ?>
                <div class="col-sm-12 d-flex justify-content-center mb-3">
                    <div class="card border-dark col-sm-9 p-0">
                        <div class="card-header">Каталоги для изображений</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-7"  for="path_to_print"><p><?= $settings['path_to_print']['name_rus'] ?></p></label>
                                <input type="text" class="form-control col-sm-5" name="path_to_print" value="<?= $settings['path_to_print']['value'] ?>">
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-7"  for="path_to_sandblasting"><?= $settings['path_to_sandblasting']['name_rus'] ?></label>
                                <input type="text" class="form-control col-sm-5" name="path_to_sandblasting" value="<?= $settings['path_to_sandblasting']['value'] ?>">
                            </div>
                            <div class="messages-for-pic"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="pathPicSubmit">Сохранить</button>
                        </div>
                    </div>
                </div>

                <div class="card-deck mb-2">
                    <div class="card border-dark col-sm-6 p-0">
                        <div class="card-header">Диаметры отверстий</div>
                        <div class="card-body text-dark">
                            <div class="form-group col-12 pr-2">
                                <select class="custom-select m-1" id="holeList" name="hole_list">
                                    <option> --- </option>
                                    <?= glass_get_holes_list_options() ?>
                                </select>
                            </div>
                            <div class="form-group col-12 row d-none" id="editHole">
                                <label class="col-sm-12" for="glass_d_bore">Диаметр, мм</label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control" name="glass_d_bore">
                                </div>
                                <button class="btn btn-primary" id="sendData">Ok</button>
                            </div>
                            <div class="col-12 messages-hole">

                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-around">
                            <button type="button" class="btn btn-sm btn-success mr-2" id="btnHoleAdd" data-post="add_diameter">Добавить
                            </button>
                            <button type="button" disabled class="btn btn-sm btn-primary mr-2" id="btnHoleEdit" data-post="edit_diameter">Редактировать
                            </button>
                            <button type="submit" disabled class="btn btn-sm btn-danger mr-2" id="btnHoleDelete">Удалить</button>
                        </div>
                    </div> <!-- end /diameter -->
                    <div class="card border-dark">
                        <div class="card-header"><?= $settings['min_size_for_edge']['name_rus'] ?></div>
                        <div class="card-body text-dark">
                            <div class="form-group  d-flex justify-content-center">
                                <input type="number" class="form-control col-sm-6" name="min_size_for_edge" value="<?= $settings['min_size_for_edge']['value'] ?>">
                            </div>
                            <div class="message-for-min-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="minSizeSubmit">Сохранить</button>
                        </div>
                    </div> <!-- end /min size for treatment edges -->
                    <div class="card border-dark">
                        <div class="card-header"><?= $settings['min_size_for_edge_with_rounded_corner']['name_rus'] ?></div>
                        <div class="card-body text-dark">
                            <div class="form-group d-flex justify-content-center">
                                <input type="number" class="form-control col-sm-6" name="min_size_for_edge_r" value="<?= $settings['min_size_for_edge_with_rounded_corner']['value'] ?>">
                            </div>
                            <div class="message-for-min-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="minSizeSubmitRad">Сохранить</button>
                        </div>
                    </div> <!-- end /min size for treatment edges with rounded corner -->
                    <div class="card border-dark">
                        <div class="card-header"><?= $settings['min_size_for_zarez']['name_rus'] ?></div>
                        <div class="card-body text-dark">
                            <div class="form-group d-flex justify-content-center">
                                <input type="number" class="form-control col-sm-6" name="min_size_for_zarez" value="<?= $settings['min_size_for_zarez']['value'] ?>">
                            </div>
                            <div class="message-for-min-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="minSizeSubmitZarez">Сохранить</button>
                        </div>
                    </div> <!-- end /min sizes for zarez -->
                </div>
                <div class="card-deck mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Максимальные размеры детали</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_length_for_edge"><p><?= $settings['max_length']['name_rus'] ?></p></label>
                                <input type="number" class="form-control col-sm-3" name="max_length_for_edge" value="<?= $settings['max_length']['value'] ?>" prevval="<?= $settings['max_length']['value'] ?>">
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_width_for_edge"><?= $settings['max_width']['name_rus'] ?></label>
                                <input type="number" class="form-control col-sm-3" name="max_width_for_edge" value="<?= $settings['max_width']['value'] ?>" prevval="<?= $settings['max_width']['value'] ?>">
                            </div>
                            <div class="message-for-max-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="maxDetailSize">Сохранить</button>
                        </div>
                    </div> <!-- end /max size of a detail -->

                    <div class="card border-dark">
                        <div class="card-header">Максимальная ширина фацета</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="to_4mm"><p><?= $settings['to_4mm']['name_rus'] ?></p></label>
                                <input type="number" class="form-control col-sm-3" name="to_4mm" value="<?= $settings['to_4mm']['value'] ?>" prevval="<?= $settings['to_4mm']['value'] ?>">
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="over_4mm"><?= $settings['over_4mm']['name_rus'] ?></label>
                                <input type="number" class="form-control col-sm-3" name="over_4mm" value="<?= $settings['over_4mm']['value'] ?>" prevval="<?= $settings['over_4mm']['value'] ?>">
                            </div>
                            <div class="message-for-max-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="maxWidthFacet">Сохранить</button>
                        </div>
                    </div> <!-- end /max width facet -->
                </div>
                <div class="card-deck mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Максимальные размеры детали для обработки торцов</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_length_for_edge"><p><?= $settings['max_length_for_edge']['name_rus'] ?></p></label>
                                <input type="number" class="form-control col-sm-3" name="max_length_for_edge" value="<?= $settings['max_length_for_edge']['value'] ?>" prevval="<?= $settings['max_length_for_edge']['value'] ?>">
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_width_for_edge"><?= $settings['max_width_for_edge']['name_rus'] ?></label>
                                <input type="number" class="form-control col-sm-3" name="max_width_for_edge" value="<?= $settings['max_width_for_edge']['value'] ?>" prevval="<?= $settings['max_width_for_edge']['value'] ?>">
                            </div>
                            <div class="message-for-max-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="maxSizeForTreatment">Сохранить</button>
                        </div>
                    </div> <!-- end /max size for treatment edges -->
                    <div class="card border-dark">
                        <div class="card-header">Максимальные размеры детали для обработки торцов при скруглении углов</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_length_for_edge_with_rounded_corner"><p><?= $settings['max_length_for_edge_with_rounded_corner']['name_rus'] ?></p></label>
                                <input type="number" class="form-control col-sm-3" name="max_length_for_edge_with_rounded_corner" value="<?= $settings['max_length_for_edge_with_rounded_corner']['value'] ?>" prevval="<?= $settings['max_length_for_edge_with_rounded_corner']['value'] ?>>
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_width_for_edge_with_rounded_corner"><?= $settings['max_width_for_edge_with_rounded_corner']['name_rus'] ?></label>
                                <input type="number" class="form-control col-sm-3" name="max_width_for_edge_with_rounded_corner" value="<?= $settings['max_width_for_edge_with_rounded_corner']['value'] ?>" prevval="<?= $settings['max_width_for_edge_with_rounded_corner']['value'] ?>">
                            </div>
                            <div class="message-for-max-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="maxSizeForTreatmentR">Сохранить</button>
                        </div>
                    </div> <!-- end /max size for edge with rounded corner -->
                </div>
                <div class="card-deck mb-2">
                    <div class="card border-dark">
                        <div class="card-header">Максимальные размеры детали для фотопечати</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_length_for_photo_print"><p><?= $settings['max_length_for_photo_print']['name_rus'] ?></p></label>
                                <input type="number" class="form-control col-sm-3" name="max_length_for_photo_print" value="<?= $settings['max_length_for_photo_print']['value'] ?>" prevval="<?= $settings['max_length_for_photo_print']['value'] ?>">
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_width_for_photo_print"><?= $settings['max_width_for_photo_print']['name_rus'] ?></label>
                                <input type="number" class="form-control col-sm-3" name="max_width_for_photo_print" value="<?= $settings['max_width_for_photo_print']['value'] ?>" prevval="<?= $settings['max_width_for_photo_print']['value'] ?>">
                            </div>
                            <div class="message-for-max-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="maxSizeForPrint">Сохранить</button>
                        </div>
                    </div> <!-- end /max size for photo print -->
                    <div class="card border-dark">
                        <div class="card-header">Максимальные размеры детали для пескоструя</div>
                        <div class="card-body text-dark">
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_length_for_sandblasting"><p><?= $settings['max_length_for_sandblasting']['name_rus'] ?></p></label>
                                <input type="number" class="form-control col-sm-3" name="max_length_for_sandblasting" value="<?= $settings['max_length_for_sandblasting']['value'] ?>" prevval="<?= $settings['max_length_for_sandblasting']['value'] ?>">
                            </div>
                            <div class="form-group row pr-2">
                                <label class="col-sm-9" for="max_width_for_sandblasting"><?= $settings['max_width_for_sandblasting']['name_rus'] ?></label>
                                <input type="number" class="form-control col-sm-3" name="max_width_for_sandblasting" value="<?= $settings['max_width_for_sandblasting']['value'] ?>" prevval="<?= $settings['max_width_for_sandblasting']['value'] ?>">
                            </div>
                            <div class="message-for-max-size"></div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-success justify-content-end" id="maxSizeForSandblasting">Сохранить</button>
                        </div>
                    </div> <!-- end /max size for edge with rounded corner -->
                </div>
            </div>
        </div> <!--    end /nav-glass    -->
        <!-- start /nav-materials -->
        <div class="tab-pane fade <?=$arr_active[5][1]?>" id="nav-materials" role="tabpanel" aria-labelledby="nav-materials-tab">
            <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Материалы</h3></div>
            <dib class="nav nav-tabs" id="mainMaterialsTab">
                <a class="nav-link active" id="mat-default-tab" data-toggle="tab" href="#mat-default" role="tab" aria-controls="mat-default" aria-selected="true"><i class="fas fa-cog"></i> Значения по умолчанию</a>
                <a class="nav-link" id="selling-principle-tab" data-toggle="tab" href="#selling-principle" role="tab" aria-controls="selling-principle" aria-selected="false"><i class="fas fa-handshake"></i> Принцип продажи</a>
            </dib>
            <div class="tab-content" id="matTabContent">
                <!-- start /mat-default  -->
                <div class="tab-pane fade show active" id="mat-default" role="tabpanel" aria-labelledby="mat-default-tab">
                    <?php
                    $mat_gen = mat_get_material_general();
                    ?>
                    <form class="row justify-content-center" id="formMatGeneral">
                        <div class="form-group col-sm-8 row m-3">
                            <lable class="col-sm-8 col-form-label"><?= $mat_gen[0]['description'] ?>, в мм</lable>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" name="<?= $mat_gen[0]['name'] ?>" value="<?= $mat_gen[0]['value_dig'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-8 row m-3">
                            <lable class="col-sm-8 col-form-label"><?= $mat_gen[1]['description'] ?>, в мм</lable>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" name="<?= $mat_gen[1]['name'] ?>" value="<?= $mat_gen[1]['value_dig'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-8 row m-3">
                            <lable class="col-sm-8 col-form-label"><?= $mat_gen[2]['description'] ?>, в мм</lable>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" name="<?= $mat_gen[2]['name'] ?>" value="<?= $mat_gen[2]['value_dig'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-8 row m-3">
                            <lable class="col-sm-8 col-form-label"><?= $mat_gen[3]['description'] ?>, в мм</lable>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" name="<?= $mat_gen[3]['name'] ?>" value="<?= $mat_gen[3]['value_dig'] ?>">
                            </div>
                        </div>
                        <div class="form-group col-sm-8 row m-3">
                            <lable class="form-check-label col-sm-8"><?= $mat_gen[4]['description'] ?> (да/нет)</lable>
                            <div class="col-sm-4">
                                <input type="checkbox" class="form-check-input checkbox-r" name="<?= $mat_gen[4]['name'] ?>" <?php if ($mat_gen[4]['value_bool']) echo 'checked'; ?>>
                            </div>
                        </div>
                        <div class="form-group col-sm-8 row m-3">
                            <lable class="form-check-label col-sm-8"><?= $mat_gen[5]['description'] ?> (да/нет)</lable>
                            <div class="col-sm-4">
                                <input type="checkbox" class="form-check-input checkbox-r" name="<?= $mat_gen[5]['name'] ?>" <?php if ($mat_gen[5]['value_bool']) echo 'checked'; ?>>
                            </div>
                        </div>
                        <div class="col-sm-8 m-3" id="messageMatGeneral"></div>
                        <div class="col-sm-8 m-3 d-flex justify-content-center">
                            <button class="btn btn-success" id="submitMatGeneral"><i class="fas fa-check"></i> Изменить</button>
                        </div>
                    </form>
                </div> <!--  end /mat-default -->
                <!--  start /selling-principle -->
                <div class="tab-pane fade" id="selling-principle" role="tabpanel" aria-labelledby="selling-principle-tab">
                    <div class="row mt-4">
                        <div class="col-sm-3">
                            <div class="input-group-sm mb-3 d-flex flex-row">
                                <label class="w-50" for="places">Выберете филиал</label>
                                <select class="custom-select" id="places" name="place_id">
                                    <?= mat_get_places_options() ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group-sm mb-3 d-flex flex-row">
                                <label class="w-50" for="salePrinciple">Принцип продажи</label>
                                <select class="custom-select" id="salePrinciple" name="salePrinciple">
                                    <option value="material">Продажа листами</option>
                                    <option value="half">Продажа половинками</option>
                                    <option value="m2">Продажа м2</option>
                                    <option value="m2_diff">Продажа м2 от размера детали</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button id="btnOpenFilter" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                                Фильтр
                            </button>
                        </div>
                        <div class="col-sm-3 d-flex flex-row justify-content-around align-items-baseline">
                            <button id="btnAddMat" class="btn btn-success d-none" type="button" data-toggle="modal" data-target="#modalAddMat" aria-expanded="false">
                                Установить
                            </button>
                            <button id="chooseAll" class="btn btn-info d-none ml-3" type="button">
                                Выделить все
                            </button>
                        </div>

                        <!-- start/ collapseFilter -->
                        <div class="collapse col-sm-12" id="collapseFilter">
                            <h6 class="text-center">Выборка материалов</h6>
                            <div class="card card-body d-flex flex-row flex-wrap justify-content-around">
                                <div class="col-sm-3">
                                    <button id="btnOpenTree" type="button" class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseTree" aria-expanded="false" aria-controls="collapseTree">
                                        Выборка по каталогу
                                    </button>
                                    <div id="matFolder"></div>
                                </div>
                                <div class="collapse col-sm-6" id="collapseTree" style="position: absolute; z-index: 999;">
                                    <div class="card card-body" id="matTree">

                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#modalPropertiesFilter">Выбрать по свойству</button>
                                </div>
                                <div class="col-sm-3 input-group mb-3 align-items-start">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">По коду</span>
                                    </div>
                                    <input type="number" class="form-control form-control" placeholder="CODE" name="code" id="filterCode">
                                </div>
                                <div class="col-sm-3 input-group mb-3 align-items-start">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">По характеристике</span>
                                    </div>
                                    <input type="number" class="form-control" placeholder="MY_1C_HAR" name="har" id="filterHar">
                                </div>
                            </div>
                        </div> <!-- end/ collapseFilter -->
                        <div id="countMat" class="col-sm-12 h5 text-info">Найдено <span></span> записей</div>
                        <div id="tableMat" class="col-sm-12" style="max-height: 600px; overflow: auto;">

                        </div>
                    </div> <!-- end /row -->

                </div> <!--  end /selling-principle -->
            </div>
        </div> <!--  end /nav-materials -->
        <!-- start /nav-band -->
        <div class="tab-pane fade <?=$arr_active[6][1]?>" id="nav-band" role="tabpanel" aria-labelledby="nav-band-tab">
            <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Кромка</h3></div>
            <div class="tab-content" id="bandTabContent">
                <div class="row mt-4 justify-content-end">
                    <div class="col-sm-12" id="filterBand">
                        <h6 class="text-center">Фильтр кромки</h6>
                        <div class="card card-body d-flex flex-row flex-wrap justify-content-around">
                            <div class="col-sm-3">
                                <button id="btnOpenTreeBand" type="button" class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseTreeBand" aria-expanded="false" aria-controls="collapseTreeBand">
                                    Выборка по каталогу
                                </button>
                                <div id="bandFolder"></div>
                            </div>
                            <div class="collapse col-sm-6" id="collapseTreeBand" style="position: absolute; z-index: 999;">
                                <div class="card card-body" id="bandTree">

                                </div>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn btn-success" data-toggle="modal" data-target="#modalPropertiesFilterBand">Выбрать по свойству</button>
                            </div>
                            <div class="col-sm-3 input-group mb-3 align-items-start">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">По коду</span>
                                </div>
                                <input type="number" class="form-control form-control" placeholder="CODE" name="code" id="bandFilterCode">
                            </div>
                            <div class="col-sm-3 input-group mb-3 align-items-start">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">По наименованию</span>
                                </div>
                                <input type="text" class="form-control" name="name" id="bandFilterName">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 mt-3 mb-3 d-flex flex-row justify-content-end align-items-baseline">
                        <button id="btnAddBand" class="btn btn-success mr-3 d-none" type="button" data-toggle="modal" data-target="#modalAddBand" aria-expanded="false">
                            Установить
                        </button>
                        <button id="chooseAllBand" class="btn btn-info d-none" type="button">
                            Выделить все
                        </button>
                    </div>
                    <div id="quantityBands" class="col-sm-12 h5 text-info">Найдено <span id="countBands"></span> записей</div>
                    <div id="tableBand" class="col-sm-12" style="max-height: 600px; overflow: auto;">
                    </div>
                </div> <!-- end /row -->
            </div>
        </div> <!--  end /nav-band -->
        <!-- start /nav-plinth -->
        <div class="tab-pane fade <?=$arr_active[7][1]?>" id="nav-plinth" role="tabpanel" aria-labelledby="nav-plinth-tab">
            <div class="col-md-6 offset-md-3 col-sm-12"><h3 class="text-center col-12 mb-3">Кухонный плинтус</h3></div>
            <div class="tab-content" id="plinthTabContent">
                <div class="row mt-4 justify-content-end">
                    <div class="col-sm-12" id="filterPlinth">
                        <h6 class="text-center">Фильтр для плинтуса</h6>
                        <div class="card card-body d-flex flex-row flex-wrap justify-content-around">
                            <div class="col-sm-3">
                                <button id="btnOpenTreePlinth" type="button" class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseTreePlinth" aria-expanded="false" aria-controls="collapseTreePlinth">
                                    Выборка по каталогу
                                </button>
                                <div id="plinthFolder"></div>
                            </div>
                            <div class="collapse col-sm-6" id="collapseTreePlinth" style="position: absolute; z-index: 999;">
                                <div class="card card-body" id="plinthTree">

                                </div>
                            </div>

                            <div id="plinthBtnProperties" class="col-sm-3">
                                <button class="btn btn-success" data-toggle="modal" data-target="#modalPropertiesFilterPlinth">Выбрать по свойству</button>
                            </div>
                            <div class="col-sm-3 input-group mb-3 align-items-start">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">По коду</span>
                                </div>
                                <input type="number" class="form-control form-control" placeholder="CODE" name="code" id="plinthFilterCode">
                            </div>
                            <div class="col-sm-3 input-group mb-3 align-items-start">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">По наименованию</span>
                                </div>
                                <input type="text" class="form-control" name="name" id="plinthFilterName">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 mt-3 mb-3 d-flex flex-row justify-content-end align-items-baseline">
                        <button id="btnAddPlinth" class="btn btn-success mr-3 d-none" type="button" data-toggle="modal" data-target="#modalAddPlinth" aria-expanded="false">
                            Установить
                        </button>
                        <button id="chooseAllPlinth" class="btn btn-info d-none" type="button">
                            Выделить все
                        </button>
                    </div>
                    <div id="quantityPlinths" class="col-sm-12 h5 text-info">Найдено <span id="countPlinths"></span> записей</div>
                    <div id="tablePlinth" class="col-sm-12" style="max-height: 600px; overflow: auto;">
                    </div>
                </div> <!-- end /row -->
            </div>
        </div> <!--  end /nav-plinth -->
    </div>

    </div>
</div>
<div class="modal fade" id="selectData" tabindex="-1" role="dialog" aria-labelledby="selectDataLabel"
     aria-hidden="true">
    <div class="modal-dialog " role="document" style="min-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="selectDataLabel">Профиль</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="modal-body-in">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal for adding materials -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalAddMat" tabindex="-1" role="dialog" aria-labelledby="modalAddMatLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавленные материалы</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <p class="h6">Список выбранных материалов</p>
                </div>
                <ul class="list-group" id="listChooseMat" style="max-height: 150px; overflow: auto;">

                </ul>
                <form id="modalFormAddMat" class="mt-3">
                    <div class="form-group">
                        <label class="w-100" for="modalSelectPlaces">Выберете филиал</label>
                        <select name="places[]" class="form-control" id="modalSelectPlaces" multiple size="9">
                            <?= mat_get_places_options()?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="w-100" for="modalSalePrincipal">Выберете принцип продажи</label>
                        <select class="form-control" name="sale_principal" id="modalSalePrincipal">
                            <option value="material">Продажа листами</option>
                            <option value="half">Продажа половинками</option>
                            <option value="m2">Продажа м2</option>
                            <option value="m2_diff">Продажа м2 от размера детали</option>
                        </select>
                    </div>

                    <div id="forM2Diff" class="row d-none">
                        <label class="ml-3">Введите параметры наценки выбранных материалов</label>
                        <div id="messageM2Diff"></div>
                        <button id="m2DiffAddInputs" class="btn-sm btn-success m-auto"><i class="fas fa-plus"></i> Добавить способ наценки</button>
                        <table class="table table-bordered m-3">
                            <thead>
                            <tr>
                                <th >Длина, мм</th>
                                <th scope="col">Ширина, мм</th>
                                <th scope="col">Площадь, м<sup>2</sup></th>
                                <th scope="col">Процент увеличения</th>
                                <th scope="col">Увеличение в гривнах</th>
                            </tr>
                            </thead>
                            <tbody id="m2DiffTbody">
                            <tr id="m2DiffRow1">
                                <td><input class="form-control" type="number" name="length"></td>
                                <td><input class="form-control" type="number" name="width"></td>
                                <td><input class="form-control" type="number" name="area"></td>
                                <td><input class="form-control" type="number" name="up_percent"></td>
                                <td><input class="form-control" type="number" name="up_uah"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="modalAddMatClose" type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" id="btnConfAddMat">Добавить</button>
            </div>
        </div>
    </div>
</div> <!-- end/ modal for adding materials -->
<!-- start/ modal for properties filter(material) -->
<div class="modal fade" id="modalPropertiesFilter" tabindex="-1" role="dialog" aria-labelledby="modalPropertiesFilter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Свойства</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <?= mat_get_properties_inputs('mat') ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" id="btnProperties">Применить</button>
            </div>
        </div>
    </div>
</div> <!-- end/ modal for properties filter(material) -->
<!-- start/ modal for properties filter(Band) -->
<div class="modal fade" id="modalPropertiesFilterBand" tabindex="-1" role="dialog" aria-labelledby="modalPropertiesFilterBand" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Свойства</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <?= mat_get_properties_inputs('band') ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" id="btnPropertiesBand">Применить</button>
            </div>
        </div>
    </div>
</div> <!-- end/ modal for properties filter(band) -->
<!-- start/ modal for properties filter(Plinth) -->
<div class="modal fade" id="modalPropertiesFilterPlinth" tabindex="-1" role="dialog" aria-labelledby="modalPropertiesFilterPlinth" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Свойства</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <?= mat_get_properties_inputs('plinth') ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" id="btnPropertiesPlinth">Применить</button>
            </div>
        </div>
    </div>
</div> <!-- end/ modal for properties filter(Plinth) -->
<!-- modal for adding band -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalAddBand" tabindex="-1" role="dialog" aria-labelledby="modalAddBandLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group" id="listChooseBand" style="max-height: 150px; overflow: auto;">

                </ul>
                <div id="modalFormAddBand" class="mt-3">
                    <div class="row">
                        <button id="bandBtnAddMat" class="btn-sm btn-success m-auto" data-toggle="collapse" data-target="#collapseMaterial" aria-expanded="false" aria-controls="collapseMaterial"><i class="fas fa-plus"></i> Добавить материал</button>
                        <div class="collapse col-sm-12" id="collapseMaterial">
                            <div class="card card-body mt-3">
                                <!-- start block filter for materials -->
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <button class="input-group-text btn btn-light" id="sendFilterMat">Фильтр материалов</button>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Введите параметры, по которым хотите отфильтровать список материалов" name="filter_mat" id="filterMatForBand">
                                </div> <!-- end block filter for materials -->
                                <div class="count-mat"><span class="count-text"></span></div>
                                <ul class="list-group" id="collapseMatList" style="max-height: 150px; overflow: auto;">

                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12" >
                            <table id="bandMaterialsTable" class="table table-hover mt-3">
                                <thead>
                                <tr>
                                    <th>Артикул</th>
                                    <th scope="col">Название</th>
                                    <th scope="col">Тип соответствия</th>
                                    <th scope="col">Входит в стоимость</th>
                                    <th scope="col">Продается с этой кромкой</th>
                                    <th scope="col">Удалить</th>
                                </tr>
                                </thead>
                                <tbody style="max-height: 400px; overflow: auto;">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="modalAddMatClose" type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary d-none" id="btnAddMatToBands">Добавить</button>
            </div>
        </div>
    </div>
</div> <!-- end/ modal for adding band -->
<!-- modal for adding plinth -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalAddPlinth" tabindex="-1" role="dialog" aria-labelledby="modalAddPlinthLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group" id="listChoosePlinth" style="max-height: 150px; overflow: auto;">

                </ul>
                <div id="modalFormAddPlinth" class="mt-3">
                    <div class="row">
                        <button id="plinthBtnAddMat" class="btn-sm btn-success m-auto" data-toggle="collapse" data-target="#collapseMaterialP" aria-expanded="false" aria-controls="collapseMaterialP"><i class="fas fa-plus"></i> Добавить столешницу</button>
                        <div class="collapse col-sm-12" id="collapseMaterialP">
                            <div class="card card-body mt-3">
                                <!-- start block filter for materials -->
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <button class="input-group-text btn btn-light" id="sendFilterMatP">Фильтр материалов</button>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Введите параметры, по которым хотите отфильтровать список материалов" name="filter_mat" id="filterMatForPlinth">
                                </div> <!-- end block filter for materials -->
                                <div class="count-mat"><span class="count-text"></span></div>
                                <ul class="list-group" id="collapseMatListP" style="max-height: 150px; overflow: auto;">

                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12" >
                            <table id="plinthMaterialsTable" class="table table-hover mt-3">
                                <thead>
                                <tr>
                                    <th>Артикул</th>
                                    <th scope="col">Название</th>
                                    <th scope="col">Тип соответствия</th>
                                    <th scope="col">Входит в стоимость</th>
                                    <th scope="col">Продается с этим плинтусом</th>
                                    <th scope="col">Удалить</th>
                                </tr>
                                </thead>
                                <tbody style="max-height: 400px; overflow: auto;">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="modalAddMatClose" type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary d-none" id="btnAddMatToPlinths">Добавить</button>
            </div>
        </div>
    </div>
</div> <!-- end/ modal for adding plinth -->
<?php include_once('footer.php'); ?>
<script>
    function list4searchSimple(sid, stringval, objectName) {
        $("input[name='"+objectName+"']").val(stringval);
        $("input[name='"+objectName+"_val']").val(sid);
        // console.log(s);
        //$("."+objectName+" input").val('ss');
        $("."+objectName+" .search-box").hide();
        $("."+objectName+" .loading-search").hide();
        // profile_form_ready_copy();
    }
</script>