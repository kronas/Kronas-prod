<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 18:47
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/_1/config.php');

require_once $serv_main_dir.'/PHPMailer/Exception.php';
require_once $serv_main_dir.'/PHPMailer/PHPMailer.php';
require_once $serv_main_dir.'/PHPMailer/SMTP.php';

global $mail_params;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//function p_($data) {
//    echo '<pre>';
//    print_r($data);
//    echo '</pre>';
//}

function save_upload_file($upload_file,$ticket_id, $upload_sub_dir) {

    $upload_dir = $GLOBALS['serv_main_dir'].'/helpdesc/uploads/'.$ticket_id.'/'.$upload_sub_dir.'/';

    mkdir($upload_dir, 0777, true);

    $upload_file = $upload_dir .mt_rand(111111111,999999999). basename($upload_file);

    $info = new SplFileInfo($upload_file);

    $file_url = str_replace($GLOBALS['serv_main_dir'], $GLOBALS['main_dir'], $upload_file);

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $upload_file)) {
        $sql = 'INSERT INTO `ticket_assets` (`ticket_id`, `file`, `name`) VALUES ('.$ticket_id.', "'.$file_url.'", "'.$_FILES['userfile']['name'].'")';
        $result = set_data($sql);
        if($result == 'complete') {
            header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
        }
    } else {
        echo "Возможная атака с помощью файловой загрузки!\n";
    }
}

function get_mpr_details_connect($order_id, $project_id) {
    $sql_project_data = 'SELECT * FROM `PROJECT` WHERE `PROJECT_ID` = '.$project_id;
    $project = get_data($sql_project_data)[0];

    //..23.11.2020 Читаем из файла, а не из базы:
    if ($p_data = get_Project_file($project['PROJECT_ID'])) {
        $project_data = $p_data['project_data'];
    } else {
        $project_data = base64_decode($project['MY_PROJECT_OUT']);
    }

    $file=$project_data;



    $p = xml_parser_create();
    xml_parse_into_struct($p, $file, $vals, $index);
    xml_parser_free($p);
    $vals=vals_out($vals);
    $index=make_index($vals);
    $vals=add_tag_print($vals);

    $order1c=$order_id;

    $sql='SELECT * FROM ticket_construct_mpr WHERE ORDER1C='.$order1c;
    $mpr=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];

    foreach($index['PART'] as $i)
    {
        if ($vals[$i]['attributes']['CL']>0)
        {
            if (!isset($vals[$i]['attributes']['T']))$vals[$i]['attributes']['T']=18;
            $part[$vals[$i]['attributes']['ID']]=['t'=>$vals[$i]['attributes']['T'],'cl'=>$vals[$i]['attributes']['CL'],'cw'=>$vals[$i]['attributes']['CW'],
            'dl'=>$vals[$i]['attributes']['DL'],'dw'=>$vals[$i]['attributes']['DW'],
            'jl'=>$vals[$i]['attributes']['JL'],'jw'=>$vals[$i]['attributes']['JW'],
            'code'=>$vals[$i]['attributes']['part.code'],
            'name'=>$vals[$i]['attributes']['NAME'],
            'id'=>$vals[$i]['attributes']['ID'],
            'count'=>$vals[$i]['attributes']['COUNT'],
            
        ];
        }
    }
// p_($part);
// p_($mpr);
    $mpr_to_part = [];

    foreach ($part as $id => $p) {
        foreach ($mpr as $m) {
            if (
                (($p['cl'] == $m['L']) && ($p['cw'] == $m['W']) && ($m['by_size'] == 1))
                OR
                (($p['cl'] == $m['W']) && ($p['cw'] == $m['L']) && ($m['by_size'] == 1))
                OR
                (($p['jl'] == $m['L']) && ($p['jw'] == $m['W']) && ($m['by_size'] == 1))
                OR
                (($p['jl'] == $m['W']) && ($p['jw'] == $m['L']) && ($m['by_size'] == 1))
                OR
                (($p['dl'] == $m['L']) && ($p['dw'] == $m['W']) && ($m['by_size'] == 0))
                OR
                (($p['dl'] == $m['W']) && ($p['dw'] == $m['L']) && ($m['by_size'] == 0))
            ) {
                foreach ($index['OPERATION'] as $i) {
                    if (($vals[$i]['attributes']['TYPEID'] == 'XNC') && ($vals[$i]['attributes']['operation.my_mpr_' . $m['mpr_id']] == 1)) {
                        $j = $i;
                        while ($vals[$j]['tag'] <> "PART") {
                            $j++;
                        }
                        if ($vals[$j]['attributes']['ID'] == $id) {
                            $mpr_to_part[$m['mpr_id']] = $id;
                        }

                    }
                }
            }
        }
    }

    $mpr_connect = [
        'mpr' => $mpr,
        'part' => $part,
        'vals' => $vals,
        'index' => $index,
        'project_data' => $project_data,
        'mpr_to_part' => $mpr_to_part
    ];
// p_($mpr_connect);

    return $mpr_connect;
}
function auth_client($client_ids) {
    $sql = 'SELECT * FROM `client` WHERE `client_id` = '.$client_ids;
    $client_data = get_data($sql);
    if(isset($client_data[0])) {
        $client_data = $client_data[0];
        $_SESSION['user'] = [
            'ID' => $client_data['client_id'],
            'name' => $client_data['name'],
            'phone' => $client_data['tel'],
            'mail' => $client_data['e-mail'],
            'role' => 'client',
            'code' => $client_data['code']

        ];
        return 1;
    } else {
        return 0;
    }
}
function auth_manager($ids) {
    $sql = 'SELECT * FROM `manager` WHERE `id` = '.$ids;
    $manager_data = get_data($sql);
    if(isset($manager_data[0])) {
        $manager_data = $manager_data[0];
        $role = $manager_data['admin'] == 1 ? 'admin' : 'manager';
        $_SESSION['user'] = [
            'ID' => $manager_data['id'],
            'name' => $manager_data['name'],
            'phone' => $manager_data['phone'],
            'mail' => $manager_data['e-mail'],
            'role' => $role,
            'code' => $manager_data['code']

        ];
        return 1;
    } else {
        return 0;
    }
}
function send_mail_report($subject, $text, $email) {

    $new_subject = $subject;
    $new_body_text = $text;
    $new_recipient_email = $email;

	$mail_params = $GLOBALS['mail_params'];
    $mail = new PHPMailer;
    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
	
	// 07.06.2021 ANDR0
	// Корректировки про настройкам почты
	$mail->isSMTP();
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = $mail_params['Host'];
    $mail->Port = $mail_params['Port']; // typically 587
    $mail->SMTPSecure = $mail_params['SMTPSecure']; // ssl is depracated
    $mail->SMTPAuth = $mail_params['SMTPAuth'];
    $mail->Username = $mail_params['Username'];
    $mail->Password = $mail_params['Password'];

    $mail->setFrom($mail_params['From'], 'HELPDESC-info');
	$mail->addAddress($new_recipient_email, '');
    $mail->AddCC('gibservice.support@kronas.com.ua', 'GibService Support');	

    $mail->Subject = $new_subject;

    $mail->msgHTML($new_body_text); // remove if you do not want to send HTML email

//    $mail->send();
    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    }

//    if (!is_array($file))
//    {
//        $file_name=mt_rand(0,1000000).'.'.$ext;
//        file_put_contents($file_name,$file);
//        $mail->addAttachment($file_name); //Attachment, can be skipped
//    }
//    else
//    {
//        unset($r);
//        foreach ($file as $n=>$f)
//        {
//            $file_name=$n.'_'.mt_rand(0,1000000).'.'.$ext;
//            $r[]=$file_name;
//            file_put_contents($file_name,$f);
//            $mail->addAttachment($file_name); //Attachment, can be skipped
//        }
//    }


//    if (!isset($r)) unlink($file_name);
//    else
//    {
//        foreach ($r as $file_name)
//        {
//            unlink($file_name);
//        }
//    }

}
