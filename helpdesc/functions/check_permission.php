<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 21.06.2020
 * Time: 22:18
 */

if(!isset($_SESSION['user'])) {
    header('Location: '.$main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
}