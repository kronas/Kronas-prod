<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 18:04
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/_1/config.php');

include_once($serv_main_dir . '/func.php');
include_once($serv_main_dir . '/func_math.php');

function get_data($sql) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        $result_array = [];
        for($i = 0; $i < $rows; ++$i) {
            $result_array[] = mysqli_fetch_assoc($result);
        }
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
    }
    mysqli_close($link);
    return $result_array;
}

function get_data_count($sql) {
    $link = db_connect();
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    if ($result = mysqli_query($link, $sql)) {
        $rows = mysqli_num_rows($result);
        mysqli_free_result($result);
    } else {
        echo 'Запрос '.$sql.' не выполнен!';
    }
    mysqli_close($link);
    return $rows;
}

function get_client($id) {
    $sql = 'SELECT * FROM `client` WHERE `client_id` = '.$id;
    $client = get_data($sql);
    return $client[0];
}

function get_manager($id) {
    $sql = 'SELECT * FROM `manager` WHERE `id` = '.$id;
    $manager = get_data($sql);
    return $manager[0];
}

function ticket_data_transform($ticket) {

    if($ticket['client']) {
        $client_db = get_client($ticket['client']);
        $client = [
            'id' => $client_db['client_id'],
            'name' => $client_db['name'],
            'email' => $client_db['e-mail'],
            'phone' => $client_db['tel'],
        ];
    } else {
        $client['name'] = 'Неизвестно';
    }

    if($ticket['manager']) {
        $manager_db = get_manager($ticket['manager']);
        $manager = [
            'id' => $manager_db['id'],
            'name' => $manager_db['name'],
            'phone' => $manager_db['phone'],
            'email' => $manager_db['e-mail']
        ];
    } else {
        $manager['name'] = 'Неизвестно';
    }

    $status = 'Не указан';

    switch($ticket['status']) {
        case 'новый':
            $status = '<span class="badge badge-pill badge-success">Новый</span>';
            break;
        case 'в работе':
            $status = '<span class="badge badge-pill badge-info">В работе</span>';
            break;
        case 'закрыт':
            $status = '<span class="badge badge-pill badge-danger">Закрыт</span>';
            break;
        case 'на согласовании':
            $status = '<span class="badge badge-pill badge-primary">На согласовании</span>';
            break;
    }

    return [
        'id' => $ticket['id'],
        'client' => $client,
        'manager' => $manager,
        'order_id' => $ticket['order_id'] ? $ticket['order_id'] : 'Неизвестно',
        'status' => $status,
        'type' => $ticket['type'],
        'date_created' => $ticket['date_created'],
        'date_updated' => $ticket['date_updated'],
        'date_closed' => $ticket['date_closed']
    ];
}

function get_tickets($user, $post, $pager = 1) {

    $filter = '';

    if(isset($post['main_filter']) && !empty($post['main_filter'])) {

        if($user['role'] == 'client') {
            $filter .= 'AND ';
        }

        if($user['role'] == 'manager') {
            $filter .= 'WHERE ';
        }

        switch($post['main_filter']) {
            case 'all':
                $filter = '';
                break;
            case 'my':
                $filter .= '`manager` = "'.$user['id'].'" ';
                break;
            case 'new':
                $filter .= '`status` = "новый" ';
                break;
            case 'close':
                $filter .= '`status` = "закрыт" ';
                break;
            case 'work':
                $filter .= '`status` = "в работе" ';
                break;
            case 'accept':
                $filter .= '`status` = "на согласовании" ';
                break;

        }
    }


    if($user['role'] == 'manager') {
        $sql = 'SELECT * FROM `ticket` 
                '.$filter;
    }
    if($user['role'] == 'client') {
        $sql = 'SELECT * FROM `ticket` 
                WHERE `client` = '.$user['id'].' '
                .$filter;  /// Поставить нормальный ид клиента
    }

    $ticket_per_page = 9;

    $ticket_counter = get_data_count($sql);

    if($pager > 1) {
        $limits = 'LIMIT '.$ticket_per_page.' OFFSET '.($pager-1)*$ticket_per_page;
    } else {
        $limits = 'LIMIT '.$ticket_per_page.' OFFSET 0';
    }

    $sql .= 'ORDER BY `date_created` DESC '.$limits;

    $tickets_array = [];
    $tickets_db = get_data($sql);
    $page_count = $ticket_counter / $ticket_per_page;
    if($page_count != round($page_count, 0)) {
        $page_count = round($page_count, 0) + 1;
    }

    foreach($tickets_db as $ticket) {



        $tickets_array[] = ticket_data_transform($ticket);
    }

    return [
        'data' => $tickets_array,
        'pages' => $page_count
    ];

}

function get_ticket($id) {
    if($_SESSION['user']['role'] == 'client') {
        $sql = 'SELECT * FROM `ticket` WHERE `id` = '.$id.' AND `client` = '.$_SESSION['user']['ID'];
    }
    else if($_SESSION['user']['role'] == 'manager' || $_SESSION['user']['role'] == 'admin') {
        $sql = 'SELECT * FROM `ticket` WHERE `id` = '.$id;
    } else {
        return [];
    }

    $ticket = get_data($sql);

    $ticket = ticket_data_transform($ticket[0]);

    return $ticket;

}

function get_ticket_messages($id) {
    $sql = 'SELECT * FROM `ticket_message` WHERE `ticket_id` = '.$id;
    $messages_db = get_data($sql);
    $messages = [];
    foreach ($messages_db as $message) {

        if($message['author_role'] == 'client') {
            $author = get_client($message['author']);
        }

        if($message['author_role'] == 'manager') {
            $author = get_manager($message['author']);
        }

        $messages[] = [
            'id' => $message['id'],
            'ticket_id' => $message['ticket_id'],
            'author' => [
                'id' => $message['author_role'] == 'client' ? $author['client_id'] : $author['id'],
                'name' => $author['name'],
                'author_role' => $message['author_role']
            ],
            'date' => $message['date'],
            'message' => $message['message']
        ];
    }
    return $messages;
}

function get_ticket_projects($id, $order_id, $manager_id) {
    $sql = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = '.$order_id.' ORDER BY `DATE` DESC';
    $projects_db = get_data($sql);
    foreach($projects_db as $project) {
        //..23.11.2020 Читаем из файла
        if ($p_data = get_Project_file($project['PROJECT_ID'])) {
            $MPO = base64_encode($p_data['project_data']);
        //.. Если нет, тогда из базы:
        } else {
            $MPO = $project['MY_PROJECT_OUT'];
        }
        if($project['manager_id'] != NULL && $project['manager_id'] == $manager_id) {
            $projects['manager'][] = [
                'id' => $project['PROJECT_ID'],
                'date' => $project['DATE'],
                'manager_id' => $project['manager_id'],
                'project' => $MPO
            ];
        } else {
            $projects['client'][] = [
                'id' => $project['PROJECT_ID'],
                'date' => $project['DATE'],
                'client_id' => $project['client_id'],
                'project' => $MPO
            ];
        }
    }
//    print_r($projects);
    return $projects;
}

function get_ticket_assets($id) {
    $sql = 'SELECT * FROM `ticket_assets` WHERE `ticket_id` = '.$id;
    $assets_db = get_data($sql);
    return $assets_db;
}

function set_data($sql) {
    $link = db_connect();

    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($link, "utf8");
    if (mysqli_query($link, $sql)) {
        $status = 'complete';
        $_SESSION['last_insert_id'] = mysqli_insert_id($link);
    } else {
        echo 'Запрос '.$sql.' не выполнен!<hr>'.mysqli_error($link);
        $status = 'error';
    }

    mysqli_close($link);

    return $status;

}

function get_managers() {
    $sql = 'SELECT * FROM `manager` WHERE `admin` = 1';
    $manager_db = get_data($sql);
    return $manager_db;
}

function get_order($id) {
    $sql = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$id;
    $order = get_data($sql)[0];
    return $order;
}

function get_ticket_mpr($id) {
    $sql = 'SELECT * FROM `ticket_construct_mpr` WHERE `ticket_id` = '.$id;
    $mpr_db = get_data($sql);

    $mpr_all_data = [];

    foreach ($mpr_db as $m) {
        $sql_mpr_prices = 'SELECT * FROM `ticket_mpr_price` as `m` LEFT JOIN `SERVICE` as `s` ON `m`.`service_id` = `s`.`SERVICE_ID` WHERE `m`.`mpr_id` = '.$m['mpr_id'];
        $sql_prices = get_data($sql_mpr_prices);
        $m['prices'] = $sql_prices;
        $mpr_all_data[] = $m;
    }
    return $mpr_all_data;
}
function get_mpr($id) {
    $sql = 'SELECT * FROM `ticket_construct_mpr` WHERE `mpr_id` = '.$id;
    $mpr_db = get_data($sql);

    $mpr_all_data = [];

    foreach ($mpr_db as $m) {
        $sql_mpr_prices = 'SELECT * FROM `ticket_mpr_price` as `m` LEFT JOIN `SERVICE` as `s` ON `m`.`service_id` = `s`.`SERVICE_ID` WHERE `m`.`mpr_id` = '.$m['mpr_id'];
        $sql_prices = get_data($sql_mpr_prices);
        $m['prices'] = $sql_prices;
        $mpr_all_data[] = $m;
    }
    return $mpr_all_data;
}
function get_services_array() {
    $sql_get_services = 'SELECT * FROM `SERVICE` WHERE `SERVICE_TYPE` IN (3,4)';
    $get_services = get_data($sql_get_services);
    return $get_services;
}
/**
* //..30.10.2020
* Выборка данных по части вводимого текста
* Параметры: таблица->колонка->часть значения
* Возвращает массив
**/
function searchString($table, $field, $value){
    $query_1 = 'SELECT * FROM '.$table.' WHERE '.$field.' LIKE "'.$value.'%"';
    $query_2 = 'SELECT * FROM '.$table.' WHERE '.$field.' LIKE "%'.$value.'%"';
    $result = get_data($query_1);
    if (!$result && $table !== 'ticket') {
        $result = get_data($query_2);
    }
    return $result;
}
// Выполнение SQL запросов
function runQuery($sql){
    $res = [];
    $connect = db_connect();
    if ($result = mysqli_query($connect, $sql)) {
        $ret = true;
    } else $ret = false;
    mysqli_close($connect);
    return $ret;
}