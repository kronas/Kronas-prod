<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 19.06.2020
 * Time: 15:20
 */
session_start();
include_once '../_1/config.php';
if(isset($_SESSION['user'])) {
    header('Location: '.$serv_main_dir.'/helpdesc/'.'?nw='.$_GET['nw']);
}
include_once('templates/header.php');

?>

<div class="container main_login_container">
            <div class="card">
                <article class="card-body">
                    <h4 class="card-title text-center mb-4 mt-1">Авторизация</h4>
                    <hr>
                    <?php if(isset($_SESSION['login_error'])): ?>
                    <p class="text-danger text-center"><?= $_SESSION['login_error'] ?></p>
                    <?php endif; ?>
                    <form action="./api/login.php<?='?nw='.$_GET['nw']?>" method="POST">
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="user_manager" name="user_type" class="custom-control-input" value="manager" checked>
                                <label class="custom-control-label" for="user_manager">Обработчик/Менеджер</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="user_client" name="user_type" class="custom-control-input" value="client">
                                <label class="custom-control-label" for="user_client" >Клиент</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                </div>
                                <input name="email" class="form-control" placeholder="E-mail" type="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                </div>
                                <input class="form-control" placeholder="******" type="password" name="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block" name="login" value="1"> Вход  </button>
                        </div>
<!--                        <p class="text-center"><a href="#" class="btn">Забыли пароль?</a></p>-->
                    </form>
                </article>
            </div>
</div>



<?php include_once('templates/footer.php'); ?>

<?php unset($_SESSION['login_error']);  ?>