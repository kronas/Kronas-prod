//.. Функция для фильтров главной страницы и страницы отчёта
$( function() {
    let baseUrl = window.location.origin+window.location.pathname;
    //.. Автодополнение номеров запросов (фильтр тикетов)
    $( "#ticketnums-menu" ).autocomplete({
        source: function(request, response){
            let part_of_string = {
                table:      'ticket',
                field:      'id',
                request:    request.term
            };
            $.ajax({
                url: 'api/filters_handler.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    part_of_string
                },
                success: function (data) {
                    response(data);
                }
            });
        }
    });
    //.. Автодополнение обработчиков
    $( "#assistant-menu" ).autocomplete({
        source: function(request, response){
            let part_of_string = {
                table:      'manager',
                field:      'name',
                request:    request.term
            };
            $.ajax({
                url: 'api/filters_handler.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    part_of_string
                },
                success: function (data) {
                    response(data);
                }
            });
        }
    });

    //.. Автодополнение клиентов
    $( "#client-menu" ).autocomplete({
        source: function(request, response){
            let part_of_string = {
                table:      'client',
                field:      'name',
                request:    request.term
            };
            $.ajax({
                url: 'api/filters_handler.php',
                method: 'POST',
                dataType: 'json',
                data: {
                    part_of_string
                },
                success: function (data) {
                    response(data);
                }
            });
        }
    });
    //.. Клик по кнопке "Сбросить фильтры" (тикеты)
    $('#clearfilters').bind('click', function() {
        let param = 'clearAll';
        setGet(param,0);
        // console.log(window.location.href);
    })
    //.. Выбор номера тикета (тикеты)
    $('#ticketnums-menu').bind('change', function() {
        let filters = new Object, param = 'ticketnum';
        filters[param] = $(this).val();
        filters['baseUrl'] = baseUrl;
        $.post("api/filters_handler.php", {filters}).done(function (data) {
            // console.log(JSON.parse(data));
            if (data) setGet(param,data);
        });
    });
    //.. Выбор обработчика
    // Когда сменяяется обработчик, то подгружаются и данные по тикетам данного обработчика
    $('#assistant-menu').bind('change', function() {
        let filters = new Object, param = 'assistant';
        filters['assistant'] = $(this).val();
        filters['baseUrl'] = baseUrl;
        // Если фильтры отчетов
        if (baseUrl.indexOf('otchet.php') != -1) {
            fetch('api/filters_handler.php', {method: 'POST'})
            .then((res) => {
                $.post("api/filters_handler.php", {filters}).done(function (data) {
                    $('#ticketsTable').html(data);
                });
            })
            .then((res) => { 
                $.post("api/filters_handler.php", {ticketCount: filters['assistant']}).done(function (data) {
                    $('#tickets-line').html(data);
                });
            })
            .catch(function(err) {  
                console.log('Fetch Error :-S', err);  
            });
        // Если фильтры тикетов
        } else {
            $.post("api/filters_handler.php", {filters}).done(function (data) {
                // console.log(JSON.parse(data));
                if (data) setGet(param,data);
            });
        }
    });
    //.. Выбор клиента (отчет)
    $('#client-menu').bind('change', function() {
        let filters = new Object, param = 'client';
        filters['client'] = $(this).val();
        filters['baseUrl'] = baseUrl;
        // Если фильтры отчетов
        if (baseUrl.indexOf('otchet.php') != -1) {
            $.post("api/filters_handler.php", {filters}).done(function (data) {
                $('#ticketsTable').html(data);
            });
        // Если фильтры тикетов
        } else {
             $.post("api/filters_handler.php", {filters}).done(function (data) {
                // console.log(JSON.parse(data));
                if (data) setGet(param,data);
            });
        }
    });
    //.. Выбор статуса
    $('#status-menu').bind('change', function() {
        let filters = new Object, param = 'status';
        filters['status'] = $(this).val();
        filters['baseUrl'] = baseUrl;
        // Если фильтры отчетов
        if (baseUrl.indexOf('otchet.php') != -1) {
            $.post("api/filters_handler.php", {filters}).done(function (data) {
                $('#ticketsTable').html(data);
            });
        // Если фильтры тикетов
        } else {
            setGet(param,filters['status']);
        }
    });

    //.. Выбор диапазона дат из селекта и переключение на инпут
    $('#period select').bind('change', function() {
        if ($(this).val() == 'Выбрать дату') {
            $(this).css('display', 'none');
            $('#period input').css('display', 'block');
            $('#period-label').html('<span id="period-span" class="green-select">Период</span>');
        } else {
            let filters = new Object, param = 'date';
            filters['date'] = $(this).val();
            filters['baseUrl'] = baseUrl;
            // Если фильтры отчетов
            if (baseUrl.indexOf('otchet.php') != -1) {
                fetch('api/filters_handler.php', {method: 'POST'})
                .then((res) => {
                    $.post("api/filters_handler.php", {filters}).done(function (data) {
                        $('#ticketsTable').html(data);
                    });
                })
                .then((res) => { 
                    $.post("api/filters_handler.php", {ticketCount: filters['date']}).done(function (data) {
                        $('#tickets-line').html(data);
                    });
                })
                .catch(function(err) {  
                    console.log('Fetch Error :-S', err);  
                });
            // Если фильтры тикетов
            } else {
                if ($(this).val() !== 'select') {
                    setGet(param,filters['date']);
                }
            }
        }
    });
    //.. Выбор даты из инпута
    $('#period input').bind('change', function() {
        let filters = new Object, param = 'date';
        filters['date'] = $(this).val();
        filters['baseUrl'] = baseUrl;
        // Если фильтры отчетов
        if (baseUrl.indexOf('otchet.php') != -1) {
            fetch('api/filters_handler.php', {method: 'POST'})
            .then((res) => {
                $.post("api/filters_handler.php", {filters}).done(function (data) {
                    $('#ticketsTable').html(data);
                });
            })
            .then((res) => { 
                $.post("api/filters_handler.php", {ticketCount: filters['date']}).done(function (data) {
                    $('#tickets-line').html(data);
                });
            })
            .catch(function(err) {  
                console.log('Fetch Error :-S', err);  
            });
        // Если фильтры тикетов
        } else {
            setGet(param,filters['date']);
        }
    })
    //.. Переключение на селект
    $('#period-label').on('click', '#period-span', function() {
        $('#period input').css('display', 'none');
        $('#period select').css('display', 'block');
        $('#period-label').html('Период');
    });

    //..07.01.2021 Таблица отчёта по услугам (modal-popup)
    //.. Модальное окно (Пока что для показа размерных сеток)
    if (document.getElementById('myModal')) {
        // Get the modal
        var jqmodal = $('#myModal');
        var jsmodal = document.getElementById("myModal");
        // Get the button that opens the modal
        var btn = document.getElementById("callBtn");
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        // When the user clicks the button, open the modal 
        btn.onclick = function() {
            // modal.style.display = "block";
            let filters = new Object;
            filters['report'] = $('input[name="fqr"]').val();

            $.ajax({
                url: 'api/filters_handler.php',
                // contentType: 'application/json; charset=utf-8',
                method: 'POST',
                data: {
                    filters
                },
                beforeSend: function() {
                    $('.preloader-box').fadeIn(250, function(){
                        $('.preloader-box').css('display', 'block');
                    });
                },
                success: function (data) {

                    let inner = JSON.parse(data);
                    let table = inner['table'];
                    let file = inner['file'];

                    if (table != 0) {
                        localStorage.setItem('main_report_table', table);
                        if ($('.modalMPW-content i.fa-arrow-left').length) {
                            $('.modalMPW-content i.fa-arrow-left').remove();
                        }
                        $('.preloader-box').fadeOut(250, function(){
                            $('.preloader-box').css('display', 'none');
                            $('#sizeCharts').html(table);
                            jqmodal.fadeIn(500, function(){
                                jqmodal.css('display', 'inline-block');
                                jqmodal.focus();
                            });
                        });

                        // Скачать файл отчёта (CSV)
                        if (file) {
                            let link = document.createElement('a');
                            link.setAttribute('href', file);
                            link.setAttribute('download','');
                            onload = link.click();
                        }

                    } else $('.preloader-box').css('display', 'none');
                }
            });

            // $.post("api/filters_handler.php", {filters}).done(function (data) {
            // });
                
            // jqmodal.fadeIn(500);
        }
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            // modal.style.display = "none";
            jqmodal.fadeOut(500);
        }
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == jsmodal) {
                // modal.style.display = "none";
                jqmodal.fadeOut(500);
            }
        }
        // When the user clicks ESCAPE button
        $(document).keydown(function(e) {
            // ESCAPE key pressed
            if (e.keyCode == 27 && jqmodal.css('display') == 'block') {
                jqmodal.fadeOut(500);
            }
        });
    }
    //..12.01.2021 Таблица отчёта по услугам (Расширенная запись) (modal-popup)
    $(document).delegate(".report-table", "click", function(event){
        var jqmodal = $('#myModal');
        let filters = new Object;
            filters['report_detail'] = $(this).attr('id');
        $.ajax({
            url: 'api/filters_handler.php',
            method: 'POST',
            data: {
                filters
            },
            success: function (data) {
                if (data != 0) {
                    jqmodal.fadeOut(250, function(){
                        $('.modalMPW-content').prepend('<i class="fa fa-arrow-left" aria-hidden="true"></i>');
                        $('#sizeCharts').html(data);
                        jqmodal.fadeIn(300, function(){
                            jqmodal.css('display', 'inline-block');
                            jqmodal.focus();
                        });
                    });
                } else $('.preloader-box').css('display', 'none');
            }
        });
    }).delegate(".report-table", "hover", function(event){
        // run mousenter, mouseleave events.
    });

    //..13.01.2021 Таблица отчёта по услугам (Расширенная запись) Кнопка "назад"
    $(document).delegate(".modalMPW-content i.fa-arrow-left", "click", function(event){
        var jqmodal = $('#myModal');
        jqmodal.fadeOut(250, function(){
            if ($('.modalMPW-content i.fa-arrow-left').length) {
                $('.modalMPW-content i.fa-arrow-left').remove();
            }
            $('#sizeCharts').html(localStorage.getItem('main_report_table'));
            jqmodal.fadeIn(300, function(){
                jqmodal.css('display', 'inline-block');
                jqmodal.focus();
            });
        });
    })

    //.. Функция установки GET-параметров
    function setGet(param, value) {
        // url без GET параметров
        let baseUrl = window.location.origin+window.location.pathname;
        // Полный url, с GET параметрами
        let fullUrl = window.location.href;
        // Если это не страница отчёта (на странице отчёта GET-параметры не учитываются), устанавливаем GET-параметры
        // (Это условие нужно, потому что на разных страницах элементы с одинаковыми id)
        if (fullUrl.indexOf('otchet.php') == -1) {
            // Тут очищаем всё
            if (param == 'clearAll') window.location.href = baseUrl;
            else {
                // Получаем GET-параметры, устанавливаем нужное значение и пересобираем строку
                let getParams = getGet(fullUrl);
                // Если параметр был но передался пустой, то удаляем его из GET
                if (getParams[param] && !value) delete getParams.param;
                // Иначе устанавливаем новое значение
                else getParams[param] = value;

                // Убираем пагинацию с каждым новым фильтром
                if (getParams['page']) delete getParams['page'];

                // Если статус == "all" - удаляем его
                if (param == 'status' && value == 'all') delete getParams.param;
                // Если в статусе выбрано "мои" - очищаем менеджера и наоборот,
                // если есть менеджер - меняем статус на "all"
                if (param == 'status' && value == 'mine') {
                    let x = 'assistant';
                    if (getParams[x]) delete getParams[x];
                }
                if (param == 'assistant' && value.length>0) {
                    let x = 'status';
                    if (getParams[x]) getParams[x] = 'all';
                }

                baseUrl+='?';
                // Пересобираем строку
                for (var key in getParams) {
                    if (baseUrl.toString().slice(-1) === '?') baseUrl += key+'='+getParams[key];
                    else baseUrl += '&'+key+'='+getParams[key];
                }
                window.location.href = baseUrl;
            }
        }
    }
    //.. Получаем Get-параметры (универсальная)
    function getGet(url) {
        // извлекаем строку из URL или объекта window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        // объект для хранения параметров
        var obj = {};
        // если есть строка запроса
        if (queryString) {
            // данные после знака # будут опущены
            queryString = queryString.split('#')[0];
            // разделяем параметры
            var arr = queryString.split('&');
            for (var i=0; i<arr.length; i++) {
                // разделяем параметр на ключ => значение
                var a = arr[i].split('=');
                // обработка данных вида: list[]=thing1&list[]=thing2
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function(v) {
                    paramNum = v.slice(1,-1);
                    return '';
                });
                // передача значения параметра ('true' если значение не задано)
                var paramValue = typeof(a[1])==='undefined' ? true : a[1];
                // преобразование регистра
                paramName = paramName.toLowerCase();
                paramValue = paramValue.toLowerCase();
                // если ключ параметра уже задан
                if (obj[paramName]) {
                    // преобразуем текущее значение в массив
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // если не задан индекс...
                    if (typeof paramNum === 'undefined') {
                        // помещаем значение в конец массива
                        obj[paramName].push(paramValue);
                    }
                    // если индекс задан...
                    else {
                        // размещаем элемент по заданному индексу
                        obj[paramName][paramNum] = paramValue;
                    }
                }else {
                    obj[paramName] = paramValue;
                }
            }
        }
        return obj;
    }
});