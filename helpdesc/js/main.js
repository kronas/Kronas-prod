$(document).ready(function () {

    if ($('div.preloader').length > 0) {
        $('.preloader').css('top',($(window).height()/2)-($('.preloader').width()/2)+'px');
        $('.preloader').css('left',($(window).width()/2)-($('.preloader').width()/2)+'px');
        $(window).resize(function() {
            $('.preloader').css('top',($(window).height()/2)-($('.preloader').width()/2)+'px');
            $('.preloader').css('left',($(window).width()/2)-($('.preloader').width()/2)+'px');
        });
    }
    
    ///Установка правильного названия выбранного файла в модалке "Загрузить файл"
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    ///Заполнение поля Название файла при добавление МПРа
    $("#validatedInputGroupCustomFile").on("change", function() {
        ///new_mpr_name_ids
        let fileName = $(this).val().split("\\").pop();
        $('#new_mpr_name_ids').val(fileName);
    });
    ///Запуск формы смены статуса в ticket.php
    $('#select_status').on('change', function () {
       $('#set_ticket_status').submit();
    });
    ///Запуск формы смены менеджера в ticket.php
    $('#select_manager').on('change', function () {
        $('#set_ticket_manager').submit();
    });
    /// Активация по всему сайту tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    /// Логика по добавлению услуг при создании
    $('#add_new_mpr_service').on('click', function () {
       ++mpr_add_service_counter;
        let services = '';
        $.ajax({
            url: '/helpdesc/api/mpr.php',
            dataType: 'json',
            async: false,
            data: {get_services:1},
            success: function(data) {
                for (var i=0; i<data.length; i++) {
                    services += `<option value="${data[i]['SERVICE_ID']}">${data[i]['NAME']}</option>`;
                }
            }
        });
       let list_block = $('#mpr_services_list');
       let input_html = `
        <div class="form-group row add_new_mpr_service_row_${mpr_add_service_counter} add_new_mpr_service_row">
            <div class="col-md-1" class="row_add_new_mpr_service_${mpr_add_service_counter}">
                #${mpr_add_service_counter}
            </div>
            <div class="col-sm-5" class="row_add_new_mpr_service_${mpr_add_service_counter}">
                <label>Введите артикул услуги:</label>
                <select name="add_mpr_service_model_${mpr_add_service_counter}" class="custom-select" id="" required>
                    ${services}
                </select>
            </div>
            <div class="col-sm-5" class="row_add_new_mpr_service_${mpr_add_service_counter}">
                <label>Введите Количество услуг:</label>
                <input type="number" class="form-control" placeholder="" name="add_mpr_service_count_${mpr_add_service_counter}" required>
            </div>
            <div class="col-sm-1" class="row_add_new_mpr_service_${mpr_add_service_counter}">
                <i class="fas fa-minus-circle del_row_mpr_icon_${mpr_add_service_counter}" data-del="${mpr_add_service_counter}" style="color: #dc3545"></i>
            </div>
        </div>
        <script>
            $('.del_row_mpr_icon_${mpr_add_service_counter}').on('click', function (e) {
                $('.add_new_mpr_service_row_${mpr_add_service_counter}').remove();
                let table_rows_group = $('[class *= "add_new_mpr_service_row_"]');
                if(table_rows_group.length == 0) {
                    $('#submit_new_mp_form').hide('slow');
                }
            });
        </script>
       `;
       $(list_block).append(input_html);
       $('#submit_new_mp_form').show('slow');
    });
    /// Валидация ширины и высоты по ограничениям
    function validateSizes() {
        let lenght = $('input[name="new_mpr_lenght"]');
        let width = $('input[name="new_mpr_width"]');
        let type = $('input[name="new_mpr_operation_type"]').prop('checked'); ///
        let prices_container = $('#mpr_tables_prices_container');
        let prices_table = $('#mpr_services_list');

        console.log(lenght);
        console.log(width);
        console.log(type);

        if(type) {
            if((lenght.val() > 200 && width.val() > 70) || (lenght.val() > 70 && width.val() > 200)) {
                lenght.parent().find('.invalid-feedback').hide('slow');
                width.parent().find('.invalid-feedback').hide('slow');
                prices_container.show('slow');
                prices_table.show('slow');
            } else {
                lenght.parent().find('.invalid-feedback').html('При выбранном сверлении, длина должна быть больше 200 (или больше 70, при условии, что ширина больше 200)');
                lenght.parent().find('.invalid-feedback').show('slow');
                width.parent().find('.invalid-feedback').html('При выбранном сверлении, ширина должна быть больше 200 (или больше 70, при условии, что длина больше 200)');
                width.parent().find('.invalid-feedback').show('slow');
                prices_container.hide('slow');
                prices_table.hide('slow');
            }
        } else {
            if((lenght.val() > 350 && width.val() > 100) || (lenght.val() > 100 && width.val() > 350)) {
                lenght.parent().find('.invalid-feedback').hide('slow');
                width.parent().find('.invalid-feedback').hide('slow');
                prices_container.show('slow');
                prices_table.show('slow');
            } else {
                lenght.parent().find('.invalid-feedback').html('При выбранном сверления и фрезеровании, длина должна быть больше 350 (или больше 100, при условии, что ширина больше 350)');
                lenght.parent().find('.invalid-feedback').show('slow');
                width.parent().find('.invalid-feedback').html('При выбранном сверления и фрезеровании, ширина должна быть больше 350 (или больше 100, при условии, что длина больше 350)');
                width.parent().find('.invalid-feedback').show('slow');
                prices_container.hide('slow');
                prices_table.hide('slow');
            }
        }
    }
    $('input[name="new_mpr_lenght"]').on('input', function (e) {
        validateSizes();
    });
    $('input[name="new_mpr_width"]').on('input', function (e) {
        validateSizes();
    });
    $('input[name="new_mpr_operation_type"]').on('change', function (e) {
        validateSizes();
    });

    //..08.12.2020 Вид меню (плитка/список)
    $('.menu-view .fa-mv').bind('click', function () {
        let baseUrl = window.location.origin+window.location.pathname;
        let filters = new Object, param = 'menuView', value;
        if (!$(this).hasClass('active')) {
            $('.menu-view .fa-mv').not($(this)).removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass('fa-th-large')) value = 'tile';
            if ($(this).hasClass('fa-th-list')) value = 'list';
            filters[param] = value;
            filters['baseUrl'] = baseUrl;
            $.post("api/filters_handler.php", {filters}).done(function (data) {
                location.reload();
            });
        }
    })

    //..14.12.2020 Чекбоксы на mpr-ах
    var del_elements = new Object();
    $.each($('#nav-profile .form-check-input'), function () {
        $(this).on('change', function(){
            let id = $(this).attr('id')
            if($(this).prop("checked")){
                del_elements[id] = $(this).attr('id');
            } else {
                delete del_elements[id];
            }
            // console.log(JSON.stringify(del_elements));
            if (Object.keys(del_elements).length != 0 && !$('#del-button').children().length) {
                $('#del-button').html('<button type="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>');
            }
            if (Object.keys(del_elements).length == 0 && $('#del-button').children().length > 0) {
                $('#del-button').html('');
            }
        })
    })
    //..14.12.2020 Удаление чекбоксов
    $('#del-button').bind('click', 'button', function () {
        $.post("api/remove_mprs.php", {"mprsArray": JSON.stringify(del_elements)}).done(function (data) {
            if (data !== false) location.reload();
            // console.log(data);
        });
    })

});