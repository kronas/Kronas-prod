<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 16:20
 */

include_once('../_1/config.php');

include_once($serv_main_dir.'/helpdesc/functions/database.php');
include_once($serv_main_dir.'/helpdesc/functions/common.php');
include_once($serv_main_dir.'/helpdesc/functions/autoload.php');
include_once($serv_main_dir.'/func_mini.php');

if (isset($title)) $title = $title;
else $title = 'Helpdesk';
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="shortcut icon" type="image/ico" href="<?=$main_dir?>/favicon.ico"/>
    <title><?=$title?></title>
</head>
<body>
<?php if(isset($_SESSION['user'])): ?>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="navbar_logo_menu_container">
                <a class="navbar-brand" href="<?= $main_dir ?>/helpdesc/">
                    <img src="https://kronas.com.ua/Media/pic/logo-dark.svg" alt="KRONAS">
                    <span>HELPDESK</span>
                </a>
                <a href="<?= $main_dir ?>/helpdesc/" style="margin-right: 15%;">Все тикеты</a>
            </div>
            <ul class="navbar-nav">
                <li>
                    <a class="nav-link"><small>Приветствуем</small>, <?= $_SESSION['user']['name'] ?> | </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?= $laravel_dir ?>">Вернуться без сохранения <i class="fas fa-cog"></i> </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?= $main_dir ?>/helpdesc/api/login.php?logout=1">Выйти <i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<body tabindex="1">
<?php endif; ?>