<!-- The Modal -->
<div id="myModal" class="modalMPW" tabindex="0">
	<!-- Modal content -->
	<div class="modalMPW-content">
		<i class="fa fa-times close" aria-hidden="true"></i>
		<div class="clearfix"></div>
		<div id="sizeCharts" class="sizeCharts">
			<?php if (isset($modal_content)) echo $modal_content; ?>
		</div>
	</div>
</div>