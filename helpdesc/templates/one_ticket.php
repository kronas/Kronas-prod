<?php if ($menuView === 'tile'): ?>

<div class="col-md-4 col-sm-6 col-xs-6">
    <div class="card tile-card">
        <div class="card-body">
            <h5 class="card-title"><b>Запрос <span class="badge badge-secondary">№<?= $ticket['id'] ?></span></b><?= $ticket['status'] ?></h5>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><b>Ответственный:</b> <?= $ticket['manager_name'] ?></li>
            <li class="list-group-item"><b>Клиент:</b> <?= $ticket['client_name'] ?></li>
            <li class="list-group-item"><b>Дата:</b> <?= $ticket['date_created'] ?></li>
            <li class="list-group-item"><b>Проект:</b> <?= $ticket['order_id']['DB_AC_ID'] ?> / (<?= $ticket['order_id']['DB_AC_NUM'] ?>)</li>
            <li class="list-group-item"><b>Менеджер проекта:</b> <?= $ticket['manager'] ?></li>
        </ul>
        <div class="card-body">
            <a class="btn btn-info" href="./ticket.php?id=<?= $ticket['id'] ?>">Открыть</a>
        </div>
    </div>
</div>
<?php endif ?>
<?php if ($menuView === 'list'): ?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="container">
		<div class="row card list-card">
	    	<div class="col-md-12 col-sm-12 col-xs-12">
	    		<div class="row">
	    			<div class="col-md-9 col-sm-8 col-xs-8 nopadding">
	    				<div class="card-body">
				            <h5 class="card-title"><b>Запрос <span class="badge badge-secondary">№<?= $ticket['id'] ?></span></b></h5>
				            <h5 class="card-title"><?= $ticket['status'] ?></h5>
				        </div>
	    			</div>
	    			<div class="col-md-3 col-sm-4 col-xs-4 nopadding">
				        <div class="card-body card-open-button">
				            <a class="btn btn-info" href="./ticket.php?id=<?= $ticket['id'] ?>">Открыть</a>
				        </div>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-md-12 col-sm-12 col-xs-12">
	    		<div class="row one-line">
	                <div class="col col-left col-md-3 col-sm-3 col-xs-4">
	                    <p>Ответственный:</p>
	                </div>
	                <div class="col col-md-9 col-sm-9 col-xs-8">
	                    <p><?= $ticket['manager_name'] ?></p>
	                </div>
	            </div>
	            <div class="row one-line">
	                <div class="col col-left col-md-3 col-sm-3 col-xs-4">
	                    <p>Клиент:</p>
	                </div>
	                <div class="col col-md-9 col-sm-9 col-xs-8">
	                    <p><?= $ticket['client_name'] ?></p>
	                </div>
	            </div>
	            <div class="row one-line">
	                <div class="col col-left col-md-3 col-sm-3 col-xs-4">
	                    <p>Дата:</p>
	                </div>
	                <div class="col col-md-9 col-sm-9 col-xs-8">
	                    <p><?= $ticket['date_created'] ?></p>
	                </div>
	            </div>
	            <div class="row one-line">
	                <div class="col col-left col-md-3 col-sm-3 col-xs-4">
	                    <p>Проект:</p>
	                </div>
	                <div class="col col-md-9 col-sm-9 col-xs-8">
	                    <p><?= $ticket['order_id']['DB_AC_ID'] ?> / (<?= $ticket['order_id']['DB_AC_NUM'] ?>)</p>
	                </div>
	            </div>
	            <div class="row one-line">
	                <div class="col col-left col-md-3 col-sm-3 col-xs-4">
	                    <p>Менеджер проекта:</p>
	                </div>
	                <div class="col col-md-9 col-sm-9 col-xs-8">
	                    <p><?= $ticket['manager'] ?></p>
	                </div>
	            </div>
	    	</div>
	    </div>
	</div>
</div>
<?php endif ?>