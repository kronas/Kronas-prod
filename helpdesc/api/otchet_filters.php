<?php
	//.. Подготавливаем переменные для фильтров
	if (!isset($_SESSION['filters']) && !isset($_SESSION['queries']) && !isset($_SESSION['tickets'])) {
		$_SESSION['filters']['assistant'] = null;
		$_SESSION['filters']['client'] = null;
		$_SESSION['filters']['status'] = null;
		$_SESSION['filters']['date'] = null;
		$_SESSION['queries']['assistant'] = null;
		$_SESSION['queries']['client'] = null;
		$_SESSION['queries']['status'] = null;
		$_SESSION['queries']['date'] = null;
		$_SESSION['tickets']['all'] = null;
		$_SESSION['tickets']['closed'] = null;
		$_SESSION['tickets']['inWork'] = null;
		$_SESSION['tickets']['forApprove'] = null;
	}
	//.. Блок обработки менеджера
	if (isset($_POST['filters']["assistant"])) {
		$_SESSION['filters']['assistant'] = $_POST['filters']["assistant"];
		if (!empty($_SESSION['filters']['assistant'])) {
			$_SESSION['filters']['assistant'] = addslashes($_SESSION['filters']['assistant']);
			$assistant_name = get_data('SELECT `id` FROM `manager` WHERE `name` = "'.$_SESSION['filters']['assistant'].'"');
			$_SESSION['queries']['assistant'] = '`manager` = "'.$assistant_name[0]['id'].'"';
		} else {
			$_SESSION['queries']['assistant'] = null;
		}
	}
	//.. Блок обработки клиента
	if (isset($_POST['filters']["client"])) {
		$_SESSION['filters']['client'] = $_POST['filters']["client"];
		if (!empty($_SESSION['filters']['client'])) {
			$_SESSION['filters']['client'] = addslashes($_SESSION['filters']['client']);
			$client_name = get_data('SELECT `client_id` FROM `client` WHERE `name` = "'.$_SESSION['filters']['client'].'"');
			$_SESSION['queries']['client'] = '`client` = "'.$client_name[0]['client_id'].'"';
		} else {
			$_SESSION['queries']['client'] = null;
		}
	}
	//.. Блок обработки статуса
	if (isset($_POST['filters']["status"])) {
		$_SESSION['filters']['status'] = $_POST['filters']["status"];
		if (!empty($_SESSION['filters']['status']) && $_SESSION['filters']['status'] != 'Все') {
			$_SESSION['queries']['status'] = '`status` = "'.$_SESSION['filters']['status'].'"';
		} else {
			$_SESSION['queries']['status'] = null;
		}
		
	}
	//.. Блок обработки даты
	if (isset($_POST['filters']["date"])) {
		$now_date = new DateTime('now');
		$last_week = new DateTime('now');
		$last_month = new DateTime('now');
		$today_date = $now_date->getTimestamp();
		$last_week = $last_week->modify('-1 week')->getTimestamp();
		$last_month = $last_month->modify('-1 month')->getTimestamp();
		$_SESSION['filters']['date'] = $_POST['filters']["date"];
		if (!empty($_SESSION['filters']['date']) && $_SESSION['filters']['date'] != 'За всё время') {
			if ($_SESSION['filters']['date'] == 'Месяц') {
				$_SESSION['queries']['date'] = '`date_created` <= "'.date("Y-m-d", $today_date).'" AND `date_created` > "'.date("Y-m-d", $last_month).'"';
			} elseif ($_SESSION['filters']['date'] == 'Неделя') {
				$_SESSION['queries']['date'] = '`date_created` <= "'.date("Y-m-d", $today_date).'" AND `date_created` > "'.date("Y-m-d", $last_week).'"';
			} elseif ($_SESSION['filters']['date'] == 'Сегодня') {
				$_SESSION['queries']['date'] = '`date_created` LIKE "'.date("Y-m-d", $today_date).'%"';
			} else {
				$_SESSION['queries']['date'] = '`date_created` LIKE "'.$_SESSION['filters']['date'].'%"';
			}
		} else {
			$_SESSION['queries']['date'] = null;
		}
	}
	//.. Здесь начинается сборка SQL запроса
	// Каждый IF проверяет каждый фильтр, и в зависимости от значения
	// запрос либо дополняется, либо обрезается
	$query = 'SELECT * FROM `ticket`';

	//.. Загружаем тикеты обработчика (обновляются только при обновлении даты, либо менеджера)
	$_SESSION['tickets']['all'] = 'SELECT COUNT(id) FROM `ticket`';
	$_SESSION['tickets']['closed'] = 'SELECT COUNT(id) FROM `ticket` WHERE `status` = "Закрыт"';
	$_SESSION['tickets']['inWork'] = 'SELECT COUNT(id) FROM `ticket` WHERE `status` = "В работе"';
	$_SESSION['tickets']['forApprove'] = 'SELECT COUNT(id) FROM `ticket` WHERE `status` = "На согласовании"';

	if ($_SESSION['queries']['assistant'] || $_SESSION['queries']['client'] || $_SESSION['queries']['status'] || $_SESSION['queries']['date']) {
		$query .= ' WHERE ';
	}
	if ($_SESSION['queries']['assistant']) {
		if (strripos($query, '=') || strripos($query, '>') || strripos($query, '<') || strripos($query, 'LIKE')) {
			$query .= ' AND '.$_SESSION['queries']['assistant'];
			$_SESSION['tickets']['all'];
			
		} else {
			$query .= $_SESSION['queries']['assistant'];
		}
		if (strripos($_SESSION['tickets']['all'], 'WHERE')) {
			$_SESSION['tickets']['all'] .= ' AND '.$_SESSION['queries']['assistant'];
		} else {
			$_SESSION['tickets']['all'] .= ' WHERE '.$_SESSION['queries']['assistant'];
		}
		$_SESSION['tickets']['closed'] .= ' AND '.$_SESSION['queries']['assistant'];
		$_SESSION['tickets']['inWork'] .= ' AND '.$_SESSION['queries']['assistant'];
		$_SESSION['tickets']['forApprove'] .= ' AND '.$_SESSION['queries']['assistant'];
	}
	if ($_SESSION['queries']['client']) {
		if (strripos($query, '=') || strripos($query, '>') || strripos($query, '<')) {
			$query .= ' AND '.$_SESSION['queries']['client'];
		} else {
			$query .= $_SESSION['queries']['client'];
		}
		
	}
	if ($_SESSION['queries']['status']) {
		if (strripos($query, '=') || strripos($query, '>') || strripos($query, '<')) {
			$query .= ' AND '.$_SESSION['queries']['status'];
		} else {
			$query .= $_SESSION['queries']['status'];
		}
		
	}
	if ($_SESSION['queries']['date']) {
		if (strripos($query, '=') || strripos($query, '>') || strripos($query, '<')) {
			$query .= ' AND '.$_SESSION['queries']['date'];
		} else {
			$query .= $_SESSION['queries']['date'];
		}
		if (strripos($_SESSION['tickets']['all'], 'WHERE')) {
			$_SESSION['tickets']['all'] .= ' AND '.$_SESSION['queries']['date'];
		} else {
			$_SESSION['tickets']['all'] .= ' WHERE '.$_SESSION['queries']['date'];
		}
		$_SESSION['tickets']['closed'] .= ' AND '.$_SESSION['queries']['date'];
		$_SESSION['tickets']['inWork'] .= ' AND '.$_SESSION['queries']['date'];
		$_SESSION['tickets']['forApprove'] .= ' AND '.$_SESSION['queries']['date'];
	}
	//.. Здесь выполняем запрос и собираем пары "id - name" из таблиц
	// manager и client
	$ticketAll = get_data($query);
	$client_all_data_array = get_data('SELECT * FROM `client`');
	$manager_all_data_array = get_data('SELECT * FROM `manager` ORDER BY `name`');
	$clientNames = [];
	$managerNames = [];
	//.. Выборка данных из таблицы "client"
	foreach ($client_all_data_array as $value) {
		$clientNames[$value['client_id']] = $value['name'];
	}
	//.. Выборка данных из таблицы "manager"
	foreach ($manager_all_data_array as $value) {
		$managerNames[$value['id']] = $value['name'];
	}
	//.. Здесь собираем HTML для вывода
	foreach ($ticketAll as $value) {
		if ($value['manager']) {
			$value['manager'] = $managerNames[$value['manager']];
		} else $value['manager'] = '—';
		ob_start();
		?>
		<div class="row table-item-line">
			<div class="col-user-20 table-item"><?=$value['order_id']?></div>
			<div class="col-user-20 table-item"><?=$value['date_created']?></div>
			<div class="col-user-20 table-item"><?=$value['status']?></div>
			<div class="col-user-20 table-item"><?=$value['manager']?></div>
			<div class="col-user-20 table-item"><?=$clientNames[$value['client']]?></div>
		</div>
		<?php
	}
	// и выводим
	ob_get_flush();exit;
?>