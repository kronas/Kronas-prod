<?php
/**
* Этот скрипт является контроллером страницы загрузки тикетов (Главная)
**/
// Количество тикетов на странице. Первая и последняя
$TOP = 12;
$FP = 1;
$LP = null;
// Массивы для заполнения селекта выбора даты
$date_select = array(
    "alltime" => "За всё время",
    "select" => "Выбрать дату",
    "today" => "Сегодня",
    "week" => "Неделя",
    "month" => "Текущий месяц"
);
//..06.01.2021
$mtru = array(
    "01" => 'Январь',
    "02" => 'Февраль',
    "03" => 'Март',
    "04" => 'Апрель',
    "05" => 'Май',
    "06" => 'Июнь',
    "07" => 'Июль',
    "08" => 'Август',
    "09" => 'Сентябрь',
    "10" => 'Октябрь',
    "11" => 'Ноябрь',
    "12" => 'Декабрь',
);
//..06.01.2021 Получим 12 последних месяцев
for ($i = 0; $i <= 11; $i++) {
	$f = $i + 1;
    $tmp = preg_split('#-#', date("Y-m-01", strtotime(date('Y-m-01') . " -$f months")));
    $months[$i][1] = $mtru[$tmp[1]].'-'.$tmp[0];
    $months[$i][2] = date("Y-m-01", strtotime(date('Y-m-01') . " -$i months"));
    $months[$i][3] = date("Y-m-01", strtotime(date('Y-m-01') . " -$f months"));
    // и добавим к селекту
    $date_select['between--'.$months[$i][3].'--'.$months[$i][2]] = $months[$i][1];
}
// Массив для заполнения селекта выбора статуса
$status_select = array(
	"all" => "Все запросы",
    "mine" => "Мои",
    "new" => "Новые",
    "closed" => "Закрытые",
    "inwork" => "В работе",
    "forapproval" => "На согласовании"
);
// Массив для конвертации запроса со страницы в параметры таблицы БД
$convert_arr = array(
	'assistant' => 'manager',
	'client' => 'client',
	'ticketnum' => 'id',
	'status' => 'status',
	'date' => 'date_created',
	'new' => 'новый',
    'closed' => 'закрыт',
    'inwork' => 'в работе',
    'forapproval' => 'на согласовании'

);
// Показывать дату в селекте или в инпуте
$select = 'block';
$input = 'none';

if (!isset($_GET) || empty($_GET)) {
	$q = 'SELECT * FROM `ticket`';
	$from = 0;
	$tmp = getTableData('SELECT * FROM `ticket` ORDER BY `date_created` DESC LIMIT '.$from.','.$TOP.'');
	$totalTickets = getTableData('SELECT COUNT(*) FROM `ticket`')[0]['COUNT(*)'];
	$LP = ceil($totalTickets / $TOP);
	$thisPage = 1;
// Подготовка параметров для запроса в БД
} else{
	// Далее идёт сборка запроса
	$q = 'SELECT * FROM `ticket`';
	if (isset($_GET['ticketnum'])) {
		if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `id` = "'.$_GET['ticketnum'].'"';
		else $q .= ' AND `id` = "'.$_GET['ticketnum'].'"';
	}
	if (isset($_GET['assistant'])) {
		$assistantName = getTableData('SELECT `name` FROM `manager` WHERE `id` = "'.$_GET['assistant'].'"')[0]['name'];
		if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `manager` = "'.$_GET['assistant'].'"';
		else $q .= ' AND `manager` = "'.$_GET['assistant'].'"';
	}
	if (isset($_GET['client'])) {
		$clientName = getTableData('SELECT `name` FROM `client` WHERE `client_id` = "'.$_GET['client'].'"')[0]['name'];
		if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `client` = "'.$_GET['client'].'"';
		else $q .= ' AND `client` = "'.$_GET['client'].'"';
	}
	if (isset($_GET['status'])) {
		if ($_GET['status'] == 'mine') {
			if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `manager` = "'.$_SESSION['user']['ID'].'"';
			else $q .= ' AND `manager` = "'.$_SESSION['user']['ID'].'"';
		} else {
			if ($_GET['status'] !== 'all') {
				if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `status` = "'.$convert_arr[$_GET['status']].'"';
				else $q .= ' AND `status` = "'.$convert_arr[$_GET['status']].'"';
			}
		}
	}
	if (isset($_GET['date'])) {
		if (preg_match('#^[\d]{4}-[\d]{2}-[\d]{2}$#', $_GET['date'])) {
			$select = 'none';
			$input = 'block';
		} else {
			$select = 'block';
			$input = 'none';
		}
		$now_date = new DateTime('now');
		$last_week = new DateTime('now');
		$last_month = new DateTime('now');
		$today_date = $now_date->getTimestamp();
		$last_week = $last_week->modify('-1 week')->getTimestamp();
		$last_month = $last_month->modify('-1 month')->getTimestamp();
		//..06.01.2021 Добавлен помесячный фильтр за последние 12 месяцев
		if (stristr($_GET['date'], 'between')) {
			$tmp = preg_split('#--#', $_GET['date']);
			if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `date_created` <= "'.$tmp[2].'" AND `date_created` > "'.$tmp[1].'"';
			else $q .= ' AND `date_created` <= "'.$tmp[2].'" AND `date_created` > "'.$tmp[1].'"';
		} else {
			if ($_GET['date'] == 'month') {
				if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `date_created` > "'.date('Y-m-01').'"';
				else $q .= ' AND `date_created` > "'.date('Y-m-01').'"';
			} elseif ($_GET['date'] == 'week') {
				if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `date_created` > "'.date("Y-m-d", $last_week).'"';
				else $q .= ' AND `date_created` > "'.date("Y-m-d", $last_week).'"';
			} elseif ($_GET['date'] == 'today') {
				if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `date_created` LIKE "'.date("Y-m-d", $today_date).'%"';
				else $q .= ' AND `date_created` LIKE "'.date("Y-m-d", $today_date).'%"';
			} elseif (preg_match('#^[\d]{4}-[\d]{2}-[\d]{2}$#', $_GET['date'])) {
				if (!preg_match('#=|LIKE|>|<#', $q)) $q .= ' WHERE `date_created` LIKE "'.$_GET['date'].'%"';
				else $q .= ' AND `date_created` LIKE "'.$_GET['date'].'%"';
			}
		}
		
	}
	
	$q .= ' ORDER BY `date_created` DESC';
	$count_q = str_replace('*', 'COUNT(*)', $q);
	// Получаем номер страницы
	if (isset($_GET['page'])) $thisPage = (int)$_GET['page'];
	else $thisPage = 1;
	$totalTickets = getTableData($count_q)[0]['COUNT(*)'];
	if ($totalTickets > $TOP) {
		$LP = ceil($totalTickets / $TOP);
		// Настраиваем вывод в рамки количества страниц
		if ($thisPage > $LP) $thisPage = $LP;
		if ($thisPage < 1) $thisPage = 1;
	}
	// Определяем запись с которой будет осуществляться выборка (какая страница подгружается)
	$from = ($thisPage - 1) * $TOP;
	// Загружаем порцию
	$tmp = getTableData($q.' LIMIT '.$from.','.$TOP.'');
}

// Получаем имена (если передан ID) менеджера и клиента по ID для value инпутов
if (isset($_GET['assistant'])) {
	$assistantName = getTableData('SELECT `name` FROM `manager` WHERE `id` = "'.$_GET['assistant'].'"')[0]['name'];
}
if (isset($_GET['client'])) {
	$clientName = getTableData('SELECT `name` FROM `client` WHERE `client_id` = "'.$_GET['client'].'"')[0]['name'];
}

// Подготовка массивов для вывода информации в тикеты
$outTickets = array();
foreach ($tmp as $value) {
	if (!empty($value['client'])) $value['client_name'] = getTableData('SELECT `name` FROM `client` WHERE `client_id` = "'.$value['client'].'"')[0]['name'];
	if (!empty($value['manager'])) $value['manager_name'] = getTableData('SELECT `name` FROM `manager` WHERE `id` = "'.$value['manager'].'"')[0]['name'];
	if ($value['status'] == 'новый' && !isset($value['manager_name']) ) $value['manager_name'] = 'Тикет не взят в обработку';
	if ($value['status'] == 'новый') $value['status'] = '<span class="badge badge-pill badge-success">'.$value['status'].'</span>';
	elseif ($value['status'] == 'закрыт') $value['status'] = '<span class="badge badge-pill badge-danger">'.$value['status'].'</span>';
	elseif ($value['status'] == 'в работе') $value['status'] = '<span class="badge badge-pill badge-info">'.$value['status'].'</span>';
	elseif ($value['status'] == 'на согласовании') $value['status'] = '<span class="badge badge-pill badge-primary">'.$value['status'].'</span>';
	$outTickets[] = $value;
}
// Получим менеджера проекта
$tmp = array(); $c = 0;
foreach ($outTickets as $ticket) {
	$order = null; $manger = null; $sql = 'SELECT `name` FROM `manager` WHERE `id` = ';
	if ($manager = get_order($ticket['order_id'])['manager']) {
		if ($manager = getTableData($sql.$manager)[0]['name']) {
			$ticket['manager'] = $manager;
		}
	}
	$order['order_id'] = $ticket['order_id'];
	$order['DB_AC_ID'] = get_order($ticket['order_id'])['DB_AC_ID'];
	$order['DB_AC_NUM'] = get_order($ticket['order_id'])['DB_AC_NUM'];
	$ticket['order_id'] = $order;
	$tmp[] = $ticket;
}
$outTickets = $tmp;

// Пагинация
$pagination = pagination($thisPage, $LP);

function pagination($thisPage, $allPages) {
	$out = null;
	$uri = '?';
	if ($_SERVER['QUERY_STRING']) {
		foreach ($_GET as $key => $value) {
			if ($key != 'page') $uri .= $key.'='.$value.'&';
		}
	}
	$fp = null; $ppp = null; $pp = null; $np = null; $pnp = null; $lp = null;
	if ($thisPage > 3 && $allPages > 5) {
		$fp = '<li class="page-item extreme-page-item first-page-item"><a class="page-link" href="'.$uri.'page=1">1</a></li>';
	}
	if ($thisPage < ($allPages - 3) && $allPages > 5) {
		$lp = '<li class="page-item extreme-page-item last-page-item"><a class="page-link" href="'.$uri.'page='.$allPages.'">'.$allPages.'</a></li>';
	}
	if($thisPage - 2 > 0) {
		$ppp = '<li class="page-item"><a class="page-link" href="'.$uri.'page='.($thisPage - 2).'">'.($thisPage - 2).'</a></li>';
	}
	if($thisPage - 1 > 0) {
		$pp = '<li class="page-item"><a class="page-link" href="'.$uri.'page='.($thisPage - 1).'">'.($thisPage - 1).'</a></li>';
	}
	if($thisPage < $allPages) {
		$np = '<li class="page-item"><a class="page-link" href="'.$uri.'page='.($thisPage + 1).'">'.($thisPage + 1).'</a></li>';
	}
	if($thisPage + 1 < $allPages) {
		$pnp = '<li class="page-item"><a class="page-link" href="'.$uri.'page='.($thisPage + 2).'">'.($thisPage + 2).'</a></li>';
	}

	$thisPage = '<li class="page-item this-page"><a class="page-link" nohref>'.$thisPage.'</a></li>';
	$out = $fp.$ppp.$pp.$thisPage.$np.$pnp.$lp;
	return $out;
}

// $outTickets
// $ticket['status']
// $ticket['manager']['name']
// $ticket['client']['name']


// _print($q);

// manager
// order_id
// id
// status
// type
// date_created
// date_updated
// date_closed
// client

// [date] => today
// [assistant] => admin
// [ticketnum] => 23
// [status] => mine
// [client] => sdsd
?>