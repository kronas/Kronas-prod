<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 21.06.2020
 * Time: 21:50
 */

// include_once('../../func.php');
// include_once('../../func_math.php');

include_once('../functions/database.php');
include_once('../functions/common.php');
include_once('../functions/autoload.php');

session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

//..11.12.2020 session restore
if (!isset($_SESSION['user'])) {
    if (isset($_GET['session_id'])) {
        if (!empty($_GET['session_id'])) {
            $_SESSION = get_temp_file_data($_GET["session_id"]);
            $_SESSION['saveAutorization'] = $_GET["session_id"];

            //.. Восстанавливаем сессию
            if (isset($session_restore['project_manager'])) {
                $_SESSION['project_manager'] = $session_restore['project_manager'];
            }
            if (isset($session_restore['project_client'])) {
                $_SESSION['project_client'] = $session_restore['project_client'];
            }
            if (isset($session_restore['user'])) {
                $_SESSION['user'] = $session_restore['user'];
            }
            if (isset($session_restore['tec_info'])) {
                $_SESSION['tec_info'] = $session_restore['tec_info'];
            }
        }
    }else{
        header('Location: '.$laravel_dir.'/auth?checkautorized=3'.'&nw='.$_GET['nw']);
    }
}

if($_GET['logout'] == 1) {
    if (isset($_SESSION['manager_id']) && isset($_SESSION['manager_name'])) {
        unset($_SESSION['manager_id']);
        unset($_SESSION['manager_name']);
        unset($_SESSION['isAdmin']);
    }
    unset($_SESSION['user']);
    //.. Удаляем сессию Гиблаба
    header('Location: '.$laravel_dir.'/set_auth_manager_ids?logout=3'.'&nw='.$_GET['nw']);
}

if(isset($_SESSION['user'])) {
    header('Location: '.$main_dir.'/helpdesc/'.'?nw='.$_GET['nw']);
}

if($_POST['login']) {
    if($_POST['user_type'] == 'client') {
        $sql = 'SELECT * FROM `client` WHERE `e-mail` = "'.$_POST['email'].'" AND `pass` = md5('.$_POST['password'].')';
        $client_data = get_data($sql);
        if(isset($client_data[0])) {
            $client_data = $client_data[0];
            $_SESSION['user'] = [
                'ID' => $client_data['client_id'],
                'name' => $client_data['name'],
                'phone' => $client_data['tel'],
                'mail' => $client_data['e-mail'],
                'role' => 'client',
                'code' => $client_data['code']
            ];
            header('Location: '.$laravel_dir.'/set_auth_client_ids?client_id='.$_SESSION['user']['ID'].'&rssession=3'.'&nw='.$_GET['nw']);
            // header('Location: '.$main_dir.'/helpdesc/');
            exit;
        } else {
            $_SESSION['login_error'] = 'Данные авторизации не правильные!';
            header('Location: '.$main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
            exit;
        }
    }
    if($_POST['user_type'] == 'manager') {
        $sql = 'SELECT * FROM `manager` WHERE `e-mail` = "'.$_POST['email'].'" AND `password` = "'.$_POST['password'].'"';
        $manager_data = get_data($sql);
        if(isset($manager_data[0])) {
            $manager_data = $manager_data[0];

            //.. Добавляем 'manager_id', 'manager_name' и 'isAdmin'
            // необходимо для поддержки manager_all_order
            // не рекомендуется использовать в скриптах т.к. этот вид авторизации устаревший)
            $_SESSION['manager_id'] = $manager_data['id'];
            $_SESSION['manager_name'] = $manager_data['name'];
            $_SESSION['isAdmin'] = $manager_data['admin'];

            $role = $manager_data['admin'] == 1 ? 'admin' : 'manager';
            $_SESSION['user'] = [
                'ID' => $manager_data['id'],
                'name' => $manager_data['name'],
                'phone' => $manager_data['phone'],
                'mail' => $manager_data['e-mail'],
                'role' => $role,
                'code' => $manager_data['code']
            ];
            header('Location: '.$laravel_dir.'/set_auth_manager_ids?manager_id='.$_SESSION['user']['ID'].'&rssession=3'.'&nw='.$_GET['nw']);
            // header('Location: '.$main_dir.'/helpdesc/');
            exit;
        } else {
            $_SESSION['login_error'] = 'Данные авторизации не правильные!';
            header('Location: '.$main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
            exit;
        }
    }
}