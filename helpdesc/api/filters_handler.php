<?php
session_start();
include_once('../functions/database.php');

//.. Условие, где надо подгружать списки из БД по части значения (LIKE ...% и LIKE %...%)
// Эта часть кода вызывает функцию, которая сначала ищет по LIKE ...%, а затем по LIKE %...%
// Сюда направляет ключ "part_of_string"
// Параметры:
	// таблица;
	// поле, по которому будет осуществляться выборка;
	// запрос;
if (isset($_POST["part_of_string"])) {
	$table = $_POST["part_of_string"]['table'];
	$field = $_POST["part_of_string"]['field'];
	$request = $_POST["part_of_string"]['request'];
	$response = [];
	$list = searchString($table, $field, $request);
	if ($list) {
		foreach ($list as $value) {
			if (!in_array($value[$field], $response)) {
				$response[] = $value[$field];
			}
		}
	}
	echo json_encode($response);exit;
}
if (isset($_POST['filters'])) {
	if (stripos($_POST['filters']['baseUrl'], 'helpdesc/otchet.php')) {
		require_once 'otchet_filters.php';
	} else { require_once 'ticket_check_queries.php'; }
}
//.. Этот блок срабатывает при смене менеджера или даты
if (isset($_POST['ticketCount'])) {
	$all = get_data($_SESSION['tickets']['all']);
	$closed = get_data($_SESSION['tickets']['closed']);
	$inWork = get_data($_SESSION['tickets']['inWork']);
	$forApprove = get_data($_SESSION['tickets']['forApprove']);
	ob_start();
	?>
	<div class="col-md-3 table-item"><?=$all[0]['COUNT(id)']?></div>
	<div class="col-md-3 table-item"><?=$closed[0]['COUNT(id)']?></div>
	<div class="col-md-3 table-item"><?=$inWork[0]['COUNT(id)']?></div>
	<div class="col-md-3 table-item"><?=$forApprove[0]['COUNT(id)']?></div>
	<?php
	ob_get_flush();exit;

	// echo $_SESSION['tickets']['closed'];
}
?>