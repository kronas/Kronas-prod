<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 19.06.2020
 * Time: 10:06
 */

// include_once('../../func.php');
// include_once('../../func_math.php');

include_once('../functions/database.php');
include_once('../functions/common.php');
include_once('../functions/autoload.php');


if($_POST['add_new_mpr']) {

    $name = $_POST['new_mpr_name'];
    $lenght = $_POST['new_mpr_lenght'];
    $width = $_POST['new_mpr_width'];
    $operation = $_POST['new_mpr_operation_type'];
    $side = $_POST['new_mpr_operation_side'];
    $order = $_POST['new_mpr_order'];
    $ticket_id = $_POST['new_mpr_ticket_ids'];
    $kl_l = $_POST['kl_l'] ? 1 : 0;
    $kl_r = $_POST['kl_r'] ? 1 : 0;
    $kl_t = $_POST['kl_t'] ? 1 : 0;
    $kl_b = $_POST['kl_b'] ? 1 : 0;

    $mpr_services = [];

    foreach ($_POST as $k=>$v) {
        if(strpos($k, 'add_mpr_service_model_') === false) {
            continue;
        } else {
            $counter = substr($k, 22);
            $mpr_services[] = [
                'service_id' => $v,
                'service_count' => $_POST['add_mpr_service_count_'.$counter]
            ];
        }
    }






    $sql_insert_mpr = 'INSERT INTO `ticket_construct_mpr`(`name`, `ORDER1C`, `ticket_id`, `side`, `L`, `W`, `by_size`, `kll`, `klr`, `klt`, `klb`) 
                       VALUES ("'.$name.'", '.$order.', '.$ticket_id.', '.$side.', '.$lenght.', '.$width.', '.$operation.', '.$kl_l.', '.$kl_r.', '.$kl_t.', '.$kl_b.')';
    $insert_mpr = set_data($sql_insert_mpr);

    $mpr_id_last = $_SESSION['last_insert_id'];

    $sql_insert_mpr_services = 'INSERT INTO `ticket_mpr_price`(`mpr_id`, `service_id`, `count`) VALUES ';

    foreach($mpr_services as $k=>$v) {

        $sql_string = '('.$_SESSION['last_insert_id'].','.$v['service_id'].','.$v['service_count'].')';

        if($k + 1 == count($mpr_services)) {
            $sql_insert_mpr_services .= $sql_string;
        } else {
            $sql_insert_mpr_services .= $sql_string.',';
        }

    }

    $sql_mpr_services = set_data($sql_insert_mpr_services);


    $upload_dir = $serv_main_dir.'/helpdesc/uploads/'.$ticket_id.'/';

    mkdir($upload_dir, 0777, true);

    $upload_pic = $upload_dir .mt_rand(111111111,999999999). basename($_FILES['new_mpr_operation_image']['name']);

    $info = new SplFileInfo($upload_pic);

    $file_pic = str_replace($serv_main_dir, $main_dir, $upload_pic);



    $upload_dir = $serv_main_dir.'/helpdesc/uploads/'.$ticket_id.'/mpr/';

    mkdir($upload_dir, 0777, true);

    $upload_mpr = $upload_dir . basename($mpr_id_last).'.mpr';

    $info = new SplFileInfo($upload_mpr);

    $file_mpr = str_replace($serv_main_dir, $main_dir, $upload_mpr);


    move_uploaded_file($_FILES['new_mpr_operation_image']['tmp_name'], $upload_pic);

    move_uploaded_file($_FILES['new_mpr_operation_mpr_file']['tmp_name'], $upload_mpr);

    $sql_update_mpr = 'UPDATE `ticket_construct_mpr` SET `file`="'.$file_mpr.'",`pic`="'.$file_pic.'" WHERE `mpr_id` = '.$mpr_id_last;
    $update_sql = set_data($sql_update_mpr);


    unset($_SESSION['last_insert_id']);

    if($sql_mpr_services == 'complete') {
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }

}
if($_GET['get_services'] == 1) {
    $sql_get_services = 'SELECT * FROM `SERVICE` WHERE `SERVICE_TYPE` IN (3,4)';
    $get_services = get_data($sql_get_services);
    echo json_encode($get_services);
}



if($_POST['edit_new_mpr']) {

    $mpr_id = $_POST['edit_mpr_ids'];
    $name = $_POST['new_mpr_name'];
    $lenght = $_POST['new_mpr_lenght'];
    $width = $_POST['new_mpr_width'];
    $operation = $_POST['new_mpr_operation_type'];
    $side = $_POST['new_mpr_operation_side'];
    $order = $_POST['new_mpr_order'];
    $ticket_id = $_POST['new_mpr_ticket_ids'];
    $kl_l = $_POST['kl_l'] ? 1 : 0;
    $kl_r = $_POST['kl_r'] ? 1 : 0;
    $kl_t = $_POST['kl_t'] ? 1 : 0;
    $kl_b = $_POST['kl_b'] ? 1 : 0;

    $mpr_services = [];

    foreach ($_POST as $k=>$v) {
        if(strpos($k, 'add_mpr_service_model_') === false) {
            continue;
        } else {
            $counter = substr($k, 22);
            $mpr_services[] = [
                'service_id' => $v,
                'service_count' => $_POST['add_mpr_service_count_'.$counter]
            ];
        }
    }

    $upload_dir = $serv_main_dir.'/helpdesc/uploads/'.$ticket_id.'/mpr/';

    mkdir($upload_dir, 0777, true);

    if($_FILES['new_mpr_operation_image']['name'] && $_FILES['new_mpr_operation_image']['size'] > 0) {

        $upload_pic = $upload_dir .mt_rand(111111111,999999999). basename($_FILES['new_mpr_operation_image']['name']);

        $info = new SplFileInfo($upload_pic);

        $file_pic = str_replace($serv_main_dir, $main_dir, $upload_pic);

        move_uploaded_file($_FILES['new_mpr_operation_image']['tmp_name'], $upload_pic);
    }

    if($_FILES['new_mpr_operation_mpr_file']['name'] && $_FILES['new_mpr_operation_mpr_file']['size'] > 0) {

        $upload_mpr = $upload_dir . basename($_FILES['new_mpr_operation_mpr_file']['name']);

        $info = new SplFileInfo($upload_mpr);

        $file_mpr = str_replace($serv_main_dir, $main_dir, $upload_mpr);

        move_uploaded_file($_FILES['new_mpr_operation_mpr_file']['tmp_name'], $upload_mpr);
    }

    $sql_update_mpr = 'UPDATE `ticket_construct_mpr` SET `name`= "'.$name.'",';

    if($file_mpr) {
        $sql_update_mpr .= '`file`= "'.$file_mpr.'",';
    }

    if($file_pic) {
        $sql_update_mpr .= '`pic`= "'.$file_pic.'",';
    }

    $sql_update_mpr .= '`side`= '.$side.',
                        `L`= '.$lenght.',
                        `W`= '.$width.',
                        `by_size`= '.$operation.',
                        `kll` = '.$kl_l.', 
                        `klr` = '.$kl_r.', 
                        `klt` = '.$kl_t.', 
                        `klb` = '.$kl_b.'
                        WHERE `mpr_id` = '.$mpr_id;


   
    $insert_mpr = set_data($sql_update_mpr);

    $sql_delete_service = 'DELETE FROM `ticket_mpr_price` WHERE `mpr_id` = '.$mpr_id;
    $delete_mpr_price = set_data($sql_delete_service);

    $sql_insert_mpr_services = 'INSERT INTO `ticket_mpr_price`(`mpr_id`, `service_id`, `count`) VALUES ';

    foreach($mpr_services as $k=>$v) {

        $sql_string = '('.$mpr_id.','.$v['service_id'].','.$v['service_count'].')';

        if($k + 1 == count($mpr_services)) {
            $sql_insert_mpr_services .= $sql_string;
        } else {
            $sql_insert_mpr_services .= $sql_string.',';
        }

    }

    $sql_mpr_services = set_data($sql_insert_mpr_services);

    unset($_SESSION['last_insert_id']);

    if($sql_mpr_services == 'complete') {
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }

}
if($_GET['delete_this_mpr'] > 0) {

    $mpr_id = $_GET['delete_this_mpr'];

    $ticket_id = $_GET['ticket_id'];

    $sql_delete_service = 'DELETE FROM `ticket_mpr_price` WHERE `mpr_id` = '.$mpr_id;

    $delete_mpr_price = set_data($sql_delete_service);

    $sql_delete_mpr = 'DELETE FROM `ticket_construct_mpr` WHERE `mpr_id` = '.$mpr_id;

    $delete_mpr_price = set_data($sql_delete_mpr);

    if($delete_mpr_price == 'complete') {
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }
}
