<?php

include_once('../../_1/config.php');
include_once('../../func.php');
include_once('../../func_math.php');
include_once('../functions/database.php');
include_once('../functions/common.php');
include_once('../functions/autoload.php');

if($_POST['send_message']) {
    $ticket_id = $_POST['ticket_id'];
    $author_id = $_SESSION['user']['ID'];
    $author_type = $_SESSION['user']['role'];
    $message = $_POST['message'];
    $handler_email = $_POST['handler_email'];
    $client_email = $_POST['client_email'];

    $sql = 'INSERT INTO `ticket_message` (`ticket_id`, `author`, `author_role`, `date`, `message`) VALUES ('.$ticket_id.', '.$author_id.', "'.$author_type.'", "'.date("Y-m-d H:i:s").'" , "'.htmlspecialchars($message).'")'; ///

    $result = set_data($sql);

    if($result == 'complete') {


        $mail_new_ticket = <<<mail
            <p><b>В чате запроса #{$ticket_id} новое сообщение:</b></p>
            <br>
            <p>"{$message}"</p>
            <br>
            <p><b>Обработчик:</b><br>{$_SESSION['handler']['name']}<br>{$_SESSION['handler']['phone']}</p>
            <p>Перейти в запрос: <a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
            <p></p>
        mail;

        if($_SESSION['user']['mail'] == $handler_email) {
            $mail_new_ticket = preg_replace('#Обработчик:.+#', '', $mail_new_ticket);
            send_mail_report('Сообщение в чате по запросу №'.$ticket_id, $mail_new_ticket, $client_email);
        }
        if($_SESSION['user']['mail'] == $client_email) {
            send_mail_report('Сообщение в чате по запросу №'.$ticket_id, $mail_new_ticket, $handler_email);
        }

        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }
}

if($_POST['send_file']) {
    $ticket_id = $_POST['ticket_id'];

    $upload_dir = $serv_main_dir.'/helpdesc/uploads/'.$ticket_id.'/';

    mkdir($upload_dir, 0777, true);

    $upload_file = $upload_dir .mt_rand(111111111,999999999). basename($_FILES['userfile']['name']);

    $info = new SplFileInfo($upload_file);

    $file_url = str_replace($serv_main_dir, $main_dir, $upload_file);

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $upload_file)) {
        $sql = 'INSERT INTO `ticket_assets` (`ticket_id`, `file`, `name`) VALUES ('.$ticket_id.', "'.$file_url.'", "'.$_FILES['userfile']['name'].'")';
        $result = set_data($sql);
        if($result == 'complete') {
            header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
        }
    } else {
        echo "Возможная атака с помощью файловой загрузки!\n";
    }
}

if($_POST['set_status']) {
    $ticket_id = $_POST['ticket_id'];
    $client_email = $_POST['client_email'];
    $manager_email = $_POST['manager_email'];
    $handler_name = $_POST['handler_name'];

    $sql = 'UPDATE `ticket` SET `status` = "'.$_POST['set_status'].'" WHERE `id` = '.$ticket_id;
    $result = set_data($sql);
    if($result == 'complete') {
        $mail_new_ticket = <<<mail
            <p><b>Запрос #{$ticket_id}: {$_POST['set_status']}</b></p>
            <p>Статус запроса изменен на: "{$_POST['set_status']}"</p>
            <p><b>Обработчик:</b><br>{$_SESSION['handler']['name']}<br>{$_SESSION['handler']['phone']}</p>
            <p>Перейти в запрос: <a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
            <p></p>
        mail;

        send_mail_report('Изменился статус запроса №'.$ticket_id, $mail_new_ticket, $_SESSION['user']['mail']);

        if($_POST['set_status'] == 'на согласовании') {
            $mail_new_ticket = <<<mail
                    <p><b>Запрос #{$ticket_id}: {$_POST['set_status']}</b></p>
                    <p>Статус запроса изменен на: "{$_POST['set_status']}"</p>
                    <p><b>Обработчик:</b><br>{$_SESSION['handler']['name']}<br>{$_SESSION['handler']['phone']}</p>
                    <p>Обработчик завершил работу над вашим запросом.<br>Пожалуйста, проверьте правильность внесенных изменений и сообщите об этом обработчику.<br>Ссылка на страницу запроса ниже.</p>
                    <p>Перейти в запрос: <a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
                    <p></p>
                mail;
        }

        if($_POST['set_status'] == 'закрыт') {
            $mail_new_ticket = <<<mail
                    <p><b>Запрос #{$ticket_id}: {$_POST['set_status']}</b></p>
                    <p>Статус запроса изменен на: "{$_POST['set_status']}"</p>
                    <p><b>Обработчик:</b><br>{$_SESSION['handler']['name']}<br>{$_SESSION['handler']['phone']}</p>
                    <p>Обработчик завершил работу над вашим запросом. <a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
                    <p></p>
                mail;
        }

        if(strlen($client_email) > 3) {
            $mail_new_ticket = preg_replace('#Обработчик:.+#', '', $mail_new_ticket);
            send_mail_report('Изменился статус запроса №'.$ticket_id, $mail_new_ticket, $client_email);
        }
        if(strlen($manager_email) > 3) {
            send_mail_report('HELPDESC: Изменился статус запроса №'.$ticket_id, $mail_new_ticket, $manager_email);
        }
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }
}

if($_POST['set_manager']) {
    $ticket_id = $_POST['ticket_id'];
    $sql = 'UPDATE `ticket` SET `manager` = "'.$_POST['set_manager'].'" WHERE `id` = '.$ticket_id;
    $result = set_data($sql);
    if($result == 'complete') {
        $manager_email = get_manager($_POST['set_manager'])['e-mail'];
        $mail_new_ticket = <<<mail
                    <p><b>Обработчиком:</b><br>{$_SESSION['handler']['name']}<br>{$_SESSION['handler']['phone']}</p>
                    <p><b>Вам был передан запрос №{$ticket_id}</b></p>
                    <p>Следить за статусом запроса и вести коммуникацию с клиентом Вы можете по ссылке:</p>
                    <p><a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
                    <p></p>
                mail;
        send_mail_report('Вам был передан запрос №'.$ticket_id, $mail_new_ticket, $manager_email);
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }
}

if($_POST['get_this_ticket'] == 1) {
    $ticket_id = $_POST['ticket_id'];
    $manager_id = $_POST['manager_id'];
    $order_id = $_POST['order_id'];
    $project = $_POST['project'];
    $client_email = $_POST['client_email'];

    $_SESSION['handler'] = $_SESSION['user'];

    $sql_check = 'SELECT * FROM `ticket` WHERE `id` = '.$ticket_id;
    $check = get_data($sql_check, 'one');
    if(!empty($check[0]['manager'])) {
        echo "Запрос уже взят в работу другим обработчиком.";
        exit;
    }

    $sql = 'UPDATE `ticket` SET `manager` = "'.$_SESSION['user']['ID'].'", `status` = "в работе" WHERE `id` = '.$ticket_id;
    $result = set_data($sql);
    $sql_project = 'INSERT INTO `PROJECT`(`status`, `DATE`, `ORDER1C`, `client_id`, `manager_id`, `MY_PROJECT_OUT`, `comment`) 
                                 VALUES ("start", CURRENT_TIMESTAMP, '.$order_id.', NULL, '.$manager_id.', "", "handler_get_ticket" )';
    $result_project = set_data($sql_project);

  

    //.. Находим PROJECT_ID в базе для использования его в качестве имени файла
    $sql_PID = 'SELECT * FROM `PROJECT` WHERE `ORDER1C` = "'.$order_id.'"';
    $query_PID = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_PID);
    //..23.11.2020 Сохраняем в файл:
    put_Project_file($_SESSION['last_insert_id'], $project);

    if($result == 'complete') {
        $mail_new_ticket = <<<mail
                    <p><b>Обработчик:</b><br>{$_SESSION['handler']['name']}<br>{$_SESSION['handler']['phone']}</p>
                    <p>взял в работу ваш запрос №{$ticket_id}</p>
                    <p>Статус запроса изменен на: "в работе"</p>
                    <p>Следить за статусом запроса и вести коммуникацию с обработчиком Вы можете по ссылке:</p>
                    <p><a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
                    <p></p>
                mail;
        if($client_email) {
            $mail_new_ticket = preg_replace('#Обработчик:.+#', '', $mail_new_ticket);
            send_mail_report('Обработчик взял в работу ваш запрос №'.$ticket_id, $mail_new_ticket, $client_email);
        }
    $manager_email = get_manager($manager_id)['e-mail'];
    if(strlen($manager_email) > 3) {
        echo $manager_email;
        send_mail_report('Обработчик взял в работу ваш запрос №'.$ticket_id, $mail_new_ticket, $manager_email);
    }
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
    }

}

/// http://service.kronas.com.ua:11080/helpdesc/api/ticket.php?create_new_ticket=1&order_id=148443&client_id=85159
if($_GET['create_new_ticket']) {


    $order_id = $_GET['order_id'];
    
    $sql_check_ticket = 'SELECT * FROM `ticket` WHERE `status` != "закрыт" AND `order_id` = '.$order_id;
    $check_ticket = get_data($sql_check_ticket);
    if(count($check_ticket) > 0) {
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$check_ticket[0]['id'].'&nw='.$_GET['nw']);
        exit;
    }

    $client_id = $_GET['client_id'];
    $manager_id = $_GET['manager_id'];
    $start_message = $_POST['first_message'];
    $client_info = get_client($client_id);

    if(!empty($manager_id)) {
        $manager_info = get_manager($manager_id);
        $author = 'менеджером: '.$manager_info['name'].' ('.$manager_info['phone'].')';
        $creator_mail = $manager_info['e-mail'];
    } else {
        $author = 'клиентом';
        $creator_mail = $client_info['e-mail'];
    }

    $sql_create_ticket = 'INSERT INTO `ticket`(`client`, `manager`, `order_id`, `status`, `type`, `date_created`) 
                          VALUES ('.$client_id.', NULL, '.$order_id.', "новый", NULL, "'.date('Y:m:d H:i:s').'" )';
    $create_ticket = set_data($sql_create_ticket);
    $ticket_id = $_SESSION['last_insert_id'];
    unset($_SESSION['last_insert_id']);

    if($author == 'клиентом') {
        $db_author = $client_id;
    } else {
        $db_author = $manager_id;
    }

    $sql_set_message = 'INSERT INTO `ticket_message`(`ticket_id`, `author`, `author_role`, `date`, `message`) 
                        VALUES ('.$ticket_id.', '.$db_author.', "client", CURRENT_TIMESTAMP, "'.$start_message.'")';
    set_data($sql_set_message);
    if($create_ticket == 'complete') {

        $client_name = get_client($client_id)['name'];
        $current_date_time = date('Y-m-d H:i:s');
        $mail_for_client = <<<mail
            <p><b>Ваше обращение отправлено обработчикам.</b></p>
            <p>Дата/время: {$current_date_time}</p>
            <p>Следить за статусом запроса Вы можете по ссылке:</p>
            <p><a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
            <p></p>
        mail;

        $mail_new_ticket = <<<mail
            <p><b>Появилось новое обращение за помощью обработчика!</b></p>
            <p>Дата/время: {$current_date_time}</p>
            <p>Обращение было создано {$author}</p>
            <p>Клиент: {$client_name} {$client_info['tel']}</p>
            <p>Перейти в запрос: <a href="{$main_dir}/helpdesc/ticket.php?id={$ticket_id}">запрос №{$ticket_id}</a></p>
            <p></p>
        mail;

        send_mail_report('Ваше обращение отправлено.', $mail_for_client, $creator_mail['e-mail']);
        send_mail_report('Создан новый запрос к обработчикам!', $mail_new_ticket, 'production.handlers@kronas.com.ua');
        /// production.handlers@kronas.com.ua

        if($client_id && $manager_id && auth_manager($manager_id)) {
            header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
            exit;
        } else if($client_id && auth_client($client_id)) {
            header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$ticket_id.'&nw='.$_GET['nw']);
            exit;
        } else {
            header('Location: '.$main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
            exit;
        }

    }
}
