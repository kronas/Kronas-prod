<?php
include_once('../functions/database.php');
if (isset($_POST['mprsArray'])) {
	$mprsArray = json_decode($_POST['mprsArray']);
	$sql1 = 'DELETE FROM `ticket_mpr_price` WHERE `mpr_id` = ';
	$sql2 = 'DELETE FROM `ticket_construct_mpr` WHERE `mpr_id` = ';

	// $mprsArray - массив ID таблицы ticket_construct_mpr
	// 1) Удаляем все элементы из таблицы ticket_mpr_price (по mpr_id)
	// 2) Удаляем все элементы из таблицы ticket_construct_mpr (по mpr_id)
	foreach ($mprsArray as $key => $value) {
		runQuery($sql1.$value);
		runQuery($sql2.$value);
	}
}
?>