<?php

ini_set('max_execution_time', '1800');// 300 - 5мин // 1800 - 30мин

// Сюда передаётся значение для GET-параметра и скрипт просто проверяет есть ли такое значение в БД
// и возвращает true или false
// GET-параметр содержит ключи: filter - один из фильтров и baseUrl - url, с которого отправляется фильтр
if ($_POST['filters']['ticketnum']) {
	if (!$data = get_data('SELECT `id` FROM `ticket` WHERE `id` LIKE "'.$_POST['filters']['ticketnum'].'%"')[0]['id']){
		$data = false;
	} else $data = $_POST['filters']['ticketnum'];
}
if ($_POST['filters']['assistant']) {
	if (!$data = get_data('SELECT `id` FROM `manager` WHERE `name` = "'.$_POST['filters']['assistant'].'"')[0]['id']){
		$data = false;
	}
}
if ($_POST['filters']['client']) {
	if (!$data = get_data('SELECT `client_id` FROM `client` WHERE `name` = "'.$_POST['filters']['client'].'"')[0]['client_id']){
		$data = false;
	}
}
//..08.12.2020 Вид меню (плитка/список)
if ($_POST['filters']['menuView']) {
	if (!isset($_SESSION['menuView'])) $_SESSION['menuView'] = $_POST['filters']['menuView'];
	else $_SESSION['menuView'] = $_POST['filters']['menuView'];
	exit;
}
//..07.01.2021 Вывод таблицы услуг и их стоимости по выбранным тикетам
if ($_POST['filters']['report']) {

	$manager = $_SESSION['user']['ID'];

	if (false !== stripos($_POST['filters']['report'], 'WHERE')) {
		$q = str_replace('WHERE', "WHERE `manager` = $manager AND", $_POST['filters']['report']);
	} else {
		// Если запрос за всё время, то выборку делаем за последний месяц (от даты до даты),
		// потому что за всё время, это огромный массив
		$lastMonth = "AND `date_created` <= '" . date('Y:m:d') . "' AND `date_created` >= '" . date('Y:m:d', strtotime("-1 month")) . "'";
		$q = str_replace('`ticket`', "`ticket` WHERE `manager` = $manager " . $lastMonth, $_POST['filters']['report']);
	}

	if ($tserv = get_data($q)) {
		$data = getSVS($tserv);
		$data = json_encode($data);
	} else $data = 0;
	// $data = 1;
}
//..12.01.2021 Вывод таблицы услуг и их стоимости по выбранным тикетам (Расширенная запись)
if ($_POST['filters']['report_detail']) {
	$data = getSVSDetail($_POST['filters']['report_detail']);
	// $data = 1;
}
echo $data;


// -------------------------- functions --------------------------------

//..06.01.2021 Получим таблицу услуг и их стоимости по выбранным тикетам
function getSVS($tickets) {
	
	$output = [];
	$_SESSION['TicketDetailReport'] = array();
	$c = 0;
	$tmp = null;
	$sum = 0;
	$data = array();
	$build = [];
	$out = '';
	$servicesCode = [];
	$CSVservicesCodeHeader = [0 => 'Номер заказа/запроса'];
	$CSVservicesCodeBody = [];
	$servicesTotal = [];
	$ticketDetailReport = [];
	$hdr = '<h3 class="AC">Отчёт по услугам</h3>
			    <div class="RS-table">
			    	<div class="one-line">
			    	<div class="col-left w240">
						<p class="cell fwb">номер заказа/запроса</p>
					</div>';
	$ftr = '</div>';

	// Получаем массив всех нужных услуг
	$tmp = getFilteredServices();
	foreach ($tmp as $key => $value) {
		$hdr .= '<div class="col-left">
					<p class="cell fwb" data-code="' . $key . '">' . $value . '</p>
				</div>';
		$servicesCode[$key] = $value;
		$CSVservicesCodeHeader[$key] = $value;
	}
	$hdr .= '</div>';

	foreach ($tickets as $value) {

			$orderData = getOrderById($value['order_id']);
			$orderData['PLACE'] = (empty($orderData['PLACE'])) ? 1 : $orderData['PLACE'];

			$lastProject = getLastProjectByOrderId($value['order_id']);
			$projectCalculate = getProjectCalculate($lastProject['project_data'], $orderData['PLACE']);
			$order = $orderData['DB_AC_ID'] .' / (' . $orderData['DB_AC_NUM'] . ') / ' . $value['id'];

			foreach ($projectCalculate as $key => $val) {

				$build[$order][$key]['name'] = $servicesCode[$val['service_id']];
				$build[$order][$key]['ticketId'] = $value['id'];
				$build[$order][$key]['orderId'] = $value['order_id'];
				$build[$order][$key]['code'] = $val['service_id'];
				$build[$order][$key]['count'] = $val['service_count'];
				$build[$order][$key]['price'] = $val['service_price'];
				$build[$order][$key]['description'] = $val['service_description'];
				$build[$order][$key]['unit'] = $val['service_unit'];

				$CSVservicesCodeBody[$order][$key]['name'] = $servicesCode[$val['service_id']];
				$CSVservicesCodeBody[$order][$key]['code'] = $val['service_id'];
				$CSVservicesCodeBody[$order][$key]['count'] = $val['service_count'];
				$CSVservicesCodeBody[$order][$key]['price'] = $val['service_price'];

			}

			$ticketDetailReport[$value['id']] = $build[$order];
	}

	$_SESSION['TicketDetailReport'] = $ticketDetailReport;

	$orderNum = 1;
	foreach ($build as $order => $services) {
		$writeOrderFlag = 0;
		foreach ($services as $key => $service) {
			if (!isset($servicesTotal[$service['code']])) {
				$servicesTotal[$service['code']] = 0;
			}
			$servicesTotal[$service['code']] += $service['count'];

			$tmpStr = '<div id="' . $service['ticketId'] . '" class="one-line report-table">';
			if ($key === 0 || $writeOrderFlag === 0) {
				$tmpStr .= '<div class="col-left w240 align-left">
			                <p class="cell">' . $order . '</p>
			            </div>';
			} else {
				$tmpStr .= '<div class="col-left w240 align-left">
			                	<p class="cell"></p>
			            	</div>';
			}

			$tmpCount = 0;
			foreach ($servicesCode as $k => $value) {
				if ($service['code'] == $k) {
					$tmpCount++;
					$tmpStr .= '<div class="col-left">
				                	<p class="cell">' . $service['count'] . '</p>
				            	</div>';
				} else {
					$tmpStr .= '<div class="col-left">
				                	<p class="cell"></p>
				            	</div>';
				}
			}

			if ($tmpCount > 0) {
				$writeOrderFlag = 1;
				$htmlNum = '<b><big>' . $orderNum . ')</big></b> ';
				$tmpStrChanged = preg_replace('#(\d{8,9} \/ \()#', $htmlNum . '$1', $tmpStr);
				if ($tmpStrChanged !== $tmpStr) $orderNum++;
				$out .= $tmpStrChanged;
				$out .= '</div>';
			}
		}
	}

	$out .= '<div class="one-line report-table">';
	$out .= '<div class="col-left w240">
                <p class="cell fwb">Общее количество по услугам</p>
            </div>';
	foreach ($servicesCode as $key => $value) {
		if (isset($servicesTotal[$key])) {
			$out .= '<div class="col-left">
		                <p class="fwb">' . round($servicesTotal[$key], 2) . '</p>
		            </div>';
		} else {
			$out .= '<div class="col-left">
		                <p class="fwb"></p>
		            </div>';
		}
	}

	$out .= '</div>';

	$table = $hdr.$out.$ftr;
	$file = export_to_csv($CSVservicesCodeHeader, $CSVservicesCodeBody);

	$output['table'] = $table;
	$output['file'] = $file;

	return $output;
}

//..06.01.2021 Получим таблицу услуг и их стоимости по выбранным тикетам (Расширенная запись)
function getSVSDetail($code) {

	$tmp = null; $sum = 0; $data = array(); $out = null;
	$hdr = '<h3 class="AC">Отчёт по услугам</h3>
			    <div class="container-fluid RS-table">
			    	<div class="row one-line">
			            <div class="col col-left col-md-2">
			                <p class="fwb">Запрос / Проект</p>
			            </div>
			            <div class="col col-left col-md-5">
			                <p class="fwb">Услуга</p>
			            </div>
			            <div class="col col-left col-md-1">
			                <p class="fwb">Количество</p>
			            </div>
			            <div class="col col-left col-md-1">
			                <p class="fwb">Ед.изм.</p>
			            </div>
			            <div class="col col-left col-md-1">
			                <p class="fwb">Цена</p>
			            </div>
			            <div class="col col-left col-md-2">
			                <p class="fwb">Сумма</p>
			            </div>
			        </div>';
	$ftr = '</div>';
	foreach ($_SESSION['TicketDetailReport'][$code] as $key => $value) {
		$sum += ($value['count'] * $value['price']);
		
		if ($tmp = get_data('SELECT * FROM `ORDER1C` WHERE `ID` = "'.$value['orderId'].'"')) {
			$order_id = $value['ticketId'].' / '.$tmp[0]['DB_AC_ID'].' ('.$tmp[0]['DB_AC_NUM'].')';
		} else $order_id = $value['orderId'];
		
		if ($key !== 0) $order_id = '';

		$out .='<div class="row one-line">
		            <div class="col col-left col-md-2">
		                <p>'.$order_id.'</p>
		            </div>
		            <div class="col col-left col-md-5">
		                <p>'.$value['description'].'</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>'.$value['count'].'</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>'.$value['unit'].'</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>'.round($value['price'], 2).'</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p>'.round($value['count'] * $value['price'], 2).'</p>
		            </div>
		        </div>';
	}
	$out_sum .='<div class="row one-line">
		            <div class="col col-left col-md-10">
		                <p>Итого:</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p>'.round($sum, 2).'</p>
		            </div>
		        </div>';
	return $hdr.$out.$out_sum.$ftr;
}

/**
 * Функция возвращает все услуги по сервису (кроме услуг раздвижки и стеколки)
 */
function getAllServices($flag = 0) {
	$servicesCode = [];
	$servicesName = [];
	$services = get_data('SELECT * FROM `SERVICE` WHERE `SERVICE_ID` < 188 OR `SERVICE_ID` = 332 AND `SERVICE_ID` <> 94');
	foreach ($services as $servArr) {
		$servicesCode[$servArr['SERVICE_ID']] = $servArr['NAME'];
		$servicesName[$servArr['NAME']] = $servArr['SERVICE_ID'];
	}

	if ($flag === 0) return $servicesCode;
	elseif ($flag === 1) return $servicesName;
	else return false;
}

/**
 * Функция возвращает те услуги, которые необходимы для отчёта
 */
function getFilteredServices($flag = 0) {

	$q = 'SELECT * FROM `SERVICE` WHERE';
	$srvNums = [
		0 => 153, // 'Паз под ДВП'
		1 => 154, // 'Срез торца под 45 градусов'
		2 => 157, // 'Отверстие'
		3 => 158, // 'Сквозное отверстие до 150мм'
		4 => 159, // 'Присадка под петлю'
		5 => 160, // 'Присадка под ручку'
		6 => 161, // 'Присадка под ручку'
		// 7 => 162, // 'Порезка столешницы в замок'
		8 => 163, // 'Фрезерный рез, длина'
		// 9 => 164, // 'Радиус сложный ДСП'
		10 => 165, // 'Фрезерный рез до 450мм'
		// 11 => 166, // 'Доработка угла под 90 градусов'
		12 => 185, // 'Глухое отверстие до 150мм'
		13 => 186, // 'Сложный паз, четверть'
		// 14 => 187, // 'Порезка ЛДСП с группировкой деталей'
	];

	foreach ($srvNums as $key => $value) {
		if ($key === 0) $q .= " `SERVICE_ID` = $value";
		else $q .= " OR `SERVICE_ID` = $value";
	}

	$servicesCode = [];
	$servicesName = [];
	$services = get_data($q);
	foreach ($services as $servArr) {
		if ($servArr['SERVICE_ID'] == 161) {
			$servicesCode[160] = $servArr['NAME'];
			$servicesName[$servArr['NAME']] = 160;
		} else {
			$servicesCode[$servArr['SERVICE_ID']] = $servArr['NAME'];
			$servicesName[$servArr['NAME']] = $servArr['SERVICE_ID'];
		}
	}

	if ($flag === 0) return $servicesCode;
	elseif ($flag === 1) return $servicesName;
	else return false;
}

/**
 * Функция возвращает заказ по ID
 */
function getOrderById($order_id) {
	$orderData = get_data('SELECT * FROM `ORDER1C` WHERE `ID` = "' . $order_id . '"');
	return $orderData[0];
}

/**
 * Функция возвращает последнюю версию проекта по ID заказа
 */
function getLastProjectByOrderId($order_id) {
	$output = [];
	$project = get_data('SELECT * FROM `PROJECT` WHERE `ORDER1C` = "' . $order_id . '" ORDER BY `DATE` DESC');
	$projectData = get_Project_file($project[0]['PROJECT_ID']);

	$output['project'] = $project[0];
	$output['project_data'] = $projectData['project_data'];;
	return $output;
}

/**
 * Функция считает услуги по проекту (обращение к стандартной функции calculate())
 */
function getProjectCalculate($projectData, $place) {
	$output = [];
	$selectedServices = getFilteredServices();
	$data = get_vals_index_without_session($projectData);
	$link = dbConnect();

	$res = calculate ($data['vals'], $data['vals'], $link, $place);
	mysqli_close($link);

	foreach ($res['service_price'] as $key => $value) {
		$service = get_data('SELECT * FROM `SERVICE` WHERE `CODE` = ' . $value['service_code']);
		if (isset($selectedServices[$service[0]['SERVICE_ID']])) {
			$value['service_id'] = $service[0]['SERVICE_ID'];
			$value['service_name'] = $service[0]['NAME'];
			$value['service_description'] = $service[0]['DESCRIPTION'];
			$value['service_unit'] = $service[0]['UNIT'];
			$output[$key] = $value;
		}
	}

	return $output;
}

/**
 * Функция экспорта таблицы в CSV файл
 */
function export_to_csv($CSVkeys, $CSVvalues){

	$reportsDir = DIR_FILES . 'helpdeskReports/';
	$reportsDirAddr = HTTP_FILES . '/helpdeskReports/';
	$dateName = date('Y-m-d_h-i-s', strtotime('now'));
	$tmpContent = [];
	$content = [];
	$serviceCodes = [];
	$content[0] = $CSVkeys;
	$data = null;
	$fields = '';

	// Очистка файлов, старше месяца
	//-------------------------------------------------------------------|
	$reportFiles = GetFileNames($reportsDir, true);
    if (!empty($reportFiles)) {
        foreach ($reportFiles as $value) {
            if (filemtime($reportsDir . $value) < (mktime() - 2678400)) {
                unlink($reportsDir . $value);
            } else break;
        }
    }
    //-------------------------------------------------------------------|

	foreach ($CSVkeys as $key => $value) {
		if ($key == 0) {
			$totalCount[$key] = 'Общее количество по услугам';
			$serviceCodes[$key] = null;
		} else {
			$serviceCodes[$key] = null;
			$totalCount[$key] = 0;
		}
	}

	foreach ($CSVvalues as $order => $services) {
		
		$tmpArr = $serviceCodes;
		$tmpArr[0] = $order;

		foreach ($services as $oneService) {
			
			foreach ($CSVkeys as $serviceCode => $serviceName) {
				
				if ($oneService['code'] == $serviceCode) {
					$tmpArr[$serviceCode] = $oneService['count'];
					$totalCount[$serviceCode] += $oneService['count'];
				}

			}

		}
		
		$tmpContent[] = $tmpArr;
	}

	$content[] = $totalCount;

	foreach ($tmpContent as $value) {
		$content[] = $value;
	}
	
	$userId = $_SESSION['user']['ID'];
	$file = fopen($reportsDir . $dateName . '-' . $userId . '.csv', 'w');

	foreach ($content as $forOneStr) {
		fputcsv($file, $forOneStr, ';');
	}

	if (fclose($file)) return $reportsDirAddr . $dateName . '-' . $userId . '.csv';
    else return false;
}

?>