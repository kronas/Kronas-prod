<?php
// Скрипт выполняет пересчёт услуг по выбранным тикетам c get-параметром "?startexpand=1"
// Скрипт выводит на экран сессию c get-параметром "?startexpand=2"
// Скрипт выводит информацию по услугам всех версий проекта c get-параметром "?order=" (после равно номер заказа либо ID из Акцента)
session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

include_once('../_1/config.php');

$title = 'Отчёт по услугам';
include_once($serv_main_dir.'/helpdesc/templates/header.php');

if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1) {
	if (isset($_GET['startexpand']) && $_GET['startexpand'] == 1) {
		if (isset($_SESSION['user'])) {
			$sess_rec['manager_id'] = $_SESSION['manager_id'];
			$sess_rec['manager_name'] = $_SESSION['manager_name'];
			$sess_rec['isAdmin'] = $_SESSION['isAdmin'];
			$sess_rec['user'] = $_SESSION['user'];
		}
		if (!isset($_SESSION['tickets_recount'])) {
			$_SESSION['tickets_recount'] = getTableData('SELECT * FROM `ticket` ORDER BY `date_created` DESC');
			$hdta = $_SESSION['tickets_recount'];
		} else $hdta = $_SESSION['tickets_recount'];
		$step = 5;
		if (!isset($_SESSION['hdsu'])) {
			$_SESSION['hdsu'] = 0;
		}
		$st_point = $_SESSION['hdsu'];

		if ($st_point-$step >= count($hdta)-1) {
			unset($_SESSION['hdsu']);
			unset($_SESSION['tickets_recount']);
			echo 'Достигнут конец массива.';
			exit;
		} else {
			if (isset($hdta[$st_point]) && isset($hdta[$st_point + $step])) {
				$prepared = array_slice($hdta, $st_point, $step);
			} elseif (isset($hdta[$st_point]) && !isset($hdta[$st_point + $step])) {
				$prepared = array_slice($hdta, $st_point);
			} elseif (!isset($hdta[$st_point])) {
				unset($_SESSION['hdsu']);
				echo 'Достигнут конец массива.';
				exit;
			}

			// основная функция
			cS($prepared);
			
			$st_point += $step;
			_eho('Выполняется пересчёт следующих записей:');
			_print($prepared);
			$_SESSION['hdsu'] = $st_point;
			$_SESSION['tickets_recount'] = $hdta;
			if (isset($sess_rec)) {
				$_SESSION['manager_id'] = $sess_rec['manager_id'];
				$_SESSION['manager_name'] = $sess_rec['manager_name'];
				$_SESSION['isAdmin'] = $sess_rec['isAdmin'];
				$_SESSION['user'] = $sess_rec['user'];
			}

			// sleep(5);
			echo '<script>document.location.href = "'.$main_dir.'/helpdesc/expandservices.php?startexpand=1";</script>';
			exit;
		}
	} elseif (isset($_GET['startexpand']) && $_GET['startexpand'] == 2) {
		// _print(cSa($hdta));
		_print($_SESSION);
	} elseif (isset($_GET['expandorder'])) {
		$prepared = array(0 => $_GET['expandorder']);
		if (isset($_SESSION['user'])) {
			$sess_rec['manager_id'] = $_SESSION['manager_id'];
			$sess_rec['manager_name'] = $_SESSION['manager_name'];
			$sess_rec['isAdmin'] = $_SESSION['isAdmin'];
			$sess_rec['user'] = $_SESSION['user'];
		}
		if ($prepared = getTableData('SELECT * FROM `ticket` WHERE `id` = "'.$_GET['expandorder'].'"')) {
			cS($prepared);
			_eho('Выполняется пересчёт следующих записей:');
			_print($prepared);
		} else {
			_eho('Тикета с таким номером не найдено!');
		}
		if (isset($sess_rec)) {
			$_SESSION['manager_id'] = $sess_rec['manager_id'];
			$_SESSION['manager_name'] = $sess_rec['manager_name'];
			$_SESSION['isAdmin'] = $sess_rec['isAdmin'];
			$_SESSION['user'] = $sess_rec['user'];
		}
	} elseif (isset($_GET['order'])) {
		if (!$tmp = getTableData('SELECT * FROM `ORDER1C` WHERE `ID` = "'.$_GET['order'].'"')) {
	    	if ($tmp = getTableData('SELECT * FROM `ORDER1C` WHERE `DB_AC_ID` = "'.$_GET['order'].'"')) {
	    		$order = $tmp[0]['ID'];
	    		$accent_id = $tmp[0]['DB_AC_ID'];
	    	} elseif ($tmp = getTableData('SELECT * FROM `ORDER1C` WHERE `DB_AC_NUM` = "'.$_GET['order'].'"')) {
	    		$order = $tmp[0]['ID'];
	    		$accent_id = $tmp[0]['DB_AC_ID'];
	    	} else $order = null;
	    } else {
	    	$order = $_GET['order'];
	    	$accent_id = $tmp[0]['DB_AC_ID'];
	    }
		if ($order) {
			$out = array();
			$versions = getOrderInfo($order);
			if (is_array($versions)) {
				$tmp = null;
				echo '<h1>Услуги по заказу № '.$order.' (Номер в Акценте: '.$accent_id.')</h1>';
				foreach ($versions as $key => $value) {
					$tmp = '<div class="container-fluid RS-table">';
					$tmp .= '<h3>'.$key.'</h3>';
					$tmp .= '<div class="row one-line">
								<div class="col col-left col-2"><p class="bd">Код услуги</p></div>
			            		<div class="col col-left col-4"><p class="bd">Название услуги</p></div>
			            		<div class="col col-left col-2"><p class="bd">Количество</p></div>
			            		<div class="col col-left col-1"><p class="bd">Ед.изм</p></div>
			            		<div class="col col-left col-1"><p class="bd">Цена</p></div>
			            		<div class="col col-left col-2"><p class="bd">Сумма</p></div>
							</div>';
					foreach ($value as $k => $v) {
						$tmp .= $v;
					}
					$tmp .= '</div>';
					echo $tmp;
				}
			} elseif (is_string($versions)) {
				_eho($versions);
			}
		} else {
			echo '<p>Номер заказа не найден.</p>';
		}
		// _print($versions);
	} else {
		echo '<div class="container" style="margin-top:50px;"><p>Скрипт выполняет пересчёт услуг по выбранным тикетам и если в базе, в "order1c_goods" нет данной услги по заказу, записывает её в базу. <br> Чтобы запустить пересчёт, нужно дописать к данному url get-параметр <b>?startexpand=1</b></p>
			<p style="color:red;"><b>Осторожно!</b> т.к. скрипт записывает данные в базу!</p>
			<p>Get-параметр <b>?startexpand=2</b> выводит на экран сессию.</p>
			<p>Если нужно вывести информацию по услугам всех версий проекта, допишите к данному url get-параметр <b>?order=</b> и после равно номер заказа либо ID из Акцента.</p>
			<p>Чтобы запустить пересчёт одного заказа, нужно дописать к данному url get-параметр <b>?expandorder=</b> и после равно номер заказа</p></div>';
		exit;
	}
} else echo '<h2>Данную страницу могут просматривать только администраторы.</h2>';

include_once('templates/footer.php');

/**
* Функция перебирает переданные ей тикеты, пересчитывает услуги и пишет их в БД
* в ticket_filter.php нужно добавить: cS(getTableData($q));
**/
function cS($tts) {
    $_SESSION = null;

    //.. Восстанавливаем сессию
	if (isset($session_restore['project_manager'])) {
	    $_SESSION['project_manager'] = $session_restore['project_manager'];
	}
	if (isset($session_restore['project_client'])) {
	    $_SESSION['project_client'] = $session_restore['project_client'];
	}
	if (isset($session_restore['user'])) {
	    $_SESSION['user'] = $session_restore['user'];
	}
	if (isset($session_restore['tec_info'])) {
	    $_SESSION['tec_info'] = $session_restore['tec_info'];
	}

    global $link;
    $tmp = null;
    $out_data = [];
    $out = [];
    // Получаем id проекта для поиска в файловой системе, а так-же филиал
    foreach ($tts as $key => $value) {
        $tmp = getTableData('SELECT `PROJECT_ID` FROM `PROJECT` WHERE `ORDER1C` = "'.$value['order_id'].'" ORDER BY `PROJECT_ID` DESC');
        if ($tmp && getPD($tmp[0]['PROJECT_ID'])) {
        	if ($t = getTableData('SELECT `PLACE` FROM `ORDER1C` WHERE `ID` = "'.$value['order_id'].'"')) {
        		$out[$value['order_id']]['place'] = $t[0]['PLACE'];
        	} else {
        		$out[$value['order_id']]['place'] = 1;
        	}
        	
            $out[$value['order_id']]['project_id'] = $tmp[0]['PROJECT_ID'];
            $out[$value['order_id']]['project_data'] = getPD($tmp[0]['PROJECT_ID']);
        }
    }
    // $c = 0;
    foreach ($out as $key => $value) {
        // if ($c < 5) {
    		$place = $value['place'];
    		$_SESSION['user_place'] = $value['place'];
            $tmp = base64_decode($value['project_data']);
            $tmp = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $tmp);
            $tmp = str_replace("\r", "", $tmp);
            $tmp = str_replace("\n", "", $tmp);
            $tmp = get_vals_index_without_session($tmp);
            $vals = $tmp["vals"];
            $vals = vals_out($vals);
            $vals = add_tag_print($vals);

            $data_xml = vals_index_to_project($vals);
		    $data_xml=gl_recalc (__LINE__,__FILE__,__FUNCTION__,$data_xml);
		    $data=get_vals_index_without_session($data_xml);
		    $vals_recalc = $data["vals"];

            $_SESSION['project_data'] = vals_index_to_project($vals);
            $res = calculate($vals, $vals_recalc, $link, $place);
            if (!empty($res['service_price'])) {
                $out_data[$key] = $res['service_price'];
            }
            $_SESSION = null;
        	
        	//.. Восстанавливаем сессию
			if (isset($session_restore['project_manager'])) {
			    $_SESSION['project_manager'] = $session_restore['project_manager'];
			}
			if (isset($session_restore['project_client'])) {
			    $_SESSION['project_client'] = $session_restore['project_client'];
			}
			if (isset($session_restore['user'])) {
			    $_SESSION['user'] = $session_restore['user'];
			}
			if (isset($session_restore['tec_info'])) {
			    $_SESSION['tec_info'] = $session_restore['tec_info'];
			}
    }
    foreach ($out_data as $key => $value) {
        foreach ($value as $k => $v) {
            if ($tmp = getTableData('SELECT `SERVICE_ID` FROM `SERVICE` WHERE `CODE` = "'.$v['service_code'].'"')) {
            	$tmp = $tmp[0]['SERVICE_ID'];
                $t = getTableData('SELECT * FROM `ORDER1C_GOODS` WHERE `ORDER1C_GOODS_ID` = '.$key.' AND `SERVICE_ID` = '.$tmp.' AND `COUNT` = '.$v['service_count'].'');
                if (!$t) {
                    runQuery('INSERT INTO `ORDER1C_GOODS`(`ORDER1C_GOODS_ID`, `SERVICE_ID`, `COUNT`, `PRICE`) VALUES ('.$key.', '.$tmp.', '.$v['service_count'].', '.$v['service_price'].')');
                }
            }
        }
    }
    return $out_data;
}

function cSa($tts) {
    return $tts;
}

/**
* Информация по всем версиям проекта
**/
function getOrderInfo($order) {
	$_SESSION = null;

	//.. Восстанавливаем сессию
	if (isset($session_restore['project_manager'])) {
	    $_SESSION['project_manager'] = $session_restore['project_manager'];
	}
	if (isset($session_restore['project_client'])) {
	    $_SESSION['project_client'] = $session_restore['project_client'];
	}
	if (isset($session_restore['user'])) {
	    $_SESSION['user'] = $session_restore['user'];
	}
	if (isset($session_restore['tec_info'])) {
	    $_SESSION['tec_info'] = $session_restore['tec_info'];
	}

    global $link;
    $tmp = null;
    $t = null;
    $out_data = [];
    $out = [];
    
	if ($tmp = getTableData('SELECT `PROJECT_ID` FROM `PROJECT` WHERE `ORDER1C` = "'.$order.'" ORDER BY `PROJECT_ID` DESC')) {
    	foreach ($tmp as $key => $value) {
    		if (getPD($value['PROJECT_ID'])) {
		    	if ($t = getTableData('SELECT `PLACE` FROM `ORDER1C` WHERE `ID` = "'.$order.'"')) {
		    		$out[$value['PROJECT_ID']]['place'] = $t[0]['PLACE'];
		    	} else {
		    		$out[$value['PROJECT_ID']]['place'] = 1;
		    	}
		        $out[$value['PROJECT_ID']]['project_data'] = getPD($value['PROJECT_ID']);
		    }
    	}
    } else return 'По заказу '.$order.' проектов не найдено.';

    $t = [];
    foreach ($out as $key => $value) {
		$place = $value['place'];
		$_SESSION['user_place'] = $value['place'];
        $tmp = base64_decode($value['project_data']);
        $tmp = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $tmp);
        $tmp = str_replace("\r", "", $tmp);
        $tmp = str_replace("\n", "", $tmp);
        $tmp = get_vals_index_without_session($tmp);
        $vals = $tmp["vals"];
        $vals = vals_out($vals);
        $vals = add_tag_print($vals);

        $data_xml = vals_index_to_project($vals);
	    $data_xml=gl_recalc (__LINE__,__FILE__,__FUNCTION__,$data_xml);
	    $data=get_vals_index_without_session($data_xml);
	    $vals_recalc = $data["vals"];

        $_SESSION['project_data'] = vals_index_to_project($vals);
        $res = calculate($vals, $vals_recalc, $link, $place);
        if (!empty($res['service_price'])) {
        	foreach ($res['service_price'] as $k => $v) {
        		$tmp = '<div class="row one-line">';
        		$tmp .= '<div class="col col-left col-2"><p>'.$v['service_code'].'</p></div>';
        		$tmp .= '<div class="col col-left col-4"><p>'.$v['service_name'].'</p></div>';
        		$tmp .= '<div class="col col-left col-2"><p>'.round($v['service_count'], 2).'</p></div>';
        		$tmp .= '<div class="col col-left col-1"><p>'.$v['service_unit'].'</p></div>';
        		$tmp .= '<div class="col col-left col-1"><p>'.round($v['service_price'], 2).'</p></div>';
        		$tmp .= '<div class="col col-left col-2"><p>'.round($v['service_count']*$v['service_price'], 2).'</p></div>';
        		$tmp .= '</div>';
        		$out_data[$k] = $tmp;
        	}
        	$t[$key] = $out_data;
        }
        $_SESSION = null;

        //.. Восстанавливаем сессию
		if (isset($session_restore['project_manager'])) {
		    $_SESSION['project_manager'] = $session_restore['project_manager'];
		}
		if (isset($session_restore['project_client'])) {
		    $_SESSION['project_client'] = $session_restore['project_client'];
		}
		if (isset($session_restore['user'])) {
		    $_SESSION['user'] = $session_restore['user'];
		}
		if (isset($session_restore['tec_info'])) {
		    $_SESSION['tec_info'] = $session_restore['tec_info'];
		}
    }
    $out = $t;

	return $out;
}

?>