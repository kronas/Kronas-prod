<?php
session_start();
$title = 'Отчет по Helpdesk';
include_once('templates/header.php');
include_once('functions/database.php');

unset($_SESSION['filters']);
unset($_SESSION['queries']);
unset($_SESSION['tickets']);
_print($_SESSION);
$now_input_date = new DateTime('now');

// Подсчёт тикетов
$count_all_tickets = get_data('SELECT COUNT(id) FROM `ticket`');
$count_new_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `status` = "Новый"');
$count_close_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `status` = "Закрыт"');
$count_work_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `status` = "В работе"');
$count_for_approve_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `status` = "На согласовании"');

// Извлекаем данные из нужных таблиц
$client_all_data_array = get_data('SELECT * FROM `client`');
$manager_all_data_array = get_data('SELECT * FROM `manager` ORDER BY `name`');
$ticket_all_data_array = get_data('SELECT * FROM `ticket`');

$clientNames = [];
$managerNames = [];
$ticketAll = [];
//.. Выборка данных из таблицы "client"
foreach ($client_all_data_array as $value) {
	$clientNames[$value['client_id']] = $value['name'];
}

//.. Выборка данных из таблицы "manager"
foreach ($manager_all_data_array as $value) {
	$managerNames[$value['id']] = $value['name'];
}
$managerNames = array_unique($managerNames);

//.. Выборка данных из таблицы "ticket"
foreach ($ticket_all_data_array as $value) {
	$ticketAll[] = $value;
}
?>

<div class="container conf-1">
	<h1>Отчет по работе обработчиков</h1>
	<div class="row  conf-1">
		<div class="col-md-3">
			<label for="assistant-menu" class="col-2 col-form-label">Обработчик</label>
			<input class="form-control" id="assistant-menu" type="text" placeholder="Обработчик" spellcheck="false">
		</div>
		<div class="col-md-3">
			<label for="status-menu" class="col-2 col-form-label">Статус</label>
			<select class="form-control" id="status-menu">
				<option>Все</option>
				<option>Новый</option>
				<option>В работе</option>
				<option>Закрыт</option>
				<option>На согласовании</option>
			</select>
		</div>
		<div class="col-md-3">
			<label class="col-2 col-form-label" id="period-label">Период</label>
			<div id="date-cell">
				<div id="period">
					<select class="form-control">
						<option>За всё время</option>
						<option>Месяц</option>
						<option>Неделя</option>
						<option>Сегодня</option>
						<option>Выбрать дату</option>
					</select>
					<input class="form-control" type="date" value="<?=$now_input_date->format('Y-m-d')?>">
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<label for="client-menu" class="col-2 col-form-label">Клиент</label>
			<input class="form-control autocomplete" id="client-menu" type="text" placeholder="Клиент" spellcheck="false">
		</div>
	</div>
	<div class="col-md-12 total-count">
			<div class="row">
				<div class="table-hdr col-md-3">Всего запросов</div>
				<div class="table-hdr col-md-3">«Закрытых»</div>
				<div class="table-hdr col-md-3">«В работе»</div>
				<div class="table-hdr col-md-3">«На согласовании»</div>
			</div>
			<div class="row" id="tickets-line">
				<div class="col-md-3 table-item"><?=$count_all_tickets[0]['COUNT(id)']?></div>
				<div class="col-md-3 table-item"><?=$count_close_tickets[0]['COUNT(id)']?></div>
				<div class="col-md-3 table-item"><?=$count_work_tickets[0]['COUNT(id)']?></div>
				<div class="col-md-3 table-item"><?=$count_for_approve_tickets[0]['COUNT(id)']?></div>
			</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="reportTables" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="inquiry-tab" data-toggle="tab" href="#inquiry" role="tab" aria-controls="home" aria-selected="true">Таблица по запросам</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="assistant-tab" data-toggle="tab" href="#assistant" role="tab" aria-controls="profile" aria-selected="false">Таблица по обработчикам</a>
				</li>
			</ul>
			<div class="tab-content" id="reportTablesContent">

				<div class="tab-pane fade show active" id="inquiry" role="tabpanel" aria-labelledby="inquiry-tab">
					<div class="row">
						<div class="table-hdr col-user-20">Запрос №</div>
						<div class="table-hdr col-user-20">Дата</div>
						<div class="table-hdr col-user-20">Статус</div>
						<div class="table-hdr col-user-20">Обработчик</div>
						<div class="table-hdr col-user-20">Клиент</div>
					</div>
					<div id="ticketsTable">
						<?php foreach ($ticketAll as $value): ?>
							<?php
								if ($value['manager']) {
									$value['manager'] = $managerNames[$value['manager']];
								} else $value['manager'] = '—';
							?>
							<div class="row table-item-line">
								<div class="col-user-20 table-item"><?=$value['order_id']?></div>
								<div class="col-user-20 table-item"><?=$value['date_created']?></div>
								<div class="col-user-20 table-item"><?=$value['status']?></div>
								<div class="col-user-20 table-item"><?=$value['manager']?></div>
								<div class="col-user-20 table-item"><?=$clientNames[$value['client']]?></div>
							</div>
						<?php endforeach ?>
					</div>
				</div>
				<div class="tab-pane fade" id="assistant" role="tabpanel" aria-labelledby="assistant-tab">
					<div class="row">
						<div class="table-hdr col-user-20">Обработчик</div>
						<div class="table-hdr col-user-20">Всего запросов</div>
						<div class="table-hdr col-user-20">Закрытых</div>
						<div class="table-hdr col-user-20">В работе</div>
						<div class="table-hdr col-user-20">На согласовании</div>
					</div>
					<?php foreach ($manager_all_data_array as $value): ?>
						<?php
							// Подсчёт тикетов для обработчика
							$all_mng_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `manager` = "'.$value['id'].'"');
							$new_mng_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `manager` = "'.$value['id'].'" AND `status` = "Новый"');
							$close_mng_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `manager` = "'.$value['id'].'" AND `status` = "Закрыт"');
							$work_mng_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `manager` = "'.$value['id'].'" AND `status` = "В работе"');
							$for_approve_mng_tickets = get_data('SELECT COUNT(id) FROM `ticket` WHERE `manager` = "'.$value['id'].'" AND `status` = "На согласовании"');
						?>
						<div class="row table-item-line" id="managersTable">
							<div class="col-user-20 table-item"><?=$value['name']?></div>
							<div class="col-user-20 table-item"><?=$all_mng_tickets[0]['COUNT(id)']?></div>
							<div class="col-user-20 table-item"><?=$close_mng_tickets[0]['COUNT(id)']?></div>
							<div class="col-user-20 table-item"><?=$work_mng_tickets[0]['COUNT(id)']?></div>
							<div class="col-user-20 table-item"><?=$for_approve_mng_tickets[0]['COUNT(id)']?></div>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include_once('templates/footer.php'); ?>