<div class="form-group row">
    <h4 class="col-sm-12">Услуги  </h4> <!-- <i class="fas fa-plus-circle" id="add_new_mpr_service" style="color:#28a745;"></i> -->
    <small>(табличная чать обязательна для заполнения)</small>
</div>
<div class="form-group row" id="mpr_services_list">
    <?php $counter_table_price = 1; ?>
    <?php foreach($mpr['prices'] as $k=>$v): ?>
        <div class="form-group row add_new_mpr_service_row_<?= $counter_table_price ?> add_new_mpr_service_row">
            <div class="col-md-1">
                #<?= $counter_table_price ?>
            </div>
            <div class="col-sm-5">
                <label>Введите артикул услуги:</label>
                <select name="add_mpr_service_model_<?= $counter_table_price ?>" class="custom-select" id="" required="">
                    <?php foreach($services as $s): ?>
                        <option value="<?= $s['SERVICE_ID'] ?>" <?php if($s['SERVICE_ID'] == $v['SERVICE_ID']) echo 'selected'; ?>><?= $s['NAME'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-5">
                <label>Введите Количество услуг:</label>
                <input type="number" class="form-control" value="<?= $v['count'] ?>" placeholder="" name="add_mpr_service_count_<?= $counter_table_price ?>" required="">
            </div>
            <!--                                <div class="col-sm-1">-->
            <!--                                    <i class="fas fa-minus-circle del_row_mpr_icon_--><?//= $counter_table_price ?><!--" data-del="--><?//= $counter_table_price ?><!--" style="color: #dc3545"></i>-->
            <!--                                </div>-->
        </div>
        <script>
            $('.del_row_mpr_icon_<?= $counter_table_price ?>').on('click', function (e) {
                $('.add_new_mpr_service_row_<?= $counter_table_price ?>').remove();
                let table_rows_group = $('[class *= "add_new_mpr_service_row_"]');
                if(table_rows_group.length == 0) {
                    $('#submit_new_mp_form').hide('slow');
                }
            });
        </script>
        <?php $counter_table_price++; ?>
    <?php endforeach; ?>
</div>



<fieldset class="form-group">
    <div class="row">
        <legend class="col-form-label col-sm-2 pt-0">Тип операций:</legend>
        <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="new_mpr_operation_type" id="gridRadios1" value="0" <?php if($mpr['by_size'] == 0) echo 'checked'; ?> readonly>
                <label class="form-check-label" for="gridRadios1">
                    Сверление
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="new_mpr_operation_type" id="gridRadios2" value="1" <?php if($mpr['by_size'] == 1) echo 'checked'; ?> readonly>
                <label class="form-check-label" for="gridRadios2">
                    сверления, фрезерования
                </label>
            </div>
        </div>
    </div>
</fieldset>




<fieldset class="form-group">
    <div class="row">
        <legend class="col-form-label col-sm-2 pt-0">Сторона:</legend>
        <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="new_mpr_operation_side" id="gridRadios1" value="1" <?php if($mpr['side'] == 1) echo 'checked'; ?> readonly>
                <label class="form-check-label" for="gridRadios1">
                    Лицевая
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="new_mpr_operation_side" id="gridRadios2" value="0" <?php if($mpr['side'] == 0) echo 'checked'; ?> readonly>
                <label class="form-check-label" for="gridRadios2">
                    Обратная
                </label>
            </div>
        </div>
    </div>
</fieldset>