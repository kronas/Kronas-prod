<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 15:44
 */
session_start();
include_once '../_1/config.php';
include_once($serv_main_dir.'/helpdesc/templates/header.php');
include_once($serv_main_dir.'/helpdesc/functions/database.php');
// if(isset($_SESSION['user'])) {
//     header('Location: '.$main_dir.'/helpdesc/');
// }

$order_id = $_GET['order_id'];

$sql_check_ticket = 'SELECT * FROM `ticket` WHERE `status` != "закрыт" AND `order_id` = '.$order_id;
$check_ticket = get_data($sql_check_ticket);
if(count($check_ticket) > 0) {
    if (stripos($_SERVER['SERVER_NAME'], '.loc') || stripos($_SERVER['SERVER_NAME'], '.local')) {
            echo '<meta http-equiv="refresh" content="0; url='.$main_dir.'/helpdesc/ticket.php?id='.$check_ticket[0]['id'].'&nw='.$_GET['nw'].'">';
    } else {
        header('Location: '.$main_dir.'/helpdesc/ticket.php?id='.$check_ticket[0]['id'].'&nw='.$_GET['nw']);
    }
    exit;
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="<?= $main_dir ?>/helpdesc/api/ticket.php?create_new_ticket=1&order_id=<?= $_GET['order_id'] ?>&client_id=<?= $_GET['client_id'] ?>&manager_id=<?= $_GET['manager_id'].'&nw='.$_GET['nw']?>" method="POST">
                <input type="hidden" name="" value="<?= $_GET['order_id'] ?>">
                <input type="hidden" name="" value="<?= $_GET['client_id'] ?>">
                <input type="hidden" name="" value="<?= $_GET['manager_id'] ?>">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Введите сообщение обработчику</label>
                    <textarea name="first_message" id="" cols="30" rows="10" placeholder="Детальное описание проблемы" style="width: 100%;"></textarea>
                </div>
                <button class="btn btn-success">
                    Создать запрос
                </button>
            </form>
        </div>
    </div>
</div>

<?php include_once('templates/footer.php'); ?>