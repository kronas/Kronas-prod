<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 15:44
 */
?>
<?php
$title = 'Helpdesk - тикеты';
include('templates/header.php');
include_once('functions/check_permission.php');

if(isset($_GET['page']))
    $tickets = get_tickets($_SESSION['user'], $_GET, $_GET['page']);
else
    $tickets = get_tickets($_SESSION['user'], $_GET);

?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Запросы на помощь</h1>
        </div>
    </div>
    <hr>
    <div class="row ticket_main_filters">
        <div class="row">
            <div class="col-md-12">
                <select name="show" id="show-select">
                    <option value="all" selected>Все заказы</option>
                    <option value="mine">Мои</option>
                    <option value="new">Новые</option>
                    <option value="closed" selected>Закрытые</option>
                    <option value="inWork">В работе</option>
                    <option value="forApproval">На согласовании</option>
                </select>
                <div>
                    <b>Показать:</b>
                </div>
                <div>
                    <form action="./" method="GET">
                        <ul class="nav">
                            <li class="nav-item">
                                <button class="btn nav-link active" type="submit" name="main_filter" value="all">Все</button>
                            </li>
                            <?php if($_SESSION['user']['role'] == 'manager'): ?>
                            <li class="nav-item">
                                <button class="btn nav-link active" type="submit" name="main_filter" value="my">Мои</button>
                            </li>
                            <?php endif; ?>
                            <li class="nav-item">
                                <button class="btn nav-link active" type="submit" name="main_filter" value="new">Новые</button>
                            </li>
                            <li class="nav-item">
                                <button class="btn nav-link active" type="submit" name="main_filter" value="close">Закрытые</button>
                            </li>
                            <li class="nav-item">
                                <button class="btn nav-link active" type="submit" name="main_filter" value="work">В работе</button>
                            </li>
                            <li class="nav-item">
                                <button class="btn nav-link active" type="submit" name="main_filter" value="accept">На согласовании</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row tickets_row">
        <?php if(count($tickets['data']) > 0): ?>
        <?php foreach($tickets['data'] as $ticket): ?>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><b>Запрос <span class="badge badge-secondary">№<?= $ticket['id'] ?></span></b><?= $ticket['status'] ?></h5>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Ответственный:</b> <?= $ticket['manager']['name'] ?></li>
                        <li class="list-group-item"><b>Клиент:</b> <?= $ticket['client']['name'] ?></li>
                        <li class="list-group-item"><b>Дата:</b> <?= $ticket['date_created'] ?></li>
                        <li class="list-group-item"><b>Проект:</b> <?= get_order($ticket['order_id'])['DB_AC_ID'] ?> / (<?= get_order($ticket['order_id'])['DB_AC_NUM'] ?>)</li>
                    </ul>

                    <div class="card-body">
                        <a class="btn btn-info" href="./ticket.php?id=<?= $ticket['id'] ?>">Открыть</a>
<!--                        --><?php //if(!isset($ticket['manager']['id']) && $_SESSION['user']['role'] == 'manager'): ?>
<!--                            <form action="./api/ticket.php" class="float-right"  method="POST">-->
<!--                                <input type="hidden" name="ticket_id" value="--><?//= $ticket['id'] ?><!--">-->
<!--                                <input type="hidden" name="client_email" value="--><?//= $ticket['client']['email'] ?><!--">-->
<!--                                <button class="btn btn-success" type="submit" name="get_this_ticket" value="1">Взять в работу</button>-->
<!--                            </form>-->
<!--                        --><?php //endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?php else: ?>
            <h3 class="row_no_tickets">Запросов не найдено!</h3>
        <?php endif; ?>
    </div>
<?php if(count($tickets['data']) > 0): ?>
    <p>Страниц:</p>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php for($i = 1; $i <= $tickets['pages']; $i++): ?>
            <li class="page-item <?php if($_GET['page'] == $i) echo 'active'; ?>"><a class="page-link" href="./?page=<?= $i ?>&main_filter=<?= $_GET['main_filter']?>"><?= $i ?></a></li>
            <?php endfor; ?>
        </ul>
    </nav>
<?php endif; ?>
</div>


<?php include('templates/footer.php'); ?>