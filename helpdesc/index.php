<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 15:44
 */
include_once '../_1/config.php';
session_start();
if (!isset($_SESSION['user'])) {
    header('Location: '.$main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
}
$title = 'Helpdesk - тикеты';
include_once($serv_main_dir.'/helpdesc/templates/header.php');
include_once($serv_main_dir.'/helpdesc/api/ticket_filters.php');
include_once($serv_main_dir.'/helpdesc/functions/check_permission.php');
include_once($serv_main_dir.'/helpdesc/templates/modal_popup.php');
// echo $thisPage;

// if(isset($_GET['page'])) $tickets = get_tickets($_SESSION['user'], $_GET, $_GET['page']);
// else $tickets = get_tickets($_SESSION['user'], $_GET);

$now_input_date = new DateTime('now');

//..08.12.2020 Вид меню (плитка/список)
if (!isset($_SESSION['menuView'])) {
    $menuView = 'tile';
} else $menuView = $_SESSION['menuView'];

?>
<div class="preloader-box"><div class="preloader"></div></div>
<div class="container">
    <div class="ticket-header">
        <h1>Запросы на помощь</h1>
        <?php if ($totalTickets > 0): ?>
            <span class="count-tickets">(всего: <?=$totalTickets?>)</span>
        <?php endif ?>
    </div>
    <div class="row  conf-1 ticket-filters">
        <div class="col col-md-4 col-sm-6 col-xs-6">
            <label for="ticketnums-menu" class="col-2 col-form-label">№ запроса</label>
            <?php if (isset($_GET['ticketnum'])): ?>
                <input class="form-control" id="ticketnums-menu" type="number" value="<?=$_GET['ticketnum']?>" spellcheck="false">
            <?php else: ?>
                <input class="form-control" id="ticketnums-menu" type="number" placeholder="№ запроса" spellcheck="false">
            <?php endif ?>
        </div>
        <div class="col col-md-4 col-sm-6 col-xs-6">
            <label for="assistant-menu" class="col-2 col-form-label">Обработчик</label>
            <?php if (isset($_GET['assistant'])): ?>
                <input class="form-control" id="assistant-menu" type="text" value="<?=$assistantName?>" spellcheck="false">
            <?php else: ?>
                <input class="form-control" id="assistant-menu" type="text" placeholder="Обработчик" spellcheck="false">
            <?php endif ?>
        </div>
        <div class="col col-md-4 col-sm-6 col-xs-6">
            <label for="client-menu" class="col-2 col-form-label">Клиент</label>
            <?php if (isset($_GET['client'])): ?>
                <input class="form-control autocomplete" id="client-menu" type="text" value="<?=$clientName?>" spellcheck="false">
            <?php else: ?>
                <input class="form-control autocomplete" id="client-menu" type="text" placeholder="Клиент" spellcheck="false">
            <?php endif ?>
        </div>
        <div class="col col-md-4 col-sm-6 col-xs-6">
            <label for="status-menu" class="col-2 col-form-label">Статус</label>
            <select name="show" id="status-menu" class="form-control">
                <?php foreach ($status_select as $key => $value): ?>
                    <?php if (isset($_GET['status']) && $_GET['status'] == $key): ?>
                        <option value="<?=$key?>" selected><?=$value?></option>
                    <?php else: ?>
                        <option value="<?=$key?>"><?=$value?></option>
                    <?php endif ?>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col col-md-4 col-sm-6 col-xs-6">
            <?php if (isset($_GET['date']) && preg_match('#^[\d]{4}-[\d]{2}-[\d]{2}$#', $_GET['date'])): ?>
                <label class="col-2 col-form-label" id="period-label"><span id="period-span" class="green-select">Период</span></label>
            <?php else: ?>
                <label class="col-2 col-form-label" id="period-label">Период</label>
            <?php endif ?>
            <div id="date-cell">
                <div id="period">
                    <select style="display:<?=$select?>;" class="form-control">
                        <?php foreach ($date_select as $key => $value): ?>
                            <?php if (isset($_GET['date']) && $_GET['date'] == $key): ?>
                                <option value="<?=$key?>" selected><?=$value?></option>
                            <?php else: ?>
                                <?php if ($key == 'select'): ?>
                                    <option class="bd"><?=$value?></option>
                                <?php else: ?>
                                    <option value="<?=$key?>"><?=$value?></option>
                                <?php endif ?>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                    <?php if (isset($_GET['date']) && preg_match('#^[\d]{4}-[\d]{2}-[\d]{2}$#', $_GET['date'])): ?>
                        <input style="display:<?=$input?>;" class="form-control" type="date" value="<?=$_GET['date']?>">
                    <?php else: ?>
                        <input style="display:<?=$input?>;" class="form-control" type="date" value="<?=$now_input_date->format('Y-m-d')?>">
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="col col-md-4 col-sm-12 col-xs-12"><button id="clearfilters" class="btn btn-info">Сбросить фильтры</button></div>
    </div> <!-- end row -->

    <div class="row">
    	<?php if (count($outTickets) > 0): ?>
    		<div class="col col-md-8 col-sm-8 col-12">
    	<?php else: ?>
    		<div class="col col-md-10 col-sm-10 col-12">
    	<?php endif ?>
            <?php if($totalTickets > $TOP): ?>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <?=$pagination?>
              </ul>
            </nav>
            <?php endif; ?>
        </div>
        <?php if (count($outTickets) > 0): ?>
        <div class="col col-md-2 col-sm-2 col-6">
            <?php
                // Инпут ниже нужен для ajax-запроса. Сервер возвращает таблицу услуг по тикетам,
                // и чтобы не передавать серверу массив отфильтрованных тикетов,
                // передаём ему строку запроса к БД, чтобы он сам его получил.
            ?>
            <?php if ($_SESSION['user']['role'] === 'admin'): ?>
	        <input type="hidden" name="fqr" value="<?=htmlspecialchars($q, ENT_QUOTES)?>">
        	<nav class="menu-view menu-view-right">
	        	<ul class="pagination">
	        		<li class="page-item">
	        			<a id="callBtn" class="page-link" nohref rel="nofollow" title="Отчёт по услугам"><i class="fa fa-rp fa-table" aria-hidden="true"></i></a>
	        		</li>
	        	</ul>
	        </nav>
            <?php endif ?>
        </div>
    	<?php endif ?>
        <div class="col col-md-2 col-sm-2 col-6">
            <nav class="menu-view menu-view-right">
                <ul class="pagination">
                    <?php if ($menuView === 'tile') { $t = 'active'; $l = null; } else { $t = null; $l = 'active'; } ?>
                    <li class="page-item"><a class="page-link" nohref rel="nofollow" title="Вид меню плитками"><i class="fa fa-mv fa-th-large <?=$t?>" aria-hidden="true"></i></a></li>
                    <li class="page-item"><a class="page-link" nohref rel="nofollow" title="Вид меню списком"><i class="fa fa-mv fa-th-list <?=$l?>" aria-hidden="true"></i></a></li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="container nopadding">
        <div class="row tickets_row">
            <?php
            if(count($outTickets) > 0) {
                foreach($outTickets as $ticket) {
                    // _print($ticket);
                    include 'templates/one_ticket.php';
                }
            } else { ?>
                <h3 class="row_no_tickets">Запросов не найдено!</h3>
            <?php } ?>
        </div> <!-- end row -->
    </div>

    <?php if($totalTickets > $TOP): ?>
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <?=$pagination?>
          </ul>
        </nav>
    <?php endif; ?>
</div>


<?php include_once('templates/footer.php'); ?>