<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 17.06.2020
 * Time: 15:44
 */

?>

<?php include_once('templates/header.php'); ?>

<?php include_once('functions/check_permission.php'); ?>

<?php

$ticket = get_ticket($_GET['id']);
$mpr = get_mpr($_GET['mpr_id'])[0];
$order = get_order($ticket['order_id']);
$services = get_services_array();

?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>
                <span><small>Редактирование MPR файла #<?= $mpr['mpr_id'] ?></small>:<br><?= $mpr['name'] ?> <small style="display: none">(<?= $ticket['type']; ?>)</small></span>
            </h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body">
                <form action="./api/mpr.php<?='?nw='.$_GET['nw']?>" enctype="multipart/form-data" method="POST" id="new_mp_form">
                    <input type="hidden" name="new_mpr_order" value="<?= $ticket['order_id'] ?>">
                    <input type="hidden" name="new_mpr_ticket_ids" value="<?= $ticket['id'] ?>">
                    <input type="hidden" name="edit_mpr_ids" value="<?= $mpr['mpr_id'] ?>">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Название файла</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?= $mpr['name'] ?>" placeholder="введите названия мпр-файла" name="new_mpr_name" required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Длина</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" value="<?= $mpr['L'] ?>" placeholder="" name="new_mpr_lenght" required readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Ширина</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" value="<?= $mpr['W'] ?>" placeholder="" name="new_mpr_width" required readonly>
                        </div>
                    </div>
                    <fieldset class="form-group" style="display: none!important;">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Тип операций:</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="new_mpr_operation_type" id="gridRadios1" value="0" <?php if($mpr['by_size'] == 0) echo 'checked'; ?>>
                                    <label class="form-check-label" for="gridRadios1">
                                        Сверление
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="new_mpr_operation_type" id="gridRadios2" value="1" <?php if($mpr['by_size'] == 1) echo 'checked'; ?>>
                                    <label class="form-check-label" for="gridRadios2">
                                        сверления, фрезерования
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group" style="display: none!important;">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Сторона:</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="new_mpr_operation_side" id="gridRadios1" value="1" <?php if($mpr['side'] == 1) echo 'checked'; ?>>
                                    <label class="form-check-label" for="gridRadios1">
                                        Лицевая
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="new_mpr_operation_side" id="gridRadios2" value="0" <?php if($mpr['side'] == 0) echo 'checked'; ?>>
                                    <label class="form-check-label" for="gridRadios2">
                                        Обратная
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Криволенейное кромкование по сторонам:</legend>
                            <div class="col-sm-10">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="kl_l" id="kl_l" value="1" <?php if($mpr['kll'] == 1) echo 'checked'; ?>>
                                    <label class="custom-control-label" for="kl_l">Лево</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="kl_r" id="kl_r" value="1" <?php if($mpr['klr'] == 1) echo 'checked'; ?>>
                                    <label class="custom-control-label" for="kl_r">Право</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="kl_t" id="kl_t" value="1" <?php if($mpr['klt'] == 1) echo 'checked'; ?>>
                                    <label class="custom-control-label" for="kl_t">Верх</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="kl_b" id="kl_b" value="1" <?php if($mpr['klb'] == 1) echo 'checked'; ?>>
                                    <label class="custom-control-label" for="kl_b">Низ</label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <div class="input-group is-invalid">
                            <label class="col-sm-2 col-form-label">Картинка:</label>
                            <div class="col-sm-10">
                                <div class="mpr_edit_image_container">
                                    <img src="<?= $mpr['pic'] ?>" alt="">
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile1" name="new_mpr_operation_image">
                                    <label class="custom-file-label" for="validatedInputGroupCustomFile1">Выбрать файл...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" style="display: none!important;">
                        <div class="input-group is-invalid">
                            <label class="col-sm-2 col-form-label">Файл МПР:</label>
                            <div class="col-sm-10">
                                <div class="mpr_edit_image_container">
                                    <p><b>Текущий файл: </b> <?= $mpr['file'] ?></p>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile" name="new_mpr_operation_mpr_file">
                                    <label class="custom-file-label" for="validatedInputGroupCustomFile">Выбрать файл...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" style="display: none!important;">
                        <h4 class="col-sm-12">Услуги  <i class="fas fa-plus-circle" id="add_new_mpr_service" style="color:#28a745;"></i></button></h4>
                        <small>(табличная чать обязательна для заполнения)</small>
                    </div>
                    <div class="form-group row" id="mpr_services_list" style="display: none!important;">
                        <?php $counter_table_price = 1; ?>
                        <?php foreach($mpr['prices'] as $k=>$v): ?>
                            <div class="form-group row add_new_mpr_service_row_<?= $counter_table_price ?> add_new_mpr_service_row">
                                <div class="col-md-1">
                                    #<?= $counter_table_price ?>
                                </div>
                                <div class="col-sm-5">
                                    <label>Введите артикул услуги:</label>
                                    <select name="add_mpr_service_model_<?= $counter_table_price ?>" class="custom-select" id="" required="">
                                        <?php foreach($services as $s): ?>
                                        <option value="<?= $s['SERVICE_ID'] ?>" <?php if($s['SERVICE_ID'] == $v['SERVICE_ID']) echo 'selected'; ?>><?= $s['NAME'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-5">
                                    <label>Введите Количество услуг:</label>
                                    <input type="number" class="form-control" value="<?= $v['count'] ?>" placeholder="" name="add_mpr_service_count_<?= $counter_table_price ?>" required="">
                                </div>
                                <div class="col-sm-1">
                                    <i class="fas fa-minus-circle del_row_mpr_icon_<?= $counter_table_price ?>" data-del="<?= $counter_table_price ?>" style="color: #dc3545"></i>
                                </div>
                            </div>
                            <script>
                                $('.del_row_mpr_icon_<?= $counter_table_price ?>').on('click', function (e) {
                                    $('.add_new_mpr_service_row_<?= $counter_table_price ?>').remove();
                                    let table_rows_group = $('[class *= "add_new_mpr_service_row_"]');
                                    if(table_rows_group.length == 0) {
                                        $('#submit_new_mp_form').hide('slow');
                                    }
                                });
                            </script>
                        <?php $counter_table_price++; ?>
                        <?php endforeach; ?>
                    </div>
                    <script>
                        var mpr_add_service_counter = <?= $counter_table_price ?>;
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" name="edit_new_mpr" id="submit_new_mp_form" value="1" class="btn btn-success">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>


<?php include_once('templates/footer.php'); ?>