<?php
/**
 * Created by PhpStorm.
 * User: Tony Angelo
 * Date: 17.06.2020
 * Time: 15:44
 */
mb_internal_encoding("UTF-8");
include_once '../_1/config.php';
session_start();
if (!isset($_SESSION['user'])) {
    header('Location: '.$serv_main_dir.'/helpdesc/login.php'.'?nw='.$_GET['nw']);
}
include_once('templates/header.php');
include_once('functions/check_permission.php');


$ticket = get_ticket($_GET['id']);
if (isset($ticket['manager'])) {
    $_SESSION['handler'] = $ticket['manager'];
}
$messages = get_ticket_messages($_GET['id']);
$projects = get_ticket_projects($_GET['id'], $ticket['order_id'], $ticket['manager']['id']);
$assets = get_ticket_assets($_GET['id']);
$managers = get_managers();
$order = get_order($ticket['order_id']);
$mpr = get_ticket_mpr($_GET['id']);

//.. Получим менеджера проекта
$sql = 'SELECT * FROM `manager` WHERE `id` = "'.$order['manager'].'" LIMIT 1';
$projectManagerId = getTableData($sql)[0]['id'];
$projectManagerName = getTableData($sql)[0]['name'];
$projectManagerEmail = getTableData($sql)[0]['e-mail'];

if($projects['manager']) {
    $details_mpr_connect = get_mpr_details_connect($ticket['order_id'], $projects['manager'][0]['id']);
}
// p_($details_mpr_connect);
// _print($assets);
$construct = array();
$price = array();
$assetss = getTableData('SELECT * FROM `ticket_assets` WHERE `id` = "804"');
foreach ($assetss as $assett) {
    $construct = getTableData('SELECT * FROM `ticket_construct_mpr` WHERE `ticket_id` = "'.$assett['ticket_id'].'"');
    // _print(count($construct));_eho();
    foreach ($construct as $value) {
        $price = getTableData('SELECT * FROM `ticket_mpr_price` WHERE `mpr_id` = "'.$value['mpr_id'].'"');
        // _print(count($price));_eho();
    }
}



// _print($assetss);
// _eho('----------------------------');
// _print($construct);
// _eho('----------------------------');
// _print($price);
?>



<script>
    var mpr_add_service_counter = 0;
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>
                <span>Запрос #<?= $ticket['id'] ?> <small style="display: none">(<?= $ticket['type']; ?>)</small></span>
                <p class="text-right">
                    <?= $ticket['status'] ?>
                </p>
            </h1>

        </div>
    </div>
    <hr>
    <?php if($_SESSION['user']['role'] == 'manager' || $_SESSION['user']['role'] == 'admin'): ?>
    <div class="row">
        <div class="col-md-3">
            <?php if($ticket['manager']['id'] == $_SESSION['user']['ID']): ?>
            <form action="./api/ticket.php<?='?nw='.$_GET['nw']?>" id="set_ticket_status" method="POST">
                <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                <input type="hidden" name="manager_email" value="<?= $projectManagerEmail ?>">
                <input type="hidden" name="client_email" value="<?= $ticket['client']['email'] ?>">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Сменить статус:</label>
                    <select class="form-control  form-control-sm" id="select_status" name="set_status">
                        <option value="" disabled selected>Выбрать...</option>
                        <option value="новый">Новый</option>
                        <option value="в работе">В работе</option>
                        <option value="закрыт">Закрыт</option>
                        <option value="на согласовании">На соглосование</option>
                    </select>
                </div>
            </form>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <?php if($ticket['manager']['id'] == $_SESSION['user']['ID']): ?>
            <form action="./api/ticket.php<?='?nw='.$_GET['nw']?>" method="POST" id="set_ticket_manager">
                <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Передать другому обработчику:</label>
                    <select class="form-control  form-control-sm" id="select_manager" name="set_manager">
                        <option value="" disabled selected>Выбрать...</option>
                        <?php foreach($managers as $manager): ?>
                            <option value="<?= $manager['id'] ?>"><?= $manager['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </form>
            <?php endif; ?>
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3">
            <?php if(!isset($ticket['manager']['id']) && ($_SESSION['user']['role'] == 'manager' || $_SESSION['user']['role'] == 'admin')): ?>
            <form action="./api/ticket.php<?='?nw='.$_GET['nw']?>" class="float-right"  method="POST">
                <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                <input type="hidden" name="manager_id" value="<?= $order['manager'] ?>">
                <input type="hidden" name="order_id" value="<?= $ticket['order_id'] ?>">
                <input type="hidden" name="project" value="<?= $projects['client'][0]['project'] ?>">
                <input type="hidden" name="client_email" value="<?= $ticket['client']['email'] ?>">
                <button class="btn btn-success" type="submit" name="get_this_ticket" value="1">Взять в работу</button>
            </form>
            <?php endif ?>
            
        </div>
    </div>
    <hr>
    <?php endif; ?>

    <div class="row ticket_item_heading">
        <div class="col-md-4">
            <b>Ответственный: </b><span><?= $ticket['manager']['name'] ?></span>
        </div>
        <div class="col-md-4">
            <b>Клиент: </b><a href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $ticket['client']['name'] ?></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="padding: 9px">
                <p><small><b>ФИО:</b> <?= $ticket['client']['name'] ?></small></p>
                <p><small><b>Телефон:</b> <?= $ticket['client']['phone'] ?></small></p>
                <p><small><b>E-mail:</b> <?= $ticket['client']['email'] ?></small></p>
            </div>
        </div>
        <div class="col-md-4">
            <b>Дата: </b><span><?= $ticket['date_created'] ?></span>
        </div>
        <div class="col-md-4">
            <b>Проект: </b><span><?= $order['DB_AC_ID'] ?> (<?= $order['DB_AC_NUM'] ?>)</span>
        </div>
        <div class="col-md-4">
            <b>Менеджер проекта: </b><span><?= $projectManagerName ?></span>
        </div>
    </div>
    <hr>
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="margin-bottom: 9px;">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Информация</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">MPR файлы</a>
            <?php if($projects['manager']): ?>
            <a class="nav-item nav-link" id="nav-details-tab" data-toggle="tab" href="#nav-details" role="tab" aria-controls="nav-details" aria-selected="false">MPR к Деталям</a>
            <?php endif; ?>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <div class="row ticket_item_main_container">
                <div class="col-md-9">
                    <div class="ticket_item_messages_main_container">
                        <div class="ticket_item_chat">
                            <?php if(count($messages) > 0): ?>
                                <?php $author_name_check = ''; ?>
                                <?php foreach($messages as $message): ?>
                                    <div class="ticket_item_chat_message <?php echo $message['author']['id'] == $_SESSION['user']['ID'] ? 'right' : 'left'; ?>">
                                        <?php if($author_name_check != $message['author']['name']): ?>
                                            <b><?= $message['author']['name'] ?></b>
                                        <?php endif; ?>
                                        <div class="ticket_item_chat_message_text alert alert-primary" role="alert">
                                            <?= $message['message'] ?>
                                        </div>
                                        <small><?= $message['date'] ?></small>
                                    </div>
                                    <?php $author_name_check = $message['author']['name']; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div><small>Сообщений еще нету. Что бы оставить сообщение - воспользуйтесь формой ниже</small></div>
                            <?php endif; ?>
                        </div>
                        <div class="ticket_item_input_area">
                            <form action="./api/ticket.php<?='?nw='.$_GET['nw']?>" method="POST">
                                <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                                <input type="hidden" name="handler_email" value="<?= $ticket['manager']['email'] ?>">
                                <input type="hidden" name="client_email" value="<?= $ticket['client']['email'] ?>">
                                <div class="form-group">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Введите Ваше сообщение" name="message"></textarea>
                                </div>
                                <button class="btn btn-success" type="submit" name="send_message" value="1">Написать</button>
                            </form>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#file_upload_modal">
                                <svg class="bi bi-arrow-up-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                    <path fill-rule="evenodd" d="M4.646 8.354a.5.5 0 0 0 .708 0L8 5.707l2.646 2.647a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 0 0 0 .708z"/>
                                    <path fill-rule="evenodd" d="M8 11.5a.5.5 0 0 0 .5-.5V6a.5.5 0 0 0-1 0v5a.5.5 0 0 0 .5.5z"/>
                                </svg>
                                <span>Загрузить файл</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" <?php if($projects['client'] && count($projects['client']) > 3) echo 'style="visibility: visible;max-height: 237px;overflow-y: scroll;"'; ?>>
                        <div class="toast-header">
                            <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
                            <strong class="mr-auto">Проект от клиента</strong>

                        </div>
                        <?php if($projects['client']): ?>
                            <?php foreach($projects['client'] as $project): ?>
                                <div class="toast-body">
                                    <div>
                                        <div><b>Версия №<?= $project['id'] ?></b></div>
                                        <small><i><time><?= $project['date'] ?></time></i></small>
                                    </div>
                                    <div>
                                        <a href="<?= $laravel_dir ?>/projects_new/1_<?= $ticket['order_id'] ?>?order_project_id=<?= $project['id']?>&is_handler=<?php echo $_SESSION['user']['role'] == 'manager' ? 1 : 0; ?>" target = "_blank">
                                            <svg class="bi bi-arrow-down-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(-90deg);">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                <path fill-rule="evenodd" d="M4.646 7.646a.5.5 0 0 1 .708 0L8 10.293l2.646-2.647a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 0-.708z"/>
                                                <path fill-rule="evenodd" d="M8 4.5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5z"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?php if($_SESSION['user']['role'] == 'client'): ?>
                                <div class="toast-body">
                                    <small>Проект еще не был загружен.</small>
                                </div>
                            <?php endif; ?>
                            <?php if($_SESSION['user']['role'] == 'manager'): ?>
                                <div class="toast-body">
                                    <small>Проект еще не был загружен клиентом</small>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false" <?php if($projects['manager'] && count($projects['manager']) > 3) echo 'style="visibility: visible;max-height: 237px;overflow-y: scroll;"'; ?>>
                        <div class="toast-header">
                            <svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#007aff"></rect></svg>
                            <strong class="mr-auto">Проект от обработчика</strong>
                        </div>
                        <?php if($projects['manager']): ?>
                            <?php foreach($projects['manager'] as $project): ?>
                                <div class="toast-body">
                                    <div>
                                        <div><b>Версия №<?= $project['id'] ?></b></div>
                                        <small><i><time><?= $project['date'] ?></time></i></small>
                                    </div>
                                    <div>
                                        <a href="<?= $laravel_dir ?>/projects_new/1_<?= $ticket['order_id'] ?>?order_project_id=<?= $project['id']?>&is_handler=<?php echo $_SESSION['user']['role'] == 'manager' ? 1 : 0; ?>" target = "_blank">
                                            <svg class="bi bi-arrow-down-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(-90deg);">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                <path fill-rule="evenodd" d="M4.646 7.646a.5.5 0 0 1 .708 0L8 10.293l2.646-2.647a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 0-.708z"/>
                                                <path fill-rule="evenodd" d="M8 4.5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5z"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?php if($_SESSION['user']['role'] == 'client'): ?>
                                <div class="toast-body">
                                    <small>Проект еще не был загружен обработчиком.</small>
                                </div>
                            <?php endif; ?>
                            <?php if($_SESSION['user']['role'] == 'manager'): ?>
                                <div class="toast-body">
                                    <small>Проект еще не был загружен.</small>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <?php if($assets): ?>
                        <div class="ticket_item_messages_peoples">
                            <div>
                                <b>Прикрепленные файлы:</b>
                            </div>
                            <ul class="list-group">
                                <?php foreach($assets as $asset): ?>
                                    <li class="list-group-item list-group-item-light">
                                        <span>
                                            <?= $asset['name'] ?>
                                        </span>
                                        <a href="<?= $asset['file'] ?>" target="_blank" download>
                                            <svg class="bi bi-arrow-down-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                <path fill-rule="evenodd" d="M4.646 7.646a.5.5 0 0 1 .708 0L8 10.293l2.646-2.647a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 0-.708z"/>
                                                <path fill-rule="evenodd" d="M8 4.5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5z"/>
                                            </svg>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <table class="table table-bordered mpr_info_table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Операция</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($mpr as $m): ?>
                    <tr>
                        <td>
                            <div class="detail_mpr_info_container">
                                <div class="detail_mpr_info_inner">
                                    <div class="detail_mpr_info_inner_image">
                                        <?php if($m['pic']): ?>
                                        <img src="<?= $m['pic'] ?>" alt="">
                                        <?php else: ?>
                                        <p class="text-center text-danger text-small">Необходимо загрузить<br> изображение МПРа!</p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="detail_mpr_info_inner_info">
                                        <ul>
                                            <li><b>Название:</b> <span><?= $m['name'] ?></span></li>
                                            <li><b>Сторона:</b> <span><?php echo $m['side'] == 1 ? 'лицевая' : 'обратная'; ?></span></li>
                                            <li><b>Тип операций:</b> <span><?php echo $m['by_size'] == 1 ? 'сверления и фрезерования' : 'сверления'; ?></span></li>
                                            <li><b>Длина:</b> <span><?= $m['L'] ?></span></li>
                                            <li><b>Ширина:</b> <span><?= $m['W'] ?></span></li>
                                            <?php if(in_array(1, [$m['kll'],$m['klr'],$m['klt'],$m['klb']])): ?>
                                            <li>
                                                <b>Криволенейное кромкование:</b>
                                                <?php
                                                    foreach ([$m['kll'],$m['klr'],$m['klt'],$m['klb']] as $k=>$v) {
                                                        if($v == 1) {
                                                                switch($k) {
                                                                    case 0:
                                                                        echo '<span>лево | </span>';
                                                                        break;
                                                                    case 1:
                                                                        echo '<span>право | </span>';
                                                                        break;
                                                                    case 2:
                                                                        echo '<span>верх | </span>';
                                                                        break;
                                                                    case 3:
                                                                        echo '<span>низ | </span>';
                                                                        break;
                                                                }
                                                        }
                                                    }
                                                ?>
                                            </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <div class="detail_mpr_info_inner_table">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">Услуга</th>
                                                <th scope="col">Количество</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($m['prices'] as $p): ?>
                                                <tr>
                                                    <td><?= $p['NAME'] ?></td>
                                                    <td><?= $p['count'] ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <div class="mpr_action">
                                    <?php if($details_mpr_connect['mpr_to_part'] && isset($details_mpr_connect['mpr_to_part'][$m['mpr_id']])): ?>
                                    <div>
                                        <span class="badge badge-success">Прикреплен к детали № <?= $details_mpr_connect['mpr_to_part'][$m['mpr_id']] ?> </span>
                                    </div>
                                    <?php else: ?>
                                    <div class="pull-right">
                                        <a href="./mpr_edit.php?id=<?= $ticket['id'] ?>&mpr_id=<?= $m['mpr_id'] ?>" title="Редактировать"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="./api/mpr.php?ticket_id=<?= $ticket['id'] ?>&delete_this_mpr=<?= $m['mpr_id'] ?>" title="Удалить"><i class="fas fa-minus-circle" style="color: tomato;"></i></a>
                                        <div class="form-check">
                                          <input class="form-check-input" type="checkbox" id="<?=$m['mpr_id']?>" title="Отметить (для удаления)">
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                    <tr>
                        <td>
                            <button type="button" class="btn btn-outline-success"data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-plus-circle"></i> Добавить MPR файл</button>
                            <div class="collapse" id="collapseExample">
                                <div class="card card-body">
                                    <form action="./api/mpr.php<?='?nw='.$_GET['nw']?>" enctype="multipart/form-data" method="POST" id="new_mp_form">
                                        <input type="hidden" name="new_mpr_order" value="<?= $ticket['order_id'] ?>">
                                        <input type="hidden" name="new_mpr_ticket_ids" value="<?= $ticket['id'] ?>">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Название файла</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="введите названия мпр-файла" id="new_mpr_name_ids" name="new_mpr_name" required readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Длина</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" placeholder="" name="new_mpr_lenght" required>
                                                <div class="invalid-feedback">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Ширина</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" placeholder="" name="new_mpr_width" required>
                                                <div class="invalid-feedback">

                                                </div>
                                            </div>
                                        </div>
                                        <fieldset class="form-group">
                                            <div class="row">
                                                <legend class="col-form-label col-sm-2 pt-0">Тип операций:</legend>
                                                <div class="col-sm-10">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="new_mpr_operation_type" id="gridRadios1" value="0" checked>
                                                        <label class="form-check-label" for="gridRadios1">
                                                            Сверление
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="new_mpr_operation_type" id="gridRadios2" value="1">
                                                        <label class="form-check-label" for="gridRadios2">
                                                            сверления, фрезерования
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <div class="row">
                                                <legend class="col-form-label col-sm-2 pt-0">Сторона:</legend>
                                                <div class="col-sm-10">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="new_mpr_operation_side" id="gridRadios1" value="1" checked>
                                                        <label class="form-check-label" for="gridRadios1">
                                                            Лицевая
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="new_mpr_operation_side" id="gridRadios2" value="0">
                                                        <label class="form-check-label" for="gridRadios2">
                                                            Обратная
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <div class="row">
                                                <legend class="col-form-label col-sm-2 pt-0">Криволенейное кромкование по сторонам:</legend>
                                                <div class="col-sm-10">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="kl_l" id="kl_l" value="1">
                                                        <label class="custom-control-label" for="kl_l">Лево</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="kl_r" id="kl_r" value="1">
                                                        <label class="custom-control-label" for="kl_r">Право</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="kl_t" id="kl_t" value="1">
                                                        <label class="custom-control-label" for="kl_t">Верх</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="kl_b" id="kl_b" value="1">
                                                        <label class="custom-control-label" for="kl_b">Низ</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="form-group row">
                                            <div class="input-group is-invalid">
                                                <label class="col-sm-2 col-form-label">Картинка:</label>
                                                <div class="col-sm-10">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile1" name="new_mpr_operation_image" required>
                                                        <label class="custom-file-label" for="validatedInputGroupCustomFile1">Выбрать файл...</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="input-group is-invalid">
                                                <label class="col-sm-2 col-form-label">Файл МПР:</label>
                                                <div class="col-sm-10">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile" name="new_mpr_operation_mpr_file" required>
                                                        <label class="custom-file-label" for="validatedInputGroupCustomFile">Выбрать файл...</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="mpr_tables_prices_container" style="display: none;">
                                            <h4 class="col-sm-12">Услуги  <i class="fas fa-plus-circle" id="add_new_mpr_service" style="color:#28a745;"></i></button></h4>
                                            <small>(табличная чать обязательна для заполнения)</small>
                                        </div>
                                        <div class="form-group row" id="mpr_services_list" style="display: none;">
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-10">
                                                <button type="submit" name="add_new_mpr" id="submit_new_mp_form" value="1" class="btn btn-success" style="display: none;">Создать MPR файл</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <button type="button" class="btn btn-outline-success"data-toggle="collapse" data-target="#collapseExampleMPR" aria-expanded="false" aria-controls="collapseExampleMPR"><i class="fas fa-plus-circle"></i> Импорт MPR </button>
                            <div class="collapse" id="collapseExampleMPR">
                                <form action="<?= $laravel_dir ?>/mpr_import_action<?='?nw='.$_GET['nw']?>" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="is_handler" value="1">
                                    <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                                    <input type="hidden" name="project_data" value="<?= $projects['manager'][0]['project'] ?>">
                                    <br>
                                    <p>Допускается загрузка одного .mpr файла или множества файлов архивом .zip (без вложненных каталогов внутри архива)</p>
                                    <div class="form-group row">
                                        <div class="input-group is-invalid">
                                            <label class="col-sm-2 col-form-label">Файл(ы) МПР:</label>
                                            <div class="col-sm-10">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile1" name="mpr_import_field" required>
                                                    <label class="custom-file-label" for="validatedInputGroupCustomFile1">Выбрать файл...</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <button type="submit" name="import_new_mpr" value="1" class="btn btn-success" style="">Импортировать</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="del-button"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="nav-details" role="tabpanel" aria-labelledby="nav-details-tab">
            <form action="<?= $main_dir ?>/good_get.php<?='?nw='.$_GET['nw']?>" method="POST">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Деталь №</th>
                            <th scope="col">Соответствующий MPR файл:</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php foreach ($details_mpr_connect['part'] as $id => $p): ?>
                        <tr>
                            <th scope="row"><?= 'id = '.$p['id'].' кол-во ' ?><?= $p['count'] ?> шт, <?= $p['name'] ?> (<?= $p['dl'] ?> * <?= $p['dw'] ?> * <?= $p['t'] ?> // <?= $p['cl'] ?> * <?= $p['cw'] ?> * <?= $p['t'] ?>)</th>
                            <td>
                                <?php
                                    // if ($p['id']==107004){
                                    //     p_($details_mpr_connect['part'][107004]);
                                    // } 
                                    $mpr_check_counter == 0;
                                    unset($tt);
                                    foreach ($details_mpr_connect['mpr'] as $m) 
                                    {
                                        
                                        // if (
                                        //     (($p['cl'] == $m['L']) && ($p['cw'] == $m['W']) && ($m['by_size'] == 1))
                                        //     OR
                                        //     (($p['cl'] == $m['W']) && ($p['cw'] == $m['L']) && ($m['by_size'] == 1))
                                        //     OR
                                        //     (($p['jl'] == $m['L']) && ($p['jw'] == $m['W']) && ($m['by_size'] == 1))
                                        //     OR
                                        //     (($p['jl'] == $m['W']) && ($p['jw'] == $m['L']) && ($m['by_size'] == 1))
                                        //     OR
                                        //     (($p['dl'] == $m['L']) && ($p['dw'] == $m['W']) && ($m['by_size'] == 0))
                                        //     OR
                                        //     (($p['dl'] == $m['W']) && ($p['dw'] == $m['L']) && ($m['by_size'] == 0))
                                        // ) 
                                        $nnew=str_replace('_','',$m['name']);
                                        $nnew=str_replace('.','',$nnew);
                                        $nnew1=str_replace('.','',$p['code']);
                                        $nnew1=str_replace('_','',$nnew1);
                                        if (
                                            (($p['cl'] == $m['L']) && ($p['cw'] == $m['W']) )
                                            OR
                                            (($p['cl'] == $m['W']) && ($p['cw'] == $m['L']) )
                                            OR
                                            (($p['jl'] == $m['L']) && ($p['jw'] == $m['W']) )
                                            OR
                                            (($p['jl'] == $m['W']) && ($p['jw'] == $m['L']))
                                            OR
                                            (($p['dl'] == $m['L']) && ($p['dw'] == $m['W']) )
                                            OR
                                            (($p['dl'] == $m['W']) && ($p['dw'] == $m['L']))
                                        ) 
                                        
                                        {
                                            // p_($p);
                                            // p_($m);
                                            // exit;
                                            unset ($checked);
                                            foreach ($details_mpr_connect['index']['OPERATION'] as $i) {

                                                if (($details_mpr_connect['vals'][$i]['attributes']['TYPEID'] == 'XNC'))
                                                {
                                                    // p_($details_mpr_connect['vals'][$i]);
                                                    if (($details_mpr_connect['vals'][$i]['attributes']['operation.my_mpr_' . $m['mpr_id']] == 1)) {
                                                        // echo 'next if true';
                                                        $j = $i;
                                                        while ($details_mpr_connect['vals'][$j]['tag'] <> "PART") {
                                                            $j++;
                                                        }
                                                        if ($details_mpr_connect['vals'][$j]['attributes']['ID'] == $id) {
                                                            $checked = 'checked';
                                                        }

                                                    }
                                                }
                                            }
                                            // test ('nnew',$nnew);
                                            // test ('nnew1',$nnew1);
                                            if ((!isset($checked)&&(substr_count($nnew,$nnew1)>0)))$checked = 'checked';
                                            if($ticket['manager']['id'] == $_SESSION['user']['ID']) {
                                                $disabled = '';
                                            } else {
                                                $disabled = 'disabled';
                                            }
//                                            echo '<input type="checkbox" name="mpr_' . $id . '_' . $m['mpr_id'] . '" value="1" ' . $checked . '>' . $m['file'];
                                            echo '<div class="custom-control custom-checkbox mr-sm-2">
                                                    <input type="checkbox" class="custom-control-input" id="mpr_' . $id . '_' . $m['mpr_id'] . '_id" name="mpr_' . $id . '_' . $m['mpr_id'] . '" value="1" '.$checked.' '.$disabled.'>
                                                    <label class="custom-control-label" for="mpr_' . $id . '_' . $m['mpr_id'] . '_id" name="mpr_' . $id . '_' . $m['mpr_id'] . '">'.$m['name'].' ('.$m['L'].'*'.$m['W'].')</label>
                                                  </div>';
                                            unset ($checked);
                                            // p_($m);
                                            $mpr_check_counter++;
                                        }
                                        elseif ((substr_count($nnew,$nnew1)>0))
                                        {
                                            $tt.=' - '.$m['name'].'<br>';
                                        }
                                        // echo 'counter: '.$mpr_check_counter;
                                    }
                                   
                                if($mpr_check_counter == 0) {
                                    echo '<p>Подходящих MPR файлов не обнаружено';
                                    if (isset($tt))
                                    {
                                        echo '<br>'.$tt;
                                    }
                                    
                                    echo '</p>';
                                }
                                unset($mpr_check_counter);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <input type="hidden" name="project_data" value="<?= base64_encode($details_mpr_connect['project_data']) ?>">
                <input type="hidden" name="order_id" value="<?= $ticket['order_id'] ?>">
                <input type="hidden" name="manager_id" value="<?= $ticket['manager']['id'] ?>">
                <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                <?php if($ticket['manager']['id'] == $_SESSION['user']['ID']): ?>
                    <input type="submit" class="btn btn-success" name="go" value="Сохранить">
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>

</div>

    <!-- Модалка загрузки файлов -->
    <div class="modal fade" id="file_upload_modal" tabindex="-1" role="dialog" aria-labelledby="file_upload_modal_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="./api/ticket.php<?='?nw='.$_GET['nw']?>" enctype="multipart/form-data" method="POST">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Загрузка файлов</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                            <input type="hidden" name="ticket_id" value="<?= $ticket['id'] ?>">
                            <div class="input-group is-invalid">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile" name="userfile" required>
                                    <label class="custom-file-label" for="validatedInputGroupCustomFile">Выбрать файл...</label>
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit" name="send_file" value="1">Загрузить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
<?php include_once('templates/footer.php'); ?>