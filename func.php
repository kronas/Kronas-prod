<?php

require_once 'kronasapp/vendor/autoload.php';

require_once 'PHPMailer/Exception.php';
require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/SMTP.php';

require_once 'dompdf/getRSPDF.php';
require_once 'dompdf/getRSRfPPDF.php';
 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

if (!defined('DIR_CORE')) {
    require_once '_1/config.php';
}

// Основные функции Сервиса
require_once DIR_CORE . 'func_math.php';
require_once DIR_FUNCTIONS . 'fs_build_req_for_project.php';


function get_detail_html_data($kromka,$sizes, $files, $id, $width, $height)
{
    if($width > $height) {
        $new_width = '300';
        $new_height = (300 * $height) / $width;
        $new_max_width = '365';
    } else {
        $new_width = (300 * $width) / $height;
        $new_max_width = ((300 * $width) / $height) + 65;
        $new_height = '300';
    }
    $new_height .= 'px';
    $new_width .= 'px';
    $new_max_width .= 'px';


    $l_kromka_style = '';
    $t_kromka_style = '';
    $r_kromka_style = '';
    $b_kromka_style = '';

    if(!$kromka['l']['letter']) $l_kromka_style = 'style="opacity: 0;"';

    if(!$kromka['t']['letter']) $t_kromka_style = 'style="opacity: 0;"';
    else $t_kromka_style = 'style="border-top: 10px solid ' . $kromka['t']['color'] . ';"';

    if(!$kromka['r']['letter']) $r_kromka_style = 'style="opacity: 0;"';
    
    if(!$kromka['b']['letter']) $b_kromka_style = 'style="opacity: 0;"';
    else $b_kromka_style = 'style="border-bottom: 10px solid  ' . $kromka['b']['color'] . ';"';

    $l_kromka_name = '';
    $t_kromka_name = '';
    $r_kromka_name = '';
    $b_kromka_name = '';

    if($kromka['l']['letter']) {
        $l_kromka_name = ' 
            <div class="detail_kromka_history_item" style="color: ' . $kromka['l']['color'] . ';">
                <b>' . $kromka['l']['letter'] . '</b><span> - ' . $kromka['l']['name'] . '</span>
            </div>
            ';
    }
    if($kromka['t']['letter']) {
        $t_kromka_name = ' 
            <div class="detail_kromka_history_item" style="color: '.$kromka['t']['color'] . ';">
                <b>' . $kromka['t']['letter'] . '</b><span> - ' . $kromka['t']['name'] . '</span>
            </div>
            ';
    }
    if($kromka['r']['letter']) {
        $r_kromka_name = ' 
            <div class="detail_kromka_history_item" style="color: '.$kromka['r']['color'] . ';">
                <b>' . $kromka['r']['letter'] . '</b><span> - ' . $kromka['r']['name'] . '</span>
            </div>
            ';
    }
    if($kromka['b']['letter']) {
        $b_kromka_name = ' 
            <div class="detail_kromka_history_item" style="color: '.$kromka['b']['color'] . ';">
                <b>' . $kromka['b']['letter'] . '</b><span> - ' . $kromka['b']['name'] . '</span>
            </div>
            ';
    }

    $l_kromka_type = '';
    $t_kromka_type = '';
    $r_kromka_type = '';
    $b_kromka_type = '';

    if($kromka['l']['type']) $l_kromka_type = '(' . $kromka['l']['type'] . ')';
    if($kromka['t']['type']) $t_kromka_type = '(' . $kromka['t']['type'] . ')';
    if($kromka['r']['type']) $r_kromka_type = '(' . $kromka['r']['type'] . ')';
    if($kromka['b']['type']) $b_kromka_type = '(' . $kromka['b']['type'] . ')';

    $pdf_files = '';
    $pdf_files_counter = 1;

    foreach ($files as $file) {
        $public_link = $_SESSION['part_images'][$file['files']]['public'];
        $pdf_files .= <<<zxc
            <div class="detail_images_links_item">
                   <a href="{$public_link}" class="part_name_container" data-img="{$file['files']}" target="_blank">
                       <span>Чертеж №{$pdf_files_counter} {$file['side']}</span>
                       <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                             <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                             <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                        </svg>
                   </a>
             </div>
        zxc;
        ++$pdf_files_counter;

    }

    $html = <<<rr
        <div style="max-width: 607px;">
            <div class="detail_image_specification_container">
                <div class="detail_image_specification">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center vertical_kromka_title">{$sizes['vertical']}</h3>
                        </div>
                    </div>
                    <div class="row main_container_row">
                        <div class="col-md-1 left-kromka-container">
                            <h3 class="text-center">{$sizes['horizontal']}</h3>
                        </div>
                        <div class="col-md-1 left_background_kromka" {$l_kromka_style}>
                            <div style="background: {$kromka['l']['color']}"></div>
                        </div>
                        <div class="col-md-1 left_kromka_name_marker" {$l_kromka_style}><span class="kromka_marker_text" style="color: {$kromka['l']['color']}">{$kromka['l']['letter']} {$l_kromka_type}</span></div>
                        <div class="col-md-6" style="max-width: {$new_max_width};">
                            <div class="top_background_kromka kromka_name_marker" {$t_kromka_style}>
                                <span class="kromka_marker_text" style="color: {$kromka['t']['color']}">{$kromka['t']['letter']} {$t_kromka_type}</span>
                            </div>
                            <div>
                                <div class="detail_image_specification_inner" style="height: {$new_height}; width: {$new_width}; opacity: 1;">
                                    <div class="detail_image_specification_inner_background"></div>
                                    <div class="detail_image_specification_inner_background"></div>
                                    <div class="detail_image_specification_inner_background"></div>
                                </div>
                            </div>
                            <div class="bottom_background_kromka kromka_name_marker" {$b_kromka_style}>
                                <span class="kromka_marker_text" style="color: {$kromka['b']['color']}">{$kromka['b']['letter']} {$b_kromka_type}</span>
                            </div>
                        </div>
                        <div class="col-md-1 right_kromka_name_marker" {$r_kromka_style}>
                            <span class="kromka_marker_text" style="color: {$kromka['r']['color']}">{$kromka['r']['letter']} {$b_kromka_type}</span>
                        </div>
                        <div class="col-md-1 right_background_kromka" {$r_kromka_style}>
                            <div style="background: {$kromka['r']['color']}"></div>
                        </div>
                        <div class="col-md-1 right-kromka-container">
                            <h3 class="text-center"></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center vertical_kromka_title"></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: flex; flex-flow: row nowrap; justify-content: space-between;">
                <div class="detail_images_links">
                    {$pdf_files}
                </div>
                <div class="detail_kromka_history">
                    {$t_kromka_name}
                    {$r_kromka_name}
                    {$b_kromka_name}
                    {$l_kromka_name}
                </div>
            </div>
        </div>
    rr;

    return $html;
}

?>