@echo off

php artisan optimize
php artisan cache:clear
php artisan route:cache
php artisan view:clear
php artisan config:cache
php artisan serve --host=0.0.0.0 --port 8080

pause