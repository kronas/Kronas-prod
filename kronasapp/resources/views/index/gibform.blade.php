<!doctype HTML>
<html>
<head>
<title>GibLabForm</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<!-- Languag (Язык) -->
<meta name="gwt:property" content="locale=ru">
<!-- Loading the form script/Загрузка скрипта формы
Attributes (Атрибуты):
data-name: Form name (Имя формы);
data-action: JS Function for request processing from form (Имя JS функции обработки запросов от форму);
data-info: Information line processing JS function (Функция обработки информационной строки)
-->
<script type="text/javascript" src="giblabform.nocache.js" data-name="test.managerform" data-info="info" data-action="action"></script>
<!-- <script type='text/javascript' src='//code.jquery.com/jquery-3.4.1.min.js'></script> -->

</head>
<body style="height: 100%;">
	<script type="text/javascript">
		var materials = "{{$materials}}";
	</script>
	<script type="text/javascript" src="js/gibform.js"></script>

	<!-- The layout of this page is for only example (Разметка данной страницы сделана только для примера) -->
	<table style="width: 100%; height: 100%; position: absolute;">
		<tr style="height: 20px;">
			<td id="projectInfo" colspan="3" style="border: solid 1px gray;">Header</td>
		</tr>
		<tr style="height: 100%;">
			<td style="width: 120px; border: solid 1px gray;">Left panel</td>
			<td>
				<!-- "giblab.container" - ID of the div tag in which the form will be loaded (идентификатор тега div в который будет загружена форма) -->
				<div id="giblab.container" style="width: 100%; height: 100%; border: solid 1px gray;"></div>
			</td>
			<td style="width: 120px; border: solid 1px gray;">Right panel</td>
		</tr>
		<tr style="height: 42px;">
			<td><button type="button" onclick="onClickSave()">Save</button></td>
			<!-- Last save time (Время последнего сохранения) -->
			<td id="saveTime"></td>
			<td></td>
		</tr>
		<tr style="height: 150px;">
			<td colspan="3" style="border: solid 1px gray;"><textarea id="saveContainer" style="width: 100%; height: 100%;"></textarea></td>
		</tr>
	</table>
</body>
</html>