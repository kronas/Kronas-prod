
        <!doctype HTML>
<html>
<head>
    <title>Приём заказов на обработку плитных материалов Кронас.</title>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <!-- Languag (Язык) -->
    <meta name="gwt:property" content="locale=ru">
    <!-- Loading the form script/Загрузка скрипта формы
    Attributes (Атрибуты):
    data-name: Form name (Имя формы);
    data-action: JS Function for request processing from form (Имя JS функции обработки запросов от форму);
    data-info: Information line processing JS function (Функция обработки информационной строки)


    {/!! $catalog !!}
    {/!! $stock !!}


    -->
    <script type='text/javascript' src='/js/jquery-3.4.1.min.js'></script>
    <script type="text/javascript" src="/giblabform.nocache.js" data-name="test.managerform" data-action="action"></script>
    <link rel="stylesheet" href="/css/main.css">

</head>

<div id="start_checks_lodaer_containers">
    <div>
        <img src="/images/download.gif" alt="">
    </div>
</div>
<script>
    $('#start_checks_lodaer_containers').show();
    $(document).ready(function () {
        setTimeout(function () {
            $('#start_checks_lodaer_containers').hide();
        },3000);
    });
</script>
<body style="height: 100%;">
<!-- Social networks -->
<script>
    const script = document.createElement('script');
    script.src = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.umd.js';
    script.onload = function () {
        const body = document.querySelector('body');
        const widgetEl = document.createElement('div');
        widgetEl.setAttribute('id', 'wt-omnichannel-widget');
        body.appendChild(widgetEl);            

        const config = {
            "view": {
                "lang": "en",
                "btnOpacity": "1",
                "logoUrl": "https://kronas.com.ua/Media/pic/icons/KR_logo_25x25px.svg",
                "accentColor": "hsl(292, 56.99999999999999%, 21%)"
            },
            "chat": {
                "url": "wss://cloud.webitel.ua/chat/hddvjbgfoitieuhtgvaygashqutmuan"
            },
            "alternativeChannels": {
                "viber": "viber://pa?chatURI=kronasbot",
                "telegram": "https://t.me/Kronascomua_bot",
                "facebook": "https://www.facebook.com/kronas.com.ua/"
            }
        };

        const app = new WtOmniWidget('#wt-omnichannel-widget', config);
    };
    document.head.appendChild(script);

    const link = document.createElement('link');
    link.href = 'https://cloud.webitel.ua/omni-widget/WtOmniWidget.css';
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.media = 'screen,print';
    document.head.appendChild(link);
</script>
<!-- Social networks -->
<script type="text/javascript">
    var tree = {!! $tree !!};
    var material_id_array = [];
    var manufacturers = [];
    var volume = [];
    var width_height = [];
    var access = {!! $access !!};
    var isAdmin = {!! $CtRhTn !!};
    var catalog = {};
    var xnc_folders = {!! $xnc_folders !!};
    var stock = {};
    var tools = {!! $folder_tools !!};
    var new_csrf_token = '{{ csrf_token() }}';
    var isChecked = {{ $has_checked_project }};
    var session_id = '{{ $session_id }}';
    var places = {!! $places_js !!};
    var app_dir = '{!! $app_dir !!}';
    var main_dir = '{!! $main_dir !!}';
    var user_type_edge_session = '{!! $user_type_edge !!}';

    //.. 06.09.2022 Тоценко А. Параметр, есть ли раскрой при загрузке .kronas файла
    var manager_id = '{!! $manager_id !!}';
    var user_sheet_cutting = '{!! $user_sheet_cutting !!}';
    console.log(new_csrf_token);
</script>

<script type="text/javascript">
    //..
    var projectID, rsflag = false, inputTimer = setInterval(getFlag, 1000);
    if (location.href.indexOf('order_project_id')) {
        projectID = location.href.split('order_project_id=').pop();
    }

    function getFlag() {
        //.. Здесь вместо "orderDate=" нужно подставить нужный параметр
        var el = $('#saveContainer'), param = $('#saveContainer').val();
        if (el.length && param.indexOf('CfvjdfhjgfhfdjpDfhdfhbx') != -1){
            rsflag = true;
            $('#user_type_edge').css('display', 'none');
            $('#extra_operations_button').css('display', 'none');
            $('#mpr_import_button').css('display', 'none');
            $('#client_materials').css('display', 'none');
            $('#create-nproject-btn').css('display', 'none');
            $('#user_save_project_file').css('display', 'none');
            $('#get_con_material_button').css('display', 'none');
            $('#admin_open_project_form').css('display', 'none');
            $('#sep_ticket_form').css('display', 'none');
            $('#user_upload_viyar').css('display', 'none');
            $('#sep_project_form_button').css('display', 'none');
            $('#save_ajax_new_version_button').css('display', 'none');
            $('#client_materials_menu').css('display', 'none');
            $('.handlers_request_button').css('display', 'none');
            $('#grb').removeClass("showEl");
            $('#grb').addClass("hideEl");
            $('#grb_RS').removeClass("hideEl");
            $('#grb_RS').addClass("showEl");
            // getStartData();
            clearInterval(inputTimer);
        }
        else rsflag = false;
    }
    inputTimer;
    function getStartData() {
        let permissions = '';
        if (isAdmin) {
            permissions = `
                <param name='perm.cs.panel.properties' value='enable'/>
                <param name='perm.el.panel.properties' value='enable'/>
                <param name='perm.sp.panel.properties' value='enable'/>
                <param name='perm.sp.panel.parts' value='enable '/>
                <param name='perm.xnc.panel.properties' value='enable'/>
                <param name='perm.panel.operations.add.cn' value='enable'/>
                `;
        } else {
            if (rsflag === true) {
                permissions = `
                  <param name='perm.check' value='disable'/>
                  <param name='perm.import' value='disable'/>
                  <param name='perm.excel' value='disable'/>
                  <param name='perm.other' value='disable'/>
                  <param name='perm.pro100' value='disable'/>
                  <param name='perm.imos' value='disable'/>
                  <param name='perm.bazis' value='disable'/>
                  <param name='perm.bcad' value='disable'/>
                  <param name='perm.woody2' value='disable'/>
                  <param name='perm.woodwop' value='disable'/>
                  <param name='perm.ilenia' value='disable'/>

                  <param name='perm.panel.products' value='disable'/>

                  <param name='perm.panel.operations.add.cs' value='disable'/>
                  <param name='perm.panel.operations.add.el' value='disable'/>
                  <param name='perm.panel.operations.add.gr' value='disable'/>
                  <param name='perm.panel.operations.add.o' value='disable'/>
                  <param name='perm.panel.operations.add.xnc' value='hide'/>
                  <param name='perm.panel.operations.add.lb' value='hide'/>
                  <param name='perm.panel.operations.add.sp' value='hide'/>
                  <param name='perm.panel.operations.add.cl' value='hide'/>

                  <param name='perm.panel.operations.edit.cs' value='hide'/>
                  <param name='perm.panel.operations.edit.el' value='disable'/>
                  <param name='perm.panel.operations.edit.gr' value='disable'/>
                  <param name='perm.panel.operations.edit.o' value='hide'/>
                  <param name='perm.panel.operations.edit.xnc' value='hide'/>
                  <param name='perm.panel.operations.edit.lb' value='hide'/>
                  <param name='perm.panel.operations.edit.sp' value='hide'/>

                  <param name='perm.panel.operations.cost' value='hide'/>
                  <param name='perm.panel.operations.print' value='hide'/>

                  <param name='perm.o.panel.properties' value='disable'/>

                  <param name='perm.cs.panel.properties' value='hide'/>
                  <param name='perm.cs.panel.parts' value='disable'/>
                  <param name='perm.cs.panel.panels' value='disable'/>
                  <param name='perm.cs.panel.patterns' value='disable'/>
                  <param name='perm.cs.action.savedefault' value='hide'/>
                  <param name='perm.cSizeMode.workpiece' value='disable'/>
                  <param name='perm.cs.panel.panels.preferences' value='hide'/>



                  <param name='perm.cl.panel.properties' value='hide'/>
                  <param name='perm.cl.panel.parts' value='hide'/>
                  <param name='perm.cl.panel.panels' value='hide'/>
                  <param name='perm.cl.panel.patterns' value='hide'/>
                  <param name='perm.cl.action.savedefault' value='hide'/>

                  <param name='perm.el.panel.properties' value='hide'/>
                  <param name='perm.el.panel.parts' value='disable'/>
                  <param name='perm.el.action.savedefault' value='hide'/>

                  <param name='perm.gr.panel.properties' value='disable'/>
                  <param name='perm.gr.panel.parts' value='disable'/>
                  <param name='perm.gr.action.savedefault' value='hide'/>

                  <param name='perm.sp.panel.properties' value='disable'/>
                  <param name='perm.sp.panel.parts' value='disable'/>
                  <param name='perm.sp.action.savedefault' value='disable'/>

                  <param name='perm.xnc.panel.properties' value='disable'/>
                  <param name='perm.xnc.panel.template' value='disable'/>
                  <param name='perm.xnc.panel.cost' value='disable'/>
                  <param name='perm.xnc.action.savedefault' value='hide'/>

                  <param name='perm.lb.panel.properties' value='hide'/>
                  <param name='perm.lb.panel.custom' value='hide'/>
                  <param name='perm.lb.panel.parts' value='hide'/>
                  <param name='perm.lb.panel.template' value='hide'/>
                  <param name='perm.lb.action.savedefault' value='hide'/>
                  <param name='logo' value='false'/>

                  `;
            } else {
                permissions = `
                  <param name='perm.check' value='enable'/>
                  <param name='perm.import' value='enable'/>
                  <param name='perm.excel' value='enable'/>
                  <param name='perm.other' value='enable'/>
                  <param name='perm.pro100' value='enable'/>
                  <param name='perm.imos' value='enable'/>
                  <param name='perm.bazis' value='enable'/>
                  <param name='perm.bcad' value='enable'/>
                  <param name='perm.woody2' value='enable'/>
                  <param name='perm.woodwop' value='enable'/>
                  <param name='perm.ilenia' value='enable'/>

                  <param name='perm.panel.products' value='enable'/>

                  <param name='perm.panel.operations.add.cs' value='enable'/>
                  <param name='perm.panel.operations.add.cn' value='disable'/>
                  <param name='perm.panel.operations.add.el' value='enable'/>
                  <param name='perm.panel.operations.add.gr' value='enable'/>
                  <param name='perm.panel.operations.add.o' value='enable'/>
                  <param name='perm.panel.operations.add.xnc' value='hide'/>
                  <param name='perm.panel.operations.add.lb' value='hide'/>
                  <param name='perm.panel.operations.add.sp' value='hide'/>
                  <param name='perm.panel.operations.add.cl' value='hide'/>

                  <param name='perm.panel.operations.edit.cs' value='hide'/>
                  <param name='perm.panel.operations.edit.el' value='disable'/>
                  <param name='perm.panel.operations.edit.gr' value='enable'/>
                  <param name='perm.panel.operations.edit.o' value='hide'/>
                  <param name='perm.panel.operations.edit.xnc' value='hide'/>
                  <param name='perm.panel.operations.edit.lb' value='hide'/>
                  <param name='perm.panel.operations.edit.sp' value='hide'/>

                  <param name='perm.panel.operations.cost' value='hide'/>
                  <param name='perm.panel.operations.print' value='hide'/>

                  <param name='perm.o.panel.properties' value='enable'/>

                  <param name='perm.cs.panel.properties' value='hide'/>
                  <param name='perm.cs.panel.parts' value='enable'/>
                  <param name='perm.cs.panel.panels' value='enable'/>
                  <param name='perm.cs.panel.patterns' value='enable'/>
                  <param name='perm.cs.action.savedefault' value='hide'/>
                  <param name='perm.cSizeMode.workpiece' value='disable'/>
                  <param name='perm.cs.panel.panels.preferences' value='hide'/>



                  <param name='perm.cl.panel.properties' value='hide'/>
                  <param name='perm.cl.panel.parts' value='hide'/>
                  <param name='perm.cl.panel.panels' value='hide'/>
                  <param name='perm.cl.panel.patterns' value='hide'/>
                  <param name='perm.cl.action.savedefault' value='hide'/>

                  <param name='perm.el.panel.properties' value='hide'/>
                  <param name='perm.el.panel.parts' value='enable'/>
                  <param name='perm.el.action.savedefault' value='hide'/>

                  <param name='perm.gr.panel.properties' value='enable'/>
                  <param name='perm.gr.panel.parts' value='enable'/>
                  <param name='perm.gr.action.savedefault' value='hide'/>

                  <param name='perm.sp.panel.properties' value='disable'/>
                  <param name='perm.sp.panel.parts' value='disable'/>
                  <param name='perm.sp.action.savedefault' value='disable'/>

                  <param name='perm.xnc.panel.properties' value='enable'/>
                  <param name='perm.xnc.panel.template' value='enable'/>
                  <param name='perm.xnc.panel.cost' value='disable'/>
                  <param name='perm.xnc.action.savedefault' value='hide'/>

                  <param name='perm.lb.panel.properties' value='hide'/>
                  <param name='perm.lb.panel.custom' value='hide'/>
                  <param name='perm.lb.panel.parts' value='hide'/>
                  <param name='perm.lb.panel.template' value='hide'/>
                  <param name='perm.lb.action.savedefault' value='hide'/>
                  <param name='logo' value='false'/>

                  `;
            }
        }

        var s = "<response>" //
            //Transferring parameters to a form (Передача параметров в форму)
            + "<param name='defaults' value='&lt;defaults&gt;  &lt;XNC bySizeDetail=\"true\" mirHor=\"false\" mirVert=\"true\" /&gt;  &lt;EL printable=\"true\" startNewPage=\"true\" elWastePRC=\"0.1\" elCalcMat=\"true\" elRoundMat=\"1\" elRoundLength=\"21\" elMinDimDetail=\"70\"/&gt;  &lt;CS cSizeMode=\"1\" cTrimL=\"8\"  cCPF=\"0.9\" cTrimW=\"8\" csDirectCut=\"3\" cCostByItemRound=\"22\" cFillRep=\"0.9\"  cCostType=\"0\" cCostCut=\"0\" cCostByItem=\"false\" cBusinessWaste=\"10\" csTexture=\"true\"  cMinDimRegWaste1=\"300\" cMinDimRegWaste2=\"300\" cTrimWaster=\"true\" csComplexBand=\"false\" cSortBands=\"0\" printable=\"true\" startNewPage=\"true\" cCombiningParts=\"false\" cGroupParts=\"0\" cTrimWaste=\"false\" cSaveDrawCut=\"false\" cMinDimDetail=\"30\" cAllowanceWorkpiece=\"0\" /&gt;   &lt;tool.cutting  swMinSizeBand=\"0\" swMinPruning=\"0\" swTrimIncludeSaw=\"false\" swMaxSizeBand=\"0\" swSortInBand=\"0\" swSort=\"2\" swComplexBand=\"false\" swPackageHeight=\"55\" swMaxLengthBand=\"5500\" swSawthick=\"4.8\" swMaxturns=\"6\"/&gt;&lt;tool.edgeline  elWastePRC=\"0.1\" elRestSide=\"35\" elMinSize=\"120\" elWidthPreJoint=\"1\" elCost=\"0\" /&gt;&lt;/defaults&gt;'/>"

            + "<param name='projectInfo' value='Project: {0}      Cost operation ${1} + Cost material ${2} = Cost ${3} '/>"
            + "<param name='closeMessage' value=''/>"
            + "<param name='partTableItems' value='/8MBAAAADMGswaXBrsGnwbTBqAFA4AAAAAAAAAAAAAAAAAAAAAABAAAACsG3wanBpMG0wagBQOAAAAAAAAAAAAAAAAAAAAAAAQAAAArBo8GvwbXBrsG0AUCgAAAAAAAAAAAAAAAAAAAAAAEAAAAGwbTBuMG0AUCgAAAAAAAAAAAAAAAAAAAAAAEAAAAGwaXBtMGuAUEgAAAAAAAAAAAAAAAAAAAAAAEAAAAGwaXBosGuAUEgAAAAAAAAAAAAAAAAAAAAAAEAAAAGwaXBrMGuAUEgAAAAAAAAAAAAAAAAAAAAAAEAAAAGwaXBssGuAUEgAAAAAAAAAAAAAAAAAAAAAAEAAAAIwa7BocGtwaUBQgwAAAAAAAAAAAAAAAAAAAAAAQAAABHBsMGhwbLBtC7Bo8GvwaTBpQBBQAAAAAAAAAAAAAAAAAAAAAABAAAADcGwwaHBssG0LsGpwaQBQOAAAAAAAAAAAAAAAAAAAAAAAQAAABPBtMGvwbTBocGsQ8GvwbXBrsG0AUCgAAAAAAAAAAAAAAAAAAAAAAEAAAARwbXBs8GlwaRDwa/BtcGuwbQBQKAAAAAAAAAAAAAAAAAAAAAAAQAAABHBssGlwbPBtEPBr8G1wa7BtAFAoAAAAAAAAAAAAAAAAAAAAAABAAAAFsGkwaXBs8GjwbLBqcGwwbTBqcGvwa4AQgwAAAAAAAAAAAAAAAAAAAAAAQAAABfBsMGywa/BpMG1waPBtC7BrsGhwa3BpQBCDAAAAAAAAAAAAAAAAAAAAAABAAAAF8GwwbLBr8GkwbXBo8G0LsGjwa/BpMGlAEGgAAAAAAAAAAAAAAAAAAAAAAEAAAAZwbDBssGvwaTBtcGjwbQuwaPBr8G1wa7BtABA4AAAAAAAAAAAAAAAAAAAAAABAAAAGcGwwbLBr8GkwbXBo8G0LsGpwa7BpMGlwbgAQKAAAAAAAAAAAAAAAAAAAAAAAQAAAArBqcGuwaTBpcG4AECgAAAAAAAAAAAAAAAAAAAAAAEAAAAewanBrsGkwaXBuEnBrk/BsMGlwbLBocG0wanBr8GuAECgAAAAAAAAAAAAAAAAAAAAAAEAAAAfwbfBr8GywavBsMGpwaXBo8GlLsGswaXBrsGnwbTBqABA4AAAAAAAAAAAAAAAAAAAAAABAAAAHcG3wa/BssGrwbDBqcGlwaPBpS7Bt8GpwaTBtMGoAEDgAAAAAAAAAAAAAAAAAAAAAAEAAAAZwaTBpcG0waHBqcGsLsGswaXBrsGnwbTBqABA4AAAAAAAAAAAAAAAAAAAAAABAAAAF8GkwaXBtMGhwanBrC7Bt8GpwaTBtMGoAEDgAAAAAAAAAAAAAAAAAAAAAAEAAAAqwaHBpsG0waXBssGQwbLBpU3BqcGswazBqcGuwacuwazBpcGuwafBtMGoAEDgAAAAAAAAAAAAAAAAAAAAAAEAAAAowaHBpsG0waXBssGQwbLBpU3BqcGswazBqcGuwacuwbfBqcGkwbTBqABA4AAAAAAAAAAAAAAAAAAAAAABAAAAG8GjwbXBtMG0wanBrsGnLsGswaXBrsGnwbTBqABA4AAAAAAAAAAAAAAAAAAAAAABAAAAGcGjwbXBtMG0wanBrsGnLsG3wanBpMG0wagAQOAAAAAAAAAAAAAAAAAAAAAAAQAAABTBocGswazBr8G3waHBrsGjwaXBlABAoAAAAAAAAAAAAAAAAAAAAAABAAAAE8GhwazBrMGvwbfBocGuwaPBpUIAQKAAAAAAAAAAAAAAAAAAAAAAAQAAABPBocGswazBr8G3waHBrsGjwaVMAECgAAAAAAAAAAAAAAAAAAAAAAEAAAAUwaHBrMGswa/Bt8Ghwa7Bo8GlwZIAQKAAAAAAAAAAAAAAAAAAAAAAAQAAAAjBp8GywbTBrgBBcAAAAAAAAAAAAAAAAAAAAAABAAAACMGnwbLBosGuAEFwAAAAAAAAAAAAAAAAAAAAAAEAAAAIwafBssGswa4AQXAAAAAAAAAAAAAAAAAAAAAAAQAAAAjBp8GywbLBrgBBcAAAAAAAAAAAAAAAAAAAAAABAAAACMGnwbLBtMGzAECgAAAAAAAAAAAAAAAAAAAAAAEAAAAIwafBssGiwbMAQKAAAAAAAAAAAAAAAAAAAAAAAQAAAAjBp8GywazBswBAoAAAAAAAAAAAAAAAAAAAAAABAAAACMGnwbLBssGzAECgAAAAAAAAAAAAAAAAAAAAAAEAAAARwbPBtcGywaZGwbLBr8GuwbQAQXAAAAAAAAAAAAAAAAAAAAAAAQAAAA/Bs8G1wbLBpkLBocGjwasAQXAAAAAAAAAAAAAAAAAAAAAAAQAAAAzBp8Gywa/Br8G2waUAQEAAAAAAAAAAAAAAAAAAAAAAAQAAAArBpMGywanBrMGsAEBAAAAAAAAAAAAAAAAAAAAAAAEAAAAIwa3BqcGswawAQEAAAAAAAAAAAAAAAAAAAAAAAQAAAAXBmE5DMQBBcAAAAAAAAAAAAAAAAAAAAAABAAAAD8G4wa7Bo8GTwanBpMGlMQBAoAAAAAAAAAAAAAAAAAAAAAABAAAAC8G4wa7Bo0LBk0QxAECgAAAAAAAAAAAAAAAAAAAAAAEAAAAFwZhOQzIAQXAAAAAAAAAAAAAAAAAAAAAAAQAAAA/BuMGuwaPBk8GpwaTBpTIAQKAAAAAAAAAAAAAAAAAAAAAAAQAAAAvBuMGuwaNCwZNEMgBAoAAAAAAAAAAAAAAAAAAAAAABAAAABcGYTkMzAEFwAAAAAAAAAAAAAAAAAAAAAAEAAAAPwbjBrsGjwZPBqcGkwaUzAECgAAAAAAAAAAAAAAAAAAAAAAEAAAALwbjBrsGjQsGTRDMAQKAAAAAAAAAAAAAAAAAAAAAAAQAAAAXBmE5DNABBcAAAAAAAAAAAAAAAAAAAAAABAAAAD8G4wa7Bo8GTwanBpMGlNABAoAAAAAAAAAAAAAAAAAAAAAABAAAAC8G4wa7Bo0LBk0Q0AECgAAAAAAAAAAAAAAAAAAAAAAEAAAAFwZhOQzUAQXAAAAAAAAAAAAAAAAAAAAAAAQAAAA/BuMGuwaPBk8GpwaTBpTUAQKAAAAAAAAAAAAAAAAAAAAAAAQAAAAvBuMGuwaNCwZNENQBAoAAAAAAAAAAAAAAAAAAAAAA='/>"



            + permissions

            //Load rest (Загрузка остатков)
            + "<param name='remaindersSource' value='true'/>"

            ///Параметры для импорта из 3D конструктора
			///С правками от 17.05.2021 (imp3dc.xncCombine true >> false)
            + "<param name='imp3dc.bySizeDetail' value='true'/>"
            + "<param name='imp3dc.millOffset' value='20'/>"
            + "<param name='imp3dc.xnc' value='true'/>"
            + "<param name='imp3dc.mirSide' value='true'/>"
            + "<param name='imp3dc.millDiameter' value='20'/>"
            + "<param name='imp3dc.groove' value='0'/>"
            + "<param name='imp3dc.xncCombine' value='false'/>"
            + "<param name='imp3dc.protrusionBore' value='3'/>"
            + "<param name='imp3dc.complex' value='true'/>"
            + "<param name='imp3dc.protrusionMill' value='3'/>"
            + "<param name='imp3dcExtrudedDoors' value='true'/>"
            // + "<param name='imp3dcPartName' value='part.id'/>"
            + "<param name='imp3dcAccessories' value='true'/>"
            + "<param name='imp3dcOtherParts' value='true'/>"
            + "<param name='imp3dcAddOperation' value='true'/>"


            //Parameters passed in request from the form/Параметры передаваемые в запросах от формы
            + "<param name='requestParams' value='userId,sessionId'/>"//
            + "<param name='userId' value='U12345'/>"// Example param/Пример параметра
            + "<param name='sessionId' value='S12345'/>"// Example param/Пример параметра

            + "<param name='catalogRootSheet' value='1'/>"
            + "<param name='catalogRootBAND' value='3'/>"

            //XNC Editor
            //Tools
            + "<param name='xncEdTC' value='{{ $tools["total_count"] }}'/>"//Количество интрументов

                @foreach($tools['items'] as $tool)
            + "<param name='xncEdT{{ $loop->iteration }}.name' value='{{ $tool["name"] }}'/>"//Наименование инструмента
            + "<param name='xncEdT{{ $loop->iteration }}.comment' value='{{ $tool["comment"] }}'/>"//Комментарий
            + "<param name='xncEdT{{ $loop->iteration }}.diameter' value='{{ $tool["diameter"] }}'/>"//Диаметр
            + "<param name='xncEdT{{ $loop->iteration }}.plane' value='{{ $tool["plane"] }}'/>"//Плоскость (0=пласть,1=,2=,3=,4=)
            + "<param name='xncEdT{{ $loop->iteration }}.through' value='{{ $tool["throught"] }}'/>"//Сквозное
            + "<param name='xncEdT{{ $loop->iteration }}.depth' value='{{ $tool["depth"] }}'/>"//Глубин поумолчанию

                @endforeach

            //PALETTE
            + "<param name='xncEdPC' value='{{ $xnc_template_folders["parend_folders_count"] }}'/>"//Количество палитр
            + "<param name='xncEdP1.name' value='xncEdPD'/>"//Наименование палитры. Для сверлений всегда "xncEdPD"
            + "<param name='xncEdP1.enable' value='true'/>"//Включен
            //
                @foreach($xnc_template_folders['items'] as $folder)
            + "<param name='xncEdP{{ $loop->iteration + 1 }}.name' value='{{ $folder["name"] }}'/>"//Наименование палитры
            + "<param name='xncEdP{{ $loop->iteration + 1 }}.enable' value='{{ $folder["visible"] }}'/>"//Включен
            + "<param name='xncEdP{{ $loop->iteration +1 }}.id' value='{{ $folder["id"] }}'/>"//Ссылка на папку контента XNC

                @endforeach

            //Loading a project into a form (Загрузка проекта в форму)
            + '{!! $start_project !!}'
            + "</response>";
        return s;
    }
</script>
<script type="text/javascript" src="/js/gibform.js"></script>
<!-- The layout of this page is for only example (Разметка данной страницы сделана только для примера) -->

<div class="user_sheet_cutting_block">
    <div class="popup-message-block">
        <h4>В проекте установлен раскрой!</h4>
        <p>При пересчёте проекта установленный вручную раскрой сбрасывается, и устанавливается автоматически, как правило он не совпадает с раскроем, установленным вручную.<br>Eсли Вы хотите установить раскрой вручную, сообщите об этом менеджеру.</p>
        <div class="message-button-block">
            <button type="button" class="btn btn-primary" id="user_sheet_cutting">Понятно</button>
        </div>
    </div>
</div>

<table style="width: 100%; height: 100%; position: absolute;">
    {{--@if($warning_status == 1)--}}
    {{--<tr style="height: 20px;">--}}
    {{--<td id="TroubleInfo" colspan="3" style="border: solid 1px gray; background: red; color: white; text-align: center">--}}
    {{--Уважаемые пользователи. На текущий момент наблюдаются трудности в работе части функционала данного сервиса, мы занимаемся усердно странением.--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--@endif--}}
    @if($project_status == 1)
        <tr style="height: 20px;">
            <td id="projectInfo" colspan="3" style="border: solid 1px gray; background: green; color: white; text-align: center">
                Уважаемый менеджер, Вы находитесь в режиме редактирования заказа №{{ $order_akcent_full }} , данные этого заказа будут обновлены
            </td>
        </tr>
    @endif
    @if($project_status == 2)
        <tr style="height: 20px;">
            <td id="projectInfo" colspan="3" style="border: solid 1px gray; background: red; color: white; text-align: center">
                Уважаемый менеджер, заказ №{{ $order_akcent_full }} открыт только для просмотра, так как уже размещён в производство
            </td>
        </tr>
    @endif
    <tr style="height: 100%;">
        <td colspan="3">
            <!-- "giblab.container" - ID of the div tag in which the form will be loaded (идентификатор тега div в который будет загружена форма) -->
            <input type="hidden" id="projectInfo" value="" />
            <div id="giblab.container" style="width: 100%; height: 100%; border: solid 1px gray;"></div>
        </td>
    </tr>
    <tr style="height: 43px;" class="bottom_buttons_line">
        <td class="form_buttons_container">

            <form id="continue_form" method="POST" action="/send_project" style="display: inline-block;">
                @csrf

                <input type="hidden" name="project_data" id="saveContainer" value="{{ $start_project }}" />

                <!-- <select name="user_place" id="user_place"
                        @if($order_status_data != 'черновик' && ($order_status == 1 || $order_status == 2))
                        style="height: 1px!important; width: 1px!important; opacity: 0!important;"
                        @endif
                > -->
                <select name="user_place" id="user_place">
                    @foreach ($places as $place)
                        @if ($place->PLACES_ID == $user_place)

                            @if (isset($edge2[$place->PLACES_ID]))
                                <option value="{{ $place->PLACES_ID }}" selected="selected" data-pur="1">{{ $place->NAME }} </option>
                            @else
                                <option value="{{ $place->PLACES_ID }}" selected="selected">{{ $place->NAME }} </option>
                            @endif
                        @else
                            @if (isset($edge2[$place->PLACES_ID]))
                                <option value="{{ $place->PLACES_ID }}" data-pur="1">{{ $place->NAME }} </option>
                            @else
                                <option value="{{ $place->PLACES_ID }}">{{ $place->NAME }} </option>
                            @endif
                        @endif
                    @endforeach
                </select>



                @if($rsflag == 0)
                <select name="user_type_edge" id="user_type_edge">

                    @if ("pur" == $user_type_edge)
                        <option value="pur" selected="selected">ПУР</option>
                    @endif

                    @if ("eva" == $user_type_edge)
                        <option value="eva" selected="selected">ЭВА</option>
                    @else
                        <option value="eva">ЭВА</option>
                    @endif

                </select>
                @endif

                @csrf

                <input type="hidden" name="project_data" id="saveContainer2" value="{{ $start_project }}" />
                <input type="hidden" name="user_type_edge" id="user_type_edge2" value="{{ $user_type_edge }}" />
                <input type="hidden" name="user_place" id="user_place2" value="{{ $user_place }}" />


                <div id="bottom_container">
                    <button type="button" id="saveBtn">Проверить</button>
                </div>


                <div id="extra_buttons_container">
                    @if ($rsflag === 0)
                    <button class="clear_project_button"><a href="#" id="extra_operations_button"> Доп. операции </a></button>
                    @endif

                    @if ($rsflag === 1)
                        <button class="clear_project_button"><a href="<?= config('app.app_main_dir') ?>/clients/RS/clients/create.php?session_id=<?= session_id() ?>" id="get_reciepe_button_RS"> Счет </a></button>
                    @else
                        <button id="grb" class="clear_project_button showEl"><a href="#" id="get_reciepe_button"> Счет </a></button>
                        <button id="grb_RS" class="clear_project_button hideEl"><a href="<?= config('app.app_main_dir') ?>/clients/RS/clients/create.php?session_id=<?= session_id() ?>" id="get_reciepe_button_RS"> Счет </a></button>
                    @endif
                        @if(!$rsflag)
                            @if ($back_start_project == true)
                                {{--<button class="clear_project_button"><a href="/back_clear_project">К начальному проекту</a></button>--}}
                            @endif
                        @endif
                </div>
            </form>
            <div style="position: relative; margin-left: 30px; text-align: center; opacity: 1;">
                <button id="set_comment">Комментарий</button>
                <div class="comment_container">
                    <form action="" method="POST" id="order_comment_form">
                        <textarea name="" id="order_comment_input" cols="30" rows="10" placeholder="Оставьте Ваш комментарий">{{ $order_comment }}</textarea>
                        <button>Сохранить</button>
                    </form>
                </div>
            </div>
            <div style="position: relative;">

                <div class="main_bottom_container">
                    <div class="options-list">
                        {{--<button class="clear_project_button" onclick="window.location.href='/clear_project'">Сброс</button>--}}
                        <a href="/clear_project" class="clear_project_button">Сброс</a>
                        @if(in_array($client_id, $project_restore_user_ids) || in_array($manager_id, $project_restore_user_ids))
                            <button class="green_button" id="recoveryProject">Восстановить проект</button>                        
                        @endif
                        <button class="clear_project_button" id="user_save_project">Сохранить .kronas</button>

                        <div class="example-2">
                            <div class="form-group">
                                <input type="file" name="kronasProject" id="kronasProject" class="input-file" accept=".kronas">
                                <label for="kronasProject" class="btn btn-tertiary js-labelFile">
                                    <span class="js-fileName">Загрузить</span>
                                </label>
                            </div>
                        </div>

                        <!-- <button id="send_pdf_button">PDF</button> -->
                        <button id="full_project_pdf">PDF</button>
                    @if(!$rsflag)
                        <button type="button" id="mpr_import_button">Импорт MPR</button>
                        
                        @if($client_code || $client_code != '')
                            <button id="client_materials">Остатки клиента</button>
                        @endif
                        
                        @if($project_status != 0)
                            <button onclick="window.location = '/start_new_porject'" id="create-nproject-btn"> Новый проект </button>
                        @endif

                        @if($managerflag === 1 || $adminflag === 1)
                        <button class="clear_project_button" id="user_save_project_file">Сохранить .project</button>
                        @endif

                        @if($adminflag === 1)
                            <div class="example-2">
                                <div class="form-group">
                                    <input type="file" name="basisProject" id="basisProject" class="input-file">
                                    <label for="basisProject" class="btn btn-tertiary js-labelFile">
                                        <span class="js-fileName">Импорт Базис проекта</span>
                                    </label>
                                </div>
                            </div>
                        @endif

                        <button id="get_con_material_button">Подбор материалов</button>
                    @endif
                        @if($CtRhTn == "true")
                            @if(!$rsflag)
                              <form action="{{ $main_dir }}/open_admin_xml.php" method="POST" id="admin_open_project_form" target="_blank">
                                  @csrf
                                  <input type="hidden" value="" name="open_project" id="admin_open_project_input">
                                  <button id="admin_open_project">Проект</button>
                              </form>
                            @endif
                            @if($project_status != 0)
                                <a class="clear_project_button" href="{{ $main_dir }}/refresh_project_programm.php?project={{$order_1c_project}}&old_code=1" id="refresh_programm_now">Выгрузить программы без корректировки из проекта</a>
                                <a class="clear_project_button" href="{{ $main_dir }}/refresh_project_programm.php?project={{$order_1c_project}}&old_code=1&prod=1" id="refresh_programm_angle_change">Выгрузить программы с автоповоротом на станке (ТЕСТИРОВАНИЕ)</a>
                            @endif
                    
                            {{--<button id="user_full_pdf_window_button">Полный отчет PDF</button>--}}
                            {{--<div id="user_full_pdf_window">--}}
                            {{--<button id="user_full_pdf_window_close">X</button>--}}
                            {{--<div>--}}
                            {{--<button id="send_pdf_button" class="send_pdf_button">PDF</button>--}}
                            {{--<button id="full_project_pdf">Полный отчет PDF</button>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        @endif
                        @if($manager_id > 0 || ($client_code && $client_code != ''))
                            @if(!$rsflag)
                              <form action="/save_edit_ticket" method="POST" id="sep_ticket_form">
                                  <input type="hidden" name="sep_order_id" value="{{ $order_1c_project }}">
                                  <input type="hidden" name="sep_manager_id" value="{{ $manager_id }}">
                                  <input type="hidden" name="sep_manager_admin" value="{{ $CtRhTn }}">
                                  <input type="hidden" name="sep_client_id" value="{{ $client_id }}">
                                  <input type="hidden" name="sep_client_code" value="{{ $client_code }}">
                                  <input type="hidden" name="sep_project_data" value="" id="ticket_project_data">
                                  @csrf
                                  <button class="clear_project_button" style="color: #fff;background-color: #28a745;border: 1px solid #28a745;padding: .375rem .75rem;text-decoration: none;" type="submit" id="sep_ticket_form_button">Помощь обработчика</button>
                              </form>
                              {{--<a class="clear_project_button" href="http://service.kronas.com.ua:11080/helpdesc/api/ticket.php?create_new_ticket=1&order_id={{$order_1c_project}}&client_id=85159">Помощь обработчика</a>--}}
                            @endif
                        @endif
                        @if($manager_id > 0)
                          @if(!$rsflag)
                            <button id="user_upload_viyar">Загрузить ВиярПРО <br>(БЕЗ ФРЕЗЕРОВАНИЯ)</button>
                            <form action="/save_edit_project_manager" method="POST" id="sep_project_form">
                                <input type="hidden" name="sep_order_id" value="{{ $order_1c_project }}">
                                <input type="hidden" name="sep_manager_id" value="{{ $manager_id }}">
                                <input type="hidden" name="sep_manager_admin" value="{{ $CtRhTn }}">
                                <input type="hidden" name="sep_project_data" value="" id="sep_project_data">
                                <input type="hidden" name="current_place" value="" id="current_place">
                                <input type="hidden" name="user_type_edge" id="user_type_edge3" value="{{ $user_type_edge }}" />
                                @csrf
                                <button class="clear_project_button" style="color: #fff;background-color: #28a745;border: 1px solid #28a745;padding: .375rem .75rem;text-decoration: none;" type="submit" id="sep_project_form_button">Хранилище заказов</button>
                            </form>
                          @endif
                            <!-- <a class="clear_project_button" href="{{$main_dir}}/manager_all_order.php">Хранилище заказов<br><small>(без сохранения версии)</small></a> -->
                            <a class="clear_project_button" href="{{$main_dir}}/login.php?session_id={{$ssnrst}}">Хранилище заказов<br><small>(без сохранения версии)</small></a>
                        @endif
                    </div>
                </div>
                <div id="mpr_import_form">
                    <button id="mpr_import_form_close">X</button>
                    <p><b>Пакетный импорт файлов MPR:</b></p>
                    <form action="/mpr_import_action" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="mpr_import_field">
                        <button type="submit">Отправить</button>
                    </form>
                </div>
                <div id="user_upload_viyar_form">
                    <button id="user_upload_form_close">X</button>
                    <p><b>Загрузить проект из ВиярПРО(.project):</b></p>
                    <form action="{{$main_dir}}/import_vp.php?data_id={{ $session_id }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="upload_project">
                        <input type="hidden" name="user_place" id="viyar_user_place">
                        <input type="hidden" name="project_data" id="viyar_project_data">
                        <button type="submit">Отправить</button>
                    </form>
                </div>
                <button id="main_bottom_container_button">&#8226; &#8226; &#8226;</button>
            </div>
            <div class="bottom_version_control_menu_container" title="Текущая версия: <?php if(isset($all_version_projects['actual'])) echo $all_version_projects['actual']['author'].' от '.$all_version_projects['actual']['date'].' '.'('.$all_version_projects['actual']['project_id'].')'?>">
                @if(isset($all_version_projects['data']) > 0)
                    Версии проекта: <button id="version_control_menu">▼</button>
                    <div class="bottom_version_control_menu">
                        <?php foreach($all_version_projects['data'] as $link): ?>
                        <a href="{{$link['link']}}">{{$link['author']}} от {{$link['date']}} ({{$link['project_id']}})</a>
                        <?php endforeach; ?>
                        <div class="ajax_save_new_version">
                            @if(!$rsflag)
                            <button id="save_ajax_new_version_button">Сохранить текущую версию</button>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </td>

        <td>
            <div class="bottom_middle_container">
                @if($manager_id > 0)
                    <div class="table_auth_container">
                        <div>
                            Менеджер: {!! $manager_name !!}
                        </div>
                        <form action="{{$app_dir}}/auth" method="GET" class="logout_form">
                            <input type="hidden" value="true" name="logout">
                            <button>Выйти</button>
                        </form>

                    </div>
                @else
                    <div id="autorize_manager_open">
                        <span>Менеджер: </span>
                        <img id="auth_button_manager"  src="/images/key.svg" alt="">
                    </div>
                @endif
                @if((!$client_code || $client_code == '') && ($manager_id > 0))
                    <div class="table_auth_container client_search_form">
                        <div>
                            Клиент: {{ $client_code }}
                        </div>
                        <form action="/set_client_auth" method="POST" class="logout_form" id="set_client_auth_confirm_form">
                            <input name="client_id_search" id="client_id_search" type="text">
                            <input name="client_id_setter" id="client_id" type="hidden">
                            <div id="client_variants_container">

                            </div>
                            @csrf
                        </form>
                        <button id="set_client_auth_confirm">ОК</button>

                    </div>
                @elseif($client_code && $client_code != '')
                    <div class="table_auth_container client_auth_container">
                        <div>
                            Клиент: {!! $client_name !!}
                        </div>
                        <button id="client_menu_button">&#9660;</button>
                        <div id="client_menu_containers">
                            <button id="client_orders_button">Заказы</button>
                            @if(!$rsflag)
                            <button id="client_materials_menu">Остатки материалов</button>
                            <button class="handlers_request_button" onclick="document.location.href = '{{$main_dir}}/helpdesc/'">Запросы обработчикам</button>
                            @endif
                            <!-- <form action="{{ $main_dir }}/clients/login.php" method="POST" id="client_menu_auth_form_idg">
                                <input type="hidden" name="submitenter" value="1">
                                <input type="hidden" name="typein" value="client">
                                <input type="hidden" name="mail1" value="{{ $client_phone }}">
                                <input type="hidden" name="password1" value="{{ $client_password }}">
                                <input type="hidden" name="client_id_auth" value="{{ $client_code }}">
                                <button class="client_menu_button">В личный кабинет</button>
                            </form> -->
                            <form action="/auth_client" method="GET" class="logout_form">
                                <input type="hidden" value="true" name="logout">
                                <button>Выйти</button>
                            </form>
                        </div>


                    </div>
                @else
                    <div id="autorize_client_open">
                        <span>Клиент: </span>
                        <img id="auth_button_client" src="/images/key.svg" alt="">
                    </div>
                @endif

            </div>
        </td>
        <!-- Last save time (Время последнего сохранения) -->
        <td id="saveTime"></td>
    </tr>
</table>

<div class="auth_form_container">
    <div class="close_button">
        <a>X</a>
    </div>
    <div class="auth_toggle_container">
        <button class="auth_button_manager">Менеджер</button>
        <button class="auth_button_client">Клиент</button>
    </div>
    <form action="" id="auth_form" class="open">
        <div class="form_group">
            <span>Логин:</span>
            <input type="text" name="login" id="login_input">
        </div>
        <div class="form_group">
            <span>Пароль:</span>
            <input type="password" name="password" id="password_input">
        </div>
        <div class="button_container">
            <button type="submit">Авторизироваться</button>
        </div>
        <div id="message">

        </div>
    </form>
    <form id="auth_client" action="">
        {{--<div class="form_group">--}}
        {{--<span>E-мейл:</span>--}}
        {{--<input type="email" name="login_client" id="login_client">--}}
        {{--</div>--}}
        {{--<p style="text-align: center">или</p>--}}
        <div class="form_group telephone_client_auth">
            <span>Телефон:</span>
            <input type="text" name="telephone_client" id="telephone_client">
        </div>
        <div class="form_group hidden pin_client_auth" style="display: none;">
            <span>Отправленный на телефон PIN-код:</span>
            <input type="password" name="password_client" id="password_client">
        </div>
        {{--<a href="#" id="get_your_password">Получить пароль</a>--}}
        <div class="button_container">
            <button type="submit">Авторизироваться</button>
        </div>
        <div id="message_client">

        </div>
    </form>
    <form action="" id="get_password_form">
        <div class="form_group">
            <span>Введите Ваш емейл или телефон. На него будет отправлен пароль:</span>
            <br>
            <input type="email" name="get_password" id="get_password_input" placeholder="Е-mail">
            <p style="text-align: center">или</p>
            <input type="text" name="get_password_phone" id="get_password_phone_input" placeholder="Телефон">
        </div>
        <div class="button_container">
            <button type="submit">Получить пароль</button>
        </div>
        <div id="message_password">

        </div>
    </form>
</div>

<div class="checks_container">
    <div id="checks_container">

    </div>
</div>
@if ($mpr_import_error != false)
    <div class="mpr_error_container">
        <div class="mpt_error_inner">
            <p>{{$mpr_import_error}}</p>
            <button id="mpr_error_close_button">ОК</button>
        </div>
    </div>
@endif
@if ($upload_project_error == 1)
    <div class="mpr_error_container">
        <div class="mpt_error_inner">
            <p>{{$upload_project_error_text}}</p>
            <button id="mpr_error_close_button">ОК</button>
        </div>
    </div>
@endif
@if ($confirm_project_error == 1)
    <div class="mpr_error_container">
        <div class="mpt_error_inner">
            <p>{{$confirm_project_error_text}}</p>
            <button id="mpr_error_close_button">ОК</button>
        </div>
    </div>
@endif
<div id="checks_lodaer_containers">
    <div>
        <img src="/images/download.gif" alt="">
    </div>
</div>

<div></div>

<div id="get_con_material">

</div>
<div id="get_client_material">
    <button id="get_client_material_close">&#215;</button>
    <div class="client_materials_filters">
        <form action="" id="client_materials_filters_form">
            <div class="client_material_filter">
                <div class="form-group">
                    <label>Толщина остатка</label>
                    <input type="text" id="client_filter_volume" name="client_filter_volume">
                </div>
            </div>
            <div class="client_material_filter">
                <div class="form-group">
                    <label>CODE</label>
                    <input type="text" id="client_filter_code" name="client_filter_code">
                </div>
            </div>
            <div class="client_material_filter">
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" id="client_filter_name" name="client_filter_name">
                </div>
            </div>
            <div class="client_material_filter">
                <div class="form-group">
                    <label>По ID материала</label>
                    <input type="text" id="client_filter_idm" name="client_filter_idm">
                </div>
            </div>
            <div class="client_material_filter">
                <div class="form-group">
                    <label>Клиент</label>
                    <input type="text" id="client_filter_tel" name="client_filter_tel">
                </div>
            </div>
            <div class="client_material_filter">
                <div class="form-group">
                    <label>Выберите склад</label>
                    <select name="client_filter_place" id="client_filter_place" name="client_filter_place">
                        <option value="">Любой</option>
                        @foreach ($places as $place)
                            <option value="{{ $place->PLACES_ID }}">{{ $place->NAME }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>
    </div>
    <div id="get_client_material_inner">
        <form action="" id="client_material_checked_form">
            <table cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th>CODE</th>
                    <th>Склад</th>
                    <th>Деталь</th>
                    <th>Клиент</th>
                    <th>Повреждения</th>
                    {{--<th>W</th>--}}
                    {{--<th>L</th>--}}
                    {{--<th>T</th>--}}
                    {{--<th>Кол-во</th>--}}
                    <th>Добавить в заказ</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <button type="submit">Добавить</button>
        </form>
        <form action="{{ $main_dir }}/put_material_client_in.php?data_id={{ $session_id }}" id="final_product_array_form" method="POST">
            <input type="hidden" id="final_product_array" name="data">
        </form>
    </div>
</div>
<div id="get_client_orders">
    <button id="get_client_orders_close">&#215;</button>
    <div class="orders_filters">
        <div>
            <label>Показать за: </label>
            <select id="client_orders_filter_date">
                <option value="365">За весь год</option>
                <option value="30">За прошлые 30 дней</option>
                <option value="7">За 7 дней</option>
            </select>
        </div>
        <div>
            <label>Статус:</label>
            <select id="client_orders_filter_status">
                <option value="all">Все</option>
                <option value="в работе">в работе</option>
                <option value="выполнен">выполнен</option>
                <option value="отменен">отменен</option>
            </select>
        </div>
        <div>
            <label>Тип:</label>
            <select id="client_orders_filter_type">
                <option value="all">Все</option>
                <option value="1">Пильный</option>
                <option value="5">Обычный</option>
            </select>
        </div>
    </div>
    <table id="orders_html_table">
        <thead>
        <th>Дата создания</th>
        <th>Номер заказа</th>
        <th>Контрагент</th>
        <th>Участок</th>
        <th>Менеджер</th>
        <th>Статус</th>
        <th>Действия</th>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div id="orders_inner_receipt">
        <button id="orders_inner_receipt_back">Назад</button>
        <h3>Счет для заказа №<b id="orders_inner_receipt_order"></b></h3>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Тип</th>
                <th>Количество</th>
                <th>Цена</th>
                <th>Стоимость</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div id="orders_inner_parts">
        <button id="orders_inner_parts_back">Назад</button>
        <h3>Спецификация деталей для заказа №<b id="orders_inner_parts_order"></b></h3>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Описание</th>
                <th>Ширина</th>
                <th>Длина</th>
                <th>Толщина</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div id="orders_div_pagination">

    </div>
</div>
<div id="client_send_pin_code_container">
    <div class="client_send_pin_code_inner">
        <p>Введите ПИН-код, отправленный на телефонный номер клиента</p>
        <input type="text" id="client_send_pin_code_input">
        <button id="client_send_pin_code_button">Подтвердить</button>
        <div id="client_send_pin_code_message"></div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script type="text/javascript" src="/js/main.js?v=1.01"></script>
<script>
    async function get_orders_goods(order, order_name) {
        let orders_table = $('#get_client_orders table#orders_html_table');
        let receipt_container = $('#orders_inner_receipt');
        let parts_container = $('#orders_inner_parts');
        let request = await fetch(app_dir + '/get_orders_goods?order=' + order);
        let body = await request.json();
        $('#orders_inner_receipt_order').html(order_name);
        $('#orders_inner_receipt table tbody').html(' ');
        let total = 0;
        body.forEach(element => {
            let price = Math.floor(element.price * element.count * 100) / 100;
            let good = `
                <tr>
                    <td>${element.id}</td>
                    <td>${element.name}</td>
                    <td>${element.type}</td>
                    <td>${element.count}</td>
                    <td>${element.price.toLocaleString('fr')}</td>
                    <td>${price.toLocaleString('fr')}</td>
                </tr>
            `;
            total += price;
            $('#orders_inner_receipt table tbody').append(good);
        });
        total = Math.floor(total * 100) / 100;
        $('#orders_inner_receipt table tbody').append(`
            <tr>
                <td colspan="6" style="text-align: right">
                     <b>Всего: ${total.toLocaleString('fr')}</b>
                </td>
            </tr>
          `);
        orders_table.hide();
        parts_container.hide();
        receipt_container.show();
        $('.orders_filters').hide();
        $('#orders_div_pagination').hide();



    };
    async function get_orders_parts(order, order_name) {
        let orders_table = $('#get_client_orders table#orders_html_table');
        let receipt_container = $('#orders_inner_receipt');
        let parts_container = $('#orders_inner_parts');
        let request = await fetch(app_dir + '/get_orders_parts?order=' + order);
        let body = await request.json();
        $('#orders_inner_parts_order').html(order_name);
        $('#orders_inner_parts table tbody').html(' ');
        body.forEach(element => {
            let part = `
                <tr>
                    <td>${element.id}</td>
                    <td>${element.name}</td>
                    <td>${element.description}</td>
                    <td>${element.W.toFixed(2)} мм</td>
                    <td>${element.L.toFixed(2)} мм</td>
                    <td>${element.T.toFixed(2)} мм</td>
                </tr>
            `;
            $('#orders_inner_parts table tbody').append(part);
        });
        orders_table.hide();
        receipt_container.hide();
        parts_container.show();
        $('.orders_filters').hide();
        $('#orders_div_pagination').hide();


    };

</script>
@if($CtRhTn == 'true')
    <script>
        $(document).ready(async function () {
            $(window).off('beforeunload');
            $(window).unbind('beforeunload');
            window.onbeforeunload = null;
            ///await window.saveFromJS();
            console.log('Запрос на перезапись cессии');
            await save_xml_project($('#saveContainer').val());
            console.log('Сессия с данными перезаписана');
            let request = await fetch('/send_project', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN':  new_csrf_token,
                    'X-XSRF-TOKEN':  new_csrf_token,
                },
                credentials: 'include',
                body: JSON.stringify({
                    _token: $('#continue_form input[name="_token"]').val(),
                    project_data: $('#continue_form input[name="project_data"]').val(),
                    user_place: $('#continue_form input[name="user_place"]').val(),
                    user_type_edge: $('#continue_form input[name="user_type_edge"]').val(),
                })
            });
            let response = await request.text();
            await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
            console.log('Data_id записан в сессию');
            $('#extra_buttons_container #extra_operations_button').attr('href', `${main_dir}/index.php?data_id=${response}`);
            $('#extra_buttons_container #get_reciepe_button').attr('href', `${main_dir}/check_sheet_parts.php?data_id=${response}`);
            $('#extra_buttons_container').show();
        });
    </script>
    <style>
        #extra_buttons_container {
            display: block!important;
        }
    </style>
@endif
</body>
</html>
