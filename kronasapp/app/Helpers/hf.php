<?php
/**
* Функции-хелперы, которые можно вызвать в любой части приложения Laravel
**/



/**
* Функция 'putDebugLog' помещает в файл по пути files/dewlogs переданную строку, подставляя в начало дату.
* Функция создавалась для вывода информации о том какое условие сработало.
* Строки старше недели затирает zzz.php для уменьшения нагрузки на память.
**/
if (! function_exists('putDebugLog')) {
    function putDebugLog($str) {
    	$fail = null;
    	$file = config('app.serv_main_dir').'files/devlogs/debug.log';
        // Сохраняем в файл
        if ($fail) file_put_contents($file, date('Y-m-d h:i:s', time()).' --- '.$fail."\n", FILE_APPEND);
        else file_put_contents($file, date('Y-m-d h:i:s', time()).' --- '.$str."\n", FILE_APPEND);
    }
}
?>