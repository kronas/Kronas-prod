<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\MaterialSheet;
use App\Models\MaterialStock;
use App\Models\Managers;
use App\Models\Band;
use App\Models\Clients;
use App\Models\BandStock;
use App\Models\Goods;
use App\Models\Orders;
use App\Models\Place;
use App\Models\Project;
use App\Models\ProjectIn;
use App\Models\Tool;
use App\Models\XncTemplate;
use App\Models\XncTemplateFolder;
use App\Models\ToolEdgeline;
use App\Models\Manufacturer;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IndexController extends Controller
{

	private function build_folder($id, $name, $items){
		$rs = "<folder id='$id' name='$name' hasChildFolders='false'>";
        foreach ($items as $row) {
            $rs .= $row->build_catalog_item();
        }
        $rs .= "</folder>";

        return $rs;
	}

    private function build_plate_materials()
    {
        return $this->build_folder(1, 'Плитные материалы', Material::where('ST', false)->get());
    }

    private function build_tabletops()
    {
        return $this->build_folder(1, 'Столешницы', Material::where('ST', true)->get());
    }

    private function build_catalog()
    {
        $rs = "<response><catalog name='Листовой материал'>";

        // build materials folder items
        $rs .= $this->build_plate_materials();
        $rs .= $this->build_tabletops();

        // build bands folder items
        $rs .= $this->build_bands();

		$rs .= "</catalog></response>";

        return $rs;
    }

    private function get_materials($items){
        $_items = [];

        foreach ($items as $item) {
            $_items[] = $item->to_json();
        }

        return $_items;
    }

    private function get_places($items){
        $_items = [];

        foreach ($items as $item) {
            $_items[] = $item->to_json();
        }

        return $_items;
    }

    private function get_tools_data(){
        $tools = Tool::all();

        return [
            'total_count' => $tools->count(),
            'items' => $tools
        ];
    }

    private function get_xnc_folders_data(){
        $parent_xnc_folders = XncTemplateFolder::where('parent_id', 0)
            ->where('visible', true);

        $all_folders = XncTemplateFolder::where('visible', true);

        return [
            'parent_foders_count' => $parent_xnc_folders->count(),
            'items' => $parent_xnc_folders->get(),
            'all_items' => $all_folders->get()
        ];
    }

    public function index(Request $request, $project_id=null )
    {
        ini_set('session.save_path',config('app.serv_main_dir').'/temp');
        session_start();

        $saved_data = $request->session()->get('project_data');
        if (isset($saved_data)) {
            $start_project = $saved_data;
        } else {
            $start_project = $request->input('project_data');
        }


        $checked_poject = $request->session()->get('checked_project_data');

        if ($project_id && $request->path() == 'projects/'.$project_id ){
            try {
                $sp_file = ProjectIn::find($project_id);

                if ($sp_file){
                  $start_project = file_get_contents(storage_path("../../files/{$sp_file->file}"));
                  $start_project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $start_project);
                  $start_project = str_replace("\r", "", $start_project);
                  $start_project = str_replace("\n", "", $start_project);
                  
                  $checked_poject = $start_project;
                }
            } catch (\Throwable $e) {}
        }


        $all_version_projects = [];

        if ($project_id && $request->path() == 'projects_new/'.$project_id ) { ///&& !($request->session()->has('order_1c_project'))

            $order_project_id = strval($request->input('order_project_id'));
            
            // echo($order_project_id);
            // exit('---333');

            $project_status_checked = substr($project_id, 0, 2);

            if($project_status_checked == '1_' || $project_status_checked == '2_') {
                $project_status = substr($project_id, 0, 1);
                $order_1c_project = substr($project_id, 2);
            } else {
                $project_status = substr(base64_decode($project_id), 0, 1);
                $order_1c_project = substr(base64_decode($project_id), 2);
            }

            $order_data = Orders::where(['ID' => $order_1c_project])->first()->toArray();
            if($order_data['readonly'] == 1) {
                $project_status = 2;
            }
            session(['order_manager_id' => $order_data['manager']]);
            session(['order_status_data' => $order_data['status']]);

            if($request->session()->get('order_1c_project') != $order_1c_project || $request->session()->get('order_project_id') != $order_project_id) 
            {
                session(['order_status' => $project_status]);
                session(['order_1c_project' => $order_1c_project]);
                session(['order_project_id' => $order_project_id]);
                //.. 04.03.2021 При создании проекта устанавливается филиал, к которому привязан менеджер
                if (!$request->session()->get('new_project_place')) {
                    session(['user_place' => $order_data['PLACE']]);
                }
                session(['client_id' => $order_data['CLIENT_ID']]);
                session(['client_code' => $order_data['CLIENT']]);

                try {
                    if($order_project_id > 0) {
                        $sp_file = Project::where(['ORDER1C' => $order_1c_project,'PROJECT_ID' => $order_project_id])->first();
                    } else {
                        $sp_file = Project::where(['ORDER1C' => $order_1c_project])->orderBy('DATE', 'DESC')->first();
                        session(['order_project_id' => $sp_file['PROJECT_ID']]);
                        //.. 04.03.2021 При создании проекта устанавливается филиал, к которому привязан менеджер
                        if (!$request->session()->get('new_project_place')) {
                            session(['user_place' => $sp_file['place']]);
                        }
                        session(['user_type_edge' => $sp_file['user_type_edge']]);
                    }

                    if(!$request->session()->has('client_phone') && !$request->session()->has('client_email') && !$request->session()->has('client_fio')) {

                        session(['client_email' => $order_data['client_email']]);
                        session(['client_fio' => $order_data['client_fio']]);
                        session(['client_phone' => $order_data['client_phone']]);
                        session(['order_akcent_ids' => $order_data['DB_AC_ID'].'('.$order_data['DB_AC_NUM'].')']);
                    }

                    if ($sp_file){
                        //.. 23.11.2020 - чтение Здесь берём PROJECT_ID и читаем одноимённый файл
                        $dirdig = ceil($sp_file->PROJECT_ID / 1000) - 1;
                        $projectFolder = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/' . $sp_file->PROJECT_ID . '.txt';

                        if ($projectFile = file_get_contents($projectFolder)) {
                            $start_project = base64_decode($projectFile);
                        } else if (Storage::disk('files_project_out')->exists($sp_file->PROJECT_ID.'.txt')) {
                            $start_project = base64_decode(Storage::disk('files_project_out')->get($sp_file->PROJECT_ID.'.txt'));
                        } else {
                            //.. Поддержка чтения из БД (Устаревшая запись)
                            $start_project = base64_decode($sp_file->MY_PROJECT_OUT);
                        }
                        $order_data = Orders::where(['ID' => $order_1c_project])->first()->toArray();
                        session(['order_akcent_ids' => $order_data['DB_AC_ID'].'('.$order_data['DB_AC_NUM'].')']);
                        session(['project_data' => $start_project]);
                        session(['strlen_project' => strlen($sp_file->MY_PROJECT_OUT)]);

                        $start_project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $start_project);
                        $start_project = str_replace("\r", "", $start_project);
                        $start_project = str_replace("\n", "", $start_project);

                    }
                } catch (\Throwable $e) {}
            }

            session(['order_project_id' => $order_project_id]);
        }

        
        if($request->session()->get('order_1c_project')) {
            $all_version_projects_db = Project::where(['ORDER1C' => $request->session()->get('order_1c_project')])->orderBy('DATE', 'DESC')->get()->toArray();
            foreach($all_version_projects_db as $project) {
                //.. 04.03.2021 Устанавливаем филиал, который указан в загружаемой версии проекта
                if ($request->session()->get('order_project_id') == $project['PROJECT_ID']) {
                    session(['user_place' => $project['place']]);
                }
                $manager_porject = Managers::where(['id' => $project['manager_id']])->first();
                $client_porject = Clients::where(['client_id' => $project['client_id']])->first();
                if($manager_porject && $manager_porject->id > 0) {
                    $author_project = 'M';
                } elseif ($client_porject && $client_porject->client_id > 0) {
                    $author_project = 'К';
                } else {
                    $author_project = 'Н';
                }
                $all_version_projects['data'][] = [
                    'link' => config('app.app_laravel_dir').'/projects_new/1_'.$request->session()->get('order_1c_project').'?order_project_id='.$project['PROJECT_ID'],
                    'author' => $author_project,
                    'date' => $project['DATE'],
                    'project_id' => $project['PROJECT_ID']
                ];
                if($request->session()->get('order_project_id') == $project['PROJECT_ID']) {
                    $all_version_projects['actual'] = [
                        'link' => config('app.app_laravel_dir').'/projects_new/1_'.$request->session()->get('order_1c_project').'?order_project_id='.$project['PROJECT_ID'],
                        'author' => $author_project,
                        'date' => $project['DATE'],
                        'project_id' => $project['PROJECT_ID']
                    ];
                }
            }
        }

        if($request->session()->get('manager_id') > 0 && (!$request->session()->get('client_id') || $request->session()->get('client_id') == '')) {
            return redirect(config('app.app_main_dir').'/manager_all_order.php?manager_id='.$request->session()->get('manager_id'));
        }


        if( isset($checked_poject) ) {
            $checked_project_bool = true;
        } else {
            $checked_project_bool = false;
        }

        $CtRhTn = $request->input('CtRhTn');

        if(session('user_place')) {
            $place_id = $request->session()->get('user_place');
        } else {
            $place_id = $request->input('user_place', 1);
        }

        if(session('user_type_edge')) {
            $type_edge = $request->session()->get('user_type_edge');
        } else {
            $type_edge = $request->input('user_type_edge', 'eva');
        }

        $tools = $this->get_tools_data();
        $xnc_folders = $this->get_xnc_folders_data();

        $xnc_root_folders = [];
        foreach ($xnc_folders['items'] as $folder) {
            $xnc_root_folders[$folder->id] = [
                'id' => $folder->id,
                'name' => $folder->name
            ];
        };

       $folders = [];
        foreach ($xnc_folders['all_items'] as $folder) {
            $folders[$folder->id] = [
                 'id' => $folder->id,
                'parent_id' => $folder->parent_id,
                'name' => $folder->name,
                'child_folders' => $folder->get_child_folders(),
                'templates' => $folder->get_templates(),
                'path' => $folder->get_path()
            ];
        };


        foreach (MaterialSheet::all() as $sheet) {
            $materials_stock[] = [
                'id' => $sheet->MATERIAL_ID,
                'code' => $sheet->CODE,
                'width' => $sheet->W,
                'length' => $sheet->L,
                'count' => $sheet->COUNT,
                'place' => $sheet->place['NAME'],
                'place_id' => $sheet->place['PLACES_ID'],
                'sheet' => true,
                'isSheet' => false,
                'from_stock' => false
            ];
        };
//        $bd_places = [];
//        foreach (Place::all() as $place) {
//            $bd_places[] = [
//                'id' => $place->PLACES_ID,
//                'code' => $place->NAME,
//                'width' => $place->ADRESS
//            ];
//        };



        $toolsEdgeline2 = ToolEdgeline::all();
        $edge2 = array();
        foreach($toolsEdgeline2 as $f)
        {
            if (!in_array($f->PLACE,$edge2))
            {
                if ($f->pur==1) $edge2[$f->PLACE]=1;
            }
        }
// return ("!!");
//        $bands_stock = [];
//        foreach (BandStock::where('PLACE_ID', $place_id)->get() as $item) {
//            $bands_stock[] = [
//                'id' => $item->BAND_ID,
//                'code' => $item->band['CODE'],
//                'width' => $item->band['W'],
//                'thikness' => $item->band['T'],
//                'count' => $item->COUNT,
//                'sheet' => false,
//            ];
//        };
//
//        $materials_stock = [];
//        foreach (MaterialStock::all() as $item) {
//            $materials_stock[] = [
//                'id' => $item->MATERIAL_ID,
//                'code' => $item->material['CODE'],
//                'width' => $item->material['W'],
//                'length' => $item->material['L'],
//                'count' => $item->COUNT,
//                'place' => $item->place['NAME'],
//                'place_id' => $item->place['PLACES_ID'],
//                'sheet' => true,
//                'isSheet' => false,
//                'from_stock' => true
//            ];
//
//            if ($item->material['ST'] == 0){
//                $materials_stock[] = [
//                    'id' => $item->MATERIAL_ID,
//                    'code' => $item->material['CODE'],
//                    'width' => floatval($item->material['W'])/2-5,
//                    'length' => $item->material['L'],
//                    'count' => intval($item->COUNT)*2,
//                    'place' => $item->material['PLACES_ID'],
//                    'sheet' => true,
//                    'isSheet' => true,
//                    'from_stock' => true
//                ];
//            };
//        };


        // return 'Start-1';
        $tools_per_place = [
            1 => [
                'CS' => "<tool.cutting csDirectCut='1' cCostByItemRound='22' swSawthick='4.8' swMaxturns='4' swMinSizeBand='0' swMinPruning='0' swComplexBand='false' swTrimIncludeSaw='false' swMaxSizeBand='0' swSortInBand='0' swSort='2' swPackageHeight='65' swMaxLengthBand='5500'/>",
                'EL' => "<tool.edgeline elWastePRC='0.1' elRestSide='35' elMinSize='120' elWidthPreJoint='1' elCost='0' />",
            ],
        ];
        for ($i=2;$i<10;$i++)
        {
            $tools_per_place[$i]= $tools_per_place[1];
        }

        if (isset($tools_per_place[$place_id])) {
            $tools_conf = $tools_per_place[$place_id];
        } else {
            $tools_conf = $tools_per_place[1];
        }

        if (!$start_project){
            $start_project = "<project name=\"New\" currency=\"грн\" version=\"1\" costOperation=\"0\" costMaterial=\"0\" cost=\"0\" ><good typeId=\"product\" count=\"1\" costMaterial=\"0\" costOperation=\"0\" cost=\"0\" name=\"product_new\" id=\"2\"/></project>";
        }

        $start_project = str_replace("\'", "'", $start_project);
        $start_project = str_replace("'", "\'", $start_project);

        $isAuth = $request->session()->get('isAdmin');
        if($isAuth == 1) {
            $CtRhTn = 'cfvjdfhjgfhfdjp';
        }

        $checked_poject = $request->session()->get('checked_project_data');
        if( isset($checked_poject) && $checked_poject != '' ) {
            $checked_project_bool = 1;
        } else {
            $checked_project_bool = 0;
        }

        $start_project_data = $request->session()->get('back_to_start');
        if( isset($start_project_data) && $start_project_data == true) {
            $back_start_project = true;
        } else {
            $back_start_project = false;
        }

        $mpr_import_error = $request->session()->get('mpr_error');;
        if( isset($mpr_import_error) && $mpr_import_error != '') {
            session()->pull('mpr_error');
        } else {
            $mpr_import_error = false;
        }
        if(!isset($project_status)) {
            $project_status = 0;
        }
        if($request->session()->get('client_id') && $request->session()->get('client_id') != '') {
            $client_data = Clients::where(['client_id' => $request->session()->get('client_id')])->get()->first();
            session(['client_phone' => $client_data->tel]);
            session(['client_code' => $client_data->code]);
            session(['client_email' => $client_data->{'e-mail'}]);
            $contragent = DB::table('client_code')->where(['code_ac' => $request->session()->get('client_code')])->first();
            session(['client_fio' => $contragent->name]);
            session(['client_password' => $client_data->pass]);
        }
        $upload_project_error = 0;
        $upload_project_error_text = '';

        if($request->session()->get('upload_project_error') == 1){
            $upload_project_error = 1;
            $upload_project_error_text = $request->session()->get('upload_project_error_text');
            session()->pull('upload_project_error');
            session()->pull('upload_project_error_text');
        }
        $confirm_project_error = 0;
        $confirm_project_error_text = '';
        if($request->session()->has('error')){
            $confirm_project_error = 1;
            $confirm_project_error_text = $request->session()->get('error');
            session()->pull('error');
        }

        if(!$request->session()->get('data_id')) {
            session_regenerate_id();
            session(['data_id' => session_id()]);
            Storage::put('giblab_sessions/'.$request->session()->get('data_id').'.txt', ' ');
            chmod(config('app.serv_main_dir').'/kronasapp/storage/app/giblab_sessions/'.$request->session()->get('data_id').'.txt', 0777);

            //..07.12.2020 Сохраняем сессию юзера во временный файл
            session(['saveAutorization' => session('data_id')]);
            Storage::put('giblab_sessions/'.session('data_id').'.txt', serialize(session()->all()));
        }


        if (strpos($start_project, 'CfvjdfhjgfhfdjpDfhdfhbx') === false) {
            $rsflag = 0;
        } else {
            $rsflag = 1;
        }
        //..28.12.2020 Если менеджер
        if(session('user')) {
            $user = session('user');
            if ($user['role'] == 'manager') $managerflag = 1;
            else $managerflag = 0;
        } else $managerflag = 0;

        //.. 1.02.2022 Если админ
        if(session('user')) {
            $user = session('user');
            if ($user['role'] == 'admin') $adminflag = 1;
            else $adminflag = 0;
        } else $adminflag = 0;

        //.. 27.08.2021 Если админ или менеджер
        if(session('user')) {
            $user = session('user');
            if ($user['role'] !== 'client' && isset($user['access'])) $access = $user['access'];
            else $access = 0;
        } else $access = 0;
        
        //..25.11.2020 Если снаружи хотим сохранить сессию
        // ssnrst - SeSsioN ReSTore
        if ($request->input('ssnrst')) {
            $ssnrst = $request->input('ssnrst');
            session(['ssnrst' => $ssnrst]);
        } else if (session('saveAutorization')) {
            $ssnrst = session('saveAutorization');
        } else $ssnrst = '';

        //.. 04.03.2021 При создании проекта устанавливается филиал, к которому привязан менеджер
        if ($request->session()->get('new_project_place')) {
            $request->session()->forget('new_project_place');
        }
        session(['additional_request_uri' => $request->getRequestUri()]);
        $data = [
            'start_project' => $start_project,

            'user_place' => $place_id,
            'user_type_edge' => $type_edge,

            'places' => Place::where('active', 1)->get(),
            'places_js' => json_encode([
                'places' => $this->get_places(Place::where('active', 1)->get()),
            ]),
//            'stock' => json_encode([
//                'bands' => $bands_stock,
//                'materials' => $materials_stock,
//            ]),
//            'catalog' => json_encode([
//                'plate_materials' => $this->get_materials(Material::where('ST', false)->get()),
//                'tabletops' => $this->get_materials(Material::where('ST', true)->get()),
//                'bands' => $this->get_materials(Band::all()),
//            ]),
            'folder_tools' => json_encode($tools_conf),
            'tools' => [
                'total_count' => $tools['total_count'],
                'items' => $tools['items']
            ],
            'xnc_folders' => json_encode([
                'root_folders' => [
                    'count' => $xnc_folders['parent_foders_count'],
                    'items' => $xnc_root_folders
                ],
                'root_items' => XncTemplate::where('folder_id', null)->get(),
                'folders' => $folders
            ]),
            'xnc_template_folders' => [
                'parend_folders_count' => $xnc_folders['parent_foders_count'],
                'all_items' => $xnc_folders['all_items'],
                'items' => $xnc_folders['items']
            ],
            'CtRhTn' => ($CtRhTn == 'cfvjdfhjgfhfdjp')?'true':'false',
            'access' => $access,
            'manager_name' => $request->session()->get('manager_name'),
            'manager_id' => $request->session()->get('manager_id'),
            'client_email' => $request->session()->get('client_email'),
            'client_name' => $request->session()->get('client_fio'),
            'client_code' => $request->session()->get('client_code'),
            'client_password' => $request->session()->get('client_password'),
            'client_phone' => $request->session()->get('client_phone'),
            'client_id' => $request->session()->get('client_id'),
            'order_comment' => $request->session()->get('order_comment'),
            'edge' => $toolsEdgeline2,
            'edge2' => $edge2,
            'has_checked_project' => $checked_project_bool,
            'session_id' => $request->session()->get('data_id'),
            //..25.11.2020 session restore
            'ssnrst' => $ssnrst, 
            'back_start_project' => $back_start_project,
            'mpr_import_error' => $mpr_import_error,
            'app_dir' => config('app.app_laravel_dir'),
            'main_dir' => config('app.app_main_dir'),
            'project_status' => $project_status,
            'tree' => Goods::where('PARENT', 0)->get(),
            'upload_project_error' => $upload_project_error,
            'upload_project_error_text' => $upload_project_error_text,
            'confirm_project_error' => $confirm_project_error,
            'confirm_project_error_text' => $confirm_project_error_text,
            'order_akcent_full' => $request->session()->get('order_akcent_ids'),
            'order_1c_project' => $request->session()->get('order_1c_project'),
            'order_status' => $request->session()->get('order_status'),
            'order_status_data' => $request->session()->get('order_status_data'),
            'warning_status' => 1,
            'all_version_projects' => $all_version_projects,
            'rsflag' => $rsflag,
            'managerflag' => $managerflag,
            'adminflag' => $adminflag,
            'project_restore_user_ids' => require config('app.serv_main_dir').'system/config/project_restore_user_ids.php',
            //.. 06.09.2022 Тоценко А. Параметр, есть ли раскрой при загрузке .kronas файла
            'user_sheet_cutting' => $request->session()->get('user_sheet_cutting'),
        ];

        return view('gibform', $data);
    }
}

