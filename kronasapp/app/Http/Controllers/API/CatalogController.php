<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Material;
use App\Models\Band;
use App\Models\XncTemplate;
use App\Models\XncTemplateFolder;
use Validator;


class CatalogController extends BaseController
{

    private function build_folder($id, $name, $items){
        $rs = "<folder id='$id' name='$name' hasChildFolders='false'>";
        foreach ($items as $row) {
            $rs .= $row->build_catalog_item();
        }
        $rs .= "</folder>";

        return $rs;
    }

    private function build_materials()
    {
        return $this->build_folder(1, 'Листовой материал', Material::all());
    }

    private function build_bands()
    {
        return $this->build_folder(2, 'Кромка', Band::all());
    }

    private function build_catalog()
    {
        $rs = "<response><catalog name='Kronas'>";

        // build materials folder items
        $rs .= $this->build_materials();

        // build bands folder items
        $rs .= $this->build_bands();

        $rs .= "</catalog></response>";

        return $rs;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function materials()
    {
        $rs = "<response>";
        $rs .= "<catalog name='Kronas :: Листовой материал'>";
        $rs .= $this->build_materials();
        $rs .= "</catalog>";
        $rs .= "</response>";

        $response = [
            'success' => true,
            'data'    => $rs
        ];

        return response()->json($response, 200);
    }

    public function bands()
    {
        $rs = "<response>";
        $rs .= "<catalog name='Кромка'>";
        $rs .= $this->build_bands();
        $rs .= "</catalog>";
        $rs .= "</response>";

        $response = [
            'success' => true,
            'data'    => $rs
        ];

        return response()->json($response, 200);
    }

    public function createXNCFolder(Request $request)
    {
        $folder_id = $request->input('folder_id');
        $name = $request->input('name');
        $content = $request->input('content');

        if (!$folder_id){
            $folder_id = 0;
        }

        $folder = new XncTemplateFolder;
        $folder->name = $name;
        $folder->parent_id = $folder_id;
        $folder->visible = 1;
        $folder->save();

        $response = [
            'success' => true,
            'data'    => $folder->id
        ];

        return response()->json($response, 200);
    }

    public function XNCRename(Request $request)
    {
        $is_folder = $request->input('isFolder');
        $folder_id = $request->input('folder_id');
        $id = $request->input('id');
        $name = $request->input('name');
        $content = $request->input('content');

        if ($is_folder){
            $item = XncTemplateFolder::find($id);
        } else {
            $item = XncTemplate::find($id);
        }

        $item->name = $name;
        $item->save();

        // dd($item);

        $response = [
            'success' => true,
            'data'    => $item->id
        ];

        return response()->json($response, 200);
    }

    public function XNCDelete(Request $request)
    {
        $is_folder = $request->input('is_folder');
        $folder_id = $request->input('folder_id');

        $id = $request->input('id');
        $content = $request->input('content');

        if ($is_folder){
            $item = XncTemplateFolder::where('id', $id)->delete();
        } else {
            $item = XncTemplate::where('id', $id)->delete();
        }

        $response = [
            'success' => true,
            'data'    => ''
        ];

        return response()->json($response, 200);
    }

    public function XNCMove(Request $request)
    {
        $name = $request->input('name');
        $content = $request->input('content');

        $is_folder = $request->input('is_folder');
        $folder_id = $request->input('folder_id');
        $new_folder_id = $request->input('new_folder_id');

        $id = $request->input('id');

        if ($is_folder){
            $item = XncTemplateFolder::where('id', id)->get();
            $item->parent_id = $new_folder_id;
        } else {
            $item = XncTemplate::where('id', id)->get();
            $item->folder_id = $new_folder_id;
        }

        $item->save();

        $response = [
            'success' => true,
            'data'    => $item->id
        ];

        return response()->json($response, 200);
    }

    public function XNCSave(Request $request)
    {
        $name = $request->input('name');
        $content = $request->input('content');
        $data = $request->input('data');
        $folder_id = $request->input('folder_id', null);

        $item = XncTemplate::where('name', $name)->where('folder_id', $folder_id);

        if (!$item){
            $item = new XncTemplate;
        };

        $item->name = $name;
        $item->data = $data;
        $item->folder_id = $folder_id;
        $item->save();

        $response = [
            'success' => true,
            'data'    => $item->id
        ];

        return response()->json($response, 200);
    }
}