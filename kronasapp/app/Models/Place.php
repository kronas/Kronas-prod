<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PLACE
 * 
 * @property int $PLACES_ID
 * @property string $NAME
 * @property string $ADRESS
 * @property string $PHONE
 *
 * @package App\Models
 */
class Place extends Model
{
	protected $table = 'PLACES';
	protected $primaryKey = 'PLACES_ID';
	public $timestamps = false;

	protected $fillable = [
		'NAME',
		'ADRESS',
		'PHONE',
        'PLACES_ID'
	];

	// public function b_a_n_d_s_t_o_c_k()
	// {
	// 	return $this->hasOne(BANDSTOCK::class, 'PLACE_ID');
	// }

	// public function e_q_u_i_p_m_e_n_t()
	// {
	// 	return $this->hasMany(EQUIPMENT::class, 'PLACE');
	// }

	// public function m_a_t_e_r_i_a_l_s_t_o_c_k()
	// {
	// 	return $this->hasOne(MATERIALSTOCK::class, 'PLACE_ID');
	// }

	// public function p_r_o_j_e_c_t__i_n_s()
	// {
	// 	return $this->hasMany(PROJECTIN::class, 'place');
	// }

	// public function s_h_e_e_t__c_o_m_p_l_e_t_e_s()
	// {
	// 	return $this->hasMany(SHEETCOMPLETE::class, 'PLACE');
	// }

	// public function t_o_o_l__c_u_t_t_i_n_g__e_q_u_i_p_m_e_n_t_s()
	// {
	// 	return $this->hasMany(TOOLCUTTINGEQUIPMENT::class, 'PLACE');
	// }

	public function t_o_o_l__e_d_g_e_l_i_n_e__e_q_u_i_p_m_e_n_t_s()
	{
	  return $this->hasMany(TOOLEDGELINEEQUIPMENT::class, 'PLACE');
	}
    public function to_json()
    {
        if ($this->pic){
            $image = '/images/pic/'. $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return [
            'id' => $this->PLACES_ID,
            'name' => $this->NAME,
            'adress' => $this->ADRESS,
            'phone' => $this->PHONE
        ];
    }
}
