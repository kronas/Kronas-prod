<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TreeBands extends Model {

    protected $table = 'GOOD_TREE_BAND_CONN';

    public function band()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Band::class, 'BAND_ID');
    }
    public function to_json()
    {
        if ($this->pic){
            $image = '/images/pic/'. $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return [
            'code' => $this->CODE,
            'name' => $this->NAME,
            'unit' => $this->UNIT,
            'price' => $this->COST,
            'thickness' => $this->T,
            'width' => $this->W,
            'count' => $this->COUNT,
            'image' => $image,
        ];
    }

}