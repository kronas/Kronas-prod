<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ClientCodes extends Model {

    protected $table = 'client_code';
    protected $primaryKey = 'idcl';
    public $timestamps = false;

    protected $casts = [
        'idcl' => 'int',
        'code_ac' => 'int',
        'name' => 'varchar',
        'code_1c' => 'int',
        'manager' => 'int',
    ];

    protected $fillable = [
        'idcl',
        'code_ac',
        'name',
    ];

}