<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tool
 * 
 * @property int $tools_id
 * @property string $name
 * @property string $comment
 * @property float $diameter
 * @property int $plane
 * @property bool $through
 * @property string $depth
 *
 * @package App\Models
 */
class Tool extends Model
{
	protected $table = 'tools';
	protected $primaryKey = 'tools_id';
	public $timestamps = false;

	protected $casts = [
		'diameter' => 'float',
		'plane' => 'int',
		'through' => 'bool'
	];

	protected $fillable = [
		'name',
		'comment',
		'diameter',
		'plane',
		'through',
		'depth'
	];
}
