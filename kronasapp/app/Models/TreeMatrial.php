<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TreeMatrial extends Model {

    protected $table = 'GOOD_TREE_MATERIAL_CONN';

    public function material()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Material::class, 'MATERIAL_ID');
    }
    public function to_json()
    {
        if ($this->pic){
            $image = '/images/pic/'. $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return [
            'code' => $this->CODE,
            'name' => $this->NAME,
            'unit' => $this->UNIT,
            'price' => $this->COST,
            'thickness' => $this->T,
            'width' => $this->W,
            'length' => $this->L,
            'count' => $this->COUNT,
            'image' => $image,
            'st' => $this->ST
        ];
    }

}