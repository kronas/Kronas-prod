<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MaterialPic
 * 
 * @property int $MATERIAL_ID
 * @property string $FILE_NAME_BIG
 * @property string $FILE_NAME_SMALL
 * @property int $SORT
 * @property string $DESCRIPTION
 * 
 * @property Material $material
 *
 * @package App\Models
 */
class MaterialPic extends Model
{
	protected $table = 'MATERIAL_PIC';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MATERIAL_ID' => 'int',
		'SORT' => 'int'
	];

	protected $fillable = [
		'MATERIAL_ID',
		'FILE_NAME_BIG',
		'FILE_NAME_SMALL',
		'SORT',
		'DESCRIPTION'
	];

	public function material()
	{
		return $this->belongsTo(Material::class, 'MATERIAL_ID');
	}
}
