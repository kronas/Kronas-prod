<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model {
    protected $table = 'GOOD_PROPERTIES_MATERIAL';

    public function materials()
    {
        return $this->hasMany('App\Models\Material');
    }
}