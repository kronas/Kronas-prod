<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model {

    protected $table = 'client';
    protected $primaryKey = 'client_id';
    public $timestamps = false;

    protected $casts = [
        'client_id' => 'int',
        'name' => 'varchar',
        'tel' => 'varchar',
        'e-mail' => 'varchar',
        'code' => 'int',
        'pass' => 'varchar',
        'access' => 'tinyint',
    ];

    protected $fillable = [
        'client_id',
        'name',
        'tel',
        'e-mail',
        'code'
    ];

}