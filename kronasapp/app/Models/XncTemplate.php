<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class XncTemplate
 * 
 * @property int $id
 * @property int $folder_id
 * @property string $name
 * @property string $data
 *
 * @package App\Models
 */
class XncTemplate extends Model
{
	protected $table = 'xnc_template';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'folder_id' => 'int'
	];

	protected $fillable = [
		'id',
		'folder_id',
		'name',
		'data'
	];
}
