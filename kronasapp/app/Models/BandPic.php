<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BandPic
 * 
 * @property int $BAND_ID
 * @property string $FILE_NAME_BIG
 * @property string $FILE_NAME_SMALL
 * @property int $SORT
 * @property string $DESCRIPTION
 * 
 * @property Band $band
 *
 * @package App\Models
 */
class BandPic extends Model
{
	protected $table = 'BAND_PIC';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BAND_ID' => 'int',
		'SORT' => 'int'
	];

	protected $fillable = [
		'BAND_ID',
		'FILE_NAME_BIG',
		'FILE_NAME_SMALL',
		'SORT',
		'DESCRIPTION'
	];

	public function band()
	{
		return $this->belongsTo(Band::class, 'BAND_ID');
	}
}
