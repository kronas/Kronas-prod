<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ToolEdgeline extends Model {
    protected $table = 'TOOL_EDGELINE_EQUIPMENT';
}