<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\ClientMaterialInfo;
use App\Models\Clients;

class ClientMaterial extends Model {

    protected $table = 'MATERIAL_KL_STOCK_COUNT';

    public function info()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(ClientMaterialInfo::class, 'Material_client','mc_id');
    }
    public function client()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Clients::class, 'Client','client_id');
    }
    public function place()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Place::class, 'Place','PLACES_ID');
    }

}
