<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MATERIALSTOCK
 * 
 * @property int $MATERIAL_ID
 * @property int $PLACE_ID
 * @property float $COUNT
 * 
 * @property MATERIAL $material
 * @property PLACE $place
 *
 * @package App\Models
 */
class MaterialStock extends Model
{
	protected $table = 'MATERIAL_STOCK';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'MATERIAL_ID' => 'int',
		'PLACE_ID' => 'int',
		'COUNT' => 'float'
	];

	protected $fillable = [
		'MATERIAL_ID',
		'PLACE_ID',
		'COUNT'
	];

	public function material()
	{
		return $this->belongsTo(Material::class, 'MATERIAL_ID');
	}

	public function place()
	{
		return $this->belongsTo(Place::class, 'PLACE_ID');
	}
}
