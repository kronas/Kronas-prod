<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class Orders extends Model {

    protected $table = 'ORDER1C';
    protected $fillable = ['CLIENT_ID', 'manager', 'status','DB_AC_IN','DB_AC_NUM','DB_AC_ID', 'CLIENT', 'PLACE'];
    public $timestamps = false;

    public function manager_info()
    {
        return $this->belongsTo(Managers::class, 'manager','id');
    }
    public function place()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Place::class, 'PLACE','PLACES_ID');
    }
    public function project()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Project::class, 'ID','ORDER1C');
    }
    public function client_info()
    {
        ///return $this->hasOne('App\Models\Material', , 'MATERIAL_ID');
        return $this->belongsTo(Clients::class, 'CLIENT_ID','client_id');
    }

}
