<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Band
 * 
 * @property int $BAND_ID
 * @property float $W
 * @property float $T
 * @property string $NAME
 * @property string $KR_NAME
 * @property int $COUNT
 * @property float $COST
 * @property string $TYPENAME
 * @property string $TYPEID
 * @property string $UNIT
 * @property int $CODE
 * @property string $MY_1C_NOM
 * @property string $MY_1C_HAR
 * @property bool $MY_LASER
 * @property string $MY_TYPE
 * 
 * @property BANDPIC $pic
 *
 * @package App\Models
 */
class Band extends Model
{
	protected $table = 'BAND';
	protected $primaryKey = 'BAND_ID';
	public $timestamps = false;

	protected $casts = [
		'W' => 'float',
		'T' => 'float',
		'COUNT' => 'int',
		'COST' => 'float',
		'CODE' => 'int',
		'MY_LASER' => 'bool'
	];

	protected $fillable = [
		'W',
		'T',
		'NAME',
		'KR_NAME',
		'COUNT',
		'COST',
		'TYPENAME',
		'TYPEID',
		'UNIT',
		'CODE',
		'MY_1C_NOM',
		'MY_1C_HAR',
		'MY_LASER',
		'MY_TYPE'
	];

	public function build_catalog_item()
	{
		if ($this->pic){
            $image = $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return "<item code='$this->CODE' name='$this->NAME' unit='$this->UNIT' price='$this->COST' thickness='$this->T' width='$this->W' count='$this->COUNT' image='$image' />";
	}

	public function to_json()
	{
		if ($this->pic){
            $image = '/images/pic/'. $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return [
        	'code' => $this->CODE,
        	'name' => $this->NAME,
        	'unit' => $this->UNIT,
        	'price' => round($this->COST,2),
        	'thickness' => $this->T,
        	'width' => $this->W,
        	'count' => $this->COUNT,
        	'image' => $image,
        ];
	}

	public function pic()
	{
		return $this->hasOne(BandPic::class, 'BAND_ID');
	}
}
