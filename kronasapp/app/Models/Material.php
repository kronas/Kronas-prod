<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Material
 *
 * @property int $MATERIAL_ID
 * @property float $L
 * @property float $W
 * @property float $T
 * @property int $COUNT
 * @property float $COST
 * @property string $TYPENAME
 * @property string $UNIT
 * @property string $NAME
 * @property string $TYPEID
 * @property int $CODE
 * @property bool $ST
 * @property string $MY_1C_NOM
 * @property string $MY_1C_HAR
 *
 * @property MaterialPic $pic
 *
 * @package App\Models
 */
class Material extends Model
{
	protected $table = 'MATERIAL';
	protected $primaryKey = 'MATERIAL_ID';
	public $timestamps = false;

	protected $casts = [
		'L' => 'float',
		'W' => 'float',
		'T' => 'float',
		'COUNT' => 'int',
		'COST' => 'float',
		'CODE' => 'int',
		'ST' => 'bool',
        'MATERIAL_ID' => 'int'
	];

	protected $fillable = [
		'L',
		'W',
		'T',
		'COUNT',
		'COST',
		'TYPENAME',
		'UNIT',
		'NAME',
		'TYPEID',
		'CODE',
		'ST',
		'MY_1C_NOM',
		'MY_1C_HAR',
        'MATERIAL_ID'
	];

	public function build_catalog_item()
	{
		if ($this->pic){
            $image = $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return "<item code='$this->CODE' name='$this->NAME' unit='$this->UNIT' price='$this->COST' thickness='$this->T' width='$this->W' length='$this->L' count='$this->COUNT' image='$image' />";
	}

	public function to_json()
	{
		if ($this->pic){
            $image = '/images/pic/'. $this->pic['FILE_NAME_BIG'];
        } else {
            $image = '/images/no-image.png';
        }

        return [
        	'code' => $this->CODE,
        	'name' => $this->NAME,
        	'unit' => $this->UNIT,
        	'price' => round($this->COST,2),
        	'thickness' => $this->T,
        	'width' => $this->W,
        	'length' => $this->L,
        	'count' => $this->COUNT,
        	'image' => $image,
        	'st' => $this->ST
        ];
	}

	public function pic()
	{
		return $this->hasOne(MaterialPic::class, 'MATERIAL_ID');
	}
    public function tolsh()
    {
        return $this->hasMany(Manufacturer::class, 'MATERIAL_ID');
    }
}
