<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BANDSTOCK
 * 
 * @property int $BAND_ID
 * @property int $PLACE_ID
 * @property float $COUNT
 * 
 * @property BAND $band
 * @property PLACE $place
 *
 * @package App\Models
 */
class BandStock extends Model
{
	protected $table = 'BAND_STOCK';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BAND_ID' => 'int',
		'PLACE_ID' => 'int',
		'COUNT' => 'float'
	];

	protected $fillable = [
		'BAND_ID',
		'PLACE_ID',
		'COUNT'
	];

	public function band()
	{
		return $this->belongsTo(Band::class, 'BAND_ID');
	}

	public function place()
	{
		return $this->belongsTo(Place::class, 'PLACE_ID');
	}
}
