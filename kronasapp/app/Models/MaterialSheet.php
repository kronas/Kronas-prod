<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MaterialSheet
 * 
 * @property int $MATERIAL_ID
 * @property float $L
 * @property float $W
 * @property int $COUNT
 * @property int $CODE
 * @property string $MY_1C_NOM
 * @property string $MY_1C_HAR
 *
 * @package App\Models
 */
class MaterialSheet extends Model
{
	protected $table = 'MATERIAL_SHEET';
	protected $primaryKey = 'MATERIAL_ID';
	public $timestamps = false;

	protected $casts = [
		'L' => 'float',
		'W' => 'float',
		'T' => 'float',
		'COUNT' => 'int',
		'CODE' => 'int',
        'PLACE' => 'int'
	];

	protected $fillable = [
		'L',
		'W',
		'COUNT',
		'CODE',
        'PLACE' => 'int',
		'MY_1C_NOM',
		'MY_1C_HAR'
	];

	public function material()
	{
		return $this->belongsTo(Material::class, 'MATERIAL_ID');
	}
    public function place()
    {
        return $this->belongsTo(Place::class, 'PLACE');
    }
}
