<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
/**
 * Class ProjectIn
 *
 * @property int $PROJECT_ID
 * @property string $status
 * @property string $DATE
 * @property int $ORDER1C
 * @property string $MY_PROJECT_OUT
 * @property string $comment
 *
 * @package App\Models
 */
class Project extends Model
{
	protected $table = 'PROJECT';
	protected $primaryKey = 'PROJECT_ID';
	public $timestamps = false;

	protected $casts = [
		'PROJECT_ID' => 'int',
		'status' => 'varchar',
		'ORDER1C' => 'int',
		'MY_PROJECT_OUT' => 'mediumtext',
		'comment' => 'varchar',
		'place' => 'int',
		'user_type_edge' => 'varchar',
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'PROJECT_ID',
		'status',
		'DATE',
		'ORDER1C',
		'MY_PROJECT_OUT',
		'comment',
		'place',
		'user_type_edge',
        'manager_id'
	];

//	public function place()
//	{
//		return $this->belongsTo(Place::class, 'place');
//	}
}
