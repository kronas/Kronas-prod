<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\XncTemplate;

/**
 * Class XncTemplateFolder
 * 
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property bool $visible
 *
 * @package App\Models
 */
class XncTemplateFolder extends Model
{
	protected $table = 'xnc_template_folder';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'parent_id' => 'int',
		'visible' => 'bool'
	];

	protected $fillable = [
		'id',
		'name',
		'parent_id',
		'visible'
	];

	public function get_child_folders()
	{
        return XncTemplateFolder::where('parent_id', $this->id)->where('visible', true)->get();
	}

	public function get_templates()
	{
        return XncTemplate::where('folder_id', $this->id)->get();
	}

	public function get_path()
	{
		$path = [$this];

		if ($this->parent_id){
			$path[] = XncTemplateFolder::find($this->parent_id);
		}

        return array_reverse($path);
	}
}
