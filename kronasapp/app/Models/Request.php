<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Request
 * 
 * @property int $id
 * @property string $headers
 * @property string $body
 * @property Carbon $date
 * @property string $name
 * @property string $action
 * @property int $sheet_complete
 * @property string $error
 * 
 * @property SHEETCOMPLETE $s_h_e_e_t_c_o_m_p_l_e_t_e
 *
 * @package App\Models
 */
class Request extends Model
{
	protected $table = 'requests';
	public $timestamps = false;

	protected $casts = [
		'sheet_complete' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'headers',
		'body',
		'date',
		'name',
		'action',
		'sheet_complete',
		'error'
	];

	// public function s_h_e_e_t_c_o_m_p_l_e_t_e()
	// {
	// 	return $this->belongsTo(SHEETCOMPLETE::class, 'sheet_complete');
	// }
}
