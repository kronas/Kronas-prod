<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectIn
 * 
 * @property int $id
 * @property Carbon $date
 * @property string $file
 * @property int $client
 * @property int $order1c
 * @property int $orderweb
 * @property int $place
 * @property bool $db_in
 * 
 * @property Place $place
 *
 * @package App\Models
 */
class ProjectIn extends Model
{
	protected $table = 'PROJECT_IN';
	public $timestamps = false;

	protected $casts = [
		'client' => 'int',
		'order1c' => 'int',
		'orderweb' => 'int',
		'place' => 'int',
		'db_in' => 'bool'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'date',
		'file',
		'client',
		'order1c',
		'orderweb',
		'place',
		'db_in'
	];

	public function place()
	{
		return $this->belongsTo(Place::class, 'place');
	}
}
