<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| +++ ---
*/

use App\Http\Controllers\IndexController;
use App\Models\Band;
use App\Models\BandStock;
use App\Models\Material;
use App\Models\MaterialSheet;
use App\Models\MaterialStock;
use App\Models\Manufacturer;
use App\Models\Goods;
use App\Models\Clients;
use App\Models\ClientCodes;
use App\Models\TreeMatrial;
use App\Models\TreeBands;
use App\Models\ClientMaterial;
use App\Models\Orders;
use App\Models\Managers;
use App\Models\Project;
use App\Models\PicDrafts;
use App\Models\OrdersGoods;
use App\Models\Service;
use App\Models\Simple;
use App\Models\Parts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use Carbon\Carbon;

Artisan::call('view:clear');
Artisan::call('cache:clear');
Artisan::call('route:clear');

Route::match(['get', 'post'], '/', 'IndexController@index');
Route::match(['get', 'post'], '/projects/{project_id}', 'IndexController@index');
Route::match(['get', 'post'], '/projects_new/{project_id}', 'IndexController@index');
Route::match(['get', 'post'], '/send_xml', 'GiblabController@send_xml');
///Route::match(['get', 'post'], '/mpr_import_action', 'GiblabController@send_mpr');
Route::get('/auth', function () {
    //.. Проверяем, есть ли авторизация
    if (Request::input('checkautorized')) {
        if (Request::input('checkautorized') == 1) {
            $uri = 'login.php';
        }
        if (Request::input('checkautorized') == 2) {
            $uri = 'login.php';
        }
        if (Request::input('checkautorized') == 3) {
            $uri = 'nelpdesc/login.php';
        }
        if(session()->get('saveAutorization')) {
            return redirect(config('app.app_main_dir').'/'.$uri.'?session_id='.session()->get('saveAutorization'));
        }else return redirect(config('app.app_main_dir').'/'.$uri.'?session_id=""');
    }
    if(Request::input('logout') == 'true') {
        session()->pull('isAdmin');
        session()->pull('manager_name');
        session()->pull('manager_id');
        //..28.12.2020
        session()->pull('user');
        return redirect('/');
    }
    $name = Request::input('login');
    $password = Request::input('password');
    $user = DB::table('manager')->where(['e-mail' => $name, 'password' => $password])->first();

    //.. 07.11.2022 Тоценко Логирование авторизации
    // #################################################################################################################
    $logPath = str_replace('\\', '/', config('app.serv_main_dir')) . '/log/login/manager/laravel/';
    $userLogStr = "Entered email -> " . $name . "\r\nEntered password -> " . $password . "\r\n";

    if(isset($user->id)) {
        $roleLog = $user->admin == 1 ? 'admin' : 'manager';
        $userLogStr .= "Manager ID -> " . $user->id . "\r\nManager name -> " . $user->name . "\r\nManager phone -> " . $user->phone . "\r\nManager mail -> " . $user->{'e-mail'} . "\r\nManager role -> " . $roleLog . "\r\n";
    }

    file_put_contents($logPath . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . "\r\n" . $userLogStr . "\r\n\r\n", FILE_APPEND);
    // #################################################################################################################

    if(isset($user->id)) {
        session(['isAdmin' => $user->admin]);
        session(['manager_name' => $user->name]);
        session(['manager_id' => $user->id]);
        session(['user_place' => $user->place]);
        //.. Массив для поддержки авторизации create.php
        $role = $user->admin == 1 ? 'admin' : 'manager';
        $user_data_for_RS_auth = array(
            'ID' => $user->id,
            'name' => $user->name,
            'phone' => $user->phone,
            'mail' => $user->{'e-mail'},
            'access' => $user->access,
            'role' => $role,
            'code' => $user->code,
        );
        session(['user' => $user_data_for_RS_auth]);
        //..07.12.2020 Сохраняем сессию юзера при входе в систему во временный файл
        session(['saveAutorization' => session()->get('data_id')]);
        Storage::put('giblab_sessions/'.session()->get('data_id').'.txt', serialize(session()->all()));
        return json_encode([
            'info' => 'Привет, '.$user->name,
            'is_manager' => $user->id,
        ]);
    } else {
        return json_encode(['info' => 'Данные авторизации не верны']);
    }
});
Route::get('/auth_client', function () {
    if(Request::input('logout') == 'true') {
        session()->pull('client_fio');
        session()->pull('client_phone');
        session()->pull('client_email');
        session()->pull('client_code');
        session()->pull('client_id');
        session()->pull('user');

        //.. Идём в Раздвижныые Системы и удаляем там сессию авторизации
        return redirect(config('app.app_main_dir').'/clients/login.php?giblabClientLogin=GLlogout');
        // return redirect('/');
    }
    $login = Request::input('login_client');
    $phone = '+'.Request::input('telephone_client');
    $password = Request::input('password_client');
    if(($login && $login != '' && $login != 'undefined') && ($password && $password != '' && $password != 'undefined')) {
        $user = DB::table('client')->where(['e-mail' => $login, 'pass' => md5($password)])->first();
        if((!isset($user->client_id)) && ($phone && $phone != '' && $phone != 'undefined')) {
            $user = DB::table('client')->where(['tel' => $phone, 'pass' => md5($password)])->first();
        }
    } else if(($phone && $phone != '' && $phone != 'undefined')) {

        $user = DB::table('client')->where(['tel' => $phone])->first();

        //.. 07.11.2022 Тоценко Логирование авторизации
        // #################################################################################################################
        $logPath = str_replace('\\', '/', config('app.serv_main_dir')) . '/log/login/client/';
        $userLogStr = "Entered phone -> " . $phone . "\r\n";

        if(isset($user->client_id)) {
            $userLogStr .= "Client ID -> " . $user->client_id . "\r\nClient name -> " . $user->name . "\r\nClient phone -> " . $user->tel . "\r\nClient mail -> " . $user->{'e-mail'} . "\r\n";
        }

        file_put_contents($logPath . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . "\r\n" . $userLogStr . "\r\n\r\n", FILE_APPEND);
        // #################################################################################################################

        if(isset($user->client_id)) {
            session(['client_phone' => $user->tel]);
            session(['client_email' => $user->{'e-mail'}]);
            session(['client_code' => $user->{'code'}]);
            $contragent = DB::table('client_code')->where(['code_ac' => $user->{'code'}])->first();
            session(['client_fio' => $contragent->name]);
            session(['client_id' => $user->client_id]);
            session(['client_password' => $user->pass]);

            //.. Массив для поддержки авторизации create.php
            $user_data_for_RS_auth = array(
                'ID' => $user->client_id,
                'name' => $user->name,
                'phone' => $user->tel,
                'mail' => $user->{'e-mail'},
                'role' => 'client',
                'code' => $user->code,
            );
            session(['user' => $user_data_for_RS_auth]);
            // return 'Привет, '.$user->name;
            //.. Идём в Раздвижныые Системы и создаём там сессию авторизации
            return 'giblabClientLogin='.$user->client_id;
            // return redirect(config('app.app_main_dir').'/clients/login.php?giblabClientLogin='.$user->client_id);
        } else {
            return 'Данные авторизации не верны';
        }
    } else {
        return 'Поля заполнены некорректно!';
    }

    if(isset($user->client_id)) { 
        session(['client_phone' => $user->tel]);
        session(['client_email' => $user->{'e-mail'}]);
        session(['client_code' => $user->{'code'}]);
        $contragent = DB::table('client_code')->where(['code_ac' => $user->{'code'}])->first();
        session(['client_fio' => $contragent->name]);
        session(['client_id' => $user->client_id]);
        session(['client_password' => $user->pass]);

        //.. Массив для поддержки авторизации create.php
        $user_data_for_RS_auth = array(
            'ID' => $user->client_id,
            'name' => $user->name,
            'phone' => $user->tel,
            'mail' => $user->{'e-mail'},
            'role' => 'client',
            'code' => $user->code,
        );
        session(['user' => $user_data_for_RS_auth]);
        // return 'Привет, '.$user->name;
        //.. Идём в Раздвижныые Системы и создаём там сессию авторизации
        return 'giblabClientLogin='.$user->client_id;
        // return redirect(config('app.app_main_dir').'/clients/login.php?giblabClientLogin='.$user->client_id);
    } else {
//        $email_new_client = '
//            <p>Уважаемый менеджер!</p>
//            <p>Клиент желает зарегистрироваться на нашем сервисе</p>
//            <p>Контактные данные клиента:</p>
//            <p>почта: '.$login.'</p>
//            <p>тел: '.$phone.'</p>
//        ';
//        Mail::send([],[], function($message) use ($email_new_client)
//        {
//            $message->from('Demchuk.Yuriy@kronas.com.ua', 'Kronas');
//            $message->to('Demchuk.Yuriy@kronas.com.ua', 'Kronas');
//            $message->subject('Новый клиент');
//            $message->setBody($email_new_client, 'text/html');
//        });
        return 'Данные авторизации не верны';
    }
});


Route::get('/auth_client_check', function() {
    $phone = '+'.Request::input('phone');
    $client = Clients::where(['tel' => $phone])->first();
    if(isset($client->client_id)) {
        return 1;
    } else {

        //.. 30.10.2022 Тоценко
        // Добавлено создание клиента на лету (если клиент уже был создан ранее в Акценте)
        // До правок здесь, в else был только return 0;

        $searchClientPhone = preg_replace('#(\+38)|\(|\)|-#', '', $phone);
        $endpoint = "http://v-webapp01.dn-kronas.local/kronasapi/gibLabService/FindPhoneFromAgents/$searchClientPhone/1";

        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $endpoint);

        $statusCode = $response->getStatusCode();
        $content = json_decode($response->getBody(), true);

        if (200 == $statusCode && !empty($content['data'])) {

            $result = $content['data'][0];

            $client = new Clients([
                'name' => $result['ClientName'],
                'tel' => $result['ClientPhone'],
                'e-mail' => $result['ClientEmail'],
                'code' => $result['ClientId']
            ]);
            $client->save();

            $clientCode = ClientCodes::where(['code_ac' => $client->code])->first();
            if(!isset($clientCode->idcl)) {
                $clientCode = new ClientCodes([
                    'code_ac' => $result['ClientId'],
                    'name' => $result['ClientName']
                ]);
                $clientCode->save();
            }

            $client = Clients::where(['tel' => $phone])->first();
            if(isset($client->client_id)) return 1;
            else return 0;
        }

        return 0;
    }
});


Route::get('/auth_get_password', function () {

    $email = Request::input('getter_email');
    $phone = '+'.Request::input('getter_phone');

    if( $email && $email != '' && $email != 'undefined' ) {
        $user = DB::table('client')->where(['e-mail' => $email])->first();
        $contact = 'email';
    } else if($phone && $phone != '' && $phone != 'undefined') {
        $user = DB::table('client')->where(['tel' => $phone])->first();
        $contact = 'phone';
    } else {
        return 'Поля заполнены некорректно!';
    }

    if(isset($user->client_id)) {
        $new_password = mt_rand(123456, 999999);
        DB::table('client')
            ->where('client_id', $user->client_id)
            ->update(['pass' => md5($new_password)]);

        if($contact == 'email') {
            $email_password = '
                            <p>Добрый день!</p>
                            <p>Ваш новый пароль: <b>'.$new_password.'</b> </p>
                            ';
            Mail::send([],[], function($message) use ($email,$email_password)
            {
                $message->from('mihaylenko.nataliya@kronas.com.ua', 'Kronas');
                $message->to($email);
                $message->subject('Ваш новый пароль');
                $message->setBody($email_password, 'text/html');
            });

            return 'Новый пароль был отправлен на указанный e-mail.';
        }
        if($contact == 'phone') {
            $xml = '<?xml version="1.0" encoding="utf-8" ?><package key="3ae50516fed179f981adf58f1defa7e557de189e"><message><msg recipient="'.$phone.'" sender="KRONAS" type="0">Ваш новый пароль: '.$new_password.'</msg></message></package>';
            $client = new Client();
            $r = $client->post('https://alphasms.ua/api/xml.php', $options = [
                'headers' => [
                    'Content-Type' => 'text/xml; charset=UTF8',
                ],
                'body' => $xml,
            ]);
            ///$r->getBody()->getContents()
            return 'Новый пароль был отправлен на указанный номер телефона.'; ///.$r->getBody()->getContents()
        }

    } else {



        $email_new_client = '
            <p>Уважаемый менеджер!</p>
            <p>Клиент желает зарегистрироваться на нашем сервисе</p>
            <p>Контактные данные клиента:</p>
            <p>тел: '.$phone.'</p>
            <p>почта: '.$email.'</p>
        ';
        Mail::send([],[], function($message) use ($email,$email_new_client)
        {
            $message->from('mihaylenko.nataliya@kronas.com.ua', 'Kronas');
            $message->to('totsenko.anton@kronas.com.ua', 'Kronas');
            $message->subject('Новый клиент с сервиса');
            $message->setBody($email_new_client, 'text/html');
        });
        return 'Данный е-мейл или телефон отсутствует в базе. Наш менеджер свяжется с Вами в ближайшее время';

    }
});




Route::get('/send_client_pin_code', function () {
    $new_password = mt_rand(1234, 9999);
    $phone = '+'.Request::input('client');
    $xml = '<?xml version="1.0" encoding="utf-8" ?><package key="3ae50516fed179f981adf58f1defa7e557de189e"><message><msg recipient="'.$phone.'" sender="KRONAS" type="0">ПИН-код для подтверждения авторизации: '.$new_password.'</msg></message></package>';
    $client = new Client();
    $r = $client->post('https://alphasms.ua/api/xml.php', $options = [
        'headers' => [
            'Content-Type' => 'text/xml; charset=UTF8',
        ],
        'body' => $xml,
    ]);
//    return $r->getBody()->getContents();
    return $new_password * pow(3, 15); ///.$r->getBody()->getContents()
});




Route::post('/send_project', function () {

    session_start();

    if( !session()->get('clear_project_data') || session()->get('clear_project_data') == '' ) {
        session(['clear_project_data' => Request::input('project_data')]);
    }
    session(['project_data' => Request::input('project_data')]);

    $session_id = session_id();

    $post_data = [
        '_token' =>  Request::input('_token'),
        'project_data' =>  Request::input('project_data'),
        'user_place' =>  Request::input('user_place'),
        'user_type_edge' =>  Request::input('user_type_edge'),
        'manager_id' => session('manager_id'),
        'manager_name' => session('manager_name'),
        'order_comment' => session('order_comment'),
        'session_id' => $session_id,
        'xnc_in' => session('mpr_project'),
        'order_status' => session('order_status'),
        'order_1c_project' => session('order_1c_project'),
        'simple_price_extra' => session('simple_price_extra'),
        'client_fio' => session('client_fio'),
        'client_phone' => session('client_phone'),
        'client_email' => session('client_email'),
        'client_code' => session('client_code'),
        'client_id' => session('client_id'),
        'order_akcent_ids' => session('order_akcent_ids'),
        'order_manager_id' => session('order_manager_id'),
        'order_status_data' => session('order_status_data'),
        'additional_request_uri' => session('additional_request_uri'),
        'tec_info' => session('tec_info'),
    ];


    Storage::put('giblab_sessions/'.$session_id.'.txt', serialize($post_data));
    chmod(config('app.serv_main_dir').'/kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt', 0777);
    session_destroy();

    return $session_id;

});

Route::post('/comment', function () {
    $comment = Request::input('comment');
    session(['order_comment' => $comment]);
    return redirect('/');
});

Route::post('/project_in_session', function(Request $request) {
    $project_data = Request::input('project_data');
    $user_place = Request::input('user_place');
    $user_type_edge = Request::input('user_type_edge');

    if($user_place && $user_type_edge && session('order_project_id')) {
        try{
            ///$project_update = Project::where(['PROJECT_ID' => session('order_project_id')])->first();
            $project_update = Project::find(session('order_project_id'));
            $project_update->place = $user_place;
            $project_update->user_type_edge = $user_type_edge;
            $project_update->save();
         }
         catch(\Exception $e){
            // do task when error
            echo $e->getMessage();   // insert query
         }
    }
    

    session(['project_data' => $project_data]);
    session(['user_place' => $user_place]);
    session(['user_type_edge' => $user_type_edge]);

    return 'Project save in session';
});
Route::get('/clear_project', function() {

    session_start();

    //.. 06.09.2022 Тоценко А. Параметр, есть ли раскрой при загрузке .kronas файла
    if (session()->get('user_sheet_cutting')) {
        session()->forget('user_sheet_cutting');
    }

    $session_id = session('data_id');
    if(!$session_id) {
        session_regenerate_id();
        session(['data_id' => session_id()]);
        $session_id = session('data_id');
    }

    session(['project_data' => '']);
    session(['clear_project_data' => '']);
    session(['checked_project_data' => '']);
    session(['back_to_start' => false]);
    session()->pull('order_status');
    session()->pull('order_1c_project');
    session()->pull('user_type_edge');
    session()->pull('user_place');
    session()->pull('order_comment');
    session()->pull('session_id');
    session()->pull('order_status');
    session()->pull('order_1c_project');

    Storage::put('giblab_sessions/'.$session_id.'.txt', '');

    // 01.02.2020 В файле проекта также сбрасываем данные проекта
    if (session()->get('order_project_id')) {
        $MPO = base64_encode("<project name=\"New\" currency=\"грн\" version=\"1\" costOperation=\"0\" costMaterial=\"0\" cost=\"0\" ><good typeId=\"product\" count=\"1\" costMaterial=\"0\" costOperation=\"0\" cost=\"0\" name=\"product_new\" id=\"2\"/></project>");
        // и сохраняем в файл
        $dirdig = ceil(session()->get('order_project_id') / 1000) - 1;
        if (!file_exists(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/')) {
            mkdir(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig, 0777);
        }
        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/' . session()->get('order_project_id') . '.txt', $MPO);

        //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
        if (!file_exists(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig . '/')) {
            mkdir(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig, 0777);
        }
        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig . '/' . session()->get('order_project_id') . '.txt', $MPO);
    }


    return redirect()->back();
});
Route::get('/new_project_from_account', function() {

    ///session_start();

    $session_id = session('data_id');

    session(['project_data' => '']);
    session(['clear_project_data' => '']);
    session(['checked_project_data' => '']);
    session(['back_to_start' => false]);
    session()->pull('order_status');
    session()->pull('order_1c_project');
    session()->pull('user_type_edge');
    session()->pull('user_place');
    session()->pull('order_comment');
    session()->pull('session_id');
    session()->pull('order_status');
    session()->pull('order_1c_project');

    Storage::put('giblab_sessions/'.$session_id.'.txt', '');

    return redirect('/');
});
Route::get('/step_1', function(Request $request) {
    $data_id = Request::input('file');
    $data = unserialize(Storage::get('giblab_sessions/'.$data_id.'.txt'));
    // return dd($data);
    ///dd($data['project_data']);
    /*session(['project_data' => str_replace('<xml version="1.0" encoding="UTF-8">', '', htmlspecialchars_decode($data['project_data']))]); */
    session(['project_data' => str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $data['project_data'])]);
    session(['checked_project_data' => str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $data['project_data'])]);
    if(isset($data['project_data'])) session(['checked_project_data' => $data['project_data']]);
    if(isset($data['order_status'])) session(['order_status' => $data['order_status']]);
    if(isset($data['order_1c_project'])) session(['order_1c_project' => $data['order_1c_project']]);
    if(isset($data['user_type_edge'])) session(['user_type_edge' => $data['user_type_edge']]);
    if(isset($data['user_place'])) session(['user_place' => $data['user_place']]);
    if(isset($data['session_id'])) session(['session_id' => $data['session_id']]);
    if(isset($data['simple_price_extra'])) session(['simple_price_extra' => $data['simple_price_extra']]);
    if(isset($data['client_fio'])) session(['client_fio' => $data['client_fio']]);
    if(isset($data['client_phone'])) session(['client_phone' => $data['client_phone']]);
    if(isset($data['client_email'])) session(['client_email' => $data['client_email']]);
    if(isset($data['tec_info'])) session(['tec_info' => $data['tec_info']]);
    ///session(['clear_project_data' => session()->get('project_data')]);
    session(['back_to_start' => true]);
    if (isset($data['additional_request_uri'])) {
        return redirect($data["additional_request_uri"]);
    } else {
        return redirect('/');
    }
});
Route::get('/check_session', function() {
    $data = session()->all();
    return dd($data);
});
Route::get('/back_to_start', function() {
    session(['project_data' => session()->get('clear_project_data')]);
    return redirect('/');
});
Route::post('/project_in_session_checked', function() {
    $user_place = Request::input('user_place');
    $user_type_edge = Request::input('user_type_edge');
    $project_data = Request::input('project_data_checked');
    session(['checked_project_data' => $project_data]);
    session(['user_place' => $user_place]);
    session(['user_type_edge' => $user_type_edge]);
    return 'Checked project save in session';
});
Route::get('/save_data_id', function() {
    $data_id = Request::input('data_id');
    session(['data_id' => $data_id]);
});

// Транзитный роут для передачи запроса скрипту index_anton и передача ответа в gibform.js
Route::get('/check_project', function () {
    ini_set('max_execution_time', '600');

    $url = config('app.app_main_dir')."/index_anton.php?data_id=" . Request::input('data_id');
    $a = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, $a);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
});

// Транзитный роут для передачи запроса скрипту con_material.php и передача ответа в main.js
Route::post('/con_material', function () {
    $url  = config('app.app_main_dir')."/con_material.php";
    $a    = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36';
    $data['project_data'] = Request::input('project_data');
    $data['place'] = Request::input('place');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_USERAGENT, $a);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
});


Route::post('/user_save_project', function() {
    //..17.02.2021 В формате .kronas сохраняется вся сессия. Там достаточно сохранять project_data, user_place, user_type_edge
    // $data['checked_project_data'] = session()->get('checked_project_data');
    // $data['data_id'] = session()->get('data_id');
    // $data['order_comment'] = session()->get('order_comment');
    $data['project_data'] = session()->get('project_data');
    $data['user_place'] = Request::input('user_place');
    $data['user_type_edge'] = Request::input('user_type_edge');


    $encode_data = base64_encode(strrev(serialize($data)));
    $url = config('app.serv_main_dir').'/kronasapp/public';
    $file = '/users-projects/'.time().'.kronas';

    file_put_contents($url.$file, $encode_data);
    return config('app.app_laravel_dir').$file;
});

Route::post('/save_project_file', function () {
    $project_data = Request::input('project_data');
    $url = config('app.serv_main_dir').'/kronasapp/public';
    $file = '/users-projects/'.time().'.project';
    file_put_contents($url.$file, $project_data);
    return config('app.app_laravel_dir').$file;
});

Route::post('/user_upload_project', function(Request $request) {
    $manager_id = session()->get('manager_id');
    $manager_name = session()->get('manager_name');
    $is_admin = session()->get('isAdmin');
    $order_1c_project = session()->get('order_1c_project');
    $innerFile = Request::input('KronasImport');

    if(@unserialize(strrev(base64_decode(base64_decode($innerFile['file']))))) {

        session(unserialize(strrev(base64_decode(base64_decode($innerFile['file'])))));
        session(['manager_id' => $manager_id]);
        session(['manager_name' => $manager_name]);
        session(['isAdmin' => $is_admin]);
        session(['order_1c_project' => $order_1c_project]);
        session()->pull('order_project_id');

        //.. 06.09.2022 Тоценко А. Параметр, есть ли раскрой при загрузке .kronas файла
        if (session()->get('user_sheet_cutting')) {
            session()->forget('user_sheet_cutting');
        }

        if (mb_stripos(session()->get('project_data'), 'data="')) {
            session(['user_sheet_cutting' => 1]);
        }

        printf('Ok');
    } else {
        printf('Error');
    }
});


Route::get('/back_clear_project', function() {
    session()->pull('checked_project_data');
    session(['project_data' => session()->get('clear_project_data')]);
    session(['back_to_start' => false]);
    return redirect('/');
});


Route::post('/check_edits', function() {
    $project_data = Request::input('project');
    if($project_data != session()->get('checked_project_data')) {
        return 0;
    } else {
        return 1;
    }
});

Route::post('/mpr_import_action', function(Request $request) {

    session_start();
    $file = Input::file('mpr_import_field');

    $ticket_id = Request::input('ticket_id');
    $project_data = base64_decode(Request::input('project_data'));
    $is_handlers = Request::input('is_handler');

    if($file->getClientOriginalExtension() == 'mpr') {
        $data = file_get_contents($file);
        $current_timestamp = time();
        $action = 'DFFB69BB';

        Storage::makeDirectory('/data/giblab/');
        // Сюда запишется ответ сервера "$action-$current_timestamp.zip($action-$current_timestamp.project)"
        $zipfile = storage_path("app/data/giblab/$action-$current_timestamp.zip");

        $client = new Client(['base_uri' => 'https://service.giblab.com', 'verify' => false]);
        $r = $client->post('/', [
            'headers'  => [
                'Content-Type' => 'application/octet-stream; charset=UTF-8',
                'Content-Length' => strlen($data),
                'fileName' => $file->getClientOriginalName(),
                'action' => $action
            ],
            'body' => $data,
            'sink' => $zipfile
        ]);

        if(!file_exists(config('app.serv_main_dir').'/helpdesc/uploads/'.$ticket_id.'/mpr')) {
             mkdir(config('app.serv_main_dir').'/helpdesc/uploads/'.$ticket_id.'/mpr', 0777, true);
        }
        move_uploaded_file($_FILES['mpr_import_field']['tmp_name'], config('app.serv_main_dir').'/helpdesc/uploads/'.$ticket_id.'/mpr/'.basename($_FILES['mpr_import_field']['name']));

        $zip = Zip::open($zipfile);
        $project_from_giblab = '';
        $files_from_giblab = $zip->listFiles();
        $result = ['files' => []];

        foreach ($zip->listFiles() as $filename) {
            $zip->extract(storage_path('app/data/giblab/'), $filename);
            $old_path = "data/giblab/$filename";

            $path = strtolower("data/giblab/$action-$current_timestamp-$filename");
            $path = str_replace(' ', '-', $path);

            Storage::move($old_path, $path);
            copy(strtolower(config('app.serv_main_dir').'/kronasapp/storage/app/data/giblab/'.$action.'-'.$current_timestamp.'-'.$filename), config('app.serv_main_dir').'/helpdesc/uploads/'.$ticket_id.'/'.$filename);

            $result['files'][] = storage_path($path);

            $extension = pathinfo($path, PATHINFO_EXTENSION);
            if ($extension == 'project'){
                $project_from_giblab = Storage::get($path);

                $result['project'] = $project_from_giblab;
            }
        };

        session(['xnc_in' => $result['project']]);
        session(['mpr' => 'MPR успешно импортирован']);

        $session_id = session_id();
        if(!file_exists(config('app.serv_main_dir').'kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt') && $is_handlers) {
            Storage::put('giblab_sessions/'.$session_id.'.txt', serialize(['project_data' => $project_data]));
        }
        $data = unserialize(Storage::get('giblab_sessions/'.$session_id.'.txt'));
        $data['xnc_in'] = session('xnc_in');
        
        Storage::put('giblab_sessions/'.$session_id.'.txt', serialize($data));
        chmod(config('app.serv_main_dir').'/kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt', 0777);
        
        return redirect()->away(config('app.app_main_dir').'/step_import_mpr.php?data_id='.$session_id.'&is_handlers='.$is_handlers.'&ticket_id='.$ticket_id);
        return redirect('/');    

    } else if($file->getClientOriginalExtension() == 'zip') {

        $check_zip = Zip::open($file);
        $ticket_path = config('app.serv_main_dir').'/helpdesc/uploads/'.$ticket_id.'/mpr';

        foreach ($check_zip->listFiles() as $filename) {
            $check_name_array = explode( '.', $filename );
            $check_extension = end($check_name_array );

            if($check_extension != 'mpr') {
                session(['mpr_error' => 'В архиве присутствуют не .mpr файлы']);
                return redirect('/');
            };
        }

        if($is_handlers) {
            if (!file_exists($ticket_path)) mkdir($ticket_path, 0777, true);
            foreach ($check_zip->listFiles() as $filename) {
                $check_zip->extract($ticket_path.'/', $filename);
            }
        }

        $data = file_get_contents($file);
        $current_timestamp = time();
        $action = 'DFFB69BB';

        Storage::makeDirectory('/data/giblab/');
        $zipfile = storage_path("app/data/giblab/sssssss$action-$current_timestamp.zip");

        $client = new Client(['base_uri' => 'https://service.giblab.com']);
        $r = $client->post('/', [
            'headers'  => [
                'Content-Type' => 'application/octet-stream; charset=UTF-8',
                'Content-Length' => strlen($data),
                'fileName' => 'reqData_'.$current_timestamp.'_'.$action.'.zip',
                'action' => $action
            ],
            'body' => $data,
            'sink' => $zipfile
        ]);

        $zip = Zip::open($zipfile);
        $project_from_giblab = '';
        $files_from_giblab = $zip->listFiles();
        $result = ['files' => []];

        foreach ($zip->listFiles() as $filename) {
            $zip->extract(storage_path('app/data/giblab/'), $filename);
            $old_path = "data/giblab/$filename";

            $path = strtolower("data/giblab/$action-$current_timestamp-$filename");
            $path = str_replace(' ', '-', $path);

            Storage::move($old_path, $path);
            $result['files'][] = storage_path($path);

            $extension = pathinfo($path, PATHINFO_EXTENSION);
            if ($extension == 'project'){
                $project_from_giblab = Storage::get($path);

                $result['project'] = $project_from_giblab;
            }
        };

        session(['xnc_in' => $result['project']]);
        session(['mpr' => 'MPR успешно импортирован']);
        $session_id = session_id();
     
        if(!file_exists(config('app.serv_main_dir').'kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt') && $is_handlers) {
            echo '11111';
            Storage::put('giblab_sessions/'.$session_id.'.txt', serialize(['project_data' => $project_data]));
        }
        $data = unserialize(Storage::get('giblab_sessions/'.$session_id.'.txt'));
        $data['xnc_in'] = session('xnc_in');
        Storage::put('giblab_sessions/'.$session_id.'.txt', serialize($data));
        chmod(config('app.serv_main_dir').'/kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt', 0777);
        return redirect()->away(config('app.app_main_dir').'/step_import_mpr.php?data_id='.$session_id.'&is_handlers='.$is_handlers.'&ticket_id='.$ticket_id);
        return redirect('/');

    } else {
        session(['mpr_error' => 'Неверный формат файла. Допустимо импортировать каждый файл MPR по одному или несколько в архиве ZIP.']);
        return redirect('/');
    };

    $result = ['files' => []];
    return redirect('/');
});




//Route::post('/mpr_import_action', function(Request $request) {
//
//    $file = Input::file('mpr_import_field');
//
//    if($file->getClientOriginalExtension() == 'mpr') {
//
//        $data = file_get_contents($file);
//
//        $data_bin = pack("A*", $data);
//
//        return $data_bin;
//
//    } else if($file->getClientOriginalExtension() == 'zip') {
//
//        echo 'This is ZIP file';
//        $zip = Zip::open($file);
//        foreach ($zip->listFiles() as $filename) {
//
//            $name_array = explode( '.', $filename );
//            $extension = end($name_array );
//
//            if($extension != 'mpr') {
//                return 'Not MPR files in ZIP';
//            };
//        }
//
//    } else {
//        return 'Wrong file format';
//    };
//
//
//
//
//    $result = [
//        'files' => []
//    ];
//
//
//
//    return redirect('/');
//});

Route::get('/get_new_catalog', function (Request $request) {

    $category = Request::input('category');

    ///return var_dump($massive);

    function get_materials($items){
        $_items = [];

        foreach ($items as $item) {
            $_items[] = $item->to_json();
        }

        return $_items;
    }

    if($category == 'bands') {
        return json_encode(get_materials(Band::all()));
    } else if($category == 'plate_materials') {
        return json_encode(get_materials(Material::where(['ST' => false,])->get()));
    } else if($category == 'tabletops') {
        return json_encode(get_materials(Material::where(['ST' => 1,])->get()));
    }


});


Route::get('/get_new_catalog_search', function (Request $request) {

    $category = Request::input('category');
    $code = Request::input('code');
    $name = Request::input('name');
    $unit = Request::input('unit');
    $length = Request::input('length');
    $width = Request::input('width');
    $thickness = Request::input('thickness');
    $price = Request::input('price');
    $stock = Request::input('stock');

    switch($category) {
        case 'plate_materials':
            $data = Material::orderBy('NAME');
            break;
        case 'tabletops':
            $data = Material::where(['ST' => 1,]);
            break;
        case 'bands':
            $data = Band::orderBy('NAME');
            break;
    }

    $filters = [];
    if($code) {
        $filters[] = ['CODE', 'like', '%'.$code.'%'];
        $data->orWhere('CODE', 'like', '%'.$code.'%');
    }
    if($name) {
        $filters[] = ['NAME', 'like', '%'.$name.'%'];
        $data->orWhere('NAME', 'like', '%'.$name.'%');
    }
    if($unit) {
        $filters[] = ['UNIT', 'like', '%'.$unit.'%'];
        $data->orWhere('UNIT', 'like', '%'.$unit.'%');
    }
    if($length && $category != 'bands') {
        $filters[] = ['L', 'like', '%'.$length.'%'];
        $data->orWhere('L', 'like', '%'.$length.'%');
    }
    if($width) {
        $filters[] = ['W', 'like', '%'.$width.'%'];
        $data->orWhere('W', 'like', '%'.$width.'%');
    }
    if($thickness) {
        $filters[] = ['T', 'like', '%'.$thickness.'%'];
        $data->orWhere('T', 'like', '%'.$thickness.'%');
    }
    if($price) {
        $filters[] = ['COST', 'like', '%'.$price.'%'];
        $data->orWhere('COST', '<=', strval($price));
    }

    $data = $data->get();
//    if($stock) {
//        $filters[] = ['place', 'like', $stock];
//    }

//    var_dump($filters);
    ///return var_dump($massive);

    function get_materials($items){
        $_items = [];

        foreach ($items as $item) {
            $_items[] = $item->to_json();
        }

        return $_items;
    }

    if($category == 'bands') {
        return json_encode(get_materials($data));
    } else if($category == 'plate_materials') {
        return json_encode(get_materials($data));
    } else if($category == 'tabletops') {
        return json_encode(get_materials($data));
    }


});

Route::get('/get_catalog_tree', function() {

    $parent = Request::input('parent');

    $trees = Goods::where('PARENT', $parent)->get();

    return json_encode($trees);

});

Route::get('/get_catalog_filtred', function () {

    $type = Request::input('type');
    $catalog = Request::input('catalog');
    $data = [];

    switch ($type) {
        case 'p_':

            $products_base = TreeMatrial::where(['FOLDER_ID' => $catalog])->get();
            $products = [];
            foreach ($products_base as $item) {
                if($item->material->ST == 0) {
                    $products[] = $item->material->to_json();
                }
            }
            $data = $products;
            break;
        case 't_':
            $products_base = TreeMatrial::where(['FOLDER_ID' => $catalog])->get();
            $products = [];
            foreach ($products_base as $item) {
//                if($item->material->ST == 1) {
                    $products[] = $item->material->to_json();
                ///}
            }
            $data = $products;
            break;
        case 'e_':
            $products_base = TreeBands::where(['FOLDER_ID' => $catalog])->get();
            $products = [];
            foreach ($products_base as $item) {
                $products[] = $item->band->to_json();
            }
            $data = $products;
            break;
    }

    return json_encode($data);
});

Route::get('/get_new_stock', function (Request $request) {
    $code = Request::input('m_id');
    $place_id = 1;
    $bands_stock = [];
        foreach (BandStock::where('PLACE_ID', $place_id)->get() as $item) {
            $bands_stock[] = [
                'id' => $item->BAND_ID,
                'code' => $item->band['CODE'],
                'width' => $item->band['W'],
                'thikness' => $item->band['T'],
                'count' => $item->COUNT,
                'sheet' => false,
            ];
        };

        $materials = Material::where('CODE', $code)->first();
        $materials_stock = [];
        foreach (MaterialStock::where('MATERIAL_ID', $materials->MATERIAL_ID)->get() as $item) {
            $materials_stock[] = [
                'id' => $item->MATERIAL_ID,
                'code' => $item->material['CODE'],
                'width' => $item->material['W'],
                'length' => $item->material['L'],
                'count' => $item->COUNT,
                'place' => $item->place['NAME'],
                'place_id' => $item->place['PLACES_ID'],
                'sheet' => true,
                'isSheet' => false,
                'from_stock' => true
            ];

            if ($item->material['ST'] == 0){
                $materials_stock[] = [
                    'id' => $item->MATERIAL_ID,
                    'code' => $item->material['CODE'],
                    'width' => floatval($item->material['W'])/2-5,
                    'length' => $item->material['L'],
                    'count' => intval($item->COUNT)*2,
                    'place' => $item->material['PLACES_ID'],
                    'sheet' => true,
                    'isSheet' => true,
                    'from_stock' => true
                ];
            };
        };

        foreach (MaterialSheet::where('CODE', $code)->get() as $sheet) {
            $materials_stock[] = [
                'id' => $sheet->MATERIAL_ID,
                'code' => $sheet->CODE,
                'width' => $sheet->W,
                'length' => $sheet->L,
                'count' => $sheet->COUNT,
                'place' => $sheet->place['NAME'],
                'place_id' => $sheet->place['PLACES_ID'],
                'sheet' => true,
                'isSheet' => false,
                'from_stock' => false
            ];
        };

        return json_encode([
                'bands' => $bands_stock,
                'materials' => $materials_stock,
            ]);
});

Route::get('/start_new_porject', function (Request $request) {
    $client_id = Request::input('client_id');
    $client_code = Request::input('client_code');
    $set_new_order = Request::input('set_new_order');
    $manager_id = Request::input('manager_id');
    if(!$manager_id) {
        $manager_id = session('manager_id');
    }
    if(!$manager_id) {
        return redirect(config('app.app_main_dir').'/login.php');
    }
    $manager_name = session('manager_name');
    session()->flush();
    $user = DB::table('manager')->where(['id' => $manager_id])->first();

    session(['isAdmin' => $user->admin]);
    session(['manager_name' => $user->name]);
    session(['manager_id' => $user->id]);
    session(['user_place' => $user->place]);
    session(['new_project_place' => $user->place]);
    if($client_id) {
        session(['client_id' => $client_id]);
        session(['client_code' => $client_code]);
    }
    //..07.12.2020 Массив для поддержки авторизации create.php
    $role = $user->admin == 1 ? 'admin' : 'manager';
    $user_data_for_RS_auth = array(
        'ID' => $user->id,
        'name' => $user->name,
        'phone' => $user->phone,
        'mail' => $user->{'e-mail'},
        'access' => $user->access,
        'role' => $role,
        'code' => $user->code,
    );
    session(['user' => $user_data_for_RS_auth]);

    if($set_new_order == 1) {
        $new_order_data = rand(111111111, 999999999);
        $order = new Orders([
            'CLIENT_ID' => $client_id,
            'manager' => $manager_id,
            'CLIENT' => $client_code,
            'status' => 'черновик',
            'PLACE' => $user->place,
            'DB_AC_IN' => date('Y-m-d H:i:s'),
            'DB_AC_NUM' => 'in_edits_'.$new_order_data,
            'DB_AC_ID' => $new_order_data,
        ]);
        $order->save();

        $project = new Project([
            'status' => 'start',
            'DATE' => date('Y-m-d H:i:s'),
            'ORDER1C' => $order->id,
            'manager_id' => $manager_id,
            //.. В базу пишем пустую строку
            'MY_PROJECT_OUT' => '',
            // 'MY_PROJECT_OUT' => base64_encode("<project name=\"New\" currency=\"грн\" version=\"1\" costOperation=\"0\" costMaterial=\"0\" cost=\"0\" ><good typeId=\"product\" count=\"1\" costMaterial=\"0\" costOperation=\"0\" cost=\"0\" name=\"product_new\" id=\"2\"/></project>"),
            ///'comment' => 'тест'
        ]);
        $project->save();

        //..23.11.2020..запись Здесь создаём переменную с начальными значениями
        $MPO = base64_encode("<project name=\"New\" currency=\"грн\" version=\"1\" costOperation=\"0\" costMaterial=\"0\" cost=\"0\" ><good typeId=\"product\" count=\"1\" costMaterial=\"0\" costOperation=\"0\" cost=\"0\" name=\"product_new\" id=\"2\"/></project>");
        // и сохраняем в файл
        $dirdig = ceil($project->PROJECT_ID / 1000) - 1;

        if (!file_exists(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/')) {
            mkdir(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig, 0777);
        }

        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/' . $project->PROJECT_ID . '.txt', $MPO);

        //.. 20.03.2023 Тоценко. Копия заказа в папку ps_share/orders_backup
        $order_backup_path_year = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/orders_backup/' . date('Y');
        $order_backup_path_month = $order_backup_path_year . '/' . date('m');
        $order_backup_path = $order_backup_path_month . '/' . date('d');
        if (!file_exists($order_backup_path_year)) mkdir($order_backup_path_year, 0777, true);
        if (!file_exists($order_backup_path_month)) mkdir($order_backup_path_month, 0777, true);
        if (!file_exists($order_backup_path)) mkdir($order_backup_path, 0777, true);

        $getNewOrder = Orders::where(['ID' => $order->id])->first()->toArray();
        $getNewOrder['projects'] = [];
        array_push($getNewOrder['projects'], $project->PROJECT_ID);
        file_put_contents($order_backup_path . '/' . $order->id . '.json', json_encode($getNewOrder));

        // И копия записи таблицы проекта в папку ps_share/projects_backup
        $dirdig = ceil($project->PROJECT_ID / 1000) - 1;
        $project_backup_path = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/projects_backup/' . $dirdig;
        if (!file_exists($project_backup_path)) mkdir($project_backup_path, 0777, true);
        $getProject = Project::where(['PROJECT_ID' => $project->PROJECT_ID])->first()->toArray();
        file_put_contents($project_backup_path . '/' . $project->PROJECT_ID . '.json', json_encode($getProject));

        //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
        if (!file_exists(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig . '/')) {
            mkdir(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig, 0777);
        }

        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig . '/' . $project->PROJECT_ID . '.txt', $MPO);

        //.. 25.08.2022 Добавляем логирование
        // ********************************************************************************************************************************
        $logData = date('Y-m-d H:i:s') . "\r\n";
        $logData .= 'ID менеджера - ' . (session()->get('manager_id') ? session()->get('manager_id') : 'нет') . "\r\n";
        $logData .= 'Менеджер - ' . (session()->get('manager_name') ? session()->get('manager_name') : 'нет') . "\r\n";
        $logData .= 'ID клиента - ' . (session()->get('client_id') ? session()->get('client_id') : 'нет') . "\r\n";
        $logData .= 'Код клиента (ID в Акценте) - ' . (session()->get('client_code') ? session()->get('client_code') : 'нет') . "\r\n";
        $logData .= 'IP пользователя - ' . $_SERVER['REMOTE_ADDR'] . "\r\n";
        $logData .= 'ID заказа- ' . $order->id . "\r\n";
        $logData .= '#######################################################' . "\r\n\r\n\r\n";

        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'log/create_new_order/' . date('Y-m-d') . '.log', $logData, FILE_APPEND);
        chmod(config('app.serv_main_dir') . '/log/create_new_order/' . date('Y-m-d') . '.log', 0777);
        // ********************************************************************************************************************************

        return redirect('/projects_new/1_'.$order->id);
    } else {
        return redirect('/');
    }

});
//.. На этот роут приводит проект который стартует с create.php
Route::get('/start_new_create_porject', function (Request $request) {
    $client_id = null;
    $project_data = null;
    $storage = config('app.serv_main_dir').'/files/temp/';
    // Ищем файл с данными проекта
    if (file_exists($storage.$_GET['order_project_id'].'.tmp')) {
        if ($file = fopen($storage.$_GET['order_project_id'].'.tmp', 'r')) {
            $new_data_array = file_get_contents($storage.$_GET['order_project_id'].'.tmp');
            fclose($file);
        }
        $project_data = $new_data_array;
    }
    if (Request::input('client_id')) {
        if (preg_match('#^[\d]+$#', Request::input('client_id'))) {
            $client_id = Request::input('client_id');

          // Ищем файл с идентификатором клиента
        } elseif (file_exists($storage.$_GET['client_id'].'.tmp')) {
            if ($file = fopen($storage.$_GET['client_id'].'.tmp', 'r')) {
                $client_id = file_get_contents($storage.$_GET['client_id'].'.tmp');
                fclose($file);
            }
            $project_data = $new_data_array;
        }
    }

    if(isset($client_id)) {
        session(['client_id' => $client_id]);
    }
    session(['project_data' => $project_data]);
    return redirect('/');
});
Route::get('/start_new_porject_client', function (Request $request) {
    $client_id = Request::input('client_id');
    $client_code = Request::input('client_code');
    session()->flush();

    if($client_id) {
        session(['client_id' => $client_id]);
        session(['client_code' => $client_code]);
         
    }
    return redirect('/');
});
Route::post('/save_edit_project_manager', function (Request $request) {
    session_start();
    $project_data = Request::input('sep_project_data');
    $order_id = Request::input('sep_order_id');
    $manager_id = Request::input('sep_manager_id');
    $current_place = Request::input('current_place');
    $user_type_edge = Request::input('user_type_edge');
    //.. передаём имя менеджера (если есть, значит менеджер в системе)
    $manager_name = Request::input('sep_manager_name');
    $manager_admin = Request::input('sep_manager_admin');
    $ajax_type_send = Request::input('is_ajax');

    //..15.01.2021 Помещаем в сессию "project_data" вновь сохранённого проекта вместо предпоследней версии
    session(['project_data' => $project_data]);

    $check_order = Orders::where(['ID' => $order_id])->first();
    if(!$check_order || $check_order['readonly'] == 1) {
        if($ajax_type_send == 1) {
            return 1;
        }
        //..25.11.2020 в сессии session_id для восстановления
        if (session('ssnrst')) {
            return redirect(config('app.app_main_dir').'/login.php?session_id='.session('ssnrst'));
        } else { return redirect(config('app.app_main_dir').'/login.php'); }
    }

    $old_project = Project::where(['ORDER1C' => $order_id])->orderBy('DATE', 'desc')->first();
    if(!$old_project) {
        $project = new Project([
            'status' => 'start',
            'DATE' => date('Y-m-d H:i:s'),
            'ORDER1C' => $order_id,
            'manager_id' => $manager_id,
            'user_type_edge' => $user_type_edge,
            //.. В базу пишем пустую строку
            'MY_PROJECT_OUT' => '',
            'place' => $current_place,
            // 'MY_PROJECT_OUT' => base64_encode($project_data)
        ]);
        $project->save();

        //..23.11.2020..запись Создаём переменную с данными проекта
        $MPO = base64_encode($project_data);
        // и сохраняем в файл
        $dirdig = ceil($project->PROJECT_ID / 1000) - 1;
        $dir_path = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig;
        if (!file_exists($dir_path)) {
            if (!mkdir($dir_path, 0777, true)) {
                die('Не удалось создать директорию ' . $dirdig);
            }
        }
        file_put_contents($dir_path . '/' . $project->PROJECT_ID . '.txt', $MPO);

        //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
        $dir_path_2 = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig;
        if (!file_exists($dir_path_2)) {
            if (!mkdir($dir_path_2, 0777, true)) {
                die('Не удалось создать директорию ' . $dirdig);
            }
        }
        file_put_contents($dir_path_2 . '/' . $project->PROJECT_ID . '.txt', $MPO);

    } else if(base64_decode($old_project->MY_PROJECT_OUT) != $project_data) {
        $project = new Project([
            'status' => 'start',
            'DATE' => date('Y-m-d H:i:s'),
            'ORDER1C' => $order_id,
            'manager_id' => $manager_id,
            'user_type_edge' => $user_type_edge,
            //.. В базу пишем пустую строку
            'MY_PROJECT_OUT' => '',
            'place' => $current_place,
            // 'MY_PROJECT_OUT' => base64_encode($project_data)
        ]);
        $project->save();

        //..23.11.2020..запись Создаём переменную с данными проекта
        $MPO = base64_encode($project_data);
        // и сохраняем в файл
        $dirdig = ceil($project->PROJECT_ID / 1000) - 1;
        $dir_path = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig;
        if (!file_exists($dir_path)) {
            if (!mkdir($dir_path, 0777, true)) {
                die('Не удалось создать директорию ' . $dirdig);
            }
        }
        file_put_contents($dir_path . '/' . $project->PROJECT_ID . '.txt', $MPO);

        //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
        $dir_path_2 = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig;
        if (!file_exists($dir_path_2)) {
            if (!mkdir($dir_path_2, 0777, true)) {
                die('Не удалось создать директорию ' . $dirdig);
            }
        }
        file_put_contents($dir_path_2 . '/' . $project->PROJECT_ID . '.txt', $MPO);
    }

    if (!$old_project || ($old_project && base64_decode($old_project->MY_PROJECT_OUT) != $project_data)) $projectId = $project->PROJECT_ID;
    else $projectId = $old_project->PROJECT_ID;

    //.. 20.03.2023 Тоценко. Копия заказа в папку ps_share/orders_backup
    $order_backup_path_year = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/orders_backup/' . date('Y');
    $order_backup_path_month = $order_backup_path_year . '/' . date('m');
    $order_backup_path = $order_backup_path_month . '/' . date('d');
    if (!file_exists($order_backup_path_year)) mkdir($order_backup_path_year, 0777, true);
    if (!file_exists($order_backup_path_month)) mkdir($order_backup_path_month, 0777, true);
    if (!file_exists($order_backup_path)) mkdir($order_backup_path, 0777, true);
    
    $getOrder = Orders::where(['ID' => $order_id])->first()->toArray();
    $getOrder['projects'] = [];
    
    if (file_exists($order_backup_path . '/' . $order_id . '.json')) {
        $savedOrder = (array)json_decode(file_get_contents($order_backup_path . '/' . $order_id . '.json'));
        $savedProjects = $savedOrder['projects'];

        if (!in_array($projectId, $savedProjects)) array_push($savedProjects, $projectId);
        
        foreach ($savedProjects as $value) $getOrder['projects'][] = $value;
    } else array_push($getOrder['projects'], $projectId);

    file_put_contents($order_backup_path . '/' . $order_id . '.json', json_encode($getOrder));

    // И копия записи таблицы проекта в папку ps_share/projects_backup
    $dirdig = ceil($projectId / 1000) - 1;
    $project_backup_path = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/projects_backup/' . $dirdig;
    if (!file_exists($project_backup_path)) mkdir($project_backup_path, 0777, true);
    $getProject = Project::where(['PROJECT_ID' => $projectId])->first()->toArray();
    file_put_contents($project_backup_path . '/' . $projectId . '.json', json_encode($getProject));

    if($ajax_type_send == 1) {
        return 4;
    }
    //..25.11.2020 в сессии session_id для восстановления
    if (session('ssnrst')) {
        return redirect(config('app.app_main_dir').'/login.php?session_id='.session('ssnrst'));
    } else { return redirect(config('app.app_main_dir').'/login.php'); }
});
//.. Создание тикета (Помощь обработчика)
Route::post('/save_edit_ticket', function (Request $request) {

    $project_data = Request::input('sep_project_data');
    $order_id = Request::input('sep_order_id');
    $manager_id = Request::input('sep_manager_id');
    $client_id = Request::input('sep_client_id');
    $client_code = Request::input('sep_client_code');
    $manager_admin = Request::input('sep_manager_admin');

    $redirect_url = config('app.app_main_dir').'/helpdesc/api/ticket.php?create_new_ticket=1&order_id='.$order_id.'&client_id='.$client_id.'&manager_id='.$manager_id;
    // Orders:: - Таблица "ORDER1C"
    $check_order = Orders::where(['ID' => $order_id])->first();
    if(!$check_order) {
        $new_order_data = rand(111111111, 999999999);
        $order = new Orders([
            'CLIENT_ID' => $client_id,
            'manager' => $manager_id,
            'CLIENT' => $client_code,
            'status' => 'черновик',
            'DB_AC_IN' => date('Y-m-d H:i:s'),
            'DB_AC_NUM' => 'in_edits_'.$new_order_data,
            'DB_AC_ID' => $new_order_data,
        ]);
        $order->save();
        $order_id = $order->id;

    } else if($check_order['readonly'] == 1) {
        return redirect(config('app.app_main_dir').'/helpdesc/add_ticket.php?create_new_ticket=1&order_id='.$order_id.'&client_id='.$client_id.'&manager_id='.$manager_id);
    }
    $old_project = Project::where(['ORDER1C' => $order_id])->orderBy('DATE', 'desc')->first();
    if(!$old_project) {
        $project = new Project([
            'status' => 'start',
            'DATE' => date('Y-m-d H:i:s'),
            'ORDER1C' => $order_id,
            'manager_id' => $manager_id,
            //.. В базу пишем пустую строку
            'MY_PROJECT_OUT' => '',
            // 'MY_PROJECT_OUT' => base64_encode($project_data)
        ]);
        $project->save();


        //..23.11.2020..запись Создаём переменную с данными проекта
        $MPO = base64_encode($project_data);
        // и сохраняем в файл
        $dirdig = ceil($project->PROJECT_ID / 1000) - 1;
        $dir_path = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig;

        if (!file_exists($dir_path)) {
            if (!mkdir($dir_path, 0777, true)) {
                die('Не удалось создать директорию ' . $dirdig);
            }
        }
        file_put_contents($dir_path . '/' . $project->PROJECT_ID . '.txt', $MPO);

        //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
        $dir_path_2 = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig;
        if (!file_exists($dir_path_2)) {
            if (!mkdir($dir_path_2, 0777, true)) {
                die('Не удалось создать директорию ' . $dirdig);
            }
        }
        file_put_contents($dir_path_2 . '/' . $project->PROJECT_ID . '.txt', $MPO);

    } else if(base64_decode($old_project->MY_PROJECT_OUT) != $project_data) {
        $project = new Project([
            'status' => 'start',
            'DATE' => date('Y-m-d H:i:s'),
            'ORDER1C' => $order_id,
            'manager_id' => $manager_id,
            //.. В базу пишем пустую строку
            'MY_PROJECT_OUT' => '',
            // 'MY_PROJECT_OUT' => base64_encode($project_data)
        ]);
        $project->save();

        //..23.11.2020..запись Создаём переменную с данными проекта
        $MPO = base64_encode($project_data);
        // и сохраняем в файл
        $dirdig = ceil($project->PROJECT_ID / 1000) - 1;
        if (!file_exists(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/')) {
            mkdir(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig, 0777);
        }
        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/' . $project->PROJECT_ID . '.txt', $MPO);

        //.. 15.03.2023 Тоценко. Копия проекта в папку ps_share/project_out
        if (!file_exists(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig . '/')) {
            mkdir(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig, 0777);
        }
        file_put_contents(str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/project_out/' . $dirdig . '/' . $project->PROJECT_ID . '.txt', $MPO);

    }

    if (!$old_project || ($old_project && base64_decode($old_project->MY_PROJECT_OUT) != $project_data)) $projectId = $project->PROJECT_ID;
    else $projectId = $old_project->PROJECT_ID;

    //.. 20.03.2023 Тоценко. Копия заказа в папку ps_share/orders_backup
    $order_backup_path_year = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/orders_backup/' . date('Y');
    $order_backup_path_month = $order_backup_path_year . '/' . date('m');
    $order_backup_path = $order_backup_path_month . '/' . date('d');
    if (!file_exists($order_backup_path_year)) mkdir($order_backup_path_year, 0777, true);
    if (!file_exists($order_backup_path_month)) mkdir($order_backup_path_month, 0777, true);
    if (!file_exists($order_backup_path)) mkdir($order_backup_path, 0777, true);
    
    $getOrder = Orders::where(['ID' => $order_id])->first()->toArray();
    $getOrder['projects'] = [];

    if (file_exists($order_backup_path . '/' . $order_id . '.json')) {
        $savedOrder = (array)json_decode(file_get_contents($order_backup_path . '/' . $order_id . '.json'));
        $savedProjects = $savedOrder['projects'];
        
        if (!in_array($projectId, $savedProjects)) array_push($savedProjects, $projectId);
        
        foreach ($savedProjects as $value) $getOrder['projects'][] = $value;
    } else array_push($getOrder['projects'], $projectId);

    file_put_contents($order_backup_path . '/' . $order_id . '.json', json_encode($getOrder));

    // И копия записи таблицы проекта в папку ps_share/projects_backup
    $dirdig = ceil($projectId / 1000) - 1;
    $project_backup_path = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/ps_share/projects_backup/' . $dirdig;
    if (!file_exists($project_backup_path)) mkdir($project_backup_path, 0777, true);
    $getProject = Project::where(['PROJECT_ID' => $projectId])->first()->toArray();
    file_put_contents($project_backup_path . '/' . $projectId . '.json', json_encode($getProject));
    
    return redirect(config('app.app_main_dir').'/helpdesc/add_ticket.php?create_new_ticket=1&order_id='.$order_id.'&client_id='.$client_id.'&manager_id='.$manager_id);
});

Route::get('/clear_site', function () {
    Log::debug('CLEARED');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return 'dddd';
});
Route::get('/get_client_data', function () {
    $tel = Request::input('tel');
    $client = Clients::where(['tel' => '+'.$tel])->get()->toArray();
    return json_encode($client);
});
Route::post('/set_client_auth', function () {
    $client_id_setter = Request::input('client_id_setter');
    session(['client_id' => $client_id_setter]);
    return redirect('/');
});
Route::get('/get_client_materials', function () {


    $filter_t = Request::input('client_filter_volume');
    $filter_code = Request::input('client_filter_code');
    $filter_name = Request::input('client_filter_name');
    $filter_idm = Request::input('client_filter_idm');
    $filter_tel = Request::input('client_filter_tel');
    $filter_place = Request::input('client_filter_place');

    $client_data = Clients::where(['client_id' => session('client_id')])->first()->toArray();
    $client_ids_data = Clients::where(['code' => $client_data['code']])->get()->toArray();
    $client_ids = [];
     foreach($client_ids_data as $cid) {
         $client_ids[] = $cid['client_id'];
     }
    /// Юлия Поляков - Петренко

    $data = [];
    if(session('client_code') && !session('manager_id')) {
        $data = ClientMaterial::whereIn('Client', $client_ids)->get();
    } else {
        $data = ClientMaterial::whereIn('Client', $client_ids)->get();
    }

    $format_data = [];






    $check_code = [];

    foreach ($data as $item) {
        if($item->Count > 0) {
            //.. 05.12.2022 Тоценко. Остатки клиента. 
            // Закомментировал условие ниже, потому что на один филиал могут быть поставлены несколько остатков на приход
            // 28.02.2023 раскомментировал, потому что один приход в списке остатков отображался как несколько отдельных.
            if(isset($check_code[$item->Place]) && in_array($item->info->CODE, $check_code[$item->Place])) {
                continue;
            }
            $check_code[$item->Place][] = $item->info->CODE;
            $check_array = [
                'id' => $item->Material_client,
                'client' => $item->client->name,
                'client_phone' => $item->client->tel,
                'client_id' => $item->Client,
                'count' => $item->Count,
                'place' => $item->place->NAME,
                'place_id' => $item->Place,
                'W' => $item->info->W,
                'L' => $item->info->L,
                'T' => $item->info->T,
                'code' => $item->info->CODE,
                'name' => $item->info->Name,
                'crash' => $item->info->crash,
                'notation' => $item->info->notation
            ];
            if($filter_t) {
                if($item->info->T != $filter_t) {
                    continue;
                }
            }
            if($filter_code) {
                if($item->info->CODE != $filter_code) {
                    continue;
                }
            }
            if($filter_name) {
                if(strpos(mb_strtolower($item->info->Name), mb_strtolower($filter_name)) === false) {
                    continue;
                }
            }
            if($filter_idm) {
                if($item->Material_client != $filter_idm) {
                    continue;
                }
            }
            if($filter_tel) {
                if(strpos( mb_strtolower(strval($item->client->name)),mb_strtolower(strval($filter_tel))) === false ) {
                    continue;
                }
            }
            if($filter_place) {
                if($item->Place != $filter_place) {
                    continue;
                }
            }

            $format_data[] = $check_array;

        }

    }
    return json_encode($format_data);
});
Route::get('/get_client_orders', function() {

    $page = strval(Request::input('page')) - 1;
    $dates = strval(Request::input('date'));
    $status = Request::input('status');
    $type = Request::input('type');
    $client = session('client_code');
    $date = Carbon::today()->subDays($dates);


    if($status == 'all') {
        $status_db = ['отменен', 'в работе', 'выполнен'];
    } else {
        $status_db[] = $status;
    }

    $orders_сount = Orders::where(['CLIENT' => $client])
        ->where('DB_AC_IN','>=', $date)
        ->whereIn('status', $status_db)
        ->count();

    if($type == 1 || $type == 5) {
        $orders_сount_checker = Orders::where(['CLIENT' => $client])
            ->where('DB_AC_IN','>=', $date)
            ->whereIn('status', $status_db)
            ->get();
        foreach($orders_сount_checker as $order) {
            $project_out_client = '';
            if($order->project) {
                //..23.11.2020..чтение Если нет в хранилище, читаем из базы
                $dirdig = ceil($order->project->PROJECT_ID / 1000) - 1;
                $projectFolder = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/' . $order->project->PROJECT_ID . '.txt';
                if ($projectFile = file_get_contents($projectFolder)) {
                    $start_project = base64_decode($projectFile);
                } else if (Storage::disk('files_project_out')->exists($order->project->PROJECT_ID.'.txt')) {
                    $project_out_client = base64_decode(Storage::disk('files_project_out')->get($order->project->PROJECT_ID.'.txt'));
                } else {
                    //.. Поддержка чтения из БД (Устаревшая запись)
                    $project_out_client = base64_decode($order->project->MY_PROJECT_OUT);
                }
            }
            if($type == 1 && $project_out_client == '') {
                $orders_сount--;
            }
            if($type == 5 && $project_out_client != '') {
                $orders_сount--;
            }
        }
    }


    $orders_per_page = $orders_сount / 25;

    $orders = Orders::where(['CLIENT' => $client,])
        ->where('DB_AC_IN','>=', $date)
        ->whereIn('status', $status_db)
        ->skip($page*25)
        ->take(25)
        ->orderBy('DB_AC_IN', 'desc')
        ->get();

    $data = [];

    $edit_link = '1_';


    foreach($orders as $order) {
        $edit_links = [];
        if($order->readonly == 1) {
            $edit_link = '2_';
        }
        $projects_edit = Project::where(['ORDER1C' => $order->ID])->orderBy('DATE', 'desc')->get()->toArray();
        foreach ($projects_edit as $link) {
            $author = 'Версия';
            if($link['manager_id'] > 0) {
                $manager_name = Managers::where(['id' => $link['manager_id']])->first()->toArray();
                $author = $manager_name['name'];
            } else if($link['client_id'] > 0) {
                $author = 'Клиент';
            }
            $edit_links[] = [
                'link' => base64_encode($edit_link.$order->ID).'?order_project_id='.$link['PROJECT_ID'],
                'date' => $link['DATE'],
                'author' => $author
            ];
        }

        $project_out_client = '';
        if($order->project && $order->project->MY_PROJECT_OUT != 'started') {
            //..23.11.2020..чтение Если нет в хранилище, читаем из базы
            $dirdig = ceil($order->project->PROJECT_ID / 1000) - 1;
            $projectFolder = str_replace('\\', '/', config('app.serv_main_dir')) . 'files/project_out/' . $dirdig . '/' . $order->project->PROJECT_ID . '.txt';
            if ($projectFile = file_get_contents($projectFolder)) {
                $start_project = base64_decode($projectFile);
            } else if (Storage::disk('files_project_out')->exists($order->project->PROJECT_ID.'.txt')) {
                $project_out_client = base64_decode(Storage::disk('files_project_out')->get($order->project->PROJECT_ID.'.txt'));
            } else {
                //.. Поддержка чтения из БД (Устаревшая запись)
                $project_out_client = base64_decode($order->project->MY_PROJECT_OUT);
            }
        }
        if($type == 1 && $project_out_client == '') {
            continue;
        }
        if($type == 5 && $project_out_client != '') {
            continue;
        }
        $data['orders'][] = [
            'id' => $order->ID,
            'id_encode' => $edit_links,
            'akcent_id' => $order->DB_AC_ID,
            'date' => $order->DB_AC_IN,
            'place' => $order->place->NAME,
            'manager' => [
                'name' => $order->manager_info->name,
                'phone' => $order->manager_info->phone,
                'email' => $order->manager_info->{'e-mail'},
            ],
            'project' => $project_out_client,
            'big_id' => $order->DB_AC_ID.'/('.$order->DB_AC_NUM.')',
            'client_name' => $order->client_info->name,
            'status' => $order->status,
            'rs_order' => $order->rs_order,
        ];
        $data['pages'] = $orders_per_page;
    }
    return json_encode($data);


});

Route::get('/get_orders_goods', function () {

   $order = Request::input('order');

   $order_data = OrdersGoods::where(['ORDER1C_GOODS_ID' => $order])->get()->toArray();

   $data = [];

   foreach ($order_data as $item) {
       if($item['MATERIAL_ID']) {
            $name = Material::where(['MATERIAL_ID' => $item['MATERIAL_ID']])->first();
            $name = $name['NAME'];

            $data[] = [
                'name' => $name,
                'id' => $item['MATERIAL_ID'],
                'type' => 'Плитный материал',
                'count' => $item['COUNT'],
                'price' => $item['PRICE']
            ];
       }
       if($item['BAND_ID']) {
           $name = Band::where(['BAND_ID' => $item['BAND_ID']])->first();
           $name = $name['NAME'];

           $data[] = [
               'name' => $name,
               'id' => $item['BAND_ID'],
               'type' => 'Кромка',
               'count' => $item['COUNT'],
               'price' => $item['PRICE']
           ];
       }
       if($item['SIMPLE_ID']) {
           $name = Simple::where(['SIMPLE_ID' => $item['SIMPLE_ID']])->first();
           $name = $name['NAME'];

           $data[] = [
               'name' => $name,
               'id' => $item['SIMPLE_ID'],
               'type' => 'Материал',
               'count' => $item['COUNT'],
               'price' => $item['PRICE']
           ];
       }
       if($item['SERVICE_ID']) {
           $name = Service::where(['SERVICE_ID' => $item['SERVICE_ID']])->first();
           $name = $name['NAME'];

           $data[] = [
               'name' => $name,
               'id' => $item['SERVICE_ID'],
               'type' => 'Услуга',
               'count' => $item['COUNT'],
               'price' => $item['PRICE']
           ];
       }
   }

   return json_encode($data);

});

Route::get('/get_orders_parts', function () {

    $order = Request::input('order');

    $data_parts = Parts::where(['ORDER1C' => $order])->get()->toArray();

    $data = [];

    foreach($data_parts as $part) {
        $data[] = [
            'id' => $part['PART_ID'],
            'name' => $part['NAME'],
            'description' => $part['DESCRIPTION'],
            'W' => $part['W'],
            'L' => $part['L'],
            'T' => $part['T']
        ];
    }

    return json_encode($data);
});

Route::get('/get_index_extra_info', function () {
    header('Access-Control-Allow-Origin: *');
    $type = Request::input('type');

    $data_db = PicDrafts::where(['id' => $type])->first()->toArray();

    return json_encode($data_db);
});

//.. Расширение авторизации клиента в GibLab, если клиент авторизировался в create.php
Route::get('/set_auth_client_ids', function () {
    session_start();
    if (!Request::input('logout')) {
        if (Request::input('rssession')) {
            if (Request::input('rssession') == 2) {
                $redirect_uri = '/clients/docs.php';
            }
            if (Request::input('rssession') == 3) {
                $redirect_uri = '/helpdesc/';
            }
        }
        //.. Получаем данные пользователя по присланному идентификатору
        $user = DB::table('client')->where(['client_id' => Request::input('client_id')])->first();
        if(isset($user->client_id)) {
            session(['client_phone' => $user->tel]);
            session(['client_email' => $user->{'e-mail'}]);
            session(['client_code' => $user->code]);
            session(['client_fio' => $user->name]);
            session(['client_id' => $user->client_id]);
            session(['client_password' => $user->pass]);

            //.. Массив для поддержки авторизации create.php и Helpdesc
            $user_data_for_RS_auth = array(
                'ID' => $user->client_id,
                'name' => $user->name,
                'phone' => $user->tel,
                'mail' => $user->{'e-mail'},
                'role' => 'client',
                'code' => $user->code,
            );
            session(['user' => $user_data_for_RS_auth]);
        }
        $redirect_uri = '/clients/RS/clients/create.php';
        return redirect(config('app.app_main_dir').$redirect_uri.'?nw='.Request::input('nw'));
    } else {
        session()->flush();
        $redirect_uri = '/clients/login.php';
        return redirect(config('app.app_main_dir').$redirect_uri);
    }
});

Route::get('/set_auth_manager_ids', function () {
    session_start();
    //.. Условие при котором выход из учётки "manager_all_orders.php" и "create.php" очищает сессию Laravel
    if (!Request::input('logout')) {
        //.. Редирект в зависимости от того, с какого скрипта запрос
        if (Request::input('rssession')) {
                if (Request::input('rssession') == 1) {
                    $redirect_uri = '/manager_all_order.php';
                }
                if (Request::input('rssession') == 2) {
                    $redirect_uri = '/clients/index.php';
                }
                if (Request::input('rssession') == 3) {
                    $redirect_uri = '/helpdesc/';
                }
        }

        // Для сессии нужен файл ".txt" но он постоянно пропадает
        // код ниже создаёт этот файл, если его нет, каждый раз, когда менеджер попадает на страницу заказов.
        if(!file_exists(config('app.serv_main_dir').'kronasapp/storage/app/giblab_sessions/.txt')) {
            Storage::put('giblab_sessions/.txt', '');
        }
        
        // Периодически случается, что в сессию попадают старые данные, или данные другого проекта
        // поэтому здесь удаляем всё из сессии, кроме авторизации
        // Этот код выполняется каждый раз, когда менеджер попадает на страницу заказов (manager_all_order) вне Ларавеля
        // clrsdt - CLeaR Session DaTa
        if (Request::input("clrsdt")) {
            $data = unserialize(Storage::get('giblab_sessions/'.session()->get('data_id').'.txt'));

            if (isset(($data)) && !empty($data)) {
                foreach ($data as $key => $value) {
                    if ($key != '_token' && $key != 'data_id' && $key != 'saveAutorization' && $key != '_flash'
                        && $key != 'user_place' && $key != 'user_type_edge' && $key != 'isAdmin'
                        && $key != 'manager_name' && $key != 'manager_id' && $key != 'user' && $key != 'tec_info') {
                        unset($data[$key]);
                        session()->forget($key);
                    }
                }
                Storage::put('giblab_sessions/'.session()->get('data_id').'.txt', serialize($data));
            }
        }
        //.. Получаем данные пользователя по присланному идентификатору
        $user = DB::table('manager')->where(['id' => Request::input('manager_id')])->first();
        if(isset($user->id)) {
            session(['isAdmin' => $user->admin]);
            session(['manager_name' => $user->name]);
            session(['manager_id' => $user->id]);
            session(['user_place' => $user->place]);
            //.. Массив для поддержки авторизации create.php и Helpdesc
            $role = $user->admin == 1 ? 'admin' : 'manager';
            $user_data_for_RS_auth = array(
                'ID' => $user->id,
                'name' => $user->name,
                'phone' => $user->phone,
                'mail' => $user->{'e-mail'},
                'access' => $user->access,
                'role' => $role,
                'code' => $user->code,
            );
            session(['user' => $user_data_for_RS_auth]);
        }
        // if (Request::input('clrsdt')) {
        //     Storage::put('giblab_sessions/'.session()->get('data_id').'.txt', serialize(session()->all()));
        // }
        return redirect(config('app.app_main_dir').$redirect_uri);
    } else {
        //..07.12.2020 Сохраняем сессию юзера при входе в систему во временный файл
        Storage::delete('giblab_sessions/'.session()->get('data_id').'.txt');
        session()->flush();
        if (Request::input('logout') == 1) {
            $redirect_uri = '/login.php';
        }
        if (Request::input('logout') == 2) {
            $redirect_uri = '/clients/login.php';
        }
        if (Request::input('logout') == 3) {
            $redirect_uri = '/helpdesc/login.php';
        }
        return redirect(config('app.app_main_dir').$redirect_uri);
    }

});

Route::post('/basis_import', function () {
    ini_set('memory_limit', '512M');
    if (Request::input('BasisImport')) {
        $postData = Request::input('BasisImport');
        $data = false;
        $root = rtrim(str_replace('kronasapp', '', dirname(__DIR__)), '\\/');
        // require_once DIR_HANDLERS . 'basisImport_handler.php';
        require_once $root . "/system/library/handlers/basisImport_handler.php";
        // return $data;
    }
});

//.. 03.06.2022
// Иногда, если проект слишком большой, то он зависает,
// поэтому здесь проект каждую минуту записывается в файл.
// Пользователи, для которых работает этот функционал, записаны в файле "system/config/project_restore_user_ids.php"
Route::post('/recovery_project', function () {

    $project_restore_user_ids = require config('app.serv_main_dir').'system/config/project_restore_user_ids.php';
    $recoveryDir = config('app.serv_main_dir').'kronasapp/storage/app/recoveryProjects/';    
    $user = session()->get('user');
    $user_id = isset($user['ID']) ? $user['ID'] : null;
    $client_id = session()->get('client_id');
    $time = strtotime(date('Y-m-d H:i:s'));

    if (isset($user)) {
        if (in_array($client_id, $project_restore_user_ids) || in_array($user_id, $project_restore_user_ids)) {

            if (!Request::input('recovery')) {
                // Удаляем маленькие файлы (со стандартной структурой проекта)
                // и файлы, которые старше трёх суток.
                $result = scandir($recoveryDir);
                foreach ($result as $file){
                    if($file != '.' && $file != '..' && preg_match('#.project$#', $file)){
                        $period = preg_replace('#^.*.(\d{10}).project$#', '$1', $file);
                        if ((400 > filesize($recoveryDir . $file)) || (filemtime($recoveryDir . $file) < ($time - (86400 * 3)))) {
                            unlink($recoveryDir . $file);
                        }
                    }
                }

                // Записываем проект в файл
                Storage::put('recoveryProjects/' . $client_id . '.' .  $time . '.project', Request::input('project'));
                chmod($recoveryDir . $client_id . '.' .  $time . '.project', 0777);

                return 'Project save';
            } else {
                // Этот блок срабатывает по нажатию на кноку восстановления проекта
                $result = scandir($recoveryDir);
                $userFiles = [];
                $userFilesTimes = [];
                foreach ($result as $file){
                    if ($file != '.' && $file != '..' && preg_match('#.project$#', $file)) {
                        $tmpId = preg_replace('#^(\d+).(\d{10}).project$#', '$1', $file);
                        if ((int)$tmpId === (int)$client_id
                            && (!filemtime($recoveryDir . $file) < ($time - 86400))
                            && (400 < filesize($recoveryDir . $file)))
                        {
                            $userFilesTimes[] = preg_replace('#^(\d+).(\d{10}).project$#', '$2', $file);
                        }
                    }
                }

                if (!empty($userFilesTimes)) {
                    rsort($userFilesTimes);
                    if (file_exists($recoveryDir . $client_id . '.' . $userFilesTimes[0] . '.project')) {
                        $session = unserialize(Storage::get('giblab_sessions/' . session()->get('data_id') . '.txt'));
                        $project_data = Storage::get('recoveryProjects/' . $client_id . '.' . $userFilesTimes[0] . '.project');
                        $session['project_data'] = $project_data;
                        session(['project_data' => $project_data]);
                        Storage::put('giblab_sessions/' . session()->get('data_id') . '.txt', serialize($session));
                        chmod(config('app.serv_main_dir') . '/kronasapp/storage/app/giblab_sessions/' . session()->get('data_id') . '.txt', 0777);
                        return 'Project recovered';
                    }
                } else return null;
            }
        }
    }

    // Если условия не выполняются, то просто возвращаем null
    return null;

});

//.. 06.09.2022 Тоценко А. Параметр, есть ли раскрой при загрузке .kronas файла
Route::get('/reset_user_sheet_cutting', function () {
    if (session()->get('user_sheet_cutting')) {
        session()->forget('user_sheet_cutting');
    }
    return null;
});