<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('materials', 'API\CatalogController@materials');
Route::get('bands', 'API\CatalogController@bands');

Route::post('xnc_folder/create/', 'API\CatalogController@createXNCFolder');
Route::post('xnc/rename/', 'API\CatalogController@XNCRename');
Route::post('xnc/move/', 'API\CatalogController@XNCMove');
Route::post('xnc/delete/', 'API\CatalogController@XNCDelete');
Route::post('xnc/save/', 'API\CatalogController@XNCSave');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
