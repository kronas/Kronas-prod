function onClickSave() {
    //Function for forced saving of the project (Функция для принудительного сохранения проекта)
    // if (document.getElementById('saveContainer').innerText) {
    window.saveFromJS();
    // }
}

//Function for displaying information about the project. The message template is configured using the "projectInfo" parameter
//(Функция для вывода информации о проекте. Шаблон сообщения настраивается с помощю параметра "projectInfo")
function info(text) {
    document.getElementById('projectInfo').innerText = text;
}

function utoa(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

// base64 encoded ascii to ucs-2 string
function atou(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

function copyToClipboard(elem) {
    console.log('ffffff');
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
function getXNCcontent(folderId) {
    rs = "";
    rs += "<response>";
    //utoa - encode to BASE64
    if (folderId == null) {
        $.each(xnc_folders['root_folders']['items'], function (id, folder) {
            rs += "<folder id='" + folder['id'] + "' name='" + folder['name'] + "' />";
        });

        $.each(xnc_folders['root_items'], function (i, template) {
            rs += "<item id='" + template['id'] + "' name='" + template['name'] + "' data='" + template['data'] + "' />";
        });
    }

    $.each(xnc_folders['folders'], function (id, folder) {
        if (folderId == folder['id']) {
            $.each(folder['child_folders'], function (i, child_folder) {
                rs += "<folder id='" + child_folder['id'] + "' name='" + child_folder['name'] + "' />";
            });

            $.each(folder['templates'], function (i, template) {
                rs += "<item id='" + template['id'] + "' name='" + template['name'] + "' data='" + template['data'] + "' />";
            });

            rs += "<path>";
            $.each(folder['path'], function (i, pathfolder) {
                rs += "<folder id='" + pathfolder['id'] + "' name='" + pathfolder['name'] + "' />";
            })
            rs += "</path>";
        }

    });

    rs += "</response>";
    return rs;
}

function buildMaterialsList(params, is_foldered) {
    var rs = "<response>";
    rs += " <catalog name='Листовой материал'>";

    var pTypeId = params['typeId'];

    if (pTypeId && (pTypeId == 'CS')) {
        rs += buildMaterials(1, 'Листовой материал', '/images/icons/plate_materials_folder.png', catalog['plate_materials'], params, is_foldered);
        rs += buildMaterials(2, 'Столешницы', '/images/icons/tabletops_folder.png', catalog['tabletops'], params, is_foldered);
    }
    ;

    if (pTypeId && (pTypeId == 'EL')) {
        rs += buildMaterials(3, 'Кромка', '/images/icons/bands_folder.png', catalog['bands'], params, is_foldered);
    }
    ;

    rs += " </catalog>";
    rs += "</response>";

    return rs;
}

function buildPlateMaterial(item) {

    var rs = "<item code='" + item.code + "' name='" + item.name + "' unit='" + item.unit + "' price='" + item.price + "' thickness='" + item.thickness + "' width='" + item.width + "' length='" + item.length + "' count='" + item.count + "' image='" + item.image + "'>";
    rs += "<CS swMaxturns='6' cTrimL='8'  cCPF='0.9' cTrimW='8' csDirectCut='3' cCostByItemRound='22' cFillRep='0.9'  cCostType='0' cCostCut='0' cCostByItem='false' cBusinessWaste='10' csTexture='true'  cMinDimRegWaste1='300' cMinDimRegWaste2='300' cTrimWaster='true' csComplexBand='false' cSortBands='0' printable='true' startNewPage='true' cSizeMode='1' cCombiningParts='false' cGroupParts='0' cTrimWaste='false' cSaveDrawCut='false' cMinDimDetail='30' cAllowanceWorkpiece='0'/>";
    rs += tools['CS'];
    rs += "</item>";

    return rs;
}

function buildTabletop(item) {
    var rs = "<item code='" + item.code + "' name='" + item.name + "' unit='" + item.unit + "' price='" + item.price + "' thickness='" + item.thickness + "' width='" + item.width + "' length='" + item.length + "' count='" + item.count + "' image='" + item.image + "'>";
    //Особые праметры для операций
    rs += "<CS swMaxturns='6' cTrimL='0' cCPF='0.9' cTrimW='16' cFillRep='0.9'  cCostType='0' cCostCut='0' cCostByItem='false' cCostByItemRound='22' cBusinessWaste='10' csTexture='true' csDirectCut='3' cMinDimRegWaste1='300' cMinDimRegWaste2='300' cTrimWaster='true' csComplexBand='false' cSortBands='0' printable='true' startNewPage='true' cSizeMode='1' cCombiningParts='false' cGroupParts='0' cTrimWaste='false' cSaveDrawCut='false' cMinDimDetail='30' cAllowanceWorkpiece='0'/>";
    rs += tools['CS'];
    rs += "</item>";

    return rs;
}

function buildBand(item) {
    var rs = "<item code='" + item.code + "' name='" + item.name + "' unit='" + item.unit + "' price='" + item.price + "' thickness='" + item.thickness + "' width='" + item.width + "' count='" + item.count + "' image='" + item.image + "'>";
    //Особые праметры для операций
    rs += "<EL printable='true' startNewPage='true' elWastePRC='0.02' elCalcMat='true' elRoundMat='1' elRoundLength='21' elMinDimDetail='30'/>";
    rs += tools['EL'];
    rs += "</item>";

    return rs;
}

function contains(string, substring) {
    return (string.toString().toLowerCase().indexOf(substring.toString().toLowerCase()) !== -1);
}

function buildMaterials(folder_id, folder_name, folder_image, folder_items, params, is_foldered) {

    //Id родительской папки
    var pParentId = params["parentId"];
    //Тип операции
    var pTypeId = params["typeId"];
    //Часть кода
    var pCode = params["code"];
    //Часть наименования
    var pName = params["name"];
    //Часть единицы измерения
    var pUnit = params["unit"];
    //Длина
    var pLength = params["length"];
    //Ширина
    var pWidth = params["width"];
    //Толщина
    var pThickness = params["thickness"];
    //Цена
    var pPrice = params["price"];
    //Наличие на складе
    var pStock = params["stock"];

    if (is_foldered) {
        var rs = "<folder id='" + folder_id + "' name='" + folder_name + "' image='" + folder_image + "' hasChildFolders='false'>";
    } else {
        var rs = '';
    }

    // console.log(folder_id, folder_name);
    for (var i = 0; i < folder_items.length; i++) {
        var item = folder_items[i];



        // check item
        // skip if code not found
        if (pCode && !contains(item.code, pCode)) {
            continue;
        }

        // skip if name not found
        if (pName && !contains(item.name, pName)) {
            continue;
        }

        // skip if unit not found
        if (pUnit && !contains(item.unit, pUnit)) {
            continue;
        }

        // skip if length not found
        if (pLength && !(item.length == pLength)) {
            continue;
        }

        // skip if width not found
        if (pWidth && !(item.width == pWidth)) {
            continue;
        }

        // skip if thickness not found
        if (pThickness && !(item.thickness == pThickness)) {
            continue;
        }

        // skip if length not found
        if (pPrice && !(item.price == pPrice)) {
            continue;
        }

        // skip if length not found
        if (pStock && !(parseInt(item.count) < parseInt(pStock))) {
            continue;
        }

        if (item.st === undefined) {
            rs += buildBand(item);
        } else if (item.st === true) {
            rs += buildTabletop(item);
        } else {
            rs += buildPlateMaterial(item);
        }
    }

    if (is_foldered) {
        rs += "</folder>";
    }

    return rs;
}

function saveProject(xmlProject) {
    //save the project (сохранения проекта)

    if (xmlProject != null) {
        var serializer = new XMLSerializer();
        var content = serializer.serializeToString(xmlProject);

        $('#saveContainer').val(content);
        $('#saveContainer2').val(content);

        var currentdate = new Date();
        document.getElementById('saveTime').innerText = "Save time: " + currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + "  " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
    }

    return "<response/>";
}

function saveSettings(requestDoc) {
    // if (isAdmin) {
        var sParams = "";
        for (i = 0; i < requestDoc.documentElement.childNodes.length; i++) {
            var tag = requestDoc.documentElement.childNodes[i];
            var tagName = tag.nodeName;
            if ("param" == tagName) {
                node = requestDoc.documentElement.childNodes[i];
                if (node.getAttribute('remove') == "true")
                    sParams += "Remove setting: " + node.getAttribute('name') + '\n';
                else
                    sParams += "Save setting: " + node.getAttribute('name') + '=' + node.getAttribute('value') + '\n';
            }
        }
        console.log(sParams);
    // } else {
    //     alert('Access deenied. Cant\'t save settings changes.');
    // }
    return "<response/>";
}

function bool(value) {
    return value.toLowerCase() == 'true' ? true : false;
}

function loadRest(params) {
    console.log('loadrest');
    document.getElementById('checks_lodaer_containers').style.display = 'block';
    //Parameters (Параметры): project.id, project.name, project.date, material.code, typeId, sheet
    var rs = "<response>";
    ///if(!stock['bands'] && !stock['materials']) {
        console.log(params['material.code']);
        getStockCategory(params['material.code']);
        console.log(stock['materials']);
    ///}
    if (params['typeId'] == 'EL') {
        $.each(stock['bands'], function (i, el) {
            if ((el.code === params['material.code']) && (el.sheet === bool(params['sheet']))) {
                rs += "<rest dbId='" + el.id + "' comment='' thickness='" + el.thickness + "' width='" + el.width + "' count='" + el.count + "' />";
            }
            ;
        });
    }
    ;

    if (params['typeId'] == 'CS') {

        ///Индикатор наличия в MATERIAL_STOCK
        let first_from_stock = false;
        let place_id = $('#user_place2').val();
        let place_name = $('#user_place option[selected="selected"]').html();
        let sheets_in_place = false;

        ///Проверка наличия целого листа в MATERIAL_STOCK
        $.each(stock['materials'], function (i, el) {
            if((el.code == params['material.code']) && (el.from_stock == true) && (el.place_id == place_id)) {
                first_from_stock = true;
            }
        });

        ///Проверка наличия кусков на данном участке
        // $.each(stock['materials'], function (i, el) {
        //     if((el.code == params['material.code']) && (el.place_id == place_id)) {
        //         sheets_in_place = true;
        //     }
        // });



        ///Если такого листа нету на стоке, то добавляем его из MATERIALS один раз с остатком 0 и без PLACE
        if(first_from_stock == false) {
            if(!catalog['plate_materials']) {
                getCatalogCategory('plate_materials');
            }
            $.each(catalog['plate_materials'], function (i, el) {
                if(el.code == params['material.code']) {
                    rs += "<rest dbId='" + el.code + "' comment='"+ place_name +"' length='" + el.length + "' width='" + el.width + "' count='0' />";
                }
            });
        } else {
            $.each(stock['materials'], function (i, el) {
                if ((el.code == params['material.code']) && (el.sheet === bool(params['sheet'])) && (el.isSheet == false) && (el.place_id == place_id) && (el.from_stock == true)) {
                    rs += "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='" + el.count + "' />";
                }
                ;
            });
        }

        ///console.log('Куски на участке', sheets_in_place);

        ///if(sheets_in_place == true) {

            ///Добавляем из MATERIAL_STOCK и MATERIAL_SHEETS
            $.each(stock['materials'], function (i, el) {
                if ((el.code == params['material.code']) && (el.sheet === bool(params['sheet'])) && (el.isSheet == false) && (el.place_id == place_id) && (el.from_stock == false) && (el.count > 0)) {
                    rs += "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='" + el.count + "' />";
                }

            });





            let check_places = Object.entries(places)[0][1].map( el => {
                return el;
            });

            var full_part = {};
            $.each(catalog['plate_materials'], function (i, el) {
                if(el.code == params['material.code']) {
                    full_part = el;
                }
            });




            $.each(check_places, function (y, place) {

                var ssss = '';
                console.log('Place in cykle: ', place.id);
                $.each(stock['materials'], function (i, el) {
                    console.log('EP:', el.place_id, 'SP:', place.id);
                    if ( (el.from_stock == true) && (el.place_id == place.id) && (el.place_id != place_id) && (el.isSheet == false) && (el.count > 0) ) {
                        ssss = "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='" + el.count + "' />";
                    }
                });
                console.log(ssss);
                if(ssss !== 5 && ssss !== 9999 && ssss !== '') {
                    rs += ssss;
                }

                // } else if(place.id != place_id){
                //     rs += "<rest dbId='" + full_part.code + "' comment='" + place.name + "' length='" + full_part.length + "' width='" + full_part.width + "' count='0' />"
                // }
            });

            // $.each(stock['materials'], function (i, el) {
            //     if ((el.code == params['material.code']) && (el.sheet === bool(params['sheet'])) && (el.isSheet == false) && (el.place_id != place_id) && (el.sheet == false) && (el.place_id == place)) {
            //         rs += "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='" + el.count + "' />";
            //     } else if((el.code == params['material.code']) && (el.sheet === bool(params['sheet'])) && (el.isSheet == false) && (el.place_id != place_id) && (el.sheet == false) && (el.place_id != place)) {
            //         rs += "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='0' />";
            //     }
            // });





            $.each(stock['materials'], function (i, el) {
                if ((el.code == params['material.code']) && (el.sheet === bool(params['sheet'])) && (el.isSheet == false) && (el.place_id != place_id) && (el.from_stock == false)) {
                    rs += "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='" + el.count + "' />";
                }
                ;
            });




        // else {
        //
        //     ///Добавляем из MATERIAL_STOCK и MATERIAL_SHEETS
        //     $.each(stock['materials'], function (i, el) {
        //         if ((el.code == params['material.code']) && (el.sheet === bool(params['sheet'])) && (el.isSheet == false)) {
        //             rs += "<rest dbId='" + el.code + "' comment='" + el.place + "' length='" + el.length + "' width='" + el.width + "' count='" + el.count + "' />";
        //         }
        //         ;
        //     });
        //
        // }


        ///Обнуляем индикатор
        first_from_stock = false;
    }

    rs += "</response>";
    document.getElementById('checks_lodaer_containers').style.display = 'none';
    // If you return null, the command will be forwarded to the HTTP service
    //(Если вернуть null, то команда будет направлена к HTTP сервису)
    return rs;
}
function getCatalogCategory(cat) {

    const request = new XMLHttpRequest();

    const url = "/get_new_catalog?category=" + cat;

    request.open("GET", url, false);

    /// request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", () => {



        if(request.readyState === 4 && request.status === 200) {

            if(cat == 'bands') {
                catalog['bands'] = JSON.parse(request.responseText);

            }
            if(cat == 'plate_materials') {
                catalog['plate_materials'] = JSON.parse(request.responseText);

            }
            if(cat == 'tabletops') {
                catalog['tabletops'] = JSON.parse(request.responseText);

            }
        }
    });


    request.send();



}

function getCatalogCategorySerach(cat, params) {
    console.log(params);
    console.log('vvvvv');
    const request = new XMLHttpRequest();
    console.log('vvvvv1');
    let url = app_dir + "/get_new_catalog_search?category=" + cat;
    url = url + '&code=' + params.code;
    url = url + '&name=' + params.name;
    url = url + '&unit=' + params.unit;
    url = url + '&length=' + params.length;
    url = url + '&width=' + params.width;
    url = url + '&thickness=' + params.thickness;
    url = url + '&price=' + params.price;
    url = url + '&stock=' + params.stock;
    console.log('vvvvv4');
    console.log(url);

    request.open("GET", url, false);

    /// request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", () => {



        if(request.readyState === 4 && request.status === 200) {

            if(cat == 'bands') {
                console.log(JSON.parse(request.responseText));
                catalog['bands'] = JSON.parse(request.responseText);

            }
            if(cat == 'plate_materials') {
                console.log(JSON.parse(request.responseText));
                catalog['plate_materials'] = JSON.parse(request.responseText);

            }
            if(cat == 'tabletops') {
                console.log(JSON.parse(request.responseText));
                catalog['tabletops'] = JSON.parse(request.responseText);

            }
        }
    });


    request.send();



}



function getStockCategory(id) {

    ///document.getElementById('checks_lodaer_containers').style.display = 'block';

    const request = new XMLHttpRequest();

    const url = "/get_new_stock?m_id=" + id;

    request.open("GET", url, false);

    /// request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", () => {

        console.log('event listener here');

        if(request.readyState === 4 && request.status === 200) {

                stock = JSON.parse(request.responseText);
        }
    });


    request.send();



}



function get_filtred_data_catalog(catalog_id, type) {

    const request = new XMLHttpRequest();

    console.log(type);
    console.log('///////////////////////');

    let url = '/get_catalog_filtred?catalog=' + catalog_id + '&type=' + type;

    console.log(url);
    request.open("GET", url, false);

    request.addEventListener("readystatechange", () => {

        if(request.readyState === 4 && request.status === 200) {
            console.log('////////////////ddddddddddddddddddddddcxzcbnmccccc///////');

            if (type == 'p_') {
                catalog['plate_materials'] = JSON.parse(request.responseText);
            }
            if (type == 't_') {
                catalog['tabletops'] = JSON.parse(request.responseText);
            }
            if (type == 'e_') {
                console.log('Массив кромки с базы:');
                catalog['bands'] = JSON.parse(request.responseText);
            }

        }

    });

    request.send();

}

function get_goods_tree(parent) {

    const request = new XMLHttpRequest();

    let url = '/get_catalog_tree?parent=' + parent;

    console.log(url);
    request.open("GET", url, false);

    request.addEventListener("readystatechange", () => {

        if(request.readyState === 4 && request.status === 200) {
            tree = JSON.parse(request.responseText);
        }
    });

    request.send();
}



function getCatalogData(params, is_foldered) {


    console.log('Catalog in params:', params);

    //Id родительской папки
    var pParentId = params["parentId"];
    //Тип операции
    var pTypeId = params["typeId"];

    console.log(params);



   var plate_materials = {};///buildMaterials(1, 'Листовой материал', '/images/icons/plate_materials_folder.png', catalog['plate_materials'], params, is_foldered);
   var tabletops = {};///buildMaterials(2, 'Столешницы', '/images/icons/tabletops_folder.png', catalog['tabletops'], params, is_foldered);
   var bands = {};///buildMaterials(3, 'Кромка', '/images/icons/bands_folder.png', catalog['bands'], params, is_foldered);

    rs = "";

    if (pParentId == null || pParentId == "") {


        if(params.thickness != undefined) {


            $('#checks_lodaer_containers').show();






            // getCatalogCategory('plate_materials');
            // getCatalogCategory('tabletops');
            // getCatalogCategory('bands');


            // getCatalogCategorySerach('tabletops', params);
            // getCatalogCategorySerach('bands', params);



            ///tabletops = buildMaterials(2, 'Столешницы', '/images/icons/tabletops_folder.png', catalog['tabletops'], params, is_foldered);



            var rs = "<response>";

            rs += " <catalog name='Листовой материал'>";

            var pTypeId = params['typeId'];


            if (pTypeId && (pTypeId == 'CS')) {
                getCatalogCategorySerach('plate_materials', params);
                plate_materials = buildMaterials(1, 'Листовой материал', '/images/icons/plate_materials_folder.png', catalog['plate_materials'], params, is_foldered);
                rs += plate_materials;
                ///rs += tabletops;
            }
            ;

            if (pTypeId && (pTypeId == 'EL')) {
                getCatalogCategorySerach('bands', params);
                bands = buildMaterials(3, 'Кромка', '/images/icons/bands_folder.png', catalog['bands'], params, is_foldered);
                rs += bands;
            }
            ;

            // rs += plate_materials;
            // rs += bands;

            rs += " </catalog>";
            rs += "</response>";


            $('#checks_lodaer_containers').hide();

            return rs;
        } else {

            var rs = "<response>";

            rs += " <catalog name='Листовой материал'>";

            var pTypeId = params['typeId'];


            if (pTypeId && (pTypeId == 'CS')) {

                rs += "<folder id='p_973'  name='Листовой материал' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
                rs += "</folder>";
                rs += "<folder id='t_1077'  name='Столешницы' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
                rs += "</folder>";
            }
            ;

            if (pTypeId && (pTypeId == 'EL')) {

                rs += "<folder id='e_682'  name='Кромка' image='/images/icons/bands_folder.png' hasChildFolders='true'>";
                rs += "</folder>";
            }
            ;

            rs += " </catalog>";
            rs += "</response>";




            return rs;
        }


        // if(!catalog['plate_materials'] || !catalog['tabletops'] || !catalog['bands']) {
        //
        //     getCatalogCategory('plate_materials');
        //     getCatalogCategory('tabletops');
        //     getCatalogCategory('bands');
        //
        // }





    }
    else if (pParentId == '1') {

        // if(!catalog['plate_materials'] || !catalog['tabletops']) {
        //
        //     getCatalogCategory('plate_materials');
        //     getCatalogCategory('tabletops');
        //
        //
        // }
        //
        // plate_materials = buildMaterials(1, 'Листовой материал', '/images/icons/plate_materials_folder.png', catalog['plate_materials'], params, is_foldered);
        // tabletops = buildMaterials(2, 'Столешницы', '/images/icons/tabletops_folder.png', catalog['tabletops'], params, is_foldered);





        var rs = "<response>";
        rs += " <catalog name='Листовой материал'>";

        var pTypeId = params['typeId'];

        if (pTypeId && (pTypeId == 'CS')) {
            // rs += plate_materials;
            // rs += tabletops;
            rs += "<folder id='p_973'  name='Листовой материал' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
            rs += "</folder>";
            rs += "<folder id='t_1077'  name='Столешницы' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
            rs += "</folder>";
        }
        ;

        rs += " </catalog>";
        rs += "</response>";

        $('#checks_lodaer_containers').hide();



        return rs;
    }
    else if (pParentId == '3') {

        var rs = "<response>";
        rs += " <catalog name='Кромка'>";

        var pTypeId = params['typeId'];


        if (pTypeId && (pTypeId == 'EL')) {
            rs += "<folder id='e_682'  name='Кромка' image='/images/icons/bands_folder.png' hasChildFolders='true'>";
            rs += "</folder>";
        }
        ;

        rs += " </catalog>";
        rs += "</response>";

        return rs;


        // if(!catalog['bands']) {
        //
        //     getCatalogCategory('bands');
        //
        // }
        //
        // bands = buildMaterials(3, 'Кромка', '/images/icons/bands_folder.png', catalog['bands'], params, is_foldered);
        //
        // var rs = "<response>";
        // rs += " <catalog name='Кромка'>";
        //
        // var pTypeId = params['typeId'];
        //
        //
        // if (pTypeId && (pTypeId == 'EL')) {
        //     rs += "<folder id='470'  name='Кромка' image='/images/icons/bands_folder.png' hasChildFolders='true'>";
        //     rs += bands;
        //     rs += "</folder>";
        // }
        // ;
        //
        // rs += " </catalog>";
        // rs += "</response>";
        //
        //
        // $('#checks_lodaer_containers').hide();


    }
    else {
        get_goods_tree(pParentId.slice(2));

        console.log(tree.length);
        if(tree.length > 0) {

            let catalog_type = pParentId.charAt(0) + pParentId.charAt(1);

            var rs = "<response>";
            rs += " <catalog name='Листовой материал'>";

            tree.forEach(element => {
                if(catalog_type == 'p_' && element.FOLDER_ID == 1077) {

                } else {
                    rs += " <folder id='" + catalog_type + element.FOLDER_ID + "' name='"+ element.NAME +"' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
                    rs += "</folder>";
                }
            });

            rs += " </catalog>";
            rs += "</response>";

            return rs

        } else {

            let catalog_type = pParentId.charAt(0) + pParentId.charAt(1);

            get_filtred_data_catalog(pParentId.slice(2),catalog_type);


            let materials = null;

            console.log(catalog);
            if(catalog_type == 'p_') {
                materials = buildMaterials('products', 'Содержимое', '/images/icons/plate_materials_folder.png', catalog['plate_materials'], params, is_foldered);
            } else if(catalog_type == 't_') {
                materials = buildMaterials('products', 'Содержимое', '/images/icons/tabletops_folder.png', catalog['tabletops'], params, is_foldered);
            } else if(catalog_type == 'e_') {
                materials = buildMaterials('products', 'Содержимое', '/images/icons/bands_folder.png', catalog['bands'], params, is_foldered);
            }



            var rs = "<response>";
            rs += " <catalog name='Листовой материал'>";

            rs += materials;

            rs += " </catalog>";
            rs += "</response>";

            return rs

        }



    }
    // else if (pParentId == 'plate_material') {
    //
    //     get_filtred_data_catalog('manufacturer', 0, {});
    //
    //     var rs = "<response>";
    //     rs += " <catalog name='Листовой материал'>";
    //
    //     manufacturers.forEach(element => {
    //
    //         rs += " <folder id='m_" + element + "' manufacturer='" + element + "'  name='"+ element +"' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
    //         rs += "</folder>";
    //     });
    //
    //     rs += " </catalog>";
    //     rs += "</response>";
    //
    //     actual_data.st = 0;
    //
    //     return rs
    // }
    // else if (pParentId == 'tabletops') {
    //
    //     get_filtred_data_catalog('manufacturer', 1, {});
    //
    //     var rs = "<response>";
    //     rs += " <catalog name='Столешницы'>";
    //
    //     manufacturers.forEach(element => {
    //         rs += " <folder id='m_" + element + "'  name='"+element +"' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
    //         rs += "</folder>";
    //     });
    //
    //     rs += " </catalog>";
    //     rs += "</response>";
    //
    //     actual_data.st = 1;
    //
    //     return rs
    // }
    // else if (pParentId.charAt(0) == 'm' && pParentId.charAt(1) == '_') {
    //
    //     get_filtred_data_catalog('volume', actual_data.st, {manufacturer: pParentId.slice(2)});
    //
    //     var rs = "<response>";
    //     rs += " <catalog name='Листовой материал'>";
    //
    //     volume.forEach(element => {
    //         rs += " <folder id='t_" + element + "'  name='" + element + "' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
    //         rs += "</folder>";
    //     });
    //
    //     rs += " </catalog>";
    //     rs += "</response>";
    //
    //     actual_data.manufacturer = pParentId.slice(2);
    //
    //     return rs
    //
    // }
    // else if (pParentId.charAt(0) == 't' && pParentId.charAt(1) == '_') {
    //
    //     get_filtred_data_catalog('height_width', actual_data.st, {manufacturer: actual_data.manufacturer, volume: pParentId.slice(2)});
    //
    //     var rs = "<response>";
    //     rs += " <catalog name='Листовой материал'>";
    //
    //     width_height.forEach(element => {
    //         rs += " <folder id='w_" + element + "'  name='" + element + "' image='http://giblab.com/download/images/dsp.jpg' hasChildFolders='true'>";
    //         // rs += plate_materials;
    //         rs += "</folder>";
    //     });
    //     rs += " </catalog>";
    //     rs += "</response>";
    //
    //     actual_data.volume = pParentId.slice(2);
    //
    //     return rs
    // }
    // else if (pParentId.charAt(0) == 'w' && pParentId.charAt(1) == '_') {
    //
    //     get_filtred_data_catalog('products', actual_data.st, {manufacturer: actual_data.manufacturer, volume: actual_data.volume, wht: pParentId.slice(2)});
    //
    //     let plate_materials = null;
    //
    //     if(actual_data.st == 0) {
    //         plate_materials = buildMaterials('products', 'Содержимое', '/images/icons/plate_materials_folder.png', catalog['plate_materials'], params, is_foldered);
    //     }
    //     if(actual_data.st == 1) {
    //         plate_materials = buildMaterials('products', 'Содержимое', '/images/icons/plate_materials_folder.png', catalog['tabletops'], params, is_foldered);
    //     }
    //
    //
    //     var rs = "<response>";
    //     rs += " <catalog name='Листовой материал'>";
    //     rs += plate_materials;
    //     rs += " </catalog>";
    //     rs += "</response>";
    //
    //     return rs
    // }


    return rs;
}

function contentList(params) {
    var folderId = params["folder"];//ID папки
    var content = params["content"];//Тип содержимого
    if (content == "XNC") {
        rs = getXNCcontent(folderId);
        return rs;
    } else {
        return null;
    }
}

function createFolder(params) {
    if (isAdmin) {
        var content = params["content"];//Тип содержимого
        var name = params["name"];//Имя папки
        var folderId = params["folder"];//ID папки

        if (content == "XNC") {
            $.post('/api/xnc_folder/create/', {
                'folder_id': folderId,
                'name': name,
                'content': content
            }, function (response) {
                console.log(response);
            });

            rs = getXNCcontent(folderId);
            return rs;
        } else {
            return null;
        }
    } else {
        alert('Access deenied.');
        return null;
    }
}

function contentRename(params) {
    if (isAdmin) {
        var id = params["id"];//ID элемента
        var name = params["name"];//Имя элемента
        var isFolder = params["isFolder"];//Папка или элемент
        var content = params["content"];//Тип содержимого
        var folderId = params["folder"];//ID папки

        if (content == "XNC") {
            $.post('/api/xnc/rename/', {
                'id': id,
                'name': name,
                'content': content,
                'is_folder': isFolder,
                'folder_id': folderId
            }, function (response) {
                console.log(response);
            });

            rs = getXNCcontent(folderId);
            return rs;
        } else {
            return null;
        }
    } else {
        alert('Доступ к операции без прав администратора запрещён.');
        return null;
    }
}

function contentDelete(requestDoc, params) {
    if (isAdmin) {
        var content = params["content"];//Тип содержимого
        var folderId = params["folder"];//ID папки

        var isFolder = true;
        var id = folderId;

        if (content == "XNC") {
            var sParams = "";
            for (i = 0; i < requestDoc.documentElement.childNodes.length; i++) {
                var tag = requestDoc.documentElement.childNodes[i];
                var tagName = tag.nodeName;
                if ("item" == tagName || "folder" == tagName) {
                    sParams += tagName + " id: " + tag.getAttribute('id') + '\n';
                    isFolder = ("folder" == tagName);
                    id = tag.getAttribute('id');
                }
            }

            $.post('/api/xnc/delete/', {
                'id': id,
                'content': content,
                'is_folder': isFolder
            }, function (response) {
                console.log(response);
            });

            rs = getXNCcontent(folderId);
            return rs;
        } else {
            return null;
        }
    } else {
        alert('Access deenied.');
        return null;
    }
}

function contentMove(requestDoc, params) {
    if (isAdmin) {
        var isFolder = true;
        var folderId = params["folder"];//ID папки
        var intoFolderId = params["intoFolder"];//ID папки в которую перемещаются элементы

        var id = folderId;
        var content = params["content"];//Тип содержимого

        if (content == "XNC") {
            var sParams = "";
            for (i = 0; i < requestDoc.documentElement.childNodes.length; i++) {
                var tag = requestDoc.documentElement.childNodes[i];
                var tagName = tag.nodeName;
                if ("item" == tagName || "folder" == tagName) {
                    sParams += tagName + " id: " + tag.getAttribute('id') + '\n';

                    isFolder = ("folder" == tagName);
                    id = tag.getAttribute('id');
                }
            }

            $.post('/api/xnc/move/', {
                'content': content,
                'is_folder': isFolder,
                'folder_id': folderId,
                'new_folder_id': intoFolderId,
                'id': id
            }, function (response) {
                console.log(response);
            });

            rs = getXNCcontent(folderId);
            return rs;
        } else {
            return null;
        }
    } else {
        alert('Access denied.');
        return null;
    }
}

function contentSave(requestDoc, params) {
    if (isAdmin) {

        console.log(params);

        var name = params["name"];//Имя элемента
        var folder = params["folder"];//Папка куда сохраняется элемент
        var content = params["content"];//Тип содержимого
        var data = params["data"];//Содержимое для сохранения в base64

        if (content == "XNC") {
            $.post('/api/xnc/save/', {
                'name': name,
                'content': content,
                'data': data,
                'folder_id': folder
            }, function (response) {
                console.log(response);
            });

            return "<response/>";
        } else {
            return null;
        }
    } else {
        alert('Access denied.');
        return null;
    }
}

function action(action, requestXml) {
    parser = new DOMParser();
    requestDoc = parser.parseFromString(requestXml, "application/xml");

    let d = new Date,
        dformat = [d.getFullYear(),
                    d.getMonth().toString().length == 1 ? '0' + (d.getMonth() + 1) : d.getMonth() + 1,
                    d.getDate().toString().length == 1 ? '0' + (d.getDate()) : d.getDate()].join('-') + ' ' +
                    [d.getHours().toString().length == 1 ? '0' + (d.getHours()) : d.getHours(),
                    d.getMinutes().toString().length == 1 ? '0' + (d.getMinutes()) : d.getMinutes(),
                    d.getSeconds().toString().length == 1 ? '0' + (d.getSeconds()) : d.getSeconds()].join(':');

    //.. 03.06.2022 Вывод проекта в консоль
    console.log("ProjectData -> " + dformat + "\r\n", requestXml);

    //.. 03.06.2022 Отправка проекта для резервного сохранения
    let re = /<\/?request>/g;
        projectData = requestXml.replace(re, '');

    $.ajax({
        url: '/recovery_project',
        method: 'POST',
        async: false,
        data: {'_token':  new_csrf_token, project: projectData},
        success: function(data) {
            
        }
    });


    var params = {};
    var sParams = "";
    var xmlProject = null;
    for (i = 0; i < requestDoc.documentElement.childNodes.length; i++) {
        var tag = requestDoc.documentElement.childNodes[i];
        var tagName = tag.nodeName;
        if ("param" == tagName) {
            node = requestDoc.documentElement.childNodes[i];
            sParams += node.getAttribute('name') + '=' + node.getAttribute('value') + '\n';
            params[node.getAttribute('name')] = node.getAttribute('value');
        } else if ("project" == tagName) {
            xmlProject = tag;
        }
    }

    if (action == "form.startData") {
        return getStartData();
    } else if (action == "form.saveProject") {
        return saveProject(xmlProject);
    } else if (action == "form.saveSettings") {
        return saveSettings(requestDoc);
    } else if (action == "form.loadRest") {
        return loadRest(params);
    }

    if (action == "form.catalogGet") {
        return getCatalogData(params, true);
    }

    if (action == "form.catalogSearch") {
        console.log('Search params:', params);
        return getCatalogData(params, false);
    }

    if (action == "form.contentList") {
        return contentList(params);
    }

    if (action == "form.contentCreateFolder") {
        return createFolder(params);
    }
    if (action == "form.contentRename") {
        return contentRename(params);
    }
    if (action == "form.contentDelete") {
        return contentDelete(requestDoc, params);
    }
    if (action == "form.contentMove") {
        return contentMove(requestDoc, params);
    }
    if (action == "form.contentSave") {
        return contentSave(requestDoc, params);
    }
    if (action == "form.check") {
        // alert("DATA: "+(new XMLSerializer().serializeToString(xmlProject)));
        checks_this_project();
        return `
            <div id="new_checks_container">
                <div class="inner">
                    <!--<img src="/images/download.gif" alt="">-->
                </div>
            </div>
            <button id="button_copy_buffer_clipboard">Скопировать текст</button>
        `;







        ///return result;

        // return setTimeout(function () {
        //     return 'sssssss';
        // }, 1000);

        // if(false) {
        //     return null;
        // } else {
        //
        //     return 'sssssss';




            ///checks_this_project();

            // return setTimeout(function () {
            //     return `<html>
            //         <div id="new_loader" style="display: block;">
            //           <div>
            //               <img src="/images/download.gif" alt="">
            //           </div>
            //         </div>
            //     </html>
            //     `;
            // }, 1500);
        }



        //Вывод информации о проблемах в HTML формате
        ///return "<html>Проблемы возникли в....</html>";

    //}
}

$(function () {
    $(window).off('beforeunload');

    $('form').submit(function (e) {
        $(window).off('beforeunload');
        $(window).unbind('beforeunload');
        window.onbeforeunload = null;
    });

    $('#user_type_edge, #user_place').on('change', function (_e) {
        var el = $(_e.currentTarget);

        window.saveFromJS();

        $('#user_place2').val($('#user_place').val());
        $('#user_type_edge2').val($('#user_type_edge').val());

        $('#config_form').submit();
    });



    $('#saveBtn').on('click', async function (_e) {
        _e.preventDefault();
        $(window).off('beforeunload');
        $(window).unbind('beforeunload');
        window.onbeforeunload = null;
        await window.saveFromJS();
        $('#checks_lodaer_containers').show();
        console.log('Запрос на перезапись cессии');
        await save_xml_project($('#saveContainer').val());
        console.log('Сессия с данными перезаписана');
        let request = await fetch('/send_project', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN':  new_csrf_token,
                'X-XSRF-TOKEN':  new_csrf_token,
            },
            credentials: 'include',
            body: JSON.stringify({
                _token: $('#continue_form input[name="_token"]').val(),
                project_data: $('#continue_form input[name="project_data"]').val(),
                user_place: $('#user_place2').val(),
                user_type_edge: $('#user_type_edge2').val(),
            })
        });

        let response = await request.text();
        await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
        console.log('Data_id записан в сессию');

        if (response.length > 0) {
            console.log('Отправляем на оптимизацию и проверки');
            let reqStatus = await fetch('/check_project?data_id=' + response, {method: 'GET'});

            let reqBody = await reqStatus.text();
            console.log('Пришел ответ с index_anton');

            let reqBodyJson = JSON.parse(reqBody.replace("Ошибок не обнаружено!", ""));

            console.log(reqBodyJson);
            $('#checks_container').html('');

            $('#checks_lodaer_containers').hide();

            if (reqBodyJson.step_1_errors == null && reqBodyJson.step_3_errors == null) {

                _e.preventDefault();
                await window.saveFromJS();
                await save_xml_project(reqBodyJson.project_data);
                console.log('Cохранена porject_data');
                await save_xml_project_checked(reqBodyJson.project_data);
                console.log('Сохранена checked_project_data');



                if((reqBodyJson.step_1_errors_ok == null && reqBodyJson.step_3_errors_ok == null) || (reqBodyJson.step_1_errors_ok == '' && reqBodyJson.step_3_errors_ok == '')) {

                    if(!reqBodyJson.curl) {
                    $('#extra_buttons_container #extra_operations_button').attr('href', `${main_dir}/index.php?data_id=${response}`);
                    $('#extra_buttons_container #get_reciepe_button').attr('href', `${main_dir}/check_sheet_parts.php?data_id=${response}`);
                    $('#extra_buttons_container').show();
                    $('#mpr_import_button').show();
                    $('#bottom_container').hide();
                        // document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                        //     copyToClipboard(document.getElementById("new_checks_container"));
                        // });
                        // $('#button_copy_buffer_clipboard').show();
                        // $('#button_copy_buffer_clipboard').on('click', function () {
                        //     copyToClipboard($('#new_checks_container'));
                        // });
                        $('.dialog-buttons .gwt-Button').on('click', function() {
                            location.reload();
                        });
                        $('.gwt-DialogBox img').on('click', function() {
                            location.reload();
                        });
                    } else {
                        $('#checks_container').append(reqBodyJson.curl);
                        $('.checks_container').show();
                    }

                } else {
                    if(!reqBodyJson.curl) {
                    $('#checks_container').append('<b style="color: orange;">Допустимые ошибки :</b><br>');
                    $('#checks_container').append(reqBodyJson.step_1_errors_ok);
                    ///$('#checks_container').append(reqBodyJson.step_3_errors_ok);
                    $('.checks_container').show();
                    $('#extra_buttons_container #extra_operations_button').attr('href', `${main_dir}/index.php?data_id=${response}`);
                    $('#extra_buttons_container #get_reciepe_button').attr('href', `${main_dir}/check_sheet_parts.php?data_id=${response}`);
                    $('#extra_buttons_container').show();
                        $('#mpr_import_button').show();
                    $('#bottom_container').hide();
                    if(document.getElementById("button_copy_buffer_clipboard")) {
                        document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                            copyToClipboard(document.getElementById("new_checks_container"));
                        });
                    }

                        $('#button_copy_buffer_clipboard').show();
                        $('.dialog-buttons .gwt-Button').on('click', function() {
                            location.reload();
                        });
                        $('.gwt-DialogBox img').on('click', function() {
                            location.reload();
                        });
                    } else {
                        $('#checks_container').append(reqBodyJson.curl);
                        $('.checks_container').show();
                    }
                }

            } else {
                $('#checks_container').append('<b style="color: red;">Критичиские ошибки :</b><br>');
                $('#checks_container').append(reqBodyJson.step_1_errors);
                $('#checks_container').append('<b style="color: orange;">Допустимые ошибки :</b><br>');
                $('#checks_container').append(reqBodyJson.step_1_errors_ok);
                if(document.getElementById("button_copy_buffer_clipboard")) {
                    document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                        copyToClipboard(document.getElementById("new_checks_container"));
                    });
                }
                $('#button_copy_buffer_clipboard').show();
                ///$('#checks_container').append(reqBodyJson.step_3_errors);
                $('.checks_container').show();
            }

            $('#checks_container').append('<button id="close_check_container">ОК</button>');
            $('#close_check_container').on('click', function () {
                $('.checks_container').hide();
            });
        }
    });


})

function unique(arr) {
    let array = arr.split('<hr>');
    let result = [];

    for (let str of array) {
        if (!result.includes(str)) {
            if(str.length > 3) {
                result.push(str);
            }
        }
    }

    return result;
}

async function checks_this_project(button = 'standart') {
    $('#start_checks_lodaer_containers').show();

    $(window).off('beforeunload');
    $(window).unbind('beforeunload');
    window.onbeforeunload = null;
    await window.saveFromJS();
    console.log('Запрос на перезапись cессии');
    await save_xml_project($('#saveContainer').val());
    console.log('Сессия с данными перезаписана');
    let request = await fetch('/send_project', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN':  new_csrf_token,
            'X-XSRF-TOKEN':  new_csrf_token,
        },
        credentials: 'include',
        body: JSON.stringify({
            _token: $('#continue_form input[name="_token"]').val(),
            project_data: $('#continue_form input[name="project_data"]').val(),
            user_place: $('#continue_form input[name="user_place"]').val(),
            user_type_edge: $('#continue_form input[name="user_type_edge"]').val(),
        })
    });
    let response = await request.text();
    await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
    console.log('Data_id записан в сессию');

    $('.gwt-DialogBox').find('.dialogTop .tool-panel-label').hide();

    if (response.length > 0) {
        console.log('Отправляем на оптимизацию и проверки');
        let reqStatus = await fetch('/check_project?data_id=' + response, {method: 'GET'});

        let reqBody = await reqStatus.text();
        console.log("Пришел ответ с index_anton\r\n"+reqBody);

        let reqBodyJson = JSON.parse(reqBody.replace("Ошибок не обнаружено!", ""));

        // console.log('reqBodyJson', reqBodyJson);
        $('#new_checks_container').html('');

        ///$('#checks_lodaer_containers').hide();

        if ((reqBodyJson.step_1_errors == null || reqBodyJson.step_1_errors == '') && (reqBodyJson.step_3_errors == null || reqBodyJson.step_3_errors == '')) {
            await window.saveFromJS();
            await save_xml_project(reqBodyJson.project_data);
            console.log('Cохранена porject_data');
            await save_xml_project_checked(reqBodyJson.project_data);
            console.log('Сохранена checked_project_data');



            if((reqBodyJson.step_1_errors_ok == null || reqBodyJson.step_1_errors_ok == '') && (reqBodyJson.step_3_errors_ok == null || reqBodyJson.step_3_errors_ok == '')) {

                if(!reqBodyJson.curl) {
                    $('#extra_buttons_container #extra_operations_button').attr('href', `${main_dir}/index.php?data_id=${response}`);
                    $('#extra_buttons_container #get_reciepe_button').attr('href', `${main_dir}/check_sheet_parts.php?data_id=${response}`);
                    $('#extra_buttons_container').show();
                    $('#mpr_import_button').show();
                    $('#bottom_container').hide();
                    $('#new_checks_container').append('Ошибок не обнаружено');

                    console.log(button)
                    if(button == 'receipt' || button == 'extra') {
                        return 1;
                    }

                    $('.dialog-buttons .gwt-Button').on('click', function() {
                        location.reload();
                    });

                    document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                        copyToClipboard(document.getElementById("new_checks_container"));
                    });

                    $('.dialog-buttons .gwt-Button').on('click', function() {
                        $('#checks_lodaer_containers').show();
                        location.reload();
                    });
                    $('.gwt-DialogBox img').on('click', function() {
                        $('#checks_lodaer_containers').show();
                        location.reload();
                    });
                    $('#start_checks_lodaer_containers').hide();
                } else {
                    $('#new_checks_container').append(reqBodyJson.curl);
                    $('#start_checks_lodaer_containers').hide();
                    return null;

                    ///$('.checks_container').show();
                }
            } else {
                if(!reqBodyJson.curl) {
                    // $('#checks_container').append('<b style="color: orange;">Допустимые ошибки :</b><br>');
                    // $('#checks_container').append(reqBodyJson.step_1_errors_ok);
                    ///$('#checks_container').append(reqBodyJson.step_3_errors_ok);
                    ///$('.checks_container').show();
                    $('#extra_buttons_container #extra_operations_button').attr('href', `${main_dir}/index.php?data_id=${response}`);
                    $('#extra_buttons_container #get_reciepe_button').attr('href', `${main_dir}/check_sheet_parts.php?data_id=${response}`);
                    $('#extra_buttons_container').show();
                    $('#mpr_import_button').show();
                    $('#bottom_container').hide();

                    if(button == 'receipt' || button == 'extra' || button == 'mpr') {
                        let new_dialog_window = `
                        
                        <div class="gwt-DialogBox" style="left: 30%; top: 173px; z-index: 2; visibility: visible; position: absolute; overflow: visible;"><div class=""><table cellspacing="0" cellpadding="0" class=""><tbody><tr class="dialogTop"><td class="dialogTopLeft"><div class="dialogTopLeftInner"></div></td><td class="dialogTopCenter"><div class="dialogTopCenterInner"><table cellpadding="0" cellspacing="0" class="tool-panel CaptionTool" style="width: 100%; height: 100%;"><colgroup><col></colgroup><tbody><tr><td width="75%"><div class="tool-panel-label" style="display: none;">Найдены проблемы</div></td><td width="5%"><table cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;"><tbody><tr><td align="left" style="vertical-align: middle;"><table cellspacing="0" cellpadding="0" class="xAnchor" title="Отмена" tabindex="0"><tbody><tr><td class="img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVR42n2TXUxbZRjHHzYKqzRhTEWn60azUUH3EQLrkM0IGRgDDqLRbTEkemGy7GJ6YTKv/Eq828eFLoNgFsmiRmWfN7gtGjTGsIQsbNCMIgzK2uI4pz3tKZx+gNvPd4du9mY+yS85yTn/3/u873leSVXKB+m3no/Md/o0c3+dFt1Xo+ntW7S/26q12WavFmz0aFO7NmgTvmc0f025dq1qdeTHUjksImsUhZLZtzVO8Ar0dkLPXsLvNsAXLXD4ZTjog7eryTavJdWynlijm4l6NwNuZ1yF22zJwjsNFn2H4P0yRn1l+C//Sqi2CFoLYZdg1TmwJm+S3eli1i2MPitcLhNLhT9WeMTsrLf49gAj20sJ+AOEYgbhwDiJ2gJS9UVYhkEWVUuLpF9dx3C50L/aFhxVVIqxf7sV/aiDoe4epvQYoUiEuYSJMTbKQmjaDt8jV1PXCWxcwXmXLThmC+Ze32ZxfC96jTB5qZ+w6kDTdRLz86SyWe7ey8W1ILHaYgYfF34qsQXHFV6Zfe0Fi8/3QJuD1GbhzpUL6PEEpmmSTqeXV5/2E9tSQGCt8OeTwnfOPMFMS7XFh83wkpDZ4eDOzRF0te/7gqzqwBbENeKvuLmxRhhQ9BbnCW41bbR4bwfpupUEx8aI6FH0aAxzYYH04tJ/W7ibIfrmVn5eJZwqzBP89WKFxRtV3D7Tyy3NIBQOMxc3mZ/0kxkfZpG8ik1x9blSuiRP4Pe5rXRHNZkqITz4G8GYSXR6HHNnKXqdi3/02yzdD2fUvO2u4Gyh0FWQJ7i+7WnLaPUyWyEkax3Ef7+A2VRuH9jIE0KkcR3MDDPT7OFSsdDnfChY/o2DlWWJYNMmxtarKVOhCa+DIRW8qgbmD3Xiv5QIgx4nF4uEc48Jp1UHX4kkHg7SKZd81v/UKuOcS5J9JZL83inJ08WS/MYhya9XSrJbcVIUBZI8ofhSxGgS+UGFP7VHOXerOhSfKI7kzP/Hkdy37bmsupLLDx67pWW8j+DBe8+D6/wvB2EUbgHsxzMAAAAASUVORK5CYII=" style="border:none;margin:0;padding:0;"></td><td class="text"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div></td><td class="dialogTopRight"><div class="dialogTopRightInner"></div></td></tr><tr class="dialogMiddle"><td class="dialogMiddleLeft"><div class="dialogMiddleLeftInner"></div></td><td class="dialogMiddleCenter"><div class="dialogMiddleCenterInner dialogContent"><div style="position: relative; width: 800px; height: 450px;"><div aria-hidden="true" style="position: absolute; z-index: -32767; top: -20ex; width: 10em; height: 10ex; visibility: hidden;">&nbsp;</div><div style="position: absolute; overflow: hidden; left: 0px; right: 0px; bottom: 0px; height: 50px;"><div class="dialog-buttons" style="width: 100%; position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;"><table cellspacing="0" cellpadding="0"><tbody><tr><td align="left" style="vertical-align: top;"><button type="button" class="gwt-Button" style="width: 100px;">Ок</button></td><td align="left" style="vertical-align: top;"><button type="button" class="gwt-Button" aria-hidden="true" style="width: 100px; display: none;">Отмена</button></td></tr></tbody></table></div></div><div style="position: absolute; overflow: hidden; left: 0px; top: 0px; right: 0px; bottom: 50px;"><div class="gwt-HTML" style="overflow-y: auto; width: 800px; height: 400px; position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;">
                             <div id="new_checks_container">
                                <b style="color: orange;">Допустимые ошибки :</b><br>${reqBodyJson.step_1_errors_ok}
                             </div>
                            <button id="button_copy_buffer_clipboard" style="display: inline-block;">Скопировать текст</button>
                        </div></div></div></div></td><td class="dialogMiddleRight"><div class="dialogMiddleRightInner"></div></td></tr><tr class="dialogBottom"><td class="dialogBottomLeft"><div class="dialogBottomLeftInner"></div></td><td class="dialogBottomCenter"><div class="dialogBottomCenterInner"></div></td><td class="dialogBottomRight"><div class="dialogBottomRightInner"></div></td></tr></tbody></table></div></div>
                        
                        `;
                        $('body').append(new_dialog_window);
                        $('#start_checks_lodaer_containers').hide();
                        $('.dialog-buttons .gwt-Button').on('click', function() {
                            if(button == 'receipt') window.location.href = `${main_dir}/check_sheet_parts.php?data_id=${response}`;
                            if(button == 'extra') window.location.href = `${main_dir}/index.php?data_id=${response}`;
                        });
                        $('.gwt-DialogBox img').on('click', function() {
                            if(button == 'receipt') window.location.href = `${main_dir}/check_sheet_parts.php?data_id=${response}`;
                            if(button == 'extra') window.location.href = `${main_dir}/index.php?data_id=${response}`;
                        });
                        document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                            copyToClipboard(document.getElementById("new_checks_container"));
                        });
                        // $('.header-panel-toolBar > tbody > tr > td:first-child > table tr td.text').click();
                    } else {
                        $('#new_checks_container').html('');
                        $('#new_checks_container').append('<b style="color: orange;">Допустимые ошибки :</b><br>' + reqBodyJson.step_1_errors_ok);
                        ///$('#new_checks_container').append('<button id="button_copy_buffer_clipboard">Скопировать текст</button>');

                        document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                            copyToClipboard(document.getElementById("new_checks_container"));
                        });
                        $('#button_copy_buffer_clipboard').show();

                        $('.dialog-buttons .gwt-Button').on('click', function() {
                            $('#checks_lodaer_containers').show();
                            location.reload();
                        });
                        $('.gwt-DialogBox img').on('click', function() {
                            $('#checks_lodaer_containers').show();
                            location.reload();
                        });
                        $('#start_checks_lodaer_containers').hide();
                    }






                } else {
                    // $('#checks_container').append(reqBodyJson.curl);
                    // $('.checks_container').show();

                    $('#new_checks_container').append(reqBodyJson.curl);
                }
            }

        } else {
            // $('#checks_container').append('<b style="color: red;">Критичиские ошибки :</b><br>');
            // $('#checks_container').append(reqBodyJson.step_1_errors);
            // $('#checks_container').append(reqBodyJson.step_3_errors);
            // $('.checks_container').show();
            if(button == 'receipt' || button == 'extra' || button == 'mpr') {
                let light_error = '';
                if(reqBodyJson.step_1_errors_ok) {
                    light_error = '<b style="color: orange;">Допустимые ошибки :</b><br>' + reqBodyJson.step_1_errors_ok;
                }
                let new_dialog_window = `
                        
                        <div class="gwt-DialogBox" style="left: 11px; top: 173px; z-index: 2; visibility: visible; position: absolute; overflow: visible;"><div class=""><table cellspacing="0" cellpadding="0" class=""><tbody><tr class="dialogTop"><td class="dialogTopLeft"><div class="dialogTopLeftInner"></div></td><td class="dialogTopCenter"><div class="dialogTopCenterInner"><table cellpadding="0" cellspacing="0" class="tool-panel CaptionTool" style="width: 100%; height: 100%;"><colgroup><col></colgroup><tbody><tr><td width="75%"><div class="tool-panel-label" style="display: none;">Найдены проблемы</div></td><td width="5%"><table cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;"><tbody><tr><td align="left" style="vertical-align: middle;"><table cellspacing="0" cellpadding="0" class="xAnchor" title="Отмена" tabindex="0"><tbody><tr><td class="img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVR42n2TXUxbZRjHHzYKqzRhTEWn60azUUH3EQLrkM0IGRgDDqLRbTEkemGy7GJ6YTKv/Eq828eFLoNgFsmiRmWfN7gtGjTGsIQsbNCMIgzK2uI4pz3tKZx+gNvPd4du9mY+yS85yTn/3/u873leSVXKB+m3no/Md/o0c3+dFt1Xo+ntW7S/26q12WavFmz0aFO7NmgTvmc0f025dq1qdeTHUjksImsUhZLZtzVO8Ar0dkLPXsLvNsAXLXD4ZTjog7eryTavJdWynlijm4l6NwNuZ1yF22zJwjsNFn2H4P0yRn1l+C//Sqi2CFoLYZdg1TmwJm+S3eli1i2MPitcLhNLhT9WeMTsrLf49gAj20sJ+AOEYgbhwDiJ2gJS9UVYhkEWVUuLpF9dx3C50L/aFhxVVIqxf7sV/aiDoe4epvQYoUiEuYSJMTbKQmjaDt8jV1PXCWxcwXmXLThmC+Ze32ZxfC96jTB5qZ+w6kDTdRLz86SyWe7ey8W1ILHaYgYfF34qsQXHFV6Zfe0Fi8/3QJuD1GbhzpUL6PEEpmmSTqeXV5/2E9tSQGCt8OeTwnfOPMFMS7XFh83wkpDZ4eDOzRF0te/7gqzqwBbENeKvuLmxRhhQ9BbnCW41bbR4bwfpupUEx8aI6FH0aAxzYYH04tJ/W7ibIfrmVn5eJZwqzBP89WKFxRtV3D7Tyy3NIBQOMxc3mZ/0kxkfZpG8ik1x9blSuiRP4Pe5rXRHNZkqITz4G8GYSXR6HHNnKXqdi3/02yzdD2fUvO2u4Gyh0FWQJ7i+7WnLaPUyWyEkax3Ef7+A2VRuH9jIE0KkcR3MDDPT7OFSsdDnfChY/o2DlWWJYNMmxtarKVOhCa+DIRW8qgbmD3Xiv5QIgx4nF4uEc48Jp1UHX4kkHg7SKZd81v/UKuOcS5J9JZL83inJ08WS/MYhya9XSrJbcVIUBZI8ofhSxGgS+UGFP7VHOXerOhSfKI7kzP/Hkdy37bmsupLLDx67pWW8j+DBe8+D6/wvB2EUbgHsxzMAAAAASUVORK5CYII=" style="border:none;margin:0;padding:0;"></td><td class="text"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div></td><td class="dialogTopRight"><div class="dialogTopRightInner"></div></td></tr><tr class="dialogMiddle"><td class="dialogMiddleLeft"><div class="dialogMiddleLeftInner"></div></td><td class="dialogMiddleCenter"><div class="dialogMiddleCenterInner dialogContent"><div style="position: relative; width: 800px; height: 450px;"><div aria-hidden="true" style="position: absolute; z-index: -32767; top: -20ex; width: 10em; height: 10ex; visibility: hidden;">&nbsp;</div><div style="position: absolute; overflow: hidden; left: 0px; right: 0px; bottom: 0px; height: 50px;"><div class="dialog-buttons" style="width: 100%; position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;"><table cellspacing="0" cellpadding="0"><tbody><tr><td align="left" style="vertical-align: top;"><button type="button" class="gwt-Button" style="width: 100px;">Ок</button></td><td align="left" style="vertical-align: top;"><button type="button" class="gwt-Button" aria-hidden="true" style="width: 100px; display: none;">Отмена</button></td></tr></tbody></table></div></div><div style="position: absolute; overflow: hidden; left: 0px; top: 0px; right: 0px; bottom: 50px;"><div class="gwt-HTML" style="overflow-y: auto; width: 800px; height: 400px; position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px;">
                             <div id="new_checks_container">
                                <b style="color: red;">Критичиские ошибки :</b><br> ${reqBodyJson.step_1_errors}
                                ${light_error}
                             </div>
                            <button id="button_copy_buffer_clipboard" style="display: inline-block;">Скопировать текст</button>
                        </div></div></div></div></td><td class="dialogMiddleRight"><div class="dialogMiddleRightInner"></div></td></tr><tr class="dialogBottom"><td class="dialogBottomLeft"><div class="dialogBottomLeftInner"></div></td><td class="dialogBottomCenter"><div class="dialogBottomCenterInner"></div></td><td class="dialogBottomRight"><div class="dialogBottomRightInner"></div></td></tr></tbody></table></div></div>
                        
                        `;
                $('body').append(new_dialog_window);
                $('#start_checks_lodaer_containers').hide();
                $('.dialog-buttons .gwt-Button').on('click', function() {
                    location.reload();
                });
                $('.gwt-DialogBox img').on('click', function() {
                    location.reload();
                });
                document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                    copyToClipboard(document.getElementById("new_checks_container"));
                });
                // $('.header-panel-toolBar > tbody > tr > td:first-child > table tr td.text').click();
            } else {
                $('#new_checks_container').append('<b style="color: red;">Критичиские ошибки :</b><br>' + reqBodyJson.step_1_errors);
                if(reqBodyJson.step_1_errors_ok) {
                    $('#new_checks_container').append('<b style="color: orange;">Допустимые ошибки :</b><br>' + reqBodyJson.step_1_errors_ok);
                }
                ///$('#new_checks_container').append('<button id="button_copy_buffer_clipboard">Скопировать текст</button>' + reqBodyJson.step_1_errors);

                document.getElementById("button_copy_buffer_clipboard").addEventListener("click", function() {
                    copyToClipboard(document.getElementById("new_checks_container"));
                });
                $('#button_copy_buffer_clipboard').show();
            }


        }





        $('#checks_container').append('<button id="close_check_container">ОК</button>');
        $('#close_check_container').on('click', function () {
            $('.checks_container').hide();
        });
    }

    $('#start_checks_lodaer_containers').hide();
    $('#checks_lodaer_containers').hide();
};

var checked_proj = false;
