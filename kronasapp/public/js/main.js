//.. 04.03.2021 Устанавливаем филиал в скрытый input, а также переустанавливаем значение при смене филиала
$(document).ready(function () {
    let place_field = $('#user_place'), hidden_input_place = $('#current_place');
    hidden_input_place.val(place_field.val());

    place_field.on('change', function() {
        hidden_input_place.val(place_field.val());
    });

    //.. 06.09.2022 Тоценко А. Параметр, есть ли раскрой при загрузке .kronas файла
    if (user_sheet_cutting) {
        const user_sheet_cutting_block = document.querySelector('.user_sheet_cutting_block');
        setTimeout(() => {
            user_sheet_cutting_block.classList.add('popup-message-block-show');
        }, 3000);
        $('#user_sheet_cutting').on('click', async function (e) {

            fetch('/reset_user_sheet_cutting', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
            })
            .then( (res) => {

                    user_sheet_cutting_block.classList.remove('popup-message-block-show');
                    user_sheet_cutting = false;
            })
            console.log('user_sheet_cutting - ', user_sheet_cutting)
        })
    }

    $('#user_type_edge').on('change', function(){
        $('#user_type_edge2').val($(this).val());
        $('#user_type_edge3').val($(this).val());
    })
});

$('#auth_form').on('submit', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    fetch('/auth/?login=' + $('#login_input').val() + '&password=' + $('#password_input').val(), {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
    })
        .then( (res) => res.text() )
        .then( (body) => {
            let parse_body = JSON.parse(body);
            $('#message').html(parse_body.info);
            if(parse_body.is_manager > 0) {
                // window.location.href = main_dir + '/manager_all_order.php?manager_id=' + parse_body.is_manager;
                //.. Идём на login.php (параметр giblabsession говорит, что идём с Гиблаб)
                // передаём только ID менеджера, а массив по менеджеру создаём уже по месту
                window.location.href = main_dir + '/login.php?manager_id=' + parse_body.is_manager + '&giblabsession';
            } else if(parse_body.info !== 'Данные авторизации не верны') {
                location.reload();
            }
        } );
});

$('#auth_client').on('submit', async function (e) {
    e.preventDefault();
    async function client_check_phone(phone) {
        let request = await fetch('/auth_client_check/?phone=' + phone);
        let body = await request.text();
        return body;
    }
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    if($('#password_client').val()) {
        $('#message_client').html(' ');
        //.. в условии поддержка локального сервера
        if(client_auth_pin == $('#password_client').val() || $('#password_client').val() == '947KHpHVKcz9Kj7s4eJ5t47kM9z5RZbe') {
            fetch('/auth_client/?telephone_client=' + $('#telephone_client').val(), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
            })
                .then( (res) => res.text() )
                .then( (body) => {
                    // $('#message_client').html(body);
                    if(body !== 'Данные авторизации не верны' && body !== 'Поля заполнены некорректно!') {
                        // location.reload();
                        //..
                        location.href = main_dir + '/clients/login.php?' + body;
                    }
                });
        } else {
            $('#message_client').html('Пин-код неверный!');
        }
    } else if($('#telephone_client').val()) {
        $('#message_client').html(' ');
        let cheks_phone = await client_check_phone($('#telephone_client').val());
        console.log(cheks_phone);
        if(cheks_phone == 1) {
            console.log('ddddddddddd');
            let get_pin_code = await fetch('/send_client_pin_code?client=' + $('#telephone_client').val());
            let pin_code = await get_pin_code.text();
            client_auth_pin = pin_code / Math.pow(3, 15);
            $('.telephone_client_auth').hide();
            $('.pin_client_auth').show();
        } else {
            $('#message_client').html('<p class="red bold">Указан неверный номер телефона</p>');
        }

    } else {
        $('#message_client').html('Укажите номер телефона!');
    }



    // fetch('/auth_client/?login_client=' + $('#login_client').val() + '&telephone_client=' + $('#telephone_client').val() + '&password_client=' + $('#password_client').val(), {

});


// $('#auth_client').on('submit', async function (e) {
//     e.preventDefault();
//     let get_pin_code = await fetch('/send_client_pin_code?client=' + client_auth_phone);
//     let pin_code = await get_pin_code.text();
//     client_auth_pin = pin_code;
//
//     await window.saveFromJS();
//     await save_xml_project($('#saveContainer').val());
//     // fetch('/auth_client/?login_client=' + $('#login_client').val() + '&telephone_client=' + $('#telephone_client').val() + '&password_client=' + $('#password_client').val(), {
//     fetch('/auth_client/?telephone_client=' + $('#telephone_client').val(), {
//         method: 'GET',
//         headers: {
//             'Content-Type': 'application/json;charset=utf-8'
//         },
//     })
//         .then( (res) => res.text() )
//         .then( (body) => {
//             $('#message_client').html(body);
//             if(body !== 'Данные авторизации не верны' && body !== 'Поля заполнены некорректно!') {
//                 location.reload();
//             }
//         } );
// });


$('#get_password_form').on('submit', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());

    let contact = '';
    let resend_email = $('#get_password_input').val();
    let resend_phone = $('#get_password_phone_input').val();

    if(resend_email && resend_email.length > 0 && resend_email !== '') {
        contact = 'getter_email=' + resend_email;
        console.log(contact);
    } else if(resend_phone && resend_phone.length > 0 && resend_phone !== '') {
        contact = 'getter_phone=' + resend_phone;
        console.log(contact);
    }


    fetch('/auth_get_password/?' + contact, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
    })
        .then( (res) => res.text() )
        .then( (body) => {
            $('#message_password').html(body);
            $('#get_password_form button').hide();
            ///if(body == 'Данный е-мейл отсутствует в базе. Наш менеджер свяжется с Вами в ближайшее время') $('#get_password_form button').attr('disabled','true');
            // if(body !== '_________' && body !== '_________') {
            //     location.reload();
            // }
        } );
});

$('#sep_project_form_button').on('click', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    $('#sep_project_data').val($('#saveContainer').val());
    $('#sep_project_form').submit();
});
$('#sep_ticket_form_button').on('click', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    $('#ticket_project_data').val($('#saveContainer').val());
    $('#sep_ticket_form').submit();
});
$('#autorize_manager_open').on('click', async function () {
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    // $('.auth_form_container').show();

    $('.auth_button_manager').addClass('active');
    $('.auth_button_client').hide();
    $('.auth_button_manager').show();
    $('#auth_form').show(200);
    $('#auth_client').hide();
    $('#get_password_form').hide();
    $('.auth_form_container').show(200);
});
$('#autorize_client_open').on('click', async function () {
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    // $('.auth_form_container').show();

    $('.auth_button_manager').hide();
    $('.auth_button_client').show();
    $('.auth_button_client').addClass('active');
    $('#auth_form').hide();
    $('#auth_client').show(200);
    $('#get_password_form').hide();
    $('.auth_form_container').show(200);
});
$(document).mouseup(function (e){ // событие клика по веб-документу
    let $this = $('.auth_form_container');
    if (!$this.is(e.target) && $this.has(e.target).length === 0) { // если клик был не по кнопке меню или его дочерним элементам
        if ($this.css('display') != 'none') {
            $this.hide(200);
        }
    }
});
$('.close_button a').on('click', function () {
    $('.auth_form_container').hide(200);
    $('.pin_client_auth').hide();
    $('.telephone_client_auth').show();
    $('#message_client').html('   ');
})



$('#order_comment_form').on('submit', function (e) {
    e.preventDefault();
    fetch('/comment', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': new_csrf_token,
            'X-XSRF-TOKEN': new_csrf_token,
        },
        body: JSON.stringify({
            comment: $('#order_comment_input').val()
        })
    });
    $('.comment_container').removeClass('open');
});
$('#set_comment').on('click', function () {
    $('.comment_container').toggleClass('open');
})
async function save_xml_project(project_data) {
    let request = await fetch('/project_in_session', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': new_csrf_token,
            'X-XSRF-TOKEN': new_csrf_token,
        },
        credentials: 'include',
        body: JSON.stringify({
            project_data: project_data,
            user_place: $('#user_place').val(),
            user_type_edge: $('#user_type_edge').val()
        })
    });
    let body = await request.text();
    return body;
    console.log('/project_in_session - отправка')
    ///.then( res => res.text())
    ///.then( body => console.log(body));
}
async function save_xml_project_checked(project_data) {
    let request = await fetch('/project_in_session_checked', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': new_csrf_token,
            'X-XSRF-TOKEN': new_csrf_token,
        },
        credentials: 'include',
        body: JSON.stringify({
            project_data_checked: project_data,
            user_place: $('#user_place').val(),
            user_type_edge: $('#user_type_edge').val()
        })
    });
    let body = await request.text();
    console.log('/project_in_session - отправка')
    ///.then( res => res.text())
    ///.then( body => console.log(body));
}
function user_type_check(type) {
    // console.log(type);
    let status = '';
    let option_id = $('#user_place').val();
    let pur = $('#user_place option[value="' + option_id +'"]').attr('data-pur');
    if(pur == 1) {
        $('#user_type_edge').html('');

        if(type == 'eva') {
            status = 'selected="selected"';
        } else {
            status = '';
        }
        $('#user_type_edge').append('<option value="eva" '+ status +'>ЭВА</option>');
        
        if(type == 'pur') {
            status = 'selected="selected"';
        } else {
            status = '';
        }
        $('#user_type_edge').append('<option value="pur" '+ status +'>ПУР</option>');

        // if(type == 'laz') {
        //     status = 'selected="selected"';
        // } else {
        //     status = '';
        // }
        // $('#user_type_edge').append('<option value="laz" '+ status +'>ЛАЗ</option>');

    } else {
        $('#user_type_edge').html('');
        $('#user_type_edge').append('<option value="eva">ЭВА</option>');
    }

}





$('#user_place').on('change', () => user_type_check(null));

user_type_check(user_type_edge_session);


$('#extra_operations_button').on('click', async function(e) {
    e.preventDefault();
    $('#checks_lodaer_containers').show();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());


    async function status_check_indicator() {


        let request = await fetch('/check_edits', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': new_csrf_token,
                'X-XSRF-TOKEN': new_csrf_token,
            },
            body: JSON.stringify({
                project: $('#saveContainer').val()
            })
        });
        let body = await request.text();
        return body;
        ///return Number(body);




    };
    setTimeout(async function () {
        let status_check = await status_check_indicator();
        console.log(status_check)
        if (status_check == 1) {



            let request = await fetch('/send_project', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN':  new_csrf_token,
                    'X-XSRF-TOKEN':  new_csrf_token,
                },
                credentials: 'include',
                body: JSON.stringify({
                    _token: $('#continue_form input[name="_token"]').val(),
                    project_data: $('#continue_form input[name="project_data"]').val(),
                    user_place: $('#user_place2').val(),
                    user_type_edge: $('#user_type_edge2').val(),
                })
            });

            let response = await request.text();
            await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
            console.log('Data_id записан в сессию');




            console.log('Okay');
            $(window).off('beforeunload');
            $(window).unbind('beforeunload');
            window.onbeforeunload = null;
            window.location.href = $(e.target).attr('href');
            // $('#checks_lodaer_containers').show();
        } else if (status_check == 0) {

            let after_new_checks = await checks_this_project('extra');
            if(after_new_checks == 1) {
                let request = await fetch('/send_project', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN':  new_csrf_token,
                        'X-XSRF-TOKEN':  new_csrf_token,
                    },
                    credentials: 'include',
                    body: JSON.stringify({
                        _token: $('#continue_form input[name="_token"]').val(),
                        project_data: $('#continue_form input[name="project_data"]').val(),
                        user_place: $('#user_place2').val(),
                        user_type_edge: $('#user_type_edge2').val(),
                    })
                });

                let response = await request.text();
                await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
                console.log('Data_id записан в сессию');




                console.log('Okay');
                $(window).off('beforeunload');
                $(window).unbind('beforeunload');
                window.onbeforeunload = null;
                window.location.href = $(e.target).attr('href');
            }

            console.log('Is some changes');
            // $('#extra_buttons_container').hide();
            // $('#bottom_container').show();
            // $('#checks_container').html('В проект были внесены изменения. Необходимо провести проверку заново');
            // $('#checks_container').append('<button id="close_check_container">ОК</button>');
            // $('#close_check_container').on('click', function () {
            //     $('.checks_container').hide();
            // });
            // $('#checks_lodaer_containers').hide();
            // $('.checks_container').show();
        } else {
            console.log('Error');
        }

        ///

    }, 400);





});





$('#get_reciepe_button').on('click', async function (e) {

    e.preventDefault();
    $('#checks_lodaer_containers').show();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());



    async function status_check_indicator() {


        let request = await fetch('/check_edits', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': new_csrf_token,
                'X-XSRF-TOKEN': new_csrf_token,
            },
            body: JSON.stringify({
                project: $('#saveContainer').val()
            })
        });
        let body = await request.text();
        return body;
        ///return Number(body);




    };
    setTimeout(async function () {
        let status_check = await status_check_indicator();
        console.log(status_check)
        if (status_check == 1) {


            let request = await fetch('/send_project', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN':  new_csrf_token,
                    'X-XSRF-TOKEN':  new_csrf_token,
                },
                credentials: 'include',
                body: JSON.stringify({
                    _token: $('#continue_form input[name="_token"]').val(),
                    project_data: $('#continue_form input[name="project_data"]').val(),
                    user_place: $('#user_place2').val(),
                    user_type_edge: $('#user_type_edge2').val(),
                })
            });

            let response = await request.text();
            await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
            console.log('Data_id записан в сессию');



            console.log('Okay');
            $(window).off('beforeunload');
            $(window).unbind('beforeunload');
            window.onbeforeunload = null;
            window.location.href = $(e.target).attr('href');
        } else if (status_check == 0) {
            console.log('Is some changes');
            let after_new_checks = await checks_this_project('receipt');
            if(after_new_checks) {
                let request = await fetch('/send_project', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN':  new_csrf_token,
                        'X-XSRF-TOKEN':  new_csrf_token,
                    },
                    credentials: 'include',
                    body: JSON.stringify({
                        _token: $('#continue_form input[name="_token"]').val(),
                        project_data: $('#continue_form input[name="project_data"]').val(),
                        user_place: $('#user_place2').val(),
                        user_type_edge: $('#user_type_edge2').val(),
                    })
                });

                let response = await request.text();
                await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
                console.log('Data_id записан в сессию');



                console.log('Okay');
                $(window).off('beforeunload');
                $(window).unbind('beforeunload');
                window.onbeforeunload = null;
                window.location.href = $(e.target).attr('href');
                $('#start_checks_lodaer_containers').hide();
            }
            $('#start_checks_lodaer_containers').hide();

            // $('#extra_buttons_container').hide();
            // $('#bottom_container').show();
            // $('#checks_container').html('В проект были внесены изменения. Необходимо провести проверку заново');
            // $('#checks_container').append('<button id="close_check_container">ОК</button>');
            // $('#close_check_container').on('click', function () {
            //     $('.checks_container').hide();
            // });
            // $('#checks_lodaer_containers').hide();
            // $('.checks_container').show();
        } else {
            console.log('Error');
        }

        ///

    }, 400);


});









$(document).ready(function () {
    if(isChecked) {
        $('#bottom_container').hide();
        $('#extra_buttons_container #extra_operations_button').attr('href', `${main_dir}/index.php?data_id=${session_id}`);
        $('#extra_buttons_container #get_reciepe_button').attr('href', `${main_dir}/check_sheet_parts.php?data_id=${session_id}`);
        $('#extra_buttons_container').show();
        $('#mpr_import_button').show();
        setTimeout(() => {
            $('.checks_container').hide();
        }, 1);
    }
});

//.. 03.06.2022 Восстановление проекта после зависания
$('#recoveryProject').on('click', async function (e) {
    $.ajax({
        url: '/recovery_project',
        method: 'POST',
        async: false,
        data: {'_token':  new_csrf_token, recovery: 'recovery'},
        success: function(data) {
            if (data == 'Project recovered') {
                location.reload()
            } else alert(`Проектов для восстановления не найдено.`)
        }
    });
})

$('#user_save_project').on('click', async function (e) {
    if($(this).html() == 'Сохранить .kronas') {
        $('#checks_lodaer_containers').show();
        e.preventDefault();
        await window.saveFromJS();
        await save_xml_project($('#saveContainer').val());
        setTimeout(async function() {
            let request = await fetch('/user_save_project', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': new_csrf_token,
                    'X-XSRF-TOKEN': new_csrf_token,
                },
                body: JSON.stringify({
                    project_data: $('#saveContainer').val(),
                    user_place: $('#user_place').val(),
                    user_type_edge: $('#user_type_edge').val()
                })
            });
            let body = await request.text();
            console.log(body);
            ///window.open(body);
            let link = document.createElement('a');
            link.setAttribute('href',body);
            console.log(link);
            link.setAttribute('download','');
            $('#checks_lodaer_containers').hide();
            onload=link.click();
        }, 1500);


        // $(this).attr('href', body);
        // $(this).html('Скачать');
        // $(this).closest('button').css({
        //     background: 'green',
        //     color: 'white',
        //     padding: '4px'
        // });
        // $(this).css({color: 'white'});
    }

    ///window.open(body);
});

// --------------------------------------------------------------------------------------------------------
// --------------------------------------- Импорт файла .kronas -------------------------------------------

const $input = $('input#kronasProject'),
sendData = new Object()
$input.on('change', function(element) {
    let kronasFile = $(this).prop('files')[0]
    const reader = new FileReader()

    reader.onload = e => {
        sendData.file = e.target.result.replace(/^.*base64,/gi, '')
        sendData.name = kronasFile.name
        sendData.size = kronasFile.size
        sendData.data_id = session_id
       
       //.. 22.02.2022 Добавлен блок для визуального отображения процесса
        let Timer = setTimeout(function(){
            alert('Не удалось загрузить проект :(')
            clearTimeout(basisTimer)
        }, 30000);

        $.post("/user_upload_project", {"KronasImport": sendData, "_token": new_csrf_token}).done(function (data) {
            $input.val('')
            clearTimeout(Timer)
            if (data === 'Ok') window.location.href = '/';
            else alert('Ошибка содержимого файла .kronas');
        })
    }
    reader.readAsDataURL(kronasFile)
})

// --------------------------------------- Импорт файла .kronas -------------------------------------------
// --------------------------------------------------------------------------------------------------------


$('#user_upload_viyar').on('click', function () {
    $('#user_upload_viyar_form').toggleClass('open');
});
$('#user_upload_viyar_form button').on('click', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    await $('#viyar_user_place').val($('#user_place').val());
    await $('#viyar_project_data').val($('#saveContainer').val());
    let request = await fetch('/send_project', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN':  new_csrf_token,
            'X-XSRF-TOKEN':  new_csrf_token,
        },
        credentials: 'include',
        body: JSON.stringify({
            _token: $('#continue_form input[name="_token"]').val(),
            project_data: $('#continue_form input[name="project_data"]').val(),
            user_place: $('#continue_form input[name="user_place"]').val(),
            user_type_edge: $('#continue_form input[name="user_type_edge"]').val(),
        })
    });
    let response = await request.text();
    await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
    console.log('Data_id записан в сессию');
    $('#user_upload_viyar_form form').submit();
});
$('#mpr_import_button').on('click', async function () {
    $('#checks_lodaer_containers').show();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    if(!isChecked) {
        await checks_this_project('mpr');
        console.log('This project must be checked!');
        $('#mpr_import_form').toggleClass('open');
    } else {
        $('#mpr_import_form').toggleClass('open');
    }
    $('#checks_lodaer_containers').hide();

});
$('#mpr_error_close_button').on('click', function () {
    $('.mpr_error_container').hide();
});
$('#user_full_pdf_window_button').on('click', function () {
    $('#user_full_pdf_window').toggleClass('open');

    $('.send_pdf_button').on('click', async function(e) {
        e.preventDefault();

        await window.saveFromJS();
        await save_xml_project($('#saveContainer').val());

        let formData = new FormData();
        formData.append('project_data', $('#saveContainer').val());
        formData.append('action', 'get_all_pdf' );

        let request = await fetch(main_dir + '/pdf_send.php', {
            method: 'POST',
            headers: {
                ///'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formData
        });
        let body = await request.text();
        console.log(body);
        if(body.indexOf('/users_pdf/') > -1){
            window.open(body);
        };
    });
});




$('#admin_open_project').on('click', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());
    $('#admin_open_project_input').val( $('#saveContainer').val() );
    $('#admin_open_project_form').submit();
});


$(document).ready(function () {
    //.. 15.06.2021 Если пользователь не администратор, убираем кнопку импорта из Excel
    if (isAdmin != 1) {
        let clearExcelImport = setInterval(function(){
            if ($("div").is(".XFileUpload")) {
                // console.log('Hello!');
                $('div.XFileUpload:eq(1)').remove();
                clearInterval(clearExcelImport);
            }
        }, 500);
    }

    //.. 27.08.2021 Если пользователь не администратор, убираем поворот и отзеркаливание деталей
    // if (isAdmin != 1 && access != 10) {
    //     // Меню слева
    //     let hideElsLeft = setInterval(async function() {
    //         if ($('table.field-panel td.field-label:contains("Поворот")').length) {
    //             $('table.field-panel td.field-label:contains("Поворот")').parent().css('display', 'none')
    //         }
    //         if ($('table.field-panel td.field-label:contains("Зеркало")').length) {
    //             $('table.field-panel td.field-label:contains("Зеркало")').parent().css('display', 'none')
    //         }
    //     }, 200);

    //     // Меню сверху
    //     let hideElsTop = setInterval(async function() {
    //         if ($('div.gwt-TabLayoutPanelTabs img.gwt-Image[title="Поворот"]').length) {
    //             $('div.gwt-TabLayoutPanelTabs img.gwt-Image[title="Поворот"]').parent().css('display', 'none')
    //         }
    //         if ($('div.gwt-TabLayoutPanelTabs select.gwt-ListBox[title="Поворот"]').length) {
    //             $('div.gwt-TabLayoutPanelTabs select.gwt-ListBox[title="Поворот"]').parent().css('display', 'none')
    //         }
    //         if ($('div.gwt-TabLayoutPanelTabs table.xAnchor[title="Горизонтальное отзеркаливание"]').length) {
    //             $('div.gwt-TabLayoutPanelTabs table.xAnchor[title="Горизонтальное отзеркаливание"]').parent().css('display', 'none')
    //         }
    //         if ($('div.gwt-TabLayoutPanelTabs table.xAnchor[title="Вертикальное отзеркаливание"]').length) {
    //             $('div.gwt-TabLayoutPanelTabs table.xAnchor[title="Вертикальное отзеркаливание"]').parent().css('display', 'none')
    //         }
    //     }, 200);
    // }

    //.. 08.10.2021 Если пользователь не администратор, убираем кнопку создания XNC-операции
    if (isAdmin != 1 && access != 10) {
        let xncButtonBlock = setInterval(async function() {
            if ($('.gwt-SplitLayoutPanel .gwt-TabLayoutPanel .gwt-TabLayoutPanelTabs').length) {
                let xncButton = $('.gwt-SplitLayoutPanel .gwt-TabLayoutPanel .gwt-TabLayoutPanelTabs table.toolbar tbody tr td:eq(9) table[title="Добавить: (XNC) Обработка"]')
                if (xncButton.length) {
                    xncButton.parent().remove()
                    clearInterval(xncButtonBlock);
                }
            }
        }, 500);
    }

    // --------------------------------------------------------------------------------------------------------
    // --------------------------------------- Импорт Базис проекта -------------------------------------------

        const $input = $('input#basisProject'),
        sendData = new Object()
        $input.on('change', function(element) {
            let basisFile = $(this).prop('files')[0]
            const reader = new FileReader()

            reader.onload = e => {
                sendData.file = e.target.result.replace(/^.*base64,/gi, '')
                sendData.name = basisFile.name
                sendData.size = basisFile.size
                sendData.data_id = session_id
               
               //.. 22.02.2022 Добавлен блок для визуального отображения процесса
                $('#start_checks_lodaer_containers').show()
                let basisTimer = setTimeout(function(){
                    alert('Не удалось загрузить проект :(')
                    $('#start_checks_lodaer_containers').hide()
                    clearTimeout(basisTimer)
                }, 30000);

                $.post("/basis_import", {"BasisImport": sendData, "_token": new_csrf_token}).done(function (data) {
                    $input.val('')
                    // console.log(data)
                    clearTimeout(basisTimer)
                    $('#start_checks_lodaer_containers').hide()
                    location.reload()
                })
            }
            reader.readAsDataURL(basisFile)
        })

    // --------------------------------------- Импорт Базис проекта -------------------------------------------
    // --------------------------------------------------------------------------------------------------------

    $(document).mouseleave(function(){
        $(window).off('beforeunload');
        $(window).unbind('beforeunload');
        window.onbeforeunload = null;
    });

    // window.alert = function(){};

});

$('#send_pdf_button').on('click', async function(e) {
    e.preventDefault();

    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());

    let formData = new FormData();
    formData.append('project_data', $('#saveContainer').val());
    formData.append('action', 'get_all_pdf' );

    let request = await fetch(main_dir + '/pdf_send.php', {
        method: 'POST',
        headers: {
            ///'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formData
    });
    let body = await request.text();
    console.log(body);
    if(body.indexOf('/users_pdf/') > -1){
        window.open(body);
    };

});


$('#mpr_import_form_close').on('click', function () {
    $('#mpr_import_form').removeClass('open');
});

$('#user_upload_form_close').on('click', function () {
    $('#user_upload_form').removeClass('open');
});

$('#user_full_pdf_window_close').on('click', function () {
    $('#user_full_pdf_window').removeClass('open');
});

$('.logout_form button').on('click', async function (e) {
    e.preventDefault();
    await window.saveFromJS();
    setTimeout(async function () {
        console.log('Отправка на сохранение');
        let request = await save_xml_project($('#saveContainer').val());

        if(request == 'Project save in session') {
            console.log('Сохранено');
            $(e.target).closest('.logout_form').submit();
        }
    }, 1000);



});



$('#full_project_pdf').on('click', async function(e) {

    e.preventDefault();
    $('#checks_lodaer_containers').show();
    await window.saveFromJS();
    await save_xml_project($('#saveContainer').val());

    let formData = new FormData();
    formData.append('project_data', $('#saveContainer').val());
    formData.append('action', 'get_all_pdf_manager' );
    console.log(isAdmin);
    formData.append('is_admin', isAdmin );

    let request = await fetch(main_dir + '/pdf_send.php?data_id=' + session_id, {
        method: 'POST',
        headers: {
            ///'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formData
    });
    let body = await request.text();
    console.log(body);

    if(body.indexOf('.pdf') > -1){
        $('#checks_lodaer_containers').hide();
        window.open(main_dir + '/pdf_send.php?show=' + body);
    }
});

$(document).ready(function () {
    $('#main_bottom_container_button').on('click', async function () {
        $('.main_bottom_container').toggleClass('open');
        $(this).toggleClass('active');
        await window.saveFromJS();
        await save_xml_project($('#saveContainer').val());

    })
    $('#auth_button_manager').on('click', function () {
        $('.auth_button_manager').addClass('active');
        $('.auth_button_client').hide();
        $('.auth_button_manager').show();
        $('#auth_form').show();
        $('#auth_client').hide();
        $('#get_password_form').hide();
    });
    $('#auth_button_client').on('click', function () {
        $('.auth_button_manager').hide();
        $('.auth_button_client').show();
        $('.auth_button_client').addClass('active');
        $('#auth_form').hide();
        $('#auth_client').show();
        $('#get_password_form').hide();
    });
    $('#get_your_password').on('click', function (e) {
        e.preventDefault();
        $('.auth_form_container form').hide();
        $('#get_password_form').show();
    });
    $('#telephone_client').mask('+38(000)000-00-00', {
        placeholder: "+38(0__)-___-__-__"
    });
    $('#get_password_phone_input').mask('+38(000)000-00-00', {
        placeholder: "+38(0__)-___-__-__"
    });
    $('#client_id_search').mask('+38(000)000-00-00', {
        placeholder: "+38(0__)___-__-__"
    });

    $('#get_con_material_button').on('click', async function () {
        $('#checks_lodaer_containers').show();

        await window.saveFromJS();
        await save_xml_project($('#saveContainer').val());

        let formData = new FormData();
        formData.append('project_data', $('#saveContainer').val());
        formData.append('user_place', $('#user_place').val());
        formData.append('_token', new_csrf_token);
        //.. 03.11.2021 Ссылка перенаправлена на роут /con_material файла web.php
        let request = await fetch('/con_material', {
            method: 'POST',
            body: formData
        });

        let data = await request.json();
        console.log(data);
        let sheet = data.sheet;
        let sheet_html = ``;

        let band = data.band;
        let band_html = ``;



        var v = 0;
        var m = 0;

        for(var i in sheet) {
            let image = '';
            if(sheet[i]['pic']) {
                image = `${main_dir}/pic/${ sheet[i]['pic'][0]['FILE_NAME_BIG'] }`;
            } else {
                image = `${app_dir}/images/no-image.png`;
            }

            sheet_html += `
                <div class="main_sheet_row">
                    <div class="main_sheet_item">
                        <p><b>[/${i}/]</b> ${sheet[i]['name']} <b>(${ Math.round( (sheet[i]['price']/(sheet[i]['l']/1000 * sheet[i]['w']/1000))) } грн/м2)</b></p>
                        <img src="${image}">
                    </div>
                    <div class="main_sheet_extra">`;


            for(var y in sheet[i]) {
                if(sheet[i][y]['name']) {
                    v++;
                    let pic = `${app_dir}/images/no-image.png`;
                    let pic_large = `${app_dir}/images/no-image.png`;
                    let extra_style = 'style="display: none!important;"';
                    if(typeof sheet[i][y]['pic'] !== 'undefined') {
                        pic = `${main_dir}/pic/` + sheet[i][y]['pic'][0]['FILE_NAME_SMALL'];
                        pic_large = `${main_dir}/pic/` + sheet[i][y]['pic'][0]['FILE_NAME_BIG'];
                        extra_style = '';
                    }
                    sheet_html += `
                       <div>
                                <img src="${ pic }">  
                                <p><b>[/${y}/]</b>"${sheet[i][y]['name']}" <b>(${ Math.round((sheet[i][y]['price'])) } грн/м)</b></p>
                                <div class="extra_large_image" ${extra_style}><img src="${ pic_large }"></div>
                                
                    `;

                    if(sheet[i][y]['in'] === 1) {
                        sheet_html += '<span style="color: green;">В проекте</span>';
                    } else {
                        sheet_html += `<input type="checkbox" name="band_in[]" value="${y}">`;
                    }

                    sheet_html += '</div>';
                }
            }
            sheet_html += '</div></div>';

        }

        for(var i in band) {
            let image = '';
            if(band[i]['pic']) {
                image = `${main_dir}/pic/${ band[i]['pic'][0]['FILE_NAME_BIG'] }`;
            } else {
                image = `${app_dir}/images/no-image.png`;
            }

            band_html += `
                <div class="main_sheet_row">
                    <div class="main_sheet_item">
                        <p><b>[/${i}/]</b> ${band[i]['name']} <b>( ${ Math.round(band[i]['price'])} грн/м)</b></p>
                        <img src="${image}">
                    </div>
                    <div class="main_sheet_extra">`

            for(var y in band[i]) {
                if( band[i][y]['name']) {
                    m++;
                    let pic_band = `${app_dir}/images/no-image.png`;
                    let pic_band_large = `${app_dir}/images/no-image.png`;
                    let extra_style = 'style="display: none!important;"';
                    if(typeof band[i][y]['pic'] !== 'undefined') {
                        pic_band = `${main_dir}/pic/` + band[i][y]['pic'][0]['FILE_NAME_SMALL'];
                        pic_band_large = `${main_dir}/pic/` + band[i][y]['pic'][0]['FILE_NAME_BIG'];
                        extra_style = '';
                    }
                    band_html += `<div>
                        <img src="${ pic_band }">
                        <p><b>[/${y}/]</b> ${ band[i][y]['name'] } <b>( ${ Math.round(band[i][y]['price']/(band[i][y]['l']/1000*band[i][y]['w']/1000)) } грн/м2)</b></p>
                        <div class="extra_large_image" ${extra_style}><img src="${ pic_band_large }"></div>          
                    `;
                    if(band[i][y]['in'] === 1) {
                        band_html += `<span style="color: green;">В проекте</span>`;
                    } else {
                        band_html += `<input type="checkbox" name="sheet_in[]" value="${y}">`
                    }
                    band_html += '</div>';
                }

            }
            band_html += '</div></div>';
        }


        let html_form = `
            <button id="get_con_material_close">&#215;</button>
            <form action="${main_dir + '/con_material.php'}" method="POST" id="get_con_material_form">
                <div class="con_material_navigate">
                    <button class="active">Подбор кромок</button>
                    <button>Подбор материалов </button>
                </div>
                <div class="con_material_content open">
                    ${sheet_html}
                </div>
                <div class="con_material_content">
                    ${band_html}
                </div>
                <input type="hidden" name="project_data" id="project_data_con_material">
                <input type="hidden" name="user_place" value="${ $('#user_place').val() }">
                <input type="hidden" name="this_data_idz" value="${ session_id }">
                <input type="submit" class="con_material_input_button" value="Добавить">
            </form>
        
        `;

        $('#get_con_material').html(html_form);


        $('input[name="sheet_in[]"]').on('change', function () {
            let sheets = $('input[name="sheet_in[]"]');
            let bands = $('input[name="band_in[]"]');
            let sheet_add = false;
            let band_add = false;
            $.each(sheets,(index, element) => {
                if($(element).prop('checked')) {
                    sheet_add = true;
                }
            });
            $.each(bands,(index, element) => {
                if($(element).prop('checked')) {
                    band_add = true;
                }
            });
            if(band_add || sheet_add ) {
                $('.con_material_input_button').show();
            } else {
                $('.con_material_input_button').hide();
            }

        });
        $('input[name="band_in[]"]').on('change', function () {
            let sheets = $('input[name="sheet_in[]"]');
            let bands = $('input[name="band_in[]"]');
            let sheet_add = false;
            let band_add = false;
            $.each(sheets,(index, element) => {
                if($(element).prop('checked')) {
                    sheet_add = true;
                }
            });
            $.each(bands,(index, element) => {
                if($(element).prop('checked')) {
                    band_add = true;
                }
            });
            if(band_add || sheet_add ) {
                $('.con_material_input_button').show();
            } else {
                $('.con_material_input_button').hide();
            }
        });

        $('#get_con_material_close').on('click', function () {
            $('#get_con_material').hide();
        });

        $('#project_data_con_material').val( $('#saveContainer').val() );

        $('#get_con_material_form').on('submit', function (e) {
            // console.log('dddd');
            // e.preventDefault();
            // console.log( $( this ).serializeArray() );
        });

        $('.con_material_navigate button').on('click', function (e) {
            e.preventDefault();
            $('.con_material_content').toggleClass('open');
            $('.con_material_navigate button').toggleClass('active');
        });




        $('#get_con_material').show();
        $('#checks_lodaer_containers').hide();
    });

    $('.main_bottom_container button').on('click', function(){
        $('.main_bottom_container').toggleClass('open');
        $('#main_bottom_container_button').toggleClass('active');
    });

    // $(document).mouseup(function (e) {
    //     var container = $(".main_bottom_container");
    //     var button = $("#main_bottom_container_button");
    //     if (container.has(e.target).length === 0 && button.has(e.target).length === 0){
    //         container.hide();
    //     }
    // });



});

function escape_z(string) {
    var htmlEscapes = {
        '&': '″',
        '<': '″',
        '>': '″',
        '"': '″',
        "'": '″'
    };

    return string.replace(/[&<>"']/g, function(match) {
        return htmlEscapes[match];
    });
};

$(document).ready(function () {

    var client_auth_phone = 0;
    var client_auth_pin = 0;

    $('#client_id_search').on('change', async function (e) {
        $('.client_search_form button').hide();
        let container = $('#client_variants_container');
        let val = e.target.value;
        client_auth_phone = val;
        let request = await fetch(app_dir + '/get_client_data?tel=' + val);
        let body = await request.json();
        if(body.length > 0) {
            container.html(' ');
            body.forEach(element => {
                container.append(`
                    <div onclick="{ $('#client_id').val(${element.client_id}); $('#client_id_search').val('${escape_z(element.name)}'); $('#client_variants_container').hide(); $('.client_search_form button').show()}">
                        <span>${element.name}</span>
                    </div>
                    
                `);
            });
            container.show();
        }
    });
    var send_client_material = {};
    $('#client_materials').on('click', async function () {
        $('#checks_lodaer_containers').show();
        $('#client_material_checked_form button').hide();
        let container = $('#get_client_material');
        let client_items = $('#get_client_material_inner table tbody');
        let request = await fetch( app_dir + '/get_client_materials' );
        let body = await request.json();
        send_client_material = body;
        // let html = `
        //     <button id="get_client_material_close">&#215;</button>
        // `;
        client_items.html('');
        console.log(body);
        body.forEach(element => {

            //.. 16.12.2022 Тоценко.
            // Добавлен блок отображения повреждений материала
            if (element.crash !== 1) {
                element.notation = 'Материал без повреждений';
            }

            let input = '<input type="checkbox" value="1" name="' + element.code + '">';
            if( $('#user_place').val() != element.place_id ) {
                input = '';
            };
            let item = `
                <tr>
                    <td>${element.code}</td>
                    <td>${element.place}</td>
                    <td>${element.name}</td>
                    <td>${element.client} <br>[ ${element.client_phone} ]</td>
                    <td>${element.notation}</td>
                    <td>
                        ${input}
                    </td>
                </tr>
            `;
            client_items.append(item);
            // <td>${element.W}</td>
            // <td>${element.L}</td>
            // <td>${element.T}</td>
            // <td>${element.count}</td>
        });

        container.show();
        $('#client_material_checked_form td input').on('change', function () {
            let inputs = $('#client_material_checked_form td input');
            console.log(inputs);
            let indic = false;
            [...inputs].forEach(element => {
                if( $(element).val() > 0 ) {
                    indic = true
                }
                if(indic) {
                    $('#client_material_checked_form button').show();
                } else {
                    $('#client_material_checked_form button').hide();
                }
            });
        });
        $('#checks_lodaer_containers').hide();

    });
    $('#client_materials_menu').on('click', async function () {
        $('#checks_lodaer_containers').show();
        $('#client_material_checked_form button').hide();
        let container = $('#get_client_material');
        let client_items = $('#get_client_material_inner table tbody');
        let request = await fetch( app_dir + '/get_client_materials' );
        let body = await request.json();
        send_client_material = body;
        // let html = `
        //     <button id="get_client_material_close">&#215;</button>
        // `;
        client_items.html('');
        console.log(body);
        body.forEach(element => {

            //.. 16.12.2022 Тоценко.
            // Добавлен блок отображения повреждений материала
            if (element.crash !== 1) {
                element.notation = 'Материал без повреждений';
            }
            
            let input = '<input type="checkbox" value="1" name="' + element.code + '">';
            if( $('#user_place').val() != element.place_id ) {
                input = '';
            };
            let item = `
                <tr>
                    <td>${element.code}</td>
                    <td>${element.place}</td>
                    <td>${element.name}</td>
                    <td>${element.client} <br>[ ${element.client_phone} ]</td>
                    <td>${element.notation}</td>
                    <td>
                        ${input}
                    </td>
                </tr>
            `;
            client_items.append(item);
            // <td>${element.W}</td>
            // <td>${element.L}</td>
            // <td>${element.T}</td>
            // <td>${element.count}</td>`;
        });

        container.show();
        $('#client_material_checked_form td input').on('change', function () {
            let inputs = $('#client_material_checked_form td input');
            console.log(inputs);
            let indic = false;
            [...inputs].forEach(element => {
                if( $(element).val() > 0 ) {
                    indic = true
                }
                if(indic) {
                    $('#client_material_checked_form button').show();
                } else {
                    $('#client_material_checked_form button').hide();
                }
            });
        });
        $('#checks_lodaer_containers').hide();

    });
    $('.client_materials_filters input').on('change', async function () {
        $('#client_material_checked_form button').hide();
        let filters = $('#client_materials_filters_form').serialize();
        console.log(filters);
        let container = $('#get_client_material');
        let client_items = $('#get_client_material_inner table tbody');
        let request = await fetch( app_dir + '/get_client_materials?' + filters );
        let body = await request.json();
        send_client_material = body;
        // let html = `
        //     <button id="get_client_material_close">&#215;</button>
        // `;
        client_items.html('');
        console.log(body);
        body.forEach(element => {

            //.. 16.12.2022 Тоценко.
            // Добавлен блок отображения повреждений материала
            if (element.crash !== 1) {
                element.notation = 'Материал без повреждений';
            }

            let input = '<input type="checkbox" value="1" name="' + element.code + '">';
            if( $('#user_place').val() != element.place_id ) {
                input = '';
            };
            let item = `
                <tr>
                    <td>${element.code}</td>
                    <td>${element.place}</td>
                    <td>${element.name}</td>
                    <td>${element.client} <br>[ ${element.client_phone} ]</td>
                    <td>${element.notation}</td>
                    <td>
                        ${input}
                    </td>
                </tr>
            `;
            // client_items.append(item);
            // <td>${element.W}</td>
            // <td>${element.L}</td>
            // <td>${element.T}</td>
            // <td>${element.count}</td>`;
            client_items.append(item);
        });

        container.show();
        $('#client_material_checked_form td input').on('change', function () {
            let inputs = $('#client_material_checked_form td input');
            console.log(inputs);
            let indic = false;
            [...inputs].forEach(element => {
                if( $(element).val() > 0 ) {
                    indic = true
                }
                if(indic) {
                    $('#client_material_checked_form button').show();
                } else {
                    $('#client_material_checked_form button').hide();
                }
            });
        });
    });
    $('.client_materials_filters select').on('change', async function () {
        $('#client_material_checked_form button').hide();
        let filters = $('#client_materials_filters_form').serialize();
        console.log(filters);
        let container = $('#get_client_material');
        let client_items = $('#get_client_material_inner table tbody');
        let request = await fetch( app_dir + '/get_client_materials?' + filters );
        let body = await request.json();
        send_client_material = body;
        // let html = `
        //     <button id="get_client_material_close">&#215;</button>
        // `;
        client_items.html('');
        console.log(body);
        body.forEach(element => {

            //.. 16.12.2022 Тоценко.
            // Добавлен блок отображения повреждений материала
            if (element.crash !== 1) {
                element.notation = 'Материал без повреждений';
            }
            
            let input = '<input type="checkbox" value="1" name="' + element.code + '">';
            if( $('#user_place').val() != element.place_id ) {
                input = '';
            };
            let item = `
                <tr>
                    <td>${element.code}</td>
                    <td>${element.place}</td>
                    <td>${element.name}</td>
                    <td>${element.client} <br>[ ${element.client_phone} ]</td>
                    <td>${element.notation}</td>
                    <td>
                        ${input}
                    </td>
                </tr>
            `;
            // client_items.append(item);
            // <td>${element.W}</td>
            // <td>${element.L}</td>
            // <td>${element.T}</td>
            // <td>${element.count}</td>`;
            client_items.append(item);
        });

        container.show();
        $('#client_material_checked_form td input').on('change', function () {
            let inputs = $('#client_material_checked_form td input');
            console.log(inputs);
            let indic = false;
            [...inputs].forEach(element => {
                if( $(element).val() > 0 ) {
                    indic = true
                }
                if(indic) {
                    $('#client_material_checked_form button').show();
                } else {
                    $('#client_material_checked_form button').hide();
                }
            });
        });
    });
    function getPlace(code) {
        let data = '';
        send_client_material.forEach(element => {
            if(element.code == code) {
                data = element.place_id;
            }
        });
        return data;
    }

    function getClient(code) {
        let data = '';
        send_client_material.forEach(element => {
            if(element.code == code) {
                data = element.client_id;
            }
        });
        return data;
    }

    $('#client_material_checked_form').on('submit', async function (e) {
        e.preventDefault();
        await window.saveFromJS();
        await save_xml_project($('#saveContainer').val());
        let form_data = $(this).serializeArray();
        let data_for_send = [];
        form_data.forEach(element => {
            let item = element.name;
            let place = getPlace(element.name);
            let client = getClient(element.name);
            // console.log(ffffff(element.name));
            if(element.value > 0) {
                data_for_send.push({
                    id: element.name,
                    place: place,
                    client_code: client,
                    // count: element.value
                });
            }
        });
        let send_array = {
            materials: data_for_send,
            project_data: $('#saveContainer').val()
        }
        console.log(send_array);
        $('#final_product_array').val(JSON.stringify(send_array));
        $('#final_product_array_form').submit();

        // const response = await fetch(main_dir + '/put_material_client_in.php', {
        //     method: 'POST', // *GET, POST, PUT, DELETE, etc.
        //     mode: 'cors', // no-cors, *cors, same-origin
        //     cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        //     credentials: 'same-origin', // include, *same-origin, omit
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     redirect: 'follow', // manual, *follow, error
        //     referrerPolicy: 'no-referrer', // no-referrer, *client
        //     body: JSON.stringify(send_array) // body data type must match "Content-Type" header
        // });
        // let response_body = await response.text(); // parses JSON response into native JavaScript objects
        // console.log(response_body);
        // $('#client_material_checked_form').append('<p>Массив данных в необходимом формате был отправлен на /put_material_client_in.php. Отправленные данные можно увидеть в консоли (вкладка Network). Настройка обработки ответа также настроена</p>');

    });

    $('#get_client_material_close').on('click', function () {
        $('#get_client_material').hide();
    });


    $('#client_menu_button').on('click', async function(e) {
        $('#client_menu_containers').toggleClass('open');
    });
    $('#client_menu_auth_form_idg .client_menu_button').on('click', async function(e) {
        e.preventDefault();
        $(window).off('beforeunload');
        $(window).unbind('beforeunload');
        window.onbeforeunload = null;
        await window.saveFromJS();
        console.log('Запрос на перезапись cессии');
        await save_xml_project($('#saveContainer').val());
        console.log('Сессия с данными перезаписана');
        let request = await fetch('/send_project', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN':  new_csrf_token,
                'X-XSRF-TOKEN':  new_csrf_token,
            },
            credentials: 'include',
            body: JSON.stringify({
                _token: $('#continue_form input[name="_token"]').val(),
                project_data: $('#continue_form input[name="project_data"]').val(),
                user_place: $('#continue_form input[name="user_place"]').val(),
                user_type_edge: $('#continue_form input[name="user_type_edge"]').val(),
            })
        });
        let response = await request.text();
        await fetch(`/save_data_id?data_id=${response}`, {method: 'GET'});
        console.log('Data_id записан в сессию');
        $('#client_menu_auth_form_idg').submit();

    });



    async function get_clients_orders_data(page = 1, datez = 365, statusz = 'all', typez = 'all') {
        $('#checks_lodaer_containers').show();
        let orders_table = $('#get_client_orders table#orders_html_table tbody');
        let request = await fetch(app_dir + '/get_client_orders?page=' + page + '&date=' + datez + '&status=' + statusz + '&type=' + typez);
        let body = await request.json();
        console.log(body);
        if(body.length < 1) {
            $('#checks_lodaer_containers').hide();
            $(orders_table).html(`
            <tr>
                <td colspan="6" style="text-align: right">
                     <h1 style="text-align: center;">Заказов не найдено</h1>
                </td>
            </tr>
          `);
            await $('#orders_div_pagination').html('');
            $('#get_client_orders').show();

            return false;
        }
        orders_table.html(' ');
        await body.orders.forEach(async (element) => {
            let request_check = await fetch(app_dir + '/get_orders_parts?order=' + element.id);
            let body_check = await request_check.json();
            let date = 'Неизвестно';
            let rs_order = element.rs_order;
            if(element.date && element.date != '') {
                date = element.date;
            }
            // console.log(element);
            //.. Кнопки по РС "button_info" и "button_RS"
            let RS_project_version = '', RS_project_last_version = '';
            element.id_encode.forEach(function (e) {
                let e_id = e.link.split('order_project_id=').pop();
                if (RS_project_last_version == '') {
                    RS_project_last_version = `<a href="${main_dir}/rs_detail.php?ip=${e_id}" target="_blank">
                        <svg fill="#000000" viewBox="0 0 50 50" width="50px" height="50px"><path fill="none" stroke="#000000" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M25 3A22 22 0 1 0 25 47A22 22 0 1 0 25 3Z"/><path d="M25 11A3 3 0 1 0 25 17 3 3 0 1 0 25 11zM23 21H27V38H23z"/><path fill="none" stroke="#000000" stroke-linecap="square" stroke-miterlimit="10" stroke-width="2" d="M22 22L26 22M22 37L28 37"/></svg>
                    </a>`
                }
                RS_project_version += `<a href="${main_dir}/clients/RS/clients/create.php?order_project_id=${e_id}" id="RS_button_edit" title="Редактировать РС ">${e.author} от ${e.date}</a>`
            });
            let button_info = `
                <button style="cursor:pointer;" id="RS-info" data-project='' title="Спецификация РС">
                    ${RS_project_last_version}
                </button>
            `;

            let button_RS = `      
                <div class="client_orders_versions_container">
                    <div class="client_order_versions_button" id="RS_versions_button_${element.id}" style="font-size:1rem;font-weight:bold;cursor:pointer;">
                        РС
                    </div>
                    <div class="client_orders_versions_variant" id="RS_versions_variant_${element.id}">
                       ${RS_project_version}
                    </div>
                </div>      
            `;
            let RS_buttons = `${button_info} ${button_RS}`;

            // let button_RS = `
            //     <button class="RS-btn" style="font-size:1rem;font-weight:bold;cursor:pointer;" id="RS-constructor-${element.id}" data-project='' title="Конструктор РС">RS</button>
                
            // `;

            let button_pdf = `
                <button class="order_project_pdf_${element.id}" data-project='' title="Полный PDF отчет">PDF</button>
            `;
            let button_specification = `
                <button id="orders_button_specifications" onclick="get_orders_parts(${element.id}, '${element.big_id}')" title="Спецификация">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="512" height="512">
                          <g>
                              <path d="M316,30h-17.57C292.239,12.539,275.556,0,256,0s-36.239,12.539-42.43,30H196c-8.284,0-15,6.716-15,15v15H76   c-8.284,0-15,6.716-15,15v422c0,8.284,6.716,15,15,15h360c8.284,0,15-6.716,15-15V75c0-8.284-6.716-15-15-15H331V45   C331,36.716,324.284,30,316,30z M211,60h15c8.284,0,15-6.716,15-15c0-8.271,6.729-15,15-15s15,6.729,15,15c0,8.284,6.716,15,15,15   h15v30h-90V60z M421,90v392H91V90h90v15c0,8.284,6.716,15,15,15h120c8.284,0,15-6.716,15-15V90H421z"/>
                              <path d="M181,165c0,8.284,6.716,15,15,15h180c8.284,0,15-6.716,15-15s-6.716-15-15-15H196C187.716,150,181,156.716,181,165z"/>
                              <circle cx="136" cy="165" r="15"/>
                              <path d="M376,210H196c-8.284,0-15,6.716-15,15s6.716,15,15,15h180c8.284,0,15-6.716,15-15S384.284,210,376,210z"/>
                              <circle cx="136" cy="225" r="15"/>
                              <path d="M376,270H196c-8.284,0-15,6.716-15,15s6.716,15,15,15h180c8.284,0,15-6.716,15-15S384.284,270,376,270z"/>
                              <circle cx="136" cy="285" r="15"/>
                              <path d="M376,330H136c-8.284,0-15,6.716-15,15v92c0,8.284,6.716,15,15,15h240c8.284,0,15-6.716,15-15v-92   C391,336.716,384.284,330,376,330z M361,422H151v-62h210V422z"/>
                          </g>
                     </svg>
                </button>
            `;
            let order_project_version = '';
            element.id_encode.forEach(function (e) {
                order_project_version += `<a href="${app_dir}/projects_new/${e.link}" id="orders_button_edit" title="Редактировать заказ ">${e.author} от ${e.date}</a>`
            });

            /*              <a href="${app_dir}/projects_new/${element.id_encode}" id="orders_button_edit" title="Редактировать заказ ">

                            </a>*/

            let button_project = `
                            
                            <div class="client_orders_versions_container">
                                <div class="client_order_versions_button" id="order_versions_button_${element.id}">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M484.14,226.886L306.46,49.202c-5.072-5.072-11.832-7.856-19.04-7.856c-7.216,0-13.972,2.788-19.044,7.856l-16.132,16.136    c-5.068,5.064-7.86,11.828-7.86,19.04c0,7.208,2.792,14.2,7.86,19.264L355.9,207.526H26.58C11.732,207.526,0,219.15,0,234.002    v22.812c0,14.852,11.732,27.648,26.58,27.648h330.496L252.248,388.926c-5.068,5.072-7.86,11.652-7.86,18.864    c0,7.204,2.792,13.88,7.86,18.948l16.132,16.084c5.072,5.072,11.828,7.836,19.044,7.836c7.208,0,13.968-2.8,19.04-7.872    l177.68-177.68c5.084-5.088,7.88-11.88,7.86-19.1C492.02,238.762,489.228,231.966,484.14,226.886z"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="client_orders_versions_variant" id="client_orders_versions_variant_${element.id}">
                                    ${order_project_version}
                                </div>
                            </div>
                            
            `;

            let button_refresh = `
                            <a href="${main_dir}/refresh_project_programm.php?project=${element.id}" id="refresh_buttons_programm_${element.id}" title="Обновить программы заказа" target="_blank">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M463.702,162.655L442.491,14.164c-1.744-12.174-16.707-17.233-25.459-8.481l-30.894,30.894
                                                C346.411,12.612,301.309,0,254.932,0C115.464,0,3.491,109.16,0.005,248.511c-0.19,7.617,5.347,14.15,12.876,15.234l59.941,8.569
                                                c8.936,1.304,17.249-5.712,17.125-15.058C88.704,165.286,162.986,90,254.932,90c22.265,0,44.267,4.526,64.6,13.183l-29.78,29.78
                                                c-8.697,8.697-3.761,23.706,8.481,25.459l148.491,21.211C456.508,181.108,465.105,172.599,463.702,162.655z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M499.117,249.412l-59.897-8.555c-7.738-0.98-17.124,5.651-17.124,16.143c0,90.981-74.019,165-165,165
                                                c-22.148,0-44.048-4.482-64.306-13.052l28.828-28.828c8.697-8.697,3.761-23.706-8.481-25.459L64.646,333.435
                                                c-9.753-1.393-18.39,6.971-16.978,16.978l21.21,148.492c1.746,12.187,16.696,17.212,25.459,8.481l31.641-31.626
                                                C165.514,499.505,210.587,512,257.096,512c138.794,0,250.752-108.618,254.897-247.28
                                                C512.213,257.088,506.676,250.496,499.117,249.412z"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
            `;

            if(body_check.length < 1) {
                button_pdf = '';
                button_specification = '';
                button_project = '';
                button_refresh = `
                    <a title="Нельзя обновить программы для данного заказа" target="_blank">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M463.702,162.655L442.491,14.164c-1.744-12.174-16.707-17.233-25.459-8.481l-30.894,30.894
                                                C346.411,12.612,301.309,0,254.932,0C115.464,0,3.491,109.16,0.005,248.511c-0.19,7.617,5.347,14.15,12.876,15.234l59.941,8.569
                                                c8.936,1.304,17.249-5.712,17.125-15.058C88.704,165.286,162.986,90,254.932,90c22.265,0,44.267,4.526,64.6,13.183l-29.78,29.78
                                                c-8.697,8.697-3.761,23.706,8.481,25.459l148.491,21.211C456.508,181.108,465.105,172.599,463.702,162.655z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M499.117,249.412l-59.897-8.555c-7.738-0.98-17.124,5.651-17.124,16.143c0,90.981-74.019,165-165,165
                                                c-22.148,0-44.048-4.482-64.306-13.052l28.828-28.828c8.697-8.697,3.761-23.706-8.481-25.459L64.646,333.435
                                                c-9.753-1.393-18.39,6.971-16.978,16.978l21.21,148.492c1.746,12.187,16.696,17.212,25.459,8.481l31.641-31.626
                                                C165.514,499.505,210.587,512,257.096,512c138.794,0,250.752-108.618,254.897-247.28
                                                C512.213,257.088,506.676,250.496,499.117,249.412z"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                `;
            }
            if(!isAdmin) {
                button_refresh = '';
            }
            //.. Если раздвижная система, добавляем кнопки "button_info" и "button_RS"
            let buttons_variants;
            if (rs_order == 1) {
                buttons_variants = `${button_info}
                                        ${button_RS}
                                        ${button_pdf}
                                        ${button_specification}
                                        <button id="orders_button_receipt" onclick="get_orders_goods(${element.id}, '${element.big_id}')" title="Счет">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="493pt" viewBox="0 -11 493.78 493" width="493pt"><path d="m378.351562 70.472656c.214844.015625.429688.03125.648438.03125.371094 0 .742188-.03125 1.105469-.082031 9.722656.199219 17.503906 8.128906 17.515625 17.851563 0 4.417968 3.582031 8 8 8 4.417968 0 8-3.582032 8-8-.019532-15.902344-11.089844-29.660157-26.621094-33.082032v-7.6875c0-4.417968-3.582031-8-8-8s-8 3.582032-8 8v8.050782c-16.421875 4.390624-27.046875 20.277343-24.832031 37.132812 2.214843 16.855469 16.582031 29.457031 33.582031 29.457031 9.871094 0 17.871094 8.003907 17.871094 17.875 0 9.867188-8 17.871094-17.871094 17.871094s-17.871094-8.003906-17.871094-17.871094c0-4.417969-3.582031-8-8-8-4.417968 0-8 3.582031-8 8 .019532 15.328125 10.316406 28.738281 25.121094 32.71875v8.765625c0 4.417969 3.582031 8 8 8s8-3.582031 8-8v-8.398437c16.894531-3.699219 28.289062-19.535157 26.425781-36.730469-1.859375-17.195312-16.378906-30.226562-33.675781-30.222656-9.597656.003906-17.484375-7.574219-17.863281-17.164063-.375-9.589843 6.894531-17.765625 16.464843-18.511719zm0 0"/><path d="m380.207031.390625c-49.214843 0-91.214843 32.113281-106.949219 75.113281h-198.558593c-4.398438 0-7.96875 3.964844-8 8.359375l-1.890625 280.640625h-56.597656c-4.417969 0-8.210938 3.199219-8.210938 7.625v35.613282c.101562 33.527343 26.507812 61.070312 60 62.585937v.175781h247v-.234375c2 .074219 2.824219.234375 4.089844.234375h.171875c34.664062-.054687 62.738281-28.171875 62.738281-62.835937v-180.0625c2 .109375 4.117188.167969 6.1875.167969 62.628906 0 113.59375-51.0625 113.59375-113.695313 0-62.628906-50.941406-113.6875-113.574219-113.6875zm-317.164062 454.113281h-.050781c-25.878907-.035156-46.875-20.960937-46.992188-46.84375v-27.15625h232v27.042969c.011719 16.695313 6.679688 32.699219 18.523438 44.46875.839843.839844 1.882812 1.488281 2.761718 2.488281zm294.957031-46.84375c.003906 25.835938-20.914062 46.792969-46.746094 46.84375h-.152344c-25.9375-.046875-46.972656-21.015625-47.101562-46.949218v-35.425782c.066406-2.046875-.714844-4.027344-2.164062-5.472656-1.449219-1.445312-3.429688-2.222656-5.472657-2.152344h-175.554687l1.835937-273h186.171875c-1.417968 7.324219-2.152344 14.761719-2.191406 22.21875-.015625 15.769532 3.273438 31.363282 9.65625 45.78125h-75.5625c-4.421875 0-8 3.582032-8 8 0 4.417969 3.578125 8 8 8h84.242188c16.503906 25.953125 42.886718 44.046875 73.039062 50.101563zm22.207031-195.882812c-53.890625 0-97.582031-43.6875-97.578125-97.582032 0-53.894531 43.6875-97.582031 97.582032-97.582031 53.890624 0 97.578124 43.691407 97.578124 97.582031-.058593 53.867188-43.710937 97.523438-97.582031 97.582032zm0 0"/><path d="m149.367188 212.746094c-14.121094 0-25.605469 11.121094-25.605469 24.792968 0 13.671876 11.484375 24.792969 25.605469 24.792969 14.121093 0 25.609374-11.121093 25.609374-24.792969 0-13.671874-11.488281-24.792968-25.609374-24.792968zm0 33.585937c-5.300782 0-9.605469-3.945312-9.605469-8.792969 0-4.851562 4.308593-8.792968 9.605469-8.792968 5.296874 0 9.609374 3.945312 9.609374 8.792968 0 4.847657-4.3125 8.792969-9.609374 8.792969zm0 0"/><path d="m192.71875 237.503906c0 4.417969 3.578125 8 8 8h106.65625c4.417969 0 8-3.582031 8-8 0-4.417968-3.582031-8-8-8h-106.65625c-4.421875 0-8 3.582032-8 8zm0 0"/><path d="m149.367188 143.203125c-14.121094 0-25.605469 11.125-25.605469 24.796875s11.484375 24.792969 25.605469 24.792969c14.121093 0 25.609374-11.121094 25.609374-24.792969s-11.488281-24.796875-25.609374-24.796875zm0 33.589844c-5.300782 0-9.605469-3.945313-9.605469-8.792969s4.308593-8.796875 9.605469-8.796875c5.296874 0 9.609374 3.945313 9.609374 8.796875 0 4.847656-4.3125 8.796875-9.609374 8.796875zm0 0"/><path d="m149.367188 282.28125c-14.121094 0-25.605469 11.121094-25.605469 24.792969s11.484375 24.792969 25.605469 24.792969c14.121093 0 25.609374-11.121094 25.609374-24.792969s-11.488281-24.792969-25.609374-24.792969zm0 33.585938c-5.300782 0-9.605469-3.941407-9.605469-8.792969 0-4.847657 4.308593-8.792969 9.605469-8.792969 5.296874 0 9.609374 3.945312 9.609374 8.792969 0 4.847656-4.3125 8.792969-9.609374 8.792969zm0 0"/><path d="m307.375 299.503906h-106.65625c-4.421875 0-8 3.582032-8 8 0 4.417969 3.578125 8 8 8h106.65625c4.417969 0 8-3.582031 8-8 0-4.417968-3.582031-8-8-8zm0 0"/></svg>
                                        </button>
                                        ${button_refresh}
                                        ${button_project}`;
            } else {
                buttons_variants = `${button_pdf}
                                        ${button_specification}
                                        <button id="orders_button_receipt" onclick="get_orders_goods(${element.id}, '${element.big_id}')" title="Счет">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="493pt" viewBox="0 -11 493.78 493" width="493pt"><path d="m378.351562 70.472656c.214844.015625.429688.03125.648438.03125.371094 0 .742188-.03125 1.105469-.082031 9.722656.199219 17.503906 8.128906 17.515625 17.851563 0 4.417968 3.582031 8 8 8 4.417968 0 8-3.582032 8-8-.019532-15.902344-11.089844-29.660157-26.621094-33.082032v-7.6875c0-4.417968-3.582031-8-8-8s-8 3.582032-8 8v8.050782c-16.421875 4.390624-27.046875 20.277343-24.832031 37.132812 2.214843 16.855469 16.582031 29.457031 33.582031 29.457031 9.871094 0 17.871094 8.003907 17.871094 17.875 0 9.867188-8 17.871094-17.871094 17.871094s-17.871094-8.003906-17.871094-17.871094c0-4.417969-3.582031-8-8-8-4.417968 0-8 3.582031-8 8 .019532 15.328125 10.316406 28.738281 25.121094 32.71875v8.765625c0 4.417969 3.582031 8 8 8s8-3.582031 8-8v-8.398437c16.894531-3.699219 28.289062-19.535157 26.425781-36.730469-1.859375-17.195312-16.378906-30.226562-33.675781-30.222656-9.597656.003906-17.484375-7.574219-17.863281-17.164063-.375-9.589843 6.894531-17.765625 16.464843-18.511719zm0 0"/><path d="m380.207031.390625c-49.214843 0-91.214843 32.113281-106.949219 75.113281h-198.558593c-4.398438 0-7.96875 3.964844-8 8.359375l-1.890625 280.640625h-56.597656c-4.417969 0-8.210938 3.199219-8.210938 7.625v35.613282c.101562 33.527343 26.507812 61.070312 60 62.585937v.175781h247v-.234375c2 .074219 2.824219.234375 4.089844.234375h.171875c34.664062-.054687 62.738281-28.171875 62.738281-62.835937v-180.0625c2 .109375 4.117188.167969 6.1875.167969 62.628906 0 113.59375-51.0625 113.59375-113.695313 0-62.628906-50.941406-113.6875-113.574219-113.6875zm-317.164062 454.113281h-.050781c-25.878907-.035156-46.875-20.960937-46.992188-46.84375v-27.15625h232v27.042969c.011719 16.695313 6.679688 32.699219 18.523438 44.46875.839843.839844 1.882812 1.488281 2.761718 2.488281zm294.957031-46.84375c.003906 25.835938-20.914062 46.792969-46.746094 46.84375h-.152344c-25.9375-.046875-46.972656-21.015625-47.101562-46.949218v-35.425782c.066406-2.046875-.714844-4.027344-2.164062-5.472656-1.449219-1.445312-3.429688-2.222656-5.472657-2.152344h-175.554687l1.835937-273h186.171875c-1.417968 7.324219-2.152344 14.761719-2.191406 22.21875-.015625 15.769532 3.273438 31.363282 9.65625 45.78125h-75.5625c-4.421875 0-8 3.582032-8 8 0 4.417969 3.578125 8 8 8h84.242188c16.503906 25.953125 42.886718 44.046875 73.039062 50.101563zm22.207031-195.882812c-53.890625 0-97.582031-43.6875-97.578125-97.582032 0-53.894531 43.6875-97.582031 97.582032-97.582031 53.890624 0 97.578124 43.691407 97.578124 97.582031-.058593 53.867188-43.710937 97.523438-97.582031 97.582032zm0 0"/><path d="m149.367188 212.746094c-14.121094 0-25.605469 11.121094-25.605469 24.792968 0 13.671876 11.484375 24.792969 25.605469 24.792969 14.121093 0 25.609374-11.121093 25.609374-24.792969 0-13.671874-11.488281-24.792968-25.609374-24.792968zm0 33.585937c-5.300782 0-9.605469-3.945312-9.605469-8.792969 0-4.851562 4.308593-8.792968 9.605469-8.792968 5.296874 0 9.609374 3.945312 9.609374 8.792968 0 4.847657-4.3125 8.792969-9.609374 8.792969zm0 0"/><path d="m192.71875 237.503906c0 4.417969 3.578125 8 8 8h106.65625c4.417969 0 8-3.582031 8-8 0-4.417968-3.582031-8-8-8h-106.65625c-4.421875 0-8 3.582032-8 8zm0 0"/><path d="m149.367188 143.203125c-14.121094 0-25.605469 11.125-25.605469 24.796875s11.484375 24.792969 25.605469 24.792969c14.121093 0 25.609374-11.121094 25.609374-24.792969s-11.488281-24.796875-25.609374-24.796875zm0 33.589844c-5.300782 0-9.605469-3.945313-9.605469-8.792969s4.308593-8.796875 9.605469-8.796875c5.296874 0 9.609374 3.945313 9.609374 8.796875 0 4.847656-4.3125 8.796875-9.609374 8.796875zm0 0"/><path d="m149.367188 282.28125c-14.121094 0-25.605469 11.121094-25.605469 24.792969s11.484375 24.792969 25.605469 24.792969c14.121093 0 25.609374-11.121094 25.609374-24.792969s-11.488281-24.792969-25.609374-24.792969zm0 33.585938c-5.300782 0-9.605469-3.941407-9.605469-8.792969 0-4.847657 4.308593-8.792969 9.605469-8.792969 5.296874 0 9.609374 3.945312 9.609374 8.792969 0 4.847656-4.3125 8.792969-9.609374 8.792969zm0 0"/><path d="m307.375 299.503906h-106.65625c-4.421875 0-8 3.582032-8 8 0 4.417969 3.578125 8 8 8h106.65625c4.417969 0 8-3.582031 8-8 0-4.417968-3.582031-8-8-8zm0 0"/></svg>
                                        </button>
                                        ${button_refresh}
                                        ${button_project}`;
            }
            let order = `
                <tr>
                    <td>${date}</td>
                    <td>${element.big_id}</td>
                    <td>${element.client_name}</td>
                    <td>${element.place}</td>
                    <td>
                        <div> 
                            <span>${element.manager.name}</span>
                        </div>
                        <div>
                            <small><a href="mailto:${element.manager.email}">${element.manager.email}</a></small>
                        </div>
                        <div>
                            <small>${element.manager.phone ? element.manager.phone : '+38(044)390-00-07'}</small>
                        </div>
                    </td>
                    <td>${element.status}</td>
                    <td>
                        <div>`
                            +buttons_variants+
                        `</div>
                    </td>
                </tr>            
            `;
            await orders_table.append(order);
            if(body_check.length > 0) {
                $('.order_project_pdf_' + element.id).on('click', async function (e) {
                    e.preventDefault();

                    $('#checks_lodaer_containers').show();


                    let formData = new FormData();
                    formData.append('project_data', element.project);
                    console.log($(e.target).attr('data-project'));
                    formData.append('action', 'get_all_pdf_manager');
                    console.log(isAdmin);
                    formData.append('is_admin', isAdmin);

                    let request = await fetch(main_dir + '/pdf_send.php', {
                        method: 'POST',
                        headers: {
                            ///'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        body: formData
                    });
                    let body = await request.text();
                    console.log(body);

                    if (body.indexOf('.pdf') > -1) {
                        $('#checks_lodaer_containers').hide();
                        window.open(main_dir + '/pdf_send.php?show=' + body);
                    }

                    // if(body.indexOf('/users_pdf/') > -1){
                    //     $('#checks_lodaer_containers').hide();
                    //     window.open(body);
                    // }
                });
                $('#refresh_buttons_programm_' + element.id).on('click', async function (e) {
                    e.preventDefault();

                    $('#checks_lodaer_containers').show();

                    let url = $(e.target).closest('a').attr('href');

                    let req_refresh = await fetch(url, {
                            method: 'GET',
                        }
                    );
                    let req_body = await req_refresh.text();
                    console.log(req_body.trim());
                    if(req_body.trim() == 'Программы успешно обновлены!') {
                        $(e.target).closest('a').html(`
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 367.805 367.805" style="enable-background:new 0 0 367.805 367.805;" xml:space="preserve">
                                    <g>
                                    <path style="fill:#3BB54A;" d="M183.903,0.001c101.566,0,183.902,82.336,183.902,183.902s-82.336,183.902-183.902,183.902
                                S0.001,285.469,0.001,183.903l0,0C-0.288,82.625,81.579,0.29,182.856,0.001C183.205,0,183.554,0,183.903,0.001z"/>
                                <polygon style="fill:#D4E1F4;" points="285.78,133.225 155.168,263.837 82.025,191.217 111.805,161.96 155.168,204.801 
                                256.001,103.968 	"/>
                                </g>
                            </svg>
                        `);
                        $(e.target).closest('a').attr('href', '');
                    }
                    $('#checks_lodaer_containers').hide();
                });
                $(`#order_versions_button_${element.id} > *`).on('click', function (e) {
                    $('#client_orders_versions_variant_' + element.id ).toggleClass('open');
                });
                //.. Меню для кнопки РС
                $('#RS_versions_button_'+element.id).on('click', function (e) {
                    $('#RS_versions_variant_' + element.id ).toggleClass('open');
                })
            }

        });

        let pages_count = body.pages;
        if(pages_count > Math.round(pages_count)) {
            pages_count = Math.round(pages_count) + 1
        }
        let pages = '';
        for(let i = 1; i <= pages_count; i++) {
            let classs = '';
            if(i == page) {
                classs = `class="active"`;
            }
            pages += `<button data-page="${i}" ${classs}>${i}</button>`;
        }

        let pageinations = `${pages}`;


        await $('#orders_div_pagination').html(pageinations);

        $('#orders_div_pagination button').on('click', async function (e) {
            let page = $(e.target).attr('data-page');
            await get_clients_orders_data(page, datez, statusz, typez);

        });

        await $('#checks_lodaer_containers').hide();

        $('#get_client_orders').show();
    }

    $('#client_orders_button').on('click', async function() {
        get_clients_orders_data(1);
    });
    $('.orders_filters select').on('change', function (e) {
        console.log('ddddd');
        let filter_date = $('#client_orders_filter_date').val();
        let filter_status = $('#client_orders_filter_status').val();
        let filter_type = $('#client_orders_filter_type').val();
        get_clients_orders_data(1, filter_date, filter_status, filter_type);
    });



    $('#version_control_menu').on('click', function () {
        $('.bottom_version_control_menu').toggleClass('open');
    });



    $('#get_client_orders_close').on('click', function () {
        $('#get_client_orders').hide();
    });

    $('#orders_inner_receipt_back').on('click', function () {
        let orders_table = $('#get_client_orders table#orders_html_table');
        let receipt_container = $('#orders_inner_receipt');
        let parts_container = $('#orders_inner_parts');
        orders_table.show();
        receipt_container.hide();
        parts_container.hide();
        $('.orders_filters').show();
        $('#orders_div_pagination').show();
    });
    $('#orders_inner_parts_back').on('click', function () {
        let orders_table = $('#get_client_orders table#orders_html_table');
        let receipt_container = $('#orders_inner_receipt');
        let parts_container = $('#orders_inner_parts');
        orders_table.show();
        receipt_container.hide();
        parts_container.hide();
        $('.orders_filters').show();
        $('#orders_div_pagination').show();
    });






    $('#set_client_auth_confirm').on('click', async function (e) {
        // if(isAdmin) {
        //     $('#set_client_auth_confirm_form').submit();
        // }
        if(true) {
            $('#set_client_auth_confirm_form').submit();
        } else {
            e.preventDefault();
            let get_pin_code = await fetch('/send_client_pin_code?client=' + client_auth_phone);
            let pin_code = await get_pin_code.text();
            client_auth_pin = pin_code / Math.pow(3, 15);
            $('#client_send_pin_code_container').toggleClass('open');
        }


    });
    $('#client_send_pin_code_button').on('click', function () {
        if(client_auth_pin) {
            if(Number(client_auth_pin) == Number($('#client_send_pin_code_input').val()) ) {
                $('#set_client_auth_confirm_form').submit();
            } else {
                $('#client_send_pin_code_message').html('ПИН-код не верный');
            }
        }
    });

    $('#user_save_project_file').on('click', async function () {
        await window.saveFromJS();
        let project = $('#saveContainer').val();
        await save_xml_project(project);
        let request = await fetch('/save_project_file', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': new_csrf_token,
                'X-XSRF-TOKEN': new_csrf_token,
            },
            credentials: 'include',
            body: JSON.stringify({
                project_data: project,
            })
        });
        let body = await request.text();
        console.log(body);
        let link = document.createElement('a');
        link.setAttribute('href',body);
        link.setAttribute('download','file' + Date.now() + '.project');
        onload=link.click();
    });



    $('#refresh_programm_now').on('click', async function (e) {
        e.preventDefault();
        await window.saveFromJS();
        $('#checks_lodaer_containers').show();
        let project = $('#saveContainer').val();
        await save_xml_project(project);

        let formData = new FormData();
        formData.append('project_data', project);
        // formData.append('action', 'get_all_pdf' );

        let request = await fetch($(e.target).attr('href'), {
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                // 'X-CSRF-TOKEN': new_csrf_token,
                // 'X-XSRF-TOKEN': new_csrf_token,
            },
            // credentials: 'include',
            body: formData
        });
        let body = await request.text();
        console.log(body);
        $('#checks_lodaer_containers').hide();
        if(body = 'Программы на сервисе успешно обновлены! Запускаем отправку на станки...') {
            $(e.target).css({
                background: 'green'
            });
            $(e.target).html('Программы успешно обновлены');
        }
    });
    $('#refresh_programm_angle_change').on('click', async function (e) {
        e.preventDefault();
        await window.saveFromJS();
        $('#checks_lodaer_containers').show();
        let project = $('#saveContainer').val();
        await save_xml_project(project);

        let formData = new FormData();
        formData.append('project_data', project);
        // formData.append('action', 'get_all_pdf' );

        let request = await fetch($(e.target).attr('href'), {
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json',
                // 'X-CSRF-TOKEN': new_csrf_token,
                // 'X-XSRF-TOKEN': new_csrf_token,
            },
            // credentials: 'include',
            body: formData
        });
        let body = await request.text();
        console.log(body);
        $('#checks_lodaer_containers').hide();
        if(body = 'Программы на сервисе успешно обновлены! Запускаем отправку на станки...') {
            $(e.target).css({
                background: 'green'
            });
            $(e.target).html('Программы успешно обновлены');
        }
    });


    $('#save_ajax_new_version_button').on('click', async function () {
        let current_place = $('#user_place').val();
        await window.saveFromJS();
        await save_xml_project($('#saveContainer').val());
        $('#sep_project_data').val($('#saveContainer').val());
        const form = document.querySelector('#sep_project_form');
        const formData = new FormData(form);
        formData.append('is_ajax', 1);
        formData.append('current_place', current_place);
        formData.append('user_type_edge', $('#user_type_edge').val());
        console.log(formData);
        let order_ids = $('input[name="sep_order_id"]').val();
        try {
            const response = await fetch('/save_edit_project_manager', {
                // headers: {
                //     'X-CSRF-TOKEN': new_csrf_token,
                //     'X-XSRF-TOKEN': new_csrf_token,
                // },
                method: 'POST',
                body: formData
            });
            const result = await response.text();
            if(result == 1) {
                alert('Текущая версия ничем не отличается от предыдущей');
                document.location.href = app_dir + '/projects_new/1_' + order_ids;

            }
            if(result == 4) {
                alert('Версия проекта успешна сохранена и доступна в хранилище');
                document.location.href = app_dir + '/projects_new/1_' + order_ids;
                // location.reload();
            }
            // console.log('Успех:', JSON.stringify(result));
        } catch (error) {
            console.error('Ошибка:', error);
        }
    });


    //.. 18.03.2023 (Егор Лысенко) Скрипт для загрузки проектов с нового сервиса.
    setProjectFromUrl = function(projectId){
        var projectEndPoint = 'https://auth.kronas.com.ua';
        var token = decodeURIComponent(document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1"))
        var tokenParse = $('#continue_form > input[type=hidden]:nth-child(1)').val();
        const projectSet = {
            "async": true,
            "crossDomain": true,
            "url": projectEndPoint + "/api/v1/project/file/" + projectId,
            "method": "GET",
            "headers": {
                "Accept": "application/json",
                "Authorization": "Bearer " + token
            }
        };
        $.ajax(projectSet).done(function (response) {
            var projectData = JSON.parse(response.file);
            const convertSet = {
                "async": true,
                "crossDomain": true,
                "url": projectEndPoint + "/api/v1/convert",
                "method": "POST",
                "headers": {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                "processData": false,
                "data": JSON.stringify({json_to_project: projectData}),
            };

            $.ajax(convertSet).done(function(response) {
                var resp = response.data;
                var result = JSON.stringify({"project_data": resp.replace('<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n', ''), "user_type_edge": "eva", "user_place": 1});
                // app_dir - http://service.kronas.com.ua:8080, задаётся при загрузке HTML-страницы
                fetch(app_dir + "/project_in_session", {
                    "headers": {
                        "accept": "*/*",
                        "accept-language": "ru,en;q=0.9,uk;q=0.8,mo;q=0.7",
                        "cache-control": "no-cache",
                        "content-type": "application/json",
                        "pragma": "no-cache",
                        "x-csrf-token": tokenParse,
                        "x-xsrf-token": tokenParse
                    },
                    "referrer": app_dir,
                    "referrerPolicy": "strict-origin-when-cross-origin",
                    "body": result,
                    "method": "POST",
                    "mode": "cors",
                    "credentials": "include"
                }).then((response) => {
                    if (response.ok) {
                        window.location.href = app_dir;
                    }
                });
            });
        });
    }
    
    var projectId = new URL(location.href).searchParams.get("myproject");
    if(projectId !== null) {
        setProjectFromUrl(projectId);    
    }
});


