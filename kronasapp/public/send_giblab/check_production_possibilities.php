<?php
if(!$link)
{
    $db = "giblabdb";
    date_default_timezone_set('Europe/Kiev');
    $link = mysqli_connect("localhost", "giblab", "85Da4892", $db);
    mysqli_set_charset($link, "utf8");
}
include_once("func.php");
if (!$vals)
{
    $vals=$_SESSION['vals'];
    $vals=vals_out($vals);
    //print_r_($vals);
    $index=$_SESSION['index'];
    $index=make_index($vals);
    //print_r_($index);
}
$v412_diam_mill=array(20,12,8,6,4);
$v412_diam_mill_depth=array(4=>10,6=>22,8=>22,12=>25,20=>45);
foreach($index["PART"] as $i12)
{
    if(($vals[$i12]["attributes"]["SHEETID"]=="")&&($vals[$i12]["attributes"]["CW"]>0)&&($vals[$i12]["attributes"]["CL"]>0))
    {
        $part++;
    }
}
foreach($index["GOOD"] as $i12)
{
    if(($vals[$i12]["attributes"]["TYPEID"]=="product")&&($vals[$i12]["type"]=="open"))
    {
        $product++;
    }
}
if (($part<1)OR($product<1))
{
    if (!$part) $part="0";
    if (!$product) $product="0";
    $error.="<br>[/00130] В проекте должно быть минимально одно изделие и хотя бы одна деталь. В данном проекте деталей ".$part." и изделий ".$product.". 
    Вернитесь пожалуста к редактированию деталей на предидущей странице.<br>";
}
unset ($product,$part);
foreach($index["PART"] as $i12)
{
    $tr=$vals[$i12]["attributes"]["ID"];
    if(($vals[$i12]["attributes"]["SHEETID"]=="")&&($vals[$i12]["attributes"]["CW"]>0)&&($vals[$i12]["attributes"]["CL"]>0))
    {
       
        foreach ($index["OPERATION"]as $i121)
        {
            if (($vals[$i121]["type"]=="open")&&($vals[$i121]["attributes"]["TYPEID"]=="CS"))
            {
                $i1211=$i121+2;
                while ($vals[$i1211]["tag"]=="PART")
                {
                    
                    if($vals[$i1211]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"])
                    {
                        
                        $mat12=$vals[$i121+1]["attributes"]["ID"];
                        foreach ($index["GOOD"] as $r121)
                        {
                            if (($vals[$r121]["attributes"]["TYPEID"]=="tool.cutting")AND($vals[$i121]["attributes"]["TOOL1"]==$vals[$r121]["attributes"]["ID"]))
                            {
                                $swsawthick=$vals[$r121]["attributes"]["SWSAWTHICK"];
                            }
                        }
                        $ctriml=$vals[$i121]["attributes"]["CTRIML"];
                        $ctrimw=$vals[$i121]["attributes"]["CTRIMW"];
                        
                        
                        
                        if ($ctriml>0) $triml=$ctriml*2+$swsawthick*2;
                        else $triml=0;
                        if ($ctrimw>0) $trimw=$ctrimw*2+$swsawthick*2;
                        else $trimw=0;
                        //test ("trim",$trim);
                    }
                    $i1211++;
                }
            }
        }
        unset($i121,$i1211);
        foreach ($index["GOOD"]as $i1212)
        {
            if ($vals[$i1212]["attributes"]["TYPEID"]=="sheet")
            {
            
                if($vals[$i1212]["attributes"]["ID"]==$mat12)
                {
                
                    $t12=$vals[$i1212]["attributes"]["T"];
                    $vals[$i12]["attributes"]["T"]=$t12;
                    $l12=$vals[$i1212]["attributes"]["L"];
                    $w12=$vals[$i1212]["attributes"]["W"];
                    $n12=$vals[$i1212]["attributes"]["NAME"];
                    $j=$i1212+1;
                    while ($vals[$j]["tag"]=="PART")
                    {
                        if ($vals[$j]["attributes"]["L"]>$l12) $l12=$vals[$j]["attributes"]["L"];
                        if ($vals[$j]["attributes"]["W"]>$w12) $w12=$vals[$j]["attributes"]["W"];
                        $j++;
                    }
                    unset ($j);
                }
            }
        }
        unset($i122);
        foreach ($index["OPERATION"]as $i32)
        {
            
            if ($vals[$i32]["attributes"]["TYPEID"]=="GR")
            {
                $i1211=$i32+1;
                while ($vals[$i1211]["type"]=="cdata")
                {
                    $i1211++;
                }
                if ($vals[$i1211]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"]) 
               {
                    $gr12=$vals[$i32]["attributes"]["GRDEPTH"];
                    $gr112=$vals[$i32]["attributes"]["GRWIDTH"];
               }
            }
        }
        unset($i121,$i1211,$i32);
        $iddd=0;
       // test ("id",$vals[$i12]["attributes"]["ID"]);
        
        foreach ($index["OPERATION"]as $i33)
        {
            
            if (($vals[$i33]["attributes"]["TYPEID"]==="XNC")AND($vals[$i33]["type"]=="open"))
            {
                
               
                if ($vals[$i33+1]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"])
                {
                   
                    
                    $pr12=$vals[$i33]["attributes"]["PROGRAM"];
                    $r = xml_parser_create();
                    xml_parse_into_struct($r, $pr12, $v412[$tr][$iddd], $i412[$tr][$iddd]);
                    xml_parser_free($r);
                   
                   // echo "1<hr>";
                    if(($vals[$i33]["attributes"]["TURN"]==1)OR($vals[$i33]["attributes"]["TURN"]==3))
                    {
                        //test ("id xnc",$vals[$i33+1]["attributes"]["ID"]);
                        //print_r_($v412[$tr][$iddd][0]["attributes"]["DX"]);
                        //test ("dx",$v412[$tr][$iddd][0]["attributes"]["DX"]);
                        //test ("dY",$v412[$tr][$iddd][0]["attributes"]["DY"]);
                        $t121_dl=$v412[$tr][$iddd][0]["attributes"]["DX"];
                        $v412[$tr][$iddd][0]["attributes"]["DX"]= $v412[$tr][$iddd][0]["attributes"]["DY"];
                        $v412[$tr][$iddd][0]["attributes"]["DY"]=$t121_dl;
                        //test ("dx",$v412[$tr][$iddd][0]["attributes"]["DX"]);
                        //test ("dY",$v412[$tr][$iddd][0]["attributes"]["DY"]);
                        $turn=$vals[$i33]["attributes"]["TURN"];
                        $turn_text="(ПОВОРОТ детали = ".($turn*90)." градусов)";
                    }
                    $v412_pr=$v412;
                    $iddd++;
                }
                
            }
        }
        unset($iddd,$i121,$i1211,$i33);
        foreach ($index["OPERATION"] as $i221)
        {
            if (($vals[$i221]["type"]=="open")&&($vals[$i221]["attributes"]["TYPEID"]=="EL"))
            {
                $i2211=$i221+2;
                while ($vals[$i2211]["tag"]=="PART")
                {
                    if($vals[$i2211]["attributes"]["ID"]==$vals[$i12]["attributes"]["ID"])
                    {
                        $ban12[]=$vals[$i221+1]["attributes"]["ID"];
                    }
                    $i2211++;
                }
            }
        }
        unset($i221,$i2211);
        foreach ($index["GOOD"]as $i2212)
        {
            if ($vals[$i2212]["attributes"]["TYPEID"]=="band")
            {
            
                if(in_array($vals[$i2212]["attributes"]["ID"],$ban12))
                {
                    
                    $ban121[$vals[$i2212]["attributes"]["ID"]]["tb12"]=$vals[$i2212]["attributes"]["T"];
                    $ban121[$vals[$i2212]["attributes"]["ID"]]["wb12"]=$vals[$i2212]["attributes"]["W"];
                    $ban121[$vals[$i2212]["attributes"]["ID"]]["nb12"]=$vals[$i2212]["attributes"]["NAME"];
                    /*test("wb12",$ban12["wb12"]);
                    test("tb12",$ban12["tb12"]);
                    test("nb2",$ban12["nb12"]); */
                    
                }
            }
        }
        if (($t12<$gr12+5)AND($t12>5)) {
            $error.="<br>[/0001] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая глубина паза ('".$gr12."'). Максимальное значение ".($t12-5)." мм.<br>";
        }
        if ((($vals[$i12]["attributes"]["GRL"])OR($vals[$i12]["attributes"]["GRR"]))&&($gr112>$vals[$i12]["attributes"]["L"]))
        {
            $error.="<br>[/0002] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая ширина паза ('".$gr112."'). Максимальное значение ".$vals[$i12]["attributes"]["L"]." мм.<br>";
        }
        if ((($vals[$i12]["attributes"]["GRT"])OR($vals[$i12]["attributes"]["GRB"]))&&($gr112>$vals[$i12]["attributes"]["W"]))
        {
            $error.="<br>[/0003] Деталь  '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая ширина паза ('".$gr112."'). Максимальное значение ".$vals[$i12]["attributes"]["W"]." мм.<br>";
        }
        if ($vals[$i12]["attributes"]["MY_DOUBLE"]==1)
        {
        
            if ($t12>54) {
                $error.="<br>[/0004] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая толщина 'T' ('".$t12."'). Максимальное значение 54 мм.<br>";
            }
            if ($t12<16) {
                $error.="<br>[/0005] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая толщина 'T' ('".$t12."'). Минимальное значение 16 мм.<br>";
            }
            if ($vals[$i12]["attributes"]["CW"]<70)
            { $error.="<br>[/0006] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 70 мм.<br>";
            }
            if ($vals[$i12]["attributes"]["CL"]<70)
            { $error.="<br>[/0007] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 70 мм.<br>";
            }
            if (($vals[$i12]["attributes"]["CW"]>=70)&&($vals[$i12]["attributes"]["CW"]<=100)&&($vals[$i12]["attributes"]["CL"]<=100))
            { $error.="<br>[/0008] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 70 до 100 мм, то 'L' должна быть больше 100 мм.<br>";
            }
            if (($vals[$i12]["attributes"]["CL"]>=70)&&($vals[$i12]["attributes"]["CL"]<=100)&&($vals[$i12]["attributes"]["CW"]<=100))
            { $error.="<br>[/0009] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 70 до 100 мм, то 'W' должна быть больше 100 мм.<br>";
            }
            if ($vals[$i12]["attributes"]["CW"]>($w12-$triml))
            { $error.="<br>[/0010] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
            }
            if ($vals[$i12]["attributes"]["CL"]>($l12-$trimw))
            { $error.="<br>[/0011] Деталь  (СШИВКА) '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
            - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
            }
        }
        if ($vals[$i12]["attributes"]["MY_DOUBLE"]<>1)
        {
            if ($t12<=21)
            {
                if ($vals[$i12]["attributes"]["CW"]<30)
                { $error.="<br>[/0012] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 30 мм.<br>Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                          
                }
                if ($vals[$i12]["attributes"]["CL"]<30)
                { $error.="<br>[/0013] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 30 мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                }
                
                if (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]=="true"))
                { $error.="<br>[/0014] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - деталь нельзя вращать, недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                }
                elseif (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { 
                    if (($vals[$i12]["attributes"]["CL"]>($w12-$trimw))AND($vals[$i12]["attributes"]["CW"]>($l12-$trimw)))
                    {
                        $error.="<br>[/0015] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                    }
                }
                if (($vals[$i12]["attributes"]["CW"]>($w12-$triml))AND($vals[$i12]["attributes"]["TXT"]=="true"))
                { 
                   // test ("trim",$trimw);
                    $error.="<br>[/0122] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - деталь нельзя вращать, недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$w12." мм, L = ".$l12." мм<br>";
                }
                elseif ((($vals[$i12]["attributes"]["CW"]>($l12-$triml))AND($vals[$i12]["attributes"]["CW"]>($w12-$triml)))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { $error.="<br>[/0123] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$l12." мм, L = ".$l12." мм<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>=30)&&($vals[$i12]["attributes"]["CW"]<=70)&&($vals[$i12]["attributes"]["CL"]<=100))
                { $error.="<br>[/0016] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 30 до 70 мм, то 'L' должна быть больше 100 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>=30)&&($vals[$i12]["attributes"]["CL"]<=70)&&($vals[$i12]["attributes"]["CW"]<=100))
                { $error.="<br>[/0017] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 30 до 70 мм, то 'W' должна быть больше 100 мм.<br>";
                }
            }
            
            elseif (($t12>21)&&($t12<27))
            {
                if ($vals[$i12]["attributes"]["CW"]<50)
                { $error.="<br>[/0018] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 50 мм.<br>";
                }
                if ($vals[$i12]["attributes"]["CL"]<50)
                { $error.="<br>[/0019] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 50 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]=="true"))
                { $error.="<br>[/0020] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - деталь нельзя вращать, недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                }
                elseif (($vals[$i12]["attributes"]["CL"]>($l12-$trimw))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { 
                    if (($vals[$i12]["attributes"]["CL"]>($w12-$trimw))AND($vals[$i12]["attributes"]["CW"]>($l12-$trimw)))
                    {
                        $error.="<br>[/0021] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".($l12-$trimw)." мм.<br> Размер материала L = ".$l12." мм, W = ".$w12." мм<br>";
                    }
                }
                if (($vals[$i12]["attributes"]["CW"]>($w12-$triml))AND($vals[$i12]["attributes"]["TXT"]=="true"))
                { $error.="<br>[/0124] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - деталь нельзя вращать, недопутимая длина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$w12." мм, L = ".$l12." мм<br>";
                }
                elseif ((($vals[$i12]["attributes"]["CW"]>($l12-$triml))AND($vals[$i12]["attributes"]["CW"]>($w12-$triml)))AND($vals[$i12]["attributes"]["TXT"]<>"true"))
                { $error.="<br>[/0125] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12-$triml)." мм.<br> Размер материала W = ".$l12." мм, L = ".$l12." мм<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>=50)&&($vals[$i12]["attributes"]["CW"]<=70)&&($vals[$i12]["attributes"]["CL"]<=100))
                { $error.="<br>[/0022] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 50 до 70 мм, то 'L' должна быть больше 100 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>=50)&&($vals[$i12]["attributes"]["CL"]<=70)&&($vals[$i12]["attributes"]["CW"]<=100))
                { $error.="<br>[/0023] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 50 до 70 мм, то 'W' должна быть больше 100 мм.<br>";
                }
                
            }
            elseif ($t12>27)
            {
                if ($vals[$i12]["attributes"]["CW"]<50)
                { $error.="<br>[/0024] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Минимальное значение 50 мм.<br>";
                }
                if ($vals[$i12]["attributes"]["CL"]<50)
                { $error.="<br>[/0025] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Минимальное значение 50 мм.<br>";
                }
                if ($vals[$i12]["attributes"]["CW"]>($w12))
                { $error.="<br>[/0026] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Максимальное значение ".($w12)." мм.<br>";
                //echo "!!!!!".$error;exit;
                }
                if ($vals[$i12]["attributes"]["CL"]>$l12)
                { $error.="<br>[/0027] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Максимальное значение ".$l12." мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CW"]>=50)&&($vals[$i12]["attributes"]["CW"]<=70)&&($vals[$i12]["attributes"]["CL"]<=100))
                { $error.="<br>[/0028] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая длина 'L' ('".$vals[$i12]["attributes"]["CL"]."'). Если 'W' от 50 до 70 мм, то 'L' должна быть больше 100 мм.<br>";
                }
                if (($vals[$i12]["attributes"]["CL"]>=50)&&($vals[$i12]["attributes"]["CL"]<=70)&&($vals[$i12]["attributes"]["CW"]<=100))
                { $error.="<br>[/0029] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая ширина 'W' ('".$vals[$i12]["attributes"]["CW"]."'). Если 'L' от 50 до 70 мм, то 'W' должна быть больше 100 мм.<br>";
                }
            }
            
        }
            //print_r_($vals[$i12]);
        if (($vals[$i12]["attributes"]["ELL"]<>"")OR($vals[$i12]["attributes"]["ELR"]<>"")OR($vals[$i12]["attributes"]["ELT"]<>"")OR($vals[$i12]["attributes"]["ELB"]<>""))
        {
           # test ("tb12",$ban12["tb12"]);
            #test("id",$vals[$i12]["attributes"]["ID"]);
            if ($t12<8) {
                $error.="<br>[/0030] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая толщина 'T' ('".$t12."') для кромкования. Минимальное значение 8 мм.<br>";
            }
            if ($t12>50) {
                $error.="<br>[/0031] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                - недопутимая толщина 'T' ('".$t12."') для кромкования. Максимальное значение 50 мм.<br>";
            }
            foreach ($ban121 as $ban12)
            {
                if($t12>($ban12["wb12"]-3))
                {
                    $error.="<br>[/00032] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая ширина 'W' ('".$ban12["wb12"]."') для кромки ('".$ban12["nb12"]."'). Найдите кромку шириной от ".($t12+3)." мм.<br>";
                }
                if(($t12<=24)&&($ban12["tb12"]<=0.8))
                {
                    
                    if ($vals[$i12]["attributes"]["L"]<35)
                    { $error.="<br>[/0033] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 35 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["W"]<35)
                    { $error.="<br>[/0034] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 35 мм.<br>";
                    }
                    if (($vals[$i12]["attributes"]["L"]>=35)&&($vals[$i12]["attributes"]["L"]<=59)&&($vals[$i12]["attributes"]["W"]<=150))
                    { $error.="<br>[/0035] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'L' от 35 до 59 мм, то 'W' должна быть больше 150 мм.<br>";
                    }
                    if (($vals[$i12]["attributes"]["W"]>=35)&&($vals[$i12]["attributes"]["W"]<=59)&&($vals[$i12]["attributes"]["L"]<=150))
                    { $error.="<br>[/0036] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'W' от 35 до 59 мм, то 'L' должна быть больше 150 мм.<br>";
                    }
                    if (($vals[$i12]["attributes"]["L"]>=60)&&($vals[$i12]["attributes"]["L"]<=70)&&($vals[$i12]["attributes"]["W"]<=60))
                    { $error.="<br>[/0037] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'L' от 60 до 70 мм, то 'W' должна быть больше 60 мм.<br>";
                    }
                    if (($vals[$i12]["attributes"]["W"]>=60)&&($vals[$i12]["attributes"]["W"]<=70)&&($vals[$i12]["attributes"]["L"]<=60))
                    { $error.="<br>[/0038] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'W' от 60 до 70 мм, то 'L' должна быть больше 60 мм.<br>";
                    }
                
                }  
                else
                {
                    if ($vals[$i12]["attributes"]["L"]<70)
                    { $error.="<br>[/0039] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["W"]<70)
                    { $error.="<br>[/0040] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Минимальное значение для кромкования 70 мм.<br>";
                    }
                    if (($vals[$i12]["attributes"]["L"]>=70)&&($vals[$i12]["attributes"]["L"]<100)&&($vals[$i12]["attributes"]["W"]<100))
                    { $error.="<br>[/0041] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая ширина детали 'W' ('".$vals[$i12]["attributes"]["W"]."') для кромкования кромкой ('".$ban12["nb12"]."')  ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'L' от 70 до 100 мм, то 'W' должна быть больше 100 мм.<br>";
                    }
                    if (($vals[$i12]["attributes"]["W"]>=70)&&($vals[$i12]["attributes"]["W"]<100)&&($vals[$i12]["attributes"]["L"]<100))
                    { $error.="<br>[/0042] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                    - недопутимая длина детали 'L' ('".$vals[$i12]["attributes"]["L"]."') для кромкования кромкой ('".$ban12["nb12"]."') ".$ban12["wb12"]."*".$ban12["tb12"]."  мм. Если 'W' от 70 до 100 мм, то 'L' должна быть больше 100 мм.<br>";
                    }
                
                }
            } 
        
        }
        if($v412)
        {
           
            $tr=$vals[$i12]["attributes"]["ID"];
           
            //echo "<hr>".count($v412)."<hr>".count($v412[$tr]);
            //exit;
            for ($i=0;$i<count($v412[$tr]);$i++)
            { 
               // print_r_($v412[$i]);
               
                if($i412[$tr][$i]["GR"])
                {
                    if (($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["DW"])AND($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["W"]))
                    {
                        
                        $error.="<br>[/0043] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (DW ".$vals[$i12]["attributes"]["DW"]." * DL ".$vals[$i12]["attributes"]["DL"].")
                            - высота DY XNC ".$v412[$tr][$i][0]["attributes"]["DY"]." мм обработки не совпадает с длиной детали 'DW' ('".$vals[$i12]["attributes"]["DW"]."' мм)
                            либо с длиной детали 'W' ('".$vals[$i12]["attributes"]["W"]."' мм)";
                    }
                    if (($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["DL"])AND($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["L"]))
                    {
                        $error.="<br>[/0044] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (DW ".$vals[$i12]["attributes"]["DW"]." * DL ".$vals[$i12]["attributes"]["DL"].")
                            - ширина DX XNC ".$v412[$tr][$i][0]["attributes"]["DX"]." мм не совпадает с шириной детали  'DL' ('".$vals[$i12]["attributes"]["DL"]."' мм)
                            либо с шириной детали  'L' ('".$vals[$i12]["attributes"]["L"]."' мм)";
                    }
                }
                else
                {
                    if ((!$i412[$tr][$i]["MS"])AND(!$i412[$tr][$i]["ML"])AND(!$i412[$tr][$i]["MA"])AND(!$i412[$tr][$i]["MAC"]))
                    {
                        if ($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["DW"])
                        {
                            
                            $error.="<br>[/0045] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - высота DY XNC ".$v412[$tr][$i][0]["attributes"]["DY"]." мм обработки не совпадает с длиной детали 'DW' ('".$vals[$i12]["attributes"]["DW"]."' мм)";
                        }
                        if ($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["DL"])
                        {
                            $error.="<br>[/0046] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - ширина DX XNC ".$v412[$tr][$i][0]["attributes"]["DX"]." мм не совпадает с шириной детали  'DL' ('".$vals[$i12]["attributes"]["DL"]."' мм)";
                        }
                    }
                    else
                    {
                        if ($v412[$tr][$i][0]["attributes"]["DY"]<>$vals[$i12]["attributes"]["W"])
                        {
                            
                            $error.="<br>[/0126] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - высота DY XNC ".$v412[$tr][$i][0]["attributes"]["DY"]." мм обработки не совпадает с длиной детали 'W' ('".$vals[$i12]["attributes"]["W"]."' мм)";
                        }
                        if ($v412[$tr][$i][0]["attributes"]["DX"]<>$vals[$i12]["attributes"]["L"])
                        {
                            $error.="<br>[/0127] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - ширина DX XNC ".$v412[$tr][$i][0]["attributes"]["DX"]." мм не совпадает с шириной детали  'L' ('".$vals[$i12]["attributes"]["L"]."' мм)";
                        }
                    }
                }
                if ($v412[$tr][$i][0]["attributes"]["DZ"]<>$t12)
                {
                    $error.="<br>[/0047] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - толщина DZ XNC ".$v412[$tr][$i][0]["attributes"]["DZ"]." мм обработки не равна толщине детали 'Т' ('".$t12."').";
                }
                $query1112="SELECT * FROM `tools`";
                $result1112 = mysqli_query($link, $query1112) or die("Ошибка " . mysqli_error($link)); 
                if($result1112)
                {
                    $rows1112 = mysqli_num_rows($result1112); // количество полученных строк
                    for ($i1111 = 0 ; $i1111 < $rows1112 ; ++$i1111)
                    {
                        $tools_from_DB[] = mysqli_fetch_array($result1112);
                        
                    }
                }
                foreach ($tools_from_DB as $o21212)
                {
                    $name_tools_from_DB[$o21212["tools_id"]]=$o21212["name"];
                    $diameter_tools_from_DB[$o21212["tools_id"]]=$o21212["diameter"];
                    if((int)($o21212["depth"]))
                    {
                        $depth_tools_from_DB[$o21212["tools_id"]]=$o21212["depth"];
                    }   
                    else
                    {
                        if (strpos($o21212["depth"],"+"))
                    {
                            $rrrr=explode("+",$o21212["depth"]);
                            foreach ($rrrr as $rrr)
                            {
                                    if ($rrr=="dz") $rrr=$t12;
                                    $rr+=$rrr;
                            } 
                            $depth_tools_from_DB[$o21212["tools_id"]]=$rr;
                            unset($rrrr,$rrr,$rr);
                    }
                    if (strpos($o21212["depth"],"-"))
                    {
                            $rrrr=explode("-",$o21212["depth"]);
                            foreach ($rrrr as $rrr)
                            {
                                    if ($rrr=="dz") 
                                    {
                                        $rrr=$t12;
                                        $rr+=$rrr;
                                    }
                                    else $rr-=$rrr;
                            } 
                            $depth_tools_from_DB[$o21212["tools_id"]]=$rr;
                            unset($rrrr,$rrr,$rr);
                    }
                    }
                }
                
                foreach ($v412[$tr] as $k1=>$o1)
                {
                  //print_r_($v412[$tr][0][0]["attributes"]);exit;
                    foreach ($o1 as $k2=>$o2)
                    {
                       // test ($k2,"k2");print_r_($o2["attributes"]);
                        foreach ($o2["attributes"] as $k3=>$o3)
                        {
                            unset($act);
                            if (substr_count($o3,"+")>0)
                            {
                                $ar=explode("+",$o3);
                                $act="plus";
                               // print_r_($ar);
                            }
                            elseif (substr_count($o3,"-")>0)
                            {
                                $ar=explode("-",$o3);
                                $act="minus";
                                //print_r_($ar);
                            }
                            if($act)
                            {
                               
                                if (mb_strtoupper($ar[0])=="DX")
                                {
                                    $ar[0]=$v412[$tr][0][0]["attributes"]["DX"];
                                   //print_r_($ar);
                                }
                                elseif (mb_strtoupper($ar[0])=="DY")
                                {
                                    $ar[0]=$v412[$tr][0][0]["attributes"]["DY"];
                                    //print_r_($ar);
                                }
                                elseif (mb_strtoupper($ar[0])=="DZ")
                                {
                                    $ar[0]=$v412[$tr][0][0]["attributes"]["DZ"];
                                    //print_r_($ar);
                                }
                                if($ar[0]>0)
                                {
                                    if ($act=="plus")
                                    {
                                        //test ("перед",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                        $v412[$tr][$k1][$k2]["attributes"][$k3]=$ar[0]+$ar[1];
                                        //test ("после",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                    }
                                    elseif ($act=="minus")
                                    {
                                    //test ("перед",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                        $v412[$tr][$k1][$k2]["attributes"][$k3]=$ar[0]-$ar[1];
                                       // test ("после",$v412[$tr][$k1][$k2]["attributes"][$k3]);
                                    }
                                }
                            }
                        }
                    }
                }
                unset($k1,$k2,$k3,$o1,$o2,$o3,$ar,$act);
                foreach ($i412[$tr][$i]["TOOL"] as $ik121)
                {
                    //print_r_($v412[$tr][$i][$ik121]);
                    //echo "!!!";
                    if ((!in_array($v412[$tr][$i][$ik121]["attributes"]["D"],$diameter_tools_from_DB))AND(substr_count(mb_strtoupper($v412[$tr][$i][$ik121]["attributes"]["NAME"]),"BORE")>0))
                    {
                        $error.="<br>[/0048] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - диаметр D инструмента ".$v412[$tr][$i][$ik121]["attributes"]["D"]." мм не обнаружен в нашей базе инструмента. <br>
                        Допустимые диаметры (".implode(",",array_unique($diameter_tools_from_DB)).") мм.<br>";
                    }
                }
                if (!$vals[$i33]["attributes"]["TURN"]) $vals[$i33]["attributes"]["TURN"]=0;
                foreach ($i412[$tr][$i]["BF"] as $ik121)
                {
                  
                    
                    if (!$v412[$tr][$i][$ik121]["attributes"]["NAME"])
                    {
                        $v412_temp=$ik121-1;
                        while ($v412[$tr][$i][$v412_temp]["tag"]<>"TOOL")
                        {
                            $v412_temp--;
                        }
                        $v412_diam=$v412[$tr][$i][$v412_temp]["attributes"]["D"];
                        $v412_tool=$v412[$tr][$i][$v412_temp];
                        unset ($v412_temp);
                        //print_r_($ik121);exit;
                    }
                    else
                    {
                        //echo "!!!";exit;
                        $v412_diam=substr($v412[$tr][$i][$ik121]["attributes"]["NAME"],5);
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0049] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина фронтального сверления DP ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм".$turn_text."
                        больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>4200)
                    {
                        $error.="<br>[/0050] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if(!$turn)
                    {
                        if ( $v412[$tr][$i][0]["attributes"]["DX"]<200)
                        {
                            $error.="<br>[/0051] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                        }
                    }
                    else
                    {
                        if ( $v412[$tr][$i][0]["attributes"]["DY"]<200)
                        {
                            $error.="<br>[/0051] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 200 мм.<br>";
                        }
                    }
                        if ( $v412[$tr][$i][0]["attributes"]["DY"]>1250)
                    {
                        $error.="<br>[/0052] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1250 мм.<br>";
                    }
                    if(!$turn)
                    {
                        if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                        {
                            $error.="<br>[/0053] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - размер детали фронтального сверления в программе для сверления DX".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                        }
                    }
                    else
                    {
                        if ( $v412[$tr][$i][0]["attributes"]["DX"]<70)
                        {
                            $error.="<br>[/0053] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - размер детали фронтального сверления в программе для сверления DY".$turn_text."
                            ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 70 мм.<br>";
                        }
                    }
                    if ($vals[$i12]["attributes"]["DL"]>4200)
                    {
                        $error.="<br>[/0054] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали для фронтального сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (!$turn)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["X"]>$v412[$tr][$i][0]["attributes"]["DX"])
                        {
                            $error.="<br>[/0055] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата фронтального сверления X".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["X"]." больше чем длина программы детали DX ".$v412[$tr][$i][0]["attributes"]["DX"]."  мм. <br>";
                        }
                        if ($v412[$tr][$i][$ik121]["attributes"]["Y"]>$v412[$tr][$i][0]["attributes"]["DY"])
                        {
                            $error.="<br>[/0056] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата фронтального сверления Y".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." больше чем ширина программы детали DW ".$v412[$tr][$i][0]["attributes"]["DY"]."  мм. <br>";
                        }
                    }
                    elseif ($turn)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["Y"]>$v412[$tr][$i][0]["attributes"]["DX"])
                        {
                            $error.="<br>[/0055] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата фронтального сверления X".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." больше чем длина программы детали DX ".$v412[$tr][$i][0]["attributes"]["DX"]."  мм. <br>";
                        }
                        if ($v412[$tr][$i][$ik121]["attributes"]["X"]>$v412[$tr][$i][0]["attributes"]["DY"])
                        {
                            $error.="<br>[/0056] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата фронтального сверления Y".$turn_text." ".$v412[$tr][$i][$ik121]["attributes"]["X"]." больше чем ширина программы детали DY ".$v412[$tr][$i][0]["attributes"]["DY"]."  мм. <br>";
                        }
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["DP"]<0)OR($v412[$tr][$i][$ik121]["attributes"]["DP"]>30)OR(($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12-3))AND($v412[$tr][$i][$ik121]["attributes"]["DP"]<$t12)))
                    {
                        if($v412[$tr][$i][$ik121]["attributes"]["DP"]<39)
                        {
                            $error.="<br>[/0057] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - глубина фронтального сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна ".($t12-3)." мм, либо сквозное более ".$t12." мм, максимальная глубина сверления 39 мм.<br>";
                        }
                        else 
                        {
                            $error.="<br>[/0058] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - глубина фронтального сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 39 мм.<br>";
                        }
                    }
                    if (!$turn)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["AC"]>1)
                        {
                            if (!$v412[$tr][$i][$ik121]["attributes"]["AS"]) 
                            {
                                $t121_dw= $ik121-1;
                                while ($v412[$tr][$i][$t121_dw]["tag"]<>"TOOL")
                                {
                                    $t121_dw--;
                                } 
                                $v412[$tr][$i][$ik121]["attributes"]["AS"]=$v412[$tr][$i][$t121_dw]["attributes"]["D"];
                            }
                            if ($v412[$tr][$i][$ik121]["attributes"]["AV"]=="true")
                            {
                                $ac1=$v412[$tr][$i][$ik121]["attributes"]["Y"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["Y"]);
                                if($ac1>$v412[$tr][$i][0]["attributes"]["DY"])
                                {
                                    echo "<hr><hr>";
                                    $error.="<br>[/0059] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по вертикали (".$ac1." мм) более ширины программы детали DY ".$v412[$tr][$i][0]["attributes"]["DY"]." мм";
                                }
                            }
                            else
                            {
                                $ac=$v412[$tr][$i][$ik121]["attributes"]["X"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["X"]);
                                if($ac>$v412[$tr][$i][0]["attributes"]["DX"])
                                {
                                    $error.="<br>[/0060] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по горизонтали (".$ac." мм) более длины программы детали DX ".$v412[$tr][$i][0]["attributes"]["DX"]." мм";
                                }
                            }
                        }
                    }
                    elseif ($turn)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["AC"]>1)
                        {
                            if (!$v412[$tr][$i][$ik121]["attributes"]["AS"]) 
                            {
                                $t121_dw= $ik121-1;
                                while ($v412[$tr][$i][$t121_dw]["tag"]<>"TOOL")
                                {
                                    $t121_dw--;
                                } 
                                $v412[$tr][$i][$ik121]["attributes"]["AS"]=$v412[$tr][$i][$t121_dw]["attributes"]["D"];
                            }
                            if ($v412[$tr][$i][$ik121]["attributes"]["AV"]=="true")
                            {
                                $ac1=$v412[$tr][$i][$ik121]["attributes"]["Y"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["Y"]);
                                if($ac1>$v412[$tr][$i][0]["attributes"]["DX"])
                                {
                                    echo "<hr><hr>";
                                    $error.="<br>[/0059] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по вертикали (".$ac1." мм) более ширины программы детали DY ".$v412[$tr][$i][0]["attributes"]["DX"]." мм";
                                }
                            }
                            else
                            {
                                $ac=$v412[$tr][$i][$ik121]["attributes"]["X"]+(($v412[$tr][$i][$ik121]["attributes"]["AC"]-1)*$v412[$tr][$i][$ik121]["attributes"]["AS"]);
                                //print_r_($v412[$tr][$i][$ik121]);
                                //test ("ac1",$v412[$tr][$i][$ik121]["attributes"]["X"]);
                                if($ac>$v412[$tr][$i][0]["attributes"]["DY"])
                                {
                                    $error.="<br>[/0060] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                    - массив отверстий фронтального сверления (количество - ".$v412[$tr][$i][$ik121]["attributes"]["AC"].", шаг - ".$v412[$tr][$i][$ik121]["attributes"]["AS"]." мм) по горизонтали (".$ac." мм) более длины программы детали DX ".$v412[$tr][$i][0]["attributes"]["DY"]." мм";
                                }
                            }
                        }
                    }
                }
                foreach ($i412[$tr][$i]["BL"] as $ik121)
                {
                    
                    if (!$v412[$tr][$i][$ik121]["attributes"]["NAME"])
                    {
                        $v412_temp=$ik121-1;
                        while ($v412[$tr][$i][$v412_temp]["tag"]<>"TOOL")
                        {
                            $v412_temp--;
                        }
                        $v412_diam=$v412[$tr][$i][$v412_temp]["attributes"]["D"];
                        $v412_tool=$v412[$tr][$i][$v412_temp];
                        unset ($v412_temp);
                        //print_r_($ik121);exit;
                    }
                    else
                    {
                        //echo "!!!";exit;
                    $v412_diam =trim( preg_replace('/[^0-9 , .]/', '', $v412[$tr][$i][$ik121]["attributes"]["NAME"]));
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0061] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина левого сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0062] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]<200)
                    {
                        $error.="<br>[/0063] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]>1250)
                    {
                        $error.="<br>[/0064] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1250 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0065] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для левого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0066] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали для левого сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Y"]>($v412[$tr][$i][0]["attributes"]["DY"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["Y"]<-20))
                    {
                        $error.="<br>[/0067] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - координата Y ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." левого сверления".$turn_text."  
                        не допустима, высота детали DY, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DY"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))OR(($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                        {
                            $error.="<br>[/0128] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z  для левого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            отсутствует. <br>";
                        }
                        else
                        {
                            if(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                            {
                            // print_r_($v412[$tr][$i]);
                                $error.="<br>[/0068] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для левого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                                не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                            }
                            else
                            {
                                $error.="<br>[/0069] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для левого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                                не допустима для материала толщиною ".$t12." мм.";
                            }
                        }
                    }
                    if ( $v412_diam>($t12-6))
                    {
                        $error.="<br>[/0070] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - толщина материала ".$t12." мм левого сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0071] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина левого сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["BR"] as $ik121)
                {
                   
                    if (!$v412[$tr][$i][$ik121]["attributes"]["NAME"])
                    {
                        $v412_temp=$ik121-1;
                        while ($v412[$tr][$i][$v412_temp]["tag"]<>"TOOL")
                        {
                            $v412_temp--;
                        }
                        $v412_diam=$v412[$tr][$i][$v412_temp]["attributes"]["D"];
                        $v412_tool=$v412[$tr][$i][$v412_temp];
                        unset ($v412_temp);
                        //print_r_($ik121);exit;
                    }
                    else
                    {
                        //echo "!!!";exit;
                        $v412_diam =trim( preg_replace('/[^0-9 , .]/', '', $v412[$tr][$i][$ik121]["attributes"]["NAME"]));
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0072] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина правого сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0073] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]<200)
                    {
                        $error.="<br>[/0074] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]>1250)
                    {
                        $error.="<br>[/0075] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1250 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0076] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для правого сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0077] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали для правого сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Y"]>($v412[$tr][$i][0]["attributes"]["DY"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["Y"]<-20))
                    {
                        $error.="<br>[/0078] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - координата Y ".$v412[$tr][$i][$ik121]["attributes"]["Y"]." правого  сверления".$turn_text."  
                        не допустима, высота детали DY, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DY"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))OR(($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                       
                       // echo $t12-($v412_diam/2+3)."!!".($v412_diam/2+3)."<hr>";
                        if(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                        {
                            $error.="<br>[/0079] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для правого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                            
                        }
                        else
                        {
                            $error.="<br>[/0080] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для правого сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима для материала толщиною ".$t12." мм.";
                            /*print_r_($v412[$tr][$i]);
                            print_r_($v412_tool);
                            print_r_($v412[$tr][$i][$ik121]);
                            exit;*/
                        }
                       
                    }
                    if ($v412_diam>($t12-6))
                    {
                        $error.="<br>[/0081] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - толщина материала ".$t12." мм правого  сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0082] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина правого сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["BT"] as $ik121)
                {
                   
                    if (!$v412[$tr][$i][$ik121]["attributes"]["NAME"])
                    {
                        $v412_temp=$ik121-1;
                        while ($v412[$tr][$i][$v412_temp]["tag"]<>"TOOL")
                        {
                            $v412_temp--;
                        }
                        $v412_diam=$v412[$tr][$i][$v412_temp]["attributes"]["D"];
                        $v412_tool=$v412[$tr][$i][$v412_temp];
                        unset ($v412_temp);
                        //print_r_($ik121);exit;
                    }
                    else
                    {
                        //echo "!!!";exit;
                        $v412_diam =trim( preg_replace('/[^0-9 , .]/', '', $v412[$tr][$i][$ik121]["attributes"]["NAME"]));
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0083] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина верхнего сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0084] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]<200)
                    {
                        $error.="<br>[/0085] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]>1250)
                    {
                        $error.="<br>[/0086] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1250 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0087] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для верхнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0088] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали для верхнего сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["X"]>($v412[$tr][$i][0]["attributes"]["DX"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["X"]<-20))
                    {
                        $error.="<br>[/0089] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - координата X ".$v412[$tr][$i][$ik121]["attributes"]["X"]." верхнего  сверления".$turn_text."  
                        не допустима, высота детали DX, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DX"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))OR(($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                        {
                            $error.="<br>[/0090] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для верхнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                        }
                        else
                        {
                            $error.="<br>[/0091] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для верхнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима для материала толщиною ".$t12." мм.";
                        }
                    }
                    if ($v412_diam>($t12-6))
                    {
                        $error.="<br>[/0092] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - толщина материала ".$t12." мм  верхнего  сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0093] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина верхнего сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["BB"] as $ik121)
                {
                    
                    if (!$v412[$tr][$i][$ik121]["attributes"]["NAME"])
                    {
                        $v412_temp=$ik121-1;
                        while ($v412[$tr][$i][$v412_temp]["tag"]<>"TOOL")
                        {
                            $v412_temp--;
                        }
                        $v412_diam=$v412[$tr][$i][$v412_temp]["attributes"]["D"];
                        $v412_tool=$v412[$tr][$i][$v412_temp];
                        unset ($v412_temp);
                        //print_r_($ik121);exit;
                    }
                    else
                    {
                        //echo "!!!";exit;
                        $v412_diam =trim( preg_replace('/[^0-9 , .]/', '', $v412[$tr][$i][$ik121]["attributes"]["NAME"]));
                    }
                    if (($v412_diam==2)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>4))
                    {
                        $error.="<br>[/0094] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина нижнего сверления DP".$turn_text."
                        ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм больше максимально допустимого 4 мм для сверла диаметром 2 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]>3100)
                    {
                        $error.="<br>[/0095] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DX"]<200)
                    {
                        $error.="<br>[/0096] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DX"]." меньше минимально допустимого размера 200 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]>1250)
                    {
                        $error.="<br>[/0097] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." больше максимально допустимого размера 1250 мм.<br>";
                    }
                    if ( $v412[$tr][$i][0]["attributes"]["DY"]<70)
                    {
                        $error.="<br>[/0098] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали в программе для нижнего сверления DX".$turn_text."
                        ".$v412[$tr][$i][0]["attributes"]["DY"]." меньше минимально допустимого размера 70 мм.<br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]>3100)
                    {
                        $error.="<br>[/0099] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - размер детали для нижнего сверления DL".$turn_text."
                        ".$vals[$i12]["attributes"]["DL"]." больше максимально допустимого размера 3100 мм.<br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["X"]>($v412[$tr][$i][0]["attributes"]["DX"]-20))OR($v412[$tr][$i][$ik121]["attributes"]["X"]<-20))
                    {
                        $error.="<br>[/0100] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - координата X ".$v412[$tr][$i][$ik121]["attributes"]["X"]." нижнего сверления".$turn_text."  
                        не допустима, высота детали DX, от 20 мм до ".($v412[$tr][$i][0]["attributes"]["DX"]-20)."  мм. <br>";
                    }
                    if(!$v412[$tr][$i][$ik121]["attributes"]["Z"])
                    {
                        $v412[$tr][$i][$ik121]["attributes"]["Z"]= $v412[$tr][$i][0]["attributes"]["DZ"]/2;
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["Z"]>=($t12-( $v412_diam/2+3)))OR(($v412[$tr][$i][$ik121]["attributes"]["Z"]<=( $v412_diam/2+3))))
                    {
                        if(($t12-($v412_diam/2+3))>($v412_diam/2+3))
                        {
                            $error.="<br>[/0101] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для нижнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима. DZ для материала толщиною ".$t12." мм возможно от ".($v412_diam/2+3)." мм до ".($t12-($v412_diam/2+3))."  мм. <br>";
                        }
                        else
                        {
                            $error.="<br>[/0102] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - координата Z ".$v412[$tr][$i][$ik121]["attributes"]["Z"]." для нижнего сверления диаметром ". $v412_diam." мм левого сверления".$turn_text."  
                            не допустима для материала толщиною ".$t12." мм.";
                        }
                    }
                    if ($v412_diam>($t12-6))
                    {
                        $error.="<br>[/0103] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - толщина материала ".$t12." мм нижнего сверления".$turn_text."  
                        не достаточна для выбранного диаметра сверления ".$v412_diam." мм. Допустимый диаметр сверления до ".($t12-6)."  мм. <br>";
                    }
                    if($v412[$tr][$i][$ik121]["attributes"]["DP"]>36)
                    {
                        $error.="<br>[/0104] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина нижнего сверления ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм не возможна. Глубина сверления должна быть менее или равна 36 мм.";
                    }
                }
                foreach ($i412[$tr][$i]["MS"] as $ik121)
                {
                   

                    foreach ($i412[$tr][$i]["TOOL"] as $v412temp2)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["NAME"]==$v412[$tr][$i][$v412temp2]["attributes"]["NAME"])
                        {
                        $v412_temp4=$v412[$tr][$i][$v412temp2]["attributes"]["D"];
                        //echo $v412_temp4; 
                        }
                    }
                    if (!in_array($v412_temp4,$v412_diam_mill))
                    {
                        $error.="<br>[/0105] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - диаметр фрезы ".$v412_temp4." мм не используется у нас в производстве. 
                        Допустимые диаметры (".implode(",",array_unique($v412_diam_mill)).").";
                    }
                    else
                {
                        if (($v412[$tr][$i][$ik121]["attributes"]["DP"]>$v412_diam_mill_depth[$v412_temp4]))
                        {
                            $error.="<br>[/0106] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более возможной (".$v412_diam_mill_depth[$v412_temp4]." мм) для диаметра ".$v412_temp4." мм.";
                        }
                    }
                    if ($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12+3))
                    {
                        $error.="<br>[/0107] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более допустимой глубины ".($t12+3)." мм <br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["DP"]<$t12)AND($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12-3))AND($v412[$tr][$i][$ik121]["attributes"]["DP"]<1))
                    {
                        $error.="<br>[/0108] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм недопустима. Диапазон глубины фрезерования от 1 мм до ".($t12-3)." мм, либо от ".$t12." мм до 39 мм. <br>";
                    }
                   // print_r_($i412[$tr][$i]); print_r_($v412[$tr][$i]);
                    if ((count($i412[$tr][$i]["MA"])>0)OR(count($i412[$tr][$i]["MAC"])>0)OR(count($i412[$tr][$i]["ML"]>1)))
                    {
                        if ($vals[$i12]["attributes"]["DL"]<100)
                        {
                            $error.="<br>[/0109] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 100 мм <br>";
                        }
                        if ($vals[$i12]["attributes"]["DW"]<100)
                        {
                            $error.="<br>[/0110] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 100 мм <br>";
                        }
                        if (($vals[$i12]["attributes"]["DW"]>100)AND($vals[$i12]["attributes"]["DW"]<350)AND($vals[$i12]["attributes"]["DL"]<350))
                        {
                            $error.="<br>[/0111] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 350 мм, при условии что ширина DW более 100 мм и менее 350 мм <br>";
                        }
                        if (($vals[$i12]["attributes"]["DL"]>100)AND($vals[$i12]["attributes"]["DL"]<350)AND($vals[$i12]["attributes"]["DW"]<350))
                        {
                            $error.="<br>[/0112] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 350 мм, при условии что длина DL более 100 мм и менее 350 мм <br>";
                        }
                    }
                    else
                    {
                        
                        if ($vals[$i12]["attributes"]["DL"]<100)
                        {
                            $error.="<br>[/0129] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для обработки на ручном раскроечном станке  100 мм.<br>";
                        }
                        if ($vals[$i12]["attributes"]["DW"]<100)
                        {
                            $error.="<br>[/0130] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для обработки на ручном раскроечном станке 100 мм.<br>";
                        }
                    }

                }
                foreach ($i412[$tr][$i]["GR"] as $ik121)
                {
                   
                   
                    foreach ($i412[$tr][$i]["TOOL"] as $v412temp2)
                    {
                        if ($v412[$tr][$i][$ik121]["attributes"]["NAME"]==$v412[$tr][$i][$v412temp2]["attributes"]["NAME"])
                        {
                        $v412_temp4=$v412[$tr][$i][$v412temp2]["attributes"]["D"];
                        //echo $v412_temp4; 
                        }
                    }
                    if ($v412_temp4<>4)
                    {
                        if (!in_array($v412_temp4,$v412_diam_mill))
                        {
                            $error.="<br>[/0113] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                            - диаметр фрезы ".$v412_temp4." мм не используется у нас в производстве. 
                            Допустимые диаметры (".implode(",",array_unique($v412_diam_mill)).").";
                        }
                        else
                        {
                            if (($v412[$tr][$i][$ik121]["attributes"]["DP"]>$v412_diam_mill_depth[$v412_temp4]))
                            {
                                $error.="<br>[/0114] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                                - глубина фрезерования ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более возможной (".$v412_diam_mill_depth[$v412_temp4]." мм) для диаметра ".$v412_temp4." мм.";
                            }
                        }
                    }
                    if ($v412[$tr][$i][$ik121]["attributes"]["DP"]>($t12-5))
                    {
                        $error.="<br>[/0115] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина паза ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм более допустимой глубины ".($t12-5)." мм <br>";
                    }
                    if (($v412[$tr][$i][$ik121]["attributes"]["X1"]<>$v412[$tr][$i][$ik121]["attributes"]["X2"])AND($v412[$tr][$i][$ik121]["attributes"]["Y1"]<>$v412[$tr][$i][$ik121]["attributes"]["Y2"]))
                    {
                        $error.="<br>[/0116] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - паз должен быть строго вертикальным или строго горизонтальным, сейчас координаты паза (начало - ".$v412[$tr][$i][$ik121]["attributes"]["X1"].", ".$v412[$tr][$i][$ik121]["attributes"]["Y1"]."// конец  ".$v412[$tr][$i][$ik121]["attributes"]["X2"].", ".$v412[$tr][$i][$ik121]["attributes"]["Y2"].") <br>";
                    }
                    if ($v412[$tr][$i][$ik121]["attributes"]["DP"]<1)
                    {
                        $error.="<br>[/01117] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - глубина паза ".$v412[$tr][$i][$ik121]["attributes"]["DP"]." мм должна быть более 1 мм <br>";
                    }
                    if ($vals[$i12]["attributes"]["DL"]<100)
                    {
                        $error.="<br>[/0118] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 100 мм <br>";
                    }
                    if ($vals[$i12]["attributes"]["DW"]<100)
                    {
                        $error.="<br>[/0119] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 100 мм <br>";
                    }
                    if (($vals[$i12]["attributes"]["DW"]>100)AND($vals[$i12]["attributes"]["DW"]<350)AND($vals[$i12]["attributes"]["DL"]<350))
                    {
                        $error.="<br>[/0120] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - длина DL детали ".$vals[$i12]["attributes"]["DL"]." мм менее допустимой для фрезерования 350 мм, при условии что ширина DW более 100 мм и менее 350 мм <br>";
                    }
                    if (($vals[$i12]["attributes"]["DL"]>100)AND($vals[$i12]["attributes"]["DL"]<350)AND($vals[$i12]["attributes"]["DW"]<350))
                    {
                        $error.="<br>[/0121] Деталь '".$vals[$i12]["attributes"]["NAME"]."' (id='".$vals[$i12]["attributes"]["ID"]."') (материал '".$n12."') (W ".$vals[$i12]["attributes"]["W"]." * L ".$vals[$i12]["attributes"]["L"].")
                        - ширина DW детали ".$vals[$i12]["attributes"]["DW"]." мм менее допустимой для фрезерования 350 мм, при условии что длина DL более 100 мм и менее 350 мм <br>";
                    }

                }
            }
            //test("id",$tr);
           // print_r_($v412[$tr]);
        }
        if($error)
        {
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["ID"].=$vals[$i12]["attributes"]["ID"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["NAME"].=$vals[$i12]["attributes"]["NAME"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["DL"].=$vals[$i12]["attributes"]["DL"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["L"].=$vals[$i12]["attributes"]["L"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["DW"].=$vals[$i12]["attributes"]["DW"];
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["W"].=$vals[$i12]["attributes"]["W"];
            //$check_error_part[$vals[$i12]["attributes"]["ID"]]["PROGRAM"]=$v412_pr;
            $check_error_part[$vals[$i12]["attributes"]["ID"]]["ERROR"].=$error;
        }
        
        //print_r_($depth_tools_from_DB);
        //print_r_($diameter_tools_from_DB);
        //print_r_($v412);
        //print_r_($i412);
        //echo "!!
         //<br>";
        unset($turn_text,$i,$ban121,$turn, $error,$v412_diam,$v412_pr,$t121_dw,$t121_dl,$v4122,$k2121211,$k12333,$o21219,$o212199,$ac,$gr12,$gr112,$mat12,$w12,$t12,$l12,$n12,$ban12["wb12"],$ban12["tb12"],$mat12,$ban12,$ban12["nb12"],$r, $pr12, $v412, $i412,$depth_tools_from_DB,$v412,$i412,$rrrr,$rrr,$rr,$o21212,
        $name_tools_from_DB, $diameter_tools_from_DB, $depth_tools_from_DB,$ik121);
        
        
       
        //exit;
    }

}
if($check_error_part)
{
    print_r_($check_error_part);
    foreach ($check_error_part as $v12344)
    {
        $error.=$v12344["ERROR"];
    }
    unset ($v12344);
}
elseif ((!$check_error_part)AND($_SESSION["vals"])) echo "Ошибок не обнаружено!";
?>
