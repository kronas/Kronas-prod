<?php
session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

require 'vendor/autoload.php';
use GuzzleHttp\Client;
$db = "giblabdb";
date_default_timezone_set('Europe/Kiev');
#mysqli_set_charset($link, "utf8");
$link = mysqli_connect("localhost", "giblab", "85Da4892", $db);
mysqli_set_charset($link, "utf8");
mysqli_autocommit($link, FALSE);
$query12="SELECT * FROM `PROJECT_IN` WHERE db_in IS NULL";
$result12 = mysqli_query($link, $query12) or die("Ошибка " . mysqli_error($link)); 
if($result12)
{
	$rows12 = mysqli_num_rows($result12); // количество полученных строк
	$res12 = mysqli_fetch_array($result12);
	echo "<pre>";
	print_r($res12);
	echo "</pre>";
	$file=file_get_contents("files/".$res12["file"]);
	if (substr($file,0,5)<>"<?xml")
	{
		$query121="DELETE FROM `PROJECT_IN` WHERE id=".$res12["id"].";";
		$result121 = mysqli_query($link, $query121) or die("Ошибка " . mysqli_error($link));
		if ($result121)
		{
			echo "<hr>Удаляем запись с id = ".$res12["id"]."<hr>";
			unlink("files/".$res12["file"]);
			unlink("files64/".$res12["file"]."64");
		} 
		
	} 
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   
    <title>Загрузка в БД проекта с проверкой</title>
</head>
<body>



    <?php
    $input_project_data = $file;
    $input_project_data = str_replace("\n","", $input_project_data);
    include_once("func.php");
    $p = xml_parser_create();
    xml_parse_into_struct($p, $input_project_data, $vals1, $index1);
    xml_parser_free($p);
    $vals1=vals_out($vals1);
    $index1=make_index($vals1);
    include("check_shablon_production_possibilities.php");
	function build_req_for_project($project){
	  $project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $project);
          $req =  <<<rr
          <?xml version="1.0" encoding="UTF-8"?>
          <request comment="PDF Report" action="4EC657AF">
          <param value="project.name" name="nameTemplate"/>
          <param value="true" name="save"/>
          <param value="true" name="xncCalcExpr"/>
          <tool name="" diameter="" plane="" number=""/>
          rr;
          $req .= $project.'</request>';
          
          return $req;
	};

        $data = array(
            'action' => '4EC657AF',
            'reqData' => build_req_for_project($input_project_data)
        );
         
        function send_post_data($endpoint, $data)
        {
          $client = new Client(['base_uri' => $GLOBALS['send_xml_link']]);
          $r = $client->request('post', $endpoint, [
            'headers'  => [
              'Content-Type' => 'application/json; charset=UTF-8',
	    ],
            'json' => $data
          ]); 
          return json_decode($r->getBody()->getContents());
        }

        $response = send_post_data('/send_xml', $data);
        if (!isset($response->project)) {
          $project_data = $input_project_data;
          echo "!!! нет ответа";exit;
        } else {
          $project_data = $response->project;
        }
       // file_put_contents("upload/last_req.xml",$project_data);
       // echo "<pre>".$project_data."</pre>";

		$main_text1=$project_data;	
		$form_no=1;
		$orderweb=$res12["orderweb"];
		$order1c=$res12["order1c"];
    $client=$res12["client"];
    $DB_AC_IN=date_create()->format('Y-m-d H:i:s');
    include("1.php");
    if($error=="") $query1211="UPDATE `PROJECT_IN` SET db_in=1, check_error=1  WHERE id=".$res12["id"].";";
		else $query1211="UPDATE `PROJECT_IN` SET db_in=1, error=\"".htmlspecialchars($error)."\" WHERE id=".$res12["id"].";";
		echo $query1211;
		$result1211 = mysqli_query($link, $query1211) or die("Ошибка " . mysqli_error($link));
		session_destroy();

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }

		if (!mysqli_commit($link)) {
			print("Не удалось зафиксировать транзакцию\n");
			exit();
		}
    mysqli_close($link);

?>
</body>
</html>
