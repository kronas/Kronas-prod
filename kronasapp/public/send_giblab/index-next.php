<?php session_start(); 
//echo "<pre>".print_r($_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/jsLists.css">
    <script src="js/jsLists.js"></script>
    <script src="a1.js"></script>
    <title>Title</title>
</head>
<body>


<form method="post" enctype='multipart/form-data' action="index.php">
    <?php
    $sheet_not_one=array();
    $band_not_one=array();
    include_once('functions1.php');
    include_once('funct.php');
    //print_r($_POST);
    if (!$_POST["project_data"] && !isset($_POST['form_step'])&&(!$_FILES))
    {
        echo "Выберите файл: <input type='file' name='filename' size='10' ><br>
                <input type='submit' value='Загрузить' >";
    }
    elseif ($_POST["project_data"])
    {
       
        $main_text1 = $_POST["project_data"];
        
        //header ("Content-Type:text/xml");
        unlink("/home/varvar/public_html/upload/1234.txt");
        if (!file_put_contents("/home/varvar/public_html/upload/1234.txt", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".$main_text1))
        {
            echo "!!!";
            exit;
        }
        $host  = $_SERVER['HTTP_HOST'];
        $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $extra = 'open_xml.php';
        header("Location: http://$host$uri/$extra".'?nw='.$_GET['nw']);
        
        //exit;
         //unlink($fullpath);
    }
    if ($_FILES)
    {
        
        $name = $_FILES['filename']['name'];
	    $fullpath = __DIR__.'/upload/'. $_FILES["filename"]['name'];
        //move_uploaded_file($_FILES['filename']['tmp_name'], $fullpath);
        $main_text1=file_get_contents($fullpath);
        create_data($main_text1);
        //print_r($_SESSION["vals"]);
        $sheet=array();
        $band=array();
        create_data_step2($sheet, $band);
        if(count($sheet) > 0) {
            $sheet_one = db_get_arr_one_in($sheet, 'MATERIAL');
            $sheet_not_one = get_arr_invert($sheet, $sheet_one);
        }
        if(count($band) > 0) {
            $band_one = db_get_arr_one_in($band, 'BAND');
            $band_not_one = get_arr_invert($band, $band_one);
        }
        //print_r($sheet_not_one);        print_r($band_not_one); exit();
        $_POST['form_step']=0;
        if(count($sheet_not_one) == 0 && count($band_not_one)==0) $_POST['form_step']=1;
    }
    ?>
<?php


if ($_POST['form_step']==0 && (count($sheet_not_one) > 0 || count($band_not_one) > 0)) {


    $vals=$_SESSION['vals'];
    $index=$_SESSION['index'];
      
    if (count($sheet_not_one) > 0) {
        ?>
        <div class="row">
            <div class="col-10">не найден sheet</div>

        </div>
        <?php

        foreach ($sheet_not_one as $key => $item) {
            ?>
            <div class="row" style="border: 2px dotted gray; margin: 20px;">
                <div class="col-6">
                    <?php
                    echo $vals[$key]['attributes']['TYPENAME'] . ' (' . $vals[$key]['attributes']['UNIT'] . ')<br>' . $vals[$key]['attributes']['NAME'];
                    echo '<br>Детали:<br>';
                    echo get_parts($vals, $index, $vals[$key]['attributes']['ID']);
                    ?>
                </div>
                <div class="col-6">
                    <div id="slectedcode-<?= $key ?>"></div>
                    <input type="hidden" name="slectedid-<?= $key ?>" class="form-control" id="slectedid-<?= $key ?>">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-typeop="1"
                            data-target="#selectPart"
                            data-targetid="<?= $key ?>">Выбрать замену
                    </button>
                </div>

            </div>
            <?php
        }
    }
    if (count($band_not_one) > 0) {
        ?>
        <div class="row">
            <div class="col-10">не найден band</div>

        </div>
        <?php

        foreach ($band_not_one as $key => $item) {
            ?>
            <div class="row" style="border: 2px dotted gray; margin: 20px;">
                <div class="col-6">
                    <?php
                    echo $vals[$key]['attributes']['TYPENAME'] . ' (' . $vals[$key]['attributes']['UNIT'] . ')<br>' . $vals[$key]['attributes']['NAME'];
                    echo '<br>Детали:<br>';
                    echo get_parts($vals, $index, $vals[$key]['attributes']['ID']);
                    ?>
                </div>
                <div class="col-6">
                    <div id="slectedcode-<?= $key ?>"></div>
                    <input type="hidden" name="slectedid-<?= $key ?>" class="form-control" id="slectedid-<?= $key ?>">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-typeop="2"
                            data-target="#selectPart"
                            data-targetid="<?= $key ?>">Выбрать замену
                    </button>
                </div>

            </div>
            <?php
        }
    }
    echo '  <input type="hidden" name="form_step" value="1">';
    echo '    <button  type="submit" class="btn btn-success" disabled name="op" value="1">Далее</button>';

}
elseif (isset($_POST) && isset($_POST['form_step']) && (int) $_POST['form_step']==1) {
    $vals=$_SESSION['vals'];
    $index=$_SESSION['index'];
    //print_r_($vals);  
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 10) == 'slectedid-') {
            $_key = str_replace('slectedid-', '', $key);
            $vals[$_key]['attributes']['CODE'] = $value;
        }
    }

    $_SESSION['vals']= $vals;
    ?>
    <input type="hidden" name="form_step" value="2"> 
    <div class="row">
        <div class="col-2">

            <label for="user_type_edge">тип кромкования</label>
            <select class="form-control" id="user_type_edge" name="user_type_edge">
                <option value="pur">ПУР</option>
                <option value="eva">ЭВА</option>
            </select>

        </div>

        <div class="col-2">

            <label for="user_place">Производственный участок</label>
            <select class="form-control" id="user_place" name="user_place">
                <?=  get_user_place_data() ?>
            </select>

        </div>
        <div class="col-2">
            <div class="wrap-user-cutting">

                <p>Вид раскроя </p>
                <input type="radio" checked name="user_cutting" value="auto">Авто<br>
                <input type="radio" name="user_cutting" value="manual">Ручной<br>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-12">

            <table border="2" cellpadding="2" cellspacing="2" class="table">
                <thead>
                <tr>
                    <th scope="col">Название</th>
                    <th scope="col">T</th>
                    <th scope="col">DL</th>
                    <th scope="col">DW</th>
                    <th scope="col">кромковка</th>
                    <th scope="col">ЧПУ</th>
                    <th scope="col">криволинейное<br>кромкование</th>
                    <th scope="col">Сшивка</th>
                </tr>
                </thead>

                <?php
                $_groupid = 0;
                $arr_parts = array();
                $materials = '';
                get_arr_parts($vals, $index, $arr_parts, $materials);
                $band_for_select=$_SESSION['band_for_select'];
                $_SESSION['arr_parts']= $arr_parts;
                $type_double=get_list_type_double();
//                echo '<pre>'; print_r($arr_parts);                echo '</pre>';
                foreach ($arr_parts as $key => $row) {
                    if ($_groupid != $row['group_id']) {
                        echo '<tr><td colspan="6" align="center" style="font-weight: bold;"  >' . $row['group_name'] . '</td></tr>';
                        $_groupid = $row['group_id'];
                    }

                    echo '<tr><td >' . $row['NAME'] . '</td><td>';
                    echo $row['T'] . '</td><td>';
                    echo $row['DL'] . '</td><td>';
                    echo $row['DW'] . '</td><td>';
                    echo ($row['edge'] == 1 ? 'да' : 'нет') . '</td><td>';
                    echo ($row['XNC'] > 0 ? 'да' : 'нет') . '</td><td>';
                    if ($row['XNC'] > 0 && $row['XNC_COUNTMILL'] > 0) {
                        echo '<input type="checkbox" name="xnc_countmill_' . $row['ID'] . '" value="1">+';
                        echo '<span style="display: none;" class="xnc_group_' . $row['ID'] . '">';
                        echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_r" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_r>право</label>';
                        echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_l" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_l>лево</label>';
                        echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_t" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_t>верх</label>';
                        echo '<input type="checkbox" checked name="xnc_el_' . $row['ID'] . '_b" value="1" style="margin-left: 10px;"><label for=""xnc_el_' . $row['ID'] . '_b>низ</label>';
                        echo '</span>';


                        echo '<br><input type="hidden" name="xnc_id_' . $row['ID'] . '" id="xnc_id_' . $row['ID'] . '" value="' . $row['XNC'] . '">';
                        echo '    <label style="display: none;" for="xnc_band_' . $row['ID'] . '">Кромка</label>
                <select style="display: none;" class="form-control" name="xnc_band_' . $row['ID'] . '" id="xnc_band_' . $row['ID'] . '">
                   
                    ' . $band_for_select . '</select>';
                    }
                    echo '</td><td>';
                        if((int)$row['T']<=20) {
                            echo '<input type="checkbox" name="stitching_' . $row['ID'] . '" value="1">+<br>';
                            echo '    <label style="display: none;" for="stitching_band_' . $row['ID'] . '">Кромка</label>
                <select style="display: none;" class="form-control" name="stitching_band_' . $row['ID'] . '" id="stitching_band_' . $row['ID'] . '">
                   
                    ' . $band_for_select . '<option value="DB">Из БД</option></select>';
                            echo '<div id="slectedcode-' . $row['ID'] . '"></div>';
                            echo '<input type="hidden" name="slectedid-' . $row['ID'] . '" class="form-control" id="slectedid-' . $row['ID'] . '">';
                            echo '<button style="display: none;" type="button" class="btn btn-primary" id="btn_band_' . $row['ID'] . '" data-toggle="modal" data-typeop="4" 
                     data-t="' . $row['T'] . '" data-target="#selectPart"
                            data-targetid="' . $row['ID'] . '">Выбрать замену</button><br>';
                            echo '<label style="display: none;"  for="type_double_' . $row['ID'] . '">подложка</label>
        <select style="display: none;"  class="form-control" name="type_double_' . $row['ID'] . '" id="type_double_' . $row['ID'] . '">' . $type_double . '</select>
        <div style="display: none;"  class="alert alert-warning" role="alert" id="type_double_descripts_' . $row['ID'] . '">

</div>
        <select style="display: none;" class="form-control" name="back_double_' . $row['ID'] . '" id="back_double_' . $row['ID'] . '" data-code="' . $row['CODE'] . '">' . $materials . '</select>
        <label style="display: none;"  for="band_inner_' . $row['ID'] . '">кромкование внутренней части сшивки</label>
        <select class="form-control" style="display: none;"  name="band_inner_' . $row['ID'] . '"  id="band_inner_' . $row['ID'] . '">
        <option value="none" data-t="0" >none</option>
        ' . $band_for_select . '</select>';
                        }
                    echo '</td></tr>';
                }
                ?>
            </table>

        </div>
    </div>
    <button type="submit" class="btn btn-success" name="op" value="1">Далее</button>
    <?php

}
elseif (isset($_POST) && isset($_POST['form_step']) && (int) $_POST['form_step']==2) {
    $vals=$_SESSION['vals'];
    $arr_parts=$_SESSION['arr_parts'];
    $arr_out=array();
    if (isset($_POST['user_type_edge'])) $arr_out['user_type_edge'] = $_POST['user_type_edge'];
    if (isset($_POST['user_place'])) $arr_out['user_place'] = $_POST['user_place'];
    if (isset($_POST['user_cutting'])) $arr_out['user_cutting'] = $_POST['user_cutting'];
    foreach ($arr_parts as $key => $value) {
        $_id=$value['ID'];
        if(isset($_POST['xnc_countmill_'.$_id]) || isset($_POST['stitching_'.$_id])) {
            $arr_out[$_id] = array();
            if (isset($_POST['xnc_countmill_' . $_id]))
            {
                $arr_out[$_id]['xnc_countmill'] = $_POST['xnc_countmill_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_KL']=1;
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_BAND']=$_POST['xnc_band_' . $_id];
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELL']=$_POST['xnc_el_' . $_id . '_l']==1?'1':'0';
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELR']=$_POST['xnc_el_' . $_id . '_r']==1?'1':'0';
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELT']=$_POST['xnc_el_' . $_id . '_t']==1?'1':'0';
                $vals[$_POST['xnc_id_' . $_id]]['attributes']['MY_ELB']=$_POST['xnc_el_' . $_id . '_b']==1?'1':'0';
            }
            $arr_out[$_id]['part_id'] = $_id;
            if (isset($_POST['xnc_band_' . $_id])) $arr_out[$_id]['xnc_band'] = $_POST['xnc_band_' . $_id];
            if (isset($_POST['xnc_id_' . $_id])) $arr_out[$_id]['xnc_id'] = $_POST['xnc_id_' . $_id];
//            if (isset($_POST['stitching_' . $_id])) $arr_out[$_id]['stitching'] = $_POST['stitching_' . $_id];
            // slectedid-41
            if (isset($_POST['stitching_band_' . $_id]))
            {
                $arr_out[$_id]['band_id'] = $_POST['stitching_band_' . $_id]=='DB'?$_POST['slectedid-' . $_id]:$_POST['stitching_band_' . $_id];
            }
            if (isset($_POST['slectedi' . $_id])) $arr_out[$_id]['slected'] = $_POST['slectedi' . $_id];
            if (isset($_POST['type_double_' . $_id])) $arr_out[$_id]['type_double'] = $_POST['type_double_' . $_id];
            if (isset($_POST['back_double_' . $_id])) $arr_out[$_id]['back_double'] = $_POST['back_double_' . $_id];
            if (isset($_POST['band_inner_' . $_id])) $arr_out[$_id]['band_edge'] = $_POST['band_inner_' . $_id];

        }
    }

    echo '<pre>';print_r($arr_out);echo '</pre>';
    echo '<pre>';print_r($arr_parts);echo '</pre>';
    echo '<pre>';print_r($vals);echo '</pre>';
}

?>
</form>
<div class="modal fade" id="selectPart" tabindex="-1" role="dialog" aria-labelledby="selectPartLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="selectPartLabel">Найдите замену</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="targetid" name="targetid" value="">
                <input type="hidden" id="typeop" name="typeop" value="">
                <input type="hidden" id="t" name="t" value="">
                <div class="row">
                    <div class="col-md-4">
                        <?php
                        echo get_folder_tree();
                        ?>
                    </div>
                    <div class="col-md-8">
                        <?php
                        echo get_list_data('MATERIAL');
                        echo get_list_data('BAND');
                        ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
