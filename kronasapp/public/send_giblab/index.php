<?php
session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

require '../../vendor/autoload.php';
use GuzzleHttp\Client;
date_default_timezone_set('Europe/Kiev');

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   
    <title>Загрузка в БД проекта с проверкой</title>
</head>
<body>





    <?php

    echo 'This page work!';


    $input_project_data = $_POST['project_data'];

    function send_post_data($project_data)
            {
              $client = new Client();
                ////$r = $client->request('POST', 'http://service.kronas.com.ua:11080/kronasapp/public/send_xml',[
                $r = $client->request('POST', $GLOBALS['send_xml_link'].'/send_xml',[
                    'form_params' => [
                        'action' => '4EC657AF',
                        'reqData' => build_req_for_project($project_data),
                    ],
                    'timeout' => 10,
                    ///'debug' => true,
                    'allow_redirects' => true
                ]);
                return $r->getBody();
            }

    function build_req_for_project($project){
        $project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $project);
        $req =  <<<rr
              <?xml version="1.0" encoding="UTF-8"?>
              <request comment="PDF Report" action="4EC657AF">
              <param value="project.name" name="nameTemplate"/>
              <param value="true" name="save"/>
              <param value="true" name="xncCalcExpr"/>
              <tool name="" diameter="" plane="" number=""/>
          rr;
        $req .= $project.'</request>';

        return $req;
    };

    $response = send_post_data($input_project_data);
    $array = json_decode($response, true);
    var_dump($array);


     if (!isset($array['project'])) {
         $project_data = $input_project_data;
         echo "!!! нет ответа";exit;
       } else {
         $project_data = $array['project'];
         var_dump($project_data);
       }

        session_destroy();

        //.. Восстанавливаем сессию
        if (isset($session_restore['project_manager'])) {
            $_SESSION['project_manager'] = $session_restore['project_manager'];
        }
        if (isset($session_restore['project_client'])) {
            $_SESSION['project_client'] = $session_restore['project_client'];
        }
        if (isset($session_restore['user'])) {
            $_SESSION['user'] = $session_restore['user'];
        }
        if (isset($session_restore['tec_info'])) {
            $_SESSION['tec_info'] = $session_restore['tec_info'];
        }
?>
</body>
</html>
