<?php

function connect_db()
{
    $host = 'localhost';
    $database = 'giblabdb';
    $user = 'giblab';
    $password = '85Da4892';
    $db = mysqli_connect($host, $user, $password, $database)    or die("Ошибка " . mysqli_error($db));
    $db->set_charset("utf8");
    return $db;
}

function db_get_arr_one_in($in,$type)
{
    $db=connect_db();
    $sql = "SELECT tbl.`code` FROM $type as tbl WHERE
tbl.`code` IN (".implode(',',$in).") AND (SELECT COUNT(*) FROM $type as t_in WHERE t_in.`code` = tbl.`code`)=1" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
function db_get_tree_OLD($type)
{
    $db=connect_db();
    $sql = "SELECT DISTINCT
root.FOLDER_ID,
root.`NAME` as fname,
root.PARENT,
root.1CCODE,
root.`CODE`
FROM
GOOD_TREE AS root
WHERE
root.PARENT = 0  AND root.FOLDER_ID IN ( SELECT  
trp.PARENT 
FROM
GOOD_TREE as trp
INNER JOIN GOOD_TREE_{$type}_CONN as pc ON pc.FOLDER_ID = trp.FOLDER_ID ) 
UNION 
SELECT DISTINCTROW 
tr.FOLDER_ID,
tr.`NAME`,
tr.PARENT,
tr.1CCODE,
tr.`CODE`
FROM
GOOD_TREE as tr
INNER JOIN GOOD_TREE_{$type}_CONN ON GOOD_TREE_{$type}_CONN.FOLDER_ID = tr.FOLDER_ID 
ORDER BY fname ASC" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
function db_get_tree()
{
    $db=connect_db();
    $sql = "SELECT DISTINCT
root.FOLDER_ID,
root.`NAME` as fname,
root.PARENT,
root.1CCODE,
root.`CODE`
FROM
GOOD_TREE AS root
ORDER BY fname ASC" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
function db_get_list_data($type)
{
    $_field=$type=='MATERIAL'?'m.L as L':'m.T as L';
    $db=connect_db();
    $sql = "SELECT
m.`NAME`,
m.TYPENAME,
{$_field},
m.W, m.CODE, 
t.FOLDER_ID
FROM
{$type} AS m
LEFT JOIN GOOD_TREE_{$type}_CONN AS t ON t.{$type}_ID = m.{$type}_ID
ORDER BY
m.`NAME` ASC

" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}

function db_get_list_type_double()
{
    $db=connect_db();
    $sql = "SELECT
dt.DOUBLE_TYPE_ID,
dt.`NAME`,
dt.DESCRIPTION,
dt.SAME,
dt.EDGE
FROM
DOUBLE_TYPE AS dt

" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
function get_list_data($type)
{
    $arr=db_get_list_data($type);
    $out='<ul class="list-data-tree-'.strtolower($type).'" style="max-height: 80vh; overflow: scroll;">';
    foreach ($arr as $item)
    {
        $out.='<li class="folid-'.$item['FOLDER_ID'].'"  data-code="'.$item['CODE'].'"  data-w="'.$item['W'].'"> '.
            $item['NAME'] . ' {'.$item['TYPENAME'] .  ') '.$item['L'] . ' X '.$item['W'] . '</li>';
    }

    $out.='</ul>';
    return $out;
}
function get_list_type_double()
{
    $arr=db_get_list_type_double();
    $out='';
    foreach ($arr as $item)
    {
        $out.='<option value="'.$item['DOUBLE_TYPE_ID'] .  '"  data-description="'.$item['DESCRIPTION'].'" data-same="'.$item['SAME'].'" data-edge="'.$item['EDGE'].'"  >'.$item['NAME'] .  '</option>';
    }
    return $out;
}
function db_get_user_place_data()
{
    $db=connect_db();
    $sql = "SELECT
PLACES.PLACES_ID,
PLACES.`NAME`
FROM
PLACES
ORDER BY
PLACES.`NAME` ASC" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
function db_get_user_place_tool_data()
{
    $db=connect_db();
    $sql = "SELECT 
TOOL_CUTTING_EQUIPMENT.PLACE,
TOOL_CUTTING.TYPE
FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
    inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID
WHERE
TOOL_CUTTING.PRODUCT = 'wt'" ;

    $result = mysqli_query($db, $sql) or die("Ошибка " . mysqli_error($db));
    $tablerows =$result->fetch_all( MYSQLI_ASSOC );
    mysqli_close($db);
    return $tablerows;
}
function get_user_place_data()
{
    $_pt=db_get_user_place_tool_data();
    $pt=array();
    $_js_auto=array();
    $_js_manual=array();
    foreach ($_pt as $key=>$value)
    {
        if(!isset($pt[$value['PLACE']])) $pt[$value['PLACE']]=array('auto'=>0,'manual'=>0);
        $pt[$value['PLACE']][$value['TYPE']]=1;
        switch ($value['TYPE'])
        {
            case 'auto':
                $_js_auto[]=$value['PLACE'];
                break;
            case 'manual':
                $_js_manual[]=$value['PLACE'];
                break;
        }
    }
    echo '<script type="text/javascript">
                                var arr_place_auto = ['.implode(',', $_js_auto).'];  
                                var arr_place_manual = ['.implode(',', $_js_manual).'];  
                                </script>';
    $arr=db_get_user_place_data();
    $out='';
    foreach ($arr as $item)
    {//  data-auto="'.$item['PLACES_ID'].'"
       $out.='<option value="'.$item['PLACES_ID'].'">'.$item['NAME'].'</option>';
    }
    return $out;
}
function get_folder_tree()
{
    $tablerows=db_get_tree();
$tree = form_tree($tablerows);


return build_tree($tree, 0, $type);
    }
function get_arr_invert($in, $out)
{
    $return = $in;
        foreach ($out as $k=>$v) {
            if(in_array($v['code'],$return))
            {
                foreach ($return as $k2=>$v2) {
                    if($v2==$v['code']) unset($return[$k2]);
                }
            }
        }
    return  $return;
}

function get_parts($vals, $index, $id)
{
    $objectID=0;
    $rez=array();
        foreach ($index['MATERIAL'] as $k=>$v) if($vals[$v]['attributes']['ID']==$id) $objectID=$v;
    $out='';
    if($objectID>0){
        $_range=array('min'=>0,'max'=>count($vals));
        $out='<table style="border: 1px solid gray;" border="2"><tr><td>W</td><td>L</td><td>Name</td></tr>';
        get_parts_range($index, $objectID, $_range);
/*
        echo  '<br>objectID='.$objectID.'<pre>';
        print_r($_range);
        echo  '</pre>';
        */
$_arr_part=array();
        for($i=$_range['min'];$i<=$_range['max'];$i++) {
            if ($vals[$i]['tag'] == 'PART') {
                $_arr_part[] = $vals[$i]['attributes']['ID'];
            }
        }

        foreach ($vals as $_key=>$_item)
        {
            if(isset($_item['tag']) && isset($_item['attributes']) && isset($_item['attributes']['ID']) && $_item['tag']=='PART' && in_array($_item['attributes']['ID'],$_arr_part ))
            {
                for($t=($_key-1);$t>=0;$t--)
                {
                    if($vals[$t]['tag']!='PART') {
                        if (isset($vals[$t]['attributes']['TYPEID']) && $vals[$t]['attributes']['TYPEID']=='product')
                        {
                            if(isset($vals[$_key]['attributes']['NAME'])) $name=$vals[$_key]['attributes']['NAME'];
                            $out.='<tr><td>'.$vals[$_key]['attributes']['W'].'</td><td>'.
                                $vals[$_key]['attributes']['L'].'</td><td>'.$name.'</td></tr>';

                        }
                        break;
                    }
                }
            }
        }

        $out.='</table>';
    }
    return $out;
}

function get_parts_range($index, $id, &$_range)
{
    foreach ($index['OPERATION'] as $k=>$v)
    {
        if($v>=$_range['min'] && $v<=$id) $_range['min']=$v;
        if($v<$_range['max'] && $v>$id) $_range['max']=$v;
    }
    $_range['max']=$_range['max']-1;
}

function form_tree($mess)
{
    if (!is_array($mess)) {
        return false;
    }
    $tree = array();
    foreach ($mess as $value) {
        $tree[$value['PARENT']][] = $value;
    }
    return $tree;
}
function build_tree($cats, $parent_id, $type)
{
    if (is_array($cats) && isset($cats[$parent_id])) {
        $tree = $parent_id==0?'<ul id="simple_list">':'<ul>';
        foreach ($cats[$parent_id] as $cat) {
            $tree .= '<li  data-folderid="'. $cat['FOLDER_ID'].'"><i class="fa fa-file-text-o" aria-hidden="true"></i><span>' . $cat['fname'].'</span>';
            $tree .= build_tree($cats, $cat['FOLDER_ID'],$type);
            $tree .= '</li>';
        }
        $tree .= '</ul>';
    } else {
        return false;
    }
    return $tree;
}

function get_arr_parts($vals, $index, &$arr_parts, &$materials)
{
    $arr_t_sheet=get_arr_t_sheet($vals);
    $arr_operation=get_arr_operation($vals);
    $arr_sheets=get_arr_sheets($vals);
    ///echo '<pre>'; print_r($arr_sheets); echo '</pre>';

    $arr_t_parts=get_arr_t_parts($arr_operation, $arr_sheets);
    $arr=array();
    $tmp=array('start'=>0,'end'=>0,'id'=>0,'NAME'=>'');

    $_materials=array();
    foreach ($vals as $k=>$v)
    {

        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='open' && isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='product')
        {
            $tmp['start']=$k;
            $tmp['id']=$v['attributes']['ID'];
            $tmp['NAME']=$v['attributes']['NAME'];


        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='open' && isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='sheet')
        {
            $__name=trim($v['attributes']['NAME']);
            if(strlen($__name)>0 && substr_count($__name,'сшивка')==0) {
                $_materials[] = array(
                    'arr_id' => $k,
                    'T' => $v['attributes']['T'],
                    'NAME' => $__name,
                    'ID' => $v['attributes']['ID'],
                    'CODE' => $v['attributes']['CODE'],
                );
            }
        }

        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='close')
        {
            $tmp['end']=$k;
            if($tmp['start']>0) $arr[$tmp['id']]=$tmp;
            $tmp=array('start'=>0,'end'=>0,'id'=>0,'NAME'=>'');


        }
    }
//    echo '<pre>'; print_r($arr); echo '</pre>';

    $arr2=array();
    foreach ($arr as $k=>$v)
    {
        for($i=$v['start'];$i<=$v['end'];$i++)
        {
            if(isset($vals[$i]['tag']) && $vals[$i]['tag']=='PART' )
            {
                $arr2[$i]=array(
                    'group_id'=>$v['id'],
                    'group_name'=>$v['NAME'],
                    'index'=>$i,
                       'edge'=>0,
                    'XNC'=>0,
                    'ID'=>$vals[$i]['attributes']['ID'],
                    'CODE'=>$arr_t_parts[$vals[$i]['attributes']['ID']]['CODE'],
                    'T'=>$arr_t_parts[$vals[$i]['attributes']['ID']]['T'],
                    'DL'=>$vals[$i]['attributes']['DL'],
                    'DW'=>$vals[$i]['attributes']['DW'],
                    'NAME'=>$vals[$i]['attributes']['NAME'],
                );
                if(isset($vals[$i]['attributes']['ELT']) || isset($vals[$i]['attributes']['ELB']) || isset($vals[$i]['attributes']['ELL']) || isset($vals[$i]['attributes']['ELR']) )
                {
                    $arr2[$i]['edge']=1;

                    foreach ($vals as $k2=>$v2)
                    {

                        if(isset($v2['tag']) && isset($v2['type']) && $v2['tag']=='OPERATION' && $v2['type']=='open' &&
                            isset($v2['attributes']['TYPEID']) && $v2['attributes']['TYPEID']=='XNC' &&
                           isset($vals[$k2+1]['attributes']['ID']) && $vals[$k2+1]['attributes']['ID']==$vals[$i]['attributes']['ID'])
                        {
                            $arr2[$i]['XNC']=$k2;
                            $arr2[$i]['XNC_COUNTMILL']=$v2['attributes']['COUNTMILL'];
                            $arr2[$i]['XNC_CODE']=$v2['attributes']['CODE'];

                        }


                    }
                }


            }
        }
    }
//    echo '<pre>'; print_r($_materials); echo '</pre>';
    foreach ($_materials as $k=>$v)
    {
        $materials.='<option value="'.$v['ID'].'" data-t="'.$v['T'].'" data-code="'.$v['CODE'].'">'.$v['NAME'].'</option>';
/*
             [arr_id] => 74
            [T] => 16
            [NAME] => ЛДСП-16 U112 PE Пепел (Пепельный)-КрУ
            [ID] => 13
            [CODE] => 1539
 */
    }

    $arr_parts= $arr2;
}
function get_arr_t_parts($arr_operation, $arr_sheets)
{
    $out=array();
    foreach ($arr_operation as $k => $v) {
        foreach ($v['PART'] as $k2 => $v2) {
            $out[$v2]['T']=$arr_sheets[$v['MATERIAL']]{'T'};
            $out[$v2]['CODE']=$arr_sheets[$v['MATERIAL']]{'CODE'};
        }
    }
    return $out;
}
function get_arr_operation($vals)
{
    $out = array();
    $tmp=array('start'=>0,'end'=>0,'id'=>0,'T'=>'', 'MATERIAL'=>0,'PART'=>array());
    foreach ($vals as $k => $v) {
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='OPERATION' && $v['type']=='open' &&
            isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='CS')
        {
            $tmp['start']=$k;
            $tmp['id']=$v['attributes']['ID'];
            $tmp['T']=$v['attributes']['T'];


        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='MATERIAL' && $v['type']=='complete' && $tmp['start']>0)
        {
            $tmp['MATERIAL']=$v['attributes']['ID'];
        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='PART' && $v['type']=='complete' && $tmp['start']>0)
        {
            $tmp['PART'][]=$v['attributes']['ID'];
        }
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='OPERATION' && $v['type']=='close' && $tmp['start']>0)
        {
            $tmp['end']=$k;
            $out[]=$tmp;
            $tmp=array('start'=>0,'end'=>0,'id'=>0,'T'=>'', 'MATERIAL'=>0,'PART'=>array());
        }

    }
    return $out;
}
function get_arr_sheets($vals)
{
    $out = array();
    $tmp=array('start'=>0,'end'=>0,'id'=>0,'T'=>'', 'MATERIAL'=>0,'PART'=>array());
    foreach ($vals as $k => $v) {
        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='open' &&
            isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='sheet')
        {
            $out[$v['attributes']['ID']]=array('T'=>$v['attributes']['T'],'CODE'=>$v['attributes']['CODE']);


        }
    }
    return $out;
}
function get_arr_t_sheet($vals)
{
    $out=array();
    foreach ($vals as $k=>$v)
    {

        if(isset($v['tag']) && isset($v['type']) && $v['tag']=='GOOD' && $v['type']=='complete' && isset($v['attributes']['TYPEID']) && $v['attributes']['TYPEID']=='tool.cutting')
        {
            if(isset($vals[$k+1]['tag']) && isset($vals[$k+1]['type']) && $vals[$k+1]['tag']=='GOOD' && $vals[$k+1]['type']=='open' &&
                 isset($vals[$k+1]['attributes']['TYPEID']) && $vals[$k+1]['attributes']['TYPEID']=='sheet' && isset($vals[$k+1]['attributes']['T']))
            {
                $out[$v['attributes']['ID']]=$vals[$k+1]['attributes']['T'];
        }


        }
    }
    return $out;
}
include_once("func.php");
function create_data($main_text1)
{
    $main_text1=str_replace("\n","",$main_text1);

    $simple = $main_text1;
    $p = xml_parser_create();
    xml_parse_into_struct($p, $simple, $vals, $index);
    xml_parser_free($p);
    $vals=vals_out($vals);
    $index=make_index($vals);
    //print_r_($vals);  
    $_SESSION['vals']=$vals;
    $_SESSION['index']=$index;
}
function create_data_step2(&$sheet, &$band)
{
    $vals=$_SESSION['vals'];
    $index=$_SESSION['index'];


    $band_for_select='';
    $_band_for_select=array();
    foreach ($index['GOOD'] as $_good)
    {
        if(isset($vals[$_good]['attributes']) && isset($vals[$_good]['attributes']['TYPEID']) && ((int) $vals[$_good]['attributes']['CODE'])>0 &&
            ( $vals[$_good]['attributes']['TYPEID']=='sheet' || $vals[$_good]['attributes']['TYPEID']=='band' ) ) {
            switch ($vals[$_good]['attributes']['TYPEID'])
            {
                case 'sheet':
                    $sheet[$_good]=$vals[$_good]['attributes']['CODE'];
                    break;
                case 'band':
                    $_band_for_select[$vals[$_good]['attributes']['CODE']]=array('name' => $vals[$_good]['attributes']['NAME'], 'w' => $vals[$_good]['attributes']['W']);
                    $band[$_good]=$vals[$_good]['attributes']['CODE'];
                    break;
            }
        }
    }
    foreach ($_band_for_select as $_k=>$_v)
    {
        $band_for_select.='<option value="'.$_k.'" data-w="'.$_v['w'].'">'.$_v['name'].'</option>';
    }
    $_SESSION['band_for_select']=$band_for_select;
}