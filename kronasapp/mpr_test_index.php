<?php
/**
 * Created by PhpStorm.
 * User: Tony
 * Date: 06.04.2020
 * Time: 09:58
 */
?>

<html>
<head></head>
<body>
    <button id="mpr" onclick="send()">Send</button>


    <script>
        function send() {
            // 1. Создаём новый объект XMLHttpRequest
            var request = new XMLHttpRequest();
            var url = 'https://service.giblab.com';

            request.open("POST", url, false);
            request.setRequestHeader('Content-Type', 'application/octet-stream; charset=UTF-8');
            request.setRequestHeader('Content-Length', '1024');
            request.setRequestHeader('action', '0xDFFB69BB'); //action number / Номер действия
            request.setRequestHeader('fileName', 'test.mpr');//File name / Имя файла с расширением
            request.setRequestHeader('user-agent', 'Command');
            request.responseType = 'blob';

// 3. Отсылаем запрос
            request.send('[H\n' +
                'VERSION="4.0 Alpha"\n' +
                'OP="1"\n' +
                'FM="1"\n' +
                'GP="0"\n' +
                'UP="0"\n' +
                'DW="0"\n' +
                '_BSX=764.00\n' +
                '_BSY=500.00\n' +
                '_BSZ=18.00\n' +
                ']1\n' +
                '$E0\n' +
                'KP \n' +
                'X=764\n' +
                'Y=0\n' +
                'Z=0\n' +
                '.X=764\n' +
                '.Y=0\n' +
                '.Z=0\n' +
                '$E1\n' +
                'KL\n' +
                'X=764\n' +
                'Y=500\n' +
                'Z=0\n' +
                '.X=764\n' +
                '.Y=500\n' +
                '.Z=0\n' +
                '$E2\n' +
                'KL\n' +
                'X=0\n' +
                'Y=500\n' +
                'Z=0\n' +
                '.X=0\n' +
                '.Y=500\n' +
                '.Z=0\n' +
                '$E3\n' +
                'KL\n' +
                'X=0\n' +
                'Y=0\n' +
                'Z=0\n' +
                '.X=0\n' +
                '.Y=0\n' +
                '.Z=0\n' +
                '$E4\n' +
                'KL\n' +
                'X=764\n' +
                'Y=0\n' +
                'Z=0\n' +
                '.X=764\n' +
                '.Y=0\n' +
                '.Z=0\n' +
                '[001\n' +
                'l="764"\n' +
                'KM=""\n' +
                'b="500"\n' +
                'KM=""\n' +
                'd="18"\n' +
                'KM=""\n' +
                'FNX_EXP="0"\n' +
                'KM=""\n' +
                'FNY_EXP="0"\n' +
                'KM=""\n' +
                'AX_EXP="0"\n' +
                'KM=""\n' +
                'AY_EXP="0"\n' +
                'KM=""\n' +
                'ShowNonM="_mirror AND _ymirror OR _nonmirror AND _nonymirror"\n' +
                'KM=""\n' +
                'ShowM="_mirror AND _nonymirror OR _ymirror AND _nonmirror"\n' +
                'KM=""\n' +
                'ModusMir="0"\n' +
                'KM=""\n' +
                'Kante_1="1"\n' +
                'KM=""\n' +
                'Kante_2="0"\n' +
                'KM=""\n' +
                'Kante_3="1"\n' +
                'KM=""\n' +
                'Kante_4="0"\n' +
                'KM=""\n' +
                'bohrver="1"\n' +
                'KM=""\n' +
                'bohrhor="1"\n' +
                'KM=""\n' +
                'bohruni="1"\n' +
                'KM=""\n' +
                'bohren="1"\n' +
                'KM=""\n' +
                'fraesen="1"\n' +
                'KM=""\n' +
                'fsaegen="1"\n' +
                'KM=""\n' +
                'saegen="1"\n' +
                'KM=""\n' +
                'nuten="1"\n' +
                'KM=""\n' +
                'ktasche="1"\n' +
                'KM=""\n' +
                'rtasche="1"\n' +
                'KM=""\n' +
                'abblas="1"\n' +
                'KM=""\n' +
                'leimen="1"\n' +
                'KM=""\n' +
                'kappen="1"\n' +
                'KM=""\n' +
                'bfraesen="1"\n' +
                'KM=""\n' +
                'ziehkl="1"\n' +
                'KM=""\n' +
                'Platz1_8="_mirror AND _ymirror OR _nonmirror AND _nonymirror"\n' +
                'KM=""\n' +
                'Platz4_5="_mirror AND _nonymirror OR _ymirror AND _nonmirror"\n' +
                'KM=""\n' +
                'platz="5"\n' +
                'KM=""\n' +
                '<100 \\Werkstck\\\n' +
                'LA="l"\n' +
                'BR="b"\n' +
                'DI="d"\n' +
                'FNX="FNX_EXP"\n' +
                'FNY="FNY_EXP"\n' +
                '\n' +
                'RNX="0"\n' +
                'RNY="0"\n' +
                'RNZ="0"\n' +
                '\n' +
                'AX="AX_EXP"\n' +
                'AY="AY_EXP"\n' +
                '\n' +
                '\n' +
                '<101 \\Kommentar\\\n' +
                'KM="MPR_PP VER: \t: 12,0,0,4 "\n' +
                'KM="Ordername \t: B25_3_vonios "\n' +
                'KM="Name\t: Top Shelf "\n' +
                'KM="Date\t: 22.10.19 "\n' +
                'KM="Material\t: LMDP 2800x2070x18 Kronodesign K016PW "\n' +
                'KM="Surftop\t: No_Surface "\n' +
                'KM="Surfbot: No_Surface "\n' +
                'KM="Material with grain   !! "\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XM"\n' +
                'XA="764"\n' +
                'YA="64"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XM"\n' +
                'XA="764"\n' +
                'YA="436"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '<102 \\BohrVert\\\n' +
                '??="bohrver=1 "\n' +
                'BM="LS"\n' +
                'XA="730"\n' +
                'YA="32"\n' +
                'DU="15"\n' +
                'TI="14"\n' +
                'AN="1"\n' +
                'MI="0"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XM"\n' +
                'XA="764"\n' +
                'YA="32"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '<102 \\BohrVert\\\n' +
                '??="bohrver=1 "\n' +
                'BM="LS"\n' +
                'XA="730"\n' +
                'YA="468"\n' +
                'DU="15"\n' +
                'TI="14"\n' +
                'AN="1"\n' +
                'MI="0"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XM"\n' +
                'XA="764"\n' +
                'YA="468"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XP"\n' +
                'XA="0"\n' +
                'YA="64"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XP"\n' +
                'XA="0"\n' +
                'YA="436"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '<102 \\BohrVert\\\n' +
                '??="bohrver=1 "\n' +
                'BM="LS"\n' +
                'XA="34"\n' +
                'YA="32"\n' +
                'DU="15"\n' +
                'TI="14"\n' +
                'AN="1"\n' +
                'MI="0"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XP"\n' +
                'XA="0"\n' +
                'YA="32"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '<102 \\BohrVert\\\n' +
                '??="bohrver=1 "\n' +
                'BM="LS"\n' +
                'XA="34"\n' +
                'YA="468"\n' +
                'DU="15"\n' +
                'TI="14"\n' +
                'AN="1"\n' +
                'MI="0"\n' +
                'AB="32"\n' +
                ' \n' +
                '\n' +
                '<103 \\BohrHoriz\\\n' +
                '??="bohrhor=1 "\n' +
                'BM="XP"\n' +
                'XA="0"\n' +
                'YA="468"\n' +
                'ZA="d/2"\n' +
                'DU="8"\n' +
                'TI="30"\n' +
                'AN="1"\n' +
                'AB="32"\n' +
                ' \n' +
                '<121 \\Block\\\n' +
                'XP="0.0"\n' +
                'YP="0.0"\n' +
                'ZP="0.0"\n' +
                'NM="Graphischer Kommentar"\n' +
                'DP="8"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l/2"\n' +
                'YA="15"\n' +
                'BR="l-50"\n' +
                'HE="10"\n' +
                'WI="0"\n' +
                'ROT="255"\n' +
                'GRUEN="0"\n' +
                'BLAU="0"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_1>0 AND Kante_1<2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l/2"\n' +
                'YA="15"\n' +
                'BR="l-50"\n' +
                'HE="10"\n' +
                'WI="0"\n' +
                'ROT="0"\n' +
                'GRUEN="0"\n' +
                'BLAU="255"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_1>=2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="15"\n' +
                'YA="b/2"\n' +
                'BR="b-20"\n' +
                'HE="10"\n' +
                'WI="90"\n' +
                'ROT="255"\n' +
                'GRUEN="0"\n' +
                'BLAU="0"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_2>0 AND Kante_2<2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="15"\n' +
                'YA="b/2"\n' +
                'BR="b-20"\n' +
                'HE="10"\n' +
                'WI="90"\n' +
                'ROT="0"\n' +
                'GRUEN="0"\n' +
                'BLAU="255"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_2>=2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l/2"\n' +
                'YA="b-15"\n' +
                'BR="l-50"\n' +
                'HE="10"\n' +
                'WI="0"\n' +
                'ROT="255"\n' +
                'GRUEN="0"\n' +
                'BLAU="0"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_3>0 AND Kante_3<2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l/2"\n' +
                'YA="b-15"\n' +
                'BR="l-50"\n' +
                'HE="10"\n' +
                'WI="0"\n' +
                'ROT="0"\n' +
                'GRUEN="0"\n' +
                'BLAU="255"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_3>=2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l-15"\n' +
                'YA="b/2"\n' +
                'BR="b-20"\n' +
                'HE="10"\n' +
                'WI="90"\n' +
                'ROT="0"\n' +
                'GRUEN="0"\n' +
                'BLAU="255"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_4>=2"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="line.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l-15"\n' +
                'YA="b/2"\n' +
                'BR="b-20"\n' +
                'HE="10"\n' +
                'WI="90"\n' +
                'ROT="255"\n' +
                'GRUEN="0"\n' +
                'BLAU="0"\n' +
                'STYLE="0"\n' +
                'WIDTH="5"\n' +
                '??="Kante_4>0 AND Kante_4<2"\n' +
                '<121 \\Block\\\n' +
                'XP="0.0"\n' +
                'YP="0.0"\n' +
                'ZP="0.0"\n' +
                'NM="Graphischer Kommentar"\n' +
                'DP="1"\n' +
                '<152 \\Grafischer Kommentar\\\n' +
                'NM="_spfeilr.ply"\n' +
                'LOCAL="0"\n' +
                'MD="0"\n' +
                'XA="l/2"\n' +
                'YA="b/2+b/5"\n' +
                'BR="b/3"\n' +
                'HE="100"\n' +
                'GRUEN="0"\n' +
                'BLAU="0"\n' +
                'STYLE="0"\n' +
                'WIDTH="1"\n' +
                'ROT="255"\n' +
                'WI="0"\n' +
                'WI="270"\n' +
                '!\n');

// 4. Если код ответа сервера не 200, то это ошибка
            if (request.status != 200) {
                // обработать ошибку
                alert( xhr.status + ': ' + xhr.statusText ); // пример вывода: 404: Not Found
            } else {
                // вывести результат
                alert( request.responseText ); // responseText -- текст ответа.
            }
        }
    </script>
</body>
</html>