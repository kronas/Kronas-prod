<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <title>Title</title>
    <style >
        .list-group.mode-1>li{ cursor: pointer; }
        .selected-check{
            border: 1px solid green;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
        }
        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }
        .pagination .page-item:not(.disabled),
        .data-list-line{
            cursor: pointer;
        }

    </style>
</head>
<body>


<?php
include_once('functions1.php');
?>
<form method="post" enctype='multipart/form-data'>
    <input type="hidden" id="targettr" name="targettr" value="<?=$_GET['targetid']?>">
    <?php
    //$data=get_list2browse($where, 0, 25,$typeop, $vart, $pics);
    ?>

    <div class="row disp-0">
        <div class="col-12">
        </div>
    </div>
    <div class="row disp-1">
        <div class="col-6">


            <?php
            $_parents = array();
            $active = 0;
            echo al2b_get_folders(array(0), 0, $_parents, $active);
            ?>

        </div>
        <div class="col-6">
            <?php
            $_z = array();
            echo al2b_get_folders($_parents, 1, $_z, $active);

            ?>
        </div>
    </div>


    <?php
    /* step 2 */
    //$typeop=1; if(isset($_GET['typeop']) && ((int)$_GET['typeop'])>0) $typeop=(int)$_GET['typeop'];







    $where2=array();$pics='';
//    echo '<pre>';    print_r($where);    echo '</pre>';
//    $data=al2b_get_list2browse($where2, 0, 25,$pics,602);
    ?>
    <div class="row disp-2" style="display: none;" >
        <div class="col-12 breadcrumbs alert alert-dark" role="alert">
        </div>
        <div class="col-12 disp-5" style="height: 120px; overflow: scroll;">

        </div>
    </div>
    <div class="row disp-3-1"  style="display: none;" >
        <div class="col-12" id="selected-prop"></div>
        <div class="col-6">
            <div class="disp-3" style="max-height: 80vh;overflow: scroll;"></div>
            <div class="col-md-12 text-center">
                <button type="button" id="submit" class="btn btn-success" value="filtr">Фильтровать</button>
                <button type="button" id="reset" class="btn btn-warning" value="reset">Сбросить фильтр</button>
            </div>
        </div>
        <div class="col-6  disp-4">
            <?php
         //   echo $data;
            ?>
        </div>
    </div>

</form>
<script>
    <?php
    include_once('addlist2browse.js');
    ?>
</script>
</body>
</html>