<?php
session_start();
include_once ("func.php");

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

if ($_GET["data_id"]) {
    $_SESSION = get_temp_file_data($_GET["data_id"]);
    $_SESSION["data_id"] = $_GET["data_id"];

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}

//$_SESSION["project_data"] = $_POST["project_data"];
//$_SESSION["user_place"] = $_POST["user_place"];

//$in_txt=file_get_contents('viyar.project');
$in_txt=file_get_contents($_FILES['upload_project']['tmp_name']);
//кодировка не читает русские символы, они приходят в win1251 в редакторе всё открывается. Нужно перегнать текст в нормальную кодировку.

$place=$_SESSION["user_place"];

$out=import_vp($in_txt,$place,$link);
$_SESSION['project_data'] = $out;
//echo $out;
//print_r_($_SESSION['project_data']);
//exit;
put_temp_file_data($_SESSION["data_id"], $_SESSION);
// $tmp = get_temp_file_data($_SESSION["data_id"]);
// p_($tmp);
header("Location: ".$laravel_dir."/step_1?file=" . $_SESSION["data_id"].'&nw='.$_GET['nw']);
exit;
// out запиххиваем в project_data и возвращаемся в редактор
?>