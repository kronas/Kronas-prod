<?php
include_once ('_1/config.php');
if(!$link)
{
    $db = "giblabdb";
    date_default_timezone_set('Europe/Kiev');
    $link = mysqli_connect("localhost", "giblab", "85Da4892", $db);
    mysqli_set_charset($link, "utf8");
}
//Антон
function put_temp_file_data($session_id, $data) {

    $file = $serv_main_dir.'/kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt';

    $file_data = unserialize(file_get_contents($file));
    foreach ($data as $key=>$value) {
        $file_data[$key] = $value;
    }

    file_put_contents($file, serialize($file_data));

}

function get_temp_file_data($session_id) {
    $file = $serv_main_dir.'/kronasapp/storage/app/giblab_sessions/'.$session_id.'.txt';
    return unserialize(file_get_contents($file));
}


function build_req_for_project($project, $action) {

    if ($action == '55C00560') {
        $req = <<<rr
              <?xml version="1.0" encoding="UTF-8"?>
              <request comment="PDF Report" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
              <param value="true" name="save"/>
              <param value="false" name="saveXmlCut"  />
              <tool name="" diameter="" plane="" number=""/>
          rr;
    } elseif ($action == '4EC657AF') {
        $req = <<<rr
              <?xml version="1.0" encoding="UTF-8"?>
              <request comment="PDF Report" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
              <param value="true" name="save"/>
              <param value="true" name="xncCalcExpr"/>
              <tool name="" diameter="" plane="" number=""/>
          rr;
    } elseif ($action == '7A5D19E5') {
        $req = <<<rr
              <?xml version="1.0" encoding="UTF-8"?>
              <request comment="PDF Report" action="{$action}">
              <param value="project.name" name="nameTemplate"/>
              <tool name="" diameter="" plane="" number=""/>
          rr;
    }

    $project = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '', $project);

    $req .= $project . '</request>';

    return $req;
};

function get_vals_index($project_data) {
    $project_data = xml_parser($project_data);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $project_data, $vals1, $index1);
    xml_parser_free($p);
    $_SESSION['vals'] = $vals1;
    $_SESSION['index'] = $index1;
    return [
            'vals' => $vals1,
            'index' => $index1
    ];
}


function test($text,$value)
{
    echo "<pre>
    
-----------------------
    
переменная: ".$text."

значение: ".$value."

----------------------
</pre>
";

}
function check_stock($vals,$link,$place)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="band")
        {
            foreach ($index["OPERATION"] as $d)
            {
                if (($vals[$d+1]["tag"]=="MATERIAL")&&($vals[$d]["attributes"]["TYPEID"]="EL")&&($vals[$d+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"])
                &&($vals[$d+1]["attributes"]["COUNT"]>0))
                {
                // echo "11";
                
                    $sql="select BAND_ID from BAND where CODE=".$vals[$i]["attributes"]["CODE"].";";
                    $res=mysqli_query($link,$sql);
                    $res2=mysqli_fetch_array($res);
                    if ($res2["BAND_ID"]>0)
                    {
                        $sql="select * from BAND_STOCK where BAND_ID=".$res2["BAND_ID"].";";
                        $res3=mysqli_query($link,$sql);
                        while ($res4=mysqli_fetch_array($res3))
                        {
                            $sql2="select NAME from PLACES where PLACES_ID=".$res4["PLACE_ID"].";";
                            //echo $sql2;
                            $res6=mysqli_query($link,$sql2);
                            $res7=mysqli_fetch_array($res6);
                            if (mysqli_num_rows($res6)==0)  $error_ok.="<br><br>Выберите кромку из БД пожалуйста для дальнейшей обработки.<br>";
                            $res_st[$res4["PLACE_ID"]]=array("count"=>$res4["COUNT"],"name"=>$res7["NAME"]);
                        }
                        if ($res_st)
                        {
                            if($vals[$d+1]["attributes"]["COUNT"]>$res_st[$place]["count"])
                            {
                                $error_ok.="<br>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                                " на складе <b><i>".$res_st[$place]["name"]."</i></b> нужно ".$vals[$d+1]["attributes"]["COUNT"]. ", в наличии только ".$res_st[$place]["count"].".<br><br> Оcтатки в метрах на других складах:<br><br>
                                ";
                                foreach ($res_st as $p=>$s)
                                {
                                    //print_r_($s);
                                    if (($p<>$place)&&($s["count"]>0))
                                    {
                                        $error_ok.="- ".$s["name"]." в наличии ".$s["count"]."<br>";
                                    }
                                }
                                $error_ok.="<hr>";
                            }
                            

                        }
                        else  $error_ok.="<br>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                        " отсутствует на всех складах Компании. Попробуйте выбрать иную кромку на нашем сайте.<hr>";
                       // $error_ok.="<hr>";
                    }
                }
            }  
        }
        elseif ($vals[$i]["attributes"]["TYPEID"]=="sheet")
        {
            
            if (($vals[$i+1]["tag"]=="PART")&&(!$vals[$i+1]["attributes"]["SHEETID"])&&($vals[$i+1]["attributes"]["USEDCOUNT"]>0))
            {
               // echo "11";
                $sql="select MATERIAL_ID from MATERIAL where CODE=".$vals[$i]["attributes"]["CODE"].";";
                $res=mysqli_query($link,$sql);
                $res2=mysqli_fetch_array($res);
                if ($res2["MATERIAL_ID"]>0)
                {
                    $sql="select * from MATERIAL_STOCK where MATERIAL_ID=".$res2["MATERIAL_ID"].";";
                    $res3=mysqli_query($link,$sql);
                    while ($res4=mysqli_fetch_array($res3))
                    {
                        $sql2="select NAME from PLACES where PLACES_ID=".$res4["PLACE_ID"].";";
                        //echo $sql2;
                        $res6=mysqli_query($link,$sql2);
                        $res7=mysqli_fetch_array($res6);
                        if (mysqli_num_rows($res6)==0)  $error_ok.="<br><br>Выберите материал из БД пожалуйста для дальнейшей обработки.<br>";
                        $res_st[$res4["PLACE_ID"]]=array("count"=>$res4["COUNT"],"name"=>$res7["NAME"]);
                    }
                    if ($res_st)
                    {
                        if($vals[$i+1]["attributes"]["USEDCOUNT"]>$res_st[$place]["count"])
                        {
                             $error_ok.="<br>выбранный плитный материал ".$vals[$i]["attributes"]["NAME"].
                            " на складе <b><i>".$res_st[$place]["name"]."</i></b> нужно ".$vals[$i+1]["attributes"]["USEDCOUNT"]. ", в наличии только ".$res_st[$place]["count"].".<br><br> Оcтатки в листах на других складах:<br><br>
                            ";
                            foreach ($res_st as $p=>$s)
                            {
                                //print_r_($s);
                                if (($p<>$place)&&($s["count"]>0))
                                {
                                    $error_ok.="- ".$s["name"]." в наличии ".$s["count"]."<br>";
                                }
                            }
                            $error_ok.="<hr>";
                        }
    
                    }
                    else  $error_ok.="<br>выбранный плитный материал ".$vals[$i]["attributes"]["NAME"].
                    " отсутствует на всех складах Компании. Попробуйте выбрать иной материал на нашем сайте.<hr>";
                   // $error_ok.="<hr>";
                }
            }            
        }
        unset ($res,$res2,$res3,$res4,$res_st);
    }
    return $error_ok;
    
}
function check_art($vals,$link,$place)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="band")
        {
            $sql="select BAND_ID from BAND where CODE=".$vals[$i]["attributes"]["CODE"].";";
            $res=mysqli_query($link,$sql);
            $res2=mysqli_fetch_array($res);
            if (mysqli_num_rows($res)==0)
            {
                
                $error.="<br>выбранная кромка ".$vals[$i]["attributes"]["NAME"].
                " отсутствует в БД, просьба выбрать кромку из БД.<hr>";
                // $error_ok.="<hr>";
            }
        }
        elseif (($vals[$i]["attributes"]["TYPEID"]=="sheet")&&($vals[$i]["attributes"]["MY_DOUBLE"]<>1))
        {
            $sql="select MATERIAL_ID from MATERIAL where CODE=".$vals[$i]["attributes"]["CODE"].";";
            $res=mysqli_query($link,$sql);
            $res2=mysqli_fetch_array($res);
            if (mysqli_num_rows($res)==0)
            {
                
                $error.="<br>выбранный материал ".$vals[$i]["attributes"]["NAME"].
                " отсутствует в БД, просьба выбрать материал из БД.<hr>";
                // $error_ok.="<hr>";
            }
        }
        unset ($res,$res2,$res3,$res4,$res_st);
    }
    return $error;
    
}
function part_get_edges($vals,$id)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    $a=array("L","R","B","T");
    
    foreach ($index["PART"] as $i1)
    {
        if(($vals[$i1]["attributes"]["ID"]==$id)&&($vals[$i1]["attributes"]["L"]>0))
        {
            foreach ($a as $a1)
            {
                if(($vals[$i1]["attributes"]["EL".$a1]<>"")&&($vals[$i1]["attributes"]["EL".$a1]))
                {
                    $op_num=substr(($vals[$i1]["attributes"]["EL".$a1]),strpos(($vals[$i1]["attributes"]["EL".$a1]),"#")+1);
                    //echo $op_num; exit;
                    foreach ($index["OPERATION"] as $i)
                    {
                        if (($vals[$i]["attributes"]["TYPEID"]=="EL")&&($vals[$i]["attributes"]["ID"]==$op_num))
                        {
                            foreach ($index["GOOD"] as $d)
                            {
                                if ((!$vals[$i]["attributes"]["MY_CHECK_KL"])&&($vals[$d]["attributes"]["TYPEID"]=="band")&&($vals[$d]["attributes"]["ID"]==$vals[$i+1]["attributes"]["ID"]))
                                {
                                    $res[$a1]=$vals[$d];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return $res;
    
}
function part_get_material($vals,$id)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="CS")
        {
            $g=$i+2;
            while ($vals[$g]["tag"]=="PART")
            {
               
                if ($vals[$g]["attributes"]["ID"]==$id)
                {
                    foreach ($index["GOOD"] as $r)
                    {
                        if (($vals[$r]["attributes"]["TYPEID"]=="sheet")&&($vals[$r]["attributes"]["ID"]==$vals[$i+1]["attributes"]["ID"]))
                        {
                            return $vals[$r];
                        }
                    }
                }
                $g++;
            }
        }
    }
}
function part_place_in_product($vals,$id)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($index["GOOD"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="product")
        {
            $g=$i+1;
            $j=0;
            while ($vals[$g]["tag"]=="PART")
            {
                $j++;
                if ($vals[$g]["attributes"]["ID"]==$id)
                {
                    
                    return $j;
                    exit;
                }
                $g++;
            }
        }
    }
}
function part_place_in_cs($vals,$id)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="CS")
        {
            $g=$i+2;
            $j=0;
            while ($vals[$g]["tag"]=="PART")
            {
                $j++;
                if ($vals[$g]["attributes"]["ID"]==$id)
                {
                   //print_r_($vals[$g]);exit;
                    return $j;
                } 
                $g++;
            }
        }
    }
}
function xnc_code($vals)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    foreach ($index["OPERATION"] as $i)
    {
        if ($vals[$i]["attributes"]["TYPEID"]=="XNC")
        {
            $vals[$i]["attributes"]["CODE"]=time().$i;
            
        }
    }
    return $vals;
}
function add_tag_print($vals)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    ksort($vals);
    foreach ($index["PART"] as $i)
    {
        if (($vals[$i]["attributes"]["L"]>0)&&(!$vals[$i]["attributes"]["SHEETID"]))
        {
            foreach ($vals[$i]["attributes"] as $k=>$v)
            {
                if (substr_count($k,"part.")>1)  
                {                
                    $y=1;
                    unset ($vals[$i]["attributes"][$k]);
                   // echo $k."<hr>";
                }
                if ((substr_count($k,"MY_")>0)&&(substr_count($k,"part.")==0)&&(substr_count($k,"PART.")==0)&&(!$y))
                {
                    $vals[$i]["attributes"]["part.".mb_strtolower($k)]=$v;
                   
                }
                if (substr_count($k,"PART.")>0)  
                {                
                   // $y=1;
                    unset ($vals[$i]["attributes"][$k]);
                    $vals[$i]["attributes"][mb_strtolower($k)]=$v;
                   // echo $k."<hr>";
                }
                unset ($y);
            }
        }
    }
    return $vals;
}
function move_vals($vals,$start,$num)
{
   /*test("1",count($vals));
   test("1st",$start);
   test("1num",$num);*/

    foreach($vals as $k=>$v)
   {
     if ($k>=$start)
     {
         $vals1[$k+$num]=$vals[$k];
         unset($vals[$k]);
     }
     else  $vals1[$k]=$vals[$k];
   }
   //print_r_($vals1);exit;
   
   return $vals1;
}
function calc_formula_from_db($x,$y)
{
    //echo $x."//".$y."<hr>";
    if (substr_count($x,"-")>0)
    {
        $x1=explode("-",$x);
        $x=$y-$x1[1];
        return $x;
        //echo "!!".$x."<hr>";
    }
    elseif (($x=="W")OR($x=="L"))
    {
        //echo "!!".$y."<hr>";
        return $y;
    } 
    else 
    {
        //echo "2!!".$x."<hr>";
        return $x;
    }
}
function get_cs_num($vals,$id)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    
    foreach ($index["OPERATION"] as $i)
    {
        
        if(($vals[$i]["attributes"]["TYPEID"]=="CS")&&($vals[$i]["type"]=="open"))
        {
            
            if ($vals[$i+1]["attributes"]["ID"]==$id)
            {
                return $i;
            }
          
        }
    }
}
function get_sheet_num($vals,$id)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    
    foreach ($index["OPERATION"] as $i)
    {
        
        if(($vals[$i]["attributes"]["TYPEID"]=="CS")&&($vals[$i]["type"]=="open"))
        {
            $g=$i+2;
            
            while ($vals[$g]["tag"]=="PART")
            {
              
               // print_r_($vals);exit;
                if ($vals[$g]["attributes"]["ID"]==$id)
                {
                    //echo $id;
                    foreach ($index["GOOD"] as $e)
                    {
                       
                        if (($vals[$e]["attributes"]["TYPEID"]=="sheet")&&($vals[$e]["attributes"]["ID"]==$vals[$i+1]["attributes"]["ID"]))
                        {
                            //p_($vals[$e]);
                            //exit;
                            return $e;
                        }
                    }
                }
                $g++;
            }
        }
    }
}
function do_double($vals,$place,$arr_out,$link)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    // перебираем сшивки

    foreach ($arr_out as $k=>$v)
    {
       
        if (($v["type_double"]>0)&&($v["back_double"]>0))
        {
            // дублируем лицевую деталь исходник
            foreach ($index["PART"] as $i)
            {
                if(($vals[$i]["attributes"]["ID"]==$k)&&(!$vals[$i]["attributes"]["SHEETID"])&&($vals[$i]["attributes"]["L"]>0))
                {
                    $i_part=$i;
                    $part_temp=$vals[$i];
                }
            }
            //узнаём сколько деталей на задней части
            $sql="select * from DOUBLE_CALC where DOUBLE_ID=".$v["type_double"].";";
            $sql1=mysqli_query($link,$sql);
            while($sql2=mysqli_fetch_array($sql1))
            {
                $part_back[]=$sql2;
            } 
            
            $count_part_back=count($part_back);
            $vals=move_vals($vals,$i_part+1,$count_part_back);
            //print_r_($vals[$i_part]);
            $vals[$i_part]["attributes"]["W"]= $vals[$i_part]["attributes"]["W"]+30;
            $vals[$i_part]["attributes"]["L"]= $vals[$i_part]["attributes"]["L"]+30;
            $vals[$i_part]["attributes"]["DW"]= $vals[$i_part]["attributes"]["W"];
            $vals[$i_part]["attributes"]["JW"]= $vals[$i_part]["attributes"]["W"];
            $vals[$i_part]["attributes"]["CW"]= $vals[$i_part]["attributes"]["W"];
            $vals[$i_part]["attributes"]["DL"]= $vals[$i_part]["attributes"]["L"];
            $vals[$i_part]["attributes"]["JL"]= $vals[$i_part]["attributes"]["L"];
            $vals[$i_part]["attributes"]["CL"]= $vals[$i_part]["attributes"]["L"];
            $vals[$i_part]["attributes"]["ELL"]="";
            $vals[$i_part]["attributes"]["ELR"]="";
            $vals[$i_part]["attributes"]["ELT"]="";
            $vals[$i_part]["attributes"]["ELB"]="";
            $vals[$i_part]["attributes"]["ELLMAT"]="";
            $vals[$i_part]["attributes"]["ELRMAT"]="";
            $vals[$i_part]["attributes"]["ELTMAT"]="";
            $vals[$i_part]["attributes"]["ELBMAT"]="";
            $vals[$i_part]["attributes"]["NAME"]="СШИВКА ЛИЦО id=[/".$vals[$i_part]["attributes"]["ID"]."/] вид сшивки ".$v["type_double"].
            $vals[$i_part]["attributes"]["NAME"];
            $vals[$i_part]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $vals[$i_part]["attributes"]["MY_DOUBLE"]=1;
            $vals[$i_part]["attributes"]["MY_DOUBLE_FACE"]=1;
            $vals[$i_part]["attributes"]["MY_DOUBLE_PARENT"]=0;
            //print_r_($vals[$i_part]);exit;
            $ii=new_id($vals)+1;
            $i_part1=$i_part+1;
            
            foreach ($part_back as $k1=>$v1)
            {
               // p_($v1);
                $count_part++;
                //test($part_back["L"],$vals[$i_part]["attributes"]["L"]);
                $ii++;
                $vals[$i_part1++]=Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> Array ("ID"=>$ii,"MY_DOUBLE"=>"1","MY_DOUBLE_PARENT"=>$vals[$i_part]["attributes"]["ID"],
                    "MY_DOUBLE_FACE"=>"0",
                    "L"=>calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["L"]),
                    "W"=>calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["W"]),
                    "DL"=>calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["DL"]),
                    "DW"=>calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["DW"]),
                    "part.my_double_type"=>$v["type_double"],
                    "MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"],
                    "COUNT"=>$vals[$i_part]["attributes"]["COUNT"],"USEDCOUNT"=>$vals[$i_part]["attributes"]["USEDCOUNT"],
                    "TXT"=>"false","NAME"=> "СШИВКА НИЗ id=[/".$vals[$i_part]["attributes"]["ID"]."/]. Часть ".$count_part." из ".$count_part_back." "
                    .$part_temp["attributes"]["NAME"])
                );
               // p_($vals[$i_part1-1]);
                //test("L",calc_formula_from_db($v1["L"],$vals[$i_part]["attributes"]["L"]));
                //test("W",calc_formula_from_db($v1["W"],$vals[$i_part]["attributes"]["W"]));
            // print_r_($part_back);exit;
                $parts_id[]=array($ii,"ELL"=>$v1["ELL"],"ELR"=>$v1["ELR"],"ELT"=>$v1["ELT"],"ELB"=>$v1["ELB"]);
                
            }
           
            if($v["band_edge"])
            {
                ksort($vals);
    // Если задняя часть кромкуется, добавляем здесь кромку, станок и указатели в детали
                $vals=put_el_into_vals($part_temp["attributes"]["ID"],$parts_id,$vals,$vals[$i_part]["attributes"]["ID"],$place,$v["band_edge"],$link);
                
            }
    // Добавляем порезку задних частей
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
            foreach ($index["OPERATION"] as $ics)
            {
                //print_r_($v);
                if (($vals[$ics]["attributes"]["TYPEID"]=="CS")&&($vals[$ics+1]["attributes"]["ID"]==$v["back_double"]))
                {
                    $vals=move_vals($vals,$ics+2,count($parts_id)+2);
                foreach ($parts_id as $pics)
                {
                        $vals[$ics+3+$iiii]=Array
                        (
                            "tag" => "PART",
                            "type" => "complete",
                            "level" => 2,
                            "attributes"=> Array ("ID"=>$pics[0],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
                        );
                        $iiii++;
                }
                }
            }
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
           
    // добавляем деталь сшивки
            $vals=move_vals($vals,$i_part,2);
            $vals[$i_part]=$part_temp;
            $vals[$i_part]["attributes"]["MY_DOUBLE_RES"]=1;
            $vals[$i_part]["attributes"]["part.my_double_type"]=$v["type_double"];
            $vals[$i_part]["attributes"]["ELL"]="";
            $vals[$i_part]["attributes"]["ELLMAT"]="";
            $vals[$i_part]["attributes"]["ELT"]="";
            $vals[$i_part]["attributes"]["ELTMAT"]="";
            $vals[$i_part]["attributes"]["ELB"]="";
            $vals[$i_part]["attributes"]["ELBMAT"]="";
            $vals[$i_part]["attributes"]["ELR"]="";
            $vals[$i_part]["attributes"]["ELRMAT"]="";
            $vals[$i_part]["attributes"]["ID"]=new_id($vals);
            $vals[$i_part+2]["attributes"]["MY_DOUBLE_PARENT"]=$vals[$i_part]["attributes"]["ID"];
            $vals[$i_part]["attributes"]["MY_DOUBLE_PARENT"]=0;
            $vals[$i_part]["attributes"]["MY_DOUBLE"]=1;
            $vals[$i_part]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $vals[$i_part]["attributes"]["NAME"]="СШИВКА [/".$part_temp["attributes"]["ID"]."/]) ".$vals[$i_part]["attributes"]["NAME"];
            foreach ($part_temp["attributes"] as $atr=>$meb)
            {
                $vals[$i_part]["attributes"]["_old_".$atr]=$meb;
            }
            ksort($vals);
    // добавляем станок порезки, материал сшивки, операцию порезки детали
            $sql="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
            inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
            where type = 'auto' and product = 'sh' and PLACE = ".$place." LIMIT 1";
        
            $sql1=mysqli_query($link,$sql);
            $sql3=mysqli_fetch_array($sql1); 
            //print_r_($sql3);exit;
            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k3=>$v3)
            {
                if (!is_int($k3)&&($k3<>"TOOL_CUTTING_ID")&&($k<>"TOOL_CUTTING_EQUIPMENT_ID"))
                {
                    $tool["attributes"][$k3]=$v3;
                }
            }
            $tool["attributes"]["ID"]=new_id($vals);
            $tool["attributes"]["DESCRIPTION"]="Порезка основной части сшивки.";
            $tool["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
    //станок есть, следующий материал сшивки
            ksort($vals);
            $vals=vals_out($vals);
            $index=make_index($vals);
            $first=$vals[get_sheet_num($vals,$part_temp["attributes"]["ID"])];
            $second=$vals[get_sheet_num($vals,$parts_id[0][0])];
            //p_($first);
            //p_($second);
            //test("id",$part_temp["attributes"]["ID"]);
            //p_($vals);
            //exit;            
            $sheet=Array
                (
                    "tag" => "GOOD",
                    "type" => "open",
                    "level" => 2
                );
            
            $sheet["attributes"]["W"]=$vals[$i_part+1]["attributes"]["W"];
            $sheet["attributes"]["L"]=$vals[$i_part+1]["attributes"]["L"];
            $sheet["attributes"]["T"]=$first["attributes"]["T"]+$second["attributes"]["T"];
            $sheet["attributes"]["T1"]=$first["attributes"]["T"];
            $sheet["attributes"]["T2"]=$second["attributes"]["T"];
            $sheet["attributes"]["UNIT"]=$first["attributes"]["UNIT"];
            $sheet["attributes"]["TYPENAME"]="Листовой";
            $sheet["attributes"]["COUNT"]=$vals[$i_part]["attributes"]["COUNT"];
            $sheet["attributes"]["NAME"]="СШИВКА № [/".$part_temp["attributes"]["ID"]."/] (".$first["attributes"]["CODE"].") ".$first["attributes"]["NAME"]." / (".$second["attributes"]["CODE"].") ".$second["attributes"]["NAME"];
            $sheet["attributes"]["ID"]=new_id($vals)+1;
            $sheet["attributes"]["DESCRIPTION"]="ОСНОВА сшивки ".$first["attributes"]["NAME"]." / ".$second["attributes"]["NAME"];
            $sheet["attributes"]["MY_DOUBLE"]=1;
            $sheet["attributes"]["TYPEID"]="sheet";
            $sheet["attributes"]["MY_FIRST"]=$first["attributes"]["CODE"];
            $sheet["attributes"]["MY_SECOND"]=$second["attributes"]["CODE"];
            $sheet["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
    //добавить листы для основной части сшивки
            $sheet_part=
                Array
                (
                    "tag" => "PART",
                    "type" => "complete",
                    "level" => 2,
                    "attributes"=> 
                    Array (
                        "L"=>$vals[$i_part+1]["attributes"]["L"],
                        "W"=>$vals[$i_part+1]["attributes"]["W"],
                        "COUNT"=>$vals[$i_part]["attributes"]["COUNT"],
                        "USEDCOUNT"=>$vals[$i_part]["attributes"]["USEDCOUNT"],
                        "ID"=>new_id($vals)+5,
                        "MY_DOUBLE"=>"1",
                        "MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"],
                    )
                );
                
                $sheet2=Array
                (
                    "tag" => "GOOD",
                    "type" => "close",
                    "level" => 2
                );
    //Операция порезки сшивки
            $operation=Array
            (
                "tag" => "OPERATION",
                "type" => "open",
                "level" => 2
            );
            $operation["attributes"]["ID"]=new_id($vals)+2;
            $operation["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $operation["attributes"]["TYPEID"] = "CS";
            $operation["attributes"]["TYPENAME"] = "Листовой раскрой";
            $operation["attributes"]["CTRIML"] = 8;
            $operation["attributes"]["CTRIMW"] = 8;
            $operation["attributes"]["CFILLREP"] = 0.9;
            $operation["attributes"]["CSTEXTURE"] = "true";
            $operation["attributes"]["PRINTABLE"] = "true";
            $operation["attributes"]["STARTNEWPAGE"] = "true";
            $operation["attributes"]["W"] = $sheet["attributes"]["W"];
            $operation["attributes"]["T"] = $sheet["attributes"]["T"];
            $operation["attributes"]["L"] = $sheet["attributes"]["L"];
            $operation["attributes"]["CSIZEMODE"] =1; 
            $operation["attributes"]["TOOL1"] =  $tool["attributes"]["ID"];
    // материал и детали в операцию порезки       
            $material=Array
            (
                "tag" => "MATERIAL",
                "type" => "complete",
                "level" => 2
            );
            $material["attributes"]["ID"]=$sheet["attributes"]["ID"];
            $material["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $operation2=Array
            (
                "tag" => "OPERATION",
                "type" => "close",
                "level" => 2
            );
            $operation2["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
            $parts=Array
            (
                "tag" => "PART",
                "type" => "complete",
                "level" => 2,
                "attributes"=> Array ("ID"=> $vals[$i_part]["attributes"]["ID"],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
            );
            $parts2=Array
            (
                "tag" => "PART",
                "type" => "complete",
                "level" => 2,
                "attributes"=> Array ("ID"=> $sheet_part["attributes"]["ID"],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
            );
            $vals=vals_into_end($vals,$tool);
            $vals=vals_into_end($vals,$sheet);
            $vals=vals_into_end($vals,$sheet_part);
            $vals=vals_into_end($vals,$sheet2);
            $vals=vals_into_end($vals,$operation);
            $vals=vals_into_end($vals,$material);
            $vals=vals_into_end($vals,$parts);
            $vals=vals_into_end($vals,$parts2);
            $vals=vals_into_end($vals,$operation2);
    //Кромкование самой сшивки
            $side=array ("l","r","b","t");
            foreach ($side as $s)
            { 
                if (($v["band_id_".$s])&&($v["band_id_".$s]<>"none")&&(($part_temp["attributes"]["ELL"])OR($part_temp["attributes"]["ELT"])OR($part_temp["attributes"]["ELR"])OR($part_temp["attributes"]["ELB"])))
                {
                    
                    ksort($vals);
                    $vals=vals_out($vals);
                    $index=make_index($vals);
                    $res=check_band($vals,$v["band_id_".$s]);
                    //test("s",$s);
                    //print_r_($res);
                    if(!$res["id"])
                    {
//res-id,name,index
                        $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
                        inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
                        where PLACE = ".$place." LIMIT 1";
                        $sql1=mysqli_query($link,$sql);
                        $sql3=mysqli_fetch_array($sql1); 
                        //print_r_($sql3);exit;
                        $tool=Array
                            (
                                "tag" => "GOOD",
                                "type" => "complete",
                                "level" => 2
                            );
                        foreach ($sql3 as $k3=>$v3)
                        {
                            if (!is_int($k3)&&($k3<>"TOOL_EDGELINE_ID")&&($k3<>"TOOL_EDGELINE_EQUIPMENT_ID"))
                            {
                                $tool["attributes"][$k3]=$v3;
                            }
                        }
                        $tool["attributes"]["ID"]=new_id($vals);
                        $tool["attributes"]["MY_DOUBLE"]=1;
                        $tool["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $tool["attributes"]["DESCRIPTION"]="Кромкование cшивки.";
                    
                        $sql="select * from BAND where CODE=".$v["band_id_".$s];
                     //echo $sql;
                    // print_r_($v);
                        $sql1=mysqli_query($link,$sql);
                        $sql3=mysqli_fetch_array($sql1); 
                        $band1=Array
                            (
                                "tag" => "GOOD",
                                "type" => "complete",
                                "level" => 2
                            );
                        foreach ($sql3 as $k3=>$v3)
                        {
                            if (!is_int($k3)&&($k3<>"BAND_ID"))
                            {
                                $band1["attributes"][$k3]=$v3;
                            }
                        }
                        $bt=$band1["attributes"]["NAME"];
                        $band1["attributes"]["NAME"]="СШИВКА [/".$part_temp["attributes"]["ID"]."/] ".$band1["attributes"]["KR_NAME"];
                        $band1["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $band1["attributes"]["MY_DOUBLE"]=1;
                        unset($band1["attributes"]["KR_NAME"]);
                        $band1["attributes"]["ID"]=new_id($vals)+1;
                        $band1["attributes"]["DESCRIPTION"]="Кромка для кромкования сшивки.";
                        //print_r_($band1);exit;
                        $operation=Array
                            (
                                "tag" => "OPERATION",
                                "type" => "open",
                                "level" => 2
                            );
                        $operation["attributes"]["ID"]=new_id($vals)+2;
                        $operation["attributes"]["MY_DOUBLE"]=1;
                        $operation["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
                        $operation["attributes"]["PRINTABLE"]="true";
                        $operation["attributes"]["STARTNEWPAGE"]="true";
                        $operation["attributes"]["W"]=$band1["attributes"]["W"];
                        $operation["attributes"]["T"]=$band1["attributes"]["T"];
                        $operation["attributes"]["L"]=10000;
                        $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
                        $operation["attributes"]["ELCALCMAT"]="true";
                        $operation["attributes"]["ELROUNDMAT"]="true";
                        $operation["attributes"]["ELROUNDLENGTH"]="0";
                        $operation["attributes"]["ELLENGTH"]="0";
                        $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
                        $operation["attributes"]["ELCOLOR"]="rgb(255,0,0)";
                        $operation["attributes"]["ELLINEMARK"]="1";
                        $operation["attributes"]["ELSYMBOL"]="Z";
                        $operation["attributes"]["TYPEID"]="EL";
                        $operation["attributes"]["DESCRIPTION"]="Кромкооблицовка прямолинейная ".$bt." для кромкования сшивки.";
                        $material=Array
                            (
                                "tag" => "MATERIAL",
                                "type" => "complete",
                                "level" => 2
                            );
                        $material["attributes"]["ID"]=new_id($vals)+1;
                        $material["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                        $operation2=Array
                            (
                                "tag" => "OPERATION",
                                "type" => "close",
                                "level" => 2
                            );
                        $operation2["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                    }
                    $parts=Array
                    (
                        "tag" => "PART",
                        "type" => "complete",
                        "level" => 2,
                        "attributes"=> Array ("ID"=>$vals[$i_part]["attributes"]["ID"],"MY_DOUBLE_ID"=>$part_temp["attributes"]["ID"])
                    );
                    if (!$res["id"])
                    {
                        $vals=vals_into_end($vals,$tool);
                        $vals=vals_into_end($vals,$band1);
                        $vals=vals_into_end($vals,$operation);
                        $vals=vals_into_end($vals,$material);
                        $vals=vals_into_end($vals,$parts);
                        $vals=vals_into_end($vals,$operation2);
                        ksort($vals);
                        $vals=vals_out($vals);
                        $index=make_index($vals);
                    }
                    else
                    {
                        $operation["attributes"]["ID"]=$res["id"];
                        $band1["attributes"]["NAME"]=$res["name"];
                        if ($res["index"]>0) $rrr=$res["index"]+2;
                        unset ($weq);
                        while ($vals[$rrr]["tag"]=="PART")
                        {
                           
                            if($vals[$rrr]["attributes"]["ID"]== $vals[$i_part]["attributes"]["ID"])
                            {
                                $weq=1;
                                //print_r_($res);exit;
                            }
                            $rrr++; 
                        }
                        if (!$weq)
                        {
                            $vals=move_vals($vals,$res["index"]+2,1);
                            $vals[$res["index"]+2]=$parts;
                            
                           // 
                        }
                    }
                    unset($ii_p,$weq);
                    foreach ($index["PART"] as $int)
                    {
                        if (($vals[$int]["attributes"]["ID"]== $vals[$i_part]["attributes"]["ID"])AND($vals[$int]["attributes"]["W"]<>0)) $ii_p=$int;
                    }
                  
                    if ($v["band_id_".$s]>0)
                    {
                       
                        $vals[$ii_p]["attributes"]["EL".mb_strtoupper($s)]="@operation#".$operation["attributes"]["ID"];
                        $vals[$ii_p]["attributes"]["EL".mb_strtoupper($s)."MAT"]=$band1["attributes"]["NAME"];
                        unset($vals[$ii_p]["attributes"]["part.my_kl_el".$s]);
        
                    }
                   // print_r_($vals[$ii_p]);exit;
                    unset($ii_n,$operation,$band1);
                    ksort($vals);
                }
    // Вносим изменения в XNC и GR операции
                ksort($vals);
                $vals=vals_out($vals);
                $index=make_index($vals);

                foreach ($index["OPERATION"] as $iop)
                {
                    //print_r_($v);
                    if ((($vals[$iop]["attributes"]["TYPEID"]=="XNC")OR($vals[$iop]["attributes"]["TYPEID"]=="GR"))AND
                    ($vals[$iop]["type"]=="open")AND($vals[$iop+1]["attributes"]["ID"]==$part_temp["attributes"]["ID"]))
                    {
                        
                        if(($vals[$iop]["attributes"]["TYPEID"]=="GR"))
                        {
                            $vals[$iop+1]["attributes"]["ID"]=$vals[$i_part]["attributes"]["ID"];
                            $vals[$iop]["attributes"]["NAME"]="СШИВКА [/".$part_temp["attributes"]["ID"]."/] ";
                            $vals[$iop]["attributes"]["TYPENAME"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop]["attributes"]["DESCRIPTION"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop+1]["attributes"]["MY_DOUBLE"]=1;
                            $vals[$iop+1]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop+1]["attributes"]["_old_ID"]=$part_temp["attributes"]["ID"];
                        }
                        elseif(($vals[$iop]["attributes"]["TYPEID"]=="XNC"))
                        {
                            $vals[$iop+1]["attributes"]["ID"]=$vals[$i_part]["attributes"]["ID"];
                            $vals[$iop]["attributes"]["NAME"]="СШИВКА [/".$part_temp["attributes"]["ID"]."/] ";
                            $vals[$iop]["attributes"]["TYPENAME"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop]["attributes"]["DESCRIPTION"]=$vals[$iop]["attributes"]["NAME"];
                            $vals[$iop+1]["attributes"]["MY_DOUBLE"]=1;
                            $vals[$iop+1]["attributes"]["MY_DOUBLE_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop+1]["attributes"]["_old_ID"]=$part_temp["attributes"]["ID"];
                            $vals[$iop]["attributes"]["_old_T"]=$sheet["attributes"]["T1"];
                            $vals[$iop]["attributes"]["PROGRAM"]=str_replace("dz=\"".$sheet["attributes"]["T1"]."\"","dz=\"".$sheet["attributes"]["T"]."\"",$vals[$iop]["attributes"]["PROGRAM"]);
                        }
                    }
                }
                ksort($vals);
            }
        }
        unset($count_part,$part_back,$count_part_back,$k1,$v1,$ii,$i_part,$i_part1,$parts_id,$part_temp,$tool,$operation);
        
    }
    //print_r_($vals);
    //exit;
    ksort($vals);
    return($vals);
}
function put_el_into_vals ($my_double_id,$part_id,$vals,$id_part,$place,$band,$link)
{
    $sql="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
    inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
    where PLACE = ".$place." LIMIT 1";
    $sql1=mysqli_query($link,$sql);
    $sql3=mysqli_fetch_array($sql1); 
    //print_r_($sql3);exit;
    $tool=Array
        (
            "tag" => "GOOD",
            "type" => "complete",
            "level" => 2
        );
    foreach ($sql3 as $k=>$v)
    {
        if (!is_int($k)&&($k<>"TOOL_EDGELINE_ID")&&($k<>"TOOL_EDGELINE_EQUIPMENT_ID"))
        {
            $tool["attributes"][$k]=$v;
        }
    }
    $tool["attributes"]["ID"]=new_id($vals);
    $tool["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    $tool["attributes"]["DESCRIPTION"]="Кромкование нижней части сшивки.";
    $sql="select * from BAND where CODE=".$band;
    $sql1=mysqli_query($link,$sql);
    $sql3=mysqli_fetch_array($sql1); 
    $band1=Array
        (
            "tag" => "GOOD",
            "type" => "complete",
            "level" => 2
        );
    foreach ($sql3 as $k=>$v)
    {
        if (!is_int($k)&&($k<>"BAND_ID"))
        {
            $band1["attributes"][$k]=$v;
        }
    }
    $bt=$band1["attributes"]["NAME"];
    $band1["attributes"]["NAME"]="СШИВКА (".$my_double_id.") ".$band1["attributes"]["KR_NAME"];
    $band1["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    unset($band1["attributes"]["KR_NAME"]);
    $band1["attributes"]["ID"]=new_id($vals)+1;
    $band1["attributes"]["DESCRIPTION"]="Кромка для кромкования нижней части сшивки.";
    $operation=Array
        (
            "tag" => "OPERATION",
            "type" => "open",
            "level" => 2
        );
    $operation["attributes"]["ID"]=new_id($vals)+2;
    $operation["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    $operation["attributes"]["TYPENAME"]="Кромкооблицовка прямолинейная";
    $operation["attributes"]["PRINTABLE"]="true";
    $operation["attributes"]["STARTNEWPAGE"]="true";
    $operation["attributes"]["W"]=$band1["attributes"]["W"];
    $operation["attributes"]["T"]=$band1["attributes"]["T"];
    $operation["attributes"]["L"]=10000;
    $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
    $operation["attributes"]["ELCALCMAT"]="true";
    $operation["attributes"]["ELROUNDMAT"]="true";
    $operation["attributes"]["ELROUNDLENGTH"]="0";
    $operation["attributes"]["ELLENGTH"]="0";
    $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
    $operation["attributes"]["ELCOLOR"]="rgb(255,0,0)";
    $operation["attributes"]["ELLINEMARK"]="1";
    $operation["attributes"]["ELSYMBOL"]="Z";
    $operation["attributes"]["TYPEID"]="EL";
    $operation["attributes"]["DESCRIPTION"]="Кромкооблицовка прямолинейная ".$bt." для кромкования нижней части сшивки (".$my_double_id.").";
    $material=Array
        (
            "tag" => "MATERIAL",
            "type" => "complete",
            "level" => 2
        );
    $material["attributes"]["ID"]=new_id($vals)+1;
    $material["attributes"]["MY_DOUBLE_ID"]=$my_double_id;
    $operation2=Array
        (
            "tag" => "OPERATION",
            "type" => "close",
            "level" => 2
        );
    $operation2["attributes"]["MY_DOUBLE_ID"]=$my_double_id;

    foreach ($part_id as $idp)
    {
       // print_r_($idp);
        $parts[]=Array
        (
            "tag" => "PART",
            "type" => "complete",
            "level" => 2,
            "attributes"=> Array ("ID"=>$idp[0],"MY_DOUBLE_ID"=>$my_double_id)
        );
       
        
        
    }
   // print_r_($parts);exit;
  
    if(count($parts)>0)
    {
       // test("1",count($vals));
        //print_r_($tool);exit;
        $vals=vals_into_end($vals,$tool);
       // test("1",count($vals));
        $vals=vals_into_end($vals,$band1);
        $vals=vals_into_end($vals,$operation);
        $vals=vals_into_end($vals,$material);
        foreach($parts as $q) $vals=vals_into_end($vals,$q);
        $vals=vals_into_end($vals,$operation2);
       // test("2",count($vals));
    }
    ksort($vals);
    $index=make_index($vals);
    reset($part_id);
    foreach ($part_id as $idp)
    {
       // print_r_($idp);
        foreach ($index["PART"] as $in)
        {
            if (($vals[$in]["attributes"]["ID"]==$idp[0])&&($vals[$in]["attributes"]["L"]>0)) $ii_p=$in;
            //break;
        }
        //test ("11",$ii_p);
        $e=array("L","R","T","B");
        foreach ($e as $l)
        {
            if ($idp["EL".$l]<>"")
            {
                $vals[$ii_p]["attributes"]["EL".$l]="@operation#".$operation["attributes"]["ID"];
                $vals[$ii_p]["attributes"]["EL".$l."MAT"]=$band1["attributes"]["NAME"];

            }
        }
        unset($ii_n);
    }


    unset($tool,$band1,$operation,$operation2,$parts);
    //$new=array($operation["attributes"]["ID"],$vals);
//    print_r_($vals);

    return $vals;
}













function el_type_lines($vals,$type,$link)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    //test ("1",count($vals));
    foreach ($index["OPERATION"] as $i)
    {
        if (($vals[$i]["attributes"]["TYPEID"]=="EL")&&($vals[$i]["type"]=="open"))
        {
            foreach ($index["GOOD"] as $i1)
            {
                if (($vals[$i1]["attributes"]["TYPEID"]=="band")&&($vals[$i1]["attributes"]["ID"]==$vals[$i+1]["attributes"]["ID"]))
                {
                    $code=$vals[$i1]["attributes"]["CODE"];
                }
            }
            if ($code>0)
            {
                $sql="select * from BAND where CODE=".$code.";";
                //echo $sql;
                $sql1=mysqli_query($link,$sql);
                $sql2=mysqli_fetch_array($sql1); 
              // print_r_($sql2);
                if ($sql2)
                {
                    if ($sql2["MY_LASER"]==1) $vals[$i]["attributes"]["MY_EL_TYPE"]="laz";
                    elseif (($sql2["MY_LASER"]<>1)&&($vals[$i]["attributes"]["MY_CHECK_KL"]=="1")) 
                    {
                        $vals[$i]["attributes"]["MY_EL_TYPE"]="eva";
                    }
                    else $vals[$i]["attributes"]["MY_EL_TYPE"]=$type;
                    $iy=$i+2;
                    while ($vals[$iy]["tag"]=="PART")
                    {
                        foreach ($index["PART"] as $im)
                        {
                            if (($vals[$im]["attributes"]["ID"]==$vals[$iy]["attributes"]["ID"])&&($vals[$im]["attributes"]["L"]>0))
                            {
                                $vals[$im]["attributes"]["part.my_el_type"]=$vals[$i]["attributes"]["MY_EL_TYPE"];
                            }
                        }
                        $iy++;
                    }
                    if ($sql2["T"]<=1) $vals[$i]["attributes"]["ELLINEMARK"]="1";
                    else $vals[$i]["attributes"]["ELLINEMARK"]="3";
                }
                else echo "Кромка с кодом ".$code." не найдена в БД";
            }
            unset($code);
        }
    }
  // test("2",count($vals));
    return $vals;
   // print_r_($vals);
}

function vals_index_to_project($vals)
{
    include ("description.php");
    /*$text="<?xml version=\"1.0\" encoding=\"utf-8\"?>";*/
    foreach ($vals as $v)
    {
        if ($v["type"]=="open")
        {
            $text.="<".mb_strtolower($v["tag"])." ";
            foreach ($v["attributes"] as $ka=>$va)
            {
                if ($ka=="PROGRAM") $va=htmlspecialchars($va);
                if (!$gib_per[$ka]) $gib_per[$ka]=$ka;
                if ($ka==array_key_last($v["attributes"])) $text.=$gib_per[$ka]."=\"".$va."\"";
                else $text.=$gib_per[$ka]."=\"".$va."\" ";
            }
            $text.=">";
        }
        elseif ($v["type"]=="complete")
        {
            $text.="<".mb_strtolower($v["tag"])." ";
            foreach ($v["attributes"] as $ka=>$va)
            {
                if (!$gib_per[$ka]) $gib_per[$ka]=$ka;
                if ($ka=="PROGRAM") $va=htmlspecialchars($va);
                if ($ka==array_key_last($v["attributes"])) $text.=$gib_per[$ka]."=\"".$va."\"";
                else $text.=$gib_per[$ka]."=\"".$va."\" ";
            }
            $text.="/>";
        }
        elseif ($v["type"]=="close")
        {
            $text.="</".mb_strtolower($v["tag"]).">";
        }
    }
    return $text;
}
function new_id ($vals)
{
    foreach ($vals as $v)
    {
        if ($v["attributes"]["ID"]) $id[]=$v["attributes"]["ID"];
    }
    return max($id)+1;
}
function check_band_kl($vals,$code)
{
    foreach ($index["GOOD"] as $i)
    {
        
        if(($vals[$i]["attributes"]["TYPEID"]=="band")&&($vals[$i]["attributes"]["CODE"]==$code))
        {
            foreach ($index["OPERATION"] as $i1)
            {
                if(($vals[$i1]["attributes"]["TYPEID"]=="EL")&&($vals[$i1+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                {
                    if($vals[$i]["attributes"]["MY_CHECK_KL"]==1)
                    {
                        $res=array("id"=>$vals[$i1]["attributes"]["ID"],"index"=>$i1,"name"=>$vals[$i]["attributes"]["NAME"],"code"=>$vals[$i]["attributes"]["CODE"]);
                        
                        return $res;
                    }
                }
            }
        }
    }
}
function check_band($vals,$code)
{
    //echo $code;
   // print_r_($vals);
   $vals=vals_out($vals);
   $index=make_index($vals);
   ksort($vals);
    foreach ($index["GOOD"] as $i)
    {
        
        if(($vals[$i]["attributes"]["TYPEID"]=="band")&&($vals[$i]["attributes"]["CODE"]==$code))
        {
           // echo $code;
            foreach ($index["OPERATION"] as $i1)
            {
                if(($vals[$i1]["attributes"]["TYPEID"]=="EL")&&($vals[$i1+1]["attributes"]["ID"]==$vals[$i]["attributes"]["ID"]))
                {
                    //print_r_($vals[$i]);exit;
                    if(!$vals[$i1]["attributes"]["MY_CHECK_KL"])
                    {
                        $res=array("id"=>$vals[$i1]["attributes"]["ID"],"index"=>$i1,"name"=>$vals[$i]["attributes"]["NAME"]);
                        
                        return $res;
                    }
                }
            }
        }
    }
}
function change_kl_edge ($vals,$arr_out,$link)
{
    $vals=vals_out($vals);
    $index=make_index($vals);
    //p_($vals);
    unset($vals1);
    foreach ($vals as $l=>$l1)
    {
        
        if ($l1["attributes"]["MY_CHECK_KL"] == '' && !isset($l1["attributes"]["MY_CHECK_KL"])) $vals1[]=$l1;
        //else echo $l."<hr>";

    }
    ksort($vals1);
    $vals=vals_out($vals1);
    $index=make_index($vals);
  
    $side=array("l","r","t","b");
    foreach ($index["PART"] as $i)
    {
        foreach ($side as $s) 
        {
            //echo $i;
            if (isset($vals[$i]["attributes"]["part.my_kl_el".$s]) || $vals[$i]["attributes"]["part.my_kl_el".$s]<>"")
            {
               // echo "!";
                unset ($vals[$i]["attributes"]["part.my_kl_el".$s]);
                unset ($vals[$i]["attributes"]["PART.MY_KL_EL".mb_strtoupper($s)]);
                unset ($vals[$i]["attributes"]["PART.MY_KL_EL".mb_strtoupper($s)."_CODE"]);
                unset ($vals[$i]["attributes"]["part.my_kl_el".$s."_code"]);
                unset ($vals[$i]["attributes"]["EL".mb_strtoupper($s)]);
                unset ($vals[$i]["attributes"]["EL".mb_strtoupper($s)."MAT"]);
                

            }
            if (isset($vals[$i]["attributes"]["PART.MY_KL_EL".mb_strtoupper($s)]) || $vals[$i]["attributes"]["PART.MY_KL_EL".mb_strtoupper($s)]<>"")
            {
               // echo "!";
                unset ($vals[$i]["attributes"]["part.my_kl_el".$s]);
                unset ($vals[$i]["attributes"]["PART.MY_KL_EL".mb_strtoupper($s)]);
                unset ($vals[$i]["attributes"]["PART.MY_KL_EL".mb_strtoupper($s)."_CODE"]);
                unset ($vals[$i]["attributes"]["part.my_kl_el".$s."_code"]);
                unset ($vals[$i]["attributes"]["EL".mb_strtoupper($s)]);
                unset ($vals[$i]["attributes"]["EL".mb_strtoupper($s)."MAT"]);
            }
        }
    }
   // p_($vals);exit;
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($arr_out as $v)
    {
        foreach ($side as $s)
        {
            if (!in_array($v["xnc_band"],$xnc_band)) $xnc_band[]=$v["xnc_band_".$s];
        }
    }
    $xnc_band=array_unique($xnc_band);
    $xnc_band=array_diff($xnc_band, array(0));
    $xnc_band=array_diff($xnc_band, array(''));
    //print_r_($xnc_band);exit;
    reset($side);
    foreach ($xnc_band as $i=>$band)
    {
        foreach ($arr_out as $a=>$b)
        {
           foreach($side as $s)
           {
                if ($band==$b["xnc_band_".$s])
                {
                    foreach ($index["PART"] as $n)
                    {
                       // echo $b["part_id"]."//<br>";
                        if (($vals[$n]["attributes"]["ID"]==$b["part_id"])&&($vals[$n]["attributes"]["L"]>0)&&
                        ($vals[$n]["attributes"]["MY_DOUBLE_PARENT"]>0))
                        {
                            $rrrrrrrr= $b["part_id"];
                            $arr_out[$vals[$n]["attributes"]["MY_DOUBLE_PARENT"]]=$arr_out[$b["part_id"]];
                            $b["part_id"]=$vals[$n]["attributes"]["MY_DOUBLE_PARENT"];
                            //unset($arr_out[$rrrrrrrr]);
                           // echo "aa".$b["part_id"]."aa<br>";
                        }
                    }
                    $part_b[$band][$s][]=$b["part_id"];
                }
            }
        }

        $res_band=check_band_kl($vals,$band);
        // print_r_($part_b);
        // print_r_($arr_out);exit;
        // p_($res_band);
        // exit;
        if ($res_band["id"]>0)
        {
            $operation["attributes"]["ID"]=$res_band["id"];
            $band1["attributes"]["NAME"]=$res_band["name"];
            $band_in=$res_band["index"];
            $band_code=$res_band["code"];
        }
        else
        {
            $sql="select * from TOOL_EDGELINE where TOOL_EDGELINE_ID=4";
            $sql1=mysqli_query($link,$sql);
            $sql3=mysqli_fetch_array($sql1); 
            $tool=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k=>$v)
            {
                if (!is_int($k)&&($k<>"TOOL_EDGELINE_ID"))
                {
                    $tool["attributes"][$k]=$v;
                }
            }
            $tool["attributes"]["ID"]=new_id($vals);
            $tool["attributes"]["MY_CHECK_KL"]="1";
            $sql="select * from BAND where CODE=".$band;
            $sql1=mysqli_query($link,$sql);
            $sql3=mysqli_fetch_array($sql1); 
            $band1=Array
                (
                    "tag" => "GOOD",
                    "type" => "complete",
                    "level" => 2
                );
            foreach ($sql3 as $k=>$v)
            {
                if (!is_int($k)&&($k<>"BAND_ID"))
                {
                    $band1["attributes"][$k]=$v;
                }
            }
            $bt=$band1["attributes"]["NAME"];
            $band1["attributes"]["NAME"]="КЛ ".$band1["attributes"]["KR_NAME"];
            unset($band1["attributes"]["KR_NAME"]);
            $band_code=$band1["attributes"]["CODE"];
            $band1["attributes"]["ID"]=new_id($vals)+1;
            $band1["attributes"]["MY_CHECK_KL"]="1";
            $operation=Array
                (
                    "tag" => "OPERATION",
                    "type" => "open",
                    "level" => 2
                );
            $operation["attributes"]["ID"]=new_id($vals)+2;
            $operation["attributes"]["MY_CHECK_KL"]="1";
            $operation["attributes"]["TYPENAME"]="Кромкооблицовка криволинейная";
            $operation["attributes"]["PRINTABLE"]="true";
            $operation["attributes"]["STARTNEWPAGE"]="true";
            $operation["attributes"]["W"]=$band1["attributes"]["W"];
            $operation["attributes"]["T"]=$band1["attributes"]["T"];
            $operation["attributes"]["L"]=10000;
            $operation["attributes"]["ELWASTEPRC"]=$tool["attributes"]["ELWASTEPRC"];
            $operation["attributes"]["ELCALCMAT"]="true";
            $operation["attributes"]["ELROUNDMAT"]="true";
            $operation["attributes"]["ELROUNDLENGTH"]="0";
            $operation["attributes"]["ELLENGTH"]="0";
            $operation["attributes"]["TOOL1"]=$tool["attributes"]["ID"];
            $operation["attributes"]["ELCOLOR"]="rgb(255,0,0)";
            $operation["attributes"]["ELLINEMARK"]="1";
            $operation["attributes"]["ELSYMBOL"]="Z";
            $operation["attributes"]["TYPEID"]="EL";
            $operation["attributes"]["DESCRIPTION"]="Кромкооблицовка криволинейная для кромки ".$bt;
            $material=Array
                (
                    "tag" => "MATERIAL",
                    "type" => "complete",
                    "level" => 2
                );
            $material["attributes"]["ID"]=new_id($vals)+1;
            $material["attributes"]["MY_CHECK_KL"]=1;
            $operation2=Array
                (
                    "tag" => "OPERATION",
                    "type" => "close",
                    "level" => 2
                );
            $operation2["attributes"]["MY_CHECK_KL"]=1;
        }
        //print_r_($vals);exit;
        reset ($side);
        foreach($side as $s)
        {
            foreach ($part_b[$band][$s] as $b)
            {
                //print_r_($b);exit;
                //print_r($side);exit;
                foreach ($index["PART"] as $i4)
                {
                    if (($vals[$i4]["attributes"]["ID"]==$b)&&($vals[$i4]["attributes"]["L"]>0)) $part_val=$i4;
                }
                    
                    if (($arr_out[$b]["xnc_band_".$s]>0)&&(!$vals[$part_val]["attributes"]["MY_CUT_ANGLE_".mb_strtoupper($s)]))
                    {
                        
                        $vals[$part_val]["attributes"]["EL".mb_strtoupper($s)]="@operation#". $operation["attributes"]["ID"];
                        $vals[$part_val]["attributes"]["EL".mb_strtoupper($s)."MAT"]=$band1["attributes"]["NAME"];
                        $vals[$part_val]["attributes"]["part.my_kl_el".$s]=1;
                        $vals[$part_val]["attributes"]["part.my_kl_el".$s."_code"]=$band_code;
                        $pt++;
                        $parts=Array
                        (
                            "tag" => "PART",
                            "type" => "complete",
                            "level" => 2,
                            "attributes"=> Array ("ID"=>$b,"MY_CHECK_KL"=>1,"MY_CHECK_KL_".mb_strtoupper($s)=>1)
                        );
                       
                        if(($pt==1)&&(!$band_in))
                        {
                            $vals=vals_into_end($vals,$tool);
                            $vals=vals_into_end($vals,$band1);
                            $vals=vals_into_end($vals,$operation);
                            $renm=count($vals)-1;
                            $vals=vals_into_end($vals,$material);
                            $vals=vals_into_end($vals,$parts);
                            $vals=vals_into_end($vals,$operation2);
                            //test ("band",$q1);exit;
                        }
                        else
                        {
                           
                            if($band_in>0) $t1=$band_in+2;
                            else $t1=$renm+1;
                            //if ($t1>0) print_r_($vals[$t1]);exit;
                            while ($vals[$t1]["tag"]=="PART")
                            {
                                if ($vals[$t1]["attributes"]["ID"]==$b) $q1=$t1;
                                $t1++;
                            }
                            //test("t1",$q1);
                            if ($q1>0)
                            {
                                foreach ($parts["attributes"] as $keyt=>$valt)
                                {
                                      //test("q",$q1);
                                    if ($vals[$q1]["attributes"][$keyt]<>$parts["attributes"][$keyt]) $vals[$q1]["attributes"][$keyt]=$valt;
                                }
                               // exit;
                            }

                            else
                            {
                                //test("1",count($vals));
                                $vals=move_vals($vals,$t1,1);
                               
                                
                               // test("b",$b);
                                $vals[$t1]=$parts;
                                ksort($vals);
                                $vals=vals_out($vals);
                                $index=make_index($vals);
                                 //print_r_($vals);exit;
                            }
                            //unset ($t,$q1);
                            
                        }
                        
                    }
               
               
            }
        }
       
        unset($tool,$band1,$operation,$operation2,$parts,$pt,$band_in,$renm);
    }
   // print_r_($part_b);exit;
    //    print_r_($vals);
   //exit;
    return $vals;
}
function vals_check_out($vals)
{
    foreach ($vals as $i=>$v)
    {
        foreach ($v["attributes"] as $k=>$v1)
        {
            if (substr_count($k,"MY_CHECK_"))
            {
                unset ($vals[$i]["attributes"][$k]);
            }
        }
    }
    return ($vals);
}
function vals_into_end($vals,$array)
{
    //echo count ($vals)."!";
    $k=array_key_last($vals);
   // print_r_($vals[$k]);

    $vals[$k+1]=$vals[$k];
   
    $vals[$k]=$array;
    //echo count ($vals)."!";
    //print_r_($vals[$k]);
    return $vals;
}
function check_engle_edge ($vals,$array)
{
    $side=array("l","r","t","b");
    foreach ($index["PART"] as $v)
    {
        if (($vals[$v]["attributes"]["L"]>0)AND(!$vals[$v]["attributes"]["SHEETID"]))
        {
            
            foreach ($side as $s)
            {
                unset ($vals[$v]["attributes"]["MY_CUT_ANGLE_".mb_strtoupper($s)]);
                unset ($vals[$v]["attributes"]["part.my_cut_angle_".$s]);

            }
            
        }
    }
    $vals=vals_out($vals);
    $index=make_index($vals);
    foreach ($array as $part_id=>$cut)
    {
        foreach ($index["PART"] as $v)
        {
            if (($vals[$v]["attributes"]["ID"]==$part_id)AND($vals[$v]["attributes"]["L"]>0)AND
            (!$vals[$v]["attributes"]["SHEETID"])&&($vals[$v]["attributes"]["MY_DOUBLE_PARENT"]>0))
            {
                
                $array[$vals[$v]["attributes"]["MY_DOUBLE_PARENT"]]=$array[$part_id];
                $part_id=$vals[$v]["attributes"]["MY_DOUBLE_PARENT"];
                //unset($arr_out[$rrrrrrrr]);
                // echo "aa".$b["part_id"]."aa<br>";
            }
        }
        foreach ($index["PART"] as $v)
        {
            
            if (($vals[$v]["attributes"]["ID"]==$part_id)AND($vals[$v]["attributes"]["L"]>0)AND(!$vals[$v]["attributes"]["SHEETID"]))
            {
                foreach ($cut as $k1=>$v1)
                {
                    if ($v1<>0)
                    {
                        $side=substr($k1,strlen($k1)-1,1);
                        
                        $vals[$v]["attributes"][$k1]=$v1;
                        unset($vals[$v]["attributes"]["EL".$side]);
                        //echo $vals[$v]["attributes"]["EL".$side];exit;
                        unset($vals[$v]["attributes"]["EL".$side."MAT"]);
                        $vals[$v]["attributes"]["part.my_cut_angle_".mb_strtolower($side)]=$v1;
                        
                    }
                    unset ($side);
                }
            }
        }
    }
    return $vals;
}
function print_r_($array)
{
	
	echo '<pre>';
    print_r($array);
    echo  '</pre>';
}
function p_($array)
{
	
	echo '<pre>';
    print_r($array);
    echo  '</pre>';
}
function br()
{
    echo "<br><hr>";
}
function sqlDoInsert ($value,$link,$db)
{
    
    if ($result=mysqli_query($link, $value)) 
    {
        #echo "<hr>Запрос ".$value." прошёл!<hr>";
        $id=mysqli_insert_id ($link);
        
    }
else 
{
        echo "Error: <hr>" . $value . "<br>" . mysqli_error($link);
        mysqli_rollback($link);
        echo"<hr>";
        exit;
}

return ($id);
}
function arrayTOsql($array,$table,$db,$link,$order1c)
{
    $src_sql="SHOW COLUMNS FROM ".$table;
   # test ("sql", $src_sql);
    $src_result=mysqli_query($link, $src_sql);

    $rows = mysqli_num_rows($src_result); 
    for($i = 0;$i < $rows;$i++) { 
        $src_res[$i] = mysqli_fetch_array($src_result)[0]; 
    } 

    if ($src_result)
    {    
       
        $sql1 = " INSERT INTO "."`".$db."`.`".$table."` (";
        
        
        $sql11="";
        $sql21="";
        $i=0;
        $ii=count($array);
        foreach ($array as $key=>$value) {    
            if(in_array($key,$src_res))
            {
                $i++; 
                
                if ($value)
                {
                    if ($i<$ii)
                    {
                        $sql11=$sql11." `".mb_strtoupper($key)."`,";
                        if($value=="NULL")$sql21.=" ".$value.",";
                        else $sql21.=" \"".$value."\",";
                    } 
                    else 
                    {
                        $sql11.=" `".mb_strtoupper($key)."`";
                        if($value=="NULL")  $sql21.=" ".$value;
                        else $sql21.=" \"".$value."\"";
                    }
                }
                next ($array);
            }
        }
        if($order1c>0) {
            if (substr($sql21,strlen($sql21)-1,1)!==",")
            {
                $sql3=", \"".$order1c."\");"; 
                $sql2= ",`ORDER1C`) VALUES ( ";
            }
            else
            {
                $sql3="\"".$order1c."\");"; 
                $sql2= "`ORDER1C`) VALUES ( ";
            }
        }
        else 
        {
            $sql3=");";
            $sql2= ") VALUES ( ";
        }
        
        return ($sql1.$sql11.$sql2.$sql21.$sql3);
    }
}

function updateTOsql($id,$array,$table,$link)
{
    //print_r_($array);
    $sql2="";
    $sql1 = " UPDATE ".$table." SET ";
    foreach($array as $k=>$a)
    {
        if($a<>"NULL") $sql2=$sql2." ".$k." = \"".$a."\" ,"; 
        elseif ($a!=="") $sql2=$sql2." ".$k." = ".$a." ,"; 
        elseif ($a=="") $sql2=$sql2." ".$k." = NULL ,"; 
        
    }
    $sql2=substr($sql2,0,strlen($sql2)-1);
    $sql3=" WHERE ".$table."_ID = ".$id.";";
    $sql=$sql1.$sql2.$sql3;
    //test ("sql",$sql);
    if($sql2<>"")$r=sqlDoselect($sql,$link);
    #test("возврат строк",mysqli_affected_rows($link));
    if ($r) return ("true");
    else "false";
}

function per ($text,$perem)
{
    if (strpos($text," ".$perem."=\"")>0)
    {
        #$text=substr($text,0,strpos($text,">")+2);
        $text = substr($text, strpos($text, " ".$perem."=\"")+strlen($perem)+3);
        $text = substr($text, 0, strpos($text, "\""));
        return $text;
    }
    else return 0;
}
function text_programm($text)
{
    
    $t=str_replace("&lt;","&amp;lt;",$text);
    $t2=str_replace("&gt;","&amp;gt;",$t);
    $t4=str_replace("\"","&quot;",$t2);
    $t5=str_replace(">","&gt;",$t4);
    $t6=str_replace("<","&lt;",$t5);

    
    return $t6;
}
function data ($date)
{
    if ($date>0) {
        $date1 = date_create();
        date_timestamp_set($date1, substr($date, 0, strlen($date) - 3));
        return date_format($date1, 'Y-m-d H:i:s');
    }
    else return "1970-01-01 00:00:00";
}
function sqlDoselect ($value,$link)
{
    if ($result=mysqli_query($link, $value)) 
    {
    $id=mysqli_insert_id ($link);
   
    }
    else 
    {
        echo "Error: <hr>" . $value . "<br>" . mysqli_error($link);
        mysqli_rollback($link);
        echo"<hr>";
        exit;
    }
    return ($result);
}

function k($t,$gib_per)
{
	if ($gib_per[$t])
	{
		return $gib_per[$t];
	}
	else return ($t);
}
function make_index($vals)
{
    foreach ($vals as $k=>$v)
    {
        $temp[]=$v["tag"];
    }
    $in_key=array_unique($temp);
    //print_r_($in_key);
    foreach ($in_key as $v)
    {
        foreach ($vals as $k1=>$v1)
        {
            if($v==$v1["tag"])
            {
                $index[$v][]=$k1;
            }
        }
    }
    return $index;
}
function vals_out($vals)
{
    //test ("кол-во1", count($vals)); 
    foreach ($vals as $k1=>$v1)
    {
        //echo $k1;
        if(!is_int($k1)) 
        {
            unset ($vals [$k1]);
            unset ($v1);
        }
        else
        {
            if (($v1["type"]<>"cdata")&&($v1))
            {
                if($v1["value"]) unset($v1["value"]);
                if (($v1["attributes"]["TYPEID"]=="product")&&($v1["type"]=="complete")) unset ($v1);
                //if (($v1["attributes"]["MY_CHECK_KL"])&&($v1["attributes"]["MY_CHECK_KL"]<>1)) unset($v1["attributes"]["MY_CHECK_KL"]);
                else $vals1[]=$v1;

            }
        }
        
    }
    //test ("кол-во2", count($vals1));  
    return ($vals1);
}

function change_tool_cutting_per($array,$good_tool_cutting_,$table,$db,$link,$type,$place,$tool_cutting_for,$description_giblab)
{
    #SELECT * FROM `".$db."`.`".$table."` WHERE TYPE = \"".$type."\" AND PLACE = \"".$place."\" AND PRODUCT=\"".$tool_cutting_for."\";";
    $value="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
    inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
    where type = '".$type."' and product = '".$tool_cutting_for."' and PLACE = ".$place." LIMIT 1";
    $result = sqlDoSelect ($value,$link,$db);
    $res=mysqli_fetch_array($result);
    if (mysqli_num_rows($result)==1)
    {
        $tool_cutting_changes=0;
        $error="";
        
        foreach ($good_tool_cutting_ as $key=>$value)
        {
            if ($key<>"ID")
            {    
                if ($value=="false")  $value=0;
                elseif ($value=="true")  $value=1;
                if (($res[$key] <> $value)&&(!in_array($key,$array)))
                {
                    $tool_cutting_changes++;
                    if($tool_cutting_changes==1) $error=$error."<hr> Во внесённом распиловочном станке ".$res["NAME"]." код (".$good_tool_cutting_["ID"].") некоторые свойства внесены ошибочно: <hr>";
                    $error=$error. "Атрибут ".$description_giblab[$key]." (".$key.") с ".$value." изменён на ".$res[$key]."<hr> ";
                }
            }
        }
    }
    return $error;
}
function change_tool_cutting_per_new_values($array,$good_tool_cutting_,$table,$db,$link,$type,$place,$tool_cutting_for,$description_giblab)
{
    #$value="SELECT * FROM `".$db."`.`".$table."` WHERE TYPE = \"".$type."\" AND PLACE = \"".$place."\" AND PRODUCT=\"".$tool_cutting_for."\";";
    $value="SELECT * FROM `TOOL_CUTTING` inner join TOOL_CUTTING_EQUIPMENT_CONN on TOOL_CUTTING.TOOL_CUTTING_ID = TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_ID 
    inner join TOOL_CUTTING_EQUIPMENT on TOOL_CUTTING_EQUIPMENT_CONN.TOOL_CUTTING_EQUIPMENT_ID = TOOL_CUTTING_EQUIPMENT.ID 
    where type = '".$type."' and product = '".$tool_cutting_for."' and PLACE = ".$place." LIMIT 1";
    $result = sqlDoSelect ($value,$link,$db);
    $res=mysqli_fetch_array($result);
    if (mysqli_num_rows(($result))==1)
    {
        foreach ($good_tool_cutting_ as $key=>$value)
        {
            if ($value=="false")  $value=0;
            elseif ($value=="true")  $value=1;
            if ($key<>"ID")
            {    
                if (($res[$key] <> $value)&&(!in_array($key,$array)))
                {
                    $good_tool_cutting_[$key]=$res[$key];
                }
            }
            
        }
    }
    return $good_tool_cutting_;
}
function change_tool_edgeline_per($array,$good_tool_edgeline_,$table,$db,$link,$place,$description_giblab)
{
   # $value="SELECT * FROM `".$db."`.`".$table."` WHERE PLACE = \"".$place."\";";
   $value="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
   inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
   where PLACE = ".$place." LIMIT 1";
    
    $result = sqlDoSelect ($value,$link,$db);
   
    $res=mysqli_fetch_array($result);
    if (mysqli_num_rows(($result))==1)
    {
        $tool_edgeline_changes=0;
        $error="";
        //print_r_($good_tool_edgeline_);exit;
        if(!$good_tool_edgeline_["MY_CHECK_KL"])
        {
            foreach ($good_tool_edgeline_ as $key=>$value)
            {
                
                if ($value=="false")  $value=0;
                elseif ($value=="true")  $value=1;
                if ($key<>"ID")
                {
                    if (($res[$key] <> $value)&&(!in_array($key,$array)))
                    {
                        $tool_edgeline_changes++;
                        if($tool_edgeline_changes==1) $error=$error. "<hr> Во внесённом кромкооблицовочном станке ".$res["NAME"]." код (".$good_tool_edgeline_["ID"].") некоторые свойства внесены ошибочно: <hr>";
                        $error=$error. "Атрибут ".$description_giblab[$key]." (".$key.") изменён с ".$value." на ".$res[$key]."<hr> ";
                    }
                }
            
            }
        }
    }
    return ($error);
   
}
function change_tool_edgeline_per_new_values($array,$good_tool_edgeline_,$table,$db,$link,$place,$description_giblab)
{
    #$value="SELECT * FROM `".$db."`.`".$table."` WHERE PLACE = \"".$place."\";";
    $value="SELECT * FROM `TOOL_EDGELINE` inner join TOOL_EDGELINE_EQUIPMENT_CONN on TOOL_EDGELINE.TOOL_EDGELINE_ID = TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_ID 
    inner join TOOL_EDGELINE_EQUIPMENT on TOOL_EDGELINE_EQUIPMENT_CONN.TOOL_EDGELINE_EQUIPMENT_ID = TOOL_EDGELINE_EQUIPMENT.ID 
    where PLACE = ".$place." LIMIT 1";
    $result = sqlDoSelect ($value,$link,$db);
    $res=mysqli_fetch_array($result);
    if (mysqli_num_rows(($result))==1)
    {
        if(!$good_tool_edgeline_["MY_CHECK_KL"])
        {
            foreach ($good_tool_edgeline_ as $key=>$value)
            {
                if ($key<>"ID")
                {
                    if (($res[$key] <> $value)&&(!in_array($key,$array)))
                    {
                        $good_tool_edgeline_[$key]=$res[$key];
                    }
                }
            }
        }
    }
    return ($good_tool_edgeline_);
}
?>