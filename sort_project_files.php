<?php

/**
 * Скрипт находит все файлы в папке и раскладывает их по подпапкам
 * по схеме "ceil(substr(55555.txt, 0, -4) / 1000) - 1"
 * 
 * Если в подпапке уже есть файл с таким именем,
 * то они сравниваются по дате создания и удаляется тот, который создан раньше.
 * **/		

ini_set('memory_limit', '3096M');
ini_set('max_execution_time', '1800');//300 - 5мин, 1800 - 30мин

// require_once 'startup.php';
require_once 'func.php';
$config['storage_dir'] = $serv_main_dir . 'files/project_out';
session_start();

if(!isset($_SESSION['user']) || $_SESSION['user']['ID'] != 218) {
    header('Location: '.$main_dir.'/login.php'.'?nw='.$_GET['nw']);
	exit();
}

$c = 0;
$a = GetFilesNames($config['storage_dir'], true);

echo count($a['files']).'<br>';
echo count($a['folders']).'<br><br><hr>';

foreach ($a['files'] as $value) {
	if (strripos($value, '.txt')) {
		$dirdig = ceil(substr($value, 0, -4) / 1000) - 1;
		if (!file_exists($config['storage_dir'].'/'.$dirdig)) {
			mkdir($config['storage_dir'].'/'.$dirdig , 0777);
		}
		if (!file_exists($config['storage_dir'].'/'.$dirdig.'/'.$value)) {
			if (!copy($config['storage_dir'].'/'.$value, $config['storage_dir'].'/'.$dirdig.'/'.$value)) {
				echo 'Error<br>';
				$c++;
			} else unlink($config['storage_dir'].'/'.$value);
		} else {
			if (md5_file($config['storage_dir'].'/'.$dirdig.'/'.$value) == md5_file($config['storage_dir'].'/'.$value)) {
				unlink($config['storage_dir'].'/'.$value);
			} else {
				if (filemtime($config['storage_dir'].'/'.$dirdig.'/'.$value) > filemtime($config['storage_dir'].'/'.$value)) {
					unlink($config['storage_dir'].'/'.$value);
				} else {
					unlink($config['storage_dir'].'/'.$dirdig.'/'.$value);
				}
			}
		}
	}
}

// $dirdig = ceil(substr($a['files'][540], 0, -4) / 1000) - 1;
if (!$c == 0) {
	echo "Не удалось скопировать ".$c." файлов!<br>";
} else {
	echo "Файлы успешно скопированы!<br>";

	$a = GetFilesNames($config['storage_dir'], true);
	echo 'Файлов: '.count($a['files']).'<br>';
	echo 'Папок: '.count($a['folders']).'<br>';
}


/**
* Получение названий каталогов и файлов в каталоге
**/
function GetFilesNames($dir, $flag=false){
	$result = null;
	$result = scandir($dir);
    $data = array();
	foreach ($result as $file){
    		if($file != '.' && $file != '..'){
       		$data[] = $file;
    		}
	}
	if (!$flag){return $data;}
	else {return onlyFiles($data);}
}
/**
* Функция очищает массив от каталогов "scandir"
**/
function onlyFiles($array){
	$result = [];
	foreach ($array as $value) {
		if (preg_match('#([.][a-z]{2,4}$)#', $value)) {
			$result['files'][] = $value;
		} else $result['folders'][] = $value;
	}
	return $result;
}
?>