<?php
// curl --form order1c=1 --form project=@1.project http://0.0.0.0:8904
// 1 111 

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use React\EventLoop\Factory;
use React\Http\Middleware\LimitConcurrentRequestsMiddleware;
use React\Http\Middleware\RequestBodyBufferMiddleware;
use React\Http\Middleware\RequestBodyParserMiddleware;
use React\Http\Response;
use React\Http\StreamingServer;
include_once ('func.php');
require_once __DIR__ . '/vendor/autoload.php';

$loop = Factory::create();

$handler = function (ServerRequestInterface $request) {
    if ($request->getMethod() === 'POST') {
        $body = $request->getParsedBody();
        $order1c = isset($body['order1c']) && is_string($body['order1c']) ? htmlspecialchars($body['order1c']) : null;
		$client = isset($body['client']) && is_string($body['client']) ? htmlspecialchars($body['client']) : null;
		$place = isset($body['place']) && is_string($body['place']) ? htmlspecialchars($body['place']) : null;
        $manager = isset($body['manager']) && is_string($body['manager']) ? htmlspecialchars($body['manager']) : null;
        if ((!isset($manager))||($manager=='')) $manager=169658;
        $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'fileproc_start', $_SESSION);
		
        $uploads = $request->getUploadedFiles();
        if (isset($uploads['project']) && $uploads['project'] instanceof UploadedFileInterface) {
            $file = $uploads['project'];
            if ($file->getError() === UPLOAD_ERR_OK) {
              $project = $file->getStream();
              #echo $project;
			  $date1111 = date('Y-m-d H:i:s');
			  if (!$order1c) $order1c="NULL";
			  if (!$place) $place="NULL";
			  if (!$client) $client="NULL";
			  $project64=base64_encode ($project);
			  $project_name=$client."_".$order1c."__".time().".project";
			  $bytesCount = file_put_contents("files/".$project_name, $project);
				if ($bytesCount === false) echo "При сохранении данных произошла ошибка!";
			//   $bytesCount = file_put_contents("files64/".$project_name."64", $project64);
            //     if ($bytesCount === false) echo "При сохранении данных произошла ошибка!";
        $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'fileproc_save_file', $_SESSION);
                
			  $sql_file_in="INSERT INTO PROJECT_IN VALUES (NULL,'".$date1111."', '".$project_name."', ".$client.", ".$manager.",".$order1c.", NULL , ".$place.", NULL,NULL,NULL);";
			  echo $sql_file_in;

                $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_file_in)['data'];
				
        $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'fileproc_save_project to db', $_SESSION);
                
              echo `php file_put_to_db.php`;
        $timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'fileproc_put_project to db', $_SESSION);
              
            } elseif ($file->getError() === UPLOAD_ERR_INI_SIZE) {
                echo 'upload exceeds file size limit';
            } else {
                echo 'upload error ' . $file->getError();
            }
        }
    }

    return new Response(
        200,
        array(
            'Content-Type' => 'text/html; charset=UTF-8'
        ),
        ''
    );
};

$server = new StreamingServer(array(
    new RequestBodyBufferMiddleware(8 * 1024 * 1024),
    new RequestBodyParserMiddleware(100 * 1024, 1),
    $handler
));

$socket = new \React\Socket\Server(isset($argv[1]) ? $argv[1] : '0.0.0.0:3001', $loop);
$server->listen($socket);

echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . PHP_EOL;

$loop->run();

?>