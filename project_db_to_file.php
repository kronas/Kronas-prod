<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,HEAD,OPTIONS");
header("Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization");
header("Access-Control-Allow-Headers: *");
date_default_timezone_set('Europe/Kiev');

session_start();

include_once ("func.php");
echo 'Hello World';

/**
 * Функция записывает данные из таблицы "PROJECT", поля "MY_PROJECT_OUT" в файлы.
 * Логика:
 *    Функция работает в два этапа:
 *        1)  записываем в файл (если поле не пустое), если не получается, сохраняем "ID" записи в массив "$write_to_file_errors"
 *        2)  очищаем записи из полей "MY_PROJECT_OUT", если "ID" записи нет в массиве "$write_to_file_errors",
 *            в случае неудачи записываем "ID" в массив "$update_fields_errors"
 *    в таком случае функция получается "безопасной" для нескольких запусков.
 *    Функция не прерывается и выполняется до конца таблицы.
 *    Принимает: -
 *    Возвращает: массив с ошибками в случае неудачи (ключами являются названия массивов с ошибками),
 *                "TRUE" в случае если запись в файл и очистка полей были успешными
 **/
function from_db_to_file()
{
    //.. Если скрипт выполняется не из корня, нужно подредактировать путь
    $dir = '/kronasapp/storage/app/db/MY_PROJECT_OUT/';
    $data = [];
    $write_to_file_errors = [];
    $update_fields_errors = [];
    $return = [];
    $query = 'SELECT * FROM `PROJECT` WHERE `PROJECT_ID` > 29000 AND `PROJECT_ID` < 30000'; ///29845

    //.. Собираем таблицу в массив
//    $result = $mysqli->query($query);
//    while ($row = $result->fetch_assoc($result)) {
//        $data[] = $row;
//    }
    $data = sql_data(__LINE__,__FILE__,__FUNCTION__,$query)['data'];
    //.. Пишем данные из полей MY_PROJECT_OUT в файлы
    foreach ($data as $value) {
        if ($value['MY_PROJECT_OUT'] != '') {
            $fp = fopen($GLOBALS['serv_main_dir'] . $dir . $value['PROJECT_ID'] . '.txt', "w");
            if (!fwrite_stream($fp, $value['MY_PROJECT_OUT'])) {
                $write_to_file_errors[] = $value['PROJECT_ID'];
            }
        }
    }
    p_($write_to_file_errors);
    exit;
    //.. Очищаем поля MY_PROJECT_OUT
    foreach ($data as $value) {
        if (empty($write_to_file_errors) || !in_array($value['PROJECT_ID'], $write_to_file_errors)) {
            $result = $mysqli->query('UPDATE `PROJECT` SET `MY_PROJECT_OUT` = "" WHERE `PROJECT_ID` = "' . $value['PROJECT_ID'] . '"');
            if (!$result) {
                $update_fields_errors[] = $value['PROJECT_ID'];
            }
        }
    }
    //.. Возвращаем либо ошибки, либо "true"
    if (!empty($write_to_file_errors)) {
        $return['write_to_file_errors'] = $write_to_file_errors;
    }
    if (!empty($update_fields_errors)) {
        $return['update_fields_errors'] = $update_fields_errors;
    }
    if (empty($write_to_file_errors) && empty($update_fields_errors)) {
        $return = true;
    }
    return $return;
}

//.. Функция записи в файл
function fwrite_stream($fp, $string)
{
    for ($written = 0; $written < strlen($string); $written += $fwrite) {
        $fwrite = fwrite($fp, substr($string, $written));
        if ($fwrite === false) {
            return $written;
        }
    }
    return $written;
}

from_db_to_file();

?>