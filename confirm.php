<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <title>Подтверждение заказа</title>
    <style >
        .erase-tr{
            border: 1px solid red;
            border-radius: 10px;
            padding-right: 5px;
            margin: 2px 5px;
            display: inline-block;
            cursor: pointer;
        }
        .close-sel {
            position: relative;
            cursor: pointer;
            color: red;
            transform: rotate(-45deg);
            margin: 0 2px 0 10px;
        }
        .loading-search{
            position: absolute;
            right: 22px;
            top: 3px;
            display: none;
        }
        #searchOnCode-box{padding: 10px;display: none;}
        #searchOnCode-box li{ cursor: pointer; }
        form{
            margin: 10px;
        }
        h3.success  {
            color: green;
        }
        body {
            padding: 45px;
        }
    </style>


</head>
<body>
<?php
session_start();

//.. Сохраняем важные данные сессии
$session_restore = array();
if (isset($_SESSION['project_manager'])) {
    $session_restore['project_manager'] = $_SESSION['project_manager'];
}
if (isset($_SESSION['project_client'])) {
    $session_restore['project_client'] = $_SESSION['project_client'];
}
if (isset($_SESSION['user'])) {
    $session_restore['user'] = $_SESSION['user'];
}
if (isset($_SESSION['tec_info'])) {
    $session_restore['tec_info'] = $_SESSION['tec_info'];
}

ini_set('max_input_time', '0');
ini_set('max_input_vars', '190000');
ini_set('max_input_nesting_level', '164');
ini_set('post_max_size', '850M');

require_once '_1/config.php';
require_once DIR_CORE . 'func.php';
require_once DIR_CORE . 'func_mini.php';
require_once DIR_CORE . 'description.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$timer=timer(0,__FILE__,__FUNCTION__,__LINE__,'confirm_start', $_SESSION);


if($_GET['get_programms']==1) {
    $structure = $_SESSION['structure'];
    $structure_mpr_bhx = $_SESSION['all_programms']['mpr_bhx'];
    $structure_mpr_venture = $_SESSION['all_programms']['mpr_venture'];
    $structure_ptx = $_SESSION['all_programms']['ptx'];
    $structure_cadmatic = $_SESSION['all_programms']['cadmatic'];

    $zip = new ZipArchive();

    $zip->open($structure.'programm.zip',ZIPARCHIVE::CREATE);

    $files = scandir($structure_mpr_bhx); // $papka перем. с путэм к папке
    $zip->addEmptyDir('mpr_bhx');
    foreach($files as $file){
        if ($file == '.' || $file == '..' ){continue;}
        $f = $structure_mpr_bhx.DIRECTORY_SEPARATOR.$file;
        $zip->addFile($f, 'mpr_bhx/'.$file);
    }
    $files = scandir($structure_mpr_venture); // $papka перем. с путэм к папке
    $zip->addEmptyDir('mpr_venture');
    foreach($files as $file){
        if ($file == '.' || $file == '..' ){continue;}
        $f = $structure_mpr_venture.DIRECTORY_SEPARATOR.$file;
        $zip->addFile($f, 'mpr_venture/'.$file);
    }
    $files = scandir($structure_ptx); // $papka перем. с путэм к папке
    $zip->addEmptyDir('ptx');
    foreach($files as $file){
        if ($file == '.' || $file == '..' ){continue;}
        $f = $structure_ptx.DIRECTORY_SEPARATOR.$file;
        $zip->addFile($f, 'ptx/'.$file);
    }
    $files = scandir($structure_cadmatic); // $papka перем. с путэм к папке
    $zip->addEmptyDir('lc4');
    foreach($files as $file){
        if ($file == '.' || $file == '..' ){continue;}
        $f = $structure_cadmatic.DIRECTORY_SEPARATOR.$file;
        $zip->addFile($f, 'lc4/'.$file);
    }

    $zip->close();

    header('Location: '.$main_dir.'/files/programm/'.$_SESSION['zip_catalog'].'programm.zip');
    exit;
}

if ($_GET["data_id"]) {
    $_SESSION = get_temp_file_data($_GET["data_id"]);

    //.. Восстанавливаем сессию
    if (isset($session_restore['project_manager'])) {
        $_SESSION['project_manager'] = $session_restore['project_manager'];
    }
    if (isset($session_restore['project_client'])) {
        $_SESSION['project_client'] = $session_restore['project_client'];
    }
    if (isset($session_restore['user'])) {
        $_SESSION['user'] = $session_restore['user'];
    }
    if (isset($session_restore['tec_info'])) {
        $_SESSION['tec_info'] = $session_restore['tec_info'];
    }
}

$glue_color = $_SESSION['glue_color'];

if ($_SESSION['is_manager'] == 1) $isGuest = false;
else $isGuest = true;

$manager_id=$_SESSION['manager_id'];
if (($manager_id > 0) && (is_numeric($manager_id))) {
    $sql_m = 'SELECT * FROM `manager` WHERE `id` = '.$manager_id;
    $manager_a = sql_data(__LINE__,__FILE__,__FUNCTION__, $sql_m);
}
if ((!isset($manager_a))||($manager_a['res']<>1)) $manager_id=39; 
if ($manager_a['data'][0]['admin']==1) $manager_admin = 1;
else $manager_admin = 0;
if(isset($_SESSION['client_fio'])) $client_fio = $_SESSION['client_fio'];
else $client_fio='Неизвестно';
if(isset($_SESSION['client_phone'])) $client_phone = $_SESSION['client_phone'];
else $client_phone=0000000000;
if(isset($_SESSION['client_email'])) $client_email = $_SESSION['client_email'];
else $client_email='testzzz@kronas.com.ua';
if($_SESSION['order_comment'] == '') $_SESSION['order_comment'] = null;
if((isset($_SESSION['client_id']))&&(is_numeric($_SESSION['client_id'])))
{
    $client_id_sql = 'SELECT client_id FROM `client` WHERE `client_id` = '.$_SESSION['client_id'];
    $client_id=sql_data(__LINE__,__FILE__,__FUNCTION__,$client_id_sql)['data'][0]['client_id'];
    if((isset($_SESSION['client_id']))&&($client_id==$_SESSION['client_id'])) $client_id = $_SESSION['client_id'];
    else $client_id=85159;
}
else $client_id=85159;
if((isset($_SESSION['client_code']))&&(is_numeric($_SESSION['client_code'])))
{
    $client_code_sql = 'SELECT * FROM `client_code` WHERE `code_ac` = '.$_SESSION['client_code'];
    $client_code=sql_data(__LINE__,__FILE__,__FUNCTION__,$client_code_sql)['data'][0]['code_ac'];
    if(isset($_SESSION['client_code'])&&($client_code==$_SESSION['client_code'])) $client_code = $_SESSION['client_code'];
    else $client_code=164180;
}
else $client_code=164180;



function project_to_akcent($project_id, $link, $client_code) {
    global $kronas_api_link;

    $curl = curl_init();
    $manager_admin = 0;
    if($_SESSION['manager_id'] && $_SESSION['manager_id'] != '') {
        $manager_req = 'SELECT * FROM `manager` WHERE `id` = '.$_SESSION['manager_id'];
        $manager_data=sql_data(__LINE__,__FILE__,__FUNCTION__,$manager_req)['data'][0];
        $manager_code=$manager_data['code'];
        $manager_admin=$manager_data['admin'];

    } else {
        $manager_code = '34043';
    }
    curl_setopt_array($curl, array(
        CURLOPT_URL => $kronas_api_link . "/gibLabService/".$project_id."/".$manager_code."/".$client_code.'?host=service.kronas.com.ua:3306',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 45,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
    ));

    $response = curl_exec($curl);
    curl_close($curl);

    $_SESSION['curl_akcent_link'] = $kronas_api_link . "/gibLabService/".$project_id."/".$manager_code."/".$client_code;

    return $response;
}

if ($_SESSION['project_data']<>'')
{
    $_SESSION['project_data'] = xml_parser($_SESSION['project_data']);
    $p = xml_parser_create();
    xml_parse_into_struct($p, $_SESSION['project_data'], $vals, $index);
    xml_parser_free($p);
}
else $vals=$_SESSION['res']['vals'];

if (!empty($glue_color)) $order_new_comment = '[Клей: ' . $glue_color . ']' . $vals[0]['attributes']['DESCRIPTION'];
else $order_new_comment = $vals[0]['attributes']['DESCRIPTION'];

if (!isset($_SESSION['manager_addition_place'])) $_SESSION['manager_addition_place'] = 'NULL';

if($_SESSION['order_status'] == 1) {
    $sql_checks = 'SELECT * FROM `ORDER1C` WHERE `ID` = '.$_SESSION['order_1c_project'];
    $sql_checks_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_checks)['data'][0]['DB_AC_NUM'];
    $sql_checks_status = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_checks)['data'][0]['status'];
    
    if(strpos($sql_checks_data,'in_edits_') === 0 || $sql_checks_status == 'черновик') {
        $sql = "INSERT INTO `ORDER1C`( `PLACE`, `addition_place`, `manager`, `client_fio`, `client_phone`, `client_email`, `CLIENT`, `CLIENT_ID`, `production`, `stanki`, `comment`, `glue_color`) 
            VALUES (".$_SESSION['user_place'].",".$_SESSION['manager_addition_place'].",".$manager_id.",'".$client_fio."','".$client_phone."','".$client_email."',".$client_code.",".$client_id.", 1, NULL, '".$order_new_comment."', '".$glue_color."')";
        
        $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

        $order1c=$query['id'];
        
        $del_draft=1;

    } else {
        $sql = "UPDATE `ORDER1C` SET `PLACE`='".$_SESSION['user_place']."',`addition_place`=".$_SESSION['manager_addition_place'].",`manager`='".$manager_id."',`client_fio`='".$client_fio."',`client_phone`='".$client_phone."',`client_email`='".$client_email."',`CLIENT_ID`='".$client_id."',`CLIENT`='".$client_code."', `stanki`=NULL, `comment`='".$order_new_comment."', `glue_color`='".$glue_color."' WHERE `ID`=".$_SESSION['order_1c_project'];
        $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        if ($query['res']==1) $order1c= $_SESSION['order_1c_project'];
    }

} else {
    if (($_SESSION['user_place']>0)&&(is_numeric($_SESSION['user_place'])))
    {
        $sql = "INSERT INTO `ORDER1C`( `PLACE`, `addition_place`, `manager`, `client_fio`, `client_phone`, `client_email`, `CLIENT`, `CLIENT_ID`, `production`, `stanki`, `comment`, `glue_color`) 
                VALUES (".$_SESSION['user_place'].",".$_SESSION['manager_addition_place'].",".$manager_id.",'".$client_fio."','".$client_phone."','".$client_email."',".$client_code.",".$client_id.", 1, NULL, '".$order_new_comment."', '".$glue_color."')";
        $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

        $order1c=$query['id'];
    }
}
if (($vals[0]['attributes']['project.my_rs']>0)||($vals[0]['attributes'][mb_strtoupper('project.my_rs')]>0))
{
    $sql1 = "UPDATE `ORDER1C` SET `rs_order`=1 WHERE `ID`=".$order1c;
    $query1 = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql1);
}
$service_price=$_SESSION['service_price'];
$band_price=$_SESSION['band_price'];
$_simple_price=$_SESSION['simple_price'];
$material_price=$_SESSION['material_price'];
$mat_kl=$_SESSION['res']['mat_kl'];
$_simple_price_extra=$_SESSION['simple_price_extra'];


if($_SESSION['order_status'] == 1) {
    $sql_d = 'DELETE FROM `ORDER1C_GOODS` WHERE `ORDER1C_GOODS_ID` = '.$order1c;
    $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_d);

}

foreach ($service_price as $item) {
    set_1corder_goods($order1c, $item, $link, 'service');
}

foreach ($band_price as $item) {
    set_1corder_goods($order1c, $item, $link, 'band');
}
foreach ($_simple_price as $item) {
    set_1corder_goods($order1c, $item, $link, 'simple');
}
foreach ($material_price as $item) {
    set_1corder_goods($order1c, $item, $link, 'material');
}

foreach($_simple_price_extra as $item) {
    switch ($item['type']) {
        case 'band':
            set_1corder_goods($order1c, $item, $link, 'band');
            break;
        case 'material':
            set_1corder_goods($order1c, $item, $link, 'material');
            break;
        case 'simple':
            set_1corder_goods($order1c, $item, $link, 'simple');
            break;
    }
}
// echo $_SESSION['project_data']; 
if (($order1c>0)&&(is_numeric($order1c)))
{
    $_SESSION['vals']=add_tag_print($_SESSION['vals']);
    foreach($_SESSION['vals'] as $i=>$v)
    {
        if (($v['attributes']['MY_1C_NOM']==5931)&&($v['attributes']['good.my_client_material']<>"1")&&($v['attributes']['TYPEID']=='sheet'))
        {
        //    p_($v);
            $sql='UPDATE ORDER1C SET `ready_to_product`=0 WHERE `ID`='.$order1c;
            $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        }
        else
        {
            $sql='UPDATE ORDER1C SET `ready_to_product`=1 WHERE `ID`='.$order1c;
            $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        }
    }
}

//.. Создаём переменную с датой, а так-же переменную с данными для записи в файл 
$tmp_date = date('Y-m-d H:i:s');
if ((!isset($_SESSION['user_type_edge']))||(!isset($_SESSION['user_place'])))
{
    $err=print_r($_SESSION,true);
    my_mail('Проблема с определением участка либо типа кромкования при записи заказа! '.__LINE__.' / '.__FILE__.' / '.__FUNCTION__,'',$err,'txt');
    exit ('<h1>Проблема с определением участка либо типа кромкования при записи заказа!</h1>');

}
$_SESSION['vals'][0]['attributes']["project.user_type_edge"]=$_SESSION['user_type_edge'];
$_SESSION['vals'][0]['attributes']["project.user_place"]=$_SESSION['user_place'];
$_SESSION['vals'][0]['attributes']["project.sess_id"]=$_SESSION['session_id'];
$_SESSION['vals'][0]['attributes']["project.manager_id"]=$_SESSION['manager_id'];
$tmp_fs_data = base64_encode(vals_index_to_project($_SESSION['vals']));
if($_SESSION['order_status'] == 1) {
    if($_SESSION['is_manager'] == 1) {
        $sql = "INSERT INTO `PROJECT`(`DATE`, `ORDER1C`, `MY_PROJECT_OUT`, `comment`, `manager_id`,`place`,`user_type_edge`) 
            VALUES ('".$tmp_date."','".$order1c."','','".$_SESSION['order_comment']."', ".$manager_id.",".$_SESSION['user_place'].",'".$_SESSION['user_type_edge']."')";
    } elseif($_SESSION['is_client'] == 1) {
        $sql = "INSERT INTO `PROJECT`(`DATE`, `ORDER1C`, `MY_PROJECT_OUT`, `comment`, `client_id`,`place`,`user_type_edge`) 
            VALUES ('".$tmp_date."','".$order1c."','','".$_SESSION['order_comment']."', ".$client_id.",".$_SESSION['user_place'].",'".$_SESSION['user_type_edge']."')";
    } else {
        $sql = "INSERT INTO `PROJECT`(`DATE`, `ORDER1C`, `MY_PROJECT_OUT`, `comment`,`place`,`user_type_edge`) 
            VALUES ('".$tmp_date."','".$order1c."','','".$_SESSION['order_comment']."',".$_SESSION['user_place'].",'".$_SESSION['user_type_edge']."')";
    }

} else {
    $sql = "INSERT INTO `PROJECT`(`DATE`, `ORDER1C`, `MY_PROJECT_OUT`, `comment`,`place`,`user_type_edge`) 
            VALUES ('".$tmp_date."','".$order1c."','','".$_SESSION['order_comment']."',".$_SESSION['user_place'].",'".$_SESSION['user_type_edge']."')";
}
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'confirm_prepare', $_SESSION);

if (($order1c > 0) && (is_numeric($order1c))) {

    $sql1 = 'SELECT * FROM ORDER1C WHERE ID = ' . $order1c . ' LIMIT 1';
    $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql1);

    if ($query['res'] > 0) {

        $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
        $project = $query['id'];

        //.. 20.03.2023 Тоценко. Ккопия записи таблицы проекта в папку ps_share/projects_backup
        $dirdig = ceil($project / 1000) - 1;
        $project_backup_path = DIR_CORE . 'files/ps_share/projects_backup/' . $dirdig;
        if (!file_exists($project_backup_path)) mkdir($project_backup_path, 0777, true);
        $q = "SELECT * FROM `PROJECT` WHERE `PROJECT_ID` = $project LIMIT 1";
        $getProject = sql_data(__LINE__,__FILE__,__FUNCTION__,$q)['data'][0];
        file_put_contents($project_backup_path . '/' . $project . '.json', json_encode($getProject));

         //.. Находим PROJECT_ID в базе для использования его в качестве имени файла
        $sql_PID='SELECT * FROM `PROJECT` WHERE `ORDER1C` = "'.$order1c.'"';
        $query_PID = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_PID);
        if ($query_PID) {
            //..23.11.2020 Сохраняем в файл:
            put_Project_file($query_PID['data'][0]['PROJECT_ID'],$tmp_fs_data);
        }

        $_SESSION['accent_response'] = project_to_akcent($order1c, $link, $client_code);

        //.. 11.08.2022 Проверка на кромки фирмы MAAG
        // Если есть такие, то запрещаем PUR-кромкование на странице подтверждения заказа
        // Признак наличия этих кромок - наличие ключа в сессии 'pur_edge_denied'
        // и здесь мы его очищаем
        unset($_SESSION['pur_edge_denied']);
        
        $timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'confirm_akcent_put', $_SESSION);

        $akcent_id_query = "SELECT * FROM `ORDER1C` WHERE `ID` = " . $order1c . " LIMIT 1";
        $newOrder = sql_data(__LINE__,__FILE__,__FUNCTION__,$akcent_id_query)['data'][0];
        $new_order_id = $newOrder['DB_AC_ID'];

        //.. 20.03.2023 Тоценко. Копия заказа в папку ps_share/orders_backup
        $order_backup_path_year = DIR_CORE . 'files/ps_share/orders_backup/' . date('Y');
        $order_backup_path_month = $order_backup_path_year . '/' . date('m');
        $order_backup_path = $order_backup_path_month . '/' . date('d');
        if (!file_exists($order_backup_path_year)) mkdir($order_backup_path_year, 0777, true);
        if (!file_exists($order_backup_path_month)) mkdir($order_backup_path_month, 0777, true);
        if (!file_exists($order_backup_path)) mkdir($order_backup_path, 0777, true);
        
        $newOrder['projects'] = [];

        if (file_exists($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json')) {
            $savedOrder = (array)json_decode(file_get_contents($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json'));
            $savedProjects = $savedOrder['projects'];

            if (!in_array($project, $savedProjects)) array_push($savedProjects, $project);
            
            foreach ($savedProjects as $value) $newOrder['projects'][] = $value;
        } else array_push($newOrder['projects'], $project);

        file_put_contents($order_backup_path . '/' . $newOrder['ID'] . '.json', json_encode($newOrder));
        //^^
    }
    else
    {
        unset($project);
        unset ($new_order_id);
    }
}

if(!isset($new_order_id) || $new_order_id == '' || $new_order_id == NULL)
{

    echo '<div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="danger">Ошибка подтверждения !</h3>
                        <hr>
                        <p>Версия проекта была сохранена в хранилище заказов</p>
                        <p>Обратитесь по указанным контактным данным:</p>
                        <b>телефон:<br>
                            +38(044)390-00-07<br>
                            +38(067)622-58-07<br>
                            +38(050)474-10-99<br>
                        </b>
                        <p>email: <b><a href="mailto:uslugi@kronas.com.ua">uslugi@kronas.com.ua</b></p>
                    </div>
                </div>
          </div>';

    $sql_m = 'SELECT * FROM `manager` WHERE `id` = '.$manager_id;
    
    $manager_admin=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_m)['data'][0]['admin'];
    if ($_SESSION['order_1c_project']>0)
    {
        $order1c=$_SESSION['order_1c_project'];
        if (($order1c>0)&&(is_numeric($order1c)))
        {
            $sql='SELECT * FROM ORDER1C WHERE ID='.$order1c;
            $query = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

            if ($query['res']>0)
            {
                //.. Создаём переменную с датой, а так-же переменную с данными для записи в файл
                $tmp_date = date('Y-m-d H:i:s');
                $tmp_fs_data = base64_encode($_SESSION['project_data']);
                $sql_save_project_insert = 'INSERT INTO `PROJECT`(`status`, `DATE`, `ORDER1C`, `manager_id`, `MY_PROJECT_OUT`,`place`,`user_type_edge`) VALUES ("start", "'.$tmp_date.'", '.$_SESSION['order_1c_project'].', '.$manager_id.', "",'.$_SESSION['user_place'].',"'.$_SESSION['user_type_edge'].'")';
                $q=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_save_project_insert);
                $newProject = $q['id'];
                $q = $q['res'];

                //.. 20.03.2023 Тоценко. Ккопия записи таблицы проекта в папку ps_share/projects_backup
                $dirdig = ceil($newProject / 1000) - 1;
                $project_backup_path = DIR_CORE . 'files/ps_share/projects_backup/' . $dirdig;
                if (!file_exists($project_backup_path)) mkdir($project_backup_path, 0777, true);
                $q = "SELECT * FROM `PROJECT` WHERE `PROJECT_ID` = $newProject LIMIT 1";
                $getProject = sql_data(__LINE__,__FILE__,__FUNCTION__,$q)['data'][0];
                file_put_contents($project_backup_path . '/' . $newProject . '.json', json_encode($getProject));

                //.. Находим PROJECT_ID в базе для использования его в качестве имени файла
                $sql_PID='SELECT * FROM `PROJECT` WHERE `ORDER1C` = "'.$_SESSION['order_1c_project'].'"';
                $query_PID = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_PID);

                if ($query_PID) {
                    //..23.11.2020 Сохраняем в файл:
                    put_Project_file($query_PID['data'][0]['PROJECT_ID'],$tmp_fs_data);
                }

                //.. 20.03.2023 Тоценко. Копия заказа в папку ps_share/orders_backup
                $order_backup_path_year = DIR_CORE . 'files/ps_share/orders_backup/' . date('Y');
                $order_backup_path_month = $order_backup_path_year . '/' . date('m');
                $order_backup_path = $order_backup_path_month . '/' . date('d');
                if (!file_exists($order_backup_path_year)) mkdir($order_backup_path_year, 0777, true);
                if (!file_exists($order_backup_path_month)) mkdir($order_backup_path_month, 0777, true);
                if (!file_exists($order_backup_path)) mkdir($order_backup_path, 0777, true);
                
                if (file_exists($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json')) {
                    $savedOrder = (array)json_decode(file_get_contents($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json'));
                    if (!in_array($newProject, $savedOrder['projects'])) array_push($savedOrder['projects'], $newProject);
                    file_put_contents($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json', json_encode($savedOrder));
                } else file_put_contents($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json', json_encode($savedOrder));
                //^^
            }
        }
    }
    
    $mail_params=$GLOBALS['mail_params'];

    $mail = new PHPMailer;
    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->isSMTP();
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = $mail_params['Host'];
    $mail->Port = $mail_params['Port']; // typically 587
    $mail->SMTPSecure = $mail_params['SMTPSecure']; // ssl is depracated
    $mail->SMTPAuth = $mail_params['SMTPAuth'];
    $mail->Username = $mail_params['Username'];
    $mail->Password = $mail_params['Password'];

    $mail->setFrom($mail_params['From'], 'Service-Kronas: Reports');
    $mail->addAddress('gibservice.support@kronas.com.ua', 'GibService Support');
    // $mail->SMTPDebug = 2;


    $mail->Subject = 'АВТООТЧЕТ по ошибке: project_to_akcent()';
        $rt45=get_defined_vars();
        $rt45=print_r($rt45,true);
        $tmpDir = sys_get_temp_dir();
        $temp_file = tempnam($tmpDir, 'tmp');
        $file = fopen($temp_file,"w");
        fwrite($file, $rt45);
        fclose($file);
    
    
    $text_erorr_email = '
            <p>ID заказа: '.$order1c.'</p>
            <p>Менеджер: '.$manager_name.'</p>
            <p>Строка запроса в акцент: '.$_SESSION['curl_akcent_link'].'</p>
            <p>Ошибка записи заказа в акцент</p>
            <hr>
        '.$rt45;

    $mail->msgHTML($text_erorr_email); // remove if you do not want to send HTML email
    ////$mail->AltBody = 'Ваш заказ успешно подтвержден!';
    $mail->addAttachment($temp_file); //Attachment, can be skipped
    $mail->send();
    exit;
}
if ($del_draft==1)
{
    $sql_update = 'UPDATE `PROJECT` SET `ORDER1C` = '.$order1c.' WHERE `ORDER1C` = '.$_SESSION['order_1c_project'];
        $query_update = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update);
        
    $sql_update_ticket='UPDATE ticket set order_id='.$order1c.' WHERE order_id='.$_SESSION['order_1c_project'];
    $query_update_ticket = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_ticket);
    if ($query_update_ticket['res']==1)
    {
        $sql_update_ticket1='UPDATE ticket_construct_mpr set ORDER1C='.$order1c.' WHERE ORDER1C='.$_SESSION['order_1c_project'];
        $query_update_ticket1 = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_ticket1);
        if ($query_update_ticket1['res']==1)
        {
            //.. 20.03.2023 Тоценко. Копия заказа в папку ps_share/orders_backup
            // Удаление не подтверждённого заказа (черновой версии) и запись подтверждённого
            $akcent_id_query = "SELECT * FROM `ORDER1C` WHERE `ID` = " . $order1c . " LIMIT 1";
            $newOrder = sql_data(__LINE__,__FILE__,__FUNCTION__,$akcent_id_query)['data'][0];

            $order_backup_path_year = DIR_CORE . 'files/ps_share/orders_backup/' . date('Y');
            $order_backup_path_month = $order_backup_path_year . '/' . date('m');
            $order_backup_path = $order_backup_path_month . '/' . date('d');
            if (!file_exists($order_backup_path_year)) mkdir($order_backup_path_year, 0777, true);
            if (!file_exists($order_backup_path_month)) mkdir($order_backup_path_month, 0777, true);
            if (!file_exists($order_backup_path)) mkdir($order_backup_path, 0777, true);
            unlink($order_backup_path . '/' . $_SESSION['order_1c_project'] . '.json');
            //^^

            $sql_order_delete = 'DELETE FROM `ORDER1C` WHERE `ID` ='.$_SESSION['order_1c_project'];
            $query_delete = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_order_delete);
            $_SESSION['order_1c_project']=$order1c;
        }
    }
}

$new_pr_name=sql_data(__LINE__,__FILE__,__FUNCTION__,$akcent_id_query)['data'][0];

// if (!isset($new_pr_name['DB_AC_NUM'])) echo "Нет номера заказа с БД.";

///echo $akcent;
//$_SESSION['project_data'] = xml_parser($_SESSION['project_data']);
//$p = xml_parser_create();
//xml_parse_into_struct($p, $_SESSION['project_data'], $vals, $index);
//xml_parser_free($p);

$vals=vals_out($vals);

$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'confirm_vals_chnge_start', $_SESSION);

$vals[0]['attributes']['NAME']=str2url($new_pr_name['DB_AC_NUM']);
$vals[0]['attributes']['project.my_order_name']=str2url($new_pr_name['DB_AC_NUM']);
$vals[0]['attributes']['project.my_order_id']=$new_pr_name['DB_AC_ID'];
$vals[0]['attributes']['project.my_project_db']=$project;
$vals[0]['attributes']['project.my_order_1c']=$order1c;
$vals[0]['attributes']['project.my_order_date']=date_create()->format('Y-m-d H:i:s');
$vals[0]['attributes']['project.my_client']=$new_pr_name['client_fio'];
$vals[0]['attributes']['project.my_client_tel']=$new_pr_name['client_phone'];
$vals[0]['attributes']['project.my_client_email']=$new_pr_name['client_email'];
$vals[0]['attributes']['DESCRIPTION']=$_SESSION['order_comment'];
$vals[0]['attributes']["project.user_type_edge"]=$_SESSION['user_type_edge'];
$vals[0]['attributes']["project.user_place"]=$_SESSION['user_place'];
$vals[0]['attributes']["project.sess_id"]=$_SESSION['session_id'];
$vals[0]['attributes']["project.manager_id"]=$_SESSION['manager_id'];
$index=make_index($vals);
foreach ($index["OPERATION"] as $i)
{
    if (($vals[$i]["attributes"]["TYPEID"]=="XNC")&&(!isset($vals[$i]["attributes"]['operation.my_mpr_in']))&&(!isset($vals[$i]["attributes"][mb_strtoupper('operation.my_mpr_in')])))
    {
        // p_($vals[$i]);
        if($vals[$i]["attributes"]["SIDE"]=='true') $sides='f';
        else $sides='b';

        // $rtrtr=$vals[0]['attributes']['project.my_order_name'];
        $vals[$i]["attributes"]["CODE"]=($vals[$i]['attributes']['ID']/1).'_'.$sides;
    }
}
// echo vals_index_to_project($vals);
// p_($_SESSION);
$vals=put_to_db($vals,$link,2,$order1c,$project,$_SESSION['part']);
$_SESSION['project_data']=vals_index_to_project($vals);
$_SESSION['vals'] = $vals;
$_SESSION['project_data'] = xml_parser($_SESSION['project_data']);
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'confirm_vals_chnge_end', $_SESSION);

// echo $_SESSION['project_data'];
if ($project>0)
{
    //..23.11.2020 Создаём переменную с датой, а так-же переменную с данными для записи в файл
    $tmp_fs_data = base64_encode($_SESSION['project_data']);
    $sql = "UPDATE PROJECT set MY_PROJECT_OUT='' WHERE PROJECT_ID='".$project."'";
    $query=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);

    //..23.11.2020 Сохраняем в файл:
    put_Project_file($project,$tmp_fs_data);
    
    if ($query['res']==1)
    {
        foreach ($mat_kl as $mc_id=>$count)
        {
            $sql='SELECT * FROM MATERIAL_CLIENT_ORDER1C WHERE mat_cl_id='.$mc_id.';';
            $mat_put_ch=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'][0];
            if (count($mat_put_ch)<1)
            {
                $sql = 'INSERT INTO MATERIAL_CLIENT_ORDER1C (`order1c`,`mat_cl_id`,`count`) VALUES ('.$order1c.','.$mc_id.','.$count.');';
                $mat_put=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                unset($mat_put_ch);
            }
            
            else 
            {
                $sql = 'UPDATE MATERIAL_CLIENT_ORDER1C SET order1c='.$order1c.', mat_cl_id='.$mc_id.', count='.$count.' WHERE mat_cl_id='.$mc_id.';';
                $mat_put=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql);
                unset($mat_put_ch);
            }
        }
    }
}

$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'confirm_update_project', $_SESSION);


$sql_m = 'SELECT * FROM `manager` WHERE `id` = '.$manager_id;
$m_q=sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_m)['data'][0];

//.. Получаем ID созданного проекта
$sql_m = 'SELECT `PROJECT_ID` FROM `PROJECT` WHERE `ORDER1C` = '.$order1c;
$RS_project_id = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_m)['data'][0];
$RS_project_id = $RS_project_id['PROJECT_ID'];


$manager_email = $m_q['e-mail'];
$manager_name = $m_q['name'];
$manager_phone = $m_q['phone'];


if($manager_id == 39) {
    $manager_email = '<b><a href="mailto:uslugi@kronas.com.ua">uslugi@kronas.com.ua</b>';
    $manager_name = '';
    $manager_phone = '
        <b>
            <br>
            +38(044)390-00-07<br>
            +38(067)622-58-07<br>
            +38(050)474-10-99<br>
        </b>';
}



if ($manager_id>0)
{
    if($isGuest) {
        echo '
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="success">Заказ успешно отправлен!</h3>
                        <hr>
                        <p>Номер вашего заказа: <b>'.$new_order_id.' ('.$new_pr_name['DB_AC_NUM'].') / ('.$order1c.')</b></p>
                        <p>Контактные данные вашего менеджера:<br>
                            <b>'.$manager_name.'</b><br>
                            <span>тел: '.$manager_phone.'</span><br>
                            <span>email: '.$manager_email.'</span><br>
                        </p>';
        if($_SESSION['user_place']<>1 && !$isGuest) {
            echo '<div>
                       <!-- <a href="'.$main_dir.'/confirm.php?get_programms=1" target="_blank" class="btn btn-info">Программы</a>
                        <a href="'.$laravel_dir.'/projects_new/'.base64_encode('1_'.$order1c).'" target="_blank" class="btn btn-danger">Вернутся к редактированию </a> -->
                        <a href="'.$laravel_dir.'/start_new_porject" class="btn btn-success">Новый заказ</a>
                  </div>';

        } else {
            echo '<div>
                        <!-- <a href="'.$laravel_dir.'/projects_new/'.base64_encode('1_'.$order1c).'" class="btn btn-danger">Вернутся к редактированию </a> -->
                        <a href="'.$laravel_dir.'/start_new_porject_client?client_code='.$_SESSION['client_code'].'&client_id='.$_SESSION['client_id'].'" class="btn btn-success">Новый заказ</a>
                  </div>';
        }
        echo '    
                    </div>
                </div>    
            </div>
        ';
    } else {
        echo '
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="success">Заказ успешно отправлен!</h3>
                        <hr>
                        <p>Номер заказа: <b>'.$new_order_id.' ('.$new_pr_name['DB_AC_NUM'].') / ('.$order1c.')</b></p>
                        <hr>';
        if($_SESSION['user_place']<>1 || ($manager_admin == 1 && $_SESSION['user_place']<>1)) {
            echo '<div>
                       <!-- <a href="'.$main_dir.'/confirm.php?get_programms=1" target="_blank" class="btn btn-info">Программы</a>
                        <a href="'.$laravel_dir.'/projects_new/'.base64_encode('1_'.$order1c).'" target="_blank" class="btn btn-danger">Вернутся к редактированию </a>
                        <a href="'.$laravel_dir.'/start_new_porject" class="btn btn-success">Новый заказ</a> -->
                        <a href="'.$main_dir.'/manager_all_order.php" target="_blank" class="btn btn-info">В хранилище заказов</a>
                        
                  </div>';
        } else {
            echo '<div>
                        <!--<a href="'.$laravel_dir.'/projects_new/'.base64_encode('1_'.$order1c).'" class="btn btn-danger">Вернутся к редактированию </a>
                        <a href="'.$laravel_dir.'/start_new_porject" class="btn btn-success">Новый заказ</a>-->
                        <a href="'.$main_dir.'/manager_all_order.php" target="_blank" class="btn btn-info">В хранилище заказов</a>
                  </div>';
        }
        echo '
                    </div>
                </div>    
            </div>
        ';
    }


}


function send_text_mail($order_id, $text, $adress, $adress_name, $sender_adress, $sender_name, $copy, $full_project_name) {
    $mail = new PHPMailer;
    $mail_params=$GLOBALS['mail_params'];
    
    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->isSMTP();
//    $mail->SMTPDebug = 2;
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = $mail_params['Host'];
    $mail->Port = $mail_params['Port']; // typically 587
    $mail->SMTPSecure = $mail_params['SMTPSecure']; // ssl is depracated
    $mail->SMTPAuth = $mail_params['SMTPAuth'];
    $mail->Username = $mail_params['Username'];
    $mail->Password = $mail_params['Password'];

    $mail->setFrom($mail_params['From'], 'KRONAS');
    $mail->addAddress($adress, $adress_name);
    $mail->addAddress('rejust.web@gmail.com', 'DEV');

    if($copy) {
        foreach ($copy as $email) {
            $mail->AddCC($email, 'Менеджер');
        }

    }



    $mail->Subject = 'Заказ №'.$full_project_name.' успешно подтвержден';
    $mail->msgHTML($text); // remove if you do not want to send HTML email
    $mail->AltBody = 'Ваш заказ успешно подтвержден!';
    ///$mail->addAttachment('docs/brochure.pdf'); //Attachment, can be skipped

    $mail->send();
}


$full_project_name = $new_order_id.' ('.str2url($new_pr_name['DB_AC_NUM']).') / ('.$project.') от '.date('Y-m-d');

//.. 17.11.2022 Тоценко. В условие добавлены проверки на менеджера. Когда менеджер подтверждает заказ, то клиенту не отправляется письмо
if(($client_email != 'Не указан' || $client_email != 'testzzz@kronas.com.ua') && (!isset($_SESSION['is_manager']) || $_SESSION['is_manager'] != 1)) {

// Закомментированный код содержит ссылку на Раздвижные системы, они недоработаны, поэтому заменяем на код, где ссылки на РС нет
//     $text_client = '
//                     <p>Заказ успешно оформлен!</p>
//                     <p>Был сформирован заказ №'.$new_order_id.'('.str2url($new_pr_name['DB_AC_NUM']).') / ('.$project.') от '.date('Y-m-d').', перейти к редактированию заказа можно по ссылкам:</p>
//                     <p><a href="'.$laravel_dir.'/projects_new/1_'.$order1c.'">редактировать заказ в GibLab</a></p>
//                     <p><a href="'.$main_dir.'/clients/RS/clients/create.php?order_project_id='.$RS_project_id.'">редактировать заказ в конструкторе раздвижных систем</a></p>
//                     <p>Номер заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_NUM'].'</b></p>
//                      <p>ID заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_ID'].'</b></p>
//                      <p><br></p>
//                      <p><b>Данные клиента:</b></p>
//                      <p>Имя: <b>'.$client_fio.'</b></p>
//                      <p>Телефон: <b>'.$client_phone.'</b></p>
//                      <p>E-mail: <b>'.$client_email.'</b></p>
//                      <p><br></p>
//                      <p><b>Менеджер:<b><br>
//                          <b>'.$manager_name.'</b><br>
//                          <span>тел: '.$manager_phone.'</span><br>
//                          <span>email: '.$manager_email.'</span><br>
//                     </p>
// ';
$text_client = '
                    <p>Заказ успешно оформлен!</p>
                    <p>Был сформирован заказ №'.$new_order_id.'('.str2url($new_pr_name['DB_AC_NUM']).') / ('.$project.') от '.date('Y-m-d').', перейти к редактированию заказа можно по ссылкам:</p>
                    <p><a href="'.$laravel_dir.'/projects_new/1_'.$order1c.'">редактировать заказ в GibLab</a></p>
                    <p>Номер заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_NUM'].'</b></p>
                     <p>ID заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_ID'].'</b></p>
                     <p><br></p>
                     <p><b>Данные клиента:</b></p>
                     <p>Имя: <b>'.$client_fio.'</b></p>
                     <p>Телефон: <b>'.$client_phone.'</b></p>
                     <p>E-mail: <b>'.$client_email.'</b></p>
                     <p><br></p>
                     <p><b>Менеджер:<b><br>
                         <b>'.$manager_name.'</b><br>
                         <span>тел: '.$manager_phone.'</span><br>
                         <span>email: '.$manager_email.'</span><br>
                    </p>
';
    send_text_mail($order1c, $text_client, $client_email, $client_fio, $manager_email, $manager_name, null, $full_project_name);
}

// Закомментированный код содержит ссылку на Раздвижные системы, они недоработаны, поэтому заменяем на код, где ссылки на РС нет
// $text_manager = '
//                      <p>Уважаемый менеджер!</p>
//                      <p>Был сформирован заказ №'.$new_order_id.'('.str2url($new_pr_name['DB_AC_NUM']).') / ('.$project.') от '.date('Y-m-d').', перейти к редактированию заказа можно по ссылкам:</p>
//                      <p><a href="'.$laravel_dir.'/projects_new/1_'.$order1c.'">редактировать заказ в GibLab</a></p>
//                      <p><a href="'.$main_dir.'/clients/RS/clients/create.php?order_project_id='.$RS_project_id.'">редактировать заказ в конструкторе раздвижных систем</a></p>
//                      <p><a href="'.$laravel_dir.'/start_new_porject">Новый заказ</a></p>
//                      <p>Номер заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_NUM'].'</b></p>
//                      <p>ID заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_ID'].'</b></p>
//                      <p><br></p>
//                      <p><b>Данные клиента:</b></p>
//                      <p>Имя: <b>'.$client_fio.'</b></p>
//                      <p>Телефон: <b>'.$client_phone.'</b></p>
//                      <p>E-mail: <b>'.$client_email.'</b></p>
//                      <p><br></p>
//                      <p><b>Менеджер:<b><br>
//                          <b>'.$manager_name.'</b><br>
//                          <span>тел: '.$manager_phone.'</span><br>
//                          <span>email: '.$manager_email.'</span><br>
//                     </p>
                     
//  ';
$text_manager = '
                     <p>Уважаемый менеджер!</p>
                     <p>Был сформирован заказ №'.$new_order_id.'('.str2url($new_pr_name['DB_AC_NUM']).') / ('.$project.') от '.date('Y-m-d').', перейти к редактированию заказа можно по ссылкам:</p>
                     <p><a href="'.$laravel_dir.'/projects_new/1_'.$order1c.'">редактировать заказ в GibLab</a></p>
                     <p><a href="'.$laravel_dir.'/start_new_porject">Новый заказ</a></p>
                     <p>Номер заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_NUM'].'</b></p>
                     <p>ID заказа в АКЦЕНТ: <b>'.$new_pr_name['DB_AC_ID'].'</b></p>
                     <p><br></p>
                     <p><b>Данные клиента:</b></p>
                     <p>Имя: <b>'.$client_fio.'</b></p>
                     <p>Телефон: <b>'.$client_phone.'</b></p>
                     <p>E-mail: <b>'.$client_email.'</b></p>
                     <p><br></p>
                     <p><b>Менеджер:<b><br>
                         <b>'.$manager_name.'</b><br>
                         <span>тел: '.$manager_phone.'</span><br>
                         <span>email: '.$manager_email.'</span><br>
                    </p>
                     
 ';


if($manager_id == 39) {
    $manager_rtz = $ordersMailing; // Список адресов в "startup.php"
    $manager_email = 'uslugi@kronas.com.ua';
} else {
    $sql_rtz = 'SELECT * FROM `manager` WHERE `place` = 1 AND `rtz` = 1';
    $manager_rtz = mysqli_query($link,$sql_rtz)->fetch_assoc()['e-mail'];
    
}
$_SESSION['pr_data_pdf']=vals_index_to_project($vals);
generatePDF_specification($GLOBALS['serv_main_dir'], $_SESSION, $new_pr_name['DB_AC_ID']);
generatePDF_fullReport($GLOBALS['serv_main_dir'], $_SESSION, $new_pr_name['DB_AC_ID']);
send_text_mail($order1c, $text_manager, $manager_email, $manager_name, $manager_email, $manager_name, $manager_rtz, $full_project_name);
$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'confirm_finish', $_SESSION);
put_temp_file_data($_SESSION["data_id"], $_SESSION);
?>



</body>
</html>