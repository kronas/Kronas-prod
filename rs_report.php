<?php
session_start();
$title = 'Отчёт по РС для производства | Кронас';
include_once "header.php";
include_once "func_mini.php";

$temp_storage = $serv_main_dir.'/files/temp/';
$project_storage = $serv_main_dir.'/files/project_out/';
$nodata = 'Нет данных';
$DSPattern = array(
	0 => 'Порезка раздвижных систем',
	1 => 'Засверловка раздвижных систем',
	2 => 'Сборка раздвижных дверей'
);
// Массив видов декора
$decor = array();
if ($result = getTableData('SELECT * FROM `rs_decor`')) {
	foreach ($result as $value) {
		$decor[$value['id']] = $value['name'];
	}
}
// Массивы видов обработки
$secOptions = array(
    'M' => array(
        "0" => 'Без обработки',
        "1" => 'Пескоструй',
        "2" => 'Снятие амальгамы',
        "6" => 'Плёнка Frost'
    ),
    'G' => array(
        "0" => 'Без обработки',
        "1" => 'Фото печать',
        "2" => 'УФ печать',
        "5" => 'УФ печать с заливкой белого фона',
        "4" => 'Стекло – прозрачное + оракал сзади',
        "6" => 'Плёнка Frost'
    ),

);
$client = array();
$place = null;
// В GET передаётся параметр, с помошью которого мы получаем данные проекта
// Передаём идентификатор проекта
if (isset($_GET['ip']) && !empty($_GET['ip'])) {
    if (preg_match('#^[\d]+$#', $_GET['ip'])) {
    	$clientArea = getPlaceAndClient($_GET['ip']);
    	$client['name'] = $clientArea[0]['name'];
        $client['phone'] = $clientArea[0]['phone'];
    	$place = $clientArea[1][0].' '.$clientArea[1][1].' '.$clientArea[1][2];
    	$orderdate = date('d.m.Y', strtotime($clientArea[0]['date']));
        // Если проект есть в базе (файловое хранилище либо база данных)
        if ($p_data = get_Project_file($_GET['ip'])) {
            $RSArray = RS_from_project($p_data['project_data']);
        }
        $fieldOfProject = 'MY_PROJECT_OUT';
        if($result = getTableData('SELECT `'.$fieldOfProject.'` FROM `PROJECT` WHERE `PROJECT_ID` = "'.$_GET['ip'].'"')) {
            $RSArray = RS_from_project(base64_decode($result[0][$fieldOfProject]));
        }
    // Условие если вновь созданный проект идёт из zzz.php
    // zzz.php создаёт временный файл и в GET передаёт его имя (начинается с приставки st_)
    } elseif (preg_match('#^st_[\d]+$#', $_GET['ip'])) {
    	$file = str_replace('st_', '', $_GET['ip']);
    	// Получим клиента, если есть
    	if (file_exists($temp_storage.'ci_'.$file.'.tmp')) {
            $tmp = file_get_contents($temp_storage.'ci_'.$file.'.tmp');
            //..29.12.2020 в таблице `client` поля: pass client_id name tel e-mail
            $tmp = getTableData('SELECT * FROM `client` WHERE `client_id` = "'.$tmp.'"');
            $client['name'] = $tmp[0]['name'];
            $client['phone'] = $tmp[0]['tel'];
            $place = $_SESSION["user_place_f_rs"];
            // Дата заказа
    		$orderdate = date("d.m.Y", substr($_SESSION['vals'][0]['attributes']['ORDERDATE'], 0, 10));
        }
        if (file_exists($temp_storage.$_GET['ip'].'.tmp')) {
            $_SESSION['RSArray'] = file_get_contents($temp_storage.$_GET['ip'].'.tmp');
        }
        $RSArray = unserialize(base64_decode($_SESSION['RSArray']));
    }

    // Комплектующие а так-же общее количество штук и погонных метров
    $accessories 	= array();
    $c = 0;
    $sht = 0;
    $mp = 0;
    foreach ($RSArray['goods'] as $key => $value) {
    	if (isset($RSArray['goods_profiles'][$key][0])) {
    		$accessories[$c]['cnt'] = key($RSArray['goods_profiles'][$key][0]);
    		$accessories[$c]['size'] = $RSArray['goods_profiles'][$key][0][$accessories[$c]['cnt']];
    	} else {
    		$accessories[$c]['cnt'] = '';
    		$accessories[$c]['size'] = '';
    	}
    	$accessories[$c]['name'] = $value['name'];
    	$accessories[$c]['unit'] = $value['unit'];
    	$accessories[$c]['count'] = $value['count'];
    	if ($value['unit'] == 'шт' || $value['unit'] == 'к-т' || $value['unit'] == 'комплект' || $value['unit'] == 'упак' || $value['unit'] == 'упаковка'){
    		$sht += $value['count'];
    	} else $mp += $value['count'];
    	$sht += $accessories[$c]['cnt'];
    	$c++;
    }

    // Услуги
    $services	= array();
    $c 			= 0;
    foreach ($RSArray['goods_serv'] as $value) {
    	if (stristr($value['name'], $DSPattern[0]) || stristr($value['name'], $DSPattern[1]) || stristr($value['name'], $DSPattern[2])) {
    		$services[$c]['name'] = $value['name'];
    		$services[$c]['unit'] = $value['unit'];
    		$services[$c]['count'] = $value['count'];
    		$c++;
    	}
    }
    // Двери (стекло/ДСП) количество
    $doors = array(
    	'glass' => 0,
    	'sheet' => 0,
    	'mixed' => 0
    );
    // Наполнение
	$filling = array();
	$c = 0;
    foreach ($RSArray['doors_data'] as $key => $value) {
    	if ($key > 0) {
    		$mat_g = null; $mat_s = null;
    		foreach ($value as $val) {
    			if ($val['mat'] == 'glass') $mat_g = $val['mat'];
    			elseif ($val['mat'] == 'sheet') $mat_s = $val['mat'];
    			$filling[$c]['name'] = $val['mat_name'];
    			$filling[$c]['width'] = $val['section_x'];
    			$filling[$c]['lenght'] = $val['section_y'];
    			$filling[$c]['count'] = $val['calc']['count'];
    			$filling[$c]['size'] = $val['calc']['mat_area'];
    			$c++;
    		}
    		if (!empty($mat_g) && !empty($mat_s)) {
    			$doors['mixed']++;
    		} else {
    			if (!empty($mat_g)) $doors['glass']++;
    			elseif (!empty($mat_s)) $doors['sheet']++;
    		}
    	}
    }
    // Количество дверей
    $count_doors	= count($RSArray['doors_data'])-1;
    // Количество перехлёстов
    $count_overlaps	= substr($RSArray['rs_count_doorsOverlap'], -1, 1);
    // Профиль РС
    $profile		= $decor[$RSArray['profileDecors']];
	// Услуги по наполнению
	$fillServ = array();
	$c = 0;
	foreach ($RSArray['goods_serv'] as $value) {
    	if (!stristr($value['name'], $DSPattern[0]) && !stristr($value['name'], $DSPattern[1]) && !stristr($value['name'], $DSPattern[2])) {
    		$fillServ[$c]['name'] = $value['name'];
    		$fillServ[$c]['unit'] = $value['unit'];
    		$fillServ[$c]['count'] = $value['count'];
    		$c++;
    	}
    }
    // Получим имя менеджера
    if (isset($clientArea['manager'])) $manager = $clientArea['manager']['name'];
    else $manager = $_SESSION['user']['name'];

}
// _print($clientArea);
// _print($filling);
// _print($_SESSION);
?>
<div class="container-fluid table-container">
	<h1 class="AC MTAMB">Заказ Раздвижных систем</h1>
	<div class="container-fluid">
		<?php if (isset($place) && !empty($place)): ?>
			<h6 class="AC"><?= $place ?></h6>
		<?php endif ?>
		<?php if (isset($RSArray['rs_profile_val']) && !empty($RSArray['rs_profile_val'])): ?>
			<h6 class="AC">Раздвижная система: <span><?= $RSArray['rs_profile_val'] ?></span></h6>
		<?php endif ?>
		<?php if (isset($profile) && !empty($profile)): ?>
			<h6 class="AC">Цвет: <span><?= $profile ?></span></h6>
		<?php endif ?>
	</div>
	<?php // Таблица 3 ?>
	<div class="row">
		<div class="col col-6">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		            <div class="col col-left col-md-4">
		                <p class="pdud bd">Заказчик</p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud">телефон</p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud">Дата заказа</p>
		            </div>
		        </div>
		        <div class="row one-line">
		            <div class="col col-left col-md-4">
		                <p class="pdud prop"><?= $client['name'] ?></p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud prop"><?= $client['phone'] ?></p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud prop"><?= $orderdate ?></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<div class="row">
		<div class="col col-6">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		            <div class="col col-left col-md-12">
		                <p class="AC pdud bd">Размеры проема</p>
		            </div>
		        </div>
		        <div class="row one-line">
		            <div class="col col-left col-md-2">
		                <p class="pdud">Высота, мм.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud">Ширина, мм.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud">Дверей ДСП, шт.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud">Дверей стекло, шт.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud">Дверей комб-х, шт.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud">Перех-в</p>
		            </div>
		        </div>
		        <div class="row one-line">
		            <div class="col col-left col-md-2">
		                <p class="pdud prop"><?=round($RSArray['rs_size_y'], 2)?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud prop"><?=round($RSArray['rs_size_x'], 2)?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud prop"><?=$doors['sheet']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud prop"><?=$doors['glass']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud prop"><?=$doors['mixed']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud prop"><?=$count_overlaps?></p>
		            </div>
		        </div>
		        <div class="row one-line">
		            <div class="col col-left col-md-8">
		                <p class="pdud">ИТОГО Дверей, шт.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud"><?= $doors['sheet'] + $doors['glass'] + $doors['mixed'] ?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p class="pdud"></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<div class="row">
		<div class="col col-6">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		            <div class="col col-left col-md-12">
		                <p class="AC pdud bd">Габариты двери</p>
		            </div>
		        </div>
		        <div class="row one-line">
		            <div class="col col-left col-md-4">
		                <p class="pdud">Высота, мм.</p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud">Ширина, мм.</p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud">Количество дверей</p>
		            </div>
		        </div>
		        <div class="row one-line">
		            <div class="col col-left col-md-4">
		                <p class="pdud prop"><?=round($RSArray['doors_data'][1][0]['height'], 2)?></p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud prop"><?=round($RSArray['doors_data'][1][0]['width'], 2)?></p>
		            </div>
		            <div class="col col-left col-md-4">
		                <p class="pdud prop"><?=$count_doors?></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<?php // Таблица 3.2 изменённая комплектующие ?>
	<div class="row">
		<div class="col col-12">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		        	<div class="col col-left col-md-1">
		                <p></p>
		            </div>
		            <div class="col col-left col-md-7">
		                <p class="bd">Комплектующие</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Ед. изм.</p>
		            </div>
		            <div class="col col-left col-md-1">
	                	<p class="AC">Размер, мм.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Кол-во шт.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Кол-во м.п.</p>
		            </div>
		        </div>
		        <?php $c = 1; ?>
		        <?php foreach ($accessories as $value): ?>
		        <div class="row one-line">
		        	<div class="col col-left col-md-1">
		                <p><?=$c?></p>
		            </div>
		            <div class="col col-left col-md-7">
		                <p><?=$value['name']?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=$value['unit']?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <?php if (!$value['size']): ?>
		                	<p><?=$value['size']?></p>
		                <?php else: ?>
		                	<p><?=round($value['size'], 2)?></p>
		                <?php endif ?>
		            </div>
		            <?php if (($value['unit'] == 'шт' || $value['unit'] == 'к-т' || $value['unit'] == 'комплект' || $value['unit'] == 'упак' || $value['unit'] == 'упаковка') && !empty($value['count'])): ?>
		            <div class="col col-left col-md-1">
		                <p><?=$value['count']?></p>
		            </div>
		            <?php else: ?>
		            <div class="col col-left col-md-1">
		                <p><?=$value['cnt']?></p>
		            </div>
		            <?php endif ?>
					<?php if ($value['unit'] == 'м.п.' || $value['unit'] == 'м.кв.'): ?>
		            <div class="col col-left col-md-1">
		                <?php if (!$value['count']): ?>
		                	<p><?=$value['count']?></p>
		                <?php else: ?>
		                	<p><?=round($value['count'], 2)?></p>
		                <?php endif ?>
		            </div>
		            <?php else: ?>
		            <div class="col col-left col-md-1">
		                <p></p>
		            </div>
		            <?php endif ?>
		        </div>
		        <?php $c++; ?>
		        <?php endforeach ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-10">
		                <p>ИТОГО</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=$sht?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=$mp?></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<?php // Таблица 3.2 изменённая наполнение ?>
	<div class="row">
		<div class="col col-12">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		            <div class="col col-left col-md-1">
		                <p></p>
		            </div>
		            <div class="col col-left col-md-6">
		                <p class="bd">Наполнение</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Ед. изм.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Высота, мм.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Ширина, мм.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Кол-во, шт.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>Площадь, м.кв.</p>
		            </div>
		        </div>
		        <?php $c = 1; $cnt = 0; $s = 0; ?>
		        <?php foreach ($filling as $key => $value): ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-1">
		                <p><?=$c?></p>
		            </div>
		            <div class="col col-left col-md-6">
		                <p><?=$value['name']?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p>м.кв.</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=round($value['width'], 2)?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=round($value['lenght'], 2)?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=$value['count']?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=round($value['size'], 2)?></p>
		            </div>
		        </div>
		        <?php $c++; $cnt += $value['count']; $s += $value['size']; ?>
		        <?php endforeach ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-10">
		                <p>ИТОГО</p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p><?=$cnt?></p>
		            </div>
		            <div class="col col-left col-md-1">
		                <p class="AC"><?=$s?></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<?php // Таблица 3.2 Услуги по наполнению ?>
	<div class="row">
		<div class="col col-6">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		            <div class="col col-left col-md-1">
		                <p></p>
		            </div>
		            <div class="col col-left col-md-7">
		                <p class="bd">Услуги по наполнению</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p>Ед. изм.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p>Кол-во, шт.</p>
		            </div>
		        </div>
		        <?php $c = 1; $s = 0; ?>
		        <?php foreach ($fillServ as $key => $value): ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-1">
		                <p><?=$c?></p>
		            </div>
		            <div class="col col-left col-md-7">
		                <p><?=$value['name']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p><?=$value['unit']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p><?=$value['count']?></p>
		            </div>
		        </div>
		        <?php $c++; $s += $value['count']; ?>
		        <?php endforeach ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-10">
		                <p>ИТОГО</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p><?=$s?></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<div class="row">
		<?php // Таблица 3.2 Услуги по дверям ?>
		<div class="col col-6">
			<div class="container-fluid RS-table">
		        <div class="row one-line">
		            <div class="col col-left col-md-1">
		                <p></p>
		            </div>
		            <div class="col col-left col-md-7">
		                <p class="bd">Услуги по дверям</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p>Ед. изм.</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p>Кол-во, шт.</p>
		            </div>
		        </div>
		        <?php $c = 1; $s = 0; ?>
		        <?php foreach ($services as $key => $value): ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-1">
		                <p><?=$c?></p>
		            </div>
		            <div class="col col-left col-md-7">
		                <p><?=$value['name']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p><?=$value['unit']?></p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p><?=$value['count']?></p>
		            </div>
		        </div>
		        <?php $c++; $s += $value['count']; ?>
		        <?php endforeach ?>
		        <div class="row one-line">
		            <div class="col col-left col-md-10">
		                <p>ИТОГО</p>
		            </div>
		            <div class="col col-left col-md-2">
		                <p><?=$s?></p>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="MTAMB">
				<h6>Менеджер: <span><?=$manager?></span></h6>
			</div>
			<div class="MTAMB">
				<p class="mnpn">Размеры деталей принимаются в миллиметрах. Подпись заказчика___________________</p>
				<p class="mnpn">После оформления заказа дополнения и изменения принимаются в виде дополнительного заказа.</p>
				<p class="mnpn">Претензии по качеству изделий принимаются только при наличии оригинального бланка заказа и в момент отгрузки заказа клиенту.</p>
			</div>
			<div class="MTAMB">
				<p class="mnpn">С момента изготовления, срок хранения заказа на складе составляет 10 рабочих дней.</p>
				<p class="mnpn">Товар изготовленный по индивидуальному заказу клиента, обмену и возврату не подлежит согласно Постановлению КМУ №172 от 19.03.1994 г.</p>
				<p class="mnpn">Заказ считается утверждённым с момента его оплаты.</p>
			</div>
			<div class="MTAMB">
				<p class="mnpn">Претензий по качеству и количеству не имею, заказ получил(а)</p>
			</div>
			<div class="MTAMB w50 MA">
				<p class="mnpn FLL">ДАТА___________________</p>
				<p class="mnpn FLR">ПОДПИСЬ___________________</p>
			</div>
		</div>
	</div>
</div>

<?php include_once("footer.php"); ?>