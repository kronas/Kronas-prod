<?php
date_default_timezone_set('Europe/Kiev');
session_start();

include_once ("func.php");

//  $sql_get_not_readonly_orders = 'SELECT * FROM `ORDER1C` WHERE `production`= 1 AND ((`status` = "выполнен" OR  `status` = "отменён") OR (`readonly`<> 1))  AND `del_programm` IS NULL LIMIT 500';
$sql_get_not_readonly_orders = 'SELECT * FROM `ORDER1C` WHERE `production`= 1 AND ((`status` = "выполнен" OR  `status` = "отменён") OR (`readonly`<> 1)) AND `del_programm` <> 4 LIMIT 500';
$not_readonly_orders = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_get_not_readonly_orders)['data'];

// p_($not_readonly_orders);
unset ($r);
foreach ($not_readonly_orders as $o) {

    unset($res);
    $slq1 = 'SELECT * FROM `PLACES`';
    $oo = sql_data(__LINE__, __FILE__, __FUNCTION__, $slq1)['data'];
    foreach ($oo as $o1)
    {

        
        if($o['DB_AC_ID'] && $o['DB_AC_NUM'] && $o1['PLACES_ID']) {

            $slq_get_tool_equip = 'SELECT * FROM `TOOL_CUTTING_EQUIPMENT` WHERE `place` = ' . $o1['PLACES_ID'];
            $tools_equip = sql_data(__LINE__, __FILE__, __FUNCTION__, $slq_get_tool_equip)['data'];

            $slq_get_tool_equip_xnc = 'SELECT * FROM `tool_xnc_equipment` WHERE `place` = ' . $o1['PLACES_ID'];
            $tools_equip_xnc = sql_data(__LINE__, __FILE__, __FUNCTION__, $slq_get_tool_equip_xnc)['data'];

            // print_r_('<hr><span>Удаляем файлы заказа:</span><br>' . $o['DB_AC_ID'] . '<hr>');
            // p_($o);
            $r[]=$o['DB_AC_ID'];
            /// Delete files form Accent folder
            $accent_dir = $serv_main_dir . '/files/accent/' . $o['DB_AC_ID'];
            // test(1,$accent_dir);
            if (is_dir($accent_dir)) {
                // echo '<hr><br><span>Удаляем директорию Акцента:</span><br>' . $accent_dir . '<hr><br><br><br>';
                if(!delDir($accent_dir)) $res=1;
            }


            /// Delete LC4 files
            foreach ($tools_equip as $t) {
                if ($t['net_folder']) {
                    // unset ($ret_str);
                    // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['net_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                    $ret_str = del_filter_files($serv_main_dir.'/'.$t['net_folder'], $o['DB_AC_ID']);
                    // echo '--- '; var_dump($ret_str); echo '<br />';
                    // if ($ret_str) $res=1;
                    // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['net_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                    $ret_str = del_filter_files($serv_main_dir.'/'.$t['net_folder'], strrev($o['DB_AC_ID']));
                    // echo '--- '; var_dump($ret_str); echo '<br />';
                    
                    // Удаление файлов старше 2 месяцев
                    del_oldest_files_mod($serv_main_dir.'/'.$t['local_folder'], '*.LC4', 5184000);
                }
                if ($t['local_folder']) {
                    // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['local_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                    $ret_str = del_filter_files($serv_main_dir.'/'.$t['local_folder'], $o['DB_AC_ID']);
                    // echo '--- '; var_dump($ret_str); echo '<br />';
                    // echo '<hr><br><span>Удаляем файлы LC4:</span><br>' . $serv_main_dir . '/' . $t['local_folder'] . ' [' . $o['DB_AC_ID'] . ']' . '<hr><br>';
                    $ret_str = del_filter_files($serv_main_dir.'/'.$t['local_folder'], strrev($o['DB_AC_ID']));
                    // echo '--- '; var_dump($ret_str); echo '<br />';

                    // Удаление файлов старше 2 месяцев
                    del_oldest_files_mod($serv_main_dir.'/'.$t['local_folder'], '*.LC4', 5184000);
                }

            }


            /// Delete MPR files
            $actual_year = date('Y');
            $actual_month_number = date('m');
            $actual_month1_string = get_month_name($actual_month_number);
            foreach ($tools_equip_xnc as $t) {
                if($t['net_folder']) {
                    $equip_folder_bhx = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/BHX/';
                    $equip_folder_venture = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/Venture/';
                }
                if($t['local_folder']) {
                    $equip_folder_new_bhx = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                    $equip_folder_new_venture = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $actual_month_number . '_' . $actual_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                }

                $folders = [];

                if($equip_folder_bhx) {
                    $folders[] = $equip_folder_bhx;
                }
                if($equip_folder_venture) {
                    $folders[] = $equip_folder_venture;
                }
                if($equip_folder_new_bhx) {
                    $folders[] = $equip_folder_new_bhx;
                }
                if($equip_folder_new_venture) {
                    $folders[] = $equip_folder_new_venture;
                }

                foreach ($folders as $d) {
                    if (is_dir($d)) {
                    // echo '<hr><br><span>Удаляем папку с МПР:</span><br>' . $d . '<hr><br><br><br>';
                    delDir($d);
                    }
                }

                $next_month_number = $actual_month_number - 1;
                $next_month1_string = get_month_name($next_month_number);

                if($t['net_folder']) {
                    $equip_folder_bhx = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/BHX/';
                    $equip_folder_venture = $serv_main_dir . '/' . $t['net_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/Venture/';
                }
                if($t['local_folder']) {
                    $equip_folder_new_bhx = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                    $equip_folder_new_venture = $serv_main_dir . '/' . $t['local_folder'] . $actual_year . '/' . $next_month_number . '_' . $next_month1_string . '/' . str2url($o['DB_AC_NUM']) . '/';
                }


                $folders = [];

                if($equip_folder_bhx) {
                    $folders[] = $equip_folder_bhx;
                }
                if($equip_folder_venture) {
                    $folders[] = $equip_folder_venture;
                }
                if($equip_folder_new_bhx) {
                    $folders[] = $equip_folder_new_bhx;
                }
                if($equip_folder_new_venture) {
                    $folders[] = $equip_folder_new_venture;
                }

                foreach ($folders as $d) {
                    if (is_dir($d)) {
                    // echo '<hr><br><span>Удаляем папку с МПР:</span><br>' . $d . '<hr><br><br><br>';
                    delDir($d);
                    }
                }
            }
        }
    }
    if (!isset($res2))
    {
        $sql = 'UPDATE ORDER1C SET del_programm=4, stanki=NULL, stanki_time=NULL WHERE ID='.$o['ID'];
        $not_readonly_orders = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql)['data'];
    }
}
p_($r);
?>
