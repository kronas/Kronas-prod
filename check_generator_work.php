<?php
include_once("func.php");

use GuzzleHttp\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$date = file_get_contents('./check_programms_generator_work.txt');
$last_work_programm_date = date_parse_from_format("Y-m-d H:i:s", $date);
$actual_date = date_parse_from_format("Y-m-d H:i:s", date("Y-m-d H:i:s"));


echo $date;
echo '<hr>';
print_r($last_work_programm_date);
echo '<hr>';
print_r($actual_date);


$actual_time_now = ($actual_date['hour'] * 60) + $actual_date['minute'];
$last_work_programm_time = ($last_work_programm_date['hour'] * 60) + $last_work_programm_date['minute'];
$time_diff_now = $actual_time_now - $last_work_programm_time;
$mail_params=$GLOBALS['mail_params'];

if($date && ($time_diff_now > 45)) {

    echo 'Необходимо перезапустить генерацию программ!';

    ///Отправка СМС сообщения
    $xml = '<?xml version="1.0" encoding="utf-8" ?><package key="3ae50516fed179f981adf58f1defa7e557de189e"><message><msg recipient="+380934261735" sender="KRONAS" type="0">Необходимо перезапустить генерацию программ</msg></message></package>';
    $client = new Client();
    $r = $client->post('https://alphasms.ua/api/xml.php', $options = [
        'headers' => [
            'Content-Type' => 'text/xml; charset=UTF8',
        ],
        'body' => $xml,
    ]);

    ///Отправка письма на почту
    $mail = new PHPMailer;
    $mail->SMTPOptions = array (
        'ssl' => array (
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->isSMTP();
    $mail->CharSet = $mail::CHARSET_UTF8;
    $mail->Host = $mail_params['Host'];
    $mail->Port = $mail_params['Port']; // typically 587
    $mail->SMTPSecure = $mail_params['SMTPSecure']; // ssl is depracated
    $mail->SMTPAuth = $mail_params['SMTPAuth'];
    $mail->Username = $mail_params['Username'];
    $mail->Password = $mail_params['Password'];

    $mail->setFrom($mail_params['From'], 'Service-Kronas-Reports');
    $mail->addAddress('totsenko.anton@kronas.com.ua', 'Anton Totsenko');


    $mail->Subject = 'Ошибка службы генерации программ';
//    $mail->SMTPDebug  = 2;

    $text_erorr_email ='Сообщение: Необходимо перезагрузить службу генерации программ. Время крайней генерации: '.$date;

    $mail->msgHTML($text_erorr_email); // remove if you do not want to send HTML email
    ////$mail->AltBody =  'Ваш заказ успешно подтвержден!';
    $mail->send();

///    print_r($r->getBody()->getContents());

} else {
    echo 'Все в порядке!';
}