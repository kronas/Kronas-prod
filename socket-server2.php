<?php
define(ROOT_PATH, dirname(__FILE__));

include_once(ROOT_PATH . '/func.php');
require_once ROOT_PATH. '/vendor/autoload.php';
date_default_timezone_set('Europe/Kiev');
use React\Socket\Connection;

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server('0.0.0.0:3000', $loop);

$link = db_connect();
mysqli_set_charset($link, "utf8mb4");

if (!$link) {
    echo "err code: " . mysqli_connect_errno() . PHP_EOL;
    echo "err msg: " . mysqli_connect_error() . PHP_EOL;
    exit;
} else {
    echo 'DB Connected successfully'. PHP_EOL;
}

$sql = "SET NAMES utf8;";
mysqli_query($link, $sql) or die(mysqli_error($link));


// drop table if needed on socket startup
// $sql = "DROP TABLE IF EXISTS requests;";
// mysqli_query($link, $sql) or die(mysqli_error($link));

$sql = "CREATE TABLE IF NOT EXISTS `SHEET_COMPLETE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PLACE` int(11) NULL,
  `EQUIPMENT` int(11) NULL,
  `MATERIAL` varchar(1024) NOT NULL,
  `THIKNESS` double NOT NULL,
  `DATE` datetime NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
mysqli_query($link, $sql) or die(mysqli_error($link));

$sql = "CREATE TABLE IF NOT EXISTS requests
( id INT(11) NOT NULL AUTO_INCREMENT,
  headers text CHARACTER SET utf8 COLLATE utf8_general_ci,
  body text CHARACTER SET utf8 COLLATE utf8_general_ci,
  date DATETIME,
  PRIMARY KEY (id)
) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';";
mysqli_query($link, $sql) or die(mysqli_error($link));

// add name column
#$sql = "ALTER TABLE requests ADD COLUMN IF NOT EXISTS name VARCHAR(255);";
#mysqli_query($link, $sql) or die(mysqli_error($link));

// add action column
#$sql = "ALTER TABLE requests ADD COLUMN IF NOT EXISTS action VARCHAR(255);";
#mysqli_query($link, $sql) or die(mysqli_error($link));

// add sheet_complete relation
#$sql = "ALTER TABLE requests ADD COLUMN IF NOT EXISTS sheet_complete int(11) NULL;";
#mysqli_query($link, $sql) or die(mysqli_error($link));

echo 'DB table initialized successfully'. PHP_EOL;

echo 'Starting sync data from file .stream'. PHP_EOL;
include ROOT_PATH.'/socket-server.php';
echo 'DB Sync data complete'. PHP_EOL;

mysqli_close($link);

$buffer = '';

$socket->on('connection', function(Connection $connection) use (&$buffer) {
  // connect to db
  $link = db_connect();
  mysqli_set_charset($link, "utf8mb4");

  #$sql = "SET NAMES utf8;";
  #mysqli_query($link, $sql) or die(mysqli_error($link));

  $body_len = 0;
  $header_len = 0;
  $length = 0;

  $headers = '';
  $body = '';
  $name = '';
  $action = '';

  $connection->on('close', function () use (&$link) {
    mysqli_close($link);
  });

  $connection->on('data', function ($data) use (&$name, &$action, &$headers, &$body, &$length, &$body_len, &$link, &$buffer, $connection) {
    file_put_contents(ROOT_PATH . '/socket-buffer.stream', $data, FILE_APPEND | LOCK_EX);
    $lines = explode(PHP_EOL, $data);
$connection->write("HTTP/1.1 200 OK\r\n\r\n");
$log.= "

++++++++++++++++++++++++++++++++++++

".date('Y-m-d H:i:s'). PHP_EOL . $data . PHP_EOL;

if (!$lines) echo "не распознан пакет";
    foreach ($lines as $line)
    {
      $line = str_replace(PHP_EOL, '', $line);
      $line = mb_convert_encoding($line, 'utf8', mb_detect_encoding($line));

      if ($length > 0)
      {
        $body .= $line . PHP_EOL;
        $body_len += strlen($line);
      } 
      else 
      {
        $headers .= $line . PHP_EOL;
      }

      // find name
      preg_match('/name: (\d+)/', $line, $matches);
      if ($matches){ $name = $matches[1]; }

      // find action
      preg_match('/action: (\S+)/', $line, $matches);
      if ($matches){ $action = $matches[1]; }
 
      if ($length == 0){
        preg_match('/Content-Length: (\d+)/', $line, $matches);
        $length = $matches[1];
      };

      //echo $body_len.PHP_EOL;

      if (($length > 0) && ($body_len >= $length))
      {
        $buffer .= $line;
        $buffer = mb_convert_encoding($buffer, 'utf8', mb_detect_encoding($buffer));

        //echo $buffer;
        echo "Pack $action got...
        
        ";
        $body = trim($body);
        $headers = trim($headers);

        $date = date('Y-m-d H:i:s');

        $error = '';
        if ($action === 'pattern')
        {
          $body = substr($body, 1);

          $parts = explode(',', $body);
          $material = $parts[7];
          $material_id = null;
          $sheet_complete_id = null;

          $d = preg_replace("/[^0-9]/", "", $parts[0]);
          $date = substr($d, 0, 4).'-'.substr($d, 4, 2).'-'.substr($d, 6, 2). ' '.substr($d, 8, 2).':'.substr($d, 10, 2).':'.substr($d, 12, 2);
          //echo $date;
          // get material name
          $sql = "select * from MATERIAL where NAME='".$material."';";
          $log.= date('Y-m-d H:i:s'). PHP_EOL . $sql . PHP_EOL;
          $data_get = mysqli_query($link, $sql);
          if (mysqli_num_rows($data_get) != 0)
          {
            $data = mysqli_fetch_array($data_get);
            $material = $data['NAME'];
            $material_id = $data['MATERIAL_ID'];

            // get place id
            $sql = "select * from TOOL_CUTTING_EQUIPMENT where ID=".$name.";";
            $log.= date('Y-m-d H:i:s'). PHP_EOL . $sql . PHP_EOL;
            $data_get = mysqli_query($link, $sql);
            if (mysqli_num_rows($data_get) != 0)
            {
              $data = mysqli_fetch_array($data_get);
              $place = $data['PLACE'];
            }

            $thickness = $parts[10];

            $sql = "insert into SHEET_COMPLETE (PLACE, EQUIPMENT, MATERIAL_ID, MATERIAL, THIKNESS, DATE) values (".$place.", ".$name.", ".$material_id.", '".$material."', ".$thickness.", '".$date."');";
            //echo $sql;
            mysqli_query($link, $sql);

            $sheet_complete_id = mysqli_insert_id($link);
            $log.= date('Y-m-d H:i:s'). PHP_EOL . $sql . PHP_EOL;
            echo mysqli_error($link) . PHP_EOL;

            if (!$sheet_complete_id) 
            {
              $error.=" Не удалось вставить запись о листе в SHEET_COMPLETE. ";
              echo '[error] -> '.$error. PHP_EOL;
              $log.= date('Y-m-d H:i:s'). PHP_EOL . $error . PHP_EOL;
            }

          }
          else 
          {
            $material_id = 'null';
            $sheet_complete_id = 'null';
            $error.=" Неизвестный материал ";
            echo '[error] -> '.$error . PHP_EOL;
            $log.= date('Y-m-d H:i:s'). PHP_EOL . $error . PHP_EOL;
          }

          // insert requests row
          $sql = "insert into requests (name, action, headers, body, sheet_complete, error, date) values ('".$name."', '".$action."', '".$headers."', '".$body."', ".$sheet_complete_id.", '".$error."', '".$date."');";
          $log.= date('Y-m-d H:i:s'). PHP_EOL . $sql . PHP_EOL;
          mysqli_query($link, $sql);

          //echo date('Y-m-d H:i:s'). PHP_EOL . $buffer . PHP_EOL;
          echo mysqli_error($link) . PHP_EOL;
        } 
        else 
        {
          $date = date('Y-m-d H:i:s');

          $sql = "insert into requests (name, action, headers, body, sheet_complete, date) values ('".$name."', '".$action."', '".$headers."', '".$body."', NULL,'".$date."');";
          mysqli_query($link, $sql);

          $log.= date('Y-m-d H:i:s'). PHP_EOL . $sql . PHP_EOL;
          echo mysqli_error($link) . PHP_EOL;
        }
        file_put_contents(ROOT_PATH . "/sql_log_socket.txt",$log,FILE_APPEND | LOCK_EX);
        //$connection->write("HTTP/1.1 200 OK\r\n\r\n");

        $name = '';
        $action = '';
        $buffer = '';
        $headers = '';
        $body = '';

        $length = 0;
        $body_len = 0;
      } 
      else 
      {
        $buffer .= $line;
      };
    };

  });
});
 
echo "Listening on {$socket->getAddress()}\n";

$loop->run();

echo '';
?>
