(function ($) {


    $(document).ready(function () {

        $(".discountPercent").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            if ($(this).val()<0) $(this).val('');
            if ($(this).val()>99) $(this).val(99);
            var numInput=$(this).attr('id');
            if(numInput=='discountPercent1') recalc_table($(this).val());
            if(numInput=='discountPercent2') recalc_table_add($(this).val());
        });

        $('[data-toggle="popover"]').popover({
            title: function () { return  $(this).attr('data-title') },
            html: true,
            placement:'auto',
            trigger: 'hover',
            content: function () { return '<div class="d-flex flex-column">' +
                '<div class="text-center"><img  class="rounded" src="' + $(this).attr('data-img') + '"></div>' +
            '' + $(this).attr('data-ins') + '</div>' }
        });


        $(document).on({
            mouseenter: function(){

                $(this).popover({
                    title: function () { return  $(this).attr('data-title') },
                    html: true,
                    trigger: 'hover',
                    placement: 'auto',
                    content: function () { return '<div class="d-flex flex-column">' +
                        '<div class="text-center"><img  class="rounded" src="' + $(this).attr('data-img') + '"></div>' +
                        '' + $(this).attr('data-ins') + '</div>' }
                });
            },
            mouseleave: function(){
                $( this ).popover('hide');
            }
        }, 'tr.addtable-tr');
/*
        $(document).on('click', '[data-toggle="popover"]', function (event, sel) {
            console.log('g');
            $(this).popover('enable');
            $(this).popover({
                title: function () { return  $(this).attr('data-title') },
                html: true,
                placement:'auto',
                trigger: 'hover',
                content: function () { return '<div class="d-flex flex-column">' +
                    '<div class="text-center"><img  class="rounded" src="' + $(this).attr('data-img') + '"></div>' +
                    '' + $(this).attr('data-ins') + '</div>' }
            });
        });

        */
     //   $('[data-toggle="popover"]').popover();

        $(document).on('click', '.erase-tr', function (event, sel) {
            $(this).parent().parent().remove();
            recalc_table_add($("input#discountPercent2").val());
        });


        $(document).on('keyup blur', '.table-add-list input', function (event, sel) {
            setTimeout(function(){  recalc_table_add($("input#discountPercent2").val()); }, 1000);
        });

        $(document).on('click', 'button[name="pdf-crea"]', function (event, sel) {
            var total = [];
            var table1 = [];
            var table2 = [];
            var table3 = [];
            var table4 = [];
            $('.table.table-1 tr.tablerow').each(function (index, value) {
                var tr =$(this);
                table1[index]=[
                    tr.children('td').eq(0).html(),
                    tr.children('td').eq(1).html(),
                    tr.children('td').eq(2).html(),
                    tr.children('td').eq(3).html(),
                    tr.children('td').eq(4).html()
                ];
            });
            $('.table.table-2 tr.tablerow').each(function (index, value) {
                var tr =$(this);
                table2[index]=[
                    tr.children('td').eq(0).html(),
                    tr.children('td').eq(1).html(),
                    tr.children('td').eq(2).html(),
                    tr.children('td').eq(3).html(),
                    tr.children('td').eq(4).html()
                ];
            });
            $('.table.table-3 tr.tablerow').each(function (index, value) {
                var tr =$(this);
                table3[index]=[
                    tr.children('td').eq(0).html(),
                    tr.children('td').eq(1).html(),
                    tr.children('td').eq(2).html(),
                    tr.children('td').eq(3).html(),
                    tr.children('td').eq(4).html(),
                    tr.children('td').eq(5).html()
                ];
            });
            $('.table.table-4 tr.tablerow').each(function (index, value) {
                var tr =$(this);

                //.. Если раздвижная система, то количество содержится в теге <p>
                let count;
                if (tr.children('td').children('p').length != 0) {
                    count = tr.children('td').children('p').html();
                } else {
                    count = tr.children('td').children('input').val();
                }

                var nameo0= tr.children('td').eq(1);
                nameo0.children().remove();
                table4[index]=[
                    tr.children('td').eq(0).html(),
                    nameo0.html(),
                    count,
                    tr.children('td').eq(3).html(),
                    tr.children('td').eq(4).html()
                ];
            });
            total[0]=$('div.resulttotal-0').html();
            total[1]=$('div.resulttotal-1').html();
            total[2]=$('div.resulttotal-2').html();
            total[3]=$('div.resulttotal-3').html();
            total[4]=$('div.fulltotal').html();
            var out={};
            out['total'] = total;
            out['table1'] = table1;
            out['table2'] = table2;
            out['table3'] = table3;
            out['table4'] = table4;


            var outJSON = JSON.stringify(out);

            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", 'pdf-crea.php');
            form.setAttribute("target", 'pdf');


            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'data';
            input.value = outJSON;
            form.appendChild(input);

            document.body.appendChild(form);

            form.submit();
            document.body.removeChild(form);
            return false;
        });


        $("#searchOnCode").keyup(function(){
            $.ajax({
                type: "POST",
                url: "ajax_resp.php",
                data:'keyword='+$(this).val(),
                beforeSend: function(){
                    $(".loading-search").show();
                },
                success: function(data){
                    $("#searchOnCode-box").show();
                    $("#searchOnCode-box").html(data);
                    $(".loading-search").hide();
                }
            });
        });


        $("#searchOnCode").blur(function(){
            setTimeout(function(){  $("#searchOnCode-box").hide(); }, 1000);

        });

        $(document).on('shown.bs.modal', '#selectPart', function (event) {
            var button = $(event.relatedTarget);
            var recipient = button.data('targetid');
            $('#targetid').val(recipient);
            $(".loading-search").hide();
            $(this).find(".modal-body-in").load("addlist2browse.php?targetid="+recipient);
            /*
             if (typeop==1 || typeop==3) {
             //                $("ul#simple_list_material").show();
             //                $("ul#simple_list_band").hide();


             $("ul#simple_list > li").hide();
             $("ul#simple_list > li[data-folderid=593]").show();


             $("ul.list-data-tree-material").show();
             $("ul.list-data-tree-band").hide();
             }
             if (typeop==2 || typeop==4) {
             //                $("ul#simple_list_material").hide();
             //                $("ul#simple_list_band").show();

             $("ul#simple_list > li").hide();
             $("ul#simple_list > li[data-folderid=470]").show();


             $("ul.list-data-tree-material").hide();
             $("ul.list-data-tree-band").show();
             }
             if (typeop==4) {
             var t = parseInt(button.data('t'));
             $('#t').val(t);
             $('[class ^= list-data-tree-] li').each(function(i,elem) {
             if(parseInt($(elem).data('w'))>=((t*2)+3)) $(elem).show();
             else  $(elem).hide();
             });
             }
             */
        });
    });



    function recalc_table(dp) {
        var resulttotal=[0,0,0];
        $('.tablerow.group-1').each(function (index, value) {
            var numcalc =Number($(this).attr('data-calc'));
            var Icount=Number( $(this).children('.count').attr('data-count'));
            var Iprice=Number( $(this).children('.price').attr('data-price'))*((100-dp)/100);
            $(this).children('.price').text(number_format(Iprice,2,'.',' '));
            var rsum=Icount*Iprice;
            resulttotal[numcalc]=resulttotal[numcalc]+rsum;
            $(this).children('.total').text(number_format(rsum,2,'.',' '));
        });
        $('.resulttotal-0').text(number_format(resulttotal[0],2,'.',' ')+' грн.');
        $('.resulttotal-1').text(number_format(resulttotal[1],2,'.',' ')+' грн.');
        $('.resulttotal-2').text(number_format(resulttotal[2],2,'.',' ')+' грн.');
        $('.resulttotal-0').attr('data-total',resulttotal[0]);
        $('.resulttotal-1').attr('data-total',resulttotal[1]);
        $('.resulttotal-2').attr('data-total',resulttotal[2]);
        var rez=resulttotal[0]+resulttotal[1]+Number($('.resulttotal-3').attr('data-total'));
        $('.fulltotal').text('Итого: '+number_format(rez,2,'.',' ')+' грн.');
    }

    function recalc_table_add(dp) {
        console.log('recalc_table_add');
        var resulttotal=0;
        $('.tablerow.group-2').each(function (index, value) {
            var numcalc =Number($(this).attr('data-calc'));
            var tdcount=$(this).children('.count');
            var Icount=Number($(tdcount).children('input').val());
            var Iprice=Number( $(this).children('.price').attr('data-price'))*((100-dp)/100);
            $(this).children('.price').text(number_format(Iprice,2,'.',' '));
            var rsum=Icount*Iprice;
            resulttotal=resulttotal+rsum;
            $(this).children('.total').text(number_format(rsum,2,'.',' '));
        });
        $('.resulttotal-3').text(number_format(resulttotal,2,'.',' ')+' грн.');
        $('.resulttotal-3').attr('data-total',resulttotal);
        console.log(resulttotal);
        console.log(Number($('.resulttotal-0').attr('data-total')));
        console.log(Number($('.resulttotal-1').attr('data-total')));
        var rez=resulttotal;
        if($("div").is(".resulttotal-0")) rez=rez+Number($('.resulttotal-0').attr('data-total'));
        if($("div").is(".resulttotal-1")) rez=rez+Number($('.resulttotal-1').attr('data-total'));
        $('.fulltotal').text('Итого: '+number_format(rez,2,'.',' ')+' грн.');
        if ($(".table-add-list tbody tr").length === 0) {
            $(".lastBlock").removeClass('visible')
            $(".lastBlock").addClass('hide')
        }
    }


    function number_format(number, decimals, dec_point, thousands_sep) {
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
                .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
                .join('0');
        }
        return s.join(dec);
    }

})(jQuery);
