<?php

/**
 * Скрипт генерации программ KRONAS.SERVICE
 * Автор: Куранда "ANDR0" Сергей (kuranda.sergey@kronas.com.ua)
 * Версия: 2021.11.17-pre
 */

error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
ini_set('display_startup_errors', 0);
ini_set('display_errors', 0);
ini_set('log_errors', 1);

date_default_timezone_set('Europe/Kiev');

// Проверяем на режим запуска скрипта (cli - консольно, cgi... - в режиме cgi, apache... - в качестве модуля Apache
if (php_sapi_name() != 'cli') {
	die("The interactive launch of the script is forbidden");
};

echo "[ *** PROCESSING " . __FILE__ . " (id " . getmypid() . ") ***]\n";

pcntl_signal(SIGTERM, "sig_handle");
pcntl_signal(SIGINT, "sig_handle");

function sig_handle($signal)
{
	global $TERMINATE;
	
    switch($signal)
    {
        case SIGTERM:
            print "Got SIGTERM\n";
            $TERMINATE = true;
        break;
        case SIGKILL:
            print "Got SIGKILL\n";
            $TERMINATE = true;
        break;
        case SIGINT:
            print "User pressed Ctrl+C - Got SIGINT\n";
            $TERMINATE = true;
        break;
    }
}

session_start();
require_once '_1/config.php';
require_once DIR_CORE . 'func.php';

declare(ticks = 1);
$TERMINATE = false;
$BREAK = false;
$CYCLE = true;

$breake_flag = dirname(__FILE__) . '/' . basename(__FILE__, '.php');

// Удаляем файл флага остановки (если он имеется
if (file_exists($breake_flag)) unlink($breake_flag);

/* =================================================================
 * Основной цикл программы
 * ================================================================= */
echo "[ *** Starting on " . date("Y-m-d h:m:s") . " ]\n";
echo "[ PHP:memory_limit: " . ini_get('memory_limit') . " ]\n";
echo "[ PHP:max_execution_time: " . ini_get('max_execution_time') . " ]\n";
echo "[ PHP:max_input_vars: " . ini_get('max_input_vars') . " ]\n";

while($CYCLE)
{

	//.. 20.08.2022 Лог автогенерации
	// Ключ для "refresh_project_programm.php" чтобы определять, что он вызван отсюда
	$_SESSION['cycle_generate'] = 1;
	file_put_contents(dirname(__FILE__) . '/log/auto_generate_programm_cycle/' . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . ' --- Cycle start' . "\r\n", FILE_APPEND);

	// Проверяем на наличие файла с таким же названием, но без расширения - флаг отработки выхода из цикла
	if ( file_exists($breake_flag) ) {
		echo "[ BREAK DETECT ]\n";
		$BREAK = true;
	}
	
	// Если в начале цикла видим запрос на завершение - прерываем выполнение программы
	if ($BREAK OR $TERMINATE) break;
	
	//echo "[ ... " . date("Y-m-d h:m:s") . " ]\n";
	
	$sql_request = 'SELECT * FROM `ORDER1C` as `o` JOIN `PROJECT` as `p` ON (`o`.`ID` = `p`.`ORDER1C`) WHERE `stanki` IS NULL AND `o`.`readonly` = 1 AND `o`.`ID` = `p`.`ORDER1C` AND `o`.`production` = 1  AND `o`.`status` = "в работе" AND `o`.`comment` != "checked" AND `o`.`comment` != "problem" AND `o`.`DB_AC_ID` > 0 GROUP BY `p`.`PROJECT_ID` ORDER BY `p`.`PROJECT_ID` DESC LIMIT 1';
    $order_data = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_request);
	
	if( isset($order_data['data'][0]) && $order_data['data'][0]['ID'] > 0 ) 
    {
		$order_data = $order_data['data'][0];
		$order_start = microtime(true);
		echo "Инициализация обработка проекта с [order_id = " . $order_data['ID'] . "]\n";
		echo "- Aкцент ID           : " . $order_data['DB_AC_ID'] . "\n";
		echo "- Номер в Акценте     : " . $order_data['DB_AC_NUM'] . "\n";
		echo "- ID в сервисе        : " . $order_data['ORDER1C'] . "\n";
		echo "- Клиент              : " . $order_data['client_fio'] . "\n";
		echo "- Дата                : " . $order_data['DATE'] . "\n";
      
		$timer=timer(1,__FILE__,__FUNCTION__,__LINE__,'need_to_new_generate '.$order_data['DB_AC_NUM'], $_SESSION);
	  
        $sql_request = 'SELECT * FROM `PROJECT` WHERE ORDER1C='.$order_data['ID'].' ORDER BY `PROJECT_ID` DESC LIMIT 1';
        $project_id = sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_request)['data'][0]['PROJECT_ID'];

        //.. 20.08.2022 Лог автогенерации
        file_put_contents(dirname(__FILE__) . '/log/auto_generate_programm_projects/' . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . ' --- Project_id - ' . $project_id . "\r\n", FILE_APPEND);
		
		echo "* project_id          : " . $project_id . " ";
		
		if ( $file_data = get_Project_file($project_id) ) echo "[OK]\n";
        else echo "[File NOT found!]\n";
		
		// Дальше - точка безоговорочного завершения программы
		if ($BREAK OR $TERMINATE) break;
		
		if ( $file_data && !empty($file_data) ) 
        {
			$order_id = $order_data['ID'];
			
			$refresh_start = microtime(true);
			echo "Обработка refresh_project_programm [order_id = " . $order_id . "] ...\n";
			
			unset($t);

			//.. 20.08.2022 Лог автогенерации
        	file_put_contents(dirname(__FILE__) . '/log/auto_generate_programm_projects/' . date('Y-m-d') . '.log', date('Y-m-d H:i:s') . ' --- Вызов refresh_project_programm.php' . "\r\n", FILE_APPEND);
        	
            include('refresh_project_programm.php');
			
			if(isset($t['res'])) 
            {
                $sql_update_order = 'UPDATE `ORDER1C` SET `comment`= "checked" WHERE `ID` = '. $order_id;    
                if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_order)['res']<>1) echo "*** Проблема с обновлением записи checked\n";
				
                $sql_update = 'UPDATE `ORDER1C` SET `stanki`=1, `stanki_time` = CURRENT_TIMESTAMP,`del_programm` = NULL WHERE `ID` = '.$order_id;
                if (sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update)['res']<>1) echo "*** Проблема с установкой stanki\n";
				
                echo "Программы на сервисе успешно обновлены!\nЗапускаем отправку на станки...\n";
                sleep(5);     
            }
            else
            {
                $sql_update_order = 'UPDATE `ORDER1C` SET `comment`= "problem" WHERE `ID` = '. $order_id;    
                if ( sql_data(__LINE__,__FILE__,__FUNCTION__,$sql_update_order)['res'] <> 1 ) echo "*** Проблема с обновлением записи checked";
            }
			
			echo "* Время выполнения обработки T2: " . round(microtime(true) - $refresh_start, 4) . " сек.\n";
		} else {
			sleep(5);
		}
		
		echo "* Время выполнения обработки T1: " . round(microtime(true) - $order_start, 4) . " сек.\n";
		echo "* Пиковое использование памяти: " . (memory_get_peak_usage(true)/1024/1024) . " МБ \n";

	}
}

echo "[ *** Ending on " . date("Y-m-d h:m:s") . " ]\n";
